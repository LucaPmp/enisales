package com.eni.enigaseluce.utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class JSONUtility {

    // ======================================================================================
    // LOGIN RESPONSE PARSER HANDLER
    // ======================================================================================

    /**
     * Work with Constants.SERVIZIO_LOGIN
     * 
     * @param arrayFornitureLogin
     * @param codiceConto
     * @return
     */
    public static String cercaTipologiaFornitura(JSONArray arrayFornitureLogin, String codiceConto) {
	String result = "";
	JSONObject anagrContoIesimo = new JSONObject();
	JSONObject anagrExtraContoIesimo = new JSONObject();
	try {
	    for (int i = 2; i < arrayFornitureLogin.length(); i++) {
		JSONArray contoIesimo;
		contoIesimo = arrayFornitureLogin.getJSONArray(i);
		anagrContoIesimo = contoIesimo.getJSONObject(0);

		String codiceContoIesimo = anagrContoIesimo.getString("CodiceConto");
		if (!codiceContoIesimo.equals(codiceConto)) {
		    continue;
		}
		else {
		    // ho trovato il conto codiceConto
		    if (contoIesimo.length() > 1) {
			anagrExtraContoIesimo = contoIesimo.getJSONObject(1);
			result = anagrExtraContoIesimo.getString("Tipologia");
		    }
		}
	    }
	}
	catch (JSONException e) {
	    e.printStackTrace();
	}
	return result;

    }

    /**
     * Work with Constants.SERVIZIO_LOGIN
     * 
     * @param arrayFornitureLogin
     * @param codiceConto
     * @return
     */
    public static boolean isDual(JSONArray arrayFornitureLogin, String codiceConto) {
	boolean result = false;
	int count = 0;
	JSONObject anagrContoIesimo = new JSONObject();
	try {
	    for (int i = 2; i < arrayFornitureLogin.length(); i++) {
		JSONArray contoIesimo;
		contoIesimo = arrayFornitureLogin.getJSONArray(i);
		anagrContoIesimo = contoIesimo.getJSONObject(0);

		String codiceContoIesimo = anagrContoIesimo.getString("CodiceConto");
		if (!codiceContoIesimo.equals(codiceConto)) {
		    continue;
		}
		else {
		    // ho trovato il conto codiceConto
		    count++;
		}
	    }
	    if (count == 2) {
		result = true;
	    }
	    else {
		result = false;
	    }

	}
	catch (JSONException e) {
	    e.printStackTrace();
	}
	return result;

    }

    /**
     * Work with Constants.SERVIZIO_LOGIN
     * 
     * @param array
     * @param codiceConto
     * @return
     */
    public static String cercaIndirizzoFornitura(JSONArray array, String codiceConto) {
	String result = "";
	String codiceContoIesimo = "";
	JSONObject anagrContoIesimo = new JSONObject();

	try {
	    for (int i = 2; i < array.length(); i++) {
		JSONArray contoIesimo;

		contoIesimo = array.getJSONArray(i);
		// codice conto
		anagrContoIesimo = contoIesimo.getJSONObject(0);
		codiceContoIesimo = anagrContoIesimo.getString("CodiceConto");

		if (!codiceContoIesimo.equals(codiceConto)) {
		    // codice conto diverso
		    continue;
		}
		else {
		    JSONObject anagrExtraContoIesimo = contoIesimo.getJSONObject(1);
		    result = anagrExtraContoIesimo.getString("IndirizzoCompleto");
		}

	    }
	}
	catch (JSONException e) {
	    e.printStackTrace();
	}
	return result;
    }

    /**
     * Work with input Constants.SERVIZIO_LOGIN
     * 
     * @param array
     * @param codiceConto
     * @return
     */
    public static String trovaTipoProdottoFornitura(JSONArray array, String codiceConto) {
	String result = "";
	String codiceContoIesimo = "";
	JSONObject anagrContoIesimo = new JSONObject();
	try {
	    for (int i = 2; i < array.length(); i++) {
		JSONArray contoIesimo;
		contoIesimo = array.getJSONArray(i);
		// codice conto
		anagrContoIesimo = contoIesimo.getJSONObject(0);
		codiceContoIesimo = anagrContoIesimo.getString("CodiceConto");
		if (!codiceContoIesimo.equals(codiceConto)) {
		    // codice conto diverso
		    continue;
		}
		else {
		    JSONObject extraContoIesimo = contoIesimo.getJSONObject(1);
		    result = extraContoIesimo.getString("TipoProdotto");
		}

	    }
	}
	catch (JSONException e) {
	    e.printStackTrace();
	}
	return result;
    }

    /**
     * Work with input Constants.SERVIZIO_LOGIN
     * 
     * @param rispostaLogin
     * @return
     */
    public static String getCodiceCliente(String rispostaLogin) {
	String result = "";
	try {
	    JSONArray loginJson = new JSONArray(rispostaLogin);
	    JSONObject datiCliente = (JSONObject) loginJson.get(1);
	    result = datiCliente.getString("CodiceCliente");
	}
	catch (JSONException e) {
	    e.printStackTrace();
	}
	return result;
    }

    /**
     * Work with input Constants.SERVIZIO_LOGIN
     * 
     * @param array
     * @param codiceConto
     * @return
     */
    public static String getPartitaIVACliente(String rispostaLogin) {
	String result = "";

	try {
	    JSONArray loginJson = new JSONArray(rispostaLogin);
	    JSONObject datiCliente = (JSONObject) loginJson.get(1);
	    result = datiCliente.getString("partitaIVA");
	}
	catch (JSONException e) {
	    e.printStackTrace();
	}

	return result;
    }

    /**
     * Work with input Constants.SERVIZIO_LOGIN
     * 
     * @param array
     * @param codiceConto
     * @return
     */
    public static boolean isPartitaIVACorretta(String rispostaLogin) {

	boolean result = false;
	try {
	    JSONArray loginJson = new JSONArray(rispostaLogin);
	    JSONObject datiCliente = (JSONObject) loginJson.get(1);
	    result = datiCliente.getBoolean("PIVA_corretta");
	}
	catch (JSONException e) {
	    e.printStackTrace();
	}

	return result;
    }

    /**
     * Work with input Constants.SERVIZIO_LOGIN
     * 
     * @param array
     * @param codiceConto
     * @return
     */
    public static String getRagioneSocialeCliente(String rispostaLogin) {
	String result = "";

	try {
	    JSONArray loginJson = new JSONArray(rispostaLogin);
	    JSONObject datiCliente = (JSONObject) loginJson.get(1);
	    result = datiCliente.getString("CognomeCliente");
	}
	catch (JSONException e) {
	    e.printStackTrace();
	}

	return result;
    }

    /**
     * Work with input Constants.SERVIZIO_LOGIN
     * 
     * @param rispostaLogin
     * @return
     */
    public static String getUserId(String rispostaLogin) {
	String result = "";
	try {
	    JSONArray loginJson = new JSONArray(rispostaLogin);
	    JSONObject datiCliente = (JSONObject) loginJson.get(1);
	    result = datiCliente.getString("UserId");
	}
	catch (JSONException e) {
	    e.printStackTrace();
	}
	return result;
    }

    /**
     * 
     * Work with input Constants.SERVIZIO_LOGIN
     * 
     * @param rispostaLogin
     * @return
     */
    public static String getNomeCliente(String rispostaLogin) {
	String result = "";
	try {
	    JSONArray loginJson = new JSONArray(rispostaLogin);
	    JSONObject datiCliente = (JSONObject) loginJson.get(1);
	    result = datiCliente.getString("NomeCliente");
	}
	catch (JSONException e) {
	    e.printStackTrace();
	}
	return result;
    }

    /**
     * Work with input Constants.SERVIZIO_LOGIN
     * 
     * @param rispostaLogin
     * @return
     */
    public static String getCognomeCliente(String rispostaLogin) {
	String result = "";
	try {
	    JSONArray loginJson = new JSONArray(rispostaLogin);
	    JSONObject datiCliente = (JSONObject) loginJson.get(1);
	    result = datiCliente.getString("CognomeCliente");
	}
	catch (JSONException e) {
	    e.printStackTrace();
	}
	return result;
    }

    /**
     * Work with input Constants.SERVIZIO_LOGIN
     * 
     * @param rispostaLogin
     * @return
     */
    public static String getCodiceFiscaleCliente(String rispostaLogin) {
	String result = "";
	try {
	    JSONArray loginJson = new JSONArray(rispostaLogin);
	    JSONObject datiCliente = (JSONObject) loginJson.get(1);
	    result = datiCliente.getString("CodiceFiscaleCliente");
	}
	catch (JSONException e) {
	    e.printStackTrace();
	}
	return result;
    }

    /**
     * Work with input Constants.SERVIZIO_LOGIN
     * 
     * @param rispostaLogin
     * @return
     */
    public static boolean isCodiceFiscaleCorretto(String rispostaLogin) {

	boolean result = false;
	try {
	    JSONArray loginJson = new JSONArray(rispostaLogin);
	    JSONObject datiCliente = (JSONObject) loginJson.get(1);
	    result = datiCliente.getBoolean("CF_corretto");
	}
	catch (JSONException e) {
	    e.printStackTrace();
	}

	return result;
    }

    /**
     * Work with input Constants.SERVIZIO_LOGIN
     * 
     * @param rispostaLogin
     * @return
     */
    public static String getTipoIndirizzo(String rispostaLogin) {
	String result = "";
	try {
	    JSONArray loginJson = new JSONArray(rispostaLogin);
	    JSONObject datiCliente = (JSONObject) loginJson.get(1);
	    result = datiCliente.getString("TipoIndirizzo");
	}
	catch (JSONException e) {
	    result = "";
	}
	return result;
    }

    /**
     * Work with input Constants.SERVIZIO_LOGIN
     * 
     * @param rispostaLogin
     * @return
     */
    public static String getStrada(String rispostaLogin) {
	String result = "";
	try {
	    JSONArray loginJson = new JSONArray(rispostaLogin);
	    JSONObject datiCliente = (JSONObject) loginJson.get(1);
	    result = datiCliente.getString("Strada");
	}
	catch (JSONException e) {
	    result = "";
	}
	return result;
    }

    /**
     * Work with input Constants.SERVIZIO_LOGIN
     * 
     * @param rispostaLogin
     * @return
     */
    public static String getNumCivico(String rispostaLogin) {
	String result = "";
	try {
	    JSONArray loginJson = new JSONArray(rispostaLogin);
	    JSONObject datiCliente = (JSONObject) loginJson.get(1);
	    result = datiCliente.getString("NumCivico");
	}
	catch (JSONException e) {
	    result = "";
	}
	return result;
    }

    /**
     * Work with input Constants.SERVIZIO_LOGIN
     * 
     * @param rispostaLogin
     * @return
     */
    public static String getEstensioneNumCivico(String rispostaLogin) {
	String result = "";
	try {
	    JSONArray loginJson = new JSONArray(rispostaLogin);
	    JSONObject datiCliente = (JSONObject) loginJson.get(1);
	    result = datiCliente.getString("EstensioneNumCivico");
	}
	catch (JSONException e) {
	    result = "";
	}
	return result;
    }

    /**
     * Work with input Constants.SERVIZIO_LOGIN
     * 
     * @param rispostaLogin
     * @return
     */
    public static String getCitta(String rispostaLogin) {
	String result = "";
	try {
	    JSONArray loginJson = new JSONArray(rispostaLogin);
	    JSONObject datiCliente = (JSONObject) loginJson.get(1);
	    result = datiCliente.getString("Citta");
	}
	catch (JSONException e) {
	    result = "";
	}
	return result;
    }

    /**
     * Work with input Constants.SERVIZIO_LOGIN
     * 
     * @param rispostaLogin
     * @return
     */
    public static String getCap(String rispostaLogin) {
	String result = "";
	try {
	    JSONArray loginJson = new JSONArray(rispostaLogin);
	    JSONObject datiCliente = (JSONObject) loginJson.get(1);
	    result = datiCliente.getString("Cap");
	}
	catch (JSONException e) {
	    result = "";
	}
	return result;
    }

    /**
     * Work with input Constants.SERVIZIO_LOGIN
     * 
     * @param rispostaLogin
     * @return
     */
    public static String getProvincia(String rispostaLogin) {
	String result = "";
	try {
	    JSONArray loginJson = new JSONArray(rispostaLogin);
	    JSONObject datiCliente = (JSONObject) loginJson.get(1);
	    result = datiCliente.getString("Provincia");
	}
	catch (JSONException e) {
	    result = "";
	}
	return result;
    }

    /**
     * Work with input Constants.SERVIZIO_LOGIN
     * 
     * @param rispostaLogin
     * @return
     */
    public static String getTelefonoPrimario(String rispostaLogin) {
	String result = "";
	try {
	    JSONArray loginJson = new JSONArray(rispostaLogin);
	    JSONObject datiCliente = (JSONObject) loginJson.get(1);
	    result = datiCliente.getString("PhoneNumber");
	}
	catch (JSONException e) {
	    result = "";
	}
	return result;
    }

    /**
     * Work with input Constants.SERVIZIO_LOGIN
     * 
     * @param rispostaLogin
     * @return
     */
    public static String getTipologiaUtenza(String rispostaLogin) {
	String result = "";
	try {
	    JSONArray loginJson = new JSONArray(rispostaLogin);
	    JSONObject datiCliente = (JSONObject) loginJson.get(1);
	    result = datiCliente.getString("TipologiaCliente");
	}
	catch (JSONException e) {
	    result = "";
	}
	return result;
    }

    // ======================================================================================
    // VISUALIZZA_FORNITURE_DOMICILIAZIONE RESPONSE PARSER HANDLER
    // ======================================================================================

    /**
     * 
     * Work with input Constants.SERVIZIO_VISUALIZZA_FORNITURE_DOMICILIAZIONE
     * 
     * @param rispostavisualizzaDomiciliazione
     * @param codiceConto
     * @return
     */
    public static JSONObject trovaOggettoDomiciliazione(JSONArray rispostavisualizzaDomiciliazione, String codiceConto) {
	JSONObject result = new JSONObject();
	try {
	    for (int i = 1; i < rispostavisualizzaDomiciliazione.length(); i++) {
		JSONObject oggettoIesimo = (JSONObject) rispostavisualizzaDomiciliazione.get(i);
		String codiceContoIesimo = oggettoIesimo.getString("CodiceContoCliente");
		if (codiceContoIesimo.equals(codiceConto)) {
		    // ho trovato i mio oggetto
		    result = oggettoIesimo;
		}
		else {
		    continue;
		}

	    }
	}
	catch (JSONException e) {
	    e.printStackTrace();
	}
	return result;

    }

    /**
     * Work with input Constants.SERVIZIO_VISUALIZZA_FORNITURE_DOMICILIAZIONE
     * 
     * @param arrayDomiciliazione
     * @param lunghezza
     * @return
     */
    public static String[] elencoCodiciConti(JSONArray arrayDomiciliazione, int lunghezza) {
	String result[] = new String[lunghezza];
	try {
	    for (int i = 1; i <= lunghezza; i++) {
		JSONObject domiciliazioneEntry = (JSONObject) arrayDomiciliazione.getJSONObject(i);
		String codiceConto = domiciliazioneEntry.getString("CodiceContoCliente");
		result[i - 1] = codiceConto;
	    }
	}
	catch (JSONException e) {
	    e.printStackTrace();
	}
	return result;
    }

    /**
     * Work with input Constants.SERVIZIO_VISUALIZZA_FORNITURE_DOMICILIAZIONE
     * 
     * @param arrayDomiciliazione
     * @param codiceConto
     * @return
     */
    public static String cercaStato(JSONArray arrayDomiciliazione, String codiceConto) {
	String result = "";
	try {
	    for (int i = 1; i < arrayDomiciliazione.length(); i++) {
		JSONObject domiciliazioneEntry = arrayDomiciliazione.getJSONObject(i);
		String codiceContoIesimo = domiciliazioneEntry.getString("CodiceContoCliente");
		if (!codiceContoIesimo.equals(codiceConto)) {
		    continue;
		}
		else {
		    result = domiciliazioneEntry.getString("StatoDomiciliazione");
		}
	    }
	}
	catch (JSONException e) {
	    e.printStackTrace();
	}
	return result;
    }

    // ======================================================================================
    // AUTOLETTURA RESPONSE PARSER HANDLER
    // ======================================================================================

    /**
     * Work with input Constants.SERVIZIO_ULTIMA_LETTURA
     * 
     * @param array
     * @param pdf
     * @return
     */
    public static String cercaCodiceConto(JSONArray array, String pdf) {
	String result = "";
	String pdfIesimo = "";
	JSONObject anagrContoIesimo = new JSONObject();
	JSONObject anagrExtraContoIesimo = new JSONObject();
	try {
	    for (int i = 2; i < array.length(); i++) {
		JSONArray contoIesimo;
		contoIesimo = array.getJSONArray(i);
		// codice conto
		if (contoIesimo.length() == 1) {
		    continue;
		}
		else {
		    anagrExtraContoIesimo = contoIesimo.getJSONObject(1);
		    pdfIesimo = anagrExtraContoIesimo.getString("Pdf");
		    if (!pdfIesimo.equals(pdf)) {
			// codice conto diverso
			continue;
		    }
		    else {
			anagrContoIesimo = contoIesimo.getJSONObject(0);
			if (!(anagrContoIesimo.getString("StatoConto")).equals("UTILIZZABILE")) {
			    continue;
			}
			else {
			    result = anagrContoIesimo.getString("CodiceConto");
			}
		    }
		}
	    }
	}
	catch (JSONException e) {
	    e.printStackTrace();
	}
	return result;
    }

    /**
     * Work with input Constants.SERVIZIO_ULTIMA_LETTURA
     * 
     * @param array
     * @param pdf
     * @return
     */
    public static String cercaSistemaBilling(JSONArray array, String pdf) {
	String result = "";
	String pdfIesimo = "";
	JSONObject anagrContoIesimo = new JSONObject();
	JSONObject anagrExtraContoIesimo = new JSONObject();
	try {
	    for (int i = 2; i < array.length(); i++) {
		JSONArray contoIesimo;
		contoIesimo = array.getJSONArray(i);
		// codice conto
		if (contoIesimo.length() == 1) {
		    continue;
		}
		else {
		    anagrExtraContoIesimo = contoIesimo.getJSONObject(1);
		    pdfIesimo = anagrExtraContoIesimo.getString("Pdf");
		    if (!pdfIesimo.equals(pdf)) {
			// codice conto diverso
			continue;
		    }
		    else {
			anagrContoIesimo = contoIesimo.getJSONObject(0);
			if (!(anagrContoIesimo.getString("StatoConto")).equals("UTILIZZABILE")) {
			    continue;
			}
			else {
			    result = anagrContoIesimo.getString("SistemaBilling");
			}
		    }
		}

	    }
	}
	catch (JSONException e) {
	    e.printStackTrace();
	}
	return result;
    }

    /**
     * Work with input Constants.SERVIZIO_ULTIMA_LETTURA
     * 
     * @param array
     * @param pdf
     * @return
     */
    public static String cercaSocietaFornitrice(JSONArray array, String pdf) {
	String result = "";
	String pdfIesimo = "";
	JSONObject anagrContoIesimo = new JSONObject();
	JSONObject anagrExtraContoIesimo = new JSONObject();
	try {
	    for (int i = 2; i < array.length(); i++) {
		JSONArray contoIesimo;
		contoIesimo = array.getJSONArray(i);
		// codice conto
		if (contoIesimo.length() == 1) {
		    continue;
		}
		else {
		    anagrExtraContoIesimo = contoIesimo.getJSONObject(1);
		    pdfIesimo = anagrExtraContoIesimo.getString("Pdf");
		    if (!pdfIesimo.equals(pdf)) {
			// codice conto diverso
			continue;
		    }
		    else {
			anagrContoIesimo = contoIesimo.getJSONObject(0);
			if (!(anagrContoIesimo.getString("StatoConto")).equals("UTILIZZABILE")) {
			    continue;
			}
			else {
			    result = anagrContoIesimo.getString("SocietaFornitrice");
			}
		    }
		}
	    }
	}
	catch (JSONException e) {
	    e.printStackTrace();
	}
	return result;
    }
}
