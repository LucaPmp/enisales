package com.eni.enigaseluce.utils;

public class InfoUtenzaEni {

    private String nomeClienteCC;
    private String cognomeClienteCC;
    private String codiceFiscaleClienteCC;
    private String ibanClienteCC;
    private String codiceCliente;
    private String codiceConto;
    private String partitaIvaClienteCC;
    private String nomeSottoscrittoreCC;
    private String cognomeSottoscrittoreCC;
    private String codiceFiscaleSottoscrittoreCC;
    private String userId;
    private String nomeCliente;
    private String cognomeCliente;
    private String codiceFiscaleCliente;
    private String partitaIvaCliente;
    private String tipoIndirizzo;
    private String strada;
    private String numCivico;
    private String estensioneNumCivico;
    private String citta;
    private String cap;
    private String provincia;
    private String cf_Modificato;
    private String piva_Modificata;
    private String ragioneSocialeClienteCC;
    private String telefonoPrimario;
    private String tipologia;

    public InfoUtenzaEni() {

    }

    public final String getNomeClienteCC() {
	return nomeClienteCC;
    }

    public final void setNomeClienteCC(String nomeClienteCC) {
	this.nomeClienteCC = nomeClienteCC;
    }

    public final String getCognomeClienteCC() {
	return cognomeClienteCC;
    }

    public final void setCognomeClienteCC(String cognomeClienteCC) {
	this.cognomeClienteCC = cognomeClienteCC;
    }

    public final String getCodiceFiscaleClienteCC() {
	return codiceFiscaleClienteCC;
    }

    public final void setCodiceFiscaleClienteCC(String codiceFiscaleClienteCC) {
	this.codiceFiscaleClienteCC = codiceFiscaleClienteCC;
    }

    public final String getIbanClienteCC() {
	return ibanClienteCC;
    }

    public final void setIbanClienteCC(String ibanClienteCC) {
	this.ibanClienteCC = ibanClienteCC;
    }

    public final String getCodiceCliente() {
	return codiceCliente;
    }

    public final void setCodiceCliente(String codiceCliente) {
	this.codiceCliente = codiceCliente;
    }

    public String getCodiceConto() {
	return codiceConto;
    }

    public void setCodiceConto(String codiceConto) {
	this.codiceConto = codiceConto;
    }

    public final String getPartitaIvaClienteCC() {
	return partitaIvaClienteCC;
    }

    public final void setPartitaIvaClienteCC(String partitaIvaClienteCC) {
	this.partitaIvaClienteCC = partitaIvaClienteCC;
    }

    public final String getNomeSottoscrittoreCC() {
	return nomeSottoscrittoreCC;
    }

    public final void setNomeSottoscrittoreCC(String nomeSottoscrittoreCC) {
	this.nomeSottoscrittoreCC = nomeSottoscrittoreCC;
    }

    public final String getCognomeSottoscrittoreCC() {
	return cognomeSottoscrittoreCC;
    }

    public final void setCognomeSottoscrittoreCC(String cognomeSottoscrittoreCC) {
	this.cognomeSottoscrittoreCC = cognomeSottoscrittoreCC;
    }

    public final String getCodiceFiscaleSottoscrittoreCC() {
	return codiceFiscaleSottoscrittoreCC;
    }

    public final void setCodiceFiscaleSottoscrittoreCC(String codiceFiscaleSottoscrittoreCC) {
	this.codiceFiscaleSottoscrittoreCC = codiceFiscaleSottoscrittoreCC;
    }

    public final String getUserId() {
	return userId;
    }

    public final void setUserId(String userId) {
	this.userId = userId;
    }

    public final String getNomeCliente() {
	return nomeCliente;
    }

    public final void setNomeCliente(String nomeCliente) {
	this.nomeCliente = nomeCliente;
    }

    public final String getCognomeCliente() {
	return cognomeCliente;
    }

    public final void setCognomeCliente(String cognomeCliente) {
	this.cognomeCliente = cognomeCliente;
    }

    public final String getCodiceFiscaleCliente() {
	return codiceFiscaleCliente;
    }

    public final void setCodiceFiscaleCliente(String codiceFiscaleCliente) {
	this.codiceFiscaleCliente = codiceFiscaleCliente;
    }

    public String getPartitaIvaCliente() {
	return partitaIvaCliente;
    }

    public void setPartitaIvaCliente(String partitaIvaCliente) {
	this.partitaIvaCliente = partitaIvaCliente;
    }

    public final String getTipoIndirizzo() {
	return tipoIndirizzo;
    }

    public final void setTipoIndirizzo(String tipoIndirizzo) {
	this.tipoIndirizzo = tipoIndirizzo;
    }

    public final String getStrada() {
	return strada;
    }

    public final void setStrada(String strada) {
	this.strada = strada;
    }

    public final String getNumCivico() {
	return numCivico;
    }

    public final void setNumCivico(String numCivico) {
	this.numCivico = numCivico;
    }

    public final String getEstensioneNumCivico() {
	return estensioneNumCivico;
    }

    public final void setEstensioneNumCivico(String estensioneNumCivico) {
	this.estensioneNumCivico = estensioneNumCivico;
    }

    public final String getCitta() {
	return citta;
    }

    public final void setCitta(String citta) {
	this.citta = citta;
    }

    public final String getCap() {
	return cap;
    }

    public final void setCap(String cap) {
	this.cap = cap;
    }

    public final String getProvincia() {
	return provincia;
    }

    public final void setProvincia(String provincia) {
	this.provincia = provincia;
    }

    public final String getCf_Modificato() {
	return cf_Modificato;
    }

    public final void setCf_Modificato(String cf_Modificato) {
	this.cf_Modificato = cf_Modificato;
    }

    public final String getPiva_Modificata() {
	return piva_Modificata;
    }

    public final void setPiva_Modificata(String piva_Modificata) {
	this.piva_Modificata = piva_Modificata;
    }

    public String getRagioneSocialeClienteCC() {
	return ragioneSocialeClienteCC;
    }

    public void setRagioneSocialeClienteCC(String ragioneSocialeClienteCC) {
	this.ragioneSocialeClienteCC = ragioneSocialeClienteCC;
    }

    public String getTelefonoPrimario() {
	return telefonoPrimario;
    }

    public void setTelefonoPrimario(String telefonoPrimario) {
	this.telefonoPrimario = telefonoPrimario;
    }

    public String getTipologia() {
	return tipologia;
    }

    public void setTipologia(String tipologia) {
	this.tipologia = tipologia;
    }
}
