package com.eni.enigaseluce.utils;

import java.util.HashMap;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.eni.enigaseluce.R;
import com.eni.enigaseluce.httpcall.ServiceCall;
import com.eni.enigaseluce.httpcall.ServiceCallFactory;
import com.eni.enigaseluce.httpcall.ServiceCallHttp;

@SuppressLint("NewApi")
public class CustomAlertDialog {

	public static void showAlertDialogNoButton(Context context, String message, String spannableMessage, int layoutId)
	{
		final Dialog dialog = prepareBaseDialog(context, message, spannableMessage, layoutId);
		
		final LinearLayout chiudi = (LinearLayout) dialog.findViewById(R.id.chiudi);
		chiudi.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v)
			{
				dialog.dismiss();
			}
		});

		dialog.show();
	}

	public static void showAlertDialogWithButton(final Context context, String message, String spannableMessage, int layoutId, final ServiceCallHttp srvCall,
			final String url, final HashMap<String, String> parametri, final Activity activity, int code, Activity oopsPage, final Intent intentOops,
			final AnimationDrawable animation, final ImageView imageAnim)
	{

		final Dialog dialog = prepareBaseDialog(context, message, spannableMessage, layoutId);

		final TextView btn_prosegui = (TextView) dialog.findViewById(R.id.btn_prosegui);
		btn_prosegui.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v)
			{

				try
				{
					dialog.dismiss();

					ServiceCall srvCall = ServiceCallFactory.createServiceCall(context);
					srvCall.executeHttpsGetDomiciliazione(url, null, parametri, activity, 1, null);
				} catch (Exception e)
				{
					e.printStackTrace();

					context.startActivity(intentOops);
					activity.overridePendingTransition(R.anim.fade, R.anim.hold);
				}
			}
		});

		final LinearLayout chiudi = (LinearLayout) dialog.findViewById(R.id.chiudi);
		chiudi.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v)
			{

				dialog.dismiss();
				if (animation.isRunning() && animation.isVisible())
				{
					imageAnim.setVisibility(View.INVISIBLE);
					animation.stop();
				}
			}
		});

		dialog.show();
	}
	
	private static Dialog prepareBaseDialog(Context context, String message, String spannableMessage, int layoutId)
	{
		final Dialog dialog = new Dialog(context,android.R.style.Theme_Translucent_NoTitleBar);
		dialog.setContentView(layoutId);
		dialog.setCancelable(false);

		// set empty string to avoid NullPointer
		if( message == null )
			message = "";
		
		SpannableString spannable = new SpannableString(message + " " + spannableMessage);
		spannable.setSpan(new ForegroundColorSpan(Color.rgb(250, 211, 59)), message.length()+1, spannable.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
		spannable.setSpan(new ForegroundColorSpan(Color.WHITE), 0, message.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

		TextView tv_message = (TextView) dialog.findViewById(R.id.domiciliazione_alert_message);
		tv_message.setText(spannable);
		
		return dialog;
	}
}
