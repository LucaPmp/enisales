package com.eni.enigaseluce.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;

import com.eni.enigaseluce.R;
import com.eni.enigaseluce.error.OopsPageMaintenance;
import com.eni.enigaseluce.httpcall.Constants;
import com.eni.enigaseluce.httpcall.ServiceCall;
import com.eni.enigaseluce.httpcall.ServiceCallFactory;

@SuppressLint("NewApi")
public class Utilities {

    /**
     * Converte i byte dell'InputStream in una singola String
     * 
     * @param is
     *            InputStrem
     * @return i byte dell'InputStream in formato String
     * @throws IOException
     */
    public static String convertStreamToString(InputStream is) throws IOException {
	if (is != null) {
	    Writer writer = new StringWriter();
	    char[] buffer = new char[1024];

	    try {
		Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
		int n;
		while ((n = reader.read(buffer)) != -1) {
		    writer.write(buffer, 0, n);
		}
	    }
	    finally {
		is.close();
	    }

	    return writer.toString();

	}
	else {
	    return "";
	}

    }

    /**
     * Crea la sequenza della url che serve per il passaggio dei parametri (in
     * formato: &<nome_parametro>=<valore_parametro>&...)
     * 
     * @param hashMap
     *            con i parametri da passare alla url (chiave: nome_parametro,
     *            valore: valore del parametro)
     * @return la sequenza che contiene i parametri (formato:
     *         ?<nome_parametro>=<valore_parametro>) da concatenare alla url
     */
    public static String hashMapParameterToString(HashMap<String, String> hashMap) {
	String result = "";
	Set<Entry<String, String>> entrySet = hashMap.entrySet();

	for (Iterator<Entry<String, String>> it = entrySet.iterator(); it.hasNext();) {
	    Entry<String, String> row = it.next();
	    result = result + "&" + row.getKey();
	    String value = (String) row.getValue();
	    result = result + "=" + value.replace(" ", "%20");
	}
	return result;
    }

    /**
     * Check network availability
     * 
     * @param context
     * @return
     */
    public static boolean isNetworkAvailable(Context context) {
	ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
	if (connectivityManager == null) return false;
	NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
	return activeNetworkInfo != null;
    }

    public static HashMap<String, String> fillParameters(InfoUtenzaEni utenzaEni, boolean checkIntestatari, int tipologiaUtenza) {

	HashMap<String, String> parametriDomForn = new HashMap<String, String>();

	parametriDomForn.put(Constants.CODICE_CLIENTE, utenzaEni.getCodiceCliente());
	parametriDomForn.put(Constants.CODICE_CONTO, utenzaEni.getCodiceConto());
	parametriDomForn.put(Constants.IBAN_CLIENTE_CC, utenzaEni.getIbanClienteCC());

	switch (tipologiaUtenza) {
	case Constants.TIPOLOGIA_PERSONA_FISICA:

	    parametriDomForn.put(Constants.NOME_CLIENTE_CC, utenzaEni.getNomeClienteCC());
	    parametriDomForn.put(Constants.COGNOME_CLIENTE_CC, utenzaEni.getCognomeClienteCC());
	    parametriDomForn.put(Constants.CODICE_FISCALE_CLIENTE_CC, utenzaEni.getCodiceFiscaleClienteCC());

	    if (checkIntestatari) {
		utenzaEni.setCf_Modificato("true");
	    }
	    else {
		utenzaEni.setCf_Modificato("false");
	    }
	    parametriDomForn.put(Constants.CF_MODIFICATO, utenzaEni.getCf_Modificato());

	    break;

	case Constants.TIPOLOGIA_PERSONA_GIURIDICA:

	    parametriDomForn.put(Constants.COGNOME_CLIENTE_CC, utenzaEni.getRagioneSocialeClienteCC());

	    parametriDomForn.put(Constants.PARTITA_IVA_CLIENTE_CC, utenzaEni.getPartitaIvaClienteCC());

	    parametriDomForn.put(Constants.NOME_SOTTOSCRITTORE_CC, utenzaEni.getNomeSottoscrittoreCC());
	    parametriDomForn.put(Constants.COGNOME_SOTTOSCRITTORE_CC, utenzaEni.getCognomeSottoscrittoreCC());
	    parametriDomForn.put(Constants.CODICE_FISCALE_SOTTOSCRITTORE_CC, utenzaEni.getCodiceFiscaleSottoscrittoreCC());

	    parametriDomForn.put(Constants.CF_MODIFICATO, "false");
	    parametriDomForn.put(Constants.PIVA_MODIFICATA, "false");

	    break;

	case Constants.TIPOLOGIA_NON_RESIDENZIALE:

	    parametriDomForn.put(Constants.COGNOME_CLIENTE_CC, utenzaEni.getRagioneSocialeClienteCC());

	    parametriDomForn.put(Constants.PARTITA_IVA_CLIENTE_CC, utenzaEni.getPartitaIvaClienteCC());

	    parametriDomForn.put(Constants.NOME_SOTTOSCRITTORE_CC, utenzaEni.getNomeSottoscrittoreCC());
	    parametriDomForn.put(Constants.COGNOME_SOTTOSCRITTORE_CC, utenzaEni.getCognomeSottoscrittoreCC());
	    parametriDomForn.put(Constants.CODICE_FISCALE_SOTTOSCRITTORE_CC, utenzaEni.getCodiceFiscaleSottoscrittoreCC());

	    if (checkIntestatari) {
		utenzaEni.setPiva_Modificata("true");
	    }
	    else {
		utenzaEni.setPiva_Modificata("false");
	    }
	    parametriDomForn.put(Constants.PIVA_MODIFICATA, utenzaEni.getPiva_Modificata());

	    break;
	}

	parametriDomForn.put(Constants.USER_ID, utenzaEni.getUserId());
	parametriDomForn.put(Constants.NOME_CLIENTE, utenzaEni.getNomeCliente());
	parametriDomForn.put(Constants.COGNOME_CLIENTE, utenzaEni.getCognomeCliente());
	parametriDomForn.put(Constants.CODICE_FISCALE_CLIENTE, utenzaEni.getCodiceFiscaleCliente());

	if (!utenzaEni.getTipoIndirizzo().equals("")) {
	    parametriDomForn.put(Constants.TIPO_INDIRIZZO, utenzaEni.getTipoIndirizzo());
	}
	if (!utenzaEni.getStrada().equals("")) {
	    parametriDomForn.put(Constants.STRADA, utenzaEni.getStrada());
	}
	if (!utenzaEni.getNumCivico().equals("")) {
	    parametriDomForn.put(Constants.NUM_CIVICO, utenzaEni.getNumCivico());
	}
	if (!utenzaEni.getEstensioneNumCivico().equals("")) {
	    parametriDomForn.put(Constants.ESTENSIONE_NUM_CIVICO, utenzaEni.getEstensioneNumCivico());
	}
	if (!utenzaEni.getCitta().equals("")) {
	    parametriDomForn.put(Constants.CITTA, utenzaEni.getCitta());
	}
	if (!utenzaEni.getCap().equals("")) {
	    parametriDomForn.put(Constants.CAP, utenzaEni.getCap());
	}
	if (!utenzaEni.getProvincia().equals("")) {
	    parametriDomForn.put(Constants.PROVINCIA, utenzaEni.getProvincia());
	}

	return parametriDomForn;
    }

    /**
     * Call to maintenance mode
     * 
     * @param activity
     * @param toGo
     * @param oopsPage
     */
    public static void maintenance(Activity activity, Class<?> toGo, Handler handler) {

	if (!activity.isFinishing()) {
	    if (!isNetworkAvailable(activity)) {
		Intent intentOops = new Intent(activity.getApplicationContext(), OopsPageMaintenance.class);
		intentOops.putExtra("MESSAGGIO", activity.getResources().getString(R.string.connessione_assente));
		activity.finish();
		activity.startActivity(intentOops);
		activity.overridePendingTransition(R.anim.fade, R.anim.hold);
	    }
	    else {
		// ******maintainance mode****//
		try {

//			ServiceCall maintainnanceMode = ServiceCallFactory.createServiceCall(activity.getBaseContext());
//			maintainnanceMode.executeHttpsGetMaintenance(Constants.URL_MAINTAINANCE_MODE, activity);
		    new RequestTask(activity, toGo, handler).execute(Constants.URL_MAINTAINANCE_MODE);
		}
		catch (Exception e) {
		    e.printStackTrace();
		    Intent intentOops = new Intent(activity.getApplicationContext(), OopsPageMaintenance.class);
		    intentOops.putExtra("MESSAGGIO", activity.getResources().getString(R.string.maintenance_message));
		    activity.finish();
		    activity.startActivity(intentOops);
		    activity.overridePendingTransition(R.anim.fade, R.anim.hold);
		    return;
		}
	    }
	}
    }

    /**
     * Shows a waiting animation
     * 
     * @param context
     */
    public static AnimationDrawable animation(Activity context) {

	AnimationDrawable animation = new AnimationDrawable();
	animation.addFrame(context.getResources().getDrawable(R.drawable.anim_load2), 300);
	animation.addFrame(context.getResources().getDrawable(R.drawable.anim_load3), 300);
	animation.addFrame(context.getResources().getDrawable(R.drawable.anim_load4), 300);
	animation.setOneShot(false);

	// ImageView imageAnim = (ImageView)
	// context.findViewById(R.id.waiting_anim);
	// imageAnim.setVisibility(View.VISIBLE);
	// if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
	// imageAnim.setBackgroundDrawable(animation);
	// }
	// else {
	// imageAnim.setBackground(animation);
	// }

	return animation;
    }

    /**
     * Check if an array contais the provided string
     * 
     * @param array
     * @param element
     * @return
     */
    public static boolean arrayContainsString(String[] array, String element) {
	boolean result = false;

	if (array == null || element == null) return result;

	for (int i = 0; i < array.length; i++) {
	    if (element.equalsIgnoreCase(array[i])) {
		result = true;
		break;
	    }
	}

	return result;
    }

    public static boolean isNotEmptyString(String strada) {
	return (strada != null && !strada.equalsIgnoreCase(""));
    }
}
