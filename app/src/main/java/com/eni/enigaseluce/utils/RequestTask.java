package com.eni.enigaseluce.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;

import com.eni.enigaseluce.R;
import com.eni.enigaseluce.error.OopsPageMaintenance;
import com.eni.enigaseluce.httpcall.Constants;

@SuppressLint("NewApi")
public class RequestTask extends AsyncTask<String, String, String> {

    private Activity activity;
    private Class<?> toGo;
    private Handler handler;

    // private static boolean firstAccess = false;

    public RequestTask(Activity activity, Class<?> toGo, Handler handler) {

	this.activity = activity;
	this.toGo = toGo;
	this.handler = handler;
    }

    @Override
    protected String doInBackground(String... uri) {
	HttpClient httpclient = new DefaultHttpClient();
	HttpResponse response;
	String responseString = "";
	try {
	    // if (!Constants.MAINTENANCE_ACTIVE) {


		HttpGet httpGet = new HttpGet("");


	    response = httpclient.execute(new HttpGet(uri[0]));
	    StatusLine statusLine = response.getStatusLine();
	    if (statusLine.getStatusCode() == HttpStatus.SC_OK) {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		response.getEntity().writeTo(out);
		out.close();
		responseString = out.toString();
	    }
	    else {
		// Closes the connection.
		response.getEntity().getContent().close();
		throw new IOException(statusLine.getReasonPhrase());
	    }
	    // }
	    // else {
	    //
	    // responseString="<maintenance>\n\n\n"
	    // + "<flag>"+firstAccess+"</flag>\n"
	    // + "<portal>resid</portal>"
	    // + "<message>"
	    // + "<![CDATA[<iw_tp>"
	    // + "<html>"
	    // + "<head>"
	    // + "<style>html,div,p ,ul,li{text-align:left !important;}</style>"
	    // + "</head>"
	    // +
	    // "<body>I servizi a tua disposizione in futuro saranno ancora piu' ricchi. E' per questo, che oggi, i nostri sistemi sono in aggiornamento e saranno nuovamente disponibili a breve</body>"
	    // + "</html>"
	    // + "</iw_tp>]]>"
	    // + "</message>"
	    // +
	    // "<text>I servizi a tua disposizione in futuro saranno ancora piu' ricchi. E' per questo, che oggi, i nostri sistemi sono in aggiornamento e saranno nuovamente disponibili a breve</text>"
	    // + "</maintenance>";
	    //
	    // if(!firstAccess) {
	    // firstAccess = true;
	    // }
	    // }
	}
	catch (ClientProtocolException e) {
	    e.printStackTrace();
	}
	catch (IOException e) {
	    e.printStackTrace();
	}
	return responseString;
    }

    @Override
    protected void onPostExecute(String result) {
	super.onPostExecute(result);

	Log.d("REQUESTTASK","Response Maintencance:\n" + result);

	// Do anything with response..
	if (!result.equals("")) {
	    if (result.indexOf("<flag>") != -1) {
		int indexStart = result.indexOf("<flag>");
		int indexEnd = result.indexOf("</flag>");
		String flag = result.substring(indexStart + 6, indexEnd);
		if (flag.equals("false")) {

		    if (this.toGo != null && handler == null) {

			// il portale non è in maintenace, quindi deve essere
			// avviato l'activity corrispondente a "toGo"

			Log.d("MAINTENCANCE_MODE", "Il portale non è in maintenance");

			Intent intent = new Intent(activity.getApplicationContext(), this.toGo);
			activity.finish();
			activity.startActivity(intent);
			activity.overridePendingTransition(R.anim.fade, R.anim.hold);
		    }
		    else {

			// il portale non è in maintenace, quindi deve essere
			// avviato il servizio dell'activity corrispondente

			Log.d("MAINTENCANCE_MODE", "Il portale non è in maintenance");

			handler.sendEmptyMessage(Constants.MAINTENANCE_DISATTIVA);

			return;
		    }
		}
		else {
		    int indexMsgStart = result.indexOf("<text>");
		    int indexMsgEnd = result.indexOf("</text>");
		    String msgCortesia = result.substring(indexMsgStart + 6, indexMsgEnd);

		    Log.d("MAINTENCANCE_MODE", "Il portale è in maintenance");

		    Intent intentOops = new Intent(activity.getApplicationContext(), OopsPageMaintenance.class);
		    intentOops.putExtra("MESSAGGIO", msgCortesia);
		    activity.finish();
		    activity.startActivity(intentOops);
		    activity.overridePendingTransition(R.anim.fade, R.anim.hold);
		}
	    }
	    else {

		Log.d("MAINTENCANCE_MODE", "Il portale è in maintenance");

		Intent intentOops = new Intent(activity.getApplicationContext(), OopsPageMaintenance.class);
		intentOops.putExtra("MESSAGGIO", activity.getResources().getString(R.string.maintenance_message));
		activity.finish();
		activity.startActivity(intentOops);
		activity.overridePendingTransition(R.anim.fade, R.anim.hold);
	    }
	}
	else {

	    Log.d("MAINTENCANCE_MODE", "Il portale è in maintenance");

	    Intent intentOops = new Intent(activity.getApplicationContext(), OopsPageMaintenance.class);
	    intentOops.putExtra("MESSAGGIO", activity.getResources().getString(R.string.errore_generico_opspage));
	    activity.finish();
	    activity.startActivity(intentOops);
	    activity.overridePendingTransition(R.anim.fade, R.anim.hold);
	}
    }
}
