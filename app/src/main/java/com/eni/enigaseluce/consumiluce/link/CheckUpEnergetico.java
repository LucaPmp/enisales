package com.eni.enigaseluce.consumiluce.link;

import java.util.Timer;
import java.util.TimerTask;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.AnimationDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import com.eni.enigaseluce.R;
import com.eni.enigaseluce.consumigas.ConsumiGas;
import com.eni.enigaseluce.consumiluce.ConsumiLuce;
import com.eni.enigaseluce.error.OopsPage;
import com.eni.enigaseluce.httpcall.Constants;
import com.eni.enigaseluce.utils.Utilities;

@SuppressLint("NewApi")
public class CheckUpEnergetico extends Activity {
    private WebView mWebView;
    AnimationDrawable animation;
    ImageView imageAnim;
    TimerTask stopAnimationTask;
    final Handler handler = new Handler();

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.eni_famiglia);

	mWebView = (WebView) findViewById(R.id.eni_famiglia_webview);

	mWebView.setInitialScale(1);
	mWebView.getSettings().setJavaScriptEnabled(true);
	mWebView.getSettings().setLoadWithOverviewMode(true);
	mWebView.getSettings().setUseWideViewPort(true);
	mWebView.getSettings().setBuiltInZoomControls(true);

	mWebView.setWebChromeClient(new WebChromeClient() {
	    public void onProgressChanged(WebView view, int progress) {

		animation = Utilities.animation(CheckUpEnergetico.this);
		imageAnim = (ImageView) findViewById(R.id.waiting_anim);
		imageAnim.setVisibility(View.VISIBLE);
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
		    imageAnim.setBackgroundDrawable(animation);
		}
		else {
		    imageAnim.setBackground(animation);
		}
		animation.start();
	    }
	});
	mWebView.setWebViewClient(new WebViewClient() {
	    public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
		if (animation != null) {
		    animation.stop();
		    imageAnim.setVisibility(View.INVISIBLE);
		}
		Intent intentOops = new Intent(getApplicationContext(), OopsPage.class);
		intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
		// EniFamiglia.this.finish();
		startActivityForResult(intentOops, OopsPage.ESCI);
		overridePendingTransition(R.anim.fade, R.anim.hold);
	    }

	    public void onPageFinished(WebView view, String url) {
		stopAnimationTask = new TimerTask() {
		    public void run() {
			handler.post(new Runnable() {
			    public void run() {
				stopAnimation();
			    }
			});
		    }
		};

		new Timer().schedule(stopAnimationTask, 100);
	    }

	});
	final ImageView indietro = (ImageView) findViewById(R.id.eni_famiglia_indietro);
	indietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
	indietro.getBackground().invalidateSelf();
	indietro.setOnClickListener(new View.OnClickListener() {
	    public void onClick(View v1) {
		indietro.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
		indietro.getBackground().invalidateSelf();

		animation = Utilities.animation(CheckUpEnergetico.this);
		imageAnim = (ImageView) findViewById(R.id.waiting_anim);
		imageAnim.setVisibility(View.VISIBLE);
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
		    imageAnim.setBackgroundDrawable(animation);
		}
		else {
		    imageAnim.setBackground(animation);
		}
		animation.start();

		String paginaDiProvenienza = getIntent().getStringExtra("PAGINA_DI_PROVENIENZA");

		Intent intentConsumiLuce;
		mWebView.stopLoading();
		if (paginaDiProvenienza != null && paginaDiProvenienza.equals(ConsumiLuce.class.getCanonicalName())) {
		    intentConsumiLuce = new Intent(getApplicationContext(), ConsumiLuce.class);
		    intentConsumiLuce.putExtra("Classificazione", getIntent().getStringExtra("Classificazione"));
		    intentConsumiLuce.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
		    intentConsumiLuce.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
		    intentConsumiLuce.putExtra("PERIODO", getIntent().getIntExtra("PERIODO", Constants.RETAIL_PERIODO_MESI_6));
		    intentConsumiLuce.putExtra("RIFERIMENTO", getIntent().getStringExtra("RIFERIMENTO"));
		    intentConsumiLuce.putExtra("CATEGORY", getIntent().getStringExtra("CATEGORY"));
		    intentConsumiLuce.putExtra("autoletturaLuce", false);
		    CheckUpEnergetico.this.finish();
		    startActivity(intentConsumiLuce);
		    overridePendingTransition(R.anim.fade, R.anim.hold);
		}
		else if (paginaDiProvenienza != null && paginaDiProvenienza.equals(ConsumiGas.class.getCanonicalName())) {
		    intentConsumiLuce = new Intent(getApplicationContext(), ConsumiGas.class);
		    intentConsumiLuce.putExtra("Classificazione", getIntent().getStringExtra("Classificazione"));
		    intentConsumiLuce.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
		    intentConsumiLuce.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
		    intentConsumiLuce.putExtra("PERIODO", getIntent().getIntExtra("PERIODO", Constants.RETAIL_PERIODO_MESI_6));
		    intentConsumiLuce.putExtra("RIFERIMENTO", getIntent().getStringExtra("RIFERIMENTO"));
		    intentConsumiLuce.putExtra("CATEGORY", getIntent().getStringExtra("CATEGORY"));
		    CheckUpEnergetico.this.finish();
		    startActivity(intentConsumiLuce);
		    overridePendingTransition(R.anim.fade, R.anim.hold);
		}
	    }
	});

	mWebView.loadUrl("http://www.famiglia.eni.it/it_IT/famiglia/servizi-online/checkup-energetico/checkup-energetico-home.page");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	if (resultCode == OopsPage.ESCI) {
	    Intent intentLoginHelp = new Intent(getApplicationContext(), ConsumiLuce.class);
	    CheckUpEnergetico.this.finish();
	    startActivity(intentLoginHelp);
	    overridePendingTransition(R.anim.fade, R.anim.hold);
	}
    }

    private void stopAnimation() {
	if (animation != null) {
	    animation.stop();
	    imageAnim.setVisibility(View.INVISIBLE);
	}
    }

    public void onBackPressed() {

	animation = Utilities.animation(this);
	imageAnim = (ImageView) findViewById(R.id.waiting_anim);
	imageAnim.setVisibility(View.VISIBLE);
	if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
	    imageAnim.setBackgroundDrawable(animation);
	}
	else {
	    imageAnim.setBackground(animation);
	}
	animation.start();

	String paginaDiProvenienza = getIntent().getStringExtra("PAGINA_DI_PROVENIENZA");
	Intent intentConsumiLuce;
	mWebView.stopLoading();
	if (paginaDiProvenienza != null && paginaDiProvenienza.equals(ConsumiLuce.class.getCanonicalName())) {
	    intentConsumiLuce = new Intent(getApplicationContext(), ConsumiLuce.class);
	    intentConsumiLuce.putExtra("Classificazione", getIntent().getStringExtra("Classificazione"));
	    intentConsumiLuce.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
	    intentConsumiLuce.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
	    intentConsumiLuce.putExtra("PERIODO", getIntent().getIntExtra("PERIODO", Constants.RETAIL_PERIODO_MESI_6));
	    intentConsumiLuce.putExtra("RIFERIMENTO", getIntent().getStringExtra("RIFERIMENTO"));
	    intentConsumiLuce.putExtra("CATEGORY", getIntent().getStringExtra("CATEGORY"));
	    intentConsumiLuce.putExtra("autoletturaLuce", false);
	    CheckUpEnergetico.this.finish();
	    startActivity(intentConsumiLuce);
	    overridePendingTransition(R.anim.fade, R.anim.hold);
	}
	else if (paginaDiProvenienza != null && paginaDiProvenienza.equals(ConsumiGas.class.getCanonicalName())) {
	    intentConsumiLuce = new Intent(getApplicationContext(), ConsumiGas.class);
	    intentConsumiLuce.putExtra("Classificazione", getIntent().getStringExtra("Classificazione"));
	    intentConsumiLuce.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
	    intentConsumiLuce.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
	    intentConsumiLuce.putExtra("PERIODO", getIntent().getIntExtra("PERIODO", Constants.RETAIL_PERIODO_MESI_6));
	    intentConsumiLuce.putExtra("RIFERIMENTO", getIntent().getStringExtra("RIFERIMENTO"));
	    intentConsumiLuce.putExtra("CATEGORY", getIntent().getStringExtra("CATEGORY"));
	    CheckUpEnergetico.this.finish();
	    startActivity(intentConsumiLuce);
	    overridePendingTransition(R.anim.fade, R.anim.hold);
	}
    }
}
