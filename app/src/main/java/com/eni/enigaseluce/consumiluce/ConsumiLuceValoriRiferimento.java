package com.eni.enigaseluce.consumiluce;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.AnimationDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.eni.enigaseluce.R;
import com.eni.enigaseluce.font.EniFont;
import com.eni.enigaseluce.httpcall.Constants;
import com.eni.enigaseluce.utils.Utilities;

@SuppressLint("NewApi")
public class ConsumiLuceValoriRiferimento extends Activity {

    private Context context;
    private static Context staticContext;
    AnimationDrawable animation;
    ImageView imageAnim;

    static ScrollView pager;
    static LayoutInflater inflater;
    static LayoutInflater inflaterBis;

    String primoConto;
    String classificazione;
    String riferimento;
    int periodo;
    boolean valoriAZero;

    String[] arrayMesi = {"Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre"};

    public String rispostaLogin = Constants.SERVIZIO_LOGIN;
    public String rispostaConsumiLuce = Constants.SERVIZIO_CONSUMI_LUCE;

    public static String nomeInteroCliente = "";

    public static Context getContext() {
        return ConsumiLuceValoriRiferimento.staticContext;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.consumi_luce_valori_riferimento);
        valoriAZero = false;

        context = this;
        ConsumiLuceValoriRiferimento.staticContext = this;
        primoConto = getIntent().getStringExtra("PRIMOCONTO");
        classificazione = getIntent().getStringExtra("Classificazione");
        periodo = getIntent().getIntExtra("PERIODO", 6);
    }

    @Override
    protected void onStart() {
        super.onStart();
        primoConto = getIntent().getStringExtra("PRIMOCONTO");

        final ImageView indietro = (ImageView) findViewById(R.id.consumi_luce_indietro);
        indietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
        indietro.getBackground().invalidateSelf();
        indietro.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                indietro.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
                indietro.getBackground().invalidateSelf();

                animation = Utilities.animation(ConsumiLuceValoriRiferimento.this);
                imageAnim = (ImageView) findViewById(R.id.waiting_anim);
                imageAnim.setVisibility(View.VISIBLE);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    imageAnim.setBackgroundDrawable(animation);
                } else {
                    imageAnim.setBackground(animation);
                }
                animation.start();

                Intent intentHome = new Intent(getApplicationContext(), ConsumiLuce.class);
                intentHome.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
                intentHome.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
                intentHome.putExtra("Classificazione", getIntent().getStringExtra("Classificazione"));
                intentHome.putExtra("CATEGORY", getIntent().getStringExtra("CATEGORY"));
                intentHome.putExtra("PERIODO", getIntent().getIntExtra("PERIODO", Constants.RETAIL_PERIODO_MESI_6));
                intentHome.putExtra("RIFERIMENTO", getIntent().getStringExtra("RIFERIMENTO"));
                intentHome.putExtra("coloreGrafico", getIntent().getIntExtra("coloreGrafico", 0));
                intentHome.putExtra("autoletturaLuce", false);

                TimerTask clearingHeapTask;
                final Handler handler = new Handler();
                clearingHeapTask = new TimerTask() {
                    public void run() {
                        handler.post(new Runnable() {
                            public void run() {
                                unbindDrawables(pager);
                                System.gc();
                            }
                        });
                    }
                };
                new Timer().schedule(clearingHeapTask, 250);

                ConsumiLuceValoriRiferimento.this.finish();
                startActivity(intentHome);
                overridePendingTransition(R.anim.fade, R.anim.hold);
            }
        });

        final ImageView help = (ImageView) findViewById(R.id.consumi_luce_help);
        help.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
        help.getBackground().invalidateSelf();
        help.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                help.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
                help.getBackground().invalidateSelf();

                animation = Utilities.animation(ConsumiLuceValoriRiferimento.this);
                imageAnim = (ImageView) findViewById(R.id.waiting_anim);
                imageAnim.setVisibility(View.VISIBLE);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    imageAnim.setBackgroundDrawable(animation);
                } else {
                    imageAnim.setBackground(animation);
                }
                animation.start();

                Intent intentHelp = new Intent(getApplicationContext(), ConsumiLuceHelp.class);
                intentHelp.putExtra("ORIGINE", Constants.ORIGINE_CONSUMI_LUCE_VALORI_RIFERIMENTO);
                intentHelp.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
                intentHelp.putExtra("CATEGORY", getIntent().getStringExtra("CATEGORY"));
                intentHelp.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
                intentHelp.putExtra("Classificazione", getIntent().getStringExtra("Classificazione"));
                intentHelp.putExtra("PERIODO", getIntent().getIntExtra("PERIODO", Constants.RETAIL_PERIODO_MESI_6));
                intentHelp.putExtra("RIFERIMENTO", getIntent().getStringExtra("RIFERIMENTO"));
                intentHelp.putExtra("coloreGrafico", getIntent().getIntExtra("coloreGrafico", 0));

                TimerTask clearingHeapTask;
                final Handler handler = new Handler();
                clearingHeapTask = new TimerTask() {
                    public void run() {
                        handler.post(new Runnable() {
                            public void run() {
                                unbindDrawables(pager);
                                System.gc();
                            }
                        });
                    }
                };
                new Timer().schedule(clearingHeapTask, 250);

                ConsumiLuceValoriRiferimento.this.finish();
                startActivity(intentHelp);
                overridePendingTransition(R.anim.fade, R.anim.hold);

            }
        });
        ImageView consumi_luce_navigation_bar_2 = (ImageView) findViewById(R.id.consumi_luce_navigation_bar_2);
        consumi_luce_navigation_bar_2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                animation = Utilities.animation(ConsumiLuceValoriRiferimento.this);
                imageAnim = (ImageView) findViewById(R.id.waiting_anim);
                imageAnim.setVisibility(View.VISIBLE);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    imageAnim.setBackgroundDrawable(animation);
                } else {
                    imageAnim.setBackground(animation);
                }
                animation.start();

                final RelativeLayout consumi_luce_navigation_bar = (RelativeLayout) findViewById(R.id.consumi_luce_navigation_bar);
                consumi_luce_navigation_bar.setBackgroundResource(R.drawable.pulsanti_footer_grafico_consumi);
                // devo portare alla pagina di Valori di riferimento
                Intent intentConsumiLuceValoriRiferimento = new Intent(getApplicationContext(), ConsumiLuceGraficoConsumi.class);
                intentConsumiLuceValoriRiferimento.putExtra("PROVENIENZA", ConsumiLuceValoriRiferimento.class.getCanonicalName());
                intentConsumiLuceValoriRiferimento.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
                intentConsumiLuceValoriRiferimento.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
                intentConsumiLuceValoriRiferimento.putExtra("Classificazione", getIntent().getStringExtra("Classificazione"));
                intentConsumiLuceValoriRiferimento.putExtra("CATEGORY", getIntent().getStringExtra("CATEGORY"));
                intentConsumiLuceValoriRiferimento.putExtra("PERIODO", getIntent().getIntExtra("PERIODO", Constants.RETAIL_PERIODO_MESI_6));
                intentConsumiLuceValoriRiferimento.putExtra("RIFERIMENTO", getIntent().getStringExtra("RIFERIMENTO"));
                intentConsumiLuceValoriRiferimento.putExtra("coloreGrafico", getIntent().getIntExtra("coloreGrafico", 0));
                TimerTask clearingHeapTask;
                final Handler handler = new Handler();
                clearingHeapTask = new TimerTask() {
                    public void run() {
                        handler.post(new Runnable() {
                            public void run() {
                                unbindDrawables(pager);
                                System.gc();
                            }
                        });
                    }
                };
                new Timer().schedule(clearingHeapTask, 250);

                ConsumiLuceValoriRiferimento.this.finish();
                startActivity(intentConsumiLuceValoriRiferimento);
                overridePendingTransition(R.anim.fade, R.anim.hold);
            }
        });
        final RelativeLayout consumi_luce_navigation_bar = (RelativeLayout) findViewById(R.id.consumi_luce_navigation_bar);
        consumi_luce_navigation_bar.setBackgroundResource(R.drawable.pulsanti_footer_valori_riferimento);
        try {
            rispostaLogin = Constants.SERVIZIO_LOGIN;
            final JSONArray servizioLogin = new JSONArray(rispostaLogin);

            pager = (ScrollView) findViewById(R.id.consumi_luce_valori_riferimento_pager);
            pager.removeAllViews();
            inflater = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);
            inflaterBis = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);

            final JSONArray servizioConsumiLuce = new JSONArray(rispostaConsumiLuce);
            popolaPaginaContenuto(servizioLogin, servizioConsumiLuce, pager, primoConto.trim());

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void popolaPaginaContenuto(final JSONArray consumiLuceLogin, final JSONArray consumiLuce, ScrollView pager, final String primoConto) {
        // try {
        pager.removeAllViews();
        final View consumi_luce_entry = inflater.inflate(R.layout.consumi_luce_valori_riferimento_entry, null);

        EniFont periodo_mesi = (EniFont) consumi_luce_entry.findViewById(R.id.periodo_mesi_label);
        periodo_mesi.setText("" + periodo + " mesi");

        LinearLayout fascie_pmi = (LinearLayout) consumi_luce_entry.findViewById(R.id.fascie_pmi);
        LinearLayout fascie_retail = (LinearLayout) consumi_luce_entry.findViewById(R.id.fascie_retail);
        LinearLayout tabella_consumi_pmi = (LinearLayout) consumi_luce_entry.findViewById(R.id.tabella_consumi_pmi);
        LinearLayout tabella_consumi_retail = (LinearLayout) consumi_luce_entry.findViewById(R.id.tabella_consumi_retail);
        LinearLayout contenuto_tabella = (LinearLayout) consumi_luce_entry.findViewById(R.id.contenuto_tabella);

        if (classificazione.equals("RETAIL")) {
            fascie_pmi.setVisibility(View.GONE);
            tabella_consumi_pmi.setVisibility(View.GONE);
            fascie_retail.setVisibility(View.VISIBLE);
            tabella_consumi_retail.setVisibility(View.VISIBLE);

            TextView periodo_tabella = (TextView) consumi_luce_entry.findViewById(R.id.periodo_tabella_retail);
            periodo_tabella.setText("" + periodo + " mesi");

            float consumiFasciaF1 = calcolaConsumiFascia(consumiLuce, periodo, "F1");
            float consumiFasciaF23 = calcolaConsumiFascia(consumiLuce, periodo, "F23");
            int sommafascie = (int) (consumiFasciaF1 + consumiFasciaF23);
            TextView riga_1_1 = (TextView) consumi_luce_entry.findViewById(R.id.riga_retail_1_1);
            TextView riga_1_2 = (TextView) consumi_luce_entry.findViewById(R.id.riga_retail_1_2);
            TextView riga_1_3 = (TextView) consumi_luce_entry.findViewById(R.id.riga_retail_1_3);
            riga_1_1.setText("" + (int) consumiFasciaF1);
            riga_1_2.setText("" + (int) consumiFasciaF23);
            riga_1_3.setText("" + sommafascie);
            TextView riga_2_1 = (TextView) consumi_luce_entry.findViewById(R.id.riga_retail_2_1);
            TextView riga_2_2 = (TextView) consumi_luce_entry.findViewById(R.id.riga_retail_2_2);
            TextView riga_2_3 = (TextView) consumi_luce_entry.findViewById(R.id.riga_retail_2_3);
            riga_2_1.setText("" + ((sommafascie != 0) ? (arrotondaFloat(((consumiFasciaF1) * 100 / sommafascie))) : "0")
                    + getResources().getString(R.string.percentuale));
            riga_2_2.setText("" + ((sommafascie != 0) ? (arrotondaFloat((consumiFasciaF23) * 100 / sommafascie)) : "0")
                    + getResources().getString(R.string.percentuale));
            riga_2_3.setText(((sommafascie) != 0) ? "100" + getResources().getString(R.string.percentuale) : "0"
                    + getResources().getString(R.string.percentuale));

            popolaTabellaContenuto(consumiLuce, periodo, riferimento, contenuto_tabella);

        } else if (classificazione.equals("PMI")) {
            fascie_pmi.setVisibility(View.VISIBLE);
            tabella_consumi_pmi.setVisibility(View.VISIBLE);
            fascie_retail.setVisibility(View.GONE);
            tabella_consumi_retail.setVisibility(View.GONE);

            TextView periodo_tabella = (TextView) consumi_luce_entry.findViewById(R.id.periodo_tabella_pmi);
            periodo_tabella.setText("" + periodo + " mesi");

            float consumiFasciaF1 = calcolaConsumiFascia(consumiLuce, periodo, "F1");
            float consumiFasciaF2 = calcolaConsumiFascia(consumiLuce, periodo, "F2");
            float consumiFasciaF3 = calcolaConsumiFascia(consumiLuce, periodo, "F3");
            int sommaFascie = (int) (consumiFasciaF1 + consumiFasciaF2 + consumiFasciaF3);
            TextView riga_1_1 = (TextView) consumi_luce_entry.findViewById(R.id.riga_pmi_1_1);
            TextView riga_1_2 = (TextView) consumi_luce_entry.findViewById(R.id.riga_pmi_1_2);
            TextView riga_1_3 = (TextView) consumi_luce_entry.findViewById(R.id.riga_pmi_1_3);
            TextView riga_1_4 = (TextView) consumi_luce_entry.findViewById(R.id.riga_pmi_1_4);
            riga_1_1.setText("" + (int) consumiFasciaF1);
            riga_1_2.setText("" + (int) consumiFasciaF2);
            riga_1_3.setText("" + (int) consumiFasciaF3);
            riga_1_4.setText("" + sommaFascie);
            TextView riga_2_1 = (TextView) consumi_luce_entry.findViewById(R.id.riga_pmi_2_1);
            TextView riga_2_2 = (TextView) consumi_luce_entry.findViewById(R.id.riga_pmi_2_2);
            TextView riga_2_3 = (TextView) consumi_luce_entry.findViewById(R.id.riga_pmi_2_3);
            TextView riga_2_4 = (TextView) consumi_luce_entry.findViewById(R.id.riga_pmi_2_4);
            riga_2_1.setText("" + ((sommaFascie != 0) ? (arrotondaFloat((consumiFasciaF1) * 100 / sommaFascie)) : "0")
                    + getResources().getString(R.string.percentuale));
            riga_2_2.setText("" + ((sommaFascie != 0) ? (arrotondaFloat((consumiFasciaF2) * 100 / sommaFascie)) : "0")
                    + getResources().getString(R.string.percentuale));
            riga_2_3.setText("" + ((sommaFascie != 0) ? (arrotondaFloat((consumiFasciaF3) * 100 / sommaFascie)) : "0")
                    + getResources().getString(R.string.percentuale));
            riga_2_4.setText(((sommaFascie) != 0) ? ("100" + getResources().getString(R.string.percentuale)) : ("0" + getResources().getString(
                    R.string.percentuale)));

            popolaTabellaContenuto(consumiLuce, periodo, riferimento, contenuto_tabella);
        }

        TextView data_attivazione = (TextView) consumi_luce_entry.findViewById(R.id.data_attivazione);
        String dataAttivazione = getDataAttivazione(rispostaLogin, primoConto);
        String dataAttivazioneFormattata = convertiData(dataAttivazione);
        data_attivazione.setText(dataAttivazioneFormattata);

        TextView valori_0_presenti = (TextView) consumi_luce_entry.findViewById(R.id.valori_0_presenti);
        if (valoriAZero) {
            valori_0_presenti.setVisibility(View.VISIBLE);
        } else {
            valori_0_presenti.setVisibility(View.GONE);
        }

        TextView ultimo_mese_missing = (TextView) consumi_luce_entry.findViewById(R.id.ultimo_mese_missing);
        int limite = calcolaLimitePeriodo(consumiLuce, periodo);
        JSONObject consumi = null;
        try {
            if (classificazione.equals("RETAIL")) {
                consumi = consumiLuce.getJSONObject(2);
            } else if (classificazione.equals("PMI")) {
                consumi = consumiLuce.getJSONObject(1);
            }
            JSONArray arrayConsumi = consumi.getJSONArray("Consumi");
            Calendar cal = Calendar.getInstance();
            int year = cal.get(Calendar.YEAR);
            int month = cal.get(Calendar.MONTH);
            boolean presente = false;
            for (int i = 0; i < arrayConsumi.length(); i++) {
                JSONObject consumoIesimo = arrayConsumi.getJSONObject(i);
                if (consumoIesimo.getString("Mese").equals(theMonth(month)) && consumoIesimo.getInt("Anno") == year) {
                    presente = true;
                }
            }

            if (!presente) {
                // manca l'ultimo mese
                ultimo_mese_missing.setVisibility(View.VISIBLE);
            } else {
                ultimo_mese_missing.setVisibility(View.GONE);
            }
        } catch (JSONException e) {
            ultimo_mese_missing.setVisibility(View.GONE);
        }

        pager.addView(consumi_luce_entry);
    }

    public static String theMonth(int month) {
        String[] monthNames = {"Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre"};
        return monthNames[month];
    }

    private void popolaTabellaContenuto(JSONArray consumiLuce, int periodo, String riferimento, LinearLayout pager) {
        int limite = calcolaLimitePeriodo(consumiLuce, periodo);
        try {

            if (classificazione.equals("RETAIL")) {
                JSONObject consumi = consumiLuce.getJSONObject(2);
                JSONArray arrayConsumi = consumi.getJSONArray("Consumi");
                pager.removeAllViews();
                for (int i = (arrayConsumi.length() - limite); i <= arrayConsumi.length() - 1; i++) {
                    final View tabella_entry = inflaterBis.inflate(R.layout.consumi_luce_riga_retail, null);
                    JSONObject consumoIesimo = arrayConsumi.getJSONObject(i);
                    String f1 = consumoIesimo.getString("F1");
                    String f23 = consumoIesimo.getString("F23");
                    String anno = consumoIesimo.getString("Anno");
                    String mese = consumoIesimo.getString("Mese");

                    TextView mese_valore = (TextView) tabella_entry.findViewById(R.id.mese_valore);
                    mese_valore.setText((mese.substring(0, 3)) + " " + anno);

                    TextView f1_label = (TextView) tabella_entry.findViewById(R.id.f1);
                    f1_label.setText("" + arrotondaFloat(Float.valueOf(f1)));

                    TextView f23_label = (TextView) tabella_entry.findViewById(R.id.f23);
                    f23_label.setText("" + arrotondaFloat(Float.valueOf(f23)));

                    TextView totale = (TextView) tabella_entry.findViewById(R.id.totale);
                    totale.setText("" + (arrotondaFloat(Float.valueOf(f1)) + arrotondaFloat(Float.valueOf(f23))));

                    if (arrotondaFloat(Float.valueOf(f1)) == 0 || arrotondaFloat(Float.valueOf(f23)) == 0) {
                        valoriAZero = true;
                    }

                    pager.addView(tabella_entry);
                }

            } else if (classificazione.equals("PMI")) {
                JSONObject consumi = consumiLuce.getJSONObject(1);
                JSONArray arrayConsumi = consumi.getJSONArray("Consumi");
                pager.removeAllViews();
                for (int i = (arrayConsumi.length() - limite); i <= arrayConsumi.length() - 1; i++) {
                    final View tabella_entry = inflaterBis.inflate(R.layout.consumi_luce_riga_pmi, null);
                    JSONObject consumoIesimo = arrayConsumi.getJSONObject(i);
                    String f1 = consumoIesimo.getString("F1");
                    String f2 = consumoIesimo.getString("F2");
                    String f3 = consumoIesimo.getString("F3");
                    String anno = consumoIesimo.getString("Anno");
                    String mese = consumoIesimo.getString("Mese");
                    TextView mese_valore = (TextView) tabella_entry.findViewById(R.id.mese_valore);
                    mese_valore.setText((mese.substring(0, 3)) + " " + anno);

                    TextView f1_label = (TextView) tabella_entry.findViewById(R.id.f1);
                    f1_label.setText("" + arrotondaFloat(Float.valueOf(f1)));

                    TextView f2_label = (TextView) tabella_entry.findViewById(R.id.f2);
                    f2_label.setText("" + arrotondaFloat(Float.valueOf(f2)));

                    TextView f3_label = (TextView) tabella_entry.findViewById(R.id.f3);
                    f3_label.setText("" + arrotondaFloat(Float.valueOf(f3)));

                    TextView totale = (TextView) tabella_entry.findViewById(R.id.totale);
                    totale.setText("" + (arrotondaFloat(Float.valueOf(f1)) + arrotondaFloat(Float.valueOf(f2)) + arrotondaFloat(Float.valueOf(f3))));

                    if (arrotondaFloat(Float.valueOf(f1)) == 0 || arrotondaFloat(Float.valueOf(f2)) == 0 || arrotondaFloat(Float.valueOf(f3)) == 0) {
                        valoriAZero = true;
                    }

                    pager.addView(tabella_entry);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private int calcolaLimitePeriodo(JSONArray arrayConsumiLuce, int periodo) {
        int result = 0;
        Calendar now = Calendar.getInstance();
        now.setTime(new Date());
        String meseCorrente = arrayMesi[now.get(Calendar.MONTH)];
        int annoCorrente = now.get(Calendar.YEAR);
        try {
            if (classificazione.equals("RETAIL")) {
                JSONObject consumi = arrayConsumiLuce.getJSONObject(2);
                JSONArray arrayConsumi = consumi.getJSONArray("Consumi");
                JSONObject ultimoObject = arrayConsumi.getJSONObject(arrayConsumi.length() - 1);
                String annoUltimo = ultimoObject.getString("Anno");
                String meseUltimo = ultimoObject.getString("Mese");
                int meseUltimoIndex = getMeseIndex(meseUltimo);
                int annoUltimoInt = Integer.valueOf(annoUltimo);

                if (arrayConsumi.length() >= periodo) {
                    int limite = 0;
                    if (annoUltimoInt == annoCorrente) {
                        if (meseCorrente.equals(meseUltimo)) {
                            limite = periodo;
                        } else {
                            int differenza = now.get(Calendar.MONTH) - meseUltimoIndex;
                            limite = periodo - differenza;
                        }
                    } else if (annoUltimoInt < annoCorrente) {
                        int differenza = 12 - meseUltimoIndex;
                        limite = periodo - differenza;
                    }
                    result = limite;
                } else {
                    int limite = 0;
                    limite = arrayConsumi.length();
                    result = limite;
                }

            } else if (classificazione.equals("PMI")) {
                JSONObject consumi = arrayConsumiLuce.getJSONObject(1);
                JSONArray arrayConsumi = consumi.getJSONArray("Consumi");
                JSONObject ultimoObject = arrayConsumi.getJSONObject(arrayConsumi.length() - 1);
                String annoUltimo = ultimoObject.getString("Anno");
                String meseUltimo = ultimoObject.getString("Mese");
                int meseUltimoIndex = getMeseIndex(meseUltimo);
                int annoUltimoInt = Integer.valueOf(annoUltimo);

                if (arrayConsumi.length() >= periodo) {
                    int limite = 0;
                    if (annoUltimoInt == annoCorrente) {
                        if (meseCorrente.equals(meseUltimo)) {
                            limite = periodo;
                        } else {
                            int differenza = now.get(Calendar.MONTH) - meseUltimoIndex;
                            limite = periodo - differenza;
                        }
                    } else if (annoUltimoInt < annoCorrente) {
                        int differenza = 12 - meseUltimoIndex;
                        limite = periodo - differenza;
                    }
                    result = limite;
                } else {
                    int limite = 0;
                    limite = arrayConsumi.length();
                    result = limite;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    private float calcolaConsumiFascia(JSONArray arrayConsumiLuce, int periodo, String fascia) {
        float result = 0;
        int limite = calcolaLimitePeriodo(arrayConsumiLuce, periodo);
        try {
            if (classificazione.equals("RETAIL")) {
                JSONObject consumi = arrayConsumiLuce.getJSONObject(2);
                JSONArray arrayConsumi = consumi.getJSONArray("Consumi");
                // int sommaConsumiFascia = 0;
                for (int i = arrayConsumi.length() - 1; i >= arrayConsumi.length() - limite; i--) {
                    JSONObject consumoIesimo = arrayConsumi.getJSONObject(i);
                    String f1 = consumoIesimo.getString(fascia);
                    result = result + arrotondaFloat(Float.valueOf(f1));
                }
            } else if (classificazione.equals("PMI")) {
                JSONObject consumi = arrayConsumiLuce.getJSONObject(1);
                JSONArray arrayConsumi = consumi.getJSONArray("Consumi");

                // int sommaConsumiFascia = 0;
                for (int i = arrayConsumi.length() - 1; i >= arrayConsumi.length() - limite; i--) {
                    JSONObject consumoIesimo = arrayConsumi.getJSONObject(i);
                    String f1 = consumoIesimo.getString(fascia);
                    result = result + arrotondaFloat(Float.valueOf(f1));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    private int getMeseIndex(String mese) {
        int result = -1;
        for (int i = 0; i < arrayMesi.length; i++) {
            if (arrayMesi[i].equals(mese)) {
                result = i;
            }
        }
        return result;
    }

    public String getDataAttivazione(String stringLogin, String codiceContoLuce) {
        String result = "";
        JSONObject anagrContoIesimo = new JSONObject();
        JSONObject anagrExtraContoIesimo = new JSONObject();
        try {
            JSONArray arrayLogin = new JSONArray(stringLogin);
            for (int i = 2; i < arrayLogin.length(); i++) {
                JSONArray contoIesimo;
                contoIesimo = arrayLogin.getJSONArray(i);
                anagrContoIesimo = contoIesimo.getJSONObject(0);
                if (contoIesimo.length() > 1) {
                    anagrExtraContoIesimo = contoIesimo.getJSONObject(1);
                    String codiceConto = anagrContoIesimo.getString("CodiceConto");
                    String tipologia = anagrExtraContoIesimo.getString("Tipologia");
                    if (!codiceConto.equals(codiceContoLuce)) {
                        continue;
                    } else {
                        if (tipologia.equals("POWER")) {
                            result = anagrExtraContoIesimo.getString("DataAttivazione");
                        } else {
                            continue;
                        }
                    }
                } else {
                    continue;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    private int arrotondaFloat(float numero) {
        int result = 0;

        result = (int) (numero + 0.5);
        return result;
    }

    private String convertiData(String data) {
        String result;
        // SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH);
        Date dataEmis = new Date();
        try {
            dataEmis = sdf.parse(data);
        } catch (ParseException e) {
            // e.printStackTrace();
        }

        SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
        result = sdf1.format(dataEmis);
        return result;
    }

    private void unbindDrawables(View view) {
        if (view.getBackground() != null) {
            view.getBackground().setCallback(null);
        }
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                unbindDrawables(((ViewGroup) view).getChildAt(i));
            }
            ((ViewGroup) view).removeAllViews();
        }
    }

    public void onBackPressed() {

        animation = Utilities.animation(this);
        imageAnim = (ImageView) findViewById(R.id.waiting_anim);
        imageAnim.setVisibility(View.VISIBLE);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
            imageAnim.setBackgroundDrawable(animation);
        } else {
            imageAnim.setBackground(animation);
        }
        animation.start();

        Intent intentHome = new Intent(getApplicationContext(), ConsumiLuce.class);
        intentHome.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
        intentHome.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
        intentHome.putExtra("Classificazione", getIntent().getStringExtra("Classificazione"));
        intentHome.putExtra("CATEGORY", getIntent().getStringExtra("CATEGORY"));
        intentHome.putExtra("PERIODO", getIntent().getIntExtra("PERIODO", Constants.RETAIL_PERIODO_MESI_6));
        intentHome.putExtra("RIFERIMENTO", getIntent().getStringExtra("RIFERIMENTO"));
        intentHome.putExtra("coloreGrafico", getIntent().getIntExtra("coloreGrafico", 0));
        intentHome.putExtra("autoletturaLuce", false);
        TimerTask clearingHeapTask;
        final Handler handler = new Handler();
        clearingHeapTask = new TimerTask() {
            public void run() {
                handler.post(new Runnable() {
                    public void run() {
                        unbindDrawables(pager);
                        System.gc();
                    }
                });
            }
        };
        new Timer().schedule(clearingHeapTask, 250);

        ConsumiLuceValoriRiferimento.this.finish();
        startActivity(intentHome);
        overridePendingTransition(R.anim.fade, R.anim.hold);
    }
}
