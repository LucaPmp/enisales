package com.eni.enigaseluce.consumiluce;

import java.util.Calendar;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.PorterDuff;
import android.graphics.drawable.AnimationDrawable;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.eni.enigaseluce.R;
import com.eni.enigaseluce.httpcall.Constants;
import com.eni.enigaseluce.utils.Utilities;

@SuppressLint("NewApi")
public class ConsumiLuceGraficoConsumi extends Activity {

    private Context context;
    private static Context staticContext;
    AnimationDrawable animation;
    ImageView imageAnim;

    String primoConto;
    String classificazione;
    String riferimento;
    String category;
    int periodo;
    int coloreGrafico;
    String[] arrayMesi = { "Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre" };

    public String rispostaLogin = Constants.SERVIZIO_LOGIN;
    public String rispostaConsumiLuce = Constants.SERVIZIO_CONSUMI_LUCE;

    String datiGrafico = "";
    
	public static Context getContext() {
		return ConsumiLuceGraficoConsumi.staticContext;
	}

    @Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.consumi_luce_grafico_consumi);

		context = this;
		ConsumiLuceGraficoConsumi.staticContext = this;
		primoConto = getIntent().getStringExtra("PRIMOCONTO");
		classificazione = getIntent().getStringExtra("Classificazione");
		periodo = getIntent().getIntExtra("PERIODO", Constants.RETAIL_PERIODO_MESI_6);
		riferimento = getIntent().getStringExtra("RIFERIMENTO");
		category = getIntent().getStringExtra("CATEGORY");
		coloreGrafico = getIntent().getIntExtra("coloreGrafico", 0);
	}

    @Override
	protected void onStart() {
		super.onStart();
		primoConto = getIntent().getStringExtra("PRIMOCONTO");

		boolean landscape = context.getResources().getBoolean(R.bool.is_landscape);
		if (!landscape) {
			portrait();
		} else {
			landscape();

			ImageView icona_land = (ImageView) findViewById(R.id.icona_land);
			icona_land.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
				}
			});
		}
	}

	private void portrait() {
		final ImageView indietro = (ImageView) findViewById(R.id.consumi_luce_indietro);
		indietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
		indietro.getBackground().invalidateSelf();
		indietro.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				indietro.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
				indietro.getBackground().invalidateSelf();

				animation = Utilities.animation(ConsumiLuceGraficoConsumi.this);
				imageAnim = (ImageView) findViewById(R.id.waiting_anim);
				imageAnim.setVisibility(View.VISIBLE);
				if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
					imageAnim.setBackgroundDrawable(animation);
				} else {
					imageAnim.setBackground(animation);
				}
				animation.start();

				Intent intentHome = new Intent(getApplicationContext(), ConsumiLuce.class);
				
				/*if (getIntent().getStringExtra("PROVENIENZA") != null && getIntent().getStringExtra("PROVENIENZA").equals(ConsumiLuceValoriRiferimento.class.getCanonicalName())) {
					intentHome = new Intent(getApplicationContext(), ConsumiLuceValoriRiferimento.class);
				} else {

				}*/
				 
				intentHome.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
				intentHome.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
				intentHome.putExtra("CATEGORY", getIntent().getStringExtra("CATEGORY"));
				intentHome.putExtra("Classificazione", getIntent().getStringExtra("Classificazione"));
				intentHome.putExtra("PERIODO", getIntent().getIntExtra("PERIODO", Constants.RETAIL_PERIODO_MESI_6));
				intentHome.putExtra("RIFERIMENTO", getIntent().getStringExtra("RIFERIMENTO"));
				intentHome.putExtra("autoletturaLuce", false);
				ConsumiLuceGraficoConsumi.this.finish();
				startActivity(intentHome);
				overridePendingTransition(R.anim.fade, R.anim.hold);
			}
		});

		final ImageView help = (ImageView) findViewById(R.id.consumi_luce_help);
		help.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
		help.getBackground().invalidateSelf();
		help.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				help.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
				help.getBackground().invalidateSelf();

				animation = Utilities.animation(ConsumiLuceGraficoConsumi.this);
				imageAnim = (ImageView) findViewById(R.id.waiting_anim);
				imageAnim.setVisibility(View.VISIBLE);
				if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
					imageAnim.setBackgroundDrawable(animation);
				} else {
					imageAnim.setBackground(animation);
				}
				animation.start();

				Intent intentHelp = new Intent(getApplicationContext(), ConsumiLuceHelp.class);
				intentHelp.putExtra("ORIGINE", Constants.ORIGINE_CONSUMI_LUCE_GRAFICO_CONSUMI);
				intentHelp.putExtra("Classificazione", getIntent().getStringExtra("Classificazione"));
				intentHelp.putExtra("CATEGORY", getIntent().getStringExtra("CATEGORY"));
				intentHelp.putExtra("coloreGrafico", getIntent().getIntExtra("coloreGrafico", 0));
				intentHelp.putExtra("RIFERIMENTO", getIntent().getStringExtra("RIFERIMENTO"));
				intentHelp.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
				intentHelp.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
				intentHelp.putExtra("PERIODO", getIntent().getIntExtra("PERIODO", Constants.RETAIL_PERIODO_MESI_6));
				ConsumiLuceGraficoConsumi.this.finish();
				startActivity(intentHelp);
				overridePendingTransition(R.anim.fade, R.anim.hold);
			}
		});

		ImageView consumi_luce_navigation_bar_1 = (ImageView) findViewById(R.id.consumi_luce_navigation_bar_1);
		consumi_luce_navigation_bar_1.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {

				animation = Utilities.animation(ConsumiLuceGraficoConsumi.this);
				imageAnim = (ImageView) findViewById(R.id.waiting_anim);
				imageAnim.setVisibility(View.VISIBLE);
				if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
					imageAnim.setBackgroundDrawable(animation);
				} else {
					imageAnim.setBackground(animation);
				}
				animation.start();

				final RelativeLayout consumi_luce_navigation_bar = (RelativeLayout) findViewById(R.id.consumi_luce_navigation_bar);
				consumi_luce_navigation_bar.setBackgroundResource(R.drawable.pulsanti_footer_valori_riferimento);
				// devo portare alla pagina di Valori di riferimento
				Intent intentConsumiLuceValoriRiferimento = new Intent(getApplicationContext(), ConsumiLuceValoriRiferimento.class);
				intentConsumiLuceValoriRiferimento.putExtra("PROVENIENZA", ConsumiLuceGraficoConsumi.class.getCanonicalName());
				intentConsumiLuceValoriRiferimento.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
				intentConsumiLuceValoriRiferimento.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
				intentConsumiLuceValoriRiferimento.putExtra("Classificazione", getIntent().getStringExtra("Classificazione"));
				intentConsumiLuceValoriRiferimento.putExtra("CATEGORY", getIntent().getStringExtra("CATEGORY"));
				intentConsumiLuceValoriRiferimento.putExtra("PERIODO", getIntent().getIntExtra("PERIODO", Constants.RETAIL_PERIODO_MESI_6));
				intentConsumiLuceValoriRiferimento.putExtra("RIFERIMENTO", getIntent().getStringExtra("RIFERIMENTO"));
				intentConsumiLuceValoriRiferimento.putExtra("coloreGrafico", getIntent().getIntExtra("coloreGrafico", 0));
				ConsumiLuceGraficoConsumi.this.finish();
				startActivity(intentConsumiLuceValoriRiferimento);
				overridePendingTransition(R.anim.fade, R.anim.hold);
			}
		});
		final RelativeLayout consumi_luce_navigation_bar = (RelativeLayout) findViewById(R.id.consumi_luce_navigation_bar);
		consumi_luce_navigation_bar.setBackgroundResource(R.drawable.pulsanti_footer_grafico_consumi);

		final TextView testo_dinamico_1 = (TextView) findViewById(R.id.testo_dinamico_1);
		final TextView testo_dinamico_2 = (TextView) findViewById(R.id.testo_dinamico_2);
		if (classificazione.equals("RETAIL")) {
			testo_dinamico_1.setText(Html.fromHtml("Periodo " + "<b>" + periodo + " mesi</b>"));
			testo_dinamico_2.setText(Html.fromHtml("Consumi di riferimento " + "<b>" + riferimento + "</b>"));
		} else if (classificazione.equals("PMI")) {
			testo_dinamico_1.setText(Html.fromHtml("Periodo " + "<b>" + periodo + " mesi</b>"));
			testo_dinamico_2.setText(Html.fromHtml("Settore merceologico " + "<b>" + category + "</b>"));
		}
		final WebView webView = (WebView) findViewById(R.id.webView);
		webView.getSettings().setJavaScriptEnabled(true);
		webView.getSettings().setLoadWithOverviewMode(true);
		webView.getSettings().setUseWideViewPort(true);
		// webView.getSettings().setBuiltInZoomControls(true);
		if (Build.VERSION.SDK_INT >= 11) {
			datiGrafico = "$(document).ready(function(){";
			String categories = "";

			int limite = calcolaLimitePeriodo(rispostaConsumiLuce, classificazione, periodo);

			if (classificazione.equals("RETAIL")) {
				String f1Retail = "";
				String f23Retail = "";
				String mioConsumo = "";
				String areaData = "";
				
				webView.loadUrl("file:///android_asset/grafici/index_luce_retail.htm");
				try {
					JSONArray consumiLuce = new JSONArray(rispostaConsumiLuce);
					JSONObject consumi = consumiLuce.getJSONObject(2);
					JSONArray arrayConsumi = consumi.getJSONArray("Consumi");
					for (int i = (arrayConsumi.length() - limite); i <= arrayConsumi.length() - 1; i++) {
						JSONObject consumoIesimo = arrayConsumi.getJSONObject(i);
						String anno = consumoIesimo.getString("Anno");
						String mese = consumoIesimo.getString("Mese");
						String f1 = consumoIesimo.getString("F1");
						String f23 = consumoIesimo.getString("F23");
						String consumo = "" + (arrotondaFloat(Float.valueOf(f1)) + arrotondaFloat(Float.valueOf(f23)));
						if (i == arrayConsumi.length() - 1) {
							categories = categories + "'" + ((mese.substring(0, 3)) + " " + anno)+ "'";
							f1Retail = f1Retail + (arrotondaFloat(Float.valueOf(f1)));
							f23Retail = f23Retail + (arrotondaFloat(Float.valueOf(f23)));
							mioConsumo = mioConsumo + consumo;
						} else {
							categories = categories + "'" + ((mese.substring(0, 3)) + " " + anno) + "',";
							f1Retail = f1Retail + (arrotondaFloat(Float.valueOf(f1))) + ",";
							f23Retail = f23Retail + (arrotondaFloat(Float.valueOf(f23))) + ",";
							mioConsumo = mioConsumo + consumo + ",";
						}
					}
					JSONObject riferimentoObject = consumiLuce.getJSONObject(3);
					JSONArray arrayRiferimento = riferimentoObject.getJSONArray("Riferimento");
					JSONArray arrayProfilo = new JSONArray();
					if (riferimento.equals(Constants.RETAIL_RIFERIMENTO_MINI)) {
						JSONObject objectRiferimentoMini = arrayRiferimento.getJSONObject(0);
						arrayProfilo = objectRiferimentoMini.getJSONArray("RiferimentoMini");
					} else if (riferimento.equals(Constants.RETAIL_RIFERIMENTO_MIDI)) {
						JSONObject objectRiferimentoMini = arrayRiferimento.getJSONObject(1);
						arrayProfilo = objectRiferimentoMini.getJSONArray("RiferimentoMidi");
					} else if (riferimento.equals(Constants.RETAIL_RIFERIMENTO_MAXI)) {
						JSONObject objectRiferimentoMini = arrayRiferimento.getJSONObject(2);
						arrayProfilo = objectRiferimentoMini.getJSONArray("RiferimentoMaxi");
					}
					for (int j = (arrayProfilo.length() - limite); j <= arrayProfilo.length() - 1; j++) {
						JSONObject consumoProfiloIesimo = arrayProfilo.getJSONObject(j);
						String consumoProfiloI = consumoProfiloIesimo.getString("Consumo");
						if (j == arrayProfilo.length() - 1) {
							areaData = areaData + consumoProfiloI;
						} else {
							areaData = areaData + consumoProfiloI + ",";
						}
					}

				} catch (JSONException e) {
					e.printStackTrace();
				}

				datiGrafico += "categories = [" + categories + "];"
							+ "mioConsumo = [" + mioConsumo + "];"
							+ "areaData = [ " + areaData + "];"
							+ "coloreGrafico = " + coloreGrafico + ";"
							+ "f1Retail = [ " + f1Retail + "];" 
							+ "f23Retail = [" + f23Retail + "];";
				datiGrafico = datiGrafico + "loadGraphRetail();";
				datiGrafico = datiGrafico + "});";
			} else if (classificazione.equals("PMI")) {
				webView.loadUrl("file:///android_asset/grafici/index_luce_pmi.htm");

				String f1Pmi = "";
				String f2Pmi = "";
				String f3Pmi = "";
				String mioConsumo = "";
				String areaData = "";
				try {
					JSONArray consumiLuce = new JSONArray(rispostaConsumiLuce);
					JSONObject consumi = consumiLuce.getJSONObject(1);
					JSONArray arrayConsumi = consumi.getJSONArray("Consumi");
					for (int i = (arrayConsumi.length() - limite); i <= arrayConsumi.length() - 1; i++) {
						JSONObject consumoIesimo = arrayConsumi.getJSONObject(i);
						String anno = consumoIesimo.getString("Anno");
						String mese = consumoIesimo.getString("Mese");
						String f1 = consumoIesimo.getString("F1");
						String f2 = consumoIesimo.getString("F2");
						String f3 = consumoIesimo.getString("F3");
						String consumo = "" + (arrotondaFloat(Float.valueOf(f1)) + arrotondaFloat(Float.valueOf(f2)) + arrotondaFloat(Float.valueOf(f3)));
						if (i == arrayConsumi.length() - 1) {
							categories = categories + "'" + ((mese.substring(0, 3)) + " " + anno) + "'";
							f1Pmi = f1Pmi + (arrotondaFloat(Float.valueOf(f1)));
							f2Pmi = f2Pmi + (arrotondaFloat(Float.valueOf(f2)));
							f3Pmi = f3Pmi + (arrotondaFloat(Float.valueOf(f3)));
							mioConsumo = mioConsumo + consumo;
						} else {
							categories = categories + "'" + ((mese.substring(0, 3)) + " " + anno) + "',";
							f1Pmi = f1Pmi + (arrotondaFloat(Float.valueOf(f1))) + ",";
							f2Pmi = f2Pmi + (arrotondaFloat(Float.valueOf(f2))) + ",";
							f3Pmi = f3Pmi + (arrotondaFloat(Float.valueOf(f3))) + ",";
							mioConsumo = mioConsumo + consumo + ",";
						}
					}
					JSONObject riferimentoObject = consumiLuce.getJSONObject(2);
					JSONArray arrayRiferimento = riferimentoObject.getJSONArray("Riferimento");
					JSONArray arrayProfilo = new JSONArray();
					if (riferimento.equals(Constants.PMI_RIFERIMENTO_BASELOAD)) {
						JSONObject objectRiferimentoBaseload = arrayRiferimento.getJSONObject(0);
						arrayProfilo = objectRiferimentoBaseload.getJSONArray("RiferimentoBaseload");
					} else if (riferimento.equals(Constants.PMI_RIFERIMENTO_PEAK)) {
						JSONObject objectRiferimentoPeak = arrayRiferimento.getJSONObject(1);
						arrayProfilo = objectRiferimentoPeak.getJSONArray("RiferimentoPeak");
					} else if (riferimento.equals(Constants.PMI_RIFERIMENTO_OFFPEAK)) {
						JSONObject objectRiferimentoOffpeak = arrayRiferimento.getJSONObject(2);
						arrayProfilo = objectRiferimentoOffpeak.getJSONArray("RiferimentoOffPeak");
					}
					for (int j = (arrayProfilo.length() - limite); j <= arrayProfilo.length() - 1; j++) {
						JSONObject consumoProfiloIesimo = arrayProfilo.getJSONObject(j);
						String consumoProfiloI = consumoProfiloIesimo.getString("Consumo");
						if (j == arrayProfilo.length() - 1) {
							areaData = areaData + consumoProfiloI;
						} else {
							areaData = areaData + consumoProfiloI + ",";
						}
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
				datiGrafico += "categories = [" + categories + "];"
							+ "mioConsumo = [" + mioConsumo + "];"
							+ "areaData = [ " + areaData + "];"
							+ "coloreGrafico = " + coloreGrafico + ";"
							+ "f1Pmi = [ " + f1Pmi + "];" 
							+ "f2Pmi = [" + f2Pmi + "];" 
							+ "f3Pmi = [" + f3Pmi + "];";
				datiGrafico = datiGrafico + "loadGraphPmi();";
				datiGrafico = datiGrafico + "});";
			}

		} else {
			webView.loadUrl("file:///android_asset/grafici/index_luce_no_graph.htm");
		}
		
		webView.setWebViewClient(new WebViewClient() {
			@Override
			public void onPageFinished(WebView view, String url) {
				super.onPageFinished(view, url);
				loadJavascript(view, datiGrafico);
			}
		});
	}

	private void landscape() {
		final WebView webView = (WebView) findViewById(R.id.webView);
		webView.getSettings().setJavaScriptEnabled(true);
		webView.getSettings().setLoadWithOverviewMode(true);
		webView.getSettings().setUseWideViewPort(true);
		// webView.getSettings().setBuiltInZoomControls(true);

		if (Build.VERSION.SDK_INT >= 11) {
			webView.loadUrl("file:///android_asset/grafici/index_luce_land.htm");

			datiGrafico = "$(document).ready(function(){";
			String categories = "";

			int limite = calcolaLimitePeriodo(rispostaConsumiLuce, classificazione, periodo);
			if (classificazione.equals("RETAIL")) {
				((TextView)findViewById(R.id.land_fascia23)).setText("Fascia F23");
				findViewById(R.id.land_fascia3_dot).setVisibility(View.GONE);
				findViewById(R.id.land_fascia3).setVisibility(View.GONE);
				String f1Retail = "";
				String f23Retail = "";
				String mioConsumo = "";
				String areaData = "";
				ImageView img_sopra = (ImageView) findViewById(R.id.img_sopra);
				if (coloreGrafico == 0) {
					// verde barra_sopra_land
					img_sopra.setBackgroundResource(R.drawable.green_dot);
				} else if (coloreGrafico == 1) {
					// giallo
					img_sopra.setBackgroundResource(R.drawable.yellow_dot);
				} else if (coloreGrafico == 2) {
					// grosso
					img_sopra.setBackgroundResource(R.drawable.red_dot);
				}
				try {
					JSONArray consumiLuce = new JSONArray(rispostaConsumiLuce);
					JSONObject consumi = consumiLuce.getJSONObject(2);
					JSONArray arrayConsumi = consumi.getJSONArray("Consumi");
					for (int i = (arrayConsumi.length() - limite); i <= arrayConsumi.length() - 1; i++) {
						JSONObject consumoIesimo = arrayConsumi.getJSONObject(i);
						String anno = consumoIesimo.getString("Anno");
						String mese = consumoIesimo.getString("Mese");
						String f1 = consumoIesimo.getString("F1");
						String f23 = consumoIesimo.getString("F23");
						String consumo = "" + (arrotondaFloat(Float.valueOf(f1)) + arrotondaFloat(Float.valueOf(f23)));
						if (i == arrayConsumi.length() - 1) {
							categories = categories + "'" + ((mese.substring(0, 3)) + " " + anno) + "'";
							f1Retail = f1Retail + (arrotondaFloat(Float.valueOf(f1)));
							f23Retail = f23Retail + (arrotondaFloat(Float.valueOf(f23)));
							mioConsumo = mioConsumo + consumo;
						} else {
							categories = categories + "'" + ((mese.substring(0, 3)) + " " + anno) + "',";
							f1Retail = f1Retail + (arrotondaFloat(Float.valueOf(f1))) + ",";
							f23Retail = f23Retail + (arrotondaFloat(Float.valueOf(f23))) + ",";
							mioConsumo = mioConsumo + consumo + ",";
						}
					}
					JSONObject riferimentoObject = consumiLuce.getJSONObject(3);
					JSONArray arrayRiferimento = riferimentoObject.getJSONArray("Riferimento");
					JSONArray arrayProfilo = new JSONArray();
					if (riferimento.equals(Constants.RETAIL_RIFERIMENTO_MINI)) {
						JSONObject objectRiferimentoMini = arrayRiferimento.getJSONObject(0);
						arrayProfilo = objectRiferimentoMini.getJSONArray("RiferimentoMini");
					} else if (riferimento.equals(Constants.RETAIL_RIFERIMENTO_MIDI)) {
						JSONObject objectRiferimentoMini = arrayRiferimento.getJSONObject(1);
						arrayProfilo = objectRiferimentoMini.getJSONArray("RiferimentoMidi");
					} else if (riferimento.equals(Constants.RETAIL_RIFERIMENTO_MAXI)) {
						JSONObject objectRiferimentoMini = arrayRiferimento.getJSONObject(2);
						arrayProfilo = objectRiferimentoMini.getJSONArray("RiferimentoMaxi");
					}
					for (int j = (arrayProfilo.length() - limite); j <= arrayProfilo.length() - 1; j++) {
						JSONObject consumoProfiloIesimo = arrayProfilo.getJSONObject(j);
						String consumoProfiloI = consumoProfiloIesimo.getString("Consumo");
						if (j == arrayProfilo.length() - 1) {
							areaData = areaData + consumoProfiloI;
						} else {
							areaData = areaData + consumoProfiloI + ",";
						}
					}

				} catch (JSONException e) {
					e.printStackTrace();
				}

				datiGrafico += "categories = [" + categories + "];"
							+ "mioConsumo = [" + mioConsumo + "];"
							+ "areaData = [ " + areaData + "];"
							+ "coloreGrafico = " + coloreGrafico + ";"
							+ "f1Retail = [ " + f1Retail + "];" 
							+ "f23Retail = [" + f23Retail + "];";
				datiGrafico = datiGrafico + "loadGraphRetail();";
				datiGrafico = datiGrafico + "});";
			} else if (classificazione.equals("PMI")) {
				((TextView)findViewById(R.id.land_fascia23)).setText("Fascia F2");
				findViewById(R.id.land_fascia3_dot).setVisibility(View.VISIBLE);
				findViewById(R.id.land_fascia3).setVisibility(View.VISIBLE);
				String f1Pmi = "";
				String f2Pmi = "";
				String f3Pmi = "";
				String mioConsumo = "";
				String areaData = "";
				ImageView img_sopra = (ImageView) findViewById(R.id.img_sopra);
				if (coloreGrafico == 0) {
					// verde barra_sopra_land
					img_sopra.setBackgroundResource(R.drawable.green_dot);
				} else if (coloreGrafico == 1) {
					// giallo
					img_sopra.setBackgroundResource(R.drawable.yellow_dot);
				} else if (coloreGrafico == 2) {
					// grosso
					img_sopra.setBackgroundResource(R.drawable.red_dot);
				}
				try {
					JSONArray consumiLuce = new JSONArray(rispostaConsumiLuce);
					JSONObject consumi = consumiLuce.getJSONObject(1);
					JSONArray arrayConsumi = consumi.getJSONArray("Consumi");
					for (int i = (arrayConsumi.length() - limite); i <= arrayConsumi.length() - 1; i++) {
						JSONObject consumoIesimo = arrayConsumi.getJSONObject(i);
						String anno = consumoIesimo.getString("Anno");
						String mese = consumoIesimo.getString("Mese");
						String f1 = consumoIesimo.getString("F1");
						String f2 = consumoIesimo.getString("F2");
						String f3 = consumoIesimo.getString("F3");
						String consumo = "" + (arrotondaFloat(Float.valueOf(f1)) + arrotondaFloat(Float.valueOf(f2)) + arrotondaFloat(Float.valueOf(f3)));
						if (i == arrayConsumi.length() - 1) {
							categories = categories + "'" + ((mese.substring(0, 3)) + " " + anno) + "'";
							f1Pmi = f1Pmi + (arrotondaFloat(Float.valueOf(f1)));
							f2Pmi = f2Pmi + (arrotondaFloat(Float.valueOf(f2)));
							f3Pmi = f3Pmi + (arrotondaFloat(Float.valueOf(f3)));
							mioConsumo = mioConsumo + consumo;
						} else {
							categories = categories + "'" + ((mese.substring(0, 3)) + " " + anno) + "',";
							f1Pmi = f1Pmi + (arrotondaFloat(Float.valueOf(f1))) + ",";
							f2Pmi = f2Pmi + (arrotondaFloat(Float.valueOf(f2))) + ",";
							f3Pmi = f3Pmi + (arrotondaFloat(Float.valueOf(f3))) + ",";
							mioConsumo = mioConsumo + consumo + ",";
						}
					}
					JSONObject riferimentoObject = consumiLuce.getJSONObject(2);
					JSONArray arrayRiferimento = riferimentoObject.getJSONArray("Riferimento");
					JSONArray arrayProfilo = new JSONArray();
					if (riferimento.equals(Constants.PMI_RIFERIMENTO_BASELOAD)) {
						JSONObject objectRiferimentoBaseload = arrayRiferimento.getJSONObject(0);
						arrayProfilo = objectRiferimentoBaseload.getJSONArray("RiferimentoBaseload");
					} else if (riferimento.equals(Constants.PMI_RIFERIMENTO_PEAK)) {
						JSONObject objectRiferimentoPeak = arrayRiferimento.getJSONObject(1);
						arrayProfilo = objectRiferimentoPeak.getJSONArray("RiferimentoPeak");
					} else if (riferimento.equals(Constants.PMI_RIFERIMENTO_OFFPEAK)) {
						JSONObject objectRiferimentoOffpeak = arrayRiferimento.getJSONObject(2);
						arrayProfilo = objectRiferimentoOffpeak.getJSONArray("RiferimentoOffPeak");
					}
					for (int j = (arrayProfilo.length() - limite); j <= arrayProfilo.length() - 1; j++) {
						JSONObject consumoProfiloIesimo = arrayProfilo.getJSONObject(j);
						String consumoProfiloI = consumoProfiloIesimo.getString("Consumo");
						if (j == arrayProfilo.length() - 1) {
							areaData = areaData + consumoProfiloI;
						} else {
							areaData = areaData + consumoProfiloI + ",";
						}
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
				datiGrafico += "categories = [" + categories + "];"
							+ "mioConsumo = [" + mioConsumo + "];"
							+ "areaData = [ " + areaData + "];"
							+ "coloreGrafico = " + coloreGrafico + ";"
							+ "f1Pmi = [ " + f1Pmi + "];" 
							+ "f2Pmi = [" + f2Pmi + "];" 
							+ "f3Pmi = [" + f3Pmi + "];";
				datiGrafico = datiGrafico + "loadGraphPmi();";
				datiGrafico = datiGrafico + "});";
			}
		} else {
			webView.loadUrl("file:///android_asset/grafici/index_luce_no_graph.htm");
		}
		
		webView.setWebViewClient(new WebViewClient() {
			@Override
			public void onPageFinished(WebView view, String url) {
				super.onPageFinished(view, url);
				loadJavascript(view, datiGrafico);
			}
		});
	}

	private int calcolaLimitePeriodo(String rispostaConsumiLuce, String classificazione, int periodo) {
		int result = 0;
		Calendar now = Calendar.getInstance();
		now.setTime(new Date());
		String meseCorrente = arrayMesi[now.get(Calendar.MONTH)];
		int annoCorrente = now.get(Calendar.YEAR);
		try {
			JSONArray array = new JSONArray(rispostaConsumiLuce);
			if (classificazione.equals("RETAIL")) {
				JSONObject consumi = array.getJSONObject(2);
				JSONArray arrayConsumi = consumi.getJSONArray("Consumi");
				JSONObject ultimoObject = arrayConsumi.getJSONObject(arrayConsumi.length() - 1);
				String annoUltimo = ultimoObject.getString("Anno");
				String meseUltimo = ultimoObject.getString("Mese");
				int meseUltimoIndex = getMeseIndex(meseUltimo);
				int annoUltimoInt = Integer.valueOf(annoUltimo);

				if (arrayConsumi.length() >= periodo) {
					int limite = 0;
					if (annoUltimoInt == annoCorrente) {
						if (meseCorrente.equals(meseUltimo)) {
							limite = periodo;
						} else {
							int differenza = now.get(Calendar.MONTH) - meseUltimoIndex;
							limite = periodo - differenza;
						}
					} else if (annoUltimoInt < annoCorrente) {
						int differenza = 12 - meseUltimoIndex;
						limite = periodo - differenza;
					}
					result = limite;
				} else {
					int limite = 0;
					limite = arrayConsumi.length();
					result = limite;
				}

			} else if (classificazione.equals("PMI")) {
				JSONObject consumi = array.getJSONObject(1);
				JSONArray arrayConsumi = consumi.getJSONArray("Consumi");
				JSONObject ultimoObject = arrayConsumi.getJSONObject(arrayConsumi.length() - 1);
				String annoUltimo = ultimoObject.getString("Anno");
				String meseUltimo = ultimoObject.getString("Mese");
				int meseUltimoIndex = getMeseIndex(meseUltimo);
				int annoUltimoInt = Integer.valueOf(annoUltimo);

				if (arrayConsumi.length() >= periodo) {
					int limite = 0;
					if (annoUltimoInt == annoCorrente) {
						if (meseCorrente.equals(meseUltimo)) {
							limite = periodo;
						} else {
							int differenza = now.get(Calendar.MONTH) - meseUltimoIndex;
							limite = periodo - differenza;
						}
					} else if (annoUltimoInt < annoCorrente) {
						int differenza = 12 - meseUltimoIndex;
						limite = periodo - differenza;
					}
					result = limite;
				} else {
					int limite = 0;
					limite = arrayConsumi.length();
					result = limite;
				}
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
		return result;
	}

	private int getMeseIndex(String mese) {
		int result = -1;
		for (int i = 0; i < arrayMesi.length; i++) {
			if (arrayMesi[i].equals(mese)) {
				result = i;
			}
		}
		return result;
	}

	private int arrotondaFloat(float numero) {
		int result = 0;

		result = (int) (numero + 0.5);
		return result;
	}

	public void onBackPressed() {

		animation = Utilities.animation(this);
		imageAnim = (ImageView) findViewById(R.id.waiting_anim);
		imageAnim.setVisibility(View.VISIBLE);
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
			imageAnim.setBackgroundDrawable(animation);
		} else {
			imageAnim.setBackground(animation);
		}
		animation.start();

		Intent intentHome = new Intent(getApplicationContext(), ConsumiLuce.class);
		
		/*if (getIntent().getStringExtra("PROVENIENZA") != null && getIntent().getStringExtra("PROVENIENZA").equals(ConsumiLuceValoriRiferimento.class.getCanonicalName())) {
			intentHome = new Intent(getApplicationContext(), ConsumiLuceValoriRiferimento.class);
		} else {

		}*/
		 
		intentHome.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
		intentHome.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
		intentHome.putExtra("CATEGORY", getIntent().getStringExtra("CATEGORY"));
		intentHome.putExtra("Classificazione", getIntent().getStringExtra("Classificazione"));
		intentHome.putExtra("PERIODO", getIntent().getIntExtra("PERIODO", Constants.RETAIL_PERIODO_MESI_6));
		intentHome.putExtra("RIFERIMENTO", getIntent().getStringExtra("RIFERIMENTO"));
		intentHome.putExtra("autoletturaLuce", false);
		ConsumiLuceGraficoConsumi.this.finish();
		startActivity(intentHome);
		overridePendingTransition(R.anim.fade, R.anim.hold);
	}
	
	private void loadJavascript(WebView webView, String datiGrafico) {
   	 if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
	    	webView.evaluateJavascript(datiGrafico, null);
        } else {
        	 webView.loadUrl("javascript:" + datiGrafico);
        }
	}
}
