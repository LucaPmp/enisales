package com.eni.enigaseluce.consumiluce;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.AnimationDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.eni.enigaseluce.R;
import com.eni.enigaseluce.httpcall.Constants;
import com.eni.enigaseluce.utils.Utilities;

@SuppressLint("NewApi")
public class SettoreMerceologicoLuce extends Activity {

    AnimationDrawable animation;
    ImageView imageAnim;

    ArrayList<String> settori_luce_list;
    static LinearLayout pager;
    static LayoutInflater inflater;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.settore_merceologico_luce);
	settori_luce_list = new ArrayList<String>();
	settori_luce_list.add(new String(getResources().getString(R.string.settore_merceologico_luce_category_1)));
	settori_luce_list.add(new String(getResources().getString(R.string.settore_merceologico_luce_category_3)));
	settori_luce_list.add(new String(getResources().getString(R.string.settore_merceologico_luce_category_4)));
	settori_luce_list.add(new String(getResources().getString(R.string.settore_merceologico_luce_category_5)));
	settori_luce_list.add(new String(getResources().getString(R.string.settore_merceologico_luce_category_6)));
	settori_luce_list.add(new String(getResources().getString(R.string.settore_merceologico_luce_category_7)));
	settori_luce_list.add(new String(getResources().getString(R.string.settore_merceologico_luce_category_8)));
	settori_luce_list.add(new String(getResources().getString(R.string.settore_merceologico_luce_category_9)));
	settori_luce_list.add(new String(getResources().getString(R.string.settore_merceologico_luce_category_10)));
	settori_luce_list.add(new String(getResources().getString(R.string.settore_merceologico_luce_category_11)));
	settori_luce_list.add(new String(getResources().getString(R.string.settore_merceologico_luce_category_12)));
	settori_luce_list.add(new String(getResources().getString(R.string.settore_merceologico_luce_category_13)));
	settori_luce_list.add(new String(getResources().getString(R.string.settore_merceologico_luce_category_14)));
	settori_luce_list.add(new String(getResources().getString(R.string.settore_merceologico_luce_category_15)));
	settori_luce_list.add(new String(getResources().getString(R.string.settore_merceologico_luce_category_16)));
	settori_luce_list.add(new String(getResources().getString(R.string.settore_merceologico_luce_category_2)));

    }

    @Override
    protected void onStart() {
	super.onStart();

	final ImageView indietro = (ImageView) findViewById(R.id.settore_merceologico_luce_indietro);
	indietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
	indietro.getBackground().invalidateSelf();
	indietro.setOnClickListener(new View.OnClickListener() {
	    public void onClick(View v) {
		indietro.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
		indietro.getBackground().invalidateSelf();

		animation = Utilities.animation(SettoreMerceologicoLuce.this);
		imageAnim = (ImageView) findViewById(R.id.waiting_anim);
		imageAnim.setVisibility(View.VISIBLE);
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
		    imageAnim.setBackgroundDrawable(animation);
		}
		else {
		    imageAnim.setBackground(animation);
		}
		animation.start();

		Intent intentConsumiLuce = new Intent(getApplicationContext(), ConsumiLuce.class);
		intentConsumiLuce.putExtra("Classificazione", getIntent().getStringExtra("Classificazione"));
		intentConsumiLuce.putExtra("PERIODO", getIntent().getIntExtra("PERIODO", Constants.RETAIL_PERIODO_MESI_6));
		intentConsumiLuce.putExtra("RIFERIMENTO", getIntent().getStringExtra("RIFERIMENTO"));
		intentConsumiLuce.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
		intentConsumiLuce.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
		intentConsumiLuce.putExtra("CATEGORY", getIntent().getStringExtra("CATEGORY"));
		intentConsumiLuce.putExtra("autoletturaLuce", false);
		SettoreMerceologicoLuce.this.finish();
		startActivity(intentConsumiLuce);
		overridePendingTransition(R.anim.fade, R.anim.hold);
	    }
	});

	pager = (LinearLayout) findViewById(R.id.settore_merceologico_luce_pager);
	pager.removeAllViews();
	inflater = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);
	for (int i = 0; i <= settori_luce_list.size() - 1; i++) {
	    final View riga = inflater.inflate(R.layout.settore_merceologico_riga, null);
	    RelativeLayout riga_entry = (RelativeLayout) riga.findViewById(R.id.settore_merceologico_riga);
	    if (i % 2 == 0) {
		riga_entry.setBackgroundResource(R.drawable.settore_merceologico_entry_bck_chiaro);
	    }
	    else {
		riga_entry.setBackgroundResource(R.drawable.settore_merceologico_entry_bck_scuro);
	    }
	    final String category = settori_luce_list.get(i);
	    TextView riga_text = (TextView) riga.findViewById(R.id.settore_merceologico_riga_text);
	    riga_text.setText(category);
	    final String riferimento = getRiferimento(settori_luce_list.get(i));

	    riga_entry.setOnClickListener(new View.OnClickListener() {
		public void onClick(View v) {

		    animation = Utilities.animation(SettoreMerceologicoLuce.this);
		    imageAnim = (ImageView) findViewById(R.id.waiting_anim);
		    imageAnim.setVisibility(View.VISIBLE);
		    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
			imageAnim.setBackgroundDrawable(animation);
		    }
		    else {
			imageAnim.setBackground(animation);
		    }
		    animation.start();

		    Intent intentConsumiLuce = new Intent(getApplicationContext(), ConsumiLuce.class);
		    intentConsumiLuce.putExtra("Classificazione", getIntent().getStringExtra("Classificazione"));
		    intentConsumiLuce.putExtra("PERIODO", getIntent().getIntExtra("PERIODO", Constants.RETAIL_PERIODO_MESI_6));
		    intentConsumiLuce.putExtra("RIFERIMENTO", riferimento);
		    intentConsumiLuce.putExtra("CATEGORY", category);
		    intentConsumiLuce.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
		    intentConsumiLuce.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
		    intentConsumiLuce.putExtra("autoletturaLuce", false);
		    SettoreMerceologicoLuce.this.finish();
		    startActivity(intentConsumiLuce);
		    overridePendingTransition(R.anim.fade, R.anim.hold);
		}
	    });
	    pager.addView(riga);
	}
    }

    private String getRiferimento(String categoryString) {
	String result = "";
	if (categoryString.equals(getResources().getString(R.string.settore_merceologico_luce_category_1)) || categoryString.equals(getResources().getString(R.string.settore_merceologico_luce_category_2)) || categoryString.equals(getResources().getString(R.string.settore_merceologico_luce_category_7)) || categoryString.equals(getResources().getString(R.string.settore_merceologico_luce_category_10)) || categoryString.equals(getResources().getString(R.string.settore_merceologico_luce_category_12))) {
	    result = Constants.PMI_RIFERIMENTO_BASELOAD;
	}
	else if (categoryString.equals(getResources().getString(R.string.settore_merceologico_luce_category_3)) || categoryString.equals(getResources().getString(R.string.settore_merceologico_luce_category_4)) || categoryString.equals(getResources().getString(R.string.settore_merceologico_luce_category_5)) || categoryString.equals(getResources().getString(R.string.settore_merceologico_luce_category_6)) || categoryString.equals(getResources().getString(R.string.settore_merceologico_luce_category_8)) || categoryString.equals(getResources().getString(R.string.settore_merceologico_luce_category_14)) || categoryString.equals(getResources().getString(R.string.settore_merceologico_luce_category_15)) || categoryString.equals(getResources().getString(R.string.settore_merceologico_luce_category_16))) {
	    result = Constants.PMI_RIFERIMENTO_PEAK;
	}
	else if (categoryString.equals(getResources().getString(R.string.settore_merceologico_luce_category_9)) || categoryString.equals(getResources().getString(R.string.settore_merceologico_luce_category_11)) || categoryString.equals(getResources().getString(R.string.settore_merceologico_luce_category_13))) {

	    result = Constants.PMI_RIFERIMENTO_OFFPEAK;
	}
	return result;
    }

    public void onBackPressed() {

	animation = Utilities.animation(this);
	imageAnim = (ImageView) findViewById(R.id.waiting_anim);
	imageAnim.setVisibility(View.VISIBLE);
	if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
	    imageAnim.setBackgroundDrawable(animation);
	}
	else {
	    imageAnim.setBackground(animation);
	}
	animation.start();

	Intent intentConsumiLuce = new Intent(getApplicationContext(), ConsumiLuce.class);
	intentConsumiLuce.putExtra("Classificazione", getIntent().getStringExtra("Classificazione"));
	intentConsumiLuce.putExtra("PERIODO", getIntent().getIntExtra("PERIODO", Constants.RETAIL_PERIODO_MESI_6));
	intentConsumiLuce.putExtra("RIFERIMENTO", getIntent().getStringExtra("RIFERIMENTO"));
	intentConsumiLuce.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
	intentConsumiLuce.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
	intentConsumiLuce.putExtra("CATEGORY", getIntent().getStringExtra("CATEGORY"));
	intentConsumiLuce.putExtra("autoletturaLuce", false);
	SettoreMerceologicoLuce.this.finish();
	startActivity(intentConsumiLuce);
	overridePendingTransition(R.anim.fade, R.anim.hold);
    }
}
