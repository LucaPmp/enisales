package com.eni.enigaseluce.consumiluce;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.http.cookie.Cookie;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.graphics.drawable.AnimationDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.eni.enigaseluce.EniApplication;
import com.eni.enigaseluce.R;
import com.eni.enigaseluce.consumiluce.link.CheckUpEnergetico;
import com.eni.enigaseluce.consumiluce.link.OpzioneBioraria;
import com.eni.enigaseluce.error.OopsPageConsumiLuceFromConsumiLuce;
import com.eni.enigaseluce.font.EniFont;
import com.eni.enigaseluce.home.Home;
import com.eni.enigaseluce.httpcall.Constants;
import com.eni.enigaseluce.httpcall.ServiceCall;
import com.eni.enigaseluce.httpcall.ServiceCallFactory;
import com.eni.enigaseluce.login.help.TrovaEni;
import com.eni.enigaseluce.utils.Utilities;

@SuppressLint("NewApi")
public class ConsumiLuce extends Activity {

    private Context context;
    private static Context staticContext;
    AnimationDrawable animation;
    ImageView imageAnim;

    static int positionSpinnerCurrentSelected;
    static int positionSpinnerPrecedentSelected;
    static int positionSpinnerRiprovaSelected;

    static ScrollView pager;
    static LayoutInflater inflater;
    static LayoutInflater inflaterDialog;

    String primoConto;
    String classificazione;
    String riferimento;
    int periodo;
    String category;
    String[] arrayMesi = {"Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre"};

    public String rispostaLogin = Constants.SERVIZIO_LOGIN;
    public String rispostaConsumiLuce = Constants.SERVIZIO_CONSUMI_LUCE;

    public static String nomeInteroCliente = "";
    private String array_conti_spinner[];

    private int coloreGrafico;

    SharedPreferences settings;
    SharedPreferences.Editor editor;
    boolean autoletturaLuce;
    boolean infoAutolettura = true;

    private RelativeLayout tendinaRigaMini, tendinaRigaMidi, tendinaRigaMaxi, tendinaChooseRetail;
    private RelativeLayout tendinaRiga6, tendinaRiga12, tendinaRiga24, tendinaChoosePeriodo;
    private RelativeLayout semaforo_verde_retail, semaforo_giallo_retail, semaforo_rosso_retail, semaforo_verde_pmi, semaforo_giallo_pmi, semaforo_rosso_pmi;
    private ImageView choose_periodo_selected, tendina_consumi_riferimento_selected;
    private View consumi_luce_entry;

    public static Context getContext() {
        return ConsumiLuce.staticContext;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.consumi_luce);

        ((EniApplication)getApplication()).sendAnalyticsEvent("[Android] - consumi luce");

        positionSpinnerCurrentSelected = getIntent().getIntExtra("INDICE_SPINNER", 0);
        positionSpinnerPrecedentSelected = getIntent().getIntExtra("INDICE_SPINNER", 0);
        positionSpinnerRiprovaSelected = getIntent().getIntExtra("INDICE_SPINNER", 0);

        context = this;
        ConsumiLuce.staticContext = this;
        classificazione = getIntent().getStringExtra("Classificazione");

        // Restore preferences
        settings = getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
        editor = settings.edit();
        autoletturaLuce = settings.getBoolean("AutoletturaLuce", false);
    }

    @Override
    protected void onStart() {
        super.onStart();
        primoConto = getIntent().getStringExtra("PRIMOCONTO");

        final ImageView indietro = (ImageView) findViewById(R.id.consumi_luce_indietro);
        indietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
        indietro.getBackground().invalidateSelf();
        indietro.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                indietro.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
                indietro.getBackground().invalidateSelf();

                animation = Utilities.animation(ConsumiLuce.this);
                imageAnim = (ImageView) findViewById(R.id.waiting_anim);
                imageAnim.setVisibility(View.VISIBLE);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    imageAnim.setBackgroundDrawable(animation);
                } else {
                    imageAnim.setBackground(animation);
                }
                animation.start();

                Intent intentHome = new Intent(getApplicationContext(), Home.class);

                TimerTask clearingHeapTask;
                final Handler handler = new Handler();
                clearingHeapTask = new TimerTask() {
                    public void run() {
                        handler.post(new Runnable() {
                            public void run() {
                                unbindDrawables(pager);
                                System.gc();
                            }
                        });
                    }
                };
                new Timer().schedule(clearingHeapTask, 250);

                ConsumiLuce.this.finish();
                startActivity(intentHome);
                overridePendingTransition(R.anim.fade, R.anim.hold);
            }
        });

        final ImageView help = (ImageView) findViewById(R.id.consumi_luce_help);
        help.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
        help.getBackground().invalidateSelf();
        help.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                help.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
                help.getBackground().invalidateSelf();

                animation = Utilities.animation(ConsumiLuce.this);
                imageAnim = (ImageView) findViewById(R.id.waiting_anim);
                imageAnim.setVisibility(View.VISIBLE);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    imageAnim.setBackgroundDrawable(animation);
                } else {
                    imageAnim.setBackground(animation);
                }
                animation.start();

                Intent intentHelp = new Intent(getApplicationContext(), ConsumiLuceHelp.class);
                intentHelp.putExtra("ORIGINE", Constants.ORIGINE_CONSUMI_LUCE);
                intentHelp.putExtra("Classificazione", classificazione);
                intentHelp.putExtra("PRIMOCONTO", array_conti_spinner[positionSpinnerCurrentSelected].trim());
                intentHelp.putExtra("INDICE_SPINNER", positionSpinnerCurrentSelected);
                intentHelp.putExtra("CATEGORY", category);
                intentHelp.putExtra("PERIODO", periodo);
                intentHelp.putExtra("RIFERIMENTO", riferimento);

                TimerTask clearingHeapTask;
                final Handler handler = new Handler();
                clearingHeapTask = new TimerTask() {
                    public void run() {
                        handler.post(new Runnable() {
                            public void run() {
                                unbindDrawables(pager);
                                System.gc();
                            }
                        });
                    }
                };
                new Timer().schedule(clearingHeapTask, 250);

                ConsumiLuce.this.finish();
                startActivity(intentHelp);
                overridePendingTransition(R.anim.fade, R.anim.hold);
            }
        });
        final RelativeLayout consumi_luce_navigation_bar = (RelativeLayout) findViewById(R.id.consumi_luce_navigation_bar);
        consumi_luce_navigation_bar.setBackgroundResource(R.drawable.pulsanti_footer_on);
        try {
            final JSONArray servizioLogin = new JSONArray(rispostaLogin);
            int lunghezzaSpinner = numeroCodiciContoLuceDiversiUtilizabiliInteri(servizioLogin);

            JSONObject datiAnagr = servizioLogin.getJSONObject(1);
            // ultimo accesso
            String ultimoaccesso = datiAnagr.getString("UltimoAccesso");
            TextView ultimoAccesso = (TextView) findViewById(R.id.ultimo_accesso);
            // ultimoAccesso.setTextSize(11);
            ultimoAccesso.setText(ultimoaccesso);
            // nome cliente
            String nomeClienteString = datiAnagr.getString("NomeCliente");
            String cognomeClienteString = datiAnagr.getString("CognomeCliente");
            TextView nomeCliente = (TextView) findViewById(R.id.nome_cliente);
            nomeInteroCliente = nomeClienteString + " " + cognomeClienteString;
            nomeCliente.setText(nomeClienteString + " " + cognomeClienteString);

            array_conti_spinner = new String[lunghezzaSpinner];
            array_conti_spinner = arrayCodiciContoLuceDiversiUtilizabiliInteri(servizioLogin, lunghezzaSpinner);
            final Spinner codiciConti = (Spinner) findViewById(R.id.numero_cliente);
            // final ArrayAdapter<String> adapterSpinnerConti = new
            // ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,
            // array_conti_spinner);
            final ArrayAdapter<String> adapterSpinnerConti = new ArrayAdapter<String>(this, R.layout.spinner_dialog_riga, R.id.spinner_riga_text, array_conti_spinner);
            codiciConti.setAdapter(adapterSpinnerConti);
            codiciConti.setPrompt(getResources().getString(R.string.seleziona_il_conto_cliente));
            // codiciConti.setSelection((positionSpinnerCurrentSelected ==
            // positionSpinnerRiprovaSelected)? positionSpinnerCurrentSelected :
            // positionSpinnerRiprovaSelected);
            codiciConti.setSelection(positionSpinnerCurrentSelected);

            ImageView consumi_luce_navigation_bar_1 = (ImageView) findViewById(R.id.consumi_luce_navigation_bar_1);
            consumi_luce_navigation_bar_1.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {

                    animation = Utilities.animation(ConsumiLuce.this);
                    imageAnim = (ImageView) findViewById(R.id.waiting_anim);
                    imageAnim.setVisibility(View.VISIBLE);
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                        imageAnim.setBackgroundDrawable(animation);
                    } else {
                        imageAnim.setBackground(animation);
                    }
                    animation.start();

                    final RelativeLayout consumi_luce_navigation_bar = (RelativeLayout) findViewById(R.id.consumi_luce_navigation_bar);
                    consumi_luce_navigation_bar.setBackgroundResource(R.drawable.pulsanti_footer_valori_riferimento);
                    // devo portare alla pagina di Valori di riferimento
                    Intent intentConsumiLuceValoriRiferimento = new Intent(getApplicationContext(), ConsumiLuceValoriRiferimento.class);
                    intentConsumiLuceValoriRiferimento.putExtra("PROVENIENZA", ConsumiLuce.class.getCanonicalName());
                    intentConsumiLuceValoriRiferimento.putExtra("PERIODO", periodo);
                    intentConsumiLuceValoriRiferimento.putExtra("Classificazione", classificazione);
                    intentConsumiLuceValoriRiferimento.putExtra("RIFERIMENTO", riferimento);
                    intentConsumiLuceValoriRiferimento.putExtra("CATEGORY", category);
                    intentConsumiLuceValoriRiferimento.putExtra("PRIMOCONTO", array_conti_spinner[positionSpinnerCurrentSelected].trim());
                    intentConsumiLuceValoriRiferimento.putExtra("INDICE_SPINNER", positionSpinnerCurrentSelected);
                    intentConsumiLuceValoriRiferimento.putExtra("coloreGrafico", coloreGrafico);

                    TimerTask clearingHeapTask;
                    final Handler handler = new Handler();
                    clearingHeapTask = new TimerTask() {
                        public void run() {
                            handler.post(new Runnable() {
                                public void run() {
                                    unbindDrawables(pager);
                                    System.gc();
                                }
                            });
                        }
                    };
                    new Timer().schedule(clearingHeapTask, 250);

                    ConsumiLuce.this.finish();
                    startActivity(intentConsumiLuceValoriRiferimento);
                    overridePendingTransition(R.anim.fade, R.anim.hold);

                }
            });

            ImageView consumi_luce_navigation_bar_2 = (ImageView) findViewById(R.id.consumi_luce_navigation_bar_2);
            consumi_luce_navigation_bar_2.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {

                    animation = Utilities.animation(ConsumiLuce.this);
                    imageAnim = (ImageView) findViewById(R.id.waiting_anim);
                    imageAnim.setVisibility(View.VISIBLE);
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                        imageAnim.setBackgroundDrawable(animation);
                    } else {
                        imageAnim.setBackground(animation);
                    }
                    animation.start();

                    final RelativeLayout consumi_luce_navigation_bar = (RelativeLayout) findViewById(R.id.consumi_luce_navigation_bar);
                    consumi_luce_navigation_bar.setBackgroundResource(R.drawable.pulsanti_footer_grafico_consumi);
                    // devo portare alla pagina di Valori di riferimento
                    Intent intentConsumiLuceValoriRiferimento = new Intent(getApplicationContext(), ConsumiLuceGraficoConsumi.class);
                    intentConsumiLuceValoriRiferimento.putExtra("PROVENIENZA", ConsumiLuce.class.getCanonicalName());
                    intentConsumiLuceValoriRiferimento.putExtra("PERIODO", periodo);
                    intentConsumiLuceValoriRiferimento.putExtra("Classificazione", classificazione);
                    intentConsumiLuceValoriRiferimento.putExtra("CATEGORY", category);
                    intentConsumiLuceValoriRiferimento.putExtra("RIFERIMENTO", riferimento);
                    intentConsumiLuceValoriRiferimento.putExtra("PRIMOCONTO", array_conti_spinner[positionSpinnerCurrentSelected].trim());
                    intentConsumiLuceValoriRiferimento.putExtra("INDICE_SPINNER", positionSpinnerCurrentSelected);
                    intentConsumiLuceValoriRiferimento.putExtra("coloreGrafico", coloreGrafico);
                    TimerTask clearingHeapTask;
                    final Handler handler = new Handler();
                    clearingHeapTask = new TimerTask() {
                        public void run() {
                            handler.post(new Runnable() {
                                public void run() {
                                    unbindDrawables(pager);
                                    System.gc();
                                }
                            });
                        }
                    };
                    new Timer().schedule(clearingHeapTask, 250);

                    ConsumiLuce.this.finish();
                    startActivity(intentConsumiLuceValoriRiferimento);
                    overridePendingTransition(R.anim.fade, R.anim.hold);

                }
            });

            pager = (ScrollView) findViewById(R.id.consumi_luce_pager);
            pager.removeAllViews();
            inflater = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);
            inflaterDialog = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);

            final JSONArray arrayConsumiLuce = new JSONArray(rispostaConsumiLuce);
            popolaPaginaContenuto(servizioLogin, arrayConsumiLuce, pager, primoConto.trim());

            if (!autoletturaLuce && infoAutolettura && getIntent().getBooleanExtra("autoletturaLuce", true)) {
                final Dialog dialog = new Dialog(ConsumiLuce.this, android.R.style.Theme_Translucent_NoTitleBar);
                final View autolettura_luce = inflaterDialog.inflate(R.layout.popup_autolettura_luce, null);
                dialog.setContentView(autolettura_luce);
                dialog.setCancelable(true);

                final LinearLayout chiudi = (LinearLayout) dialog.findViewById(R.id.chiudi);
                final CheckBox checkBox = (CheckBox) dialog.findViewById(R.id.checkbox_autolettura);
                chiudi.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        dialog.dismiss();
                        infoAutolettura = false;
                        if (checkBox.isChecked()) {
                            editor.putBoolean("AutoletturaLuce", true);
                            editor.commit();
                        }
                    }
                });
                infoAutolettura = false;
                dialog.show();
            }
            codiciConti.setOnItemSelectedListener(new OnItemSelectedListener() {
                public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                    Object obj = (Object) parent.getSelectedItem();
                    String codiceContoCliente = obj.toString().trim();
                    if (!codiceContoCliente.equals(adapterSpinnerConti.getItem(positionSpinnerCurrentSelected).toString().trim())) {
                        positionSpinnerPrecedentSelected = positionSpinnerCurrentSelected;
                        positionSpinnerCurrentSelected = codiciConti.getLastVisiblePosition();
                        positionSpinnerRiprovaSelected = codiciConti.getLastVisiblePosition();

                        animation = Utilities.animation(ConsumiLuce.this);
                        imageAnim = (ImageView) findViewById(R.id.waiting_anim);
                        imageAnim.setVisibility(View.VISIBLE);
                        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                            imageAnim.setBackgroundDrawable(animation);
                        } else {
                            imageAnim.setBackground(animation);
                        }
                        animation.start();

                        String parametri = "";
                        String codiceCliente = getCodiceCliente(rispostaLogin);
                        parametri = parametri + "&codiceCliente=" + codiceCliente;
                        String segmento = getSegmentoCliente(rispostaLogin);
                        parametri = parametri + "&segmento=" + segmento;
                        String tipologiaPdfRegioneProdottoDataattivazione = getTipologiaPdfRegioneProdottoDataAttivazione(rispostaLogin, codiceContoCliente, segmento);
                        parametri = parametri + tipologiaPdfRegioneProdottoDataattivazione;
                        if (!isNetworkAvailable(context)) {
                            Intent intentOops = new Intent(getApplicationContext(), OopsPageConsumiLuceFromConsumiLuce.class);
                            intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.connessione_assente));
                            intentOops.putExtra("PARAMETER", parametri);
                            positionSpinnerCurrentSelected = positionSpinnerPrecedentSelected;
                            startActivity(intentOops);
                            if (animation != null) {
                                animation.stop();
                                imageAnim.setVisibility(View.INVISIBLE);
                            }
                            overridePendingTransition(R.anim.fade, R.anim.hold);
                        } else {
                            /********************* Chiamata consumi luce ******************************/
                            ServiceCall consumiLuceServiceCall = ServiceCallFactory.createServiceCall(context);
                            // consumiLuceServiceCall.myCookieStore = new
                            // PersistentCookieStore(context);

                            try {
                                consumiLuceServiceCall.executeHttpsGetConsumiLuce(Constants.URL_CONSUMI_LUCE, parametri, ConsumiLuce.this, null);
                            } catch (Exception e) {
                                Intent intentOops = new Intent(getApplicationContext(), OopsPageConsumiLuceFromConsumiLuce.class);
                                intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
                                intentOops.putExtra("PARAMETER", parametri);
                                positionSpinnerCurrentSelected = positionSpinnerPrecedentSelected;
                                startActivity(intentOops);
                                overridePendingTransition(R.anim.fade, R.anim.hold);
                            }
                        }
                    }
                }

                public void onNothingSelected(AdapterView<?> arg0) {
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void popolaPaginaContenuto(final JSONArray consumiLuceLogin, final JSONArray consumiLuce, ScrollView pager, final String primoConto) {
        // try {
        pager.removeAllViews();

        periodo = getIntent().getIntExtra("PERIODO", Constants.RETAIL_PERIODO_MESI_6);

        consumi_luce_entry = inflater.inflate(R.layout.consumi_luce_entry, null);

        LinearLayout sezione_verifica_consumi_retail = (LinearLayout) consumi_luce_entry.findViewById(R.id.sezione_verifica_consumi_retail);
        LinearLayout sezione_verifica_consumi_pmi = (LinearLayout) consumi_luce_entry.findViewById(R.id.sezione_verifica_consumi_pmi);

        semaforo_verde_retail = (RelativeLayout) consumi_luce_entry.findViewById(R.id.semaforo_verde_contenuto_retail);
        semaforo_giallo_retail = (RelativeLayout) consumi_luce_entry.findViewById(R.id.semaforo_giallo_contenuto_retail);
        semaforo_rosso_retail = (RelativeLayout) consumi_luce_entry.findViewById(R.id.semaforo_rosso_contenuto_retail);

        semaforo_verde_pmi = (RelativeLayout) consumi_luce_entry.findViewById(R.id.semaforo_verde_contenuto_pmi);
        semaforo_giallo_pmi = (RelativeLayout) consumi_luce_entry.findViewById(R.id.semaforo_giallo_contenuto_pmi);
        semaforo_rosso_pmi = (RelativeLayout) consumi_luce_entry.findViewById(R.id.semaforo_rosso_contenuto_pmi);

        choose_periodo_selected = (ImageView) consumi_luce_entry.findViewById(R.id.choose_periodo_selected);
        tendinaChooseRetail = (RelativeLayout) consumi_luce_entry.findViewById(R.id.tendina_choose_retail);
        tendinaRigaMini = (RelativeLayout) consumi_luce_entry.findViewById(R.id.tendina_riga_mini);
        tendinaRigaMidi = (RelativeLayout) consumi_luce_entry.findViewById(R.id.tendina_riga_midi);
        tendinaRigaMaxi = (RelativeLayout) consumi_luce_entry.findViewById(R.id.tendina_riga_maxi);

        tendinaChoosePeriodo = (RelativeLayout) consumi_luce_entry.findViewById(R.id.tendina_choose_periodo);
        tendinaRiga6 = (RelativeLayout) consumi_luce_entry.findViewById(R.id.tendina_riga_6);
        tendinaRiga12 = (RelativeLayout) consumi_luce_entry.findViewById(R.id.tendina_riga_12);
        tendinaRiga24 = (RelativeLayout) consumi_luce_entry.findViewById(R.id.tendina_riga_24);

        tendinaRigaMini.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tendinaRigaClick(1, 1);
            }
        });

        tendinaRigaMidi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tendinaRigaClick(2, 1);
            }
        });

        tendinaRigaMaxi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tendinaRigaClick(3, 1);
            }
        });

        tendinaRiga6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tendinaRigaClick(1, 2);
            }
        });

        tendinaRiga12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tendinaRigaClick(2, 2);
            }
        });

        tendinaRiga24.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tendinaRigaClick(3, 2);
            }
        });


        if (classificazione.equals("RETAIL")) {
            sezione_verifica_consumi_retail.setVisibility(View.VISIBLE);
            sezione_verifica_consumi_pmi.setVisibility(View.GONE);

            TextView consumi_luce_fornitura_valore = (TextView) consumi_luce_entry.findViewById(R.id.consumi_luce_fornitura_retail_valore);
            String fornitura = getPdfFromCodiceConto(rispostaLogin, primoConto);
            consumi_luce_fornitura_valore.setText(fornitura);

            TextView consumi_luce_indirizzo = (TextView) consumi_luce_entry.findViewById(R.id.consumi_luce_retail_indirizzo);
            String indirizzoFornitura = cercaIndirizzoFornituraTipologia(consumiLuceLogin, primoConto, "POWER");
            consumi_luce_indirizzo.setText(indirizzoFornitura);

            riferimento = ((getIntent().getStringExtra("RIFERIMENTO") != null) ? ((getIntent().getStringExtra("RIFERIMENTO"))) : (Constants.RETAIL_RIFERIMENTO_MIDI));

            float consumo_medio_luce = calcolaConsumoMedio(consumiLuce, classificazione, riferimento, periodo);
            // test
            // consumo_medio_luce = (float) 150.0;
            semaforo_verde_pmi.setVisibility(View.GONE);
            semaforo_giallo_pmi.setVisibility(View.GONE);
            semaforo_rosso_pmi.setVisibility(View.GONE);

            popolaSemaforoRetail(consumo_medio_luce);

            // quando click su 'i' deve aparire il popup determinazione retail
            final ImageView info_icon = (ImageView) consumi_luce_entry.findViewById(R.id.info_icon);
            info_icon.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
            info_icon.getBackground().invalidateSelf();
            info_icon.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    info_icon.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
                    info_icon.getBackground().invalidateSelf();
                    final Dialog dialog = new Dialog(ConsumiLuce.this, android.R.style.Theme_Translucent_NoTitleBar);
                    final View det_retail = inflaterDialog.inflate(R.layout.popup_determinare_retail, null);
                    dialog.setContentView(det_retail);

                    dialog.setCancelable(true);

                    dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        public void onDismiss(DialogInterface dialog) {
                            info_icon.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
                            info_icon.getBackground().invalidateSelf();
                        }
                    });

                    final LinearLayout chiudi = (LinearLayout) dialog.findViewById(R.id.chiudi);
                    chiudi.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                    dialog.show();

                }
            });

            tendina_consumi_riferimento_selected = (ImageView) consumi_luce_entry.findViewById(R.id.tendina_consumi_riferimento_selected);
            if (riferimento.equals(Constants.RETAIL_RIFERIMENTO_MINI)) {
                tendina_consumi_riferimento_selected.setBackgroundResource(R.drawable.mini_selected);
            } else if (riferimento.equals(Constants.RETAIL_RIFERIMENTO_MIDI)) {
                tendina_consumi_riferimento_selected.setBackgroundResource(R.drawable.midi_selected);
            } else if (riferimento.equals(Constants.RETAIL_RIFERIMENTO_MAXI)) {
                tendina_consumi_riferimento_selected.setBackgroundResource(R.drawable.maxi_selected);
            }
            tendina_consumi_riferimento_selected.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
            tendina_consumi_riferimento_selected.getBackground().invalidateSelf();
            tendina_consumi_riferimento_selected.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    tendinaChooseRetail.setVisibility(tendinaChooseRetail.getVisibility() == View.GONE ? View.VISIBLE : View.GONE);
                }
            });

            if (periodo == Constants.RETAIL_PERIODO_MESI_6) {
                choose_periodo_selected.setBackgroundResource(R.drawable.choose_6_mesi);
            } else if (periodo == Constants.RETAIL_PERIODO_MESI_12) {
                choose_periodo_selected.setBackgroundResource(R.drawable.choose_12_mesi);
            } else if (periodo == Constants.RETAIL_PERIODO_MESI_24) {
                choose_periodo_selected.setBackgroundResource(R.drawable.choose_24_mesi);
            }
            choose_periodo_selected.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
            choose_periodo_selected.getBackground().invalidateSelf();
            choose_periodo_selected.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    tendinaChoosePeriodo.setVisibility(tendinaChoosePeriodo.getVisibility() == View.GONE ? View.VISIBLE : View.GONE);
                }
            });

            popolaGauge(consumi_luce_entry, consumiLuce, classificazione, periodo);

        } else if (classificazione.equals("PMI")) {
            riferimento = ((getIntent().getStringExtra("RIFERIMENTO") != null) ? ((getIntent().getStringExtra("RIFERIMENTO"))) : (Constants.PMI_RIFERIMENTO_BASELOAD));
            sezione_verifica_consumi_retail.setVisibility(View.GONE);
            sezione_verifica_consumi_pmi.setVisibility(View.VISIBLE);

            TextView consumi_luce_fornitura_valore = (TextView) consumi_luce_entry.findViewById(R.id.consumi_luce_fornitura_pmi_valore);
            String fornitura = getPdfFromCodiceConto(rispostaLogin, primoConto);
            consumi_luce_fornitura_valore.setText(fornitura);

            TextView consumi_luce_indirizzo = (TextView) consumi_luce_entry.findViewById(R.id.consumi_luce_pmi_indirizzo);
            String indirizzoFornitura = cercaIndirizzoFornituraTipologia(consumiLuceLogin, primoConto, "POWER");
            consumi_luce_indirizzo.setText(indirizzoFornitura);

            float consumo_medio_luce = calcolaConsumoMedio(consumiLuce, classificazione, riferimento, periodo);
            // test
            semaforo_verde_retail.setVisibility(View.GONE);
            semaforo_giallo_retail.setVisibility(View.GONE);
            semaforo_rosso_retail.setVisibility(View.GONE);

            popolaSemaforoPmi(consumo_medio_luce);

            EniFont settore_merceologico_selected = (EniFont) consumi_luce_entry.findViewById(R.id.settore_merceologico_selected);
            category = ((getIntent().getStringExtra("CATEGORY") != null) ? getIntent().getStringExtra("CATEGORY") : getResources().getString(R.string.settore_merceologico_luce_category_10));
            settore_merceologico_selected.setText(category);
            // quando click su 'i' deve aparire il popup determinazione retail
            final ImageView sett_merceologico_icon = (ImageView) consumi_luce_entry.findViewById(R.id.sett_merceologico_icon);
            sett_merceologico_icon.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
            sett_merceologico_icon.getBackground().invalidateSelf();
            sett_merceologico_icon.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    sett_merceologico_icon.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
                    sett_merceologico_icon.getBackground().invalidateSelf();
                    Intent intentSettoreMerceologico = new Intent(getApplicationContext(), SettoreMerceologicoLuce.class);
                    intentSettoreMerceologico.putExtra("Classificazione", classificazione);
                    intentSettoreMerceologico.putExtra("PRIMOCONTO", array_conti_spinner[positionSpinnerCurrentSelected].trim());
                    intentSettoreMerceologico.putExtra("INDICE_SPINNER", positionSpinnerCurrentSelected);
                    intentSettoreMerceologico.putExtra("PERIODO", periodo);
                    intentSettoreMerceologico.putExtra("RIFERIMENTO", riferimento);
                    intentSettoreMerceologico.putExtra("CATEGORY", category);
                    ConsumiLuce.this.finish();
                    startActivity(intentSettoreMerceologico);
                    overridePendingTransition(R.anim.fade, R.anim.hold);
                }
            });
            if (periodo == Constants.RETAIL_PERIODO_MESI_6) {
                choose_periodo_selected.setBackgroundResource(R.drawable.choose_6_mesi);
            } else if (periodo == Constants.RETAIL_PERIODO_MESI_12) {
                choose_periodo_selected.setBackgroundResource(R.drawable.choose_12_mesi);
            } else if (periodo == Constants.RETAIL_PERIODO_MESI_24) {
                choose_periodo_selected.setBackgroundResource(R.drawable.choose_24_mesi);
            }
            choose_periodo_selected.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
            choose_periodo_selected.getBackground().invalidateSelf();
            choose_periodo_selected.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    tendinaChoosePeriodo.setVisibility(tendinaChoosePeriodo.getVisibility() == View.GONE ? View.VISIBLE : View.GONE);
                }
            });
            popolaGauge(consumi_luce_entry, consumiLuce, classificazione, periodo);
        }

        pager.addView(consumi_luce_entry);
        // } catch (JSONException e) {
        // e.printStackTrace();
        // }
    }

    private int arrotondaFloat(float numero) {
        int result = 0;

        result = (int) (numero + 0.5);
        return result;
    }

    private boolean getAttivaBioraria(JSONArray arrayConsumiLuce) {
        boolean result = false;
        try {
            JSONObject attivaBioraria = arrayConsumiLuce.getJSONObject(1);
            result = attivaBioraria.getBoolean("attivaBioraria");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }


    private void popolaGauge(View consumi_luce_entry, final JSONArray arrayConsumiLuce, final String classificazione, final int periodo) {
        RelativeLayout sezione_grafico_fascia_consumo_retail = (RelativeLayout) consumi_luce_entry.findViewById(R.id.sezione_grafico_fascia_consumo_retail);
        RelativeLayout sezione_grafico_fascia_consumo_pmi = (RelativeLayout) consumi_luce_entry.findViewById(R.id.sezione_grafico_fascia_consumo_pmi);
        if (classificazione.equals("RETAIL") ) {
            sezione_grafico_fascia_consumo_retail.setVisibility(View.VISIBLE);
            sezione_grafico_fascia_consumo_pmi.setVisibility(View.GONE);

            float consumiFasciaF1 = calcolaConsumiFascia(arrayConsumiLuce, classificazione, periodo, "F1");
            float consumiFasciaF23 = calcolaConsumiFascia(arrayConsumiLuce, classificazione, periodo, "F23");
            int sommaFascie = (int) (consumiFasciaF1 + consumiFasciaF23);
            int percentualeFasciaF1 = ((sommaFascie != 0) ? arrotondaFloat(((consumiFasciaF1 * 100) / sommaFascie)) : 0);
            int percentualeFasciaF23 = ((sommaFascie != 0) ? arrotondaFloat(((consumiFasciaF23 * 100) / sommaFascie)) : 0);
            RelativeLayout sezione_grafico_fascia_consumo_retail_f1 = (RelativeLayout) consumi_luce_entry.findViewById(R.id.sezione_grafico_fascia_consumo_retail_f1);
            RelativeLayout sezione_grafico_fascia_consumo_retail_f23 = (RelativeLayout) consumi_luce_entry.findViewById(R.id.sezione_grafico_fascia_consumo_retail_f23);

            final RelativeLayout box_fascia_consumo = (RelativeLayout) consumi_luce_entry.findViewById(R.id.box_fascia_consumo);
            box_fascia_consumo.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
            box_fascia_consumo.getBackground().invalidateSelf();
            //TextView box_fascia_consumo_a_capo = (TextView) consumi_luce_entry.findViewById(R.id.box_fascia_consumo_a_capo);

            if (percentualeFasciaF23 > 67) {
                sezione_grafico_fascia_consumo_retail_f1.setVisibility(View.GONE);
                sezione_grafico_fascia_consumo_retail_f23.setVisibility(View.VISIBLE);
                TextView percentuale_retail_f23 = (TextView) consumi_luce_entry.findViewById(R.id.percentuale_retail_f23);
                percentuale_retail_f23.setText("" + percentualeFasciaF23 + getResources().getString(R.string.percentuale));

                boolean attivaBioraria = getAttivaBioraria(arrayConsumiLuce);

                if (attivaBioraria) {
                    box_fascia_consumo.setVisibility(View.VISIBLE);
                    ((TextView) consumi_luce_entry.findViewById(R.id.box_fascia_consumo_text_retail)).setText(Html.fromHtml("Il tuo consumo totale in fascia <b>F23</b> &#232; circa del <b>" + percentualeFasciaF23 + getResources().getString(R.string.percentuale) + ". Ti consigliamo di attivare la Bioraria</b>"));
                    //((TextView) consumi_luce_entry.findViewById(R.id.box_fascia_consumo_text_retail)).setText(String.format(getString(R.string.box_fascia_consumo_text_retail), percentualeFasciaF23 + getResources().getString(R.string.percentuale)));
                    //TextView text_fascia_consumo_4 = (TextView) consumi_luce_entry.findViewById(R.id.text_fascia_consumo_4);
                    //text_fascia_consumo_4.setText("" + percentualeFasciaF23 + getResources().getString(R.string.percentuale));
                    //box_fascia_consumo_a_capo.setVisibility(View.VISIBLE);
                    box_fascia_consumo.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            box_fascia_consumo.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
                            box_fascia_consumo.getBackground().invalidateSelf();
                            final Dialog dialog = new Dialog(ConsumiLuce.this, android.R.style.Theme_Translucent_NoTitleBar);
                            dialog.setContentView(R.layout.popup_opzione_bioraria);
                            dialog.setCancelable(true);

                            final LinearLayout chiudi = (LinearLayout) dialog.findViewById(R.id.chiudi);
                            chiudi.setOnClickListener(new View.OnClickListener() {
                                public void onClick(View v) {
                                    dialog.dismiss();
                                }
                            });
                            final ImageView attiva_bioraria_button = (ImageView) dialog.findViewById(R.id.attiva_bioraria_button);
                            attiva_bioraria_button.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
                            attiva_bioraria_button.getBackground().invalidateSelf();
                            attiva_bioraria_button.setOnClickListener(new View.OnClickListener() {
                                public void onClick(View v) {
                                    attiva_bioraria_button.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
                                    attiva_bioraria_button.getBackground().invalidateSelf();
                                    Intent intentOpzioneBioraria = new Intent(getApplicationContext(), OpzioneBioraria.class);
                                    intentOpzioneBioraria.putExtra("Classificazione", classificazione);
                                    intentOpzioneBioraria.putExtra("PRIMOCONTO", array_conti_spinner[positionSpinnerCurrentSelected].trim());
                                    intentOpzioneBioraria.putExtra("INDICE_SPINNER", positionSpinnerCurrentSelected);
                                    intentOpzioneBioraria.putExtra("PERIODO", periodo);
                                    intentOpzioneBioraria.putExtra("RIFERIMENTO", riferimento);
                                    intentOpzioneBioraria.putExtra("CATEGORY", category);
                                    dialog.dismiss();
                                    ConsumiLuce.this.finish();
                                    startActivity(intentOpzioneBioraria);
                                    overridePendingTransition(R.anim.fade, R.anim.hold);
                                }
                            });
                            dialog.show();
                            box_fascia_consumo.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
                            box_fascia_consumo.getBackground().invalidateSelf();
                        }
                    });
                } else {
                    box_fascia_consumo.setVisibility(View.GONE);
                    //box_fascia_consumo_a_capo.setVisibility(View.GONE);
                }

            } else {
                sezione_grafico_fascia_consumo_retail_f1.setVisibility(View.VISIBLE);
                sezione_grafico_fascia_consumo_retail_f23.setVisibility(View.GONE);
                box_fascia_consumo.setVisibility(View.GONE);
                TextView percentuale_retail_f1 = (TextView) consumi_luce_entry.findViewById(R.id.percentuale_retail_f1);
                percentuale_retail_f1.setText("" + percentualeFasciaF1 + getResources().getString(R.string.percentuale));
            }
        } else if (classificazione.equals("PMI")) {
            sezione_grafico_fascia_consumo_retail.setVisibility(View.GONE);
            sezione_grafico_fascia_consumo_pmi.setVisibility(View.VISIBLE);

            float consumiFasciaF1 = calcolaConsumiFascia(arrayConsumiLuce, classificazione, periodo, "F1");
            float consumiFasciaF2 = calcolaConsumiFascia(arrayConsumiLuce, classificazione, periodo, "F2");
            float consumiFasciaF3 = calcolaConsumiFascia(arrayConsumiLuce, classificazione, periodo, "F3");
            int sommaFascie = (int) (consumiFasciaF1 + consumiFasciaF2 + consumiFasciaF3);
            int percentualeFasciaF1 = ((sommaFascie != 0) ? (arrotondaFloat((consumiFasciaF1 * 100) / sommaFascie)) : 0);
            int percentualeFasciaF2 = ((sommaFascie != 0) ? (arrotondaFloat((consumiFasciaF2 * 100) / sommaFascie)) : 0);
            int percentualeFasciaF3 = ((sommaFascie != 0) ? (arrotondaFloat((consumiFasciaF3 * 100) / sommaFascie)) : 0);

            RelativeLayout sezione_grafico_fascia_consumo_pmi_f1 = (RelativeLayout) consumi_luce_entry.findViewById(R.id.sezione_grafico_fascia_consumo_pmi_f1);
            RelativeLayout sezione_grafico_fascia_consumo_pmi_f2 = (RelativeLayout) consumi_luce_entry.findViewById(R.id.sezione_grafico_fascia_consumo_pmi_f2);
            RelativeLayout sezione_grafico_fascia_consumo_pmi_f3 = (RelativeLayout) consumi_luce_entry.findViewById(R.id.sezione_grafico_fascia_consumo_pmi_f3);
            if ((percentualeFasciaF1 >= percentualeFasciaF2) && (percentualeFasciaF1 >= percentualeFasciaF3)) {
                // f1 maggiore
                sezione_grafico_fascia_consumo_pmi_f1.setVisibility(View.VISIBLE);
                sezione_grafico_fascia_consumo_pmi_f2.setVisibility(View.GONE);
                sezione_grafico_fascia_consumo_pmi_f3.setVisibility(View.GONE);
                TextView percentuale_pmi_f1 = (TextView) consumi_luce_entry.findViewById(R.id.percentuale_pmi_f1);
                percentuale_pmi_f1.setText("" + percentualeFasciaF1 + getResources().getString(R.string.percentuale));
            } else if ((percentualeFasciaF2 > percentualeFasciaF1) && (percentualeFasciaF2 > percentualeFasciaF3)) {
                // f2 maggiore
                sezione_grafico_fascia_consumo_pmi_f1.setVisibility(View.GONE);
                sezione_grafico_fascia_consumo_pmi_f2.setVisibility(View.VISIBLE);
                sezione_grafico_fascia_consumo_pmi_f3.setVisibility(View.GONE);
                TextView percentuale_pmi_f2 = (TextView) consumi_luce_entry.findViewById(R.id.percentuale_pmi_f2);
                percentuale_pmi_f2.setText("" + percentualeFasciaF2 + getResources().getString(R.string.percentuale));
            } else if ((percentualeFasciaF3 > percentualeFasciaF1) && (percentualeFasciaF3 > percentualeFasciaF2)) {
                // f3 maggiore
                sezione_grafico_fascia_consumo_pmi_f1.setVisibility(View.GONE);
                sezione_grafico_fascia_consumo_pmi_f2.setVisibility(View.GONE);
                sezione_grafico_fascia_consumo_pmi_f3.setVisibility(View.VISIBLE);
                TextView percentuale_pmi_f3 = (TextView) consumi_luce_entry.findViewById(R.id.percentuale_pmi_f3);
                percentuale_pmi_f3.setText("" + percentualeFasciaF3 + getResources().getString(R.string.percentuale));
            }
        }

    }

    private void popolaSemaforoPmi(float consumo_medio_luce) {
        if (consumo_medio_luce <= 120) {
            // verde
            semaforo_verde_pmi.setVisibility(View.VISIBLE);
            semaforo_giallo_pmi.setVisibility(View.GONE);
            semaforo_rosso_pmi.setVisibility(View.GONE);
            coloreGrafico = 0;
        } else if (consumo_medio_luce > 120 && consumo_medio_luce <= 140) {
            // giallo
            semaforo_verde_pmi.setVisibility(View.GONE);
            semaforo_giallo_pmi.setVisibility(View.VISIBLE);
            semaforo_rosso_pmi.setVisibility(View.GONE);
            coloreGrafico = 1;
        } else if (consumo_medio_luce > 140) {
            // rosso
            semaforo_verde_pmi.setVisibility(View.GONE);
            semaforo_giallo_pmi.setVisibility(View.GONE);
            semaforo_rosso_pmi.setVisibility(View.VISIBLE);
            coloreGrafico = 2;
        }
    }

    private void popolaSemaforoRetail(float consumo_medio_luce) {
        if (consumo_medio_luce <= 120) {
            // verde
            semaforo_verde_retail.setVisibility(View.VISIBLE);
            semaforo_giallo_retail.setVisibility(View.GONE);
            semaforo_rosso_retail.setVisibility(View.GONE);
            coloreGrafico = 0;
        } else if (consumo_medio_luce > 120 && consumo_medio_luce <= 140) {
            // giallo
            semaforo_verde_retail.setVisibility(View.GONE);
            semaforo_giallo_retail.setVisibility(View.VISIBLE);
            semaforo_rosso_retail.setVisibility(View.GONE);

            final RelativeLayout desc_giallo = (RelativeLayout) consumi_luce_entry.findViewById(R.id.desc_giallo_retail);
            desc_giallo.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
            desc_giallo.getBackground().invalidateSelf();
            desc_giallo.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    desc_giallo.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
                    desc_giallo.getBackground().invalidateSelf();
                    final Dialog dialog = new Dialog(ConsumiLuce.this, android.R.style.Theme_Translucent_NoTitleBar);
                    dialog.setContentView(R.layout.popup_consumi_luce_stato_giallo);
                    dialog.setCancelable(true);

                    TextView titolo = (TextView) dialog.findViewById(R.id.sotto_titolo);
                    titolo.setText(getResources().getString(R.string.consumi_luce_non_in_linea));

                    final LinearLayout chiudi = (LinearLayout) dialog.findViewById(R.id.chiudi);
                    chiudi.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                    ImageView check_up_button = (ImageView) dialog.findViewById(R.id.check_up_button);
                    check_up_button.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            Intent intentCheckUp = new Intent(getApplicationContext(), CheckUpEnergetico.class);
                            intentCheckUp.putExtra("PAGINA_DI_PROVENIENZA", ConsumiLuce.class.getCanonicalName());
                            intentCheckUp.putExtra("Classificazione", classificazione);
                            intentCheckUp.putExtra("PRIMOCONTO", array_conti_spinner[positionSpinnerCurrentSelected].trim());
                            intentCheckUp.putExtra("INDICE_SPINNER", positionSpinnerCurrentSelected);
                            intentCheckUp.putExtra("PERIODO", periodo);
                            intentCheckUp.putExtra("RIFERIMENTO", riferimento);
                            intentCheckUp.putExtra("CATEGORY", category);
                            dialog.dismiss();
                            ConsumiLuce.this.finish();
                            startActivity(intentCheckUp);
                            overridePendingTransition(R.anim.fade, R.anim.hold);
                        }
                    });
                    dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        public void onDismiss(DialogInterface dialog) {
                            desc_giallo.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
                            desc_giallo.getBackground().invalidateSelf();
                        }
                    });
                    dialog.show();

                }
            });
            coloreGrafico = 1;
        } else if (consumo_medio_luce > 140) {
            // rosso
            semaforo_verde_retail.setVisibility(View.GONE);
            semaforo_giallo_retail.setVisibility(View.GONE);
            semaforo_rosso_retail.setVisibility(View.VISIBLE);

            final RelativeLayout desc_rosso = (RelativeLayout) consumi_luce_entry.findViewById(R.id.desc_rosso_retail);
            desc_rosso.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
            desc_rosso.getBackground().invalidateSelf();
            desc_rosso.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    desc_rosso.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
                    desc_rosso.getBackground().invalidateSelf();
                    final Dialog dialog = new Dialog(ConsumiLuce.this, android.R.style.Theme_Translucent_NoTitleBar);
                    dialog.setContentView(R.layout.popup_consumi_luce_stato_rosso);
                    dialog.setCancelable(true);

                    TextView titolo = (TextView) dialog.findViewById(R.id.sotto_titolo);
                    titolo.setText(getResources().getString(R.string.consumi_luce_non_in_linea));

                    final LinearLayout chiudi = (LinearLayout) dialog.findViewById(R.id.chiudi);
                    chiudi.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                    final ImageView goto_energy_store_button = (ImageView) dialog.findViewById(R.id.goto_energy_store_button);
                    goto_energy_store_button.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            goto_energy_store_button.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
                            goto_energy_store_button.getBackground().invalidateSelf();
                            Intent intentEnergyStoreLocator = new Intent(getApplicationContext(), TrovaEni.class);
                            goto_energy_store_button.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
                            goto_energy_store_button.getBackground().invalidateSelf();
                            intentEnergyStoreLocator.putExtra("Classificazione", classificazione);
                            intentEnergyStoreLocator.putExtra("PRIMOCONTO", array_conti_spinner[positionSpinnerCurrentSelected].trim());
                            intentEnergyStoreLocator.putExtra("INDICE_SPINNER", positionSpinnerCurrentSelected);
                            intentEnergyStoreLocator.putExtra("PERIODO", periodo);
                            intentEnergyStoreLocator.putExtra("RIFERIMENTO", riferimento);
                            intentEnergyStoreLocator.putExtra("CATEGORY", category);
                            intentEnergyStoreLocator.putExtra("PAGINA_DI_PROVENIENZA", ConsumiLuce.class.getCanonicalName());
                            dialog.dismiss();
                            ConsumiLuce.this.finish();
                            startActivity(intentEnergyStoreLocator);
                            overridePendingTransition(R.anim.fade, R.anim.hold);
                        }
                    });
                    dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        public void onDismiss(DialogInterface dialog) {
                            desc_rosso.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
                            desc_rosso.getBackground().invalidateSelf();
                        }
                    });
                    dialog.show();

                }
            });
            coloreGrafico = 2;
        }

    }

    private int calcolaLimitePeriodo(JSONArray arrayConsumiLuce, String classificazione, int periodo) {
        int result = 0;
        Calendar now = Calendar.getInstance();
        now.setTime(new Date());
        String meseCorrente = arrayMesi[now.get(Calendar.MONTH)];
        int annoCorrente = now.get(Calendar.YEAR);
        try {
            if (classificazione.equals("RETAIL")) {
                JSONObject consumi = arrayConsumiLuce.getJSONObject(2);
                JSONArray arrayConsumi = consumi.getJSONArray("Consumi");
                JSONObject ultimoObject = arrayConsumi.getJSONObject(arrayConsumi.length() - 1);
                String annoUltimo = ultimoObject.getString("Anno");
                String meseUltimo = ultimoObject.getString("Mese");
                int meseUltimoIndex = getMeseIndex(meseUltimo);
                int annoUltimoInt = Integer.valueOf(annoUltimo);

                if (arrayConsumi.length() >= periodo) {
                    int limite = 0;
                    if (annoUltimoInt == annoCorrente) {
                        if (meseCorrente.equals(meseUltimo)) {
                            limite = periodo;
                        } else {
                            int differenza = now.get(Calendar.MONTH) - meseUltimoIndex;
                            limite = periodo - differenza;
                        }
                    } else if (annoUltimoInt < annoCorrente) {
                        int differenza = 12 - meseUltimoIndex;
                        limite = periodo - differenza;
                    }
                    result = limite;
                } else {
                    int limite = 0;
                    limite = arrayConsumi.length();
                    result = limite;
                }

            } else if (classificazione.equals("PMI")) {
                JSONObject consumi = arrayConsumiLuce.getJSONObject(1);
                JSONArray arrayConsumi = consumi.getJSONArray("Consumi");
                JSONObject ultimoObject = arrayConsumi.getJSONObject(arrayConsumi.length() - 1);
                String annoUltimo = ultimoObject.getString("Anno");
                String meseUltimo = ultimoObject.getString("Mese");
                int meseUltimoIndex = getMeseIndex(meseUltimo);
                int annoUltimoInt = Integer.valueOf(annoUltimo);

                if (arrayConsumi.length() >= periodo) {
                    int limite = 0;
                    if (annoUltimoInt == annoCorrente) {
                        if (meseCorrente.equals(meseUltimo)) {
                            limite = periodo;
                        } else {
                            int differenza = now.get(Calendar.MONTH) - meseUltimoIndex;
                            limite = periodo - differenza;
                        }
                    } else if (annoUltimoInt < annoCorrente) {
                        int differenza = 12 - meseUltimoIndex;
                        limite = periodo - differenza;
                    }
                    result = limite;
                } else {
                    int limite = 0;
                    limite = arrayConsumi.length();
                    result = limite;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    private float calcolaConsumiFascia(JSONArray arrayConsumiLuce, String classificazione, int periodo, String fascia) {
        float result = 0;
        int limite = calcolaLimitePeriodo(arrayConsumiLuce, classificazione, periodo);

        try {
            if (classificazione.equals("RETAIL")) {
                JSONObject consumi = arrayConsumiLuce.getJSONObject(2);
                JSONArray arrayConsumi = consumi.getJSONArray("Consumi");

                // int sommaConsumiFascia = 0;
                for (int i = arrayConsumi.length() - 1; i >= arrayConsumi.length() - limite; i--) {
                    JSONObject consumoIesimo = arrayConsumi.getJSONObject(i);
                    String f1 = consumoIesimo.getString(fascia);
                    result = result + arrotondaFloat(Float.valueOf(f1));
                }
            } else if (classificazione.equals("PMI")) {
                JSONObject consumi = arrayConsumiLuce.getJSONObject(1);
                JSONArray arrayConsumi = consumi.getJSONArray("Consumi");

                // int sommaConsumiFascia = 0;
                for (int i = arrayConsumi.length() - 1; i >= arrayConsumi.length() - limite; i--) {
                    JSONObject consumoIesimo = arrayConsumi.getJSONObject(i);
                    String f1 = consumoIesimo.getString(fascia);
                    result = result + arrotondaFloat(Float.valueOf(f1));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    private float calcolaConsumoMedio(JSONArray arrayConsumiLuce, String classificazione, String riferimento, int periodo) {
        float result = 0;
        int limite = calcolaLimitePeriodo(arrayConsumiLuce, classificazione, periodo);
        try {
            if (classificazione.equals("RETAIL")) {
                JSONObject consumi = arrayConsumiLuce.getJSONObject(2);
                JSONArray arrayConsumi = consumi.getJSONArray("Consumi");

                float sommaConsumiFascie = 0;
                for (int i = arrayConsumi.length() - 1; i >= (arrayConsumi.length() - limite); i--) {
                    JSONObject consumoIesimo = arrayConsumi.getJSONObject(i);
                    String f1 = consumoIesimo.getString("F1");
                    String f23 = consumoIesimo.getString("F23");
                    sommaConsumiFascie = sommaConsumiFascie + arrotondaFloat(Float.valueOf(f1)) + arrotondaFloat(Float.valueOf(f23));
                }

                JSONObject riferimentoObject = arrayConsumiLuce.getJSONObject(3);
                JSONArray arrayRiferimento = riferimentoObject.getJSONArray("Riferimento");
                JSONArray arrayProfilo = new JSONArray();
                if (riferimento.equals(Constants.RETAIL_RIFERIMENTO_MINI)) {
                    JSONObject objectRiferimentoMini = arrayRiferimento.getJSONObject(0);
                    arrayProfilo = objectRiferimentoMini.getJSONArray("RiferimentoMini");
                } else if (riferimento.equals(Constants.RETAIL_RIFERIMENTO_MIDI)) {
                    JSONObject objectRiferimentoMini = arrayRiferimento.getJSONObject(1);
                    arrayProfilo = objectRiferimentoMini.getJSONArray("RiferimentoMidi");
                } else if (riferimento.equals(Constants.RETAIL_RIFERIMENTO_MAXI)) {
                    JSONObject objectRiferimentoMini = arrayRiferimento.getJSONObject(2);
                    arrayProfilo = objectRiferimentoMini.getJSONArray("RiferimentoMaxi");
                }

                float sommaConsumiProfili = 0;
                for (int j = arrayProfilo.length() - 1; j >= (arrayProfilo.length() - limite); j--) {
                    JSONObject consumoProfiloIesimo = arrayProfilo.getJSONObject(j);
                    String consumoProfiloI = consumoProfiloIesimo.getString("Consumo");
                    sommaConsumiProfili = sommaConsumiProfili + arrotondaFloat(Float.valueOf(consumoProfiloI));
                }

                if (sommaConsumiProfili > 0) {
                    result = 100 * (sommaConsumiFascie / sommaConsumiProfili);
                } else {
                    result = 0;
                }
            } else if (classificazione.equals("PMI")) {
                JSONObject consumi = arrayConsumiLuce.getJSONObject(1);
                JSONArray arrayConsumi = consumi.getJSONArray("Consumi");

                float sommaConsumiFascie = 0;
                for (int i = arrayConsumi.length() - 1; i >= (arrayConsumi.length() - limite); i--) {
                    JSONObject consumoIesimo = arrayConsumi.getJSONObject(i);
                    String f1 = consumoIesimo.getString("F1");
                    String f2 = consumoIesimo.getString("F2");
                    String f3 = consumoIesimo.getString("F3");
                    sommaConsumiFascie = sommaConsumiFascie + arrotondaFloat(Float.valueOf(f1)) + arrotondaFloat(Float.valueOf(f2)) + arrotondaFloat(Float.valueOf(f3));
                }

                JSONObject riferimentoObject = arrayConsumiLuce.getJSONObject(2);
                JSONArray arrayRiferimento = riferimentoObject.getJSONArray("Riferimento");
                JSONArray arrayProfilo = new JSONArray();
                if (riferimento.equals(Constants.PMI_RIFERIMENTO_BASELOAD)) {
                    JSONObject objectRiferimentoBaseload = arrayRiferimento.getJSONObject(0);
                    arrayProfilo = objectRiferimentoBaseload.getJSONArray("RiferimentoBaseload");
                } else if (riferimento.equals(Constants.PMI_RIFERIMENTO_PEAK)) {
                    JSONObject objectRiferimentoPeak = arrayRiferimento.getJSONObject(1);
                    arrayProfilo = objectRiferimentoPeak.getJSONArray("RiferimentoPeak");
                } else if (riferimento.equals(Constants.PMI_RIFERIMENTO_OFFPEAK)) {
                    JSONObject objectRiferimentoOffpeak = arrayRiferimento.getJSONObject(2);
                    arrayProfilo = objectRiferimentoOffpeak.getJSONArray("RiferimentoOffPeak");
                }

                float sommaConsumiProfili = 0;
                for (int j = arrayProfilo.length() - 1; j >= (arrayProfilo.length() - limite); j--) {
                    JSONObject consumoProfiloIesimo = arrayProfilo.getJSONObject(j);
                    String consumoProfiloI = consumoProfiloIesimo.getString("Consumo");
                    sommaConsumiProfili = sommaConsumiProfili + arrotondaFloat(Float.valueOf(consumoProfiloI));
                }
                if (sommaConsumiProfili > 0) {
                    result = 100 * (sommaConsumiFascie / sommaConsumiProfili);
                } else {
                    result = 0;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    private int getMeseIndex(String mese) {
        int result = -1;
        for (int i = 0; i < arrayMesi.length; i++) {
            if (arrayMesi[i].equals(mese)) {
                result = i;
            }
        }
        return result;
    }

    private String cercaIndirizzoFornituraTipologia(JSONArray array, String codiceConto, String tipologia) {
        String result = "";
        String codiceContoIesimo = "";
        JSONObject anagrContoIesimo = new JSONObject();
        try {
            for (int i = 2; i < array.length(); i++) {
                JSONArray contoIesimo;

                contoIesimo = array.getJSONArray(i);
                // codice conto
                anagrContoIesimo = contoIesimo.getJSONObject(0);
                codiceContoIesimo = anagrContoIesimo.getString("CodiceConto");

                if (!codiceContoIesimo.equals(codiceConto)) {
                    // codice conto diverso
                    continue;
                } else {
                    // codice conto utilizzabile diverso, controllo non sia
                    // vuoto
                    if (contoIesimo.length() < 2) {
                        continue;
                    } else {
                        JSONObject anagrExtraContoIesimo = contoIesimo.getJSONObject(1);
                        String tipologiaConto = anagrExtraContoIesimo.getString("Tipologia");
                        if (!tipologiaConto.equals(tipologia)) {
                            // codice conto diverso
                            continue;
                        } else {
                            result = anagrExtraContoIesimo.getString("IndirizzoCompleto");
                        }
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public int numeroCodiciContoLuceDiversiUtilizabiliInteri(JSONArray array) {
        int result = 0;
        String codiceContoIesimo = "";
        String codiciConcatenati = "";
        for (int i = 2; i < array.length(); i++) {
            JSONArray contoIesimo;
            try {
                contoIesimo = array.getJSONArray(i);
                // codice conto
                JSONObject anagrContoIesimo = contoIesimo.getJSONObject(0);
                if (contoIesimo.length() < 2) {
                    continue;
                } else {
                    String stato = anagrContoIesimo.getString("StatoConto");
                    if (!stato.equals("UTILIZZABILE")) {
                        continue;
                    } else {
                        JSONObject anagrExtraContoIesimo = contoIesimo.getJSONObject(1);
                        String tipologia = anagrExtraContoIesimo.getString("Tipologia");
                        if (!tipologia.equals("POWER")) {
                            continue;
                        } else {
                            // trovato conto luce utilizzabile pieno
                            codiceContoIesimo = anagrContoIesimo.getString("CodiceConto");
                            boolean isPresente = codiciConcatenati.contains("-" + codiceContoIesimo + "-");
                            if (isPresente) {
                                continue;
                            } else {
                                codiciConcatenati = codiciConcatenati + "-" + codiceContoIesimo + "-";
                                result++;
                            }
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public String[] arrayCodiciContoLuceDiversiUtilizabiliInteri(JSONArray array, int lunghezza) {
        String result[] = new String[lunghezza];
        String codiceContoIesimo = "";
        String codiciConcatenati = "";
        int count = 0;
        for (int i = 2; i < array.length(); i++) {
            JSONArray contoIesimo;
            try {
                contoIesimo = array.getJSONArray(i);
                // codice conto
                JSONObject anagrContoIesimo = contoIesimo.getJSONObject(0);
                if (contoIesimo.length() < 2) {
                    continue;
                } else {
                    String stato = anagrContoIesimo.getString("StatoConto");
                    if (!stato.equals("UTILIZZABILE")) {
                        continue;
                    } else {
                        JSONObject anagrExtraContoIesimo = contoIesimo.getJSONObject(1);
                        String tipologia = anagrExtraContoIesimo.getString("Tipologia");
                        if (!tipologia.equals("POWER")) {
                            continue;
                        } else {
                            // trovato conto luce utilizzabile pieno
                            codiceContoIesimo = anagrContoIesimo.getString("CodiceConto");
                            boolean isPresente = codiciConcatenati.contains("-" + codiceContoIesimo + "-");
                            if (isPresente) {
                                continue;
                            } else {
                                result[count] = "  " + codiceContoIesimo;
                                codiciConcatenati = codiciConcatenati + "-" + codiceContoIesimo + "-";
                                count++;
                            }
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public String getCodiceCliente(String rispostaLogin) {
        String result = "";
        try {
            JSONArray loginJson = new JSONArray(rispostaLogin);
            JSONObject datiCliente = (JSONObject) loginJson.get(1);
            result = datiCliente.getString("CodiceCliente");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public String getSegmentoCliente(String rispostaLogin) {
        String result = "";
        try {
            JSONArray loginJson = new JSONArray(rispostaLogin);
            JSONObject datiCliente = (JSONObject) loginJson.get(1);
            result = datiCliente.getString("SegmentoCliente");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public String getTipologiaPdfRegioneProdottoDataAttivazione(String stringLogin, String codiceContoLuce, String segmento) {
        String result = "";
        JSONObject anagrContoIesimo = new JSONObject();
        JSONObject anagrExtraContoIesimo = new JSONObject();
        try {
            JSONArray arrayLogin = new JSONArray(stringLogin);
            JSONObject anagrCliente = arrayLogin.getJSONObject(1);
            String tipologia = anagrCliente.getString("TipologiaCliente");
            result = result + "&tipologia=" + tipologia;
            for (int i = 2; i < arrayLogin.length(); i++) {
                JSONArray contoIesimo;
                contoIesimo = arrayLogin.getJSONArray(i);
                anagrContoIesimo = contoIesimo.getJSONObject(0);
                String codiceConto = anagrContoIesimo.getString("CodiceConto");
                if (!codiceConto.equals(codiceContoLuce)) {
                    continue;
                } else {
                    if (contoIesimo.length() < 2) {
                        continue;
                    } else {
                        anagrExtraContoIesimo = contoIesimo.getJSONObject(1);
                        String tipologiaLuceGase = anagrExtraContoIesimo.getString("Tipologia");
                        if (!tipologiaLuceGase.equals("POWER")) {
                            continue;
                        } else {
                            // trovato il conto
                            // tipologia, pdf, regione, prodotto,
                            // dataAttivazione
                            String pdf = anagrExtraContoIesimo.getString("Pdf");
                            result = result + "&pdf=" + pdf;
                            String regione = anagrExtraContoIesimo.getString("Regione");
                            result = result + "&regione=" + regione;
                            String prodotto = getProdotto(stringLogin, codiceContoLuce);
                            if (!prodotto.equals("") && (tipologia.equals("RETAIL") && segmento.equals("RESIDENTIAL"))) {
                                result = result + "&codiceProdotto=" + prodotto;
                            }
                            String dataAttivazione = getDataAttivazione(stringLogin, codiceContoLuce);
                            if (!dataAttivazione.equals("")) {
                                result = result + "&dataAttivazione=" + dataAttivazione;
                            }
                        }
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public String getProdotto(String stringLogin, String codiceContoLuce) {
        String result = "";
        JSONObject anagrContoIesimo = new JSONObject();
        JSONObject anagrExtraContoIesimo = new JSONObject();
        try {
            JSONArray arrayLogin = new JSONArray(stringLogin);
            for (int i = 2; i < arrayLogin.length(); i++) {
                JSONArray contoIesimo;
                contoIesimo = arrayLogin.getJSONArray(i);
                anagrContoIesimo = contoIesimo.getJSONObject(0);
                if (contoIesimo.length() < 2) {
                    continue;
                } else {
                    anagrExtraContoIesimo = contoIesimo.getJSONObject(1);
                    String codiceConto = anagrContoIesimo.getString("CodiceConto");
                    if (!codiceConto.equals(codiceContoLuce)) {
                        continue;
                    } else {
                        // trovato il conto
                        result = anagrExtraContoIesimo.getString("CodiceProdotto");
                        result = result.replaceAll(" ", "%20");
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public String getDataAttivazione(String stringLogin, String codiceContoLuce) {
        String result = "";
        JSONObject anagrContoIesimo = new JSONObject();
        JSONObject anagrExtraContoIesimo = new JSONObject();
        try {
            JSONArray arrayLogin = new JSONArray(stringLogin);
            for (int i = 2; i < arrayLogin.length(); i++) {
                JSONArray contoIesimo;
                contoIesimo = arrayLogin.getJSONArray(i);
                anagrContoIesimo = contoIesimo.getJSONObject(0);
                if (contoIesimo.length() < 2) {
                    continue;
                } else {
                    anagrExtraContoIesimo = contoIesimo.getJSONObject(1);
                    String codiceConto = anagrContoIesimo.getString("CodiceConto");
                    if (!codiceConto.equals(codiceContoLuce)) {
                        continue;
                    } else {
                        // trovato il conto
                        result = anagrExtraContoIesimo.getString("DataAttivazione");
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    private boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager == null) return false;
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }

    public void postChiamata(String rispostaChiamata, List<Cookie> lista, String parametri) {
        if (animation != null) {
            animation.stop();
            imageAnim.setVisibility(View.INVISIBLE);
        }

        final ImageView indietro = (ImageView) findViewById(R.id.consumi_luce_indietro);
        indietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
        indietro.getBackground().invalidateSelf();

        JSONArray rispostaJSON = new JSONArray();
        String esito = "";
        String descEsito = "";
        try {
            rispostaJSON = new JSONArray(rispostaChiamata);
            JSONObject oggettoRisposta = (JSONObject) rispostaJSON.get(0);
            esito = oggettoRisposta.getString("esito");
            descEsito = oggettoRisposta.getString("descEsito");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (esito.equals("200")) {
            Constants.SERVIZIO_CONSUMI_LUCE = rispostaChiamata;
            rispostaConsumiLuce = rispostaChiamata;
            try {
                String codiceConto = getCodiceContoFromPdf(rispostaLogin, parametri.substring(parametri.indexOf("pdf=") + 4, parametri.indexOf("&regione=")));
                final JSONArray arrayConsumiLuce = new JSONArray(rispostaConsumiLuce);
                final JSONArray servizioLogin = new JSONArray(rispostaLogin);
                popolaPaginaContenuto(servizioLogin, arrayConsumiLuce, pager, codiceConto);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            Intent intentOops = new Intent(getApplicationContext(), OopsPageConsumiLuceFromConsumiLuce.class);
            positionSpinnerCurrentSelected = positionSpinnerPrecedentSelected;
            intentOops.putExtra("PARAMETER", parametri);
            intentOops.putExtra("MESSAGGIO", descEsito);
            startActivity(intentOops);
            overridePendingTransition(R.anim.fade, R.anim.hold);
        }

    }

    public void postChiamataException(boolean exception, String parametri) {
        if (exception) {
            if (animation != null) {
                animation.stop();
                imageAnim.setVisibility(View.INVISIBLE);
            }
            final ImageView indietro = (ImageView) findViewById(R.id.bollette_pagamenti_indietro);
            indietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
            indietro.getBackground().invalidateSelf();

            Intent intentOops = new Intent(getApplicationContext(), OopsPageConsumiLuceFromConsumiLuce.class);
            positionSpinnerCurrentSelected = positionSpinnerPrecedentSelected;
            intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
            intentOops.putExtra("PARAMETER", parametri);
            startActivity(intentOops);
            overridePendingTransition(R.anim.fade, R.anim.hold);
        }
    }

    public String getCodiceContoFromPdf(String stringLogin, String pdfConto) {
        String result = "";
        JSONObject anagrContoIesimo = new JSONObject();
        JSONObject anagrExtraContoIesimo = new JSONObject();
        try {
            JSONArray arrayLogin = new JSONArray(stringLogin);
            for (int i = 2; i < arrayLogin.length(); i++) {
                JSONArray contoIesimo;
                contoIesimo = arrayLogin.getJSONArray(i);
                anagrContoIesimo = contoIesimo.getJSONObject(0);
                if (contoIesimo.length() < 2) {
                    continue;
                } else {
                    anagrExtraContoIesimo = contoIesimo.getJSONObject(1);
                    String tipologiaLuceGas = anagrExtraContoIesimo.getString("Tipologia");
                    if (!tipologiaLuceGas.equals("POWER")) {
                        continue;
                    } else {
                        String pdf = anagrExtraContoIesimo.getString("Pdf");
                        if (!pdf.equals(pdfConto)) {
                            continue;
                        } else {
                            // trovato il conto
                            result = anagrContoIesimo.getString("CodiceConto");
                        }
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public String getPdfFromCodiceConto(String stringLogin, String codiceConto) {
        String result = "";
        JSONObject anagrContoIesimo = new JSONObject();
        JSONObject anagrExtraContoIesimo = new JSONObject();
        try {
            JSONArray arrayLogin = new JSONArray(stringLogin);
            for (int i = 2; i < arrayLogin.length(); i++) {
                JSONArray contoIesimo;
                contoIesimo = arrayLogin.getJSONArray(i);
                anagrContoIesimo = contoIesimo.getJSONObject(0);
                if (contoIesimo.length() < 2) {
                    continue;
                } else {
                    anagrExtraContoIesimo = contoIesimo.getJSONObject(1);
                    String tipologia = anagrExtraContoIesimo.getString("Tipologia");
                    if (!tipologia.equals("POWER")) {
                        continue;
                    } else {
                        String codiceContoIesimo = anagrContoIesimo.getString("CodiceConto");
                        if (!codiceContoIesimo.equals(codiceConto)) {
                            continue;
                        } else {
                            // trovato il conto luce
                            result = anagrExtraContoIesimo.getString("Pdf");
                        }
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    private void unbindDrawables(View view) {
        if (view.getBackground() != null) {
            view.getBackground().setCallback(null);
        }
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                unbindDrawables(((ViewGroup) view).getChildAt(i));
            }
            ((ViewGroup) view).removeAllViews();
        }
    }

    public void onBackPressed() {

        animation = Utilities.animation(this);
        imageAnim = (ImageView) findViewById(R.id.waiting_anim);
        imageAnim.setVisibility(View.VISIBLE);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
            imageAnim.setBackgroundDrawable(animation);
        } else {
            imageAnim.setBackground(animation);
        }
        animation.start();

        Intent intentHome = new Intent(getApplicationContext(), Home.class);

        TimerTask clearingHeapTask;
        final Handler handler = new Handler();
        clearingHeapTask = new TimerTask() {
            public void run() {
                handler.post(new Runnable() {
                    public void run() {
                        unbindDrawables(pager);
                        System.gc();
                    }
                });
            }
        };
        new Timer().schedule(clearingHeapTask, 250);

        ConsumiLuce.this.finish();
        startActivity(intentHome);
        overridePendingTransition(R.anim.fade, R.anim.hold);
    }

    private void tendinaRigaClick(int riga, int tendina) {
        tendinaChoosePeriodo.setVisibility(View.GONE);
        tendinaChooseRetail.setVisibility(View.GONE);

        switch (riga) {
            case 1:
                if (tendina == 1){
                    tendina_consumi_riferimento_selected.setBackgroundResource(R.drawable.mini_selected);
                    riferimento = Constants.RETAIL_RIFERIMENTO_MINI;
                } else if (tendina == 2){
                    choose_periodo_selected.setBackgroundResource(R.drawable.choose_6_mesi);
                    periodo = Constants.RETAIL_PERIODO_MESI_6;
                }
                break;
            case 2:
                if (tendina == 1){
                    tendina_consumi_riferimento_selected.setBackgroundResource(R.drawable.midi_selected);
                    riferimento = Constants.RETAIL_RIFERIMENTO_MIDI;
                } else if (tendina == 2) {
                    choose_periodo_selected.setBackgroundResource(R.drawable.choose_12_mesi);
                    periodo = Constants.RETAIL_PERIODO_MESI_12;
                }
                break;
            case 3:
                if (tendina == 1){
                    tendina_consumi_riferimento_selected.setBackgroundResource(R.drawable.maxi_selected);
                    riferimento = Constants.RETAIL_RIFERIMENTO_MAXI;
                } else if (tendina == 2) {
                    choose_periodo_selected.setBackgroundResource(R.drawable.choose_24_mesi);
                    periodo = Constants.RETAIL_PERIODO_MESI_24;
                }
                break;
        }

        float consumo_medio_luce = 0;
        JSONArray consumiLuce = null;
        try {
            consumiLuce = new JSONArray(rispostaConsumiLuce);
            consumo_medio_luce = calcolaConsumoMedio(consumiLuce, classificazione, riferimento, periodo);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (classificazione.compareToIgnoreCase("retail") == 0) {
            popolaSemaforoRetail(consumo_medio_luce);
        } else if (classificazione.compareToIgnoreCase("pmi") == 0) {
            popolaSemaforoPmi(consumo_medio_luce);
        }
        popolaGauge(consumi_luce_entry, consumiLuce, classificazione, periodo);
    }
}
