package com.eni.enigaseluce.consumiluce;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.AnimationDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.eni.enigaseluce.R;
import com.eni.enigaseluce.httpcall.Constants;
import com.eni.enigaseluce.utils.Utilities;

@SuppressLint("NewApi")
public class ConsumiLuceHelp extends Activity {
    AnimationDrawable animation;
    ImageView imageAnim;
    String origine;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.consumi_luce_help);
	origine = getIntent().getStringExtra("ORIGINE");

	final TextView contenuto = (TextView) findViewById(R.id.contenuto_help);
	if (origine.equals(Constants.ORIGINE_CONSUMI_LUCE)) {
	    if (getIntent().getStringExtra("Classificazione").equals("RETAIL")) {
	    	contenuto.setText(getResources().getString(R.string.help_consumi_luce_content_text_retail));
	    }
	    else if (getIntent().getStringExtra("Classificazione").equals("PMI")) {
	    	contenuto.setText(getResources().getString(R.string.help_consumi_luce_content_text_pmi));
	    }
	}
	else if (origine.equals(Constants.ORIGINE_CONSUMI_LUCE_VALORI_RIFERIMENTO)) {
		 if (getIntent().getStringExtra("Classificazione").equals("RETAIL")) {
			 contenuto.setText(getResources().getString(R.string.help_consumi_luce_valori_riferimento_content_text_retail));
		 }else if(getIntent().getStringExtra("Classificazione").equals("PMI")){
			 contenuto.setText(getResources().getString(R.string.help_consumi_luce_valori_riferimento_content_text_pmi));
		 }
	}
	else if (origine.equals(Constants.ORIGINE_CONSUMI_LUCE_GRAFICO_CONSUMI)) {
	    contenuto.setText(getResources().getString(R.string.help_consumi_luce_grafico_text));
	}

	final ImageView help = (ImageView) findViewById(R.id.consumi_luce_help_indietro);
	help.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
	help.getBackground().invalidateSelf();
	help.setOnClickListener(new View.OnClickListener() {
	    public void onClick(View v1) {
		help.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
		help.getBackground().invalidateSelf();

		animation = Utilities.animation(ConsumiLuceHelp.this);
		imageAnim = (ImageView) findViewById(R.id.waiting_anim);
		imageAnim.setVisibility(View.VISIBLE);
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
		    imageAnim.setBackgroundDrawable(animation);
		}
		else {
		    imageAnim.setBackground(animation);
		}
		animation.start();

		Intent intentHelp = new Intent();
		if (origine.equals(Constants.ORIGINE_CONSUMI_LUCE)) {
		    intentHelp = new Intent(getApplicationContext(), ConsumiLuce.class);
		    intentHelp.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
		    intentHelp.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
		    intentHelp.putExtra("CATEGORY", getIntent().getStringExtra("CATEGORY"));
		    intentHelp.putExtra("Classificazione", getIntent().getStringExtra("Classificazione"));
		    intentHelp.putExtra("PERIODO", getIntent().getIntExtra("PERIODO", Constants.RETAIL_PERIODO_MESI_6));
		    intentHelp.putExtra("RIFERIMENTO", getIntent().getStringExtra("RIFERIMENTO"));
		    intentHelp.putExtra("coloreGrafico", getIntent().getIntExtra("coloreGrafico", 0));
		    intentHelp.putExtra("autoletturaLuce", false);
		}
		else if (origine.equals(Constants.ORIGINE_CONSUMI_LUCE_VALORI_RIFERIMENTO)) {
		    intentHelp = new Intent(getApplicationContext(), ConsumiLuceValoriRiferimento.class);
		    intentHelp.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
		    intentHelp.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
		    intentHelp.putExtra("CATEGORY", getIntent().getStringExtra("CATEGORY"));
		    intentHelp.putExtra("Classificazione", getIntent().getStringExtra("Classificazione"));
		    intentHelp.putExtra("PERIODO", getIntent().getIntExtra("PERIODO", Constants.RETAIL_PERIODO_MESI_6));
		    intentHelp.putExtra("coloreGrafico", getIntent().getIntExtra("coloreGrafico", 0));
		    intentHelp.putExtra("RIFERIMENTO", getIntent().getStringExtra("RIFERIMENTO"));
		}
		else if (origine.equals(Constants.ORIGINE_CONSUMI_LUCE_GRAFICO_CONSUMI)) {
		    intentHelp = new Intent(getApplicationContext(), ConsumiLuceGraficoConsumi.class);
		    intentHelp.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
		    intentHelp.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
		    intentHelp.putExtra("CATEGORY", getIntent().getStringExtra("CATEGORY"));
		    intentHelp.putExtra("Classificazione", getIntent().getStringExtra("Classificazione"));
		    intentHelp.putExtra("PERIODO", getIntent().getIntExtra("PERIODO", Constants.RETAIL_PERIODO_MESI_6));
		    intentHelp.putExtra("RIFERIMENTO", getIntent().getStringExtra("RIFERIMENTO"));
		    intentHelp.putExtra("coloreGrafico", getIntent().getIntExtra("coloreGrafico", 0));
		}
		ConsumiLuceHelp.this.finish();
		startActivity(intentHelp);
		overridePendingTransition(R.anim.fade, R.anim.hold);
	    }
	});

    }

    public void onBackPressed() {

	animation = Utilities.animation(this);
	imageAnim = (ImageView) findViewById(R.id.waiting_anim);
	imageAnim.setVisibility(View.VISIBLE);
	if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
	    imageAnim.setBackgroundDrawable(animation);
	}
	else {
	    imageAnim.setBackground(animation);
	}
	animation.start();

	Intent intentHelp = new Intent();
	if (origine.equals(Constants.ORIGINE_CONSUMI_LUCE)) {
	    intentHelp = new Intent(getApplicationContext(), ConsumiLuce.class);
	    intentHelp.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
	    intentHelp.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
	    intentHelp.putExtra("CATEGORY", getIntent().getStringExtra("CATEGORY"));
	    intentHelp.putExtra("Classificazione", getIntent().getStringExtra("Classificazione"));
	    intentHelp.putExtra("PERIODO", getIntent().getIntExtra("PERIODO", Constants.RETAIL_PERIODO_MESI_6));
	    intentHelp.putExtra("RIFERIMENTO", getIntent().getStringExtra("RIFERIMENTO"));
	    intentHelp.putExtra("coloreGrafico", getIntent().getIntExtra("coloreGrafico", 0));
	    intentHelp.putExtra("autoletturaLuce", false);
	}
	else if (origine.equals(Constants.ORIGINE_CONSUMI_LUCE_VALORI_RIFERIMENTO)) {
	    intentHelp = new Intent(getApplicationContext(), ConsumiLuceValoriRiferimento.class);
	    intentHelp.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
	    intentHelp.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
	    intentHelp.putExtra("CATEGORY", getIntent().getStringExtra("CATEGORY"));
	    intentHelp.putExtra("Classificazione", getIntent().getStringExtra("Classificazione"));
	    intentHelp.putExtra("PERIODO", getIntent().getIntExtra("PERIODO", Constants.RETAIL_PERIODO_MESI_6));
	    intentHelp.putExtra("RIFERIMENTO", getIntent().getStringExtra("RIFERIMENTO"));
	    intentHelp.putExtra("coloreGrafico", getIntent().getIntExtra("coloreGrafico", 0));
	}
	else if (origine.equals(Constants.ORIGINE_CONSUMI_LUCE_GRAFICO_CONSUMI)) {
	    intentHelp = new Intent(getApplicationContext(), ConsumiLuceGraficoConsumi.class);
	    intentHelp.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
	    intentHelp.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
	    intentHelp.putExtra("CATEGORY", getIntent().getStringExtra("CATEGORY"));
	    intentHelp.putExtra("Classificazione", getIntent().getStringExtra("Classificazione"));
	    intentHelp.putExtra("PERIODO", getIntent().getIntExtra("PERIODO", Constants.RETAIL_PERIODO_MESI_6));
	    intentHelp.putExtra("RIFERIMENTO", getIntent().getStringExtra("RIFERIMENTO"));
	    intentHelp.putExtra("coloreGrafico", getIntent().getIntExtra("coloreGrafico", 0));
	}
	ConsumiLuceHelp.this.finish();
	startActivity(intentHelp);
	overridePendingTransition(R.anim.fade, R.anim.hold);
    }
}
