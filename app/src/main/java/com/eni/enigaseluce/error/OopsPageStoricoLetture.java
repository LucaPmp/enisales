package com.eni.enigaseluce.error;

import java.util.HashMap;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.graphics.drawable.AnimationDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.eni.enigaseluce.R;
import com.eni.enigaseluce.autolettura.AutoLetturaInsert;
import com.eni.enigaseluce.font.EniFont;
import com.eni.enigaseluce.httpcall.Constants;
import com.eni.enigaseluce.httpcall.ServiceCall;
import com.eni.enigaseluce.httpcall.ServiceCallFactory;
import com.eni.enigaseluce.utils.Log;
import com.eni.enigaseluce.utils.Utilities;

@SuppressLint("NewApi")
public class OopsPageStoricoLetture extends Activity {
    public final static int ESCI = 1;
    private Context context;
    AnimationDrawable animation;
    ImageView imageAnim;
    boolean checkbox = false;
    SharedPreferences settings;
    SharedPreferences.Editor editor;
    String userStored;
    String passwordStored;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.oops_page);
	context = this;

	Intent intent = getIntent();
	String messaggioString = intent.getStringExtra("MESSAGGIO");
	final String pdf = intent.getStringExtra("PDF");
	final String sistemaBilling = intent.getStringExtra("SISTEMA_BILLING");
	final String societaFornitrice = intent.getStringExtra("SOCIETA_FORNITRICE");
	final String pdr = intent.getStringExtra("PDR");
	final String numeroCifreMisuratore=getIntent().getStringExtra("NUM_CIFRE_MISURATORE");

	final ImageView indietro = (ImageView) findViewById(R.id.oops_indietro);
	indietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
	indietro.getBackground().invalidateSelf();
	indietro.setOnClickListener(new View.OnClickListener() {
	    public void onClick(View v1) {
		indietro.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
		indietro.getBackground().invalidateSelf();
		OopsPageStoricoLetture.this.setResult(OopsPageStoricoLetture.ESCI);
		OopsPageStoricoLetture.this.finish();
	    }
	});
	
	final ImageView riprova = (ImageView) findViewById(R.id.oops_riprova);
	riprova.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
	riprova.getBackground().invalidateSelf();
	riprova.setOnClickListener(new View.OnClickListener() {
	    public void onClick(View v1) {
		riprova.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
		riprova.getBackground().invalidateSelf();
		// parte la chiamata al dettaglio dello storico

		animation = Utilities.animation(OopsPageStoricoLetture.this);
		imageAnim = (ImageView) findViewById(R.id.waiting_anim);
		imageAnim.setVisibility(View.VISIBLE);
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
		    imageAnim.setBackgroundDrawable(animation);
		}
		else {
		    imageAnim.setBackground(animation);
		}
		animation.start();

		/************************************ Storico letture gas ************************************/
		if (!isNetworkAvailable(context)) {
		    Intent intentOops = new Intent(getApplicationContext(), OopsPageStoricoLetture.class);
		    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.connessione_assente));
		    intentOops.putExtra("PDF", pdf);
		    intentOops.putExtra("SISTEMA_BILLING", sistemaBilling);
		    intentOops.putExtra("SOCIETA_FORNITRICE", societaFornitrice);
		    intentOops.putExtra("NUM_CIFRE_MISURATORE", numeroCifreMisuratore);
		    intentOops.putExtra("PDR", pdr);
		    // StatoAttivazioneFornitura.this.finish();
		    OopsPageStoricoLetture.this.finish();
		    startActivity(intentOops);
		    overridePendingTransition(R.anim.fade, R.anim.hold);
		}
		else {
		    ServiceCall stAvFornServiceCall = ServiceCallFactory.createServiceCall(context);
		    // stAvFornServiceCall.myCookieStore = new
		    // PersistentCookieStore(context);
                    try {
		    HashMap<String, String> parametri = new HashMap<String, String>();
		    parametri.put("pdf", pdf);
		    parametri.put("sistemaBilling", sistemaBilling);
		    parametri.put("societaFornitrice", societaFornitrice);

		    
			stAvFornServiceCall.executeHttpsGetStoricoLetture(Constants.URL_STORICO_LETTURE_GAS, parametri, (AutoLetturaInsert) AutoLetturaInsert.getContext(), pdf, pdr, OopsPageStoricoLetture.this);
		    }
		    catch (Exception e) {
			
		    Log.d("OOPSPAGESTORICOLETTURE","Errore: "+e.getMessage());
			e.printStackTrace();
			
			Intent intentOops = new Intent(getApplicationContext(), OopsPageStoricoLetture.class);
			intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
			intentOops.putExtra("PDF", pdf);
			intentOops.putExtra("SISTEMA_BILLING", sistemaBilling);
			intentOops.putExtra("NUM_CIFRE_MISURATORE", numeroCifreMisuratore);
			intentOops.putExtra("SOCIETA_FORNITRICE", societaFornitrice);
			intentOops.putExtra("PDR", pdr);

			// StatoAttivazioneFornitura.this.finish();
			OopsPageStoricoLetture.this.finish();
			startActivity(intentOops);
			overridePendingTransition(R.anim.fade, R.anim.hold);
		    }
		}
		/*******************************************************************************************/
	    }
	});
	final EniFont messaggio = (EniFont) findViewById(R.id.oops_messaggio);
	messaggio.setText(messaggioString);
    }

    private boolean isNetworkAvailable(Context context) {
	ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
	if (connectivityManager == null) return false;
	NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
	return activeNetworkInfo != null;
    }

    public void onBackPressed() {
	OopsPageStoricoLetture.this.setResult(OopsPageStoricoLetture.ESCI);
	OopsPageStoricoLetture.this.finish();
    }
}
