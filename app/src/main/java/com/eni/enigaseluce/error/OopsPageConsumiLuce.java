package com.eni.enigaseluce.error;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.graphics.drawable.AnimationDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.eni.enigaseluce.R;
import com.eni.enigaseluce.font.EniFont;
import com.eni.enigaseluce.home.Home;
import com.eni.enigaseluce.httpcall.Constants;
import com.eni.enigaseluce.httpcall.ServiceCall;
import com.eni.enigaseluce.httpcall.ServiceCallFactory;
import com.eni.enigaseluce.utils.Utilities;

@SuppressLint("NewApi")
public class OopsPageConsumiLuce extends Activity {
    public final static int ESCI = 1;
    private Context context;
    AnimationDrawable animation;
    ImageView imageAnim;
    boolean checkbox = false;
    SharedPreferences settings;
    SharedPreferences.Editor editor;
    String userStored;
    String passwordStored;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.oops_page);
	context = this;

	Intent intent = getIntent();
	String messaggioString = intent.getStringExtra("MESSAGGIO");
	final String parameter = intent.getStringExtra("PARAMETER");

	final ImageView indietro = (ImageView) findViewById(R.id.oops_indietro);
	indietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
	indietro.getBackground().invalidateSelf();
	indietro.setOnClickListener(new View.OnClickListener() {
	    public void onClick(View v1) {
		indietro.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
		indietro.getBackground().invalidateSelf();
		OopsPageConsumiLuce.this.setResult(OopsPageConsumiLuce.ESCI);
		OopsPageConsumiLuce.this.finish();
	    }
	});
	final ImageView riprova = (ImageView) findViewById(R.id.oops_riprova);
	riprova.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
	riprova.getBackground().invalidateSelf();
	riprova.setOnClickListener(new View.OnClickListener() {
	    public void onClick(View v1) {
		riprova.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
		riprova.getBackground().invalidateSelf();

		animation = Utilities.animation(OopsPageConsumiLuce.this);
		imageAnim = (ImageView) findViewById(R.id.waiting_anim);
		imageAnim.setVisibility(View.VISIBLE);
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
		    imageAnim.setBackgroundDrawable(animation);
		}
		else {
		    imageAnim.setBackground(animation);
		}
		animation.start();

		if (!isNetworkAvailable(context)) {
		    Intent intentOops = new Intent(getApplicationContext(), OopsPageConsumiLuce.class);
		    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.connessione_assente));
		    intentOops.putExtra("PARAMETER", parameter);
		    OopsPageConsumiLuce.this.finish();
		    startActivity(intentOops);
		    overridePendingTransition(R.anim.fade, R.anim.hold);
		}
		else {
		    /********************* Chiamata consumi luce ******************************/
		    ServiceCall visFornDomicilServiceCall = ServiceCallFactory.createServiceCall(context);
		    // visFornDomicilServiceCall.myCookieStore = new
		    // PersistentCookieStore(context);

		    try {
			visFornDomicilServiceCall.executeHttpsGet(Constants.URL_CONSUMI_LUCE, parameter, (Home) Home.getContext(), Constants.FUNZIONALITA_CONSUMI_LUCE, OopsPageConsumiLuce.this);
		    }
		    catch (Exception e) {
			Intent intentOops = new Intent(getApplicationContext(), OopsPageConsumiLuce.class);
			intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
			intentOops.putExtra("PARAMETER", parameter);
			OopsPageConsumiLuce.this.finish();
			startActivity(intentOops);
			overridePendingTransition(R.anim.fade, R.anim.hold);
		    }
		    /**********************************************************************************/
		}
	    }
	});
	final EniFont messaggio = (EniFont) findViewById(R.id.oops_messaggio);
	messaggio.setText(messaggioString);
    }

    private boolean isNetworkAvailable(Context context) {
	ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
	if (connectivityManager == null) return false;
	NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
	return activeNetworkInfo != null;
    }

    public void onBackPressed() {
	OopsPageConsumiLuce.this.setResult(OopsPageConsumiLuce.ESCI);
	OopsPageConsumiLuce.this.finish();
    }
}
