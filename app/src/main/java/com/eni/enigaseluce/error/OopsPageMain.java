package com.eni.enigaseluce.error;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.AnimationDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.eni.enigaseluce.Main;
import com.eni.enigaseluce.R;
import com.eni.enigaseluce.font.EniFont;
import com.eni.enigaseluce.httpcall.Constants;
import com.eni.enigaseluce.httpcall.ServiceCall;
import com.eni.enigaseluce.httpcall.ServiceCallFactory;
import com.eni.enigaseluce.login.Login;
import com.eni.enigaseluce.utils.Utilities;

@SuppressLint("NewApi")
public class OopsPageMain extends Activity {
    AnimationDrawable animation;
    ImageView imageAnim;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.oops_page);
        Intent intent = getIntent();
        String messaggioString = intent.getStringExtra("MESSAGGIO");

        final ImageView indietro = (ImageView) findViewById(R.id.oops_indietro);
        indietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
        indietro.getBackground().invalidateSelf();
        indietro.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v1) {
                indietro.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
                indietro.getBackground().invalidateSelf();
                Intent intentMain = new Intent(getApplicationContext(), Main.class);
                OopsPageMain.this.finish();
                startActivity(intentMain);
                overridePendingTransition(R.anim.fade, R.anim.hold);
            }
        });
        final ImageView riprova = (ImageView) findViewById(R.id.oops_riprova);
        riprova.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
        riprova.getBackground().invalidateSelf();
        riprova.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v1) {
                riprova.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
                riprova.getBackground().invalidateSelf();
                // quando l'utente clicka si deve rieseguire l'ultima chiamata
                // fatta
                if (isNetworkAvailable(OopsPageMain.this)) {

                    animation = Utilities.animation(OopsPageMain.this);
                    imageAnim = (ImageView) findViewById(R.id.waiting_anim);
                    imageAnim.setVisibility(View.VISIBLE);
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                        imageAnim.setBackgroundDrawable(animation);
                    } else {
                        imageAnim.setBackground(animation);
                    }
                    animation.start();

                    maintanence();
                } else {
                    animation = Utilities.animation(OopsPageMain.this);
                    imageAnim = (ImageView) findViewById(R.id.waiting_anim);
                    imageAnim.setVisibility(View.VISIBLE);
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                        imageAnim.setBackgroundDrawable(animation);
                    } else {
                        imageAnim.setBackground(animation);
                    }
                    animation.start();

                    Intent nextIntent = new Intent(getApplicationContext(), OopsPageMain.class);
                    nextIntent.putExtra("MESSAGGIO", getResources().getString(R.string.connessione_assente));
                    startActivity(nextIntent);
                    OopsPageMain.this.finish();
                }

            }
        });
        final EniFont messaggio = (EniFont) findViewById(R.id.oops_messaggio);
        messaggio.setText(messaggioString);
    }

    public void maintanence() {
        if (!isNetworkAvailable(OopsPageMain.this)) {
            Intent intentOops = new Intent(getApplicationContext(), OopsPageMain.class);
            intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.connessione_assente));
            // Main.this.finish();
            startActivity(intentOops);
            overridePendingTransition(R.anim.fade, R.anim.hold);
        } else {
            // ******maintainance mode****//
            ServiceCall maintainnanceMode = ServiceCallFactory.createServiceCall(this);
            String rispostaMaintainanceMode = "";
            try {
                rispostaMaintainanceMode = maintainnanceMode.executeHttpGet(Constants.URL_MAINTAINANCE_MODE);
//                maintainnanceMode.executeHttpsGetMaintenance(Constants.URL_MAINTAINANCE_MODE, OopsPageMain.this);

            } catch (Exception e) {
                Intent intentOops = new Intent(getApplicationContext(), OopsPageMain.class);
                intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
                OopsPageMain.this.finish();
                startActivity(intentOops);
                overridePendingTransition(R.anim.fade, R.anim.hold);
                return;
            }
            if (rispostaMaintainanceMode.indexOf("<flag>") != -1) {
                int indexStart = rispostaMaintainanceMode.indexOf("<flag>");
                int indexEnd = rispostaMaintainanceMode.indexOf("</flag>");
                String flag = rispostaMaintainanceMode.substring(indexStart + 6, indexEnd);
                if (flag.equals("false")) {
                    // Passaggio alla Login
                    Intent intentLogin = new Intent(getApplicationContext(), Login.class);
                    OopsPageMain.this.finish();
                    startActivity(intentLogin);
                    overridePendingTransition(R.anim.fade, R.anim.hold);
                } else {
                    int indexMsgStart = rispostaMaintainanceMode.indexOf("<text>");
                    int indexMsgEnd = rispostaMaintainanceMode.indexOf("</text>");
                    String msgCortesia = rispostaMaintainanceMode.substring(indexMsgStart + 6, indexMsgEnd);

                    Intent intentOops = new Intent(getApplicationContext(), OopsPageMain.class);
                    intentOops.putExtra("MESSAGGIO", msgCortesia);
                    OopsPageMain.this.finish();
                    startActivity(intentOops);
                    overridePendingTransition(R.anim.fade, R.anim.hold);
                }
            } else {
                Intent intentOops = new Intent(getApplicationContext(), OopsPageMain.class);
                intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.maintenance_message));
                OopsPageMain.this.finish();
                startActivity(intentOops);
                overridePendingTransition(R.anim.fade, R.anim.hold);
            }
        }
    }

    public void onResponse(String rispostaMaintainanceMode)
    {
        if (rispostaMaintainanceMode.indexOf("<flag>") != -1) {
            int indexStart = rispostaMaintainanceMode.indexOf("<flag>");
            int indexEnd = rispostaMaintainanceMode.indexOf("</flag>");
            String flag = rispostaMaintainanceMode.substring(indexStart + 6, indexEnd);
            if (flag.equals("false")) {
                // Passaggio alla Login
                Intent intentLogin = new Intent(getApplicationContext(), Login.class);
                OopsPageMain.this.finish();
                startActivity(intentLogin);
                overridePendingTransition(R.anim.fade, R.anim.hold);
            } else {
                int indexMsgStart = rispostaMaintainanceMode.indexOf("<text>");
                int indexMsgEnd = rispostaMaintainanceMode.indexOf("</text>");
                String msgCortesia = rispostaMaintainanceMode.substring(indexMsgStart + 6, indexMsgEnd);

                Intent intentOops = new Intent(getApplicationContext(), OopsPageMain.class);
                intentOops.putExtra("MESSAGGIO", msgCortesia);
                OopsPageMain.this.finish();
                startActivity(intentOops);
                overridePendingTransition(R.anim.fade, R.anim.hold);
            }
        } else {
            Intent intentOops = new Intent(getApplicationContext(), OopsPageMain.class);
            intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.maintenance_message));
            OopsPageMain.this.finish();
            startActivity(intentOops);
            overridePendingTransition(R.anim.fade, R.anim.hold);
        }
    }

    public void onBackPressed() {
        Intent intentMain = new Intent(getApplicationContext(), Main.class);
        OopsPageMain.this.finish();
        startActivity(intentMain);
        overridePendingTransition(R.anim.fade, R.anim.hold);
    }

    private boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager == null) return false;
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return (activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting());
    }
}
