package com.eni.enigaseluce.error;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.eni.enigaseluce.R;
import com.eni.enigaseluce.bollettePagamenti.BollettePagamentiRicerca;
import com.eni.enigaseluce.font.EniFont;

public class OopsPageBollettePagamentiDownloadBolletta extends Activity {
    private Context context;
    AnimationDrawable animation;
    ImageView imageAnim;
    boolean checkbox = false;
    SharedPreferences settings;
    SharedPreferences.Editor editor;
    String userStored;
    String passwordStored;

    @Override
    public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.oops_page);
	context = this;

	Intent intent = getIntent();
	String messaggioString = intent.getStringExtra("MESSAGGIO");

	final ImageView indietro = (ImageView) findViewById(R.id.oops_indietro);
	indietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
	indietro.getBackground().invalidateSelf();
	indietro.setOnClickListener(new View.OnClickListener() {
	    public void onClick(View v1) {
		indietro.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
		indietro.getBackground().invalidateSelf();
		Intent intentBollettePagamentiRicerca = new Intent(getApplicationContext(), BollettePagamentiRicerca.class);
		intentBollettePagamentiRicerca.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
		intentBollettePagamentiRicerca.putExtra("ORIGINE", getIntent().getStringExtra("ORIGINE"));
		intentBollettePagamentiRicerca.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
		OopsPageBollettePagamentiDownloadBolletta.this.finish();
		startActivity(intentBollettePagamentiRicerca);
		overridePendingTransition(R.anim.fade, R.anim.hold);
	    }
	});
	final ImageView riprova = (ImageView) findViewById(R.id.oops_riprova);
	riprova.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
	riprova.getBackground().invalidateSelf();
	riprova.setOnClickListener(new View.OnClickListener() {
	    public void onClick(View v1) {
		riprova.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
		riprova.getBackground().invalidateSelf();
		// quando l'utente clicka si deve rieseguire l'ultima chiamata
		// fatta
		/*
		 * animation = new AnimationDrawable();
		 * animation.addFrame(getResources
		 * ().getDrawable(R.drawable.anim_load2), 300);
		 * animation.addFrame
		 * (getResources().getDrawable(R.drawable.anim_load3), 300);
		 * animation
		 * .addFrame(getResources().getDrawable(R.drawable.anim_load4),
		 * 300); animation.setOneShot(false); imageAnim = (ImageView)
		 * findViewById(R.id.waiting_ops);
		 * imageAnim.setVisibility(View.VISIBLE);
		 * if(Build.VERSION.SDK_INT < 16){
		 * imageAnim.setBackgroundDrawable(animation); }else{
		 * imageAnim.setBackground(animation); } // start the animation!
		 * animation.start();
		 * 
		 * if(!isNetworkAvailable(context)){ Intent intentOops = new
		 * Intent(getApplicationContext(),
		 * OopsPageBollettePagamentiDownloadBolletta.class);
		 * intentOops.putExtra("MESSAGGIO",
		 * getResources().getString(R.string.connessione_assente));
		 * intentOops.putExtra("ID_PDF", pdf);
		 * OopsPageBollettePagamentiDownloadBolletta.this.finish();
		 * startActivity(intentOops);
		 * overridePendingTransition(R.anim.fade, R.anim.hold); } else{
		 *//********************* Chiamata download ******************************/
		/*
		 * ServiceCall downloadServiceCall =
		 * ServiceCallFactory.createServiceCall(context);
		 * downloadServiceCall.myCookieStore = new
		 * PersistentCookieStore(context);
		 * 
		 * HashMap<String,String> parametri = new
		 * HashMap<String,String>(); parametri.put("idProtocolloDocPdf",
		 * pdf);
		 * 
		 * try {
		 * downloadServiceCall.executeHttpsGetBollettePagamentiRicerca
		 * (Constants.URL_DOWNLOAD_PDF,parametri,
		 * (BollettePagamentiRicerca
		 * )BollettePagamentiRicerca.getContext(),
		 * OopsPageBollettePagamentiDownloadBolletta.this); } catch
		 * (Exception e) { Intent intentOops = new
		 * Intent(getApplicationContext(),
		 * OopsPageBollettePagamentiDownloadBolletta.class);
		 * intentOops.putExtra("MESSAGGIO",
		 * getResources().getString(R.string.servizio_non_disponibile));
		 * intentOops.putExtra("ID_PDF", pdf);
		 * startActivity(intentOops);
		 * OopsPageBollettePagamentiDownloadBolletta.this.finish();
		 * overridePendingTransition(R.anim.fade, R.anim.hold); } }
		 */
		Intent intentBollettePagamentiRicerca = new Intent(getApplicationContext(), BollettePagamentiRicerca.class);
		intentBollettePagamentiRicerca.putExtra("ID_PDF", getIntent().getStringExtra("ID_PDF"));
		intentBollettePagamentiRicerca.putExtra("NumeroFattura", getIntent().getStringExtra("NumeroFattura"));
		intentBollettePagamentiRicerca.putExtra("OopsPage", true);
		intentBollettePagamentiRicerca.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
		intentBollettePagamentiRicerca.putExtra("ORIGINE", getIntent().getStringExtra("ORIGINE"));
		intentBollettePagamentiRicerca.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
		OopsPageBollettePagamentiDownloadBolletta.this.finish();
		startActivity(intentBollettePagamentiRicerca);
		overridePendingTransition(R.anim.fade, R.anim.hold);
	    }
	});
	final EniFont messaggio = (EniFont) findViewById(R.id.oops_messaggio);
	messaggio.setText(messaggioString);
    }

    /*
     * private boolean isNetworkAvailable(Context context) { ConnectivityManager
     * connectivityManager = (ConnectivityManager)
     * context.getSystemService(Context.CONNECTIVITY_SERVICE); if
     * (connectivityManager == null) return false; NetworkInfo activeNetworkInfo
     * = connectivityManager.getActiveNetworkInfo(); return activeNetworkInfo !=
     * null; }
     */

    public void onBackPressed() {
	Intent intentBollettePagamentiRicerca = new Intent(getApplicationContext(), BollettePagamentiRicerca.class);
	intentBollettePagamentiRicerca.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
	intentBollettePagamentiRicerca.putExtra("ORIGINE", getIntent().getStringExtra("ORIGINE"));
	intentBollettePagamentiRicerca.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
	OopsPageBollettePagamentiDownloadBolletta.this.finish();
	startActivity(intentBollettePagamentiRicerca);
	overridePendingTransition(R.anim.fade, R.anim.hold);
    }
}
