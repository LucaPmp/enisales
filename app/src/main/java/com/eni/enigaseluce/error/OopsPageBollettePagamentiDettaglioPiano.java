package com.eni.enigaseluce.error;

import java.util.HashMap;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.graphics.drawable.AnimationDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.eni.enigaseluce.R;
import com.eni.enigaseluce.bollettePagamenti.BollettePagamentiDettaglioPiano;
import com.eni.enigaseluce.font.EniFont;
import com.eni.enigaseluce.httpcall.Constants;
import com.eni.enigaseluce.httpcall.ServiceCall;
import com.eni.enigaseluce.httpcall.ServiceCallFactory;
import com.eni.enigaseluce.utils.Utilities;

@SuppressLint("NewApi")
public class OopsPageBollettePagamentiDettaglioPiano extends Activity {
    public final static int ESCI = 1;
    private Context context;
    AnimationDrawable animation;
    ImageView imageAnim;
    boolean checkbox = false;
    SharedPreferences settings;
    SharedPreferences.Editor editor;
    String userStored;
    String passwordStored;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.oops_page);
	context = this;

	Intent intent = getIntent();
	String messaggioString = intent.getStringExtra("MESSAGGIO");
	final String accettaModifica = getIntent().getStringExtra("accettaModifica");

	final ImageView indietro = (ImageView) findViewById(R.id.oops_indietro);
	indietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
	indietro.getBackground().invalidateSelf();
	indietro.setOnClickListener(new View.OnClickListener() {
	    public void onClick(View v1) {
		indietro.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
		indietro.getBackground().invalidateSelf();
		OopsPageBollettePagamentiDettaglioPiano.this.setResult(OopsPageBollettePagamentiDettaglioPiano.ESCI);
		OopsPageBollettePagamentiDettaglioPiano.this.finish();
	    }
	});
	final ImageView riprova = (ImageView) findViewById(R.id.oops_riprova);
	riprova.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
	riprova.getBackground().invalidateSelf();
	riprova.setOnClickListener(new View.OnClickListener() {
	    public void onClick(View v1) {
		riprova.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
		riprova.getBackground().invalidateSelf();

		animation = Utilities.animation(OopsPageBollettePagamentiDettaglioPiano.this);
		imageAnim = (ImageView) findViewById(R.id.waiting_anim);
		imageAnim.setVisibility(View.VISIBLE);
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
		    imageAnim.setBackgroundDrawable(animation);
		}
		else {
		    imageAnim.setBackground(animation);
		}
		animation.start();

		HashMap<String, String> parametri = new HashMap<String, String>();
		parametri.put("codiceCliente", getIntent().getStringExtra("codiceCliente"));
		parametri.put("codiceContoCliente", getIntent().getStringExtra("codiceContoCliente"));
		parametri.put("userId", getIntent().getStringExtra("userId"));
		parametri.put("identificativoFattura", getIntent().getStringExtra("identificativoFattura"));
		parametri.put("codiceCausaleRateizzabilita", getIntent().getStringExtra("codiceCausaleRateizzabilita"));
		parametri.put("codiceFattura", getIntent().getStringExtra("codiceFattura"));
		parametri.put("numeroRate", getIntent().getStringExtra("numeroRate"));
		parametri.put("periodicitaScadenza", getIntent().getStringExtra("periodicitaScadenza"));
		parametri.put("cognome", getIntent().getStringExtra("cognome"));
		parametri.put("codiceContratto", getIntent().getStringExtra("codiceContratto"));
		if (!isNetworkAvailable(context)) {
		    Intent intentOops = new Intent(getApplicationContext(), OopsPageBollettePagamentiDettaglioPiano.class);
		    intentOops.putExtra("codiceCliente", getIntent().getStringExtra("codiceCliente"));
		    intentOops.putExtra("codiceContoCliente", getIntent().getStringExtra("codiceContoCliente"));
		    intentOops.putExtra("userId", getIntent().getStringExtra("userId"));
		    intentOops.putExtra("identificativoFattura", getIntent().getStringExtra("identificativoFattura"));
		    intentOops.putExtra("codiceCausaleRateizzabilita", getIntent().getStringExtra("codiceCausaleRateizzabilita"));
		    intentOops.putExtra("codiceFattura", getIntent().getStringExtra("codiceFattura"));
		    intentOops.putExtra("numeroRate", getIntent().getStringExtra("numeroRate"));
		    intentOops.putExtra("periodicitaScadenza", getIntent().getStringExtra("periodicitaScadenza"));
		    intentOops.putExtra("cognome", getIntent().getStringExtra("cognome"));
		    intentOops.putExtra("codiceContratto", getIntent().getStringExtra("codiceContratto"));
		    intentOops.putExtra("accettaModifica", accettaModifica);
		    intentOops.putExtra("domiciliazioneApprovata", getIntent().getBooleanExtra("domiciliazioneApprovata", false));
			intentOops.putExtra("infoDomiciliazioneApprovata",  getIntent().getBooleanExtra("infoDomiciliazioneApprovata", false));
			
		    OopsPageBollettePagamentiDettaglioPiano.this.finish();
		    startActivity(intentOops);
		    overridePendingTransition(R.anim.fade, R.anim.hold);
		}
		else {
		    /********************* Chiamata attiva proposta ******************************/
		    ServiceCall visFornDomicilServiceCall = ServiceCallFactory.createServiceCall(context);
		    // visFornDomicilServiceCall.myCookieStore = new
		    // PersistentCookieStore(context);

		    try {
			visFornDomicilServiceCall.executeHttpsGetBollettePagamentiAttivaPropostaPiano(Constants.URL_ATTIVA_PROPOSTA_RATEIZZAZIONE, parametri, (BollettePagamentiDettaglioPiano) BollettePagamentiDettaglioPiano.getContext(), OopsPageBollettePagamentiDettaglioPiano.this, accettaModifica);

		    }
		    catch (Exception e) {
			Intent intentOops = new Intent(getApplicationContext(), OopsPageBollettePagamentiDettaglioPiano.class);
			intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
			intentOops.putExtra("codiceCliente", getIntent().getStringExtra("codiceCliente"));
			intentOops.putExtra("codiceContoCliente", getIntent().getStringExtra("codiceContoCliente"));
			intentOops.putExtra("userId", getIntent().getStringExtra("userId"));
			intentOops.putExtra("identificativoFattura", getIntent().getStringExtra("identificativoFattura"));
			intentOops.putExtra("codiceCausaleRateizzabilita", getIntent().getStringExtra("codiceCausaleRateizzabilita"));
			intentOops.putExtra("codiceFattura", getIntent().getStringExtra("codiceFattura"));
			intentOops.putExtra("numeroRate", getIntent().getStringExtra("numeroRate"));
			intentOops.putExtra("periodicitaScadenza", getIntent().getStringExtra("periodicitaScadenza"));
			intentOops.putExtra("cognome", getIntent().getStringExtra("cognome"));
			intentOops.putExtra("codiceContratto", getIntent().getStringExtra("codiceContratto"));
			intentOops.putExtra("accettaModifica", accettaModifica);
			
			intentOops.putExtra("domiciliazioneApprovata", getIntent().getBooleanExtra("domiciliazioneApprovata", false));
			intentOops.putExtra("infoDomiciliazioneApprovata",  getIntent().getBooleanExtra("infoDomiciliazioneApprovata", false));
			
			OopsPageBollettePagamentiDettaglioPiano.this.finish();
			startActivity(intentOops);
			overridePendingTransition(R.anim.fade, R.anim.hold);
		    }
		    /**********************************************************************************/
		}
	    }
	});
	final EniFont messaggio = (EniFont) findViewById(R.id.oops_messaggio);
	messaggio.setText(messaggioString);
    }

    private boolean isNetworkAvailable(Context context) {
	ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
	if (connectivityManager == null) return false;
	NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
	return activeNetworkInfo != null;
    }

    public void onBackPressed() {
	OopsPageBollettePagamentiDettaglioPiano.this.setResult(OopsPageBollettePagamentiDettaglioPiano.ESCI);
	OopsPageBollettePagamentiDettaglioPiano.this.finish();
    }
}
