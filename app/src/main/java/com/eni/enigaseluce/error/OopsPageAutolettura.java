package com.eni.enigaseluce.error;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.graphics.drawable.AnimationDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.eni.enigaseluce.R;
import com.eni.enigaseluce.font.EniFont;
import com.eni.enigaseluce.home.Home;
import com.eni.enigaseluce.httpcall.Constants;
import com.eni.enigaseluce.httpcall.ServiceCall;
import com.eni.enigaseluce.httpcall.ServiceCallFactory;
import com.eni.enigaseluce.utils.Utilities;

@SuppressLint("NewApi")
public class OopsPageAutolettura extends Activity {
    public final static int ESCI = 1;
    private Context context;
    AnimationDrawable animation;
    ImageView imageAnim;
    boolean checkbox = false;
    SharedPreferences settings;
    SharedPreferences.Editor editor;
    String userStored;
    String passwordStored;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.oops_page);
	context = this;

	Intent intent = getIntent();
	String messaggioString = intent.getStringExtra("MESSAGGIO");
	// final String primoConto =
	// intent.getStringExtra("CODICE_CONTO_CLIENTE");
	final String parametri = intent.getStringExtra("PARAMETRI");

	final ImageView indietro = (ImageView) findViewById(R.id.oops_indietro);
	indietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
	indietro.getBackground().invalidateSelf();
	indietro.setOnClickListener(new View.OnClickListener() {
	    public void onClick(View v1) {
		indietro.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
		indietro.getBackground().invalidateSelf();
		OopsPageAutolettura.this.setResult(OopsPageAutolettura.ESCI);
		OopsPageAutolettura.this.finish();
	    }
	});
	final ImageView riprova = (ImageView) findViewById(R.id.oops_riprova);
	riprova.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
	riprova.getBackground().invalidateSelf();
	riprova.setOnClickListener(new View.OnClickListener() {
	    public void onClick(View v1) {
		riprova.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
		riprova.getBackground().invalidateSelf();

		animation = Utilities.animation(OopsPageAutolettura.this);
		imageAnim = (ImageView) findViewById(R.id.waiting_anim);
		imageAnim.setVisibility(View.VISIBLE);
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
		    imageAnim.setBackgroundDrawable(animation);
		}
		else {
		    imageAnim.setBackground(animation);
		}
		animation.start();

		if (!isNetworkAvailable(context)) {
		    Intent intentOops = new Intent(getApplicationContext(), OopsPageAutolettura.class);
		    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.connessione_assente));
		    intentOops.putExtra("PARAMETRI", parametri);
		    startActivity(intentOops);
		    OopsPageAutolettura.this.finish();
		    overridePendingTransition(R.anim.fade, R.anim.hold);
		}
		else {
		    /********************* loadFornitureInfoGas ******************************/
		    ServiceCall stAvFornServiceCall = ServiceCallFactory.createServiceCall(context);
		    // stAvFornServiceCall.myCookieStore = new
		    // PersistentCookieStore(context);
		    // parametri senza pdf (nessuna fornitura gas), la chiamata
		    // non deve essere fatta senza paramtri pdf, popup con
		    // messaggio nessuna fornitura gas presente
		    if (!parametri.contains("pdf")) {
			AlertDialog.Builder builder = new AlertDialog.Builder(context);
			builder.setMessage(R.string.nessuna_fornitura_presente).setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
			    public void onClick(DialogInterface dialog, int id) {
				if (animation != null) {
				    animation.stop();
				    imageAnim.setVisibility(View.INVISIBLE);
				}
			    }
			});

			builder.create().show();

		    }
		    else {
			/*****************************************************************************/
			try {
			    stAvFornServiceCall.executeHttpsGet(Constants.URL_FORNITURE_GAS_ATTIVE, parametri, (Home) Home.getContext(), Constants.FUNZIONALITA_AUTOLETTURA, OopsPageAutolettura.this);
			}
			catch (Exception e) {
			    Intent intentOops = new Intent(getApplicationContext(), OopsPageAutolettura.class);
			    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
			    intentOops.putExtra("PARAMETRI", parametri);
			    startActivity(intentOops);
			    OopsPageAutolettura.this.finish();
			    overridePendingTransition(R.anim.fade, R.anim.hold);
			}
			/************************************************************************************/
			// sorpassando loadFornitureInfoGas
			/*
			 * Intent intentStoricoLetture = new
			 * Intent(getApplicationContext(),StoricoLetture.class);
			 * Home.this.finish();
			 * startActivity(intentStoricoLetture);
			 * overridePendingTransition(R.anim.fade, R.anim.hold);
			 */
		    }

		}

	    }
	});
	final EniFont messaggio = (EniFont) findViewById(R.id.oops_messaggio);
	messaggio.setText(messaggioString);
    }

    private boolean isNetworkAvailable(Context context) {
	ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
	if (connectivityManager == null) return false;
	NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
	return activeNetworkInfo != null;
    }

    public void onBackPressed() {
	OopsPageAutolettura.this.setResult(OopsPageAutolettura.ESCI);
	OopsPageAutolettura.this.finish();
    }
}
