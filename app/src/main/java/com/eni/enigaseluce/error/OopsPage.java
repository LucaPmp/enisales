package com.eni.enigaseluce.error;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.AnimationDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.eni.enigaseluce.R;
import com.eni.enigaseluce.font.EniFont;
import com.eni.enigaseluce.login.Login;
import com.eni.enigaseluce.utils.Utilities;

@SuppressLint("NewApi")
public class OopsPage extends Activity {

    public final static int ESCI = 1;
    AnimationDrawable animation;
    ImageView imageAnim;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.oops_page);
	Intent intent = getIntent();
	String messaggioString = intent.getStringExtra("MESSAGGIO");

	final ImageView indietro = (ImageView) findViewById(R.id.oops_indietro);
	indietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
	indietro.getBackground().invalidateSelf();
	indietro.setOnClickListener(new View.OnClickListener() {
	    public void onClick(View v1) {
		indietro.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
		indietro.getBackground().invalidateSelf();
		OopsPage.this.setResult(OopsPage.ESCI);
		OopsPage.this.finish();
	    }
	});
	final ImageView riprova = (ImageView) findViewById(R.id.oops_riprova);
	riprova.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
	riprova.getBackground().invalidateSelf();
	riprova.setOnClickListener(new View.OnClickListener() {
	    public void onClick(View v1) {
		riprova.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
		riprova.getBackground().invalidateSelf();
		// quando l'utente clicka si deve rieseguire l'ultima chiamata
		// fatta
		// OopsPage.this.setResult(OopsPage.ESCI);
		// OopsPage.this.finish();
		if (isNetworkAvailable(OopsPage.this)) {

		    animation = Utilities.animation(OopsPage.this);
		    imageAnim = (ImageView) findViewById(R.id.waiting_anim);
		    imageAnim.setVisibility(View.VISIBLE);
		    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
			imageAnim.setBackgroundDrawable(animation);
		    }
		    else {
			imageAnim.setBackground(animation);
		    }
		    animation.start();

		    Intent nextIntent = new Intent(getApplicationContext(), Login.class);
		    startActivity(nextIntent);
		    OopsPage.this.finish();
		}
		else {

		    animation = Utilities.animation(OopsPage.this);
		    imageAnim = (ImageView) findViewById(R.id.waiting_anim);
		    imageAnim.setVisibility(View.VISIBLE);
		    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
			imageAnim.setBackgroundDrawable(animation);
		    }
		    else {
			imageAnim.setBackground(animation);
		    }
		    animation.start();

		    Intent nextIntent = new Intent(getApplicationContext(), OopsPage.class);
		    nextIntent.putExtra("MESSAGGIO", getResources().getString(R.string.connessione_assente));
		    startActivity(nextIntent);
		    OopsPage.this.finish();
		}

	    }
	});
	final EniFont messaggio = (EniFont) findViewById(R.id.oops_messaggio);
	messaggio.setText(messaggioString);
    }

    public void onBackPressed() {
	OopsPage.this.setResult(OopsPage.ESCI);
	OopsPage.this.finish();
    }

    private boolean isNetworkAvailable(Context context) {
	ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
	if (connectivityManager == null) return false;
	NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
	return (activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting());
    }
}
