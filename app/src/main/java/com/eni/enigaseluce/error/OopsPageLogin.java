package com.eni.enigaseluce.error;

import java.util.List;

import org.apache.http.cookie.Cookie;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.graphics.drawable.AnimationDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.eni.enigaseluce.R;
import com.eni.enigaseluce.font.EniFont;
import com.eni.enigaseluce.home.Home;
import com.eni.enigaseluce.httpcall.Constants;
import com.eni.enigaseluce.httpcall.ServiceCall;
import com.eni.enigaseluce.httpcall.ServiceCallFactory;
import com.eni.enigaseluce.login.Login;
import com.eni.enigaseluce.utils.Utilities;

@SuppressLint("NewApi")
public class OopsPageLogin extends Activity {
    public final static int ESCI = 1;
    private Context context;
    AnimationDrawable animation;
    ImageView imageAnim;
    String username = "";
    String password = "";
    boolean checkbox = false;
    SharedPreferences settings;
    SharedPreferences.Editor editor;
    String userStored;
    String passwordStored;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.oops_page);
	context = this;

	settings = getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
	editor = settings.edit();

	userStored = settings.getString("User", "");
	passwordStored = settings.getString("Password", "");

	Intent intent = getIntent();
	String messaggioString = intent.getStringExtra("MESSAGGIO");
	username = intent.getStringExtra("USERNAME");
	password = intent.getStringExtra("PASSWORD");
	checkbox = intent.getBooleanExtra("CHECKBOX", false);

	final ImageView indietro = (ImageView) findViewById(R.id.oops_indietro);
	indietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
	indietro.getBackground().invalidateSelf();
	indietro.setOnClickListener(new View.OnClickListener() {
	    public void onClick(View v1) {
		indietro.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
		indietro.getBackground().invalidateSelf();
		Intent intentLogin = new Intent(getApplicationContext(), Login.class);
		intentLogin.putExtra("USERNAME", username);
		intentLogin.putExtra("PASSWORD", password);
		intentLogin.putExtra("CHECKBOX", checkbox);
		OopsPageLogin.this.finish();
		startActivity(intentLogin);
		overridePendingTransition(R.anim.fade, R.anim.hold);
	    }
	});
	final ImageView riprova = (ImageView) findViewById(R.id.oops_riprova);
	riprova.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
	riprova.getBackground().invalidateSelf();
	riprova.setOnClickListener(new View.OnClickListener() {
	    public void onClick(View v1) {
		riprova.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
		riprova.getBackground().invalidateSelf();
		// quando l'utente clicka si deve rieseguire l'ultima chiamata
		// fatta

		animation = Utilities.animation(OopsPageLogin.this);
		imageAnim = (ImageView) findViewById(R.id.waiting_anim);
		imageAnim.setVisibility(View.VISIBLE);
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
		    imageAnim.setBackgroundDrawable(animation);
		}
		else {
		    imageAnim.setBackground(animation);
		}
		animation.start();

		if (isNetworkAvailable(context)) {
		    /********************* Chiamata Login ******************************/
		    ServiceCall loginServiceCall = ServiceCallFactory.createServiceCall(context);
		    // loginServiceCall.myCookieStore = new
		    // PersistentCookieStore(context);
		    String rispostaLogin = "";
		    try {
//			rispostaLogin = loginServiceCall.executeHttpsPostAuth(Constants.URL_AUTH, username, password, OopsPageLogin.this);

				if(Constants.TEST_ENV) {
					rispostaLogin = loginServiceCall.executeHttpsPostAuth(Constants.URL_LOGIN_TEST, username, password, OopsPageLogin.this);
				}
				else {
					rispostaLogin = loginServiceCall.executeHttpsPostAuth(Constants.URL_LOGIN_PROD, username, password, OopsPageLogin.this);
				}

		    }
		    catch (Exception e) {
			Intent intentOops = new Intent(getApplicationContext(), OopsPageLogin.class);
			intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
			intentOops.putExtra("USERNAME", username);
			intentOops.putExtra("PASSWORD", password);
			OopsPageLogin.this.finish();
			startActivity(intentOops);
			overridePendingTransition(R.anim.fade, R.anim.hold);
		    }
		    /*****************************************************************/
		}
		else {
		    Intent intentOops = new Intent(getApplicationContext(), OopsPageLogin.class);
		    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.connessione_assente));
		    intentOops.putExtra("USERNAME", username);
		    intentOops.putExtra("PASSWORD", password);
		    OopsPageLogin.this.finish();
		    startActivity(intentOops);
		    overridePendingTransition(R.anim.fade, R.anim.hold);
		}

	    }
	});
	final EniFont messaggio = (EniFont) findViewById(R.id.oops_messaggio);
	messaggio.setText(messaggioString);
    }

    private boolean isNetworkAvailable(Context context) {
	ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
	if (connectivityManager == null) return false;
	NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
	return activeNetworkInfo != null;
    }

    public void postAuth(String rispostaLogin, List<Cookie> lista) {
	if (animation != null) {
	    animation.stop();
	    imageAnim.setVisibility(View.INVISIBLE);
	}
	JSONArray rispostaJSON = new JSONArray();
	JSONObject oggettoRisposta = new JSONObject();
	String esito = "";
	String messaggio = "";
	// se si ha pagina di authentication failed
	if (rispostaLogin.contains("Authentication Failed")) {
	    ImageButton login_help_btn1 = (ImageButton) findViewById(R.id.login_help_btn);
	    login_help_btn1.setVisibility(View.VISIBLE);
	    return;
	}
	try {
	    rispostaJSON = new JSONArray(rispostaLogin);
	    oggettoRisposta = (JSONObject) rispostaJSON.get(0);
	    esito = oggettoRisposta.getString("esito");
	    messaggio = oggettoRisposta.getString("descEsito");
	}
	catch (JSONException e) {
	    // gestire le risposte non JSON (non andate a buon fine)
	    Intent intentOops = new Intent(getApplicationContext(), OopsPageLogin.class);
	    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
	    intentOops.putExtra("USERNAME", username);
	    intentOops.putExtra("PASSWORD", password);
	    intentOops.putExtra("CHECKBOX", checkbox);
	    OopsPageLogin.this.finish();
	    startActivity(intentOops);
	    overridePendingTransition(R.anim.fade, R.anim.hold);
	    return;
	}
	if (!esito.equals("")) {

	    if (!esito.equals("200")) {
		// checkbox
		if (!checkbox && (!userStored.equals("") || !passwordStored.equals(""))) {
		    editor.remove("User");
		    editor.remove("Password");
		    editor.commit();
		}
		// codici diversi da 200 devono far portare alla oops page
		if (!messaggio.equals("")) {
		    Intent intentOops = new Intent(getApplicationContext(), OopsPageLogin.class);
		    intentOops.putExtra("MESSAGGIO", messaggio);
		    intentOops.putExtra("USERNAME", username);
		    intentOops.putExtra("PASSWORD", password);
		    intentOops.putExtra("CHECKBOX", checkbox);
		    OopsPageLogin.this.finish();
		    startActivity(intentOops);
		    overridePendingTransition(R.anim.fade, R.anim.hold);
		}
		else {

		    Intent intentOops = new Intent(getApplicationContext(), OopsPageLogin.class);
		    intentOops.putExtra("USERNAME", username);
		    intentOops.putExtra("PASSWORD", password);
		    intentOops.putExtra("CHECKBOX", checkbox);

		    // errori ops page
		    if (esito.equals("309")) {
			intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
			OopsPageLogin.this.finish();
			startActivity(intentOops);
			overridePendingTransition(R.anim.fade, R.anim.hold);

		    }
		    else if (esito.equals("400")) {
			intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
			OopsPageLogin.this.finish();
			startActivity(intentOops);
			overridePendingTransition(R.anim.fade, R.anim.hold);

		    }
		    else if (esito.equals("420")) {
			intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
			OopsPageLogin.this.finish();
			startActivity(intentOops);
			overridePendingTransition(R.anim.fade, R.anim.hold);

		    }
		    else if (esito.equals("449")) {
			intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
			OopsPageLogin.this.finish();
			startActivity(intentOops);
			overridePendingTransition(R.anim.fade, R.anim.hold);

		    }
		    else if (esito.equals("451")) {
			intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_popup_451));
			OopsPageLogin.this.finish();
			startActivity(intentOops);
			overridePendingTransition(R.anim.fade, R.anim.hold);

		    }
		    // errori popup
		    else if (esito.equals("307")) {
			Utils.popup(OopsPageLogin.this, getResources().getString(R.string.errore_popup_307), "Riprova");

		    }
		    else if (esito.equals("435")) {
			Utils.popup(OopsPageLogin.this, getResources().getString(R.string.errore_popup_435), "Riprova");

		    }
		    else if (esito.equals("437")) {
			Utils.popup(OopsPageLogin.this, getResources().getString(R.string.errore_popup_437), "Riprova");

		    }
		    else if (esito.equals("438")) {
			Utils.popup(OopsPageLogin.this, getResources().getString(R.string.errore_popup_438), "Riprova");

		    }
		    else if (esito.equals("443")) {
			Utils.popup(OopsPageLogin.this, getResources().getString(R.string.errore_popup_443), "Riprova");

		    }
		    else if (esito.equals("444")) {
			Utils.popup(OopsPageLogin.this, getResources().getString(R.string.errore_popup_444), "Riprova");

		    }
		    else if (esito.equals("445")) {
			Utils.popup(OopsPageLogin.this, getResources().getString(R.string.errore_popup_445), "Riprova");

		    }
		    else if (esito.equals("446")) {
			Utils.popup(OopsPageLogin.this, getResources().getString(R.string.errore_popup_446), "Riprova");

		    }
		    else if (esito.equals("447")) {
			Utils.popup(OopsPageLogin.this, getResources().getString(R.string.errore_popup_447), "Riprova");
		    }
		    else {
			intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
			startActivity(intentOops);
			overridePendingTransition(R.anim.fade, R.anim.hold);
		    }

		}
	    }
	    else {
		// memorizzo la risposta in memoria
		Constants.SERVIZIO_LOGIN = rispostaJSON.toString();
		Constants.LISTA_COOKIE = lista;
		userStored = settings.getString("User", "");
		passwordStored = settings.getString("Password", "");
		// checkbox
		if (checkbox && (!username.equals("") && !password.equals(""))) {
		    editor.putString("User", username);
		    editor.putString("Password", password);
		    editor.commit();
		}
		else {
		    if (!userStored.equals("") || !passwordStored.equals("")) {
			editor.remove("User");
			editor.remove("Password");
			editor.commit();
		    }
		}
		Intent intentHome = new Intent(getApplicationContext(), Home.class);
		OopsPageLogin.this.finish();
		startActivity(intentHome);
		overridePendingTransition(R.anim.fade, R.anim.hold);
	    }
	}
    }

    public void postLoginException(boolean exception) {
	if (exception) {
	    if (animation != null) {
		animation.stop();
		imageAnim.setVisibility(View.INVISIBLE);
	    }
	    Intent intentOops = new Intent(getApplicationContext(), OopsPageLogin.class);
	    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
	    OopsPageLogin.this.finish();
	    startActivityForResult(intentOops, OopsPageLogin.ESCI);
	    overridePendingTransition(R.anim.fade, R.anim.hold);
	}
    }

    public void onBackPressed() {
	Intent intentLogin = new Intent(getApplicationContext(), Login.class);
	intentLogin.putExtra("USERNAME", username);
	intentLogin.putExtra("PASSWORD", password);
	intentLogin.putExtra("CHECKBOX", checkbox);
	OopsPageLogin.this.finish();
	startActivity(intentLogin);
	overridePendingTransition(R.anim.fade, R.anim.hold);
    }
}
