package com.eni.enigaseluce.error;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.eni.enigaseluce.R;
import com.eni.enigaseluce.font.EniFont;
import com.eni.enigaseluce.login.Login;

@SuppressLint("NewApi")
public class OopsPageSessioneScaduta extends Activity {
    public final static int ESCI = 1;
    AnimationDrawable animation;
    ImageView imageAnim;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.oops_page);
	
		Intent intent = getIntent();
		String messaggioString = intent.getStringExtra("MESSAGGIO");
	
		final ImageView indietro = (ImageView) findViewById(R.id.oops_indietro);
		indietro.setImageResource(R.drawable.esci);
		indietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
		indietro.getBackground().invalidateSelf();
		indietro.setOnClickListener(new View.OnClickListener() {
		    public void onClick(View v1) {
			indietro.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
			indietro.getBackground().invalidateSelf();
			Intent intentLogin = new Intent(getApplicationContext(), Login.class);
			OopsPageSessioneScaduta.this.finish();
			startActivity(intentLogin);
			overridePendingTransition(R.anim.fade, R.anim.hold);
		    }
		});
		final ImageView riprova = (ImageView) findViewById(R.id.oops_riprova);
		riprova.setVisibility(View.INVISIBLE);
		
		final EniFont messaggio = (EniFont) findViewById(R.id.oops_messaggio);
		messaggio.setText(messaggioString);
    }

    public void onBackPressed() {
		Intent intentLogin = new Intent(getApplicationContext(), Login.class);
		OopsPageSessioneScaduta.this.finish();
		startActivity(intentLogin);
		overridePendingTransition(R.anim.fade, R.anim.hold);
    }
}
