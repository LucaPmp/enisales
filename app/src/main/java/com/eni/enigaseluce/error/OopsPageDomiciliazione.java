package com.eni.enigaseluce.error;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.graphics.drawable.AnimationDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.eni.enigaseluce.R;
import com.eni.enigaseluce.font.EniFont;
import com.eni.enigaseluce.home.Home;
import com.eni.enigaseluce.httpcall.Constants;
import com.eni.enigaseluce.httpcall.ServiceCall;
import com.eni.enigaseluce.httpcall.ServiceCallFactory;

@SuppressLint("NewApi")
public class OopsPageDomiciliazione extends Activity {
    public final static int ESCI = 1;
    private Context context;
    AnimationDrawable animation;
    ImageView imageAnim;
    boolean checkbox = false;
    SharedPreferences settings;
    SharedPreferences.Editor editor;
    String userStored;
    String passwordStored;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.oops_page);
	context = this;

	Intent intent = getIntent();
	String messaggioString = intent.getStringExtra("MESSAGGIO");
	final String parameter = intent.getStringExtra("PARAMETER");

	final ImageView indietro = (ImageView) findViewById(R.id.oops_indietro);
	indietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
	indietro.getBackground().invalidateSelf();
	indietro.setOnClickListener(new View.OnClickListener() {
	    public void onClick(View v1) {
		indietro.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
		indietro.getBackground().invalidateSelf();
		OopsPageDomiciliazione.this.setResult(OopsPageDomiciliazione.ESCI);
		OopsPageDomiciliazione.this.finish();
	    }
	});
	final ImageView riprova = (ImageView) findViewById(R.id.oops_riprova);
	riprova.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
	riprova.getBackground().invalidateSelf();
	riprova.setOnClickListener(new View.OnClickListener() {
	    public void onClick(View v1) {
		riprova.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
		riprova.getBackground().invalidateSelf();
		animation = new AnimationDrawable();
		animation.addFrame(getResources().getDrawable(R.drawable.anim_load2), 300);
		animation.addFrame(getResources().getDrawable(R.drawable.anim_load3), 300);
		animation.addFrame(getResources().getDrawable(R.drawable.anim_load4), 300);
		animation.setOneShot(false);
		imageAnim = (ImageView) findViewById(R.id.waiting_anim);
		imageAnim.setVisibility(View.VISIBLE);
		if (Build.VERSION.SDK_INT < 16) {
		    imageAnim.setBackgroundDrawable(animation);
		}
		else {
		    imageAnim.setBackground(animation);
		}
		// start the animation!
		animation.start();

		if (!isNetworkAvailable(context)) {
		    Intent intentOops = new Intent(getApplicationContext(), OopsPageDomiciliazione.class);
		    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.connessione_assente));
		    intentOops.putExtra("PARAMETER", parameter);
		    OopsPageDomiciliazione.this.finish();
		    startActivity(intentOops);
		    overridePendingTransition(R.anim.fade, R.anim.hold);
		}
		else {

		    /********************* Chiamata visualizza Forniture Domiciliazione ******************************/
		    ServiceCall visFornDomicilServiceCall = ServiceCallFactory.createServiceCall(context);

		    try {
			visFornDomicilServiceCall.executeHttpsGet(Constants.URL_VISUALIZZA_FORNITURE_DOMICILIAZIONE, parameter, (Home) Home.getContext(), Constants.FUNZIONALITA_DOMICILIAZIONE, OopsPageDomiciliazione.this);
		    }
		    catch (Exception e) {
			Intent intentOops = new Intent(getApplicationContext(), OopsPageDomiciliazione.class);
			intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
			intentOops.putExtra("PARAMETER", parameter);
			OopsPageDomiciliazione.this.finish();
			startActivity(intentOops);
			overridePendingTransition(R.anim.fade, R.anim.hold);
		    }
		    /**********************************************************************************/
		    // sorpassando lo visualizzaFornitureDomiciliazione
		    /*
		     * Intent intentDomiciliazione = new
		     * Intent(getApplicationContext(), Domiciliazione.class);
		     * Home.this.finish(); startActivity(intentDomiciliazione);
		     * overridePendingTransition(R.anim.fade, R.anim.hold);
		     */
		}
	    }
	});
	final EniFont messaggio = (EniFont) findViewById(R.id.oops_messaggio);
	messaggio.setText(messaggioString);
    }

    private boolean isNetworkAvailable(Context context) {
	ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
	if (connectivityManager == null) return false;
	NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
	return activeNetworkInfo != null;
    }

    public void onBackPressed() {
	OopsPageDomiciliazione.this.setResult(OopsPageDomiciliazione.ESCI);
	OopsPageDomiciliazione.this.finish();
    }
}
