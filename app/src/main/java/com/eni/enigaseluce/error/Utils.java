package com.eni.enigaseluce.error;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

public class Utils {

    public static void popup(Context context, final String message, final String buttonText) {

	AlertDialog.Builder builder = new AlertDialog.Builder(context);
	builder.setMessage(message).setCancelable(false).setPositiveButton(buttonText, new DialogInterface.OnClickListener() {
	    public void onClick(DialogInterface dialog, int id) {

	    }
	});

	builder.create().show();

    }

}
