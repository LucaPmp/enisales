package com.eni.enigaseluce.error;

import java.util.HashMap;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.graphics.drawable.AnimationDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.eni.enigaseluce.R;
import com.eni.enigaseluce.domiciliazione.DomiciliazioneInserimentoNonResidenziali;
import com.eni.enigaseluce.domiciliazione.DomiciliazioneInserimentoPersonaFisica;
import com.eni.enigaseluce.domiciliazione.DomiciliazioneInserimentoPersonaGiuridica;
import com.eni.enigaseluce.font.EniFont;
import com.eni.enigaseluce.httpcall.Constants;
import com.eni.enigaseluce.httpcall.ServiceCall;
import com.eni.enigaseluce.httpcall.ServiceCallFactory;
import com.eni.enigaseluce.utils.Utilities;

@SuppressLint("NewApi")
public class OopsPageDomiciliazioneInserimento extends Activity {
    public final static int ESCI = 1;
    private Context context;
    AnimationDrawable animation;
    ImageView imageAnim;
    boolean checkbox = false;
    SharedPreferences settings;
    SharedPreferences.Editor editor;
    String userStored;
    String passwordStored;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.oops_page);
	context = this;

	final Intent intent = getIntent();
	final String tipologiaUtenza = intent.getStringExtra(Constants.TIPOLOGIA_UTENZA);
	final String messaggioString = intent.getStringExtra(Constants.MESSAGGIO);
	final String codiceCliente = intent.getStringExtra(Constants.CODICE_CLIENTE);
	final String codiceConto = intent.getStringExtra(Constants.CODICE_CONTO);
	final String codiceFiscaleClienteCC = intent.getStringExtra(Constants.CODICE_FISCALE_CLIENTE_CC);
	final String nomeClienteCC = intent.getStringExtra(Constants.NOME_CLIENTE_CC);
	final String cognomeClienteCC = intent.getStringExtra(Constants.COGNOME_CLIENTE_CC);
	final String ibanClienteCC = intent.getStringExtra(Constants.IBAN_CLIENTE_CC);
	final String userId = intent.getStringExtra(Constants.USER_ID);
	final String nomeCliente = intent.getStringExtra(Constants.NOME_CLIENTE);
	final String cognomeCliente = intent.getStringExtra(Constants.COGNOME_CLIENTE);
	final String codiceFiscaleCliente = intent.getStringExtra(Constants.CODICE_FISCALE_CLIENTE);
	final String tipoIndirizzo = intent.getStringExtra(Constants.TIPO_INDIRIZZO);
	final String strada = intent.getStringExtra(Constants.STRADA);
	final String numCivico = intent.getStringExtra(Constants.NUM_CIVICO);
	final String estensioneNumCivico = intent.getStringExtra(Constants.ESTENSIONE_NUM_CIVICO);
	final String citta = intent.getStringExtra(Constants.CITTA);
	final String cap = intent.getStringExtra(Constants.CAP);
	final String provincia = intent.getStringExtra(Constants.PROVINCIA);
	final String cf_Modificato = intent.getStringExtra(Constants.CF_MODIFICATO) != null ? intent.getStringExtra(Constants.CF_MODIFICATO) : null;
	final String piva_Modificata = intent.getStringExtra(Constants.PIVA_MODIFICATA) != null ? intent.getStringExtra(Constants.PIVA_MODIFICATA) : null;

	final ImageView indietro = (ImageView) findViewById(R.id.oops_indietro);
	indietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
	indietro.getBackground().invalidateSelf();
	indietro.setOnClickListener(new View.OnClickListener() {
	    public void onClick(View v1) {
		indietro.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
		indietro.getBackground().invalidateSelf();
		OopsPageDomiciliazioneInserimento.this.setResult(OopsPageDomiciliazioneInserimento.ESCI);
		OopsPageDomiciliazioneInserimento.this.finish();
	    }
	});
	final ImageView riprova = (ImageView) findViewById(R.id.oops_riprova);
	riprova.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
	riprova.getBackground().invalidateSelf();
	riprova.setOnClickListener(new View.OnClickListener() {
	    public void onClick(View v1) {
		riprova.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
		riprova.getBackground().invalidateSelf();
		animation = new AnimationDrawable();
		animation.addFrame(getResources().getDrawable(R.drawable.anim_load2), 300);
		animation.addFrame(getResources().getDrawable(R.drawable.anim_load3), 300);
		animation.addFrame(getResources().getDrawable(R.drawable.anim_load4), 300);
		animation.setOneShot(false);
		imageAnim = (ImageView) findViewById(R.id.waiting_anim);
		imageAnim.setVisibility(View.VISIBLE);
		if (Build.VERSION.SDK_INT < 16) {
		    imageAnim.setBackgroundDrawable(animation);
		}
		else {
		    imageAnim.setBackground(animation);
		}
		// start the animation!
		animation.start();
		if (!isNetworkAvailable(context)) {
		    Intent intentOops = new Intent(getApplicationContext(), OopsPageDomiciliazioneInserimento.class);
		    intentOops.putExtra(Constants.TIPOLOGIA_UTENZA, tipologiaUtenza);
		    intentOops.putExtra(Constants.MESSAGGIO, getResources().getString(R.string.connessione_assente));
		    intentOops.putExtra(Constants.CODICE_CLIENTE, codiceCliente);
		    intentOops.putExtra(Constants.CODICE_CONTO, codiceConto);
		    intentOops.putExtra(Constants.CODICE_FISCALE_CLIENTE_CC, codiceFiscaleClienteCC);
		    intentOops.putExtra(Constants.NOME_CLIENTE_CC, nomeClienteCC);
		    intentOops.putExtra(Constants.COGNOME_CLIENTE_CC, cognomeClienteCC);
		    intentOops.putExtra(Constants.IBAN_CLIENTE_CC, ibanClienteCC);
		    intentOops.putExtra(Constants.USER_ID, userId);
		    intentOops.putExtra(Constants.NOME_CLIENTE, nomeCliente);
		    intentOops.putExtra(Constants.COGNOME_CLIENTE, cognomeCliente);
		    intentOops.putExtra(Constants.CODICE_FISCALE_CLIENTE, codiceFiscaleCliente);
		    intentOops.putExtra(Constants.PIVA_MODIFICATA, piva_Modificata);
		    intentOops.putExtra(Constants.CF_MODIFICATO, cf_Modificato);

		    if (!tipoIndirizzo.equals("") || tipoIndirizzo != null) {
			intentOops.putExtra(Constants.TIPO_INDIRIZZO, tipoIndirizzo);
		    }
		    if (!strada.equals("") || strada != null) {
			intentOops.putExtra(Constants.STRADA, strada);
		    }
		    if (!numCivico.equals("")) {
			intentOops.putExtra(Constants.NUM_CIVICO, numCivico);
		    }
		    if (!estensioneNumCivico.equals("")) {
			intentOops.putExtra(Constants.ESTENSIONE_NUM_CIVICO, estensioneNumCivico);
		    }
		    if (!citta.equals("")) {
			intentOops.putExtra(Constants.CITTA, citta);
		    }
		    if (!cap.equals("")) {
			intentOops.putExtra(Constants.CAP, cap);
		    }
		    if (!provincia.equals("")) {
			intentOops.putExtra(Constants.PROVINCIA, provincia);
		    }

		    OopsPageDomiciliazioneInserimento.this.finish();
		    startActivity(intentOops);
		    overridePendingTransition(R.anim.fade, R.anim.hold);
		}
		else {
		    /********************* Chiamata execute domiciliazione conto Fornitura ******************************/

		    ServiceCall stAvFornServiceCall = ServiceCallFactory.createServiceCall(context);

		    // Parametri obbligatori: codiceCliente, codiceConto,
		    // nomeClienteCC,cognomeClienteCC,codiceFiscaleClienteCC,ibanClienteCC,userId,
		    // nomeCliente,cognomeCliente,codiceFiscaleCliente
		    // parametri opzionali: tipoIndirizzo, strada, numCivico,
		    // estensioneNumCivico, citta, cap, provincia

		    HashMap<String, String> parametri = new HashMap<String, String>();
		    parametri.put(Constants.CODICE_CLIENTE, codiceCliente);
		    parametri.put(Constants.CODICE_CONTO, codiceConto);
		    parametri.put(Constants.CODICE_FISCALE_CLIENTE_CC, codiceFiscaleClienteCC);
		    parametri.put(Constants.NOME_CLIENTE_CC, nomeClienteCC);
		    parametri.put(Constants.COGNOME_CLIENTE_CC, cognomeClienteCC);
		    parametri.put(Constants.IBAN_CLIENTE_CC, ibanClienteCC);
		    parametri.put(Constants.USER_ID, userId);
		    parametri.put(Constants.NOME_CLIENTE, nomeCliente);
		    parametri.put(Constants.COGNOME_CLIENTE, cognomeCliente);
		    parametri.put(Constants.CODICE_FISCALE_CLIENTE, codiceFiscaleCliente);
		    parametri.put(Constants.CF_MODIFICATO, cf_Modificato);
		    parametri.put(Constants.PIVA_MODIFICATA, piva_Modificata);
		    if (tipoIndirizzo != null && !tipoIndirizzo.equals("")) {
			parametri.put(Constants.TIPO_INDIRIZZO, tipoIndirizzo);
		    }
		    if (strada != null && !strada.equals("")) {
			parametri.put(Constants.STRADA, strada);
		    }
		    if (numCivico != null && !numCivico.equals("")) {
			parametri.put(Constants.NUM_CIVICO, numCivico);
		    }
		    if (estensioneNumCivico != null && !estensioneNumCivico.equals("")) {
			parametri.put(Constants.ESTENSIONE_NUM_CIVICO, estensioneNumCivico);
		    }
		    if (citta != null && !citta.equals("")) {
			parametri.put(Constants.CITTA, citta);
		    }
		    if (cap != null && !cap.equals("")) {
			parametri.put(Constants.CAP, cap);
		    }
		    if (provincia != null && !provincia.equals("")) {
			parametri.put(Constants.PROVINCIA, provincia);
		    }

		    try {
			if (intent.getStringExtra(Constants.TIPOLOGIA_UTENZA).equals("NON_RESIDENZIALI")) stAvFornServiceCall.executeHttpsGetDomiciliazione(Constants.URL_EXECUTE_DOMICILIAZIONE_CONTO, null, parametri, (DomiciliazioneInserimentoNonResidenziali) DomiciliazioneInserimentoNonResidenziali.getContext(), 1, OopsPageDomiciliazioneInserimento.this);
			else if (intent.getStringExtra(Constants.TIPOLOGIA_UTENZA).equals("PERSONA_FISICA")) stAvFornServiceCall.executeHttpsGetDomiciliazione(Constants.URL_EXECUTE_DOMICILIAZIONE_CONTO, null, parametri, (DomiciliazioneInserimentoPersonaFisica) DomiciliazioneInserimentoPersonaFisica.getContext(), 1, OopsPageDomiciliazioneInserimento.this);
			else
			    stAvFornServiceCall.executeHttpsGetDomiciliazione(Constants.URL_EXECUTE_DOMICILIAZIONE_CONTO, null, parametri, (DomiciliazioneInserimentoPersonaGiuridica) DomiciliazioneInserimentoPersonaGiuridica.getContext(), 1, OopsPageDomiciliazioneInserimento.this);
		    }
		    catch (Exception e) {

			Intent intentOops = new Intent(getApplicationContext(), OopsPageDomiciliazioneInserimento.class);
			intentOops.putExtra(Constants.TIPOLOGIA_UTENZA, tipologiaUtenza);
			intentOops.putExtra(Constants.MESSAGGIO, getResources().getString(R.string.connessione_assente));
			intentOops.putExtra(Constants.CODICE_CLIENTE, codiceCliente);
			intentOops.putExtra(Constants.CODICE_CONTO, codiceConto);
			intentOops.putExtra(Constants.CODICE_FISCALE_CLIENTE_CC, codiceFiscaleClienteCC);
			intentOops.putExtra(Constants.NOME_CLIENTE_CC, nomeClienteCC);
			intentOops.putExtra(Constants.COGNOME_CLIENTE_CC, cognomeClienteCC);
			intentOops.putExtra(Constants.IBAN_CLIENTE_CC, ibanClienteCC);
			intentOops.putExtra(Constants.USER_ID, userId);
			intentOops.putExtra(Constants.NOME_CLIENTE, nomeCliente);
			intentOops.putExtra(Constants.COGNOME_CLIENTE, cognomeCliente);
			intentOops.putExtra(Constants.CODICE_FISCALE_CLIENTE, codiceFiscaleCliente);
			intentOops.putExtra(Constants.PIVA_MODIFICATA, piva_Modificata);
			intentOops.putExtra(Constants.CF_MODIFICATO, cf_Modificato);

			if (tipoIndirizzo != null) {
			    intentOops.putExtra(Constants.TIPO_INDIRIZZO, tipoIndirizzo);
			}
			if (Utilities.isNotEmptyString(strada)) {
			    intentOops.putExtra(Constants.STRADA, strada);
			}
			if (Utilities.isNotEmptyString(numCivico)) {
			    intentOops.putExtra(Constants.NUM_CIVICO, numCivico);
			}
			if (Utilities.isNotEmptyString(estensioneNumCivico)) {
			    intentOops.putExtra(Constants.ESTENSIONE_NUM_CIVICO, estensioneNumCivico);
			}
			if (Utilities.isNotEmptyString(citta)) {
			    intentOops.putExtra(Constants.CITTA, citta);
			}
			if (Utilities.isNotEmptyString(cap)) {
			    intentOops.putExtra(Constants.CAP, cap);
			}
			if (Utilities.isNotEmptyString(provincia)) {
			    intentOops.putExtra(Constants.PROVINCIA, provincia);
			}

			OopsPageDomiciliazioneInserimento.this.finish();
			startActivity(intentOops);
			overridePendingTransition(R.anim.fade, R.anim.hold);
		    }
		    // sorpassando la chiamata
		    /*
		     * AlertDialog.Builder builder = new
		     * AlertDialog.Builder(context);
		     * builder.setMessage(R.string.
		     * chiamata_OK).setCancelable(false).setPositiveButton("OK",
		     * new DialogInterface.OnClickListener() { public void
		     * onClick(DialogInterface dialog, int id) { //chiamata
		     * visualizza domiciliazione fornitura e vado alla schermata
		     * Domiciliazione chiamataVisualizzaDomiciliazione(); } });
		     * builder.create().show();
		     */
		}

	    }
	});
	final EniFont messaggio = (EniFont) findViewById(R.id.oops_messaggio);
	messaggio.setText(messaggioString);
    }

    private boolean isNetworkAvailable(Context context) {
	ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
	if (connectivityManager == null) return false;
	NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
	return activeNetworkInfo != null;
    }

    public void onBackPressed() {
	OopsPageDomiciliazioneInserimento.this.setResult(OopsPageDomiciliazioneInserimento.ESCI);
	OopsPageDomiciliazioneInserimento.this.finish();
    }
}
