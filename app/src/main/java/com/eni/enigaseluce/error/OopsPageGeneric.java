package com.eni.enigaseluce.error;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.eni.enigaseluce.EniApplication;
import com.eni.enigaseluce.R;
import com.eni.enigaseluce.font.EniFont;
import com.eni.enigaseluce.home.Home;
import com.eni.enigaseluce.httpcall.Constants;
import com.eni.enigaseluce.httpcall.ServiceCall;
import com.eni.enigaseluce.httpcall.ServiceCallFactory;
import com.eni.enigaseluce.utils.Utilities;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.util.HashMap;

/**
 * Created by luca.quaranta on 21/10/2015.
 */
public class OopsPageGeneric extends OopsPage {

    private ImageView indietro, riprova;
    private EniFont messaggio;
    private int errorType, previousPage;
    private String codiceCliente, primoConto;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.oops_page);

        errorType = getIntent().getIntExtra(Constants.ERROR_TYPE, -1);
        previousPage = getIntent().getIntExtra(Constants.PREVIOUS_PAGE, -1);

        switch (previousPage) {
            case Constants.PAGE_STATO_ATTIVAZIONE_FORNITURA:
                codiceCliente = getIntent().getStringExtra("CODICE_CLIENTE");
                primoConto = getIntent().getStringExtra("CODICE_CONTO_CLIENTE");
                break;
        }

        indietro = (ImageView) findViewById(R.id.oops_indietro);
        riprova = (ImageView) findViewById(R.id.oops_riprova);
        messaggio = (EniFont) findViewById(R.id.oops_messaggio);

        indietro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                indietro.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
                indietro.getBackground().invalidateSelf();
                onBackPressed();
            }
        });

        riprova.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                riprova.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
                riprova.getBackground().invalidateSelf();
                retryCallService();
            }
        });

        messaggio.setText(getErrorMessage());
    }

    private String getErrorMessage() {
        String errorMessage = "";

        switch (errorType) {
            case Constants.ERROR_CONNECTION_NOT_AVAILABLE:
                return getResources().getString(R.string.connessione_assente);

            case Constants.ERROR_SERVICE_NOT_AVAILABLE:
                return getResources().getString(R.string.servizio_non_disponibile);
        }

        return errorMessage;
    }

    private void retryCallService() {
        if (!isNetworkAvailable(this)) {
            openOopsPage();
        } else {
            startSpinnerAnimation();
            ServiceCall stAvFornServiceCall = ServiceCallFactory.createServiceCall(this);
            HashMap<String, String> parametri = new HashMap<String, String>();
            try {
                switch (previousPage) {
                    case Constants.PAGE_STATO_ATTIVAZIONE_FORNITURA:
                        parametri.put("codiceCliente", codiceCliente);
                        parametri.put("codiceContoCliente", primoConto);
                        stAvFornServiceCall.executeHttpsGet(Constants.URL_STATO_AVANZAMENTO_FORNITURE, parametri, (Home) Home.getContext(), Constants.FUNZIONALITA_STATO_AVANZAMENTO_FORNITURA, OopsPageGeneric.this);
                        break;
                }
            } catch (Exception e) {
                openOopsPage();
            }
        }
    }

    private void openOopsPage(){
        Intent intentOops = new Intent(this, OopsPageGeneric.class);
        intentOops.putExtra(Constants.ERROR_TYPE, errorType);
        intentOops.putExtra(Constants.PREVIOUS_PAGE, previousPage);
        switch (previousPage) {
            case Constants.PAGE_STATO_ATTIVAZIONE_FORNITURA:
                intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
                intentOops.putExtra("CODICE_CONTO_CLIENTE", primoConto);
                break;
        }
        OopsPageGeneric.this.finish();
        startActivity(intentOops);
        overridePendingTransition(R.anim.fade, R.anim.hold);
    }

    private boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager == null) return false;
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }

    private void startSpinnerAnimation(){
        animation = Utilities.animation(OopsPageGeneric.this);
        imageAnim = (ImageView) findViewById(R.id.waiting_anim);
        imageAnim.setVisibility(View.VISIBLE);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
            imageAnim.setBackgroundDrawable(animation);
        } else {
            imageAnim.setBackground(animation);
        }
        animation.start();
    }

}
