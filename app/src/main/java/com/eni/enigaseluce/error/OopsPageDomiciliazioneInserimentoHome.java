package com.eni.enigaseluce.error;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.graphics.drawable.AnimationDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.eni.enigaseluce.R;
import com.eni.enigaseluce.domiciliazione.DomiciliazioneInserimentoPersonaFisica;
import com.eni.enigaseluce.font.EniFont;
import com.eni.enigaseluce.httpcall.Constants;
import com.eni.enigaseluce.httpcall.ServiceCall;
import com.eni.enigaseluce.httpcall.ServiceCallFactory;

@SuppressLint("NewApi")
public class OopsPageDomiciliazioneInserimentoHome extends Activity {
    public final static int ESCI = 1;
    private Context context;
    AnimationDrawable animation;
    ImageView imageAnim;
    boolean checkbox = false;
    SharedPreferences settings;
    SharedPreferences.Editor editor;
    String userStored;
    String passwordStored;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.oops_page);
	context = this;

	Intent intent = getIntent();
	final String messaggioString = intent.getStringExtra("MESSAGGIO");
	final String parametri = intent.getStringExtra("Parametri");

	final ImageView indietro = (ImageView) findViewById(R.id.oops_indietro);
	indietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
	indietro.getBackground().invalidateSelf();
	indietro.setOnClickListener(new View.OnClickListener() {
	    public void onClick(View v1) {
		indietro.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
		indietro.getBackground().invalidateSelf();
		OopsPageDomiciliazioneInserimentoHome.this.setResult(OopsPageDomiciliazioneInserimentoHome.ESCI);
		OopsPageDomiciliazioneInserimentoHome.this.finish();
	    }
	});
	final ImageView riprova = (ImageView) findViewById(R.id.oops_riprova);
	riprova.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
	riprova.getBackground().invalidateSelf();
	riprova.setOnClickListener(new View.OnClickListener() {
	    public void onClick(View v1) {
		riprova.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
		riprova.getBackground().invalidateSelf();
		animation = new AnimationDrawable();
		animation.addFrame(getResources().getDrawable(R.drawable.anim_load2), 300);
		animation.addFrame(getResources().getDrawable(R.drawable.anim_load3), 300);
		animation.addFrame(getResources().getDrawable(R.drawable.anim_load4), 300);
		animation.setOneShot(false);
		imageAnim = (ImageView) findViewById(R.id.waiting_anim);
		imageAnim.setVisibility(View.VISIBLE);
		if (Build.VERSION.SDK_INT < 16) {
		    imageAnim.setBackgroundDrawable(animation);
		}
		else {
		    imageAnim.setBackground(animation);
		}
		// start the animation!
		animation.start();
		if (!isNetworkAvailable(context)) {
		    Intent intentOops = new Intent(getApplicationContext(), OopsPageDomiciliazioneInserimentoHome.class);
		    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.connessione_assente));
		    intentOops.putExtra("Parametri", parametri);
		    OopsPageDomiciliazioneInserimentoHome.this.finish();
		    startActivity(intentOops);
		    overridePendingTransition(R.anim.fade, R.anim.hold);
		}
		else {
		    /********************* Chiamata execute domiciliazione conto Fornitura ******************************/

		    ServiceCall stAvFornServiceCall = ServiceCallFactory.createServiceCall(context);

		    try {
			stAvFornServiceCall.executeHttpsGetDomiciliazione(Constants.URL_EXECUTE_DOMICILIAZIONE_CONTO, parametri, null, (DomiciliazioneInserimentoPersonaFisica) DomiciliazioneInserimentoPersonaFisica.getContext(), 2, OopsPageDomiciliazioneInserimentoHome.this);
		    }
		    catch (Exception e) {

			Intent intentOops = new Intent(getApplicationContext(), OopsPageDomiciliazioneInserimentoHome.class);
			intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
			intentOops.putExtra("Parametri", parametri);
			OopsPageDomiciliazioneInserimentoHome.this.finish();
			startActivity(intentOops);
			overridePendingTransition(R.anim.fade, R.anim.hold);
		    }
		    // sorpassando la chiamata
		    /*
		     * AlertDialog.Builder builder = new
		     * AlertDialog.Builder(context);
		     * builder.setMessage(R.string.
		     * chiamata_OK).setCancelable(false).setPositiveButton("OK",
		     * new DialogInterface.OnClickListener() { public void
		     * onClick(DialogInterface dialog, int id) { //chiamata
		     * visualizza domiciliazione fornitura e vado alla schermata
		     * Domiciliazione chiamataVisualizzaDomiciliazione(); } });
		     * builder.create().show();
		     */
		}

	    }
	});
	final EniFont messaggio = (EniFont) findViewById(R.id.oops_messaggio);
	messaggio.setText(messaggioString);
    }

    private boolean isNetworkAvailable(Context context) {
	ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
	if (connectivityManager == null) return false;
	NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
	return activeNetworkInfo != null;
    }

    public void onBackPressed() {
	OopsPageDomiciliazioneInserimentoHome.this.setResult(OopsPageDomiciliazioneInserimentoHome.ESCI);
	OopsPageDomiciliazioneInserimentoHome.this.finish();
    }
}
