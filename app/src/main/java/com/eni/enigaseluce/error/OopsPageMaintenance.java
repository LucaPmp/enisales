package com.eni.enigaseluce.error;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.eni.enigaseluce.Main;
import com.eni.enigaseluce.R;
import com.eni.enigaseluce.font.EniFont;

@SuppressLint("NewApi")
public class OopsPageMaintenance extends Activity {

    AnimationDrawable animation;
    ImageView imageAnim;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.oops_page);

	final ImageView esci = (ImageView) findViewById(R.id.oops_indietro);
	esci.setBackgroundResource(R.drawable.esci);
	esci.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
	esci.getBackground().invalidateSelf();
	esci.setOnClickListener(new View.OnClickListener() {
	    public void onClick(View v1) {
		esci.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
		esci.getBackground().invalidateSelf();

		Intent intent = new Intent(OopsPageMaintenance.this, Main.class);
		startActivity(intent);

		finish();
	    }
	});

	ImageView riprova = (ImageView) findViewById(R.id.oops_riprova);
	riprova.setVisibility(View.INVISIBLE);

	EniFont messaggio = (EniFont) findViewById(R.id.oops_messaggio);
		if (getIntent() != null && getIntent().getStringExtra("MESSAGGIO") != null){
			messaggio.setText(getIntent().getStringExtra("MESSAGGIO"));
		} else {
			messaggio.setText(R.string.maintenance_message);
		}

    }
}
