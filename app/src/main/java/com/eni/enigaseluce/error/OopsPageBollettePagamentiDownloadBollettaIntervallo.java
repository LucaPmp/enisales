package com.eni.enigaseluce.error;

import java.util.HashMap;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.graphics.drawable.AnimationDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.eni.enigaseluce.R;
import com.eni.enigaseluce.bollettePagamenti.BollettePagamentiRicercaPostIntervallo;
import com.eni.enigaseluce.font.EniFont;
import com.eni.enigaseluce.httpcall.Constants;
import com.eni.enigaseluce.httpcall.ServiceCall;
import com.eni.enigaseluce.httpcall.ServiceCallFactory;
import com.eni.enigaseluce.utils.Utilities;

@SuppressLint("NewApi")
public class OopsPageBollettePagamentiDownloadBollettaIntervallo extends Activity {
    public final static int ESCI = 1;
    private Context context;
    AnimationDrawable animation;
    ImageView imageAnim;
    boolean checkbox = false;
    SharedPreferences settings;
    SharedPreferences.Editor editor;
    String userStored;
    String passwordStored;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.oops_page);
	context = this;

	Intent intent = getIntent();
	String messaggioString = intent.getStringExtra("MESSAGGIO");
	final boolean isDownload = intent.getBooleanExtra("isDownload", false);
	final String pdf = (isDownload) ? intent.getStringExtra("ID_PDF") : "";
	final String codiceCliente = (!isDownload) ? intent.getStringExtra("CODICE_CLIENTE") : "";
	final String primoConto = (!isDownload) ? intent.getStringExtra("CODICE_CONTO_CLIENTE") : "";

	final ImageView indietro = (ImageView) findViewById(R.id.oops_indietro);
	indietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
	indietro.getBackground().invalidateSelf();
	indietro.setOnClickListener(new View.OnClickListener() {
	    public void onClick(View v1) {
		indietro.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
		indietro.getBackground().invalidateSelf();
		if (isDownload) {
		    Intent intentBollettePagamentiRicerca = new Intent(getApplicationContext(), BollettePagamentiRicercaPostIntervallo.class);
		    intentBollettePagamentiRicerca.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
		    intentBollettePagamentiRicerca.putExtra("ORIGINE", getIntent().getStringExtra("ORIGINE"));
		    intentBollettePagamentiRicerca.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
		    intentBollettePagamentiRicerca.putExtra("DATA_INIZIO", getIntent().getStringExtra("DATA_INIZIO"));
		    intentBollettePagamentiRicerca.putExtra("DATA_FINE", getIntent().getStringExtra("DATA_FINE"));
		    OopsPageBollettePagamentiDownloadBollettaIntervallo.this.finish();
		    startActivity(intentBollettePagamentiRicerca);
		    overridePendingTransition(R.anim.fade, R.anim.hold);
		}
		else {
		    OopsPageBollettePagamentiDownloadBollettaIntervallo.this.setResult(OopsPageBollettePagamentiDownloadBollettaIntervallo.ESCI);
		    OopsPageBollettePagamentiDownloadBollettaIntervallo.this.finish();
		}

	    }
	});
	final ImageView riprova = (ImageView) findViewById(R.id.oops_riprova);
	riprova.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
	riprova.getBackground().invalidateSelf();
	riprova.setOnClickListener(new View.OnClickListener() {
	    public void onClick(View v1) {
		riprova.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
		riprova.getBackground().invalidateSelf();
		// quando l'utente clicka si deve rieseguire l'ultima chiamata
		// fatta

		animation = Utilities.animation(OopsPageBollettePagamentiDownloadBollettaIntervallo.this);
		imageAnim = (ImageView) findViewById(R.id.waiting_anim);
		imageAnim.setVisibility(View.VISIBLE);
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
		    imageAnim.setBackgroundDrawable(animation);
		}
		else {
		    imageAnim.setBackground(animation);
		}
		animation.start();

		if (isDownload) {
		    /*
		     * if(!isNetworkAvailable(context)){ Intent intentOops = new
		     * Intent(getApplicationContext(),
		     * OopsPageBollettePagamentiDownloadBollettaIntervallo
		     * .class); intentOops.putExtra("MESSAGGIO",
		     * getResources().getString(R.string.connessione_assente));
		     * intentOops.putExtra("ID_PDF", pdf);
		     * intentOops.putExtra("isDownload", true);
		     * OopsPageBollettePagamentiDownloadBollettaIntervallo
		     * .this.finish(); startActivity(intentOops);
		     * overridePendingTransition(R.anim.fade, R.anim.hold); }
		     * else{
		     *//********************* Chiamata download ******************************/
		    /*
		     * ServiceCall downloadServiceCall =
		     * ServiceCallFactory.createServiceCall(context);
		     * downloadServiceCall.myCookieStore = new
		     * PersistentCookieStore(context);
		     * 
		     * HashMap<String,String> parametri = new
		     * HashMap<String,String>();
		     * parametri.put("idProtocolloDocPdf", pdf);
		     * 
		     * try { downloadServiceCall.
		     * executeHttpsGetBollettePagamentiRicercaPostIntervallo
		     * (Constants.URL_DOWNLOAD_PDF,parametri,
		     * (BollettePagamentiRicercaPostIntervallo
		     * )BollettePagamentiRicercaPostIntervallo.getContext(),
		     * OopsPageBollettePagamentiDownloadBollettaIntervallo.this,
		     * true); } catch (Exception e) { Intent intentOops = new
		     * Intent(getApplicationContext(),
		     * OopsPageBollettePagamentiDownloadBollettaIntervallo
		     * .class); intentOops.putExtra("MESSAGGIO",
		     * getResources().getString
		     * (R.string.servizio_non_disponibile));
		     * intentOops.putExtra("ID_PDF", pdf);
		     * startActivity(intentOops);
		     * OopsPageBollettePagamentiDownloadBollettaIntervallo
		     * .this.finish(); overridePendingTransition(R.anim.fade,
		     * R.anim.hold); } }
		     */
		    Intent intentBollettePagamentiRicercaPostIntervallo = new Intent(getApplicationContext(), BollettePagamentiRicercaPostIntervallo.class);
		    intentBollettePagamentiRicercaPostIntervallo.putExtra("ID_PDF", getIntent().getStringExtra("ID_PDF"));
		    intentBollettePagamentiRicercaPostIntervallo.putExtra("NumeroFattura", getIntent().getStringExtra("NumeroFattura"));
		    intentBollettePagamentiRicercaPostIntervallo.putExtra("OopsPage", true);
		    intentBollettePagamentiRicercaPostIntervallo.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
		    intentBollettePagamentiRicercaPostIntervallo.putExtra("ORIGINE", getIntent().getStringExtra("ORIGINE"));
		    intentBollettePagamentiRicercaPostIntervallo.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
		    intentBollettePagamentiRicercaPostIntervallo.putExtra("DATA_INIZIO", getIntent().getStringExtra("DATA_INIZIO"));
		    intentBollettePagamentiRicercaPostIntervallo.putExtra("DATA_FINE", getIntent().getStringExtra("DATA_FINE"));
		    OopsPageBollettePagamentiDownloadBollettaIntervallo.this.finish();
		    startActivity(intentBollettePagamentiRicercaPostIntervallo);
		    overridePendingTransition(R.anim.fade, R.anim.hold);
		}
		else {
		    if (!isNetworkAvailable(context)) {
			Intent intentOops = new Intent(getApplicationContext(), OopsPageBollettePagamentiDownloadBollettaIntervallo.class);
			intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.connessione_assente));
			intentOops.putExtra("ID_PDF", pdf);
			OopsPageBollettePagamentiDownloadBollettaIntervallo.this.finish();
			startActivity(intentOops);
			overridePendingTransition(R.anim.fade, R.anim.hold);
		    }
		    else {
			/********************* Chiamata show lista forniture ******************************/
			ServiceCall stAvFornServiceCall = ServiceCallFactory.createServiceCall(context);
			// stAvFornServiceCall.myCookieStore = new
			// PersistentCookieStore(context);

			HashMap<String, String> parametri = new HashMap<String, String>();
			parametri.put("codiceCliente", codiceCliente);
			parametri.put("codiceContoCliente", primoConto);

			try {
			    stAvFornServiceCall.executeHttpsGetBollettePagamentiRicercaPostIntervallo(Constants.URL_SHOW_LISTA_FATTURE, parametri, (BollettePagamentiRicercaPostIntervallo) BollettePagamentiRicercaPostIntervallo.getContext(), OopsPageBollettePagamentiDownloadBollettaIntervallo.this, false, "");
			}
			catch (Exception e) {
			    Intent intentOops = new Intent(getApplicationContext(), OopsPageBollettePagamentiDownloadBollettaIntervallo.class);
			    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
			    intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
			    intentOops.putExtra("CODICE_CONTO_CLIENTE", primoConto);
			    startActivity(intentOops);
			    overridePendingTransition(R.anim.fade, R.anim.hold);
			}
		    }
		}

	    }
	});
	final EniFont messaggio = (EniFont) findViewById(R.id.oops_messaggio);
	messaggio.setText(messaggioString);
    }

    private boolean isNetworkAvailable(Context context) {
	ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
	if (connectivityManager == null) return false;
	NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
	return activeNetworkInfo != null;
    }

    public void onBackPressed() {
	boolean isDownload = getIntent().getBooleanExtra("isDownload", false);
	if (isDownload) {
	    Intent intentBollettePagamentiRicerca = new Intent(getApplicationContext(), BollettePagamentiRicercaPostIntervallo.class);
	    intentBollettePagamentiRicerca.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
	    intentBollettePagamentiRicerca.putExtra("ORIGINE", getIntent().getStringExtra("ORIGINE"));
	    intentBollettePagamentiRicerca.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
	    intentBollettePagamentiRicerca.putExtra("DATA_INIZIO", getIntent().getStringExtra("DATA_INIZIO"));
	    intentBollettePagamentiRicerca.putExtra("DATA_FINE", getIntent().getStringExtra("DATA_FINE"));
	    OopsPageBollettePagamentiDownloadBollettaIntervallo.this.finish();
	    startActivity(intentBollettePagamentiRicerca);
	    overridePendingTransition(R.anim.fade, R.anim.hold);
	}
	else {
	    OopsPageBollettePagamentiDownloadBollettaIntervallo.this.setResult(OopsPageBollettePagamentiDownloadBollettaIntervallo.ESCI);
	    OopsPageBollettePagamentiDownloadBollettaIntervallo.this.finish();
	}
    }
}
