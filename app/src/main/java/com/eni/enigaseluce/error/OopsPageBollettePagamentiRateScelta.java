/*package com.eni.enigaseluce.error;

 import java.util.HashMap;

 import android.app.Activity;
 import android.content.Context;
 import android.content.Intent;
 import android.content.SharedPreferences;
 import android.graphics.PorterDuff;
 import android.graphics.drawable.AnimationDrawable;
 import android.net.ConnectivityManager;
 import android.net.NetworkInfo;
 import android.os.Bundle;
 import android.view.View;
 import android.widget.ImageView;
 import android.os.Build;
 import com.eni.enigaseluce.R;
 import com.eni.enigaseluce.bollettePagamenti.BollettePagamentiSceltaPiano;
 import com.eni.enigaseluce.font.EniFont;
 import com.eni.enigaseluce.httpcall.Constants;
 import com.eni.enigaseluce.httpcall.ServiceCall;
 import com.loopj.android.http.PersistentCookieStore;

 public class OopsPageBollettePagamentiRateScelta extends Activity {
 public final static int ESCI = 1;
 private Context context;
 AnimationDrawable animation;
 ImageView imageAnim;
 boolean checkbox = false;
 SharedPreferences settings;
 SharedPreferences.Editor editor;
 String userStored;
 String passwordStored;

 *//** Called when the activity is first created. */
/*
 @Override
 public void onCreate(Bundle savedInstanceState) {
 super.onCreate(savedInstanceState);
 setContentView(R.layout.oops_page);
 context = this;

 String messaggioString = getIntent().getStringExtra("MESSAGGIO");
 final String codiceCliente = getIntent().getStringExtra("CODICE_CLIENTE");

 final String identificativoFattura = getIntent().getStringExtra("identificativoFattura");
 final String codiceCausaleRateizzabilita = getIntent().getStringExtra("codiceCausaleRateizzabilita");
 final String codiceFattura = getIntent().getStringExtra("codiceFattura");
 final int tipoPiano = getIntent().getIntExtra("tipoPiano",1);
 final String codiceCausaleRateizzabilitaAlternativa = getIntent().getStringExtra("codiceCausaleRateizzabilitaAlternativa");

 final ImageView indietro = (ImageView) findViewById(R.id.oops_indietro);
 indietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
 indietro.getBackground().invalidateSelf();
 indietro.setOnClickListener(new View.OnClickListener() {
 public void onClick(View v1) {
 indietro.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
 indietro.getBackground().invalidateSelf();
 OopsPageBollettePagamentiRateScelta.this.setResult(OopsPageBollettePagamentiRateScelta.ESCI);
 OopsPageBollettePagamentiRateScelta.this.finish();
 }
 });
 final ImageView riprova = (ImageView) findViewById(R.id.oops_riprova);
 riprova.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
 riprova.getBackground().invalidateSelf();
 riprova.setOnClickListener(new View.OnClickListener() {
 public void onClick(View v1) {
 riprova.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
 riprova.getBackground().invalidateSelf();
 // quando l'utente clicka si deve rieseguire l'ultima chiamata
 // fatta
 animation = new AnimationDrawable();
 animation.addFrame(getResources().getDrawable(R.drawable.anim_load2), 300);
 animation.addFrame(getResources().getDrawable(R.drawable.anim_load3), 300);
 animation.addFrame(getResources().getDrawable(R.drawable.anim_load4), 300);
 animation.setOneShot(false);
 imageAnim = (ImageView) findViewById(R.id.waiting_ops);
 imageAnim.setVisibility(View.VISIBLE);
 if(Build.VERSION.SDK_INT < 16){
 imageAnim.setBackgroundDrawable(animation);
 }else{
 imageAnim.setBackground(animation);
 }
 // start the animation!
 animation.start();

 if (!isNetworkAvailable(context)) {
 Intent intentOops = new Intent(getApplicationContext(),OopsPageBollettePagamentiRateScelta.class);
 intentOops.putExtra("MESSAGGIO",getResources().getString(R.string.connessione_assente));
 intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
 intentOops.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
 intentOops.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));

 intentOops.putExtra("identificativoFattura", identificativoFattura);
 intentOops.putExtra("codiceCausaleRateizzabilita", codiceCausaleRateizzabilita);
 intentOops.putExtra("codiceFattura", codiceFattura);
 intentOops.putExtra("tipoPiano", tipoPiano);

 intentOops.putExtra("dataEmissione", getIntent().getStringExtra("dataEmissione"));
 intentOops.putExtra("dataScadenza", getIntent().getStringExtra("dataScadenza"));
 intentOops.putExtra("importoRateizzabile", getIntent().getStringExtra("importoRateizzabile"));
 intentOops.putExtra("codiceCausaleRateizzabilitaAlternativa", codiceCausaleRateizzabilitaAlternativa);

 OopsPageBollettePagamentiRateScelta.this.finish();
 startActivity(intentOops);
 overridePendingTransition(R.anim.fade, R.anim.hold);
 } else {
 *//********************* Chiamata statoAvanzamento Fornitura ******************************/
/*
 * ServiceCall stAvFornServiceCall =
 * ServiceCallFactory.createServiceCall(context);
 * stAvFornServiceCall.myCookieStore = new PersistentCookieStore(context);
 * 
 * HashMap<String, String> parametri = new HashMap<String, String>();
 * parametri.put("codiceCliente", codiceCliente);
 * parametri.put("identificativoFattura", identificativoFattura);
 * parametri.put("codiceCausaleRateizzabilita", codiceCausaleRateizzabilita);
 * parametri.put("periodicitaScadenza",
 * getIntent().getStringExtra("periodicitaScadenza"));
 * parametri.put("codiceCausaleRateizzabilitaAlternativa",
 * codiceCausaleRateizzabilitaAlternativa); try {
 * stAvFornServiceCall.executeHttpsGetBollettePagamentiCalcolaPropostaRateScelta
 * (Constants.URL_CALCOLA_PROPOSTA_RATE, parametri,
 * (BollettePagamentiSceltaPiano)BollettePagamentiSceltaPiano.getContext(),
 * OopsPageBollettePagamentiRateScelta.this); } catch (Exception e) { Intent
 * intentOops = new
 * Intent(getApplicationContext(),OopsPageBollettePagamentiRateScelta.class);
 * intentOops
 * .putExtra("MESSAGGIO",getResources().getString(R.string.servizio_non_disponibile
 * )); intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
 * intentOops.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
 * intentOops.putExtra("INDICE_SPINNER",
 * getIntent().getIntExtra("INDICE_SPINNER", 0));
 * 
 * intentOops.putExtra("identificativoFattura", identificativoFattura);
 * intentOops.putExtra("codiceCausaleRateizzabilita",
 * codiceCausaleRateizzabilita); intentOops.putExtra("codiceFattura",
 * codiceFattura); intentOops.putExtra("tipoPiano", tipoPiano);
 * intentOops.putExtra("codiceCausaleRateizzabilitaAlternativa",
 * codiceCausaleRateizzabilitaAlternativa); intentOops.putExtra("dataEmissione",
 * getIntent().getStringExtra("dataEmissione"));
 * intentOops.putExtra("dataScadenza",
 * getIntent().getStringExtra("dataScadenza"));
 * intentOops.putExtra("importoRateizzabile",
 * getIntent().getStringExtra("importoRateizzabile"));
 * 
 * startActivity(intentOops); OopsPageBollettePagamentiRateScelta.this.finish();
 * overridePendingTransition(R.anim.fade, R.anim.hold); } }
 * 
 * } }); final EniFont messaggio = (EniFont) findViewById(R.id.oops_messaggio);
 * messaggio.setText(messaggioString); }
 * 
 * private boolean isNetworkAvailable(Context context) { ConnectivityManager
 * connectivityManager = (ConnectivityManager)
 * context.getSystemService(Context.CONNECTIVITY_SERVICE); if
 * (connectivityManager == null) return false; NetworkInfo activeNetworkInfo =
 * connectivityManager.getActiveNetworkInfo(); return activeNetworkInfo != null;
 * }
 * 
 * public void onBackPressed() {
 * OopsPageBollettePagamentiRateScelta.this.setResult
 * (OopsPageBollettePagamentiRateScelta.ESCI);
 * OopsPageBollettePagamentiRateScelta.this.finish(); } }
 */