package com.eni.enigaseluce.error;

import java.util.HashMap;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.graphics.drawable.AnimationDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.eni.enigaseluce.R;
import com.eni.enigaseluce.autolettura.Autolettura;
import com.eni.enigaseluce.font.EniFont;
import com.eni.enigaseluce.httpcall.Constants;
import com.eni.enigaseluce.httpcall.ServiceCall;
import com.eni.enigaseluce.httpcall.ServiceCallFactory;
import com.eni.enigaseluce.utils.Utilities;

@SuppressLint("NewApi")
public class OopsPageAutoletturaAutolettura extends Activity {
    public final static int ESCI = 1;
    private Context context;
    AnimationDrawable animation;
    ImageView imageAnim;
    boolean checkbox = false;
    SharedPreferences settings;
    SharedPreferences.Editor editor;
    String userStored;
    String passwordStored;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.oops_page);
	context = this;

	Intent intent = getIntent();
	String messaggioString = intent.getStringExtra("MESSAGGIO");
	final String codiceCliente = intent.getStringExtra("CODICE_CLIENTE");
	final String pdf = intent.getStringExtra("PDF");
	final String pdr = intent.getStringExtra("PDR");

	final String sistemaBilling = intent.getStringExtra("SISTEMA_BILLING");
	final String societaFornitrice = intent.getStringExtra("SOCIETA_FORNITRICE");
	final String numeroCifreMisuratore = intent.getStringExtra("NUMERO_CIFRE_MISURATORE");

	final ImageView indietro = (ImageView) findViewById(R.id.oops_indietro);
	indietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
	indietro.getBackground().invalidateSelf();
	indietro.setOnClickListener(new View.OnClickListener() {
	    public void onClick(View v1) {
		indietro.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
		indietro.getBackground().invalidateSelf();
		OopsPageAutoletturaAutolettura.this.setResult(OopsPageAutoletturaAutolettura.ESCI);
		OopsPageAutoletturaAutolettura.this.finish();
	    }
	});
	final ImageView riprova = (ImageView) findViewById(R.id.oops_riprova);
	riprova.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
	riprova.getBackground().invalidateSelf();
	riprova.setOnClickListener(new View.OnClickListener() {
	    public void onClick(View v1) {
		riprova.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
		riprova.getBackground().invalidateSelf();
		// parte la chiamata al dettaglio dello storico

		animation = Utilities.animation(OopsPageAutoletturaAutolettura.this);
		imageAnim = (ImageView) findViewById(R.id.waiting_anim);
		imageAnim.setVisibility(View.VISIBLE);
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
		    imageAnim.setBackgroundDrawable(animation);
		}
		else {
		    imageAnim.setBackground(animation);
		}
		animation.start();

		HashMap<String, String> parametri = new HashMap<String, String>();
		parametri.put("pdf", pdf);
		parametri.put("codiceCliente", codiceCliente);

		/************************************ Ultima lettura gas ************************************/
		if (!isNetworkAvailable(context)) {
		    Intent intentOops = new Intent(getApplicationContext(), OopsPageAutoletturaAutolettura.class);
		    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.connessione_assente));
		    intentOops.putExtra("PDF", pdf);
		    intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
		    intentOops.putExtra("SISTEMA_BILLING", sistemaBilling);
		    intentOops.putExtra("SOCIETA_FORNITRICE", societaFornitrice);
		    intentOops.putExtra("NUMERO_CIFRE_MISURATORE", numeroCifreMisuratore);

		    OopsPageAutoletturaAutolettura.this.finish();
		    startActivity(intentOops);
		    overridePendingTransition(R.anim.fade, R.anim.hold);

		}
		else {
		    ServiceCall stAvFornServiceCall = ServiceCallFactory.createServiceCall(context);
		    // stAvFornServiceCall.myCookieStore = new
		    // PersistentCookieStore(context);
		    try {
			stAvFornServiceCall.executeHttpsGetAutolettura(Constants.URL_ULTIMA_LETTURA_GAS, parametri, (Autolettura) Autolettura.getContext(), pdf, pdr, sistemaBilling, societaFornitrice, numeroCifreMisuratore, OopsPageAutoletturaAutolettura.this);
		    }
		    catch (Exception e) {
			Intent intentOops = new Intent(getApplicationContext(), OopsPageAutoletturaAutolettura.class);
			intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
			intentOops.putExtra("PDF", pdf);
			intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
			intentOops.putExtra("SISTEMA_BILLING", sistemaBilling);
			intentOops.putExtra("SOCIETA_FORNITRICE", societaFornitrice);
			intentOops.putExtra("NUMERO_CIFRE_MISURATORE", numeroCifreMisuratore);

			OopsPageAutoletturaAutolettura.this.finish();
			startActivity(intentOops);
			overridePendingTransition(R.anim.fade, R.anim.hold);
		    }
		}
		/*******************************************************************************************/

	    }
	});
	final EniFont messaggio = (EniFont) findViewById(R.id.oops_messaggio);
	messaggio.setText(messaggioString);
    }

    private boolean isNetworkAvailable(Context context) {
	ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
	if (connectivityManager == null) return false;
	NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
	return activeNetworkInfo != null;
    }

    public void onBackPressed() {
	OopsPageAutoletturaAutolettura.this.setResult(OopsPageAutoletturaAutolettura.ESCI);
	OopsPageAutoletturaAutolettura.this.finish();
    }
}
