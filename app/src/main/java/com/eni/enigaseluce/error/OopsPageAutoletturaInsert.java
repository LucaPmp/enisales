package com.eni.enigaseluce.error;

import java.util.HashMap;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.graphics.drawable.AnimationDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.eni.enigaseluce.R;
import com.eni.enigaseluce.autolettura.AutoLetturaInsert;
import com.eni.enigaseluce.font.EniFont;
import com.eni.enigaseluce.httpcall.Constants;
import com.eni.enigaseluce.httpcall.ServiceCall;
import com.eni.enigaseluce.httpcall.ServiceCallFactory;
import com.eni.enigaseluce.utils.Log;
import com.eni.enigaseluce.utils.Utilities;

@SuppressLint("NewApi")
public class OopsPageAutoletturaInsert extends Activity {
    public final static int ESCI = 1;
    private Context context;
    AnimationDrawable animation;
    ImageView imageAnim;
    boolean checkbox = false;
    SharedPreferences settings;
    SharedPreferences.Editor editor;
    String userStored;
    String passwordStored;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.oops_page);
	context = this;

	Intent intent = getIntent();
	String messaggioString = intent.getStringExtra("MESSAGGIO");
	// final String primoConto =
	// intent.getStringExtra("CODICE_CONTO_CLIENTE");
	final String pdf = intent.getStringExtra("PDF");
	final String codiceCliente = intent.getStringExtra("CODICE_CLIENTE");
	final String valoreAutolettura = intent.getStringExtra("VALORE_AUTOLETTURA");
	final String sistemaBilling = intent.getStringExtra("SISTEMA_BILLING");
	final String societaFornitrice = intent.getStringExtra("SOCIETA_FORNITRICE");
	final String numeroCifreMisuratore = intent.getStringExtra("NUMERO_CIFRE_MISURATORE");
	final String canale = intent.getStringExtra("CANALE");
	final String modalita = intent.getStringExtra("MODALITA");

	Log.d("OOPSPAGEAUTOLETTURAINSERT","Values from getIntent()\n" + "pdf: " + pdf + "\ncodiceCliente: " + codiceCliente + "\nvaloreAutolettura: " + valoreAutolettura + "\nsistemaBilling: " + sistemaBilling + "\n" + "societaFornitrice: " + societaFornitrice + "\nnumeroCifreMisuratore: " + numeroCifreMisuratore);

	final ImageView indietro = (ImageView) findViewById(R.id.oops_indietro);
	indietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
	indietro.getBackground().invalidateSelf();
	indietro.setOnClickListener(new View.OnClickListener() {
	    public void onClick(View v1) {
		indietro.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
		indietro.getBackground().invalidateSelf();
		OopsPageAutoletturaInsert.this.setResult(OopsPageAutoletturaInsert.ESCI);
		OopsPageAutoletturaInsert.this.finish();
	    }
	});
	final ImageView riprova = (ImageView) findViewById(R.id.oops_riprova);
	riprova.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
	riprova.getBackground().invalidateSelf();
	riprova.setOnClickListener(new View.OnClickListener() {
	    public void onClick(View v1) {
		riprova.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
		riprova.getBackground().invalidateSelf();
		// chiamata invio autolettura URL_INVIA_AUTOLETTURA_GAS

		animation = Utilities.animation(OopsPageAutoletturaInsert.this);
		imageAnim = (ImageView) findViewById(R.id.waiting_anim);
		imageAnim.setVisibility(View.VISIBLE);
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
		    imageAnim.setBackgroundDrawable(animation);
		}
		else {
		    imageAnim.setBackground(animation);
		}
		animation.start();

		/************************************ invia lettura gas ************************************/
		if (!isNetworkAvailable(context)) {
		    Intent intentOops = new Intent(getApplicationContext(), OopsPageAutoletturaInsert.class);
		    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.connessione_assente));
		    intentOops.putExtra("PDF", pdf);
		    intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
		    intentOops.putExtra("VALORE_AUTOLETTURA", valoreAutolettura);
		    intentOops.putExtra("SISTEMA_BILLING", sistemaBilling);
		    intentOops.putExtra("SOCIETA_FORNITRICE", societaFornitrice);
		    intentOops.putExtra("NUMERO_CIFRE_MISURATORE", numeroCifreMisuratore);
		    intentOops.putExtra("CANALE", "APPS");
		    intentOops.putExtra("MODALITA", "MANUALE");

		    OopsPageAutoletturaInsert.this.finish();
		    startActivity(intentOops);
		    overridePendingTransition(R.anim.fade, R.anim.hold);

		}
		else {
		    ServiceCall stAvFornServiceCall = ServiceCallFactory.createServiceCall(context);
		    // stAvFornServiceCall.myCookieStore = new
		    // PersistentCookieStore(context);

		    HashMap<String, String> parametri = new HashMap<String, String>();
		    parametri.put("pdf", pdf);
		    parametri.put("codiceCliente", codiceCliente);
		    parametri.put("valoreAutolettura", valoreAutolettura);
		    parametri.put("sistemaBilling", sistemaBilling);
		    parametri.put("societaFornitrice", societaFornitrice);
		    parametri.put("numCifreMisuratore", numeroCifreMisuratore);
		    parametri.put("canale", canale);
		    parametri.put("modalita", modalita);
		    try {
			stAvFornServiceCall.executeHttpsGetAutoletturaInsert(Constants.URL_INVIA_AUTOLETTURA_GAS, parametri, (AutoLetturaInsert) AutoLetturaInsert.getContext(), OopsPageAutoletturaInsert.this);
		    }
		    catch (Exception e) {
			Intent intentOops = new Intent(getApplicationContext(), OopsPageAutoletturaInsert.class);
			intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
			intentOops.putExtra("PDF", pdf);
			intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
			intentOops.putExtra("VALORE_AUTOLETTURA", valoreAutolettura);
			intentOops.putExtra("SISTEMA_BILLING", sistemaBilling);
			intentOops.putExtra("SOCIETA_FORNITRICE", societaFornitrice);
			intentOops.putExtra("NUMERO_CIFRE_MISURATORE", numeroCifreMisuratore);
			intentOops.putExtra("CANALE", "APPS");
			intentOops.putExtra("MODALITA", "MANUALE");

			startActivity(intentOops);
			OopsPageAutoletturaInsert.this.finish();
			overridePendingTransition(R.anim.fade, R.anim.hold);
		    }
		}
		/*******************************************************************************************/

	    }
	});
	final EniFont messaggio = (EniFont) findViewById(R.id.oops_messaggio);
	messaggio.setText(messaggioString);
    }

    private boolean isNetworkAvailable(Context context) {
	ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
	if (connectivityManager == null) return false;
	NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
	return activeNetworkInfo != null;
    }

    public void onBackPressed() {
	OopsPageAutoletturaInsert.this.setResult(OopsPageAutoletturaInsert.ESCI);
	OopsPageAutoletturaInsert.this.finish();
    }
}
