package com.eni.enigaseluce.home;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.cookie.Cookie;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.AnimationDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.eni.enigaseluce.EniApplication;
import com.eni.enigaseluce.R;
import com.eni.enigaseluce.autolettura.Autolettura;
import com.eni.enigaseluce.bollettePagamenti.BollettePagamenti;
import com.eni.enigaseluce.consumigas.ConsumiGas;
import com.eni.enigaseluce.consumiluce.ConsumiLuce;
import com.eni.enigaseluce.domiciliazione.Domiciliazione;
import com.eni.enigaseluce.error.OopsPageAttivazioneFornitura;
import com.eni.enigaseluce.error.OopsPageAutolettura;
import com.eni.enigaseluce.error.OopsPageBollettePagamenti;
import com.eni.enigaseluce.error.OopsPageConsumiGas;
import com.eni.enigaseluce.error.OopsPageConsumiLuce;
import com.eni.enigaseluce.error.OopsPageDomiciliazione;
import com.eni.enigaseluce.error.OopsPageEniWebBolletta;
import com.eni.enigaseluce.error.OopsPageGeneric;
import com.eni.enigaseluce.error.OopsPageSessioneScaduta;
import com.eni.enigaseluce.error.OopsPageStorico;
import com.eni.enigaseluce.error.Utils;
import com.eni.enigaseluce.httpcall.Constants;
import com.eni.enigaseluce.httpcall.ServiceCall;
import com.eni.enigaseluce.httpcall.ServiceCallFactory;
import com.eni.enigaseluce.login.Login;
import com.eni.enigaseluce.login.help.GuidaBolletta;
import com.eni.enigaseluce.login.help.TrovaEni;
import com.eni.enigaseluce.pager.HorizontalPager;
import com.eni.enigaseluce.statoavanzamentofornitura.StatoAvanzamentoFornitura;
import com.eni.enigaseluce.storicoletture.StoricoLetture;
import com.eni.enigaseluce.utils.Utilities;
import com.eni.enigaseluce.webolletta.Webolletta;

@SuppressLint("NewApi")
public class Home extends Activity {
    private Context context;
    private static Context staticContext;
    private static Dialog dialogDisableClicks;
    Dialog dialogWaiting;
    AnimationDrawable animation;
    ImageView imageAnim;
    static boolean fornitureAttive;
    static int clienteAttivo;
    static ArrayList<String> contiBollettePagamenti;
    static ArrayList<String> contiConsumiLuce;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        context = this;
        Home.staticContext = this;
        Home.dialogDisableClicks = new Dialog(Home.this, android.R.style.Theme_Translucent_NoTitleBar);
        fornitureAttive = false;
        clienteAttivo = -1;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home);

        ((EniApplication)getApplication()).sendAnalyticsEvent("[Android] - Home");

        contiBollettePagamenti = new ArrayList<String>();
        contiConsumiLuce = new ArrayList<String>();
        inizializzaContiBollettePagamenti(Constants.SERVIZIO_LOGIN);
        inizializzaContiConsumiLuce(Constants.SERVIZIO_LOGIN);
    }

    @Override
    protected void onStart() {
        super.onStart();

        /******************* Chiamata Stato avanzamento ***********************/
        // controllo se ci sono forniture attive
        // boolean fornitureAttive = randomBoolean();
        // "[{\"descEsito\":\"Chiamata effettuata correttamente\",\"esito\":200},{\"Strada\":\"QUARTONALI\",\"UltimoAccesso\":\"4/10/2012\",\"CodiceCliente\":\"811445469\",\"CodiceFiscaleCliente\":\"SLNDNT29T25H641Y\",\"NumCivico\":\"6\",\"NomeCliente\":\"DONATO\",\"UserId\":\"prova_siebel_2@cambio.it\",\"StatoCliente\":\"ATTIVO\",\"CognomeCliente\":\"SALINARI\",\"TipoIndirizzo\":\"CONTRADA\",\"Provincia\":\"PZ\",\"IndirizzoResidenzaCliente\":\"CONTRADA QUARTONALI 6 RUOTI 85056 PZ \",\"Cap\":\"85056\",\"TipologiaCliente\":\"RETAIL\",\"Citta\":\"RUOTI\"},[{\"IdServizio\":\"1-YPOZYS\",\"IndirizzoCompleto\":\"CONTRADA QUARTONALI 6 RUOTI 85056 PZ \",\"CodiceConto\":\"505347053389\",\"SocietaFornitrice\":\"800\",\"Tipologia\":\"POWER\",\"StatoConto\":\"UTILIZZABILE\",\"SistemaBilling\":\"NETA\",\"Pdf\":\"8001462661\",\"CodiceContratto\":\"1-2176826267\"},{\"TipoGaranzia\":\"DEPOSITO CAUZIONALE\",\"TipoProdotto\":\"Energia QUOTAFISSA\",\"FrequenzaFatturazione\":\"BIMESTRALE\",\"ConsumoPrevisto\":\"1600\"}],[{\"IdServizio\":\"1-YPQ18I\",\"IndirizzoCompleto\":\"CONTRADA QUARTONALI 6 RUOTI 85056 PZ \",\"CodiceConto\":\"505347053389\",\"SocietaFornitrice\":\"800\",\"Tipologia\":\"GAS\",\"StatoConto\":\"UTILIZZABILE\",\"SistemaBilling\":\"NETA\",\"Pdf\":\"8001462665\",\"CodiceContratto\":\"1-2176826267\"},{\"TipoGaranzia\":\"DEPOSITO CAUZIONALE\",\"TipoProdotto\":\"Energia QUOTAFISSA\",\"FrequenzaFatturazione\":\"BIMESTRALE\",\"ConsumoPrevisto\":\"1630\"}]]";
        final String loginService = Constants.SERVIZIO_LOGIN;
        fornitureAttive = esisteFornituraAttiva(loginService);
        clienteAttivo = isClienteAttivo(loginService);
        /*************************************************/

        final ImageView statoAttivazioneFornitura = (ImageView) findViewById(R.id.home_icona_1);
        final ImageView domiciliazione = (ImageView) findViewById(R.id.home_icona_2);
        final ImageView autolettura = (ImageView) findViewById(R.id.home_icona_3);
        final ImageView energyStoreLocator = (ImageView) findViewById(R.id.home_icona_4);
        final ImageView bollette = (ImageView) findViewById(R.id.home_icona_6);
        final ImageView eniwebBol = (ImageView) findViewById(R.id.home_icona_7);
        final ImageView consLuce = (ImageView) findViewById(R.id.home_icona_8);
        final ImageView consGas = (ImageView) findViewById(R.id.home_icona_9);
        final ImageView guidaBolletta = (ImageView) findViewById(R.id.home_icona_10);

        // 3 casi: StatoCliente "ATTIVO" tutte abilitate, StatoCliente
        // "IN ATTIVAZIONE" con almeno 1 codice conto 'URILIZZABILE' StVfORN e
        // energy locator abilitate, StatoCliente "NON ATTIVO" solo energy store
        // abilitato
        // Se si allora passa alla home con 5 icone

        // stub cliente non attivo
        // clienteAttivo=0;

        if (clienteAttivo == 1) {
            // icone tutte abilitate
            statoAttivazioneFornitura.getBackground().setAlpha(255);
            statoAttivazioneFornitura.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
            statoAttivazioneFornitura.getBackground().invalidateSelf();

            domiciliazione.getBackground().setAlpha(255);
            domiciliazione.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
            domiciliazione.getBackground().invalidateSelf();

            final String contoGasAttivo = getContoGasAttivo(loginService);
            if (contoGasAttivo.equals("")) {
                autolettura.getBackground().setAlpha(80);
                autolettura.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
                autolettura.getBackground().invalidateSelf();
                // storico.getBackground().setAlpha(80);
                // storico.getBackground().setColorFilter(0xFFFFFFFF,
                // PorterDuff.Mode.MULTIPLY);
                // storico.getBackground().invalidateSelf();
                consGas.getBackground().setAlpha(80);
                consGas.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
                consGas.getBackground().invalidateSelf();
            } else {
                autolettura.getBackground().setAlpha(255);
                autolettura.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
                autolettura.getBackground().invalidateSelf();
                autolettura.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        autolettura.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
                        autolettura.getBackground().invalidateSelf();
                        dialogDisableClicks.show();

                        animation = Utilities.animation(Home.this);
                        imageAnim = (ImageView) findViewById(R.id.waiting_anim);
                        imageAnim.setVisibility(View.VISIBLE);
                        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                            imageAnim.setBackgroundDrawable(animation);
                        } else {
                            imageAnim.setBackground(animation);
                        }
                        animation.start();

                        String codiceCliente = getCodiceCliente(loginService);
                        String elencoCodiciCompleti[];
                        String parametri = "&codiceCliente=" + codiceCliente;
                        try {
                            elencoCodiciCompleti = elencoCodiciContiAttiviCompleti(new JSONArray(loginService));
                            for (int i = 0; i < elencoCodiciCompleti.length; i++) {
                                String pdf = getPdfGas(new JSONArray(loginService), elencoCodiciCompleti[i]);
                                if (!pdf.equals("")) {
                                    parametri = parametri + "&" + "pdf=" + pdf;
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        if (!isNetworkAvailable(context)) {
                            if (animation != null) {
                                animation.stop();
                                imageAnim.setVisibility(View.INVISIBLE);
                            }
                            dialogDisableClicks.dismiss();
                            Intent intentOops = new Intent(getApplicationContext(), OopsPageAutolettura.class);
                            intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.connessione_assente));
                            intentOops.putExtra("PARAMETRI", parametri);
                            startActivity(intentOops);
                            overridePendingTransition(R.anim.fade, R.anim.hold);
                        } else {
                            /********************* loadFornitureInfoGas ******************************/
                            ServiceCall stAvFornServiceCall = ServiceCallFactory.createServiceCall(context);

                            // parametri senza pdf (nessuna fornitura gas), la
                            // chiamata non deve essere fatta senza paramtri
                            // pdf, popup con messaggio nessuna fornitura gas
                            // presente
                            if (!parametri.contains("pdf")) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                builder.setMessage(R.string.nessuna_fornitura_presente).setCancelable(false)
                                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                if (animation != null) {
                                                    animation.stop();
                                                    imageAnim.setVisibility(View.INVISIBLE);
                                                }
                                                autolettura.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
                                                autolettura.getBackground().invalidateSelf();
                                            }
                                        });

                                builder.create().show();
                            } else {
                                /*****************************************************************************/
                                try {
                                    stAvFornServiceCall.executeHttpsGet(Constants.URL_FORNITURE_GAS_ATTIVE, parametri, Home.this,
                                            Constants.FUNZIONALITA_AUTOLETTURA, null);
                                } catch (Exception e) {
                                    if (animation != null) {
                                        animation.stop();
                                        imageAnim.setVisibility(View.INVISIBLE);
                                    }
                                    dialogDisableClicks.dismiss();
                                    Intent intentOops = new Intent(getApplicationContext(), OopsPageAutolettura.class);
                                    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
                                    intentOops.putExtra("PARAMETRI", parametri);
                                    // Home.this.finish();
                                    startActivity(intentOops);
                                    overridePendingTransition(R.anim.fade, R.anim.hold);
                                }
                                /************************************************************************************/
                            }
                        }
                    }
                });

                consGas.getBackground().setAlpha(255);
                consGas.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
                consGas.getBackground().invalidateSelf();
                consGas.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        // codiceCliente, segmento, tipologia, pdf, regione,
                        // prodotto, dataAttivazione
                        consGas.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
                        consGas.getBackground().invalidateSelf();
                        dialogDisableClicks.show();
                        String parametri = "";
                        String codiceCliente = getCodiceCliente(loginService);
                        parametri = parametri + "&codiceCliente=" + codiceCliente;
                        String segmento = getSegmentoCliente(loginService);
                        parametri = parametri + "&segmento=" + segmento;
                        String tipologiaPdfRegioneUtilizzoGasDataAttivazione = getTipologiaPdfRegioneUtilizzoGasDataAttivazione(loginService, contoGasAttivo,
                                segmento);
                        parametri = parametri + tipologiaPdfRegioneUtilizzoGasDataAttivazione;

                        if (!isNetworkAvailable(context)) {
                            if (animation != null) {
                                animation.stop();
                                imageAnim.setVisibility(View.INVISIBLE);
                            }
                            dialogDisableClicks.dismiss();
                            Intent intentOops = new Intent(getApplicationContext(), OopsPageConsumiGas.class);
                            intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.connessione_assente));
                            intentOops.putExtra("PARAMETER", parametri);
                            startActivity(intentOops);
                            overridePendingTransition(R.anim.fade, R.anim.hold);
                        } else {

                            animation = Utilities.animation(Home.this);
                            imageAnim = (ImageView) findViewById(R.id.waiting_anim);
                            imageAnim.setVisibility(View.VISIBLE);
                            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                                imageAnim.setBackgroundDrawable(animation);
                            } else {
                                imageAnim.setBackground(animation);
                            }
                            animation.start();

                            /****************************** Chiamata consumi luce **************************************/
                            ServiceCall consumiGasServiceCall = ServiceCallFactory.createServiceCall(context);
                            // consumiGasServiceCall.myCookieStore = new
                            // PersistentCookieStore(context);
                            try {
                                consumiGasServiceCall
                                        .executeHttpsGet(Constants.URL_CONSUMI_GAS, parametri, Home.this, Constants.FUNZIONALITA_CONSUMI_GAS, null);
                            } catch (Exception e) {
                                if (animation != null) {
                                    animation.stop();
                                    imageAnim.setVisibility(View.INVISIBLE);
                                }
                                dialogDisableClicks.dismiss();
                                Intent intentOops = new Intent(getApplicationContext(), OopsPageConsumiGas.class);
                                intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
                                intentOops.putExtra("PARAMETER", parametri);
                                startActivity(intentOops);
                                overridePendingTransition(R.anim.fade, R.anim.hold);
                            }
                            /*******************************************************************************************/
                        }
                    }
                });
            }

            bollette.getBackground().setAlpha(255);
            bollette.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
            bollette.getBackground().invalidateSelf();

            eniwebBol.getBackground().setAlpha(255);
            eniwebBol.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
            eniwebBol.getBackground().invalidateSelf();

            final String contoLuceAttivo = getContoLuceAttivo(loginService);
            if (contoLuceAttivo.equals("")) {
                consLuce.getBackground().setAlpha(80);
                consLuce.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
                consLuce.getBackground().invalidateSelf();
            } else {
                consLuce.getBackground().setAlpha(255);
                consLuce.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
                consLuce.getBackground().invalidateSelf();
                consLuce.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        // codiceCliente, segmento, tipologia, pdf, regione,
                        // prodotto, dataAttivazione
                        consLuce.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
                        consLuce.getBackground().invalidateSelf();

                        dialogDisableClicks.show();

                        String parametri = "";
                        String codiceCliente = getCodiceCliente(loginService);
                        parametri = parametri + "&codiceCliente=" + codiceCliente;
                        String segmento = getSegmentoCliente(loginService);
                        parametri = parametri + "&segmento=" + segmento;
                        String tipologiaPdfRegioneProdottoDataattivazione = getTipologiaPdfRegioneProdottoDataAttivazione(loginService, contoLuceAttivo,
                                segmento);
                        parametri = parametri + tipologiaPdfRegioneProdottoDataattivazione;

                        if (!isNetworkAvailable(context)) {
                            dialogDisableClicks.dismiss();
                            Intent intentOops = new Intent(getApplicationContext(), OopsPageConsumiLuce.class);
                            intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.connessione_assente));
                            intentOops.putExtra("PARAMETER", parametri);
                            startActivity(intentOops);
                            overridePendingTransition(R.anim.fade, R.anim.hold);
                        } else {
                            animation = Utilities.animation(Home.this);
                            imageAnim = (ImageView) findViewById(R.id.waiting_anim);
                            imageAnim.setVisibility(View.VISIBLE);
                            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                                imageAnim.setBackgroundDrawable(animation);
                            } else {
                                imageAnim.setBackground(animation);
                            }
                            animation.start();
                            /****************************** Chiamata consumi luce **************************************/
                            ServiceCall consumiLuceServiceCall = ServiceCallFactory.createServiceCall(context);
                            // consumiLuceServiceCall.myCookieStore = new
                            // PersistentCookieStore(context);
                            try {
                                consumiLuceServiceCall.executeHttpsGet(Constants.URL_CONSUMI_LUCE, parametri, Home.this, Constants.FUNZIONALITA_CONSUMI_LUCE,
                                        null);
                            } catch (Exception e) {
                                if (animation != null) {
                                    animation.stop();
                                    imageAnim.setVisibility(View.INVISIBLE);
                                }
                                dialogDisableClicks.dismiss();
                                Intent intentOops = new Intent(getApplicationContext(), OopsPageConsumiLuce.class);
                                intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
                                intentOops.putExtra("PARAMETER", parametri);
                                startActivity(intentOops);
                                overridePendingTransition(R.anim.fade, R.anim.hold);
                            }
                            /*******************************************************************************************/
                        }
                    }
                });
            }

            domiciliazione.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v1) {
                    domiciliazione.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
                    domiciliazione.getBackground().invalidateSelf();

                    dialogDisableClicks.show();

                    JSONArray array;
                    String parametri = "";
                    try {
                        array = new JSONArray(loginService);
                        String arrayCodiciConti[] = elencoCodiciContiAttiviCompleti(array);
                        for (int i = 0; i < arrayCodiciConti.length; i++) {
                            parametri = parametri + "&codiciContoCliente=" + arrayCodiciConti[i];
                        }
                    } catch (JSONException e1) {
                        e1.printStackTrace();
                    }
                    if (!isNetworkAvailable(context)) {
                        if (animation != null) {
                            animation.stop();
                            imageAnim.setVisibility(View.INVISIBLE);
                        }
                        dialogDisableClicks.dismiss();
                        Intent intentOops = new Intent(getApplicationContext(), OopsPageDomiciliazione.class);
                        intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.connessione_assente));
                        intentOops.putExtra("PARAMETER", parametri);
                        Home.this.finish();
                        startActivity(intentOops);
                        overridePendingTransition(R.anim.fade, R.anim.hold);
                    } else {

                        animation = Utilities.animation(Home.this);
                        imageAnim = (ImageView) findViewById(R.id.waiting_anim);
                        imageAnim.setVisibility(View.VISIBLE);
                        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                            imageAnim.setBackgroundDrawable(animation);
                        } else {
                            imageAnim.setBackground(animation);
                        }
                        animation.start();

                        /********************* Chiamata visualizza Forniture Domiciliazione ******************************/
                        ServiceCall visFornDomicilServiceCall = ServiceCallFactory.createServiceCall(context);
                        try {
                            visFornDomicilServiceCall.executeHttpsGet(Constants.URL_VISUALIZZA_FORNITURE_DOMICILIAZIONE, parametri, Home.this,
                                    Constants.FUNZIONALITA_DOMICILIAZIONE, null);
                        } catch (Exception e) {
                            if (animation != null) {
                                animation.stop();
                                imageAnim.setVisibility(View.INVISIBLE);
                            }
                            dialogDisableClicks.dismiss();
                            Intent intentOops = new Intent(getApplicationContext(), OopsPageDomiciliazione.class);
                            intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
                            intentOops.putExtra("PARAMETER", parametri);
                            // Home.this.finish();
                            startActivity(intentOops);
                            overridePendingTransition(R.anim.fade, R.anim.hold);
                        }
                        /**********************************************************************************/
                    }
                }
            });

            bollette.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    bollette.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
                    bollette.getBackground().invalidateSelf();
                    dialogDisableClicks.show();
                    String codiceCliente = getCodiceCliente(loginService);
                    // String primoContoUtilizzabilePieno =
                    // getPrimoContoBollettePagamenti(loginService);
                    String primoContoUtilizzabilePieno = contiBollettePagamenti.get(0);

                    if (!isNetworkAvailable(context)) {
                        if (animation != null) {
                            animation.stop();
                            imageAnim.setVisibility(View.INVISIBLE);
                        }
                        dialogDisableClicks.dismiss();
                        Intent intentOops = new Intent(getApplicationContext(), OopsPageBollettePagamenti.class);
                        intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.connessione_assente));
                        intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
                        intentOops.putExtra("CODICE_CONTO_CLIENTE", primoContoUtilizzabilePieno);
                        startActivity(intentOops);
                        overridePendingTransition(R.anim.fade, R.anim.hold);
                    } else {
                        animation = Utilities.animation(Home.this);
                        imageAnim = (ImageView) findViewById(R.id.waiting_anim);
                        imageAnim.setVisibility(View.VISIBLE);
                        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                            imageAnim.setBackgroundDrawable(animation);
                        } else {
                            imageAnim.setBackground(animation);
                        }
                        animation.start();
                        /********************* Chiamata show Lista fatture ******************************/
                        ServiceCall stAvFornServiceCall = ServiceCallFactory.createServiceCall(context);
                        // stAvFornServiceCall.myCookieStore = new
                        // PersistentCookieStore(context);

                        HashMap<String, String> parametri = new HashMap<String, String>();
                        parametri.put("codiceCliente", codiceCliente);
                        parametri.put("codiceContoCliente", primoContoUtilizzabilePieno);

                        try {
                            stAvFornServiceCall.executeHttpsGet(Constants.URL_SHOW_LISTA_FATTURE, parametri, Home.this,
                                    Constants.FUNZIONALITA_BOLLETTE_PAGAMENTI, null);
                        } catch (Exception e) {
                            if (animation != null) {
                                animation.stop();
                                imageAnim.setVisibility(View.INVISIBLE);
                            }
                            dialogDisableClicks.dismiss();
                            Intent intentOops = new Intent(getApplicationContext(), OopsPageBollettePagamenti.class);
                            intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
                            intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
                            intentOops.putExtra("CODICE_CONTO_CLIENTE", primoContoUtilizzabilePieno);
                            startActivity(intentOops);
                            overridePendingTransition(R.anim.fade, R.anim.hold);
                        }
                    }
                }
            });

            eniwebBol.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    eniwebBol.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
                    eniwebBol.getBackground().invalidateSelf();
                    dialogDisableClicks.show();
                    String codiceCliente = getCodiceCliente(loginService);

                    if (!isNetworkAvailable(context)) {
                        if (animation != null) {
                            animation.stop();
                            imageAnim.setVisibility(View.INVISIBLE);
                        }
                        dialogDisableClicks.dismiss();
                        Intent intentOops = new Intent(getApplicationContext(), OopsPageEniWebBolletta.class);
                        intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.connessione_assente));
                        intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
                        startActivity(intentOops);
                        overridePendingTransition(R.anim.fade, R.anim.hold);
                    } else {

                        animation = Utilities.animation(Home.this);
                        imageAnim = (ImageView) findViewById(R.id.waiting_anim);
                        imageAnim.setVisibility(View.VISIBLE);
                        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                            imageAnim.setBackgroundDrawable(animation);
                        } else {
                            imageAnim.setBackground(animation);
                        }
                        animation.start();
                        ;

                        /***************************** chiamata visualizza info fattura online *******************************/
                        ServiceCall eniWebBollettaServiceCall = ServiceCallFactory.createServiceCall(context);
                        // eniWebBollettaServiceCall.myCookieStore = new
                        // PersistentCookieStore(context);

                        HashMap<String, String> parametri = new HashMap<String, String>();
                        parametri.put("codiceCliente", codiceCliente);

                        try {
                            eniWebBollettaServiceCall.executeHttpsGet(Constants.URL_INFO_FATTURA_ONLINE, parametri, Home.this,
                                    Constants.FUNZIONALITA_ENI_WEB_BOLLETTA, null);
                        } catch (Exception e) {
                            if (animation != null) {
                                animation.stop();
                                imageAnim.setVisibility(View.INVISIBLE);
                            }
                            dialogDisableClicks.dismiss();
                            Intent intentOops = new Intent(getApplicationContext(), OopsPageEniWebBolletta.class);
                            intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
                            intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
                            startActivity(intentOops);
                            overridePendingTransition(R.anim.fade, R.anim.hold);
                        }

                        /*******************************************************************************/
                    }

                }
            });

        } else if (clienteAttivo == 0) {
            // tutte disabilitate tranne energy
            domiciliazione.getBackground().setAlpha(80);
            autolettura.getBackground().setAlpha(80);
            // storico.getBackground().setAlpha(80);
            eniwebBol.getBackground().setAlpha(80);
            consLuce.getBackground().setAlpha(80);
            consGas.getBackground().setAlpha(80);

            statoAttivazioneFornitura.getBackground().setAlpha(255);
            statoAttivazioneFornitura.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
            statoAttivazioneFornitura.getBackground().invalidateSelf();

            bollette.getBackground().setAlpha(255);
            bollette.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
            bollette.getBackground().invalidateSelf();

            bollette.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    bollette.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
                    bollette.getBackground().invalidateSelf();

                    String codiceCliente = getCodiceCliente(loginService);
                    // String primoContoUtilizzabilePieno =
                    // getPrimoContoBollettePagamenti(loginService);
                    String primoContoUtilizzabilePieno = contiBollettePagamenti.get(0);

                    animation = Utilities.animation(Home.this);
                    imageAnim = (ImageView) findViewById(R.id.waiting_anim);
                    imageAnim.setVisibility(View.VISIBLE);
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                        imageAnim.setBackgroundDrawable(animation);
                    } else {
                        imageAnim.setBackground(animation);
                    }
                    animation.start();

                    if (!isNetworkAvailable(context)) {
                        Intent intentOops = new Intent(getApplicationContext(), OopsPageBollettePagamenti.class);
                        intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.connessione_assente));
                        intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
                        intentOops.putExtra("CODICE_CONTO_CLIENTE", primoContoUtilizzabilePieno);
                        // Home.this.finish();
                        startActivity(intentOops);
                        if (animation != null) {
                            animation.stop();
                            imageAnim.setVisibility(View.INVISIBLE);
                        }
                        overridePendingTransition(R.anim.fade, R.anim.hold);
                    } else {
                        /********************* Chiamata show Lista fatture ******************************/
                        ServiceCall stAvFornServiceCall = ServiceCallFactory.createServiceCall(context);
                        // stAvFornServiceCall.myCookieStore = new
                        // PersistentCookieStore(context);

                        HashMap<String, String> parametri = new HashMap<String, String>();
                        parametri.put("codiceCliente", codiceCliente);
                        parametri.put("codiceContoCliente", primoContoUtilizzabilePieno);

                        try {
                            stAvFornServiceCall.executeHttpsGet(Constants.URL_SHOW_LISTA_FATTURE, parametri, Home.this,
                                    Constants.FUNZIONALITA_BOLLETTE_PAGAMENTI, null);
                        } catch (Exception e) {
                            if (animation != null) {
                                animation.stop();
                                imageAnim.setVisibility(View.INVISIBLE);
                            }
                            Intent intentOops = new Intent(getApplicationContext(), OopsPageBollettePagamenti.class);
                            intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
                            intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
                            intentOops.putExtra("CODICE_CONTO_CLIENTE", primoContoUtilizzabilePieno);
                            // Home.this.finish();
                            startActivity(intentOops);
                            overridePendingTransition(R.anim.fade, R.anim.hold);
                        }
                    }
                }
            });

        } else if (fornitureAttive) {
            // vai alla home con icone stAvforn, energy abilitate
            statoAttivazioneFornitura.getBackground().setAlpha(255);
            statoAttivazioneFornitura.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
            statoAttivazioneFornitura.getBackground().invalidateSelf();

            domiciliazione.getBackground().setAlpha(80);
            autolettura.getBackground().setAlpha(80);
            // storico.getBackground().setAlpha(80);
            eniwebBol.getBackground().setAlpha(80);
            consLuce.getBackground().setAlpha(80);
            consGas.getBackground().setAlpha(80);

            final Dialog dialog = new Dialog(Home.this, android.R.style.Theme_Translucent_NoTitleBar);
            dialog.setContentView(R.layout.popup_home);
            dialog.setCancelable(true);
            final ImageView chiudi = (ImageView) dialog.findViewById(R.id.home_popup_chiudi);
            chiudi.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            final ImageView esciInvisibile = (ImageView) dialog.findViewById(R.id.esci_invisible);
            esciInvisibile.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    dialog.dismiss();

                    animation = Utilities.animation(Home.this);
                    imageAnim = (ImageView) findViewById(R.id.waiting_anim);
                    imageAnim.setVisibility(View.VISIBLE);
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                        imageAnim.setBackgroundDrawable(animation);
                    } else {
                        imageAnim.setBackground(animation);
                    }
                    animation.start();

                    Intent intentLogin = new Intent(getApplicationContext(), Login.class);
                    Home.this.finish();
                    startActivity(intentLogin);
                    overridePendingTransition(R.anim.fade, R.anim.hold);
                }
            });

            final ImageView iconaStatoAttForn = (ImageView) dialog.findViewById(R.id.home_icona_invisible_1);
            iconaStatoAttForn.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v1) {
                    dialog.dismiss();

                    animation = Utilities.animation(Home.this);
                    imageAnim = (ImageView) findViewById(R.id.waiting_anim);
                    imageAnim.setVisibility(View.VISIBLE);
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                        imageAnim.setBackgroundDrawable(animation);
                    } else {
                        imageAnim.setBackground(animation);
                    }
                    animation.start();

                    String codiceCliente = getCodiceCliente(loginService);
                    // String primoConto =
                    // getPrimoContoUtilizzabile(loginService);
                    String primoConto = getPrimoConto(loginService);
                    boolean contoUtilizzabile = isContoUtilizzabile(primoConto, loginService);

                    if (!isNetworkAvailable(context)) {
                        Intent intentOops = new Intent(getApplicationContext(), OopsPageAttivazioneFornitura.class);
                        intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.connessione_assente));
                        intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
                        intentOops.putExtra("CODICE_CONTO_CLIENTE", primoConto);
                        // Home.this.finish();
                        startActivity(intentOops);
                        overridePendingTransition(R.anim.fade, R.anim.hold);
                    } else {
                        /********************* Chiamata statoAvanzamento Fornitura ******************************/

                        ServiceCall stAvFornServiceCall = ServiceCallFactory.createServiceCall(context);

                        if (!primoConto.equals("")) {
                            if (contoUtilizzabile) {
                                HashMap<String, String> parametri = new HashMap<String, String>();
                                parametri.put("codiceCliente", codiceCliente);
                                parametri.put("codiceContoCliente", primoConto);

                                try {
                                    stAvFornServiceCall.executeHttpsGet(Constants.URL_STATO_AVANZAMENTO_FORNITURE, parametri, Home.this,
                                            Constants.FUNZIONALITA_STATO_AVANZAMENTO_FORNITURA, null);
                                } catch (Exception e) {
                                    if (animation != null) {
                                        animation.stop();
                                        imageAnim.setVisibility(View.INVISIBLE);
                                    }
                                    Intent intentOops = new Intent(getApplicationContext(), OopsPageAttivazioneFornitura.class);
                                    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
                                    intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
                                    intentOops.putExtra("CODICE_CONTO_CLIENTE", primoConto);
                                    startActivity(intentOops);
                                    overridePendingTransition(R.anim.fade, R.anim.hold);
                                }
                            } else {
                                Intent intentStatoAttivazioneFornitura = new Intent(getApplicationContext(), StatoAvanzamentoFornitura.class);
                                intentStatoAttivazioneFornitura.putExtra("CHIAMATA", false);
                                Home.this.finish();
                                startActivity(intentStatoAttivazioneFornitura);
                                overridePendingTransition(R.anim.fade, R.anim.hold);
                            }
                        } else {
                            Intent intentStatoAttivazioneFornitura = new Intent(getApplicationContext(), StatoAvanzamentoFornitura.class);
                            intentStatoAttivazioneFornitura.putExtra("CHIAMATA", false);
                            Home.this.finish();
                            startActivity(intentStatoAttivazioneFornitura);
                            overridePendingTransition(R.anim.fade, R.anim.hold);
                        }
                    }
                }
            });

            boolean esisteContoPieno = esisteFornituraAttivaPienaOCessata(loginService);
            if (!esisteContoPieno) {
                bollette.getBackground().setAlpha(80);
                bollette.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
                bollette.getBackground().invalidateSelf();
            } else {
                bollette.getBackground().setAlpha(255);
                bollette.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
                bollette.getBackground().invalidateSelf();

                ImageView home_icona_invisible_bollette = (ImageView) dialog.findViewById(R.id.home_icona_invisible_bollette);
                home_icona_invisible_bollette.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        dialog.dismiss();
                        String codiceCliente = getCodiceCliente(loginService);
                        // String primoContoUtilizzabilePieno =
                        // getPrimoContoBollettePagamenti(loginService);
                        String primoContoUtilizzabilePieno = contiBollettePagamenti.get(0);

                        animation = Utilities.animation(Home.this);
                        imageAnim = (ImageView) findViewById(R.id.waiting_anim);
                        imageAnim.setVisibility(View.VISIBLE);
                        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                            imageAnim.setBackgroundDrawable(animation);
                        } else {
                            imageAnim.setBackground(animation);
                        }
                        animation.start();

                        if (!isNetworkAvailable(context)) {
                            Intent intentOops = new Intent(getApplicationContext(), OopsPageBollettePagamenti.class);
                            intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.connessione_assente));
                            intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
                            intentOops.putExtra("CODICE_CONTO_CLIENTE", primoContoUtilizzabilePieno);
                            // Home.this.finish();
                            startActivity(intentOops);
                            if (animation != null) {
                                animation.stop();
                                imageAnim.setVisibility(View.INVISIBLE);
                            }
                            overridePendingTransition(R.anim.fade, R.anim.hold);
                        } else {
                            /********************* Chiamata show Lista fatture ******************************/
                            ServiceCall stAvFornServiceCall = ServiceCallFactory.createServiceCall(context);
                            // stAvFornServiceCall.myCookieStore = new
                            // PersistentCookieStore(context);

                            HashMap<String, String> parametri = new HashMap<String, String>();
                            parametri.put("codiceCliente", codiceCliente);
                            parametri.put("codiceContoCliente", primoContoUtilizzabilePieno);

                            try {
                                stAvFornServiceCall.executeHttpsGet(Constants.URL_SHOW_LISTA_FATTURE, parametri, Home.this,
                                        Constants.FUNZIONALITA_BOLLETTE_PAGAMENTI, null);
                            } catch (Exception e) {
                                if (animation != null) {
                                    animation.stop();
                                    imageAnim.setVisibility(View.INVISIBLE);
                                }
                                Intent intentOops = new Intent(getApplicationContext(), OopsPageBollettePagamenti.class);
                                intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
                                intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
                                intentOops.putExtra("CODICE_CONTO_CLIENTE", primoContoUtilizzabilePieno);
                                startActivity(intentOops);
                                overridePendingTransition(R.anim.fade, R.anim.hold);
                            }
                        }
                    }
                });

                bollette.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        bollette.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
                        bollette.getBackground().invalidateSelf();

                        String codiceCliente = getCodiceCliente(loginService);
                        // String primoContoUtilizzabilePieno =
                        // getPrimoContoBollettePagamenti(loginService);
                        String primoContoUtilizzabilePieno = contiBollettePagamenti.get(0);

                        animation = Utilities.animation(Home.this);
                        imageAnim = (ImageView) findViewById(R.id.waiting_anim);
                        imageAnim.setVisibility(View.VISIBLE);
                        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                            imageAnim.setBackgroundDrawable(animation);
                        } else {
                            imageAnim.setBackground(animation);
                        }
                        animation.start();

                        if (!isNetworkAvailable(context)) {
                            Intent intentOops = new Intent(getApplicationContext(), OopsPageBollettePagamenti.class);
                            intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.connessione_assente));
                            intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
                            intentOops.putExtra("CODICE_CONTO_CLIENTE", primoContoUtilizzabilePieno);
                            // Home.this.finish();
                            startActivity(intentOops);
                            if (animation != null) {
                                animation.stop();
                                imageAnim.setVisibility(View.INVISIBLE);
                            }
                            overridePendingTransition(R.anim.fade, R.anim.hold);
                        } else {
                            /********************* Chiamata show Lista fatture ******************************/
                            ServiceCall stAvFornServiceCall = ServiceCallFactory.createServiceCall(context);
                            // stAvFornServiceCall.myCookieStore = new
                            // PersistentCookieStore(context);

                            HashMap<String, String> parametri = new HashMap<String, String>();
                            parametri.put("codiceCliente", codiceCliente);
                            parametri.put("codiceContoCliente", primoContoUtilizzabilePieno);

                            try {
                                stAvFornServiceCall.executeHttpsGet(Constants.URL_SHOW_LISTA_FATTURE, parametri, Home.this,
                                        Constants.FUNZIONALITA_BOLLETTE_PAGAMENTI, null);
                            } catch (Exception e) {
                                if (animation != null) {
                                    animation.stop();
                                    imageAnim.setVisibility(View.INVISIBLE);
                                }
                                Intent intentOops = new Intent(getApplicationContext(), OopsPageBollettePagamenti.class);
                                intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
                                intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
                                intentOops.putExtra("CODICE_CONTO_CLIENTE", primoContoUtilizzabilePieno);
                                // Home.this.finish();
                                startActivity(intentOops);
                                overridePendingTransition(R.anim.fade, R.anim.hold);
                            }
                        }
                    }
                });
            }

            dialog.show();

        }
        statoAttivazioneFornitura.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v1) {
                statoAttivazioneFornitura.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
                statoAttivazioneFornitura.getBackground().invalidateSelf();

                dialogDisableClicks.show();

                animation = Utilities.animation(Home.this);
                imageAnim = (ImageView) findViewById(R.id.waiting_anim);
                imageAnim.setVisibility(View.VISIBLE);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    imageAnim.setBackgroundDrawable(animation);
                } else {
                    imageAnim.setBackground(animation);
                }
                animation.start();

                if (!isNetworkAvailable(context)) {
                    String codiceCliente = getCodiceCliente(loginService);
                    String primoConto = getPrimoContoUtilizzabile(loginService);

                    if (animation != null) {
                        animation.stop();
                        imageAnim.setVisibility(View.INVISIBLE);
                    }

                    dialogDisableClicks.dismiss();

                    Intent intentOops = new Intent(Home.this, OopsPageGeneric.class);
                    intentOops.putExtra(Constants.ERROR_TYPE, Constants.ERROR_CONNECTION_NOT_AVAILABLE);
                    intentOops.putExtra(Constants.PREVIOUS_PAGE, Constants.PAGE_STATO_ATTIVAZIONE_FORNITURA);
                    intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
                    intentOops.putExtra("CODICE_CONTO_CLIENTE", primoConto);
                    startActivity(intentOops);
                    overridePendingTransition(R.anim.fade, R.anim.hold);
                } else {
                    /********************* Chiamata statoAvanzamento Fornitura ******************************/
                    ServiceCall stAvFornServiceCall = ServiceCallFactory.createServiceCall(context);

                    String codiceCliente = getCodiceCliente(loginService);
                    // String primoConto =
                    // getPrimoContoUtilizzabile(loginService);
                    String primoConto = getPrimoConto(loginService);

                    dialogDisableClicks.dismiss();

                    if (!primoConto.equals("")) {
                        boolean contoUtilizzabile = isContoUtilizzabile(primoConto, loginService);

                        if (contoUtilizzabile) {
                            HashMap<String, String> parametri = new HashMap<String, String>();
                            parametri.put("codiceCliente", codiceCliente);
                            parametri.put("codiceContoCliente", primoConto);

                            try {
                                stAvFornServiceCall.executeHttpsGet(Constants.URL_STATO_AVANZAMENTO_FORNITURE, parametri, Home.this,
                                        Constants.FUNZIONALITA_STATO_AVANZAMENTO_FORNITURA, null);
                            } catch (Exception e) {
                                if (animation != null) {
                                    animation.stop();
                                    imageAnim.setVisibility(View.INVISIBLE);
                                }
                                dialogDisableClicks.dismiss();
                                Intent intentOops = new Intent(Home.this, OopsPageGeneric.class);
                                intentOops.putExtra(Constants.ERROR_TYPE, Constants.ERROR_SERVICE_NOT_AVAILABLE);
                                intentOops.putExtra(Constants.PREVIOUS_PAGE, Constants.PAGE_STATO_ATTIVAZIONE_FORNITURA);
                                intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
                                intentOops.putExtra("CODICE_CONTO_CLIENTE", primoConto);
                                startActivity(intentOops);
                                overridePendingTransition(R.anim.fade, R.anim.hold);
                            }
                        } else {
                            Constants.SERVIZIO_STATO_AVANZAMENTO_FORNITURA = "";
                            Intent intentStatoAttivazioneFornitura = new Intent(getApplicationContext(), StatoAvanzamentoFornitura.class);
                            intentStatoAttivazioneFornitura.putExtra("CHIAMATA", false);
                            Home.this.finish();
                            startActivity(intentStatoAttivazioneFornitura);
                            overridePendingTransition(R.anim.fade, R.anim.hold);
                        }
                    } else {
                        Constants.SERVIZIO_STATO_AVANZAMENTO_FORNITURA = "";
                        Intent intentStatoAttivazioneFornitura = new Intent(getApplicationContext(), StatoAvanzamentoFornitura.class);
                        intentStatoAttivazioneFornitura.putExtra("CHIAMATA", false);
                        Home.this.finish();
                        startActivity(intentStatoAttivazioneFornitura);
                        overridePendingTransition(R.anim.fade, R.anim.hold);
                    }

                }
            }
        });
        energyStoreLocator.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
        energyStoreLocator.getBackground().invalidateSelf();
        energyStoreLocator.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v1) {
                energyStoreLocator.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
                energyStoreLocator.getBackground().invalidateSelf();
                dialogDisableClicks.show();

                animation = Utilities.animation(Home.this);
                imageAnim = (ImageView) findViewById(R.id.waiting_anim);
                imageAnim.setVisibility(View.VISIBLE);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    imageAnim.setBackgroundDrawable(animation);
                } else {
                    imageAnim.setBackground(animation);
                }
                animation.start();

                ((EniApplication)getApplication()).sendAnalyticsEvent("[Android] - energy store locator");
                Intent intentEnergyStoreLocator = new Intent(getApplicationContext(), TrovaEni.class);
                dialogDisableClicks.dismiss();
                Home.this.finish();
                startActivity(intentEnergyStoreLocator);
                overridePendingTransition(R.anim.fade, R.anim.hold);
            }
        });

        guidaBolletta.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
        guidaBolletta.getBackground().invalidateSelf();
        guidaBolletta.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v1) {
                guidaBolletta.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
                guidaBolletta.getBackground().invalidateSelf();
                dialogDisableClicks.show();

                animation = Utilities.animation(Home.this);
                imageAnim = (ImageView) findViewById(R.id.waiting_anim);
                imageAnim.setVisibility(View.VISIBLE);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    imageAnim.setBackgroundDrawable(animation);
                } else {
                    imageAnim.setBackground(animation);
                }
                animation.start();

                Intent intentGuidaBolletta = new Intent(getApplicationContext(), GuidaBolletta.class);
                dialogDisableClicks.dismiss();
                Home.this.finish();
                startActivity(intentGuidaBolletta);
                overridePendingTransition(R.anim.fade, R.anim.hold);
            }
        });
        final ImageView esci = (ImageView) findViewById(R.id.home_esci);
        esci.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
        esci.getBackground().invalidateSelf();
        esci.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v1) {
                esci.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
                esci.getBackground().invalidateSelf();

                animation = Utilities.animation(Home.this);
                imageAnim = (ImageView) findViewById(R.id.waiting_anim);
                imageAnim.setVisibility(View.VISIBLE);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    imageAnim.setBackgroundDrawable(animation);
                } else {
                    imageAnim.setBackground(animation);
                }
                animation.start();

                Intent intentLogin = new Intent(getApplicationContext(), Login.class);
                Home.this.finish();
                startActivity(intentLogin);
                overridePendingTransition(R.anim.fade, R.anim.hold);
            }
        });

        final LinearLayout scroller = (LinearLayout) findViewById(R.id.home_scroller);
        scroller.removeAllViews();
        final HorizontalPager pager = (HorizontalPager) findViewById(R.id.pager);
        int numeroPagine = pager.getChildCount();
        for (int i = 0; i < numeroPagine; i++) {
            ImageView iv = new ImageView(this);
            iv.setId(i);
            LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            param.setMargins(10, 0, 0, 0);
            iv.setImageResource(R.drawable.dot_not_selected);
            iv.setLayoutParams(param);
            scroller.addView(iv);

            int idFocusedPage = pager.getCurrentPage();
            ImageView pageFocused = (ImageView) findViewById(idFocusedPage);
            pageFocused.setImageResource(R.drawable.dot_selected);

        }

        pager.addOnScrollListener(new HorizontalPager.OnScrollListener() {
            int numeroPagine = pager.getChildCount();

            public void onScroll(int scrollX) {
                for (int i = 0; i < numeroPagine; i++) {
                    ImageView iv = (ImageView) findViewById(i);
                    iv.setImageResource(R.drawable.dot_not_selected);
                }
                int idFocusedPage = pager.getCurrentPage();
                ImageView pageFocused = (ImageView) findViewById(idFocusedPage);
                pageFocused.setImageResource(R.drawable.dot_selected);
            }

            public void onViewScrollFinished(int currentPage) {
                for (int i = 0; i < numeroPagine; i++) {
                    ImageView iv = (ImageView) findViewById(i);
                    iv.setImageResource(R.drawable.dot_not_selected);
                }
                int idFocusedPage = pager.getCurrentPage();
                ImageView pageFocused = (ImageView) findViewById(idFocusedPage);
                pageFocused.setImageResource(R.drawable.dot_selected);
            }
        });

    }

    public static Context getContext() {
        return Home.staticContext;
    }

    public static Dialog getDisableClicksDialog() {
        return Home.dialogDisableClicks;
    }

    public boolean esisteFornituraAttiva(String rispostaLogin) {
        boolean result = false;
        try {
            JSONArray loginJson = new JSONArray(rispostaLogin);
            JSONObject datiCliente = (JSONObject) loginJson.get(1);
            String statoCliente = datiCliente.getString("StatoCliente");
            if (statoCliente.equals("IN ATTIVAZIONE")) {
                for (int i = 2; i < loginJson.length(); i++) {
                    JSONArray fornitura = (JSONArray) loginJson.get(i);
                    JSONObject fornituraAnagr = (JSONObject) fornitura.get(0);
                    String statoConto = fornituraAnagr.getString("StatoConto");
                    if (statoConto.equals("UTILIZZABILE")) {
                        return true;
                    } else {
                        result = false;
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public boolean esisteFornituraAttivaPienaOCessata(String rispostaLogin) {
        boolean result = false;
        try {
            JSONArray loginJson = new JSONArray(rispostaLogin);
            JSONObject datiCliente = (JSONObject) loginJson.get(1);
            String statoCliente = datiCliente.getString("StatoCliente");
            if (statoCliente.equals("IN ATTIVAZIONE")) {
                for (int i = 2; i < loginJson.length(); i++) {
                    JSONArray fornitura = (JSONArray) loginJson.get(i);
                    JSONObject fornituraAnagr = (JSONObject) fornitura.get(0);
                    String statoConto = fornituraAnagr.getString("StatoConto");
                    if ((statoConto.equals("UTILIZZABILE") && fornitura.length() > 1) || statoConto.equals("NON UTILIZZABILE")) {
                        return true;
                    } else {
                        continue;
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public int isClienteAttivo(String rispostaLogin) {
        int result = -1;
        try {
            JSONArray loginJson = new JSONArray(rispostaLogin);
            JSONObject datiCliente = (JSONObject) loginJson.get(1);
            String statoCliente = datiCliente.getString("StatoCliente");
            if (statoCliente.equals("ATTIVO")) {
                result = 1;
            } else if (statoCliente.equals("NON ATTIVO")) {
                result = 0;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public String getCodiceCliente(String rispostaLogin) {
        String result = "";
        try {
            JSONArray loginJson = new JSONArray(rispostaLogin);
            JSONObject datiCliente = (JSONObject) loginJson.get(1);
            result = datiCliente.getString("CodiceCliente");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public String getSegmentoCliente(String rispostaLogin) {
        String result = "";
        try {
            JSONArray loginJson = new JSONArray(rispostaLogin);
            JSONObject datiCliente = (JSONObject) loginJson.get(1);
            result = datiCliente.getString("SegmentoCliente");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public boolean isContoUtilizzabile(String conto, String arrayString) {
        boolean result = false;
        String codiceContoIesimo = "";
        JSONObject anagrContoIesimo = new JSONObject();
        try {
            JSONArray array = new JSONArray(arrayString);
            for (int i = 2; i < array.length(); i++) {
                JSONArray contoIesimo;
                contoIesimo = array.getJSONArray(i);
                // codice conto
                anagrContoIesimo = contoIesimo.getJSONObject(0);
                codiceContoIesimo = anagrContoIesimo.getString("CodiceConto");
                if (codiceContoIesimo.equals(conto)) {
                    String statoCodiceContoIesimo = anagrContoIesimo.getString("StatoConto");
                    if (statoCodiceContoIesimo.equals("UTILIZZABILE")) {
                        return true;
                    } else {
                        result = false;
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public String getPrimoConto(String rispostaLogin) {
        String result = "";
        try {
            JSONArray loginJson = new JSONArray(rispostaLogin);
            JSONObject datiCliente = (JSONObject) loginJson.get(1);
            String statoCliente = datiCliente.getString("StatoCliente");
            if (!statoCliente.equals("NON ATTIVO")) {
                if (loginJson.length() > 2) {
                    JSONArray fornitura = (JSONArray) loginJson.get(2);
                    JSONObject fornituraAnagr = (JSONObject) fornitura.get(0);
                    return fornituraAnagr.getString("CodiceConto");
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public String getPrimoContoUtilizzabile(String rispostaLogin) {
        String result = "";
        try {
            JSONArray loginJson = new JSONArray(rispostaLogin);
            JSONObject datiCliente = (JSONObject) loginJson.get(1);
            String statoCliente = datiCliente.getString("StatoCliente");
            if (!statoCliente.equals("NON ATTIVO")) {
                for (int i = 2; i < loginJson.length(); i++) {
                    JSONArray fornitura = (JSONArray) loginJson.get(i);
                    JSONObject fornituraAnagr = (JSONObject) fornitura.get(0);
                    String statoConto = fornituraAnagr.getString("StatoConto");
                    if (statoConto.equals("UTILIZZABILE")) {
                        return fornituraAnagr.getString("CodiceConto");
                    } else {
                        continue;
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public String getPrimoContoUtilizzabilePieno(String rispostaLogin) {
        String result = "";
        try {
            JSONArray loginJson = new JSONArray(rispostaLogin);
            JSONObject datiCliente = (JSONObject) loginJson.get(1);
            String statoCliente = datiCliente.getString("StatoCliente");
            if (!statoCliente.equals("NON ATTIVO")) {
                for (int i = 2; i < loginJson.length(); i++) {
                    JSONArray fornitura = (JSONArray) loginJson.get(i);
                    JSONObject fornituraAnagr = (JSONObject) fornitura.get(0);
                    String statoConto = fornituraAnagr.getString("StatoConto");
                    if (statoConto.equals("UTILIZZABILE")) {
                        if (fornitura.length() == 1) {
                            continue;
                        } else {
                            return fornituraAnagr.getString("CodiceConto");
                        }
                    } else {
                        continue;
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public String getPrimoContoBollettePagamenti(String rispostaLogin) {
        String result = "";
        try {
            JSONArray loginJson = new JSONArray(rispostaLogin);
            for (int i = 2; i < loginJson.length(); i++) {
                JSONArray fornitura = (JSONArray) loginJson.get(i);
                JSONObject fornituraAnagr = (JSONObject) fornitura.get(0);
                String statoConto = fornituraAnagr.getString("StatoConto");
                if (statoConto.equals("UTILIZZABILE")) {
                    if (fornitura.length() == 1) {
                        continue;
                    } else {
                        return fornituraAnagr.getString("CodiceConto");
                    }
                } else if (statoConto.equals("NON UTILIZZABILE")) {
                    return fornituraAnagr.getString("CodiceConto");
                } else {
                    continue;
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public void inizializzaContiBollettePagamenti(String rispostaLogin) {
        try {
            JSONArray loginJson = new JSONArray(rispostaLogin);
            for (int i = 2; i < loginJson.length(); i++) {
                JSONArray fornitura = (JSONArray) loginJson.get(i);
                JSONObject fornituraAnagr = (JSONObject) fornitura.get(0);
                String statoConto = fornituraAnagr.getString("StatoConto");
                if (statoConto.equals("UTILIZZABILE")) {
                    if (fornitura.length() == 1) {
                        continue;
                    } else {
                        if (!contiBollettePagamenti.contains(fornituraAnagr.getString("CodiceConto"))) {
                            contiBollettePagamenti.add(fornituraAnagr.getString("CodiceConto"));
                        }
                    }
                } else if (statoConto.equals("NON UTILIZZABILE")) {
                    if (!contiBollettePagamenti.contains(fornituraAnagr.getString("CodiceConto"))) {
                        contiBollettePagamenti.add(fornituraAnagr.getString("CodiceConto"));
                    }
                } else {
                    continue;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void inizializzaContiConsumiLuce(String rispostaLogin) {
        try {
            JSONArray loginJson = new JSONArray(rispostaLogin);
            for (int i = 2; i < loginJson.length(); i++) {
                JSONArray fornitura = (JSONArray) loginJson.get(i);
                JSONObject fornituraAnagr = (JSONObject) fornitura.get(0);
                String statoConto = fornituraAnagr.getString("StatoConto");
                if (statoConto.equals("UTILIZZABILE")) {
                    if (fornitura.length() == 1) {
                        continue;
                    } else {
                        JSONObject fornituraAnagrExtra = fornitura.getJSONObject(1);
                        String tipologia = fornituraAnagrExtra.getString("Tipologia");
                        if (!tipologia.equals("POWER")) {
                            continue;
                        } else {
                            if (!contiConsumiLuce.contains(fornituraAnagr.getString("CodiceConto"))) {
                                contiConsumiLuce.add(fornituraAnagr.getString("CodiceConto"));
                            }
                        }
                    }
                } else {
                    continue;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<String> elencoContiBollettePagamenti() {
        ArrayList<String> result = new ArrayList<String>();
        try {
            JSONArray loginJson = new JSONArray(Constants.SERVIZIO_LOGIN);
            for (int i = 2; i < loginJson.length(); i++) {
                JSONArray fornitura = (JSONArray) loginJson.get(i);
                JSONObject fornituraAnagr = (JSONObject) fornitura.get(0);
                String statoConto = fornituraAnagr.getString("StatoConto");
                if (statoConto.equals("UTILIZZABILE")) {
                    if (fornitura.length() == 1) {
                        continue;
                    } else {
                        if (!result.contains(fornituraAnagr.getString("CodiceConto"))) {
                            result.add(fornituraAnagr.getString("CodiceConto"));
                        }
                    }
                } else if (statoConto.equals("NON UTILIZZABILE")) {
                    if (!result.contains(fornituraAnagr.getString("CodiceConto"))) {
                        result.add(fornituraAnagr.getString("CodiceConto"));
                    }
                } else {
                    continue;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public ArrayList<String> elencoContiConsumiLuce() {
        ArrayList<String> result = new ArrayList<String>();
        try {
            JSONArray loginJson = new JSONArray(Constants.SERVIZIO_LOGIN);
            for (int i = 2; i < loginJson.length(); i++) {
                JSONArray fornitura = (JSONArray) loginJson.get(i);
                JSONObject fornituraAnagr = (JSONObject) fornitura.get(0);
                String statoConto = fornituraAnagr.getString("StatoConto");
                if (statoConto.equals("UTILIZZABILE")) {
                    if (fornitura.length() == 1) {
                        continue;
                    } else {
                        JSONObject fornituraAnagrExtra = fornitura.getJSONObject(1);
                        String tipologia = fornituraAnagrExtra.getString("Tipologia");
                        if (!tipologia.equals("POWER")) {
                            continue;
                        } else {
                            if (!result.contains(fornituraAnagr.getString("CodiceConto"))) {
                                String conto = fornituraAnagr.getString("CodiceConto");
                                result.add(conto);
                            }
                        }
                    }
                } else {
                    continue;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public String[] elencoCodiciConti(JSONArray array) {
        int lunghezza = numeroCodiciContoDiversi(array);
        String result[] = new String[lunghezza];
        String codiceContoConfrontato = "";
        String codiceContoIesimo = "";
        String codiciConcatenati = "";
        int count = 0;
        for (int i = 2; i < array.length(); i++) {
            JSONArray contoIesimo;
            try {
                contoIesimo = array.getJSONArray(i);
                // codice conto
                JSONObject anagrContoIesimo = contoIesimo.getJSONObject(0);
                codiceContoIesimo = anagrContoIesimo.getString("CodiceConto");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (!codiceContoIesimo.equals(codiceContoConfrontato)) {
                if (!codiciConcatenati.contains("-" + codiceContoIesimo)) {
                    // codice conto diverso
                    codiceContoConfrontato = codiceContoIesimo;
                    codiciConcatenati = codiciConcatenati + "-" + codiceContoIesimo;
                    result[count] = codiceContoConfrontato;
                    count++;
                }
            }
        }
        return result;
    }

    public String[] elencoCodiciContiAttiviCompleti(JSONArray array) {
        int lunghezza = numeroCodiciContoDiversiAttiviCompleti(array);
        String result[] = new String[lunghezza];
        String codiceContoConfrontato = "";
        String codiceContoIesimo = "";
        String codiciConcatenati = "";
        int count = 0;
        for (int i = 2; i < array.length(); i++) {
            JSONArray contoIesimo;
            try {
                contoIesimo = array.getJSONArray(i);
                // codice conto
                JSONObject anagrContoIesimo = contoIesimo.getJSONObject(0);
                codiceContoIesimo = anagrContoIesimo.getString("CodiceConto");

                if (!codiceContoIesimo.equals(codiceContoConfrontato)) {
                    String stato = anagrContoIesimo.getString("StatoConto");
                    if (!stato.equals("UTILIZZABILE")) {
                        continue;
                    } else {
                        // codice conto utilizzabile diverso, controllo non sia
                        // vuoto
                        if (contoIesimo.length() > 1) {
                            // che non sia gia' presente
                            boolean isPresente = codiciConcatenati.contains("-" + codiceContoIesimo + "-");
                            if (isPresente) {
                                continue;
                            } else {
                                codiceContoConfrontato = codiceContoIesimo;
                                result[count] = codiceContoConfrontato;
                                codiciConcatenati = codiciConcatenati + "-" + codiceContoIesimo + "-";
                                count++;
                            }
                        } else {
                            continue;
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public int numeroCodiciContoDiversi(JSONArray array) {
        int result = 0;
        String codiceContoConfrontato = "";
        String codiceContoIesimo = "";
        String codiciConcatenati = "";
        for (int i = 2; i < array.length(); i++) {
            JSONArray contoIesimo;
            try {
                contoIesimo = array.getJSONArray(i);
                // codice conto
                JSONObject anagrContoIesimo = contoIesimo.getJSONObject(0);
                codiceContoIesimo = anagrContoIesimo.getString("CodiceConto");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (!codiceContoIesimo.equals(codiceContoConfrontato)) {
                if (!codiciConcatenati.contains("-" + codiceContoIesimo)) {
                    // codice conto diverso
                    codiceContoConfrontato = codiceContoIesimo;
                    codiciConcatenati = codiciConcatenati + "-" + codiceContoIesimo;
                    result++;
                }
            }
        }
        return result;
    }

    public int numeroCodiciContoDiversiAttiviCompleti(JSONArray array) {
        int result = 0;
        String codiceContoConfrontato = "";
        String codiceContoIesimo = "";
        String codiciConcatenati = "";
        for (int i = 2; i < array.length(); i++) {
            JSONArray contoIesimo;
            try {
                contoIesimo = array.getJSONArray(i);
                // codice conto
                JSONObject anagrContoIesimo = contoIesimo.getJSONObject(0);
                codiceContoIesimo = anagrContoIesimo.getString("CodiceConto");

                if (!codiceContoIesimo.equals(codiceContoConfrontato)) {
                    String stato = anagrContoIesimo.getString("StatoConto");
                    if (!stato.equals("UTILIZZABILE")) {
                        continue;
                    } else {
                        // codice conto utilizzabile diverso, controllo non sia
                        // vuoto
                        if (contoIesimo.length() > 1) {
                            if (!codiciConcatenati.contains("-" + codiceContoIesimo)) {
                                // codice conto diverso
                                codiceContoConfrontato = codiceContoIesimo;
                                codiciConcatenati = codiciConcatenati + "-" + codiceContoIesimo;
                                result++;
                            }
                        } else {
                            continue;
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public boolean randomBoolean() {
        return Math.random() < 0.5;
    }

    public boolean isDual(JSONArray arrayFornitureLogin, String codiceConto) {
        boolean result = false;
        int count = 0;
        JSONObject anagrContoIesimo = new JSONObject();
        try {
            for (int i = 2; i < arrayFornitureLogin.length(); i++) {
                JSONArray contoIesimo;
                contoIesimo = arrayFornitureLogin.getJSONArray(i);
                anagrContoIesimo = contoIesimo.getJSONObject(0);

                String codiceContoIesimo = anagrContoIesimo.getString("CodiceConto");
                if (!codiceContoIesimo.equals(codiceConto)) {
                    continue;
                } else {
                    // ho trovato il conto codiceConto
                    count++;
                }
            }
            if (count == 2) {
                result = true;
            } else {
                result = false;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;

    }

    public String getPdfGas(JSONArray arrayFornitureLogin, String codiceConto) {
        String result = "";
        JSONObject anagrContoIesimo = new JSONObject();
        JSONObject anagrExtraContoIesimo = new JSONObject();
        try {
            for (int i = 2; i < arrayFornitureLogin.length(); i++) {
                JSONArray contoIesimo;
                contoIesimo = arrayFornitureLogin.getJSONArray(i);
                anagrContoIesimo = contoIesimo.getJSONObject(0);

                String codiceContoIesimo = anagrContoIesimo.getString("CodiceConto");
                if (!codiceContoIesimo.equals(codiceConto)) {
                    continue;
                } else {
                    // ho trovato il conto codiceConto
                    anagrExtraContoIesimo = contoIesimo.getJSONObject(1);
                    String tipologia = anagrExtraContoIesimo.getString("Tipologia");
                    if (!tipologia.equals("GAS")) {
                        continue;
                    } else {
                        result = anagrExtraContoIesimo.getString("Pdf");
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;

    }

    public String getContoLuceAttivo(String stringLogin) {
        String result = "";
        JSONObject anagrContoIesimo = new JSONObject();
        JSONObject anagrExtraContoIesimo = new JSONObject();
        try {
            JSONArray arrayLogin = new JSONArray(stringLogin);
            for (int i = 2; i < arrayLogin.length(); i++) {
                JSONArray contoIesimo;
                contoIesimo = arrayLogin.getJSONArray(i);
                anagrContoIesimo = contoIesimo.getJSONObject(0);
                String stato = anagrContoIesimo.getString("StatoConto");
                if (!stato.equals("UTILIZZABILE")) {
                    continue;
                } else {
                    if (contoIesimo.length() < 2) {
                        continue;
                    } else {
                        anagrExtraContoIesimo = contoIesimo.getJSONObject(1);
                        String tipologia = anagrExtraContoIesimo.getString("Tipologia");
                        if (!tipologia.equals("POWER")) {
                            continue;
                        } else {
                            // conto luce attivo e pieno
                            return anagrContoIesimo.getString("CodiceConto");
                        }
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public String getContoGasAttivo(String stringLogin) {
        String result = "";
        JSONObject anagrContoIesimo = new JSONObject();
        JSONObject anagrExtraContoIesimo = new JSONObject();
        try {
            JSONArray arrayLogin = new JSONArray(stringLogin);
            for (int i = 2; i < arrayLogin.length(); i++) {
                JSONArray contoIesimo;
                contoIesimo = arrayLogin.getJSONArray(i);
                anagrContoIesimo = contoIesimo.getJSONObject(0);
                String stato = anagrContoIesimo.getString("StatoConto");
                if (!stato.equals("UTILIZZABILE")) {
                    continue;
                } else {
                    if (contoIesimo.length() < 2) {
                        continue;
                    } else {
                        anagrExtraContoIesimo = contoIesimo.getJSONObject(1);
                        String tipologia = anagrExtraContoIesimo.getString("Tipologia");
                        if (!tipologia.equals("GAS")) {
                            continue;
                        } else {
                            // conto luce attivo e pieno
                            return anagrContoIesimo.getString("CodiceConto");
                        }
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public String getTipologiaPdfRegioneProdottoDataAttivazione(String stringLogin, String codiceContoLuce, String segmento) {
        String result = "";
        JSONObject anagrContoIesimo = new JSONObject();
        JSONObject anagrExtraContoIesimo = new JSONObject();
        try {
            JSONArray arrayLogin = new JSONArray(stringLogin);
            JSONObject anagrCliente = arrayLogin.getJSONObject(1);
            String tipologia = anagrCliente.getString("TipologiaCliente");
            result = result + "&tipologia=" + tipologia;
            for (int i = 2; i < arrayLogin.length(); i++) {
                JSONArray contoIesimo;
                contoIesimo = arrayLogin.getJSONArray(i);
                anagrContoIesimo = contoIesimo.getJSONObject(0);
                String codiceConto = anagrContoIesimo.getString("CodiceConto");
                if (!codiceConto.equals(codiceContoLuce)) {
                    continue;
                } else {
                    if (contoIesimo.length() < 2) {
                        continue;
                    } else {
                        anagrExtraContoIesimo = contoIesimo.getJSONObject(1);
                        String tipologiaLuceGase = anagrExtraContoIesimo.getString("Tipologia");
                        if (!tipologiaLuceGase.equals("POWER")) {
                            continue;
                        } else {
                            // trovato il conto
                            // tipologia, pdf, regione, prodotto,
                            // dataAttivazione
                            String pdf = anagrExtraContoIesimo.getString("Pdf");
                            result = result + "&pdf=" + pdf;
                            String regione = anagrExtraContoIesimo.getString("Regione");
                            result = result + "&regione=" + regione;
                            String prodotto = getProdotto(stringLogin, codiceContoLuce);
                            if (!prodotto.equals("") && (tipologia.equals("RETAIL") && segmento.equals("RESIDENTIAL"))) {
                                result = result + "&codiceProdotto=" + prodotto;
                            }
                            String dataAttivazione = getDataAttivazione(stringLogin, codiceContoLuce, "POWER");
                            if (!dataAttivazione.equals("")) {
                                result = result + "&dataAttivazione=" + dataAttivazione;
                            }
                        }
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public String getTipologiaPdfRegioneUtilizzoGasDataAttivazione(String stringLogin, String codiceContoGas, String segmento) {
        String result = "";
        JSONObject anagrContoIesimo = new JSONObject();
        JSONObject anagrExtraContoIesimo = new JSONObject();
        try {
            JSONArray arrayLogin = new JSONArray(stringLogin);
            JSONObject anagrCliente = arrayLogin.getJSONObject(1);
            String tipologia = anagrCliente.getString("TipologiaCliente");
            result = result + "&tipologia=" + tipologia;
            for (int i = 2; i < arrayLogin.length(); i++) {
                JSONArray contoIesimo;
                contoIesimo = arrayLogin.getJSONArray(i);
                anagrContoIesimo = contoIesimo.getJSONObject(0);
                String codiceConto = anagrContoIesimo.getString("CodiceConto");
                if (!codiceConto.equals(codiceContoGas)) {
                    continue;
                } else {
                    if (contoIesimo.length() < 2) {
                        continue;
                    } else {
                        anagrExtraContoIesimo = contoIesimo.getJSONObject(1);
                        String tipologiaLuceGase = anagrExtraContoIesimo.getString("Tipologia");
                        if (!tipologiaLuceGase.equals("GAS")) {
                            continue;
                        } else {
                            // trovato il conto
                            // tipologia, pdf, regione, prodotto,
                            // dataAttivazione
                            String pdf = anagrExtraContoIesimo.getString("Pdf");
                            result = result + "&pdf=" + pdf;
                            String regione = anagrExtraContoIesimo.getString("Regione");
                            result = result + "&regione=" + regione;
                            String utilizzoGas = getUtilizzoGas(stringLogin, codiceContoGas);
                            if (!utilizzoGas.equals("") && (tipologia.equals("RETAIL") && segmento.equals("RESIDENTIAL"))) {
                                result = result + "&tipoUtilizzoGas=" + utilizzoGas;
                            }
                            String dataAttivazione = getDataAttivazione(stringLogin, codiceContoGas, "GAS");
                            if (!dataAttivazione.equals("")) {
                                result = result + "&dataAttivazione=" + dataAttivazione;
                            }
                        }
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public String getProdotto(String stringLogin, String codiceContoLuce) {
        String result = "";
        JSONObject anagrContoIesimo = new JSONObject();
        JSONObject anagrExtraContoIesimo = new JSONObject();
        try {
            JSONArray arrayLogin = new JSONArray(stringLogin);
            for (int i = 2; i < arrayLogin.length(); i++) {
                JSONArray contoIesimo;
                contoIesimo = arrayLogin.getJSONArray(i);
                anagrContoIesimo = contoIesimo.getJSONObject(0);
                if (contoIesimo.length() < 2) {
                    continue;
                } else {
                    anagrExtraContoIesimo = contoIesimo.getJSONObject(1);
                    String tipologiaLuceGase = anagrExtraContoIesimo.getString("Tipologia");
                    String codiceConto = anagrContoIesimo.getString("CodiceConto");
                    if (!codiceConto.equals(codiceContoLuce)) {
                        continue;
                    } else {
                        if (tipologiaLuceGase.equals("POWER")) {
                            // trovato il conto
                            result = anagrExtraContoIesimo.getString("CodiceProdotto");
                            result = result.replaceAll(" ", "%20");
                            break;
                        }
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public String getUtilizzoGas(String stringLogin, String codiceContoLuce) {
        String result = "";
        JSONObject anagrContoIesimo = new JSONObject();
        JSONObject anagrExtraContoIesimo = new JSONObject();
        try {
            JSONArray arrayLogin = new JSONArray(stringLogin);
            for (int i = 2; i < arrayLogin.length(); i++) {
                JSONArray contoIesimo;
                contoIesimo = arrayLogin.getJSONArray(i);
                anagrContoIesimo = contoIesimo.getJSONObject(0);
                if (contoIesimo.length() < 2) {
                    continue;
                } else {
                    anagrExtraContoIesimo = contoIesimo.getJSONObject(1);
                    String codiceConto = anagrContoIesimo.getString("CodiceConto");
                    String tipologia = anagrExtraContoIesimo.getString("Tipologia");
                    if (!tipologia.equals("GAS")) {
                        continue;
                    } else {
                        if (!codiceConto.equals(codiceContoLuce)) {
                            continue;
                        } else {
                            // trovato il conto
                            result = anagrExtraContoIesimo.getString("TipoUtilizzoGas");
                            // result = result.replaceAll("\\+", "%2B");
                            // result = result.replaceAll(" ", "%20");
                            result = Uri.encode(result);
                        }
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public String getDataAttivazione(String stringLogin, String codiceContoLuce, String luceOgas) {
        String result = "";
        JSONObject anagrContoIesimo = new JSONObject();
        JSONObject anagrExtraContoIesimo = new JSONObject();
        try {
            JSONArray arrayLogin = new JSONArray(stringLogin);
            for (int i = 2; i < arrayLogin.length(); i++) {
                JSONArray contoIesimo;
                contoIesimo = arrayLogin.getJSONArray(i);
                anagrContoIesimo = contoIesimo.getJSONObject(0);
                if (contoIesimo.length() < 2) {
                    continue;
                } else {
                    anagrExtraContoIesimo = contoIesimo.getJSONObject(1);
                    String codiceConto = anagrContoIesimo.getString("CodiceConto");
                    if (!codiceConto.equals(codiceContoLuce)) {
                        continue;
                    } else {
                        // trovato il conto
                        String tipologia = anagrExtraContoIesimo.getString("Tipologia");
                        if (tipologia.equals(luceOgas)) {
                            result = anagrExtraContoIesimo.getString("DataAttivazione");
                        }
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public String getCodiceContoFromPdf(String stringLogin, String pdfConto) {
        String result = "";
        JSONObject anagrContoIesimo = new JSONObject();
        JSONObject anagrExtraContoIesimo = new JSONObject();
        try {
            JSONArray arrayLogin = new JSONArray(stringLogin);
            for (int i = 2; i < arrayLogin.length(); i++) {
                JSONArray contoIesimo;
                contoIesimo = arrayLogin.getJSONArray(i);
                anagrContoIesimo = contoIesimo.getJSONObject(0);
                if (contoIesimo.length() < 2) {
                    continue;
                } else {
                    String statoConto = anagrContoIesimo.getString("StatoConto");
                    anagrExtraContoIesimo = contoIesimo.getJSONObject(1);
                    String pdf = anagrExtraContoIesimo.getString("Pdf");
                    if (!pdf.equals(pdfConto) || "NON UTILIZZABILE".equals(statoConto)) {
                        continue;
                    } else {
                        // trovato il conto
                        result = anagrContoIesimo.getString("CodiceConto");
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public void postChiamata(String rispostaChiamata, List<Cookie> lista, int posizioneIcona, HashMap<String, String> parametri) {
        if (animation != null) {
            animation.stop();
            imageAnim.setVisibility(View.INVISIBLE);
        }
        if (dialogDisableClicks.isShowing()) {
            dialogDisableClicks.dismiss();
        }

        JSONArray rispostaJSON = new JSONArray();
        String esito = "";
        String descEsito = "";
        try {
            rispostaJSON = new JSONArray(rispostaChiamata);

            if (rispostaJSON.length() > 0) {
                JSONObject oggettoRisposta = (JSONObject) rispostaJSON.get(0);
                esito = oggettoRisposta.getString("esito");
                descEsito = oggettoRisposta.getString("descEsito");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (esito.equals("200")) {
            switch (posizioneIcona) {
                case Constants.FUNZIONALITA_STATO_AVANZAMENTO_FORNITURA:
                    Constants.SERVIZIO_STATO_AVANZAMENTO_FORNITURA = rispostaChiamata;
                    Intent intentStatoAttivazioneFornitura = new Intent(getApplicationContext(), StatoAvanzamentoFornitura.class);
                    intentStatoAttivazioneFornitura.putExtra("PRIMOCONTO", parametri.get("codiceContoCliente"));
                    Home.this.finish();
                    startActivity(intentStatoAttivazioneFornitura);
                    overridePendingTransition(R.anim.fade, R.anim.hold);
                    break;
                case Constants.FUNZIONALITA_DOMICILIAZIONE:
                    Constants.SERVIZIO_VISUALIZZA_FORNITURE_DOMICILIAZIONE = rispostaChiamata;
                    Intent intentVisualizzaFornitureDomiciliazione = new Intent(getApplicationContext(), Domiciliazione.class);
                    Home.this.finish();
                    startActivity(intentVisualizzaFornitureDomiciliazione);
                    overridePendingTransition(R.anim.fade, R.anim.hold);
                    break;
                case Constants.FUNZIONALITA_AUTOLETTURA:
                    Constants.SERVIZIO_FORNITURE_GAS_ATTIVE = rispostaChiamata;
                    Intent intentAutolettura = new Intent(getApplicationContext(), Autolettura.class);
                    Home.this.finish();
                    startActivity(intentAutolettura);
                    overridePendingTransition(R.anim.fade, R.anim.hold);
                    break;
                case Constants.FUNZIONALITA_STORICO_LETTURE:
                    Constants.SERVIZIO_FORNITURE_GAS_ATTIVE = rispostaChiamata;
                    Intent intentStoricoLetture = new Intent(getApplicationContext(), StoricoLetture.class);
                    Home.this.finish();
                    startActivity(intentStoricoLetture);
                    overridePendingTransition(R.anim.fade, R.anim.hold);
                    break;
                case Constants.FUNZIONALITA_BOLLETTE_PAGAMENTI:
                    Constants.SERVIZIO_SHOW_LISTA_FATTURE = rispostaChiamata;
                    Intent intentBollettePagamenti = new Intent(getApplicationContext(), BollettePagamenti.class);
                    intentBollettePagamenti.putExtra("PRIMOCONTO", parametri.get("codiceContoCliente"));
                    ArrayList<String> elencoContiBolPag = elencoContiBollettePagamenti();
                    for (int i = 0; i < elencoContiBolPag.size(); i++) {
                        if (elencoContiBolPag.get(i).equals(parametri.get("codiceContoCliente"))) {
                            intentBollettePagamenti.putExtra("INDICE_SPINNER", i);
                            break;
                        }
                    }
                    Home.this.finish();
                    startActivity(intentBollettePagamenti);
                    overridePendingTransition(R.anim.fade, R.anim.hold);
                    break;
                case Constants.FUNZIONALITA_ENI_WEB_BOLLETTA:
                    Constants.SERVIZIO_INFO_FATTURA_ONLINE = rispostaChiamata;
                    Intent intentEniWebBolletta = new Intent(getApplicationContext(), Webolletta.class);
                    intentEniWebBolletta.putExtra("PRIMOCONTO", parametri.get("codiceContoCliente"));
                    Home.this.finish();
                    startActivity(intentEniWebBolletta);
                    overridePendingTransition(R.anim.fade, R.anim.hold);
                    break;
                case Constants.FUNZIONALITA_CONSUMI_LUCE:
                    Constants.SERVIZIO_CONSUMI_LUCE = rispostaChiamata;
                    Intent intentConsumiLuce = new Intent(getApplicationContext(), ConsumiLuce.class);
                    String pdfConto = parametri.get("PARAMETER").substring(parametri.get("PARAMETER").indexOf("pdf=") + 4,
                            parametri.get("PARAMETER").indexOf("&regione="));
                    String primoConto = getCodiceContoFromPdf(Constants.SERVIZIO_LOGIN, pdfConto);
                    intentConsumiLuce.putExtra("PRIMOCONTO", primoConto);
                    ArrayList<String> elencoContiConsumiLuce = elencoContiConsumiLuce();
                    for (int i = 0; i < elencoContiConsumiLuce.size(); i++) {
                        if (elencoContiConsumiLuce.get(i).equals(primoConto)) {
                            intentConsumiLuce.putExtra("INDICE_SPINNER", i);
                            break;
                        }
                    }
                    intentConsumiLuce.putExtra("Classificazione", calcolaClassificazioneCliente(Constants.SERVIZIO_LOGIN));
                    Home.this.finish();
                    startActivity(intentConsumiLuce);
                    overridePendingTransition(R.anim.fade, R.anim.hold);
                    break;
                case Constants.FUNZIONALITA_CONSUMI_GAS:
                    Constants.SERVIZIO_CONSUMI_GAS = rispostaChiamata;
                    Intent intentConsumiGas = new Intent(getApplicationContext(), ConsumiGas.class);
                    String pdfContoGas = parametri.get("PARAMETER").substring(parametri.get("PARAMETER").indexOf("pdf=") + 4,
                            parametri.get("PARAMETER").indexOf("&regione="));
                    String primoContoGas = getCodiceContoFromPdf(Constants.SERVIZIO_LOGIN, pdfContoGas);
                    intentConsumiGas.putExtra("PRIMOCONTO", primoContoGas);
                    intentConsumiGas.putExtra("Classificazione", calcolaClassificazioneCliente(Constants.SERVIZIO_LOGIN));
                    Home.this.finish();
                    startActivity(intentConsumiGas);
                    overridePendingTransition(R.anim.fade, R.anim.hold);
                    break;

            }
        } else {
            Intent intentOops = null;
            switch (posizioneIcona) {
                case Constants.FUNZIONALITA_STATO_AVANZAMENTO_FORNITURA:
                    intentOops = new Intent(getApplicationContext(), OopsPageAttivazioneFornitura.class);
                    intentOops.putExtra("CODICE_CLIENTE", parametri.get("codiceCliente"));
                    intentOops.putExtra("CODICE_CONTO_CLIENTE", parametri.get("codiceContoCliente"));
                    break;
                case Constants.FUNZIONALITA_DOMICILIAZIONE:
                    intentOops = new Intent(getApplicationContext(), OopsPageDomiciliazione.class);
                    intentOops.putExtra("PARAMETER", parametri.get("PARAMETER"));
                    break;
                case Constants.FUNZIONALITA_AUTOLETTURA:
                    intentOops = new Intent(getApplicationContext(), OopsPageAutolettura.class);
                    intentOops.putExtra("PARAMETRI", parametri);
                    break;
                case Constants.FUNZIONALITA_STORICO_LETTURE:
                    intentOops = new Intent(getApplicationContext(), OopsPageStorico.class);
                    intentOops.putExtra("PARAMETER", parametri);
                    break;
                case Constants.FUNZIONALITA_BOLLETTE_PAGAMENTI:
                    if (!esito.equals("431")) {
                        intentOops = new Intent(getApplicationContext(), OopsPageBollettePagamenti.class);
                        intentOops.putExtra("CODICE_CLIENTE", parametri.get("codiceCliente"));
                        intentOops.putExtra("CODICE_CONTO_CLIENTE", parametri.get("codiceContoCliente"));
                    } else {
                        // Errore nel recupero dell'estratto conto: devo eliminare
                        // il primo conto della lista, rifare la chiamata per il
                        // prox. se ce'
                        if (contiBollettePagamenti.size() > 0) {
                            contiBollettePagamenti.remove(0);
                            // contiBollettePagamenti = (ArrayList<String>)
                            // contiBollettePagamenti.subList(1,
                            // contiBollettePagamenti.size());
                            if (contiBollettePagamenti.size() > 0) {
                                // chiamata
                                String codiceCliente = getCodiceCliente(Constants.SERVIZIO_LOGIN);
                                // String primoContoUtilizzabilePieno =
                                // getPrimoContoBollettePagamenti(loginService);
                                String primoContoUtilizzabilePieno = contiBollettePagamenti.get(0);

                                animation = Utilities.animation(Home.this);
                                animation.start();

                                if (!isNetworkAvailable(context)) {
                                    intentOops = new Intent(getApplicationContext(), OopsPageBollettePagamenti.class);
                                    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.connessione_assente));
                                    intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
                                    intentOops.putExtra("CODICE_CONTO_CLIENTE", primoContoUtilizzabilePieno);
                                    // Home.this.finish();
                                    startActivity(intentOops);
                                    if (animation != null) {
                                        animation.stop();
                                        imageAnim.setVisibility(View.INVISIBLE);
                                    }
                                    overridePendingTransition(R.anim.fade, R.anim.hold);
                                } else {
                                    /********************* Chiamata show Lista fatture ******************************/
                                    ServiceCall stAvFornServiceCall = ServiceCallFactory.createServiceCall(context);
                                    // stAvFornServiceCall.myCookieStore = new
                                    // PersistentCookieStore(context);

                                    HashMap<String, String> parametriNew = new HashMap<String, String>();
                                    parametriNew.put("codiceCliente", codiceCliente);
                                    parametriNew.put("codiceContoCliente", primoContoUtilizzabilePieno);

                                    try {
                                        stAvFornServiceCall.executeHttpsGet(Constants.URL_SHOW_LISTA_FATTURE, parametriNew, Home.this,
                                                Constants.FUNZIONALITA_BOLLETTE_PAGAMENTI, null);
                                    } catch (Exception e) {
                                        intentOops = new Intent(getApplicationContext(), OopsPageBollettePagamenti.class);
                                        intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
                                        intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
                                        intentOops.putExtra("CODICE_CONTO_CLIENTE", primoContoUtilizzabilePieno);
                                        startActivity(intentOops);
                                        overridePendingTransition(R.anim.fade, R.anim.hold);
                                    }
                                }
                                return;
                            }
                            intentOops = new Intent(getApplicationContext(), OopsPageBollettePagamenti.class);
                            intentOops.putExtra("CODICE_CLIENTE", parametri.get("codiceCliente"));
                            intentOops.putExtra("CODICE_CONTO_CLIENTE", parametri.get("codiceContoCliente"));
                            intentOops.putExtra("MESSAGGIO", descEsito);
                            startActivity(intentOops);
                            overridePendingTransition(R.anim.fade, R.anim.hold);
                            inizializzaContiBollettePagamenti(Constants.SERVIZIO_LOGIN);
                            return;
                        } else {
                            intentOops = new Intent(getApplicationContext(), OopsPageBollettePagamenti.class);
                            intentOops.putExtra("CODICE_CLIENTE", parametri.get("codiceCliente"));
                            intentOops.putExtra("CODICE_CONTO_CLIENTE", parametri.get("codiceContoCliente"));
                            intentOops.putExtra("MESSAGGIO", descEsito);
                            startActivity(intentOops);
                            overridePendingTransition(R.anim.fade, R.anim.hold);
                            inizializzaContiBollettePagamenti(Constants.SERVIZIO_LOGIN);
                            return;
                        }
                    }

                    break;
                case Constants.FUNZIONALITA_ENI_WEB_BOLLETTA:
                    intentOops = new Intent(getApplicationContext(), OopsPageEniWebBolletta.class);
                    intentOops.putExtra("CODICE_CLIENTE", parametri.get("codiceCliente"));
                    break;
                case Constants.FUNZIONALITA_CONSUMI_LUCE:
                    if (!esito.equals("455")) {
                        intentOops = new Intent(getApplicationContext(), OopsPageConsumiLuce.class);
                        intentOops.putExtra("PARAMETER", parametri.get("PARAMETER"));
                    } else {
                        // Errore nel recupero dell'estratto conto: devo eliminare
                        // il primo conto della lista, rifare la chiamata per il
                        // prox. se ce'
                        if (contiConsumiLuce.size() > 0) {
                            contiConsumiLuce.remove(0);
                            // contiBollettePagamenti = (ArrayList<String>)
                            // contiBollettePagamenti.subList(1,
                            // contiBollettePagamenti.size());
                            if (contiConsumiLuce.size() > 0) {
                                // chiamata
                                String codiceCliente = getCodiceCliente(Constants.SERVIZIO_LOGIN);
                                // String primoContoUtilizzabilePieno =
                                // getPrimoContoBollettePagamenti(loginService);
                                String primoContoUtilizzabilePienoLuce = contiConsumiLuce.get(0);

                                String parametriNew = "";
                                parametriNew = parametriNew + "&codiceCliente=" + codiceCliente;
                                String segmento = getSegmentoCliente(Constants.SERVIZIO_LOGIN);
                                parametriNew = parametriNew + "&segmento=" + segmento;
                                String tipologiaPdfRegioneProdottoDataattivazione = getTipologiaPdfRegioneProdottoDataAttivazione(Constants.SERVIZIO_LOGIN,
                                        primoContoUtilizzabilePienoLuce, segmento);
                                parametriNew = parametriNew + tipologiaPdfRegioneProdottoDataattivazione;

                                animation = Utilities.animation(Home.this);
                                imageAnim = (ImageView) findViewById(R.id.waiting_anim);
                                imageAnim.setVisibility(View.VISIBLE);
                                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                                    imageAnim.setBackgroundDrawable(animation);
                                } else {
                                    imageAnim.setBackground(animation);
                                }
                                animation.start();

                                if (!isNetworkAvailable(context)) {
                                    intentOops = new Intent(getApplicationContext(), OopsPageConsumiLuce.class);
                                    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.connessione_assente));
                                    intentOops.putExtra("PARAMETER", parametriNew);
                                    // Home.this.finish();
                                    startActivity(intentOops);
                                    if (animation != null) {
                                        animation.stop();
                                        imageAnim.setVisibility(View.INVISIBLE);
                                    }
                                    overridePendingTransition(R.anim.fade, R.anim.hold);
                                } else {
                                    /********************* Chiamata consumi luce ******************************/
                                    ServiceCall stAvFornServiceCall = ServiceCallFactory.createServiceCall(context);
                                    // stAvFornServiceCall.myCookieStore = new
                                    // PersistentCookieStore(context);
                                    try {
                                        stAvFornServiceCall.executeHttpsGet(Constants.URL_CONSUMI_LUCE, parametriNew, Home.this,
                                                Constants.FUNZIONALITA_CONSUMI_LUCE, null);
                                    } catch (Exception e) {
                                        intentOops = new Intent(getApplicationContext(), OopsPageConsumiLuce.class);
                                        intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
                                        intentOops.putExtra("PARAMETER", parametriNew);
                                        startActivity(intentOops);
                                        overridePendingTransition(R.anim.fade, R.anim.hold);
                                    }
                                }
                                return;
                            }
                            intentOops = new Intent(getApplicationContext(), OopsPageConsumiLuce.class);
                            intentOops.putExtra("PARAMETER", parametri.get("PARAMETER"));
                            intentOops.putExtra("MESSAGGIO", descEsito);
                            startActivity(intentOops);
                            overridePendingTransition(R.anim.fade, R.anim.hold);
                            inizializzaContiConsumiLuce(Constants.SERVIZIO_LOGIN);
                            return;
                        } else {
                            intentOops = new Intent(getApplicationContext(), OopsPageConsumiLuce.class);
                            intentOops.putExtra("PARAMETER", parametri.get("PARAMETER"));
                            intentOops.putExtra("MESSAGGIO", descEsito);
                            startActivity(intentOops);
                            overridePendingTransition(R.anim.fade, R.anim.hold);
                            inizializzaContiConsumiLuce(Constants.SERVIZIO_LOGIN);
                            return;
                        }
                    }
                    break;
                case Constants.FUNZIONALITA_CONSUMI_GAS:
                    intentOops = new Intent(getApplicationContext(), OopsPageConsumiGas.class);
                    intentOops.putExtra("PARAMETER", parametri.get("PARAMETER"));
                    break;
            }

            // errori ops page
            if (esito.equals("309")) {
                intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
                startActivity(intentOops);
                overridePendingTransition(R.anim.fade, R.anim.hold);

            } else if (esito.equals("400")) {
                intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
                startActivity(intentOops);
                overridePendingTransition(R.anim.fade, R.anim.hold);

            } else if (esito.equals("420")) {
                intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
                startActivity(intentOops);
                overridePendingTransition(R.anim.fade, R.anim.hold);

            } else if (esito.equals("449")) {
                intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
                startActivity(intentOops);
                overridePendingTransition(R.anim.fade, R.anim.hold);

            } else if (esito.equals("451")) {
                intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_popup_451));
                startActivity(intentOops);
                overridePendingTransition(R.anim.fade, R.anim.hold);

            }
            // errori popup
            else if (esito.equals("307")) {
                Utils.popup(Home.this, getResources().getString(R.string.errore_popup_307), "Riprova");

            } else if (esito.equals("435")) {
                Utils.popup(Home.this, getResources().getString(R.string.errore_popup_435), "Riprova");

            } else if (esito.equals("437")) {
                Utils.popup(Home.this, getResources().getString(R.string.errore_popup_437), "Riprova");

            } else if (esito.equals("438")) {
                Utils.popup(Home.this, getResources().getString(R.string.errore_popup_438), "Riprova");

            } else if (esito.equals("443")) {
                Utils.popup(Home.this, getResources().getString(R.string.errore_popup_443), "Riprova");

            } else if (esito.equals("444")) {
                Utils.popup(Home.this, getResources().getString(R.string.errore_popup_444), "Riprova");

            } else if (esito.equals("445")) {
                Utils.popup(Home.this, getResources().getString(R.string.errore_popup_445), "Riprova");

            } else if (esito.equals("446")) {
                Utils.popup(Home.this, getResources().getString(R.string.errore_popup_446), "Riprova");

            } else if (esito.equals("447")) {
                Utils.popup(Home.this, getResources().getString(R.string.errore_popup_447), "Riprova");

            } else {
                if (descEsito.equals("")) {
                    intentOops = new Intent(getApplicationContext(), OopsPageSessioneScaduta.class);
                    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.sessione_scaduta));
                } else {
                    intentOops.putExtra("MESSAGGIO", descEsito);
                }

                startActivity(intentOops);
                overridePendingTransition(R.anim.fade, R.anim.hold);
            }

        }

    }

    public void postChiamataException(boolean exception, int numeroPosizione, String parametri, HashMap<String, String> parametriHash) {
        if (exception) {
            if (animation != null) {
                animation.stop();
                imageAnim.setVisibility(View.INVISIBLE);
            }
            if (dialogDisableClicks.isShowing()) {
                dialogDisableClicks.dismiss();
            }
            Intent intentOops = null;

            switch (numeroPosizione) {
                case Constants.FUNZIONALITA_STATO_AVANZAMENTO_FORNITURA:
                    intentOops = new Intent(getApplicationContext(), OopsPageAttivazioneFornitura.class);
                    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
                    intentOops.putExtra("CODICE_CLIENTE", parametriHash.get("codiceCliente"));
                    intentOops.putExtra("CODICE_CONTO_CLIENTE", parametriHash.get("codiceContoCliente"));
                    break;
                case Constants.FUNZIONALITA_DOMICILIAZIONE:
                    intentOops = new Intent(getApplicationContext(), OopsPageDomiciliazione.class);
                    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
                    intentOops.putExtra("PARAMETER", parametri);
                    break;
                case Constants.FUNZIONALITA_AUTOLETTURA:
                    intentOops = new Intent(getApplicationContext(), OopsPageAutolettura.class);
                    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
                    intentOops.putExtra("PARAMETRI", parametri);
                    break;
                case Constants.FUNZIONALITA_STORICO_LETTURE:
                    intentOops = new Intent(getApplicationContext(), OopsPageStorico.class);
                    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
                    intentOops.putExtra("PARAMETER", parametri);
                    break;
                case Constants.FUNZIONALITA_BOLLETTE_PAGAMENTI:
                    intentOops = new Intent(getApplicationContext(), OopsPageBollettePagamenti.class);
                    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
                    intentOops.putExtra("CODICE_CLIENTE", parametriHash.get("codiceCliente"));
                    intentOops.putExtra("CODICE_CONTO_CLIENTE", parametriHash.get("codiceContoCliente"));
                    break;
                case Constants.FUNZIONALITA_ENI_WEB_BOLLETTA:
                    intentOops = new Intent(getApplicationContext(), OopsPageEniWebBolletta.class);
                    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
                    intentOops.putExtra("CODICE_CLIENTE", parametriHash.get("codiceCliente"));
                    break;
                case Constants.FUNZIONALITA_CONSUMI_LUCE:
                    intentOops = new Intent(getApplicationContext(), OopsPageConsumiLuce.class);
                    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
                    intentOops.putExtra("PARAMETER", parametri);
                    break;
                case Constants.FUNZIONALITA_CONSUMI_GAS:
                    intentOops = new Intent(getApplicationContext(), OopsPageConsumiGas.class);
                    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
                    intentOops.putExtra("PARAMETER", parametri);
                    break;
            }
            startActivity(intentOops);
            overridePendingTransition(R.anim.fade, R.anim.hold);
        }
    }

    private String calcolaClassificazioneCliente(String stringLogin) {
        String result = "";
        try {
            JSONArray arrayLogin = new JSONArray(stringLogin);
            JSONObject anagrCliente = arrayLogin.getJSONObject(1);

            String tipologiaCliente = anagrCliente.getString("TipologiaCliente");
            String segmento = anagrCliente.getString("SegmentoCliente");
            if (tipologiaCliente.equals("RETAIL") && segmento.equals("RESIDENTIAL")) {
                result = "RETAIL";
            } else {
                result = "PMI";
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    private boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager == null)
            return false;
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }

    public void onBackPressed() {

        animation = Utilities.animation(Home.this);
        animation.start();

        Intent intentLogin = new Intent(getApplicationContext(), Login.class);
        Home.this.finish();
        startActivity(intentLogin);
        overridePendingTransition(R.anim.fade, R.anim.hold);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // if(resultCode == OopsPage.ESCI){
        // Home.this.finish();
        // }
    }
}
