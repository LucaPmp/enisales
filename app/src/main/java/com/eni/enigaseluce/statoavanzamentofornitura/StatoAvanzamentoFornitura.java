package com.eni.enigaseluce.statoavanzamentofornitura;

import java.util.HashMap;
import java.util.List;

import org.apache.http.cookie.Cookie;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.AnimationDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.eni.enigaseluce.EniApplication;
import com.eni.enigaseluce.R;
import com.eni.enigaseluce.error.OopsPageAttivazioneFornituraFromSaf;
import com.eni.enigaseluce.error.OopsPageSessioneScaduta;
import com.eni.enigaseluce.error.Utils;
import com.eni.enigaseluce.home.Home;
import com.eni.enigaseluce.httpcall.Constants;
import com.eni.enigaseluce.httpcall.ServiceCall;
import com.eni.enigaseluce.httpcall.ServiceCallFactory;
import com.eni.enigaseluce.utils.Utilities;

@SuppressLint("NewApi")
public class StatoAvanzamentoFornitura extends Activity {
	private Context context;
	private static Context staticContext;
	AnimationDrawable animation;
	ImageView imageAnim;
	static int positionSpinnerCurrentSelected;
	static int positionSpinnerPrecedentSelected;
	static RelativeLayout pager;
	static boolean chiamata;

	public String rispostaLogin = Constants.SERVIZIO_LOGIN;
	public String rispostaStatoAvanzamentoForniture = Constants.SERVIZIO_STATO_AVANZAMENTO_FORNITURA;

	public static String nomeInteroCliente = "";
	static LayoutInflater inflater;
	private String array_conti_spinner[];
	String codiceEsito;
	ArrayAdapter<String> adapterSpinnerConti;
	String primoConto;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		context = this;
		StatoAvanzamentoFornitura.staticContext = this;
		super.onCreate(savedInstanceState);
		setContentView(R.layout.stato_avanzamento_fornitura);

		((EniApplication)getApplication()).sendAnalyticsEvent("[Android] - attivazione fornitura");

		Intent intent = getIntent();
		primoConto = intent.getStringExtra("PRIMOCONTO");
		chiamata = getIntent().getBooleanExtra("CHIAMATA", true);

		positionSpinnerCurrentSelected = getIntent().getIntExtra("INDICE_SPINNER_ST", 0);
		positionSpinnerPrecedentSelected = getIntent().getIntExtra("INDICE_SPINNER_ST", 0);

		final ImageView statoAttivazioneIndietro = (ImageView) findViewById(R.id.stato_attivazione_fornitura_indietro);
		statoAttivazioneIndietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
		statoAttivazioneIndietro.getBackground().invalidateSelf();
		statoAttivazioneIndietro.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v1)
			{
				statoAttivazioneIndietro.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
				statoAttivazioneIndietro.getBackground().invalidateSelf();
				animation = Utilities.animation(StatoAvanzamentoFornitura.this);
				imageAnim = (ImageView) findViewById(R.id.waiting_anim);
				imageAnim.setVisibility(View.VISIBLE);
				if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN)
				{
					imageAnim.setBackgroundDrawable(animation);
				} else
				{
					imageAnim.setBackground(animation);
				}
				animation.start();
				Intent intentHome = new Intent(getApplicationContext(), Home.class);
				StatoAvanzamentoFornitura.this.finish();
				startActivity(intentHome);
				overridePendingTransition(R.anim.fade, R.anim.hold);
			}
		});
		final ImageView stAvFornHelp = (ImageView) findViewById(R.id.stato_attivazione_fornitura_help);
		stAvFornHelp.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
		stAvFornHelp.getBackground().invalidateSelf();
		stAvFornHelp.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v1)
			{
				stAvFornHelp.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
				stAvFornHelp.getBackground().invalidateSelf();
				animation = Utilities.animation(StatoAvanzamentoFornitura.this);
				imageAnim = (ImageView) findViewById(R.id.waiting_anim);
				imageAnim.setVisibility(View.VISIBLE);
				if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN)
				{
					imageAnim.setBackgroundDrawable(animation);
				} else
				{
					imageAnim.setBackground(animation);
				}
				animation.start();
				Intent intentStHelp = new Intent(getApplicationContext(), StatoAvanzamentoFronituraHelp.class);
				intentStHelp.putExtra("INDICE_SPINNER_ST", positionSpinnerCurrentSelected);
				intentStHelp.putExtra("CHIAMATA", chiamata);
				StatoAvanzamentoFornitura.this.finish();
				startActivity(intentStHelp);
				overridePendingTransition(R.anim.fade, R.anim.hold);
			}
		});

		try
		{
			if (!rispostaStatoAvanzamentoForniture.equals(""))
			{
				JSONArray statoAvanzamentoFornJSON = new JSONArray(rispostaStatoAvanzamentoForniture);
				if (statoAvanzamentoFornJSON.length() == 1 && chiamata)
				{
					AlertDialog.Builder popup = new AlertDialog.Builder(context);
					popup.setMessage(R.string.nessuna_fornitura_attiva).setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id)
						{
						}
					});
					popup.create().show();
				}
			}
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
	}

	public static Context getContext()
	{
		return StatoAvanzamentoFornitura.staticContext;
	}

	@Override
	protected void onStart()
	{
		super.onStart();
		try
		{
			final JSONArray stAvFornLogin = new JSONArray(rispostaLogin);
			int lunghezzaStAvForn = numeroCodiciContoDiversi(stAvFornLogin);
			JSONObject esito = stAvFornLogin.getJSONObject(0);
			codiceEsito = (String) esito.getString("esito");
			if (codiceEsito.equals("200"))
			{
				JSONObject datiAnagr = stAvFornLogin.getJSONObject(1);

				// ultimo accesso
				String ultimoaccesso = datiAnagr.getString("UltimoAccesso");
				TextView ultimoAccesso = (TextView) findViewById(R.id.ultimo_accesso);
				// ultimoAccesso.setTextSize(11);
				ultimoAccesso.setText(ultimoaccesso);

				// nome cliente
				String nomeClienteString = datiAnagr.getString("NomeCliente");
				String cognomeClienteString = datiAnagr.getString("CognomeCliente");
				TextView nomeCliente = (TextView) findViewById(R.id.nome_cliente);
				nomeInteroCliente = nomeClienteString + " " + cognomeClienteString;
				nomeCliente.setText(nomeClienteString + " " + cognomeClienteString);
				array_conti_spinner = new String[lunghezzaStAvForn];
				array_conti_spinner = arrayCodiciContoDiversi(stAvFornLogin, lunghezzaStAvForn);
				final Spinner codiciConti = (Spinner) findViewById(R.id.numero_cliente);
				// final ArrayAdapter<String> adapterSpinnerConti = new
				// ArrayAdapter<String>(this,
				// android.R.layout.simple_spinner_item, array_conti_spinner);
				final ArrayAdapter<String> adapterSpinnerConti = new ArrayAdapter<String>(this, R.layout.spinner_dialog_riga, R.id.spinner_riga_text,
						array_conti_spinner);
				codiciConti.setAdapter(adapterSpinnerConti);
				codiciConti.setPrompt(getResources().getString(R.string.seleziona_il_conto_cliente));
				codiciConti.setSelection(positionSpinnerCurrentSelected);

				// final LinearLayout scroller = (LinearLayout)
				// findViewById(R.id.stato_attivazione_fornitura_scroller);
				// final HorizontalPager pager = (HorizontalPager)
				// findViewById(R.id.stato_attivazione_fornitura_pager);
				pager = (RelativeLayout) findViewById(R.id.stato_attivazione_fornitura_pager);

				inflater = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);
				if (!rispostaStatoAvanzamentoForniture.equals(""))
				{
					JSONArray statoAvanzamentoFornJSON = new JSONArray(rispostaStatoAvanzamentoForniture);
					if (statoAvanzamentoFornJSON.length() == 1)
					{
					}
					// controllo se ce ne sono 2 con indirizzo uguale (caso
					// Dual)
					else
					{
						JSONArray oggettiFornitura = (JSONArray) statoAvanzamentoFornJSON.get(1);
						if (oggettiFornitura.length() > 1)
						{
							if (oggettiFornitura.length() == 2)
							{
								// se ce ne sono due allora per forza � caso
								// DUAL (l'ha detto Baraldo)
								JSONObject fornituraPrima = (JSONObject) oggettiFornitura.get(0);
								JSONObject fornituraSeconda = (JSONObject) oggettiFornitura.get(1);
								popolaFornituraDual(pager, fornituraPrima, fornituraSeconda);
							}
						} else
						{
							JSONObject fornitura = (JSONObject) oggettiFornitura.get(0);
							popolaSingolaFornitura(pager, fornitura);
						}
					}
				}

				/*
				 * int numeroPagine = pager.getChildCount();
				 * scroller.removeAllViews(); for(int i = 0; i < numeroPagine;
				 * i++){ ImageView iv = new ImageView(this); iv.setId(i);
				 * LinearLayout.LayoutParams param = new
				 * LinearLayout.LayoutParams
				 * (LinearLayout.LayoutParams.WRAP_CONTENT,
				 * LinearLayout.LayoutParams.WRAP_CONTENT); param.setMargins(10,
				 * 0, 0, 0); iv.setImageResource(R.drawable.dot_not_selected);
				 * iv.setLayoutParams(param); scroller.addView(iv);
				 * 
				 * }
				 */

				codiciConti.setOnItemSelectedListener(new OnItemSelectedListener() {
					public void onItemSelected(AdapterView<?> parent, View view, int pos, long id)
					{
						Object obj = (Object) parent.getSelectedItem();
						// Object obj = parent.getItemAtPosition(pos);
						String codiceContoCliente = obj.toString().trim();
						// controllo se il conto � utilizzalibe o meno
						boolean contoUtilizzabile = isContoUtilizzabile(codiceContoCliente, stAvFornLogin);
						if (contoUtilizzabile)
						{
							if (!codiceContoCliente.equals(adapterSpinnerConti.getItem(positionSpinnerCurrentSelected).toString().trim()))
							{
								positionSpinnerPrecedentSelected = positionSpinnerCurrentSelected;
								positionSpinnerCurrentSelected = codiciConti.getLastVisiblePosition();
								animation = Utilities.animation(StatoAvanzamentoFornitura.this);
								imageAnim = (ImageView) findViewById(R.id.waiting_anim);
								imageAnim.setVisibility(View.VISIBLE);
								if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN)
								{
									imageAnim.setBackgroundDrawable(animation);
								} else
								{
									imageAnim.setBackground(animation);
								}
								animation.start();
								String codiceCliente = getCodiceCliente(rispostaLogin);
								if (!isNetworkAvailable(context))
								{
									Intent intentOops = new Intent(getApplicationContext(), OopsPageAttivazioneFornituraFromSaf.class);
									intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.connessione_assente));
									intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
									intentOops.putExtra("CODICE_CONTO_CLIENTE", codiceContoCliente);
									positionSpinnerCurrentSelected = positionSpinnerPrecedentSelected;
									// StatoAvanzamentoFornitura.this.finish();
									startActivity(intentOops);
									if (animation != null)
									{
										animation.stop();
										imageAnim.setVisibility(View.INVISIBLE);
									}
									overridePendingTransition(R.anim.fade, R.anim.hold);
								} else
								{
									/********************* Chiamata statoAvanzamento Fornitura ******************************/
									ServiceCall stAvFornServiceCall = ServiceCallFactory.createServiceCall(context);
									// stAvFornServiceCall.myCookieStore = new
									// PersistentCookieStore(context);

									HashMap<String, String> parametri = new HashMap<String, String>();
									parametri.put("codiceCliente", codiceCliente);
									parametri.put("codiceContoCliente", codiceContoCliente);

									try
									{
										stAvFornServiceCall.executeHttpsGetAvanzamentoForniture(Constants.URL_STATO_AVANZAMENTO_FORNITURE, parametri,
												StatoAvanzamentoFornitura.this, null);
									} catch (Exception e)
									{
										Intent intentOops = new Intent(getApplicationContext(), OopsPageAttivazioneFornituraFromSaf.class);
										intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
										intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
										intentOops.putExtra("CODICE_CONTO_CLIENTE", codiceContoCliente);
										positionSpinnerCurrentSelected = positionSpinnerPrecedentSelected;
										// StatoAttivazioneFornitura.this.finish();
										startActivity(intentOops);
										overridePendingTransition(R.anim.fade, R.anim.hold);
									}
									// sorpassando lo statoAvanzamentoForniture
									/*
									 * String rispostaChiamata =
									 * "[{\"descEsito\":\"Chiamata effettuata correttamente\","
									 * + "\"esito\":200}," +
									 * 
									 * "[{\"DataAttivazione\":\"01/01/2011\"," +
									 * "\"FornituraRichiesta\":\"POWER\"," +
									 * "\"TipologiaStato\":\"ATTIVA\"," +
									 * "\"Pod/pdr\":\"Erinda\"," +
									 * "\"IndirizzoFornitura\":\"CONTRADA QUARTONALI 6 RUOTI 85056 \","
									 * + "\"Pdf\":\"BOH\"}" +
									 * 
									 * ",{\"DataAttivazione\":\"01/01/2011\"," +
									 * "\"FornituraRichiesta\":\"GAS\"," +
									 * "\"TipologiaStato\":\"ATTIVA\"," +
									 * "\"Pod/pdr\":\"Erinda2\"," +
									 * "\"IndirizzoFornitura\":\"CONTRADA QUARTONALI 6 RUOTI 85056 \","
									 * + "\"Pdf\":\"BOH\"}" + "]] ";
									 * postChiamata(rispostaChiamata, null);
									 */
								}
							}
						} else
						{
							positionSpinnerCurrentSelected = codiciConti.getLastVisiblePosition();
							pager.removeAllViews();
							chiamata = false;
							popolaFornituraNonUtilizzabile(pager, codiceContoCliente, stAvFornLogin);
						}
					}

					public void onNothingSelected(AdapterView<?> arg0)
					{
					}
				});

			} else
			{
				Intent intentOops = new Intent(getApplicationContext(), OopsPageAttivazioneFornituraFromSaf.class);
				intentOops.putExtra("CODICE_CLIENTE", getCodiceCliente(rispostaLogin));
				intentOops.putExtra("CODICE_CONTO_CLIENTE", primoConto);
				positionSpinnerCurrentSelected = positionSpinnerPrecedentSelected;
				// errori ops page
				if (esito.equals("309"))
				{
					intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
					startActivity(intentOops);
					overridePendingTransition(R.anim.fade, R.anim.hold);
				} else if (esito.equals("400"))
				{
					intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
					startActivity(intentOops);
					overridePendingTransition(R.anim.fade, R.anim.hold);
				} else if (esito.equals("420"))
				{
					intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
					startActivity(intentOops);
					overridePendingTransition(R.anim.fade, R.anim.hold);
				} else if (esito.equals("449"))
				{
					intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
					startActivity(intentOops);
					overridePendingTransition(R.anim.fade, R.anim.hold);
				} else if (esito.equals("451"))
				{
					intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_popup_451));
					startActivity(intentOops);
					overridePendingTransition(R.anim.fade, R.anim.hold);
				}
				// errori popup
				else if (esito.equals("307"))
				{
					Utils.popup(StatoAvanzamentoFornitura.this, getResources().getString(R.string.errore_popup_307), "Riprova");
				} else if (esito.equals("435"))
				{
					Utils.popup(StatoAvanzamentoFornitura.this, getResources().getString(R.string.errore_popup_435), "Riprova");
				} else if (esito.equals("437"))
				{
					Utils.popup(StatoAvanzamentoFornitura.this, getResources().getString(R.string.errore_popup_437), "Riprova");
				} else if (esito.equals("438"))
				{
					Utils.popup(StatoAvanzamentoFornitura.this, getResources().getString(R.string.errore_popup_438), "Riprova");
				} else if (esito.equals("443"))
				{
					Utils.popup(StatoAvanzamentoFornitura.this, getResources().getString(R.string.errore_popup_443), "Riprova");
				} else if (esito.equals("444"))
				{
					Utils.popup(StatoAvanzamentoFornitura.this, getResources().getString(R.string.errore_popup_444), "Riprova");
				} else if (esito.equals("445"))
				{
					Utils.popup(StatoAvanzamentoFornitura.this, getResources().getString(R.string.errore_popup_445), "Riprova");
				} else if (esito.equals("446"))
				{
					Utils.popup(StatoAvanzamentoFornitura.this, getResources().getString(R.string.errore_popup_446), "Riprova");
				} else if (esito.equals("447"))
				{
					Utils.popup(StatoAvanzamentoFornitura.this, getResources().getString(R.string.errore_popup_447), "Riprova");
				}
			}
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
	}

	public int numeroCodiciContoDiversi(JSONArray array)
	{
		int result = 0;
		String codiceContoConfrontato = "";
		String codiceContoIesimo = "";
		for (int i = 2; i < array.length(); i++)
		{
			JSONArray contoIesimo;
			try
			{
				contoIesimo = array.getJSONArray(i);
				// codice conto
				JSONObject anagrContoIesimo = contoIesimo.getJSONObject(0);
				codiceContoIesimo = anagrContoIesimo.getString("CodiceConto");
			} catch (JSONException e)
			{
				e.printStackTrace();
			}
			if (!codiceContoIesimo.equals(codiceContoConfrontato))
			{
				// codice conto diverso
				codiceContoConfrontato = codiceContoIesimo;
				result++;
			}
		}
		return result;
	}

	public String[] arrayCodiciContoDiversi(JSONArray array, int lunghezza)
	{
		String result[] = new String[lunghezza];
		String codiceContoConfrontato = "";
		String codiceContoIesimo = "";
		int count = 0;
		for (int i = 2; i < array.length(); i++)
		{
			JSONArray contoIesimo;
			try
			{
				contoIesimo = array.getJSONArray(i);
				// codice conto
				JSONObject anagrContoIesimo = contoIesimo.getJSONObject(0);
				codiceContoIesimo = anagrContoIesimo.getString("CodiceConto");
			} catch (JSONException e)
			{
				e.printStackTrace();
			}
			if (!codiceContoIesimo.equals(codiceContoConfrontato))
			{
				// codice conto diverso
				codiceContoConfrontato = codiceContoIesimo;
				result[count] = "  " + codiceContoConfrontato;
				count++;
			}
		}
		return result;
	}

	public boolean isContoUtilizzabile(String conto, JSONArray array)
	{
		boolean result = false;
		String codiceContoIesimo = "";
		JSONObject anagrContoIesimo = new JSONObject();
		for (int i = 2; i < array.length(); i++)
		{
			JSONArray contoIesimo;
			try
			{
				contoIesimo = array.getJSONArray(i);
				// codice conto
				anagrContoIesimo = contoIesimo.getJSONObject(0);
				codiceContoIesimo = anagrContoIesimo.getString("CodiceConto");
				if (codiceContoIesimo.equals(conto))
				{
					String statoCodiceContoIesimo = anagrContoIesimo.getString("StatoConto");
					if (statoCodiceContoIesimo.equals("UTILIZZABILE"))
					{
						return true;
					} else
					{
						result = false;
					}
				}
			} catch (JSONException e)
			{
				e.printStackTrace();
			}
		}
		return result;
	}

	public void popolaSingolaFornitura(RelativeLayout pager, JSONObject fornitura)
	{
		try
		{
			RelativeLayout stato_avanzamento_fornitura_page = new RelativeLayout(this);
			stato_avanzamento_fornitura_page = new RelativeLayout(this);
			View stato_avanzamento_forniture_entry = inflater.inflate(R.layout.stato_avanzamento_fornitura_entry_singola, null);

			String fornituraRichiesta = fornitura.getString("FornituraRichiesta");
			TextView tipoFornitura = (TextView) stato_avanzamento_forniture_entry.findViewById(R.id.fornitura_iesima_richiesta);
			ImageView iconaLuce = (ImageView) stato_avanzamento_forniture_entry.findViewById(R.id.stato_attivazione_fornitura_iesima_luce_icona);
			ImageView iconaGas = (ImageView) stato_avanzamento_forniture_entry.findViewById(R.id.stato_attivazione_fornitura_iesima_gas_icona);
			TextView labelPodPdr = (TextView) stato_avanzamento_forniture_entry.findViewById(R.id.pod_iesima_label);
			if (fornituraRichiesta.equals("POWER"))
			{
				tipoFornitura.setText(R.string.luce);
				iconaLuce.setImageResource(R.drawable.light_yes);
				iconaGas.setImageResource(R.drawable.gas_no);
				labelPodPdr.setText(R.string.punto_consegna_pod);
			} else
			{
				tipoFornitura.setText(R.string.gas);
				iconaLuce.setImageResource(R.drawable.light_no);
				iconaGas.setImageResource(R.drawable.gas_yes);
				labelPodPdr.setText(R.string.punto_consegna_pdr);
			}
			TextView statoFornitura = (TextView) stato_avanzamento_forniture_entry.findViewById(R.id.stato_fornitura_iesima);
			ImageView iconaStato = (ImageView) stato_avanzamento_forniture_entry.findViewById(R.id.stato_attivazione_fornitura_iesima_lucina);
			String tipologiaStato = fornitura.getString("TipologiaStato");
			if (tipologiaStato.equals("ATTIVA"))
			{
				statoFornitura.setText(R.string.fornitura_attiva_numero);
				iconaStato.setImageResource(R.drawable.green_dot);
				TextView labelfornituraDal = (TextView) stato_avanzamento_forniture_entry.findViewById(R.id.fornitura_iesima_dal_label);
				labelfornituraDal.setText(R.string.fornitura_attiva_dal);
				TextView fornituraDal = (TextView) stato_avanzamento_forniture_entry.findViewById(R.id.fornitura_iesima_dal);
				fornituraDal.setText(fornitura.getString("DataAttivazione"));
				TextView attivazionePrevista = (TextView) stato_avanzamento_forniture_entry.findViewById(R.id.attivazione_prevista_il_label);
				attivazionePrevista.setVisibility(View.INVISIBLE);
				TextView attivazionePrevistaData = (TextView) stato_avanzamento_forniture_entry.findViewById(R.id.attivazione_prevista_il);
				attivazionePrevistaData.setVisibility(View.INVISIBLE);
			} else
			{
				statoFornitura.setText(R.string.fornitura_in_attivazione);
				iconaStato.setImageResource(R.drawable.yellow_dot);
				TextView labelfornituraDal = (TextView) stato_avanzamento_forniture_entry.findViewById(R.id.fornitura_iesima_dal_label);
				labelfornituraDal.setText(R.string.fornitura_accettata_il);
				TextView fornituraDal = (TextView) stato_avanzamento_forniture_entry.findViewById(R.id.fornitura_iesima_dal);
				fornituraDal.setText(fornitura.getString("DataAccettazione"));
				TextView attivazionePrevista = (TextView) stato_avanzamento_forniture_entry.findViewById(R.id.attivazione_prevista_il_label);
				attivazionePrevista.setVisibility(View.VISIBLE);
				TextView attivazionePrevistaData = (TextView) stato_avanzamento_forniture_entry.findViewById(R.id.attivazione_prevista_il);
				attivazionePrevistaData.setVisibility(View.VISIBLE);
				attivazionePrevistaData.setText(fornitura.getString("DataPrevistaAttivazione"));
			}
			TextView numeroFornitura = (TextView) stato_avanzamento_forniture_entry.findViewById(R.id.numero_fornitura_iesima);
			numeroFornitura.setText(fornitura.getString("Pdf"));
			TextView intestatario = (TextView) stato_avanzamento_forniture_entry.findViewById(R.id.intestato_a);
			intestatario.setText(nomeInteroCliente);
			TextView podPdr = (TextView) stato_avanzamento_forniture_entry.findViewById(R.id.pod_iesima);
			podPdr.setText(fornitura.getString("Pod/pdr"));
			TextView indirizzo1Riga = (TextView) stato_avanzamento_forniture_entry.findViewById(R.id.indirizzo_fornitura_iesima_riga_1);
			String indirizzoFornitura = fornitura.getString("IndirizzoFornitura");
			indirizzo1Riga.setText(indirizzoFornitura);

			stato_avanzamento_fornitura_page.addView(stato_avanzamento_forniture_entry);
			pager.addView(stato_avanzamento_fornitura_page);
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
	}

	public void popolaFornituraDual(RelativeLayout pager, JSONObject fornituraPrima, JSONObject fornituraSeconda)
	{
		try
		{
			RelativeLayout stato_avanzamento_fornitura_page = new RelativeLayout(this);
			stato_avanzamento_fornitura_page = new RelativeLayout(this);
			View stato_avanzamento_forniture_entry = inflater.inflate(R.layout.stato_avanzamento_fornitura_entry_doppia, null);

			String fornituraRichiesta = fornituraPrima.getString("FornituraRichiesta");
			TextView tipoFornitura = (TextView) stato_avanzamento_forniture_entry.findViewById(R.id.fornitura_iesima_richiesta_1);
			ImageView iconaLuce = (ImageView) stato_avanzamento_forniture_entry.findViewById(R.id.stato_attivazione_fornitura_iesima_luce_icona_1);
			ImageView iconaGas = (ImageView) stato_avanzamento_forniture_entry.findViewById(R.id.stato_attivazione_fornitura_iesima_gas_icona_1);
			TextView labelPodPdr = (TextView) stato_avanzamento_forniture_entry.findViewById(R.id.pod_iesima_label_1);
			if (fornituraRichiesta.equals("POWER"))
			{
				tipoFornitura.setText(R.string.luce);
				iconaLuce.setImageResource(R.drawable.light_yes);
				iconaGas.setImageResource(R.drawable.gas_no);
				labelPodPdr.setText(R.string.punto_consegna_pod);
			} else
			{
				tipoFornitura.setText(R.string.gas);
				iconaLuce.setImageResource(R.drawable.light_no);
				iconaGas.setImageResource(R.drawable.gas_yes);
				labelPodPdr.setText(R.string.punto_consegna_pdr);
			}
			TextView statoFornitura = (TextView) stato_avanzamento_forniture_entry.findViewById(R.id.stato_fornitura_iesima_1);
			ImageView iconaStato = (ImageView) stato_avanzamento_forniture_entry.findViewById(R.id.stato_attivazione_fornitura_iesima_lucina_1);
			String tipologiaStato = fornituraPrima.getString("TipologiaStato");
			if (tipologiaStato.equals("ATTIVA"))
			{
				statoFornitura.setText(R.string.fornitura_attiva_numero);
				iconaStato.setImageResource(R.drawable.green_dot);
				TextView labelfornituraDal = (TextView) stato_avanzamento_forniture_entry.findViewById(R.id.fornitura_iesima_dal_label_1);
				labelfornituraDal.setText(R.string.fornitura_attiva_dal);
				TextView fornituraDal = (TextView) stato_avanzamento_forniture_entry.findViewById(R.id.fornitura_iesima_dal_1);
				fornituraDal.setText(fornituraPrima.getString("DataAttivazione"));
				TextView attivazionePrevista = (TextView) stato_avanzamento_forniture_entry.findViewById(R.id.attivazione_prevista_il_label_1);
				attivazionePrevista.setVisibility(View.INVISIBLE);
				TextView attivazionePrevistaData = (TextView) stato_avanzamento_forniture_entry.findViewById(R.id.attivazione_prevista_il_1);
				attivazionePrevistaData.setVisibility(View.INVISIBLE);
			} else
			{
				statoFornitura.setText(R.string.fornitura_in_attivazione);
				iconaStato.setImageResource(R.drawable.yellow_dot);
				TextView labelfornituraDal = (TextView) stato_avanzamento_forniture_entry.findViewById(R.id.fornitura_iesima_dal_label_1);
				labelfornituraDal.setText(R.string.fornitura_accettata_il);
				TextView fornituraDal = (TextView) stato_avanzamento_forniture_entry.findViewById(R.id.fornitura_iesima_dal_1);
				fornituraDal.setText(fornituraPrima.getString("DataAccettazione"));
				TextView attivazionePrevista = (TextView) stato_avanzamento_forniture_entry.findViewById(R.id.attivazione_prevista_il_label_1);
				attivazionePrevista.setVisibility(View.VISIBLE);
				TextView attivazionePrevistaData = (TextView) stato_avanzamento_forniture_entry.findViewById(R.id.attivazione_prevista_il_1);
				attivazionePrevistaData.setVisibility(View.VISIBLE);
				attivazionePrevistaData.setText(fornituraPrima.getString("DataPrevistaAttivazione"));
			}
			TextView numeroFornitura = (TextView) stato_avanzamento_forniture_entry.findViewById(R.id.numero_fornitura_iesima_1);
			numeroFornitura.setText(fornituraPrima.getString("Pdf"));
			TextView intestatario = (TextView) stato_avanzamento_forniture_entry.findViewById(R.id.intestato_a_1);
			intestatario.setText(nomeInteroCliente);
			TextView podPdr = (TextView) stato_avanzamento_forniture_entry.findViewById(R.id.pod_iesima_1);
			podPdr.setText(fornituraPrima.getString("Pod/pdr"));
			TextView indirizzo1Riga = (TextView) stato_avanzamento_forniture_entry.findViewById(R.id.indirizzo_fornitura_iesima_riga_1_1);
			String indirizzoFornitura = fornituraPrima.getString("IndirizzoFornitura");
			indirizzo1Riga.setText(indirizzoFornitura);
			// seconda fornitura
			RelativeLayout fornitura2 = (RelativeLayout) stato_avanzamento_forniture_entry.findViewById(R.id.fornitura_2);
			fornitura2.setVisibility(View.VISIBLE);

			String fornituraRichiesta2 = fornituraSeconda.getString("FornituraRichiesta");
			TextView tipoFornitura2 = (TextView) stato_avanzamento_forniture_entry.findViewById(R.id.fornitura_iesima_richiesta_2);
			ImageView iconaLuce2 = (ImageView) stato_avanzamento_forniture_entry.findViewById(R.id.stato_attivazione_fornitura_iesima_luce_icona_2);
			ImageView iconaGas2 = (ImageView) stato_avanzamento_forniture_entry.findViewById(R.id.stato_attivazione_fornitura_iesima_gas_icona_2);
			TextView labelPodPdr2 = (TextView) stato_avanzamento_forniture_entry.findViewById(R.id.pod_iesima_label_2);
			if (fornituraRichiesta2.equals("POWER"))
			{
				tipoFornitura2.setText(R.string.luce);
				iconaLuce2.setImageResource(R.drawable.light_yes);
				iconaGas2.setImageResource(R.drawable.gas_no);
				labelPodPdr2.setText(R.string.punto_consegna_pod);
			} else
			{
				tipoFornitura2.setText(R.string.gas);
				iconaLuce2.setImageResource(R.drawable.light_no);
				iconaGas2.setImageResource(R.drawable.gas_yes);
				labelPodPdr2.setText(R.string.punto_consegna_pdr);
			}
			TextView statoFornitura2 = (TextView) stato_avanzamento_forniture_entry.findViewById(R.id.stato_fornitura_iesima_2);
			ImageView iconaStato2 = (ImageView) stato_avanzamento_forniture_entry.findViewById(R.id.stato_attivazione_fornitura_iesima_lucina_2);
			String tipologiaStato2 = fornituraSeconda.getString("TipologiaStato");
			if (tipologiaStato2.equals("ATTIVA"))
			{
				statoFornitura2.setText(R.string.fornitura_attiva_numero);
				iconaStato2.setImageResource(R.drawable.green_dot);
				TextView labelfornituraDal = (TextView) stato_avanzamento_forniture_entry.findViewById(R.id.fornitura_iesima_dal_label_2);
				labelfornituraDal.setText(R.string.fornitura_attiva_dal);
				TextView fornituraDal = (TextView) stato_avanzamento_forniture_entry.findViewById(R.id.fornitura_iesima_dal_2);
				fornituraDal.setText(fornituraSeconda.getString("DataAttivazione"));
				TextView attivazionePrevista = (TextView) stato_avanzamento_forniture_entry.findViewById(R.id.attivazione_prevista_il_label_2);
				attivazionePrevista.setVisibility(View.INVISIBLE);
				TextView attivazionePrevistaData = (TextView) stato_avanzamento_forniture_entry.findViewById(R.id.attivazione_prevista_il_2);
				attivazionePrevistaData.setVisibility(View.INVISIBLE);
			} else
			{
				statoFornitura2.setText(R.string.fornitura_in_attivazione);
				iconaStato2.setImageResource(R.drawable.yellow_dot);
				TextView labelfornituraDal = (TextView) stato_avanzamento_forniture_entry.findViewById(R.id.fornitura_iesima_dal_label_2);
				labelfornituraDal.setText(R.string.fornitura_accettata_il);
				TextView fornituraDal = (TextView) stato_avanzamento_forniture_entry.findViewById(R.id.fornitura_iesima_dal_2);
				fornituraDal.setText(fornituraSeconda.getString("DataAccettazione"));
				TextView attivazionePrevista = (TextView) stato_avanzamento_forniture_entry.findViewById(R.id.attivazione_prevista_il_label_2);
				attivazionePrevista.setVisibility(View.VISIBLE);
				TextView attivazionePrevistaData = (TextView) stato_avanzamento_forniture_entry.findViewById(R.id.attivazione_prevista_il_2);
				attivazionePrevistaData.setVisibility(View.VISIBLE);
				attivazionePrevistaData.setText(fornituraSeconda.getString("DataPrevistaAttivazione"));
			}
			TextView numeroFornitura2 = (TextView) stato_avanzamento_forniture_entry.findViewById(R.id.numero_fornitura_iesima_2);
			numeroFornitura2.setText(fornituraSeconda.getString("Pdf"));
			TextView intestatario2 = (TextView) stato_avanzamento_forniture_entry.findViewById(R.id.intestato_a_2);
			intestatario2.setText(nomeInteroCliente);
			TextView podPdr2 = (TextView) stato_avanzamento_forniture_entry.findViewById(R.id.pod_iesima_2);
			podPdr2.setText(fornituraSeconda.getString("Pod/pdr"));
			TextView indirizzo1Riga2 = (TextView) stato_avanzamento_forniture_entry.findViewById(R.id.indirizzo_fornitura_iesima_riga_1_2);
			String indirizzoFornitura2 = fornituraSeconda.getString("IndirizzoFornitura");
			indirizzo1Riga2.setText(indirizzoFornitura2);

			stato_avanzamento_fornitura_page.addView(stato_avanzamento_forniture_entry);
			pager.addView(stato_avanzamento_fornitura_page);
		} catch (JSONException e)
		{
			e.printStackTrace();
		}

	}

	public void popolaFornituraNonUtilizzabile(RelativeLayout pager, String codiceConto, JSONArray array)
	{
		String codiceContoIesimo = "";
		JSONObject anagrContoIesimo = new JSONObject();
		JSONObject anagrExtraContoIesimo = new JSONObject();
		JSONObject anagrContoIesimo1 = new JSONObject();
		JSONObject anagrExtraContoIesimo1 = new JSONObject();
		JSONObject anagrContoIesimo2 = new JSONObject();
		JSONObject anagrExtraContoIesimo2 = new JSONObject();
		int count = 0;
		// che tipo di conto e', singlo o dual
		try
		{
			for (int i = 2; i < array.length(); i++)
			{
				JSONArray contoIesimo;
				contoIesimo = array.getJSONArray(i);
				anagrContoIesimo = contoIesimo.getJSONObject(0);

				codiceContoIesimo = anagrContoIesimo.getString("CodiceConto");
				if (!codiceContoIesimo.equals(codiceConto))
				{
					continue;
				} else
				{
					// ho trovato il conto codiceConto
					if (contoIesimo.length() > 1)
					{
						anagrExtraContoIesimo = contoIesimo.getJSONObject(1);
						if (count == 0)
						{
							anagrContoIesimo1 = anagrContoIesimo;
							anagrExtraContoIesimo1 = anagrExtraContoIesimo;
							count++;
						} else
						{
							anagrContoIesimo2 = anagrContoIesimo;
							anagrExtraContoIesimo2 = anagrExtraContoIesimo;
							count++;
						}
					} else
					{
						// cessato e vuoto
						AlertDialog.Builder popup = new AlertDialog.Builder(context);
						popup.setMessage(R.string.nessuna_fornitura_attiva).setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id)
							{
							}
						});
						popup.create().show();
						return;
					}

				}
			}
			if (count == 1)
			{
				// conto singolo
				RelativeLayout stato_avanzamento_fornitura_page = new RelativeLayout(this);
				stato_avanzamento_fornitura_page = new RelativeLayout(this);
				View stato_avanzamento_forniture_entry = inflater.inflate(R.layout.stato_avanzamento_fornitura_entry_singola, null);
				
				RelativeLayout fornituraBackgroundSingolo = (RelativeLayout) stato_avanzamento_forniture_entry.findViewById(R.id.fornitura);
				fornituraBackgroundSingolo.setVisibility(View.GONE);
				RelativeLayout fornituraBackgroundSingoloCessata = (RelativeLayout) stato_avanzamento_forniture_entry.findViewById(R.id.fornitura_cessata);
				fornituraBackgroundSingoloCessata.setVisibility(View.VISIBLE);
				
				String fornituraRichiesta = anagrExtraContoIesimo1.getString("Tipologia");
				TextView tipoFornitura = (TextView) stato_avanzamento_forniture_entry.findViewById(R.id.fornitura_iesima_richiesta_cessata);
				ImageView iconaLuce = (ImageView) stato_avanzamento_forniture_entry.findViewById(R.id.stato_attivazione_fornitura_iesima_luce_icona_cessata);
				ImageView iconaGas = (ImageView) stato_avanzamento_forniture_entry.findViewById(R.id.stato_attivazione_fornitura_iesima_gas_icona_cessata);
				if (fornituraRichiesta.equals("POWER"))
				{
					tipoFornitura.setText(R.string.luce);
					iconaLuce.setImageResource(R.drawable.light_yes);
					iconaGas.setImageResource(R.drawable.gas_no);
				} else
				{
					tipoFornitura.setText(R.string.gas);
					iconaLuce.setImageResource(R.drawable.light_no);
					iconaGas.setImageResource(R.drawable.gas_yes);
				}
				TextView statoFornitura = (TextView) stato_avanzamento_forniture_entry.findViewById(R.id.stato_fornitura_iesima_cessata);
				ImageView iconaStato = (ImageView) stato_avanzamento_forniture_entry.findViewById(R.id.stato_attivazione_fornitura_iesima_lucina_cessata);

				statoFornitura.setText(R.string.fornitura_cessata);
				iconaStato.setImageResource(R.drawable.gray_dot);

				TextView numeroFornitura = (TextView) stato_avanzamento_forniture_entry.findViewById(R.id.numero_fornitura_iesima_cessata);
				numeroFornitura.setText(anagrExtraContoIesimo1.getString("Pdf"));
				TextView intestatario = (TextView) stato_avanzamento_forniture_entry.findViewById(R.id.intestato_a_cessata);
				intestatario.setText(nomeInteroCliente);
				TextView indirizzo1Riga = (TextView) stato_avanzamento_forniture_entry.findViewById(R.id.indirizzo_fornitura_iesima_riga_1_cessata);
				String indirizzoFornitura = anagrExtraContoIesimo1.getString("IndirizzoCompleto");
				indirizzo1Riga.setText(indirizzoFornitura);

				stato_avanzamento_fornitura_page.addView(stato_avanzamento_forniture_entry);
				pager.addView(stato_avanzamento_fornitura_page);
			} else
			{
				// conto dual
				RelativeLayout stato_avanzamento_fornitura_page = new RelativeLayout(this);
				stato_avanzamento_fornitura_page = new RelativeLayout(this);
				View stato_avanzamento_forniture_entry = inflater.inflate(R.layout.stato_avanzamento_fornitura_entry_doppia, null);

				RelativeLayout fornituraBackground = (RelativeLayout) stato_avanzamento_forniture_entry.findViewById(R.id.fornitura_1);
				fornituraBackground.setVisibility(View.GONE);
				RelativeLayout fornituraBackgroundCessata = (RelativeLayout) stato_avanzamento_forniture_entry.findViewById(R.id.fornitura_1_cessata);
				fornituraBackgroundCessata.setVisibility(View.VISIBLE);
				
				String fornituraRichiesta = anagrExtraContoIesimo1.getString("Tipologia");
				TextView tipoFornitura = (TextView) stato_avanzamento_forniture_entry.findViewById(R.id.fornitura_iesima_richiesta_1_cessata);
				ImageView iconaLuce = (ImageView) stato_avanzamento_forniture_entry.findViewById(R.id.stato_attivazione_fornitura_iesima_luce_icona_1_cessata);
				ImageView iconaGas = (ImageView) stato_avanzamento_forniture_entry.findViewById(R.id.stato_attivazione_fornitura_iesima_gas_icona_1_cessata);
				if (fornituraRichiesta.equals("POWER"))
				{
					tipoFornitura.setText(R.string.luce);
					iconaLuce.setImageResource(R.drawable.light_yes);
					iconaGas.setImageResource(R.drawable.gas_no);
				} else
				{
					tipoFornitura.setText(R.string.gas);
					iconaLuce.setImageResource(R.drawable.light_no);
					iconaGas.setImageResource(R.drawable.gas_yes);
				}
				TextView statoFornitura = (TextView) stato_avanzamento_forniture_entry.findViewById(R.id.stato_fornitura_iesima_1_cessata);
				ImageView iconaStato = (ImageView) stato_avanzamento_forniture_entry.findViewById(R.id.stato_attivazione_fornitura_iesima_lucina_1_cessata);
				
				statoFornitura.setText(R.string.fornitura_cessata);
				iconaStato.setImageResource(R.drawable.gray_dot);

				TextView numeroFornitura = (TextView) stato_avanzamento_forniture_entry.findViewById(R.id.numero_fornitura_iesima_1_cessata);
				numeroFornitura.setText(anagrExtraContoIesimo1.getString("Pdf"));
				TextView intestatario = (TextView) stato_avanzamento_forniture_entry.findViewById(R.id.intestato_a_1_cessata);
				intestatario.setText(nomeInteroCliente);
				TextView indirizzo1Riga = (TextView) stato_avanzamento_forniture_entry.findViewById(R.id.indirizzo_fornitura_iesima_riga_1_1_cessata);
				String indirizzoFornitura = anagrExtraContoIesimo1.getString("IndirizzoCompleto");
				indirizzo1Riga.setText(indirizzoFornitura);

				
				
				// seconda fornitura
				RelativeLayout fornituraBackground2 = (RelativeLayout) stato_avanzamento_forniture_entry.findViewById(R.id.fornitura_2);
				fornituraBackground2.setVisibility(View.GONE);
				RelativeLayout fornituraBackground2Cessato  = (RelativeLayout) stato_avanzamento_forniture_entry.findViewById(R.id.fornitura_2_cessata);
				fornituraBackground2Cessato.setVisibility(View.VISIBLE);

				String fornituraRichiesta2 = anagrExtraContoIesimo2.getString("Tipologia");
				TextView tipoFornitura2 = (TextView) stato_avanzamento_forniture_entry.findViewById(R.id.fornitura_iesima_richiesta_2_cessata);
				ImageView iconaLuce2 = (ImageView) stato_avanzamento_forniture_entry.findViewById(R.id.stato_attivazione_fornitura_iesima_luce_icona_2_cessata);
				ImageView iconaGas2 = (ImageView) stato_avanzamento_forniture_entry.findViewById(R.id.stato_attivazione_fornitura_iesima_gas_icona_2_cessata);
				if (fornituraRichiesta2.equals("POWER"))
				{
					tipoFornitura2.setText(R.string.luce);
					iconaLuce2.setImageResource(R.drawable.light_yes);
					iconaGas2.setImageResource(R.drawable.gas_no);
				} else
				{
					tipoFornitura2.setText(R.string.gas);
					iconaLuce2.setImageResource(R.drawable.light_no);
					iconaGas2.setImageResource(R.drawable.gas_yes);
				}
				TextView statoFornitura2 = (TextView) stato_avanzamento_forniture_entry.findViewById(R.id.stato_fornitura_iesima_2_cessata);
				ImageView iconaStato2 = (ImageView) stato_avanzamento_forniture_entry.findViewById(R.id.stato_attivazione_fornitura_iesima_lucina_2_cessata);

				statoFornitura2.setText(R.string.fornitura_cessata);
				iconaStato2.setImageResource(R.drawable.gray_dot);

				TextView numeroFornitura2 = (TextView) stato_avanzamento_forniture_entry.findViewById(R.id.numero_fornitura_iesima_2_cessata);
				numeroFornitura2.setText(anagrExtraContoIesimo2.getString("Pdf"));
				TextView intestatario2 = (TextView) stato_avanzamento_forniture_entry.findViewById(R.id.intestato_a_2_cessata);
				intestatario2.setText(nomeInteroCliente);
				TextView indirizzo1Riga2 = (TextView) stato_avanzamento_forniture_entry.findViewById(R.id.indirizzo_fornitura_iesima_riga_1_2_cessata);
				String indirizzoFornitura2 = anagrExtraContoIesimo2.getString("IndirizzoCompleto");
				indirizzo1Riga2.setText(indirizzoFornitura2);

				stato_avanzamento_fornitura_page.addView(stato_avanzamento_forniture_entry);
				pager.addView(stato_avanzamento_fornitura_page);

			}
		} catch (JSONException e)
		{
			e.printStackTrace();
		}

	}

	public String getCodiceCliente(String rispostaLogin)
	{
		String result = "";
		try
		{
			JSONArray loginJson = new JSONArray(rispostaLogin);
			JSONObject datiCliente = (JSONObject) loginJson.get(1);
			result = datiCliente.getString("CodiceCliente");
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
		return result;
	}

	public void postChiamata(String rispostaChiamata, List<Cookie> lista, HashMap<String, String> parametri)
	{

		final ImageView statoAttivazioneIndietro = (ImageView) findViewById(R.id.stato_attivazione_fornitura_indietro);
		statoAttivazioneIndietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
		statoAttivazioneIndietro.getBackground().invalidateSelf();

		JSONArray rispostaJSON = new JSONArray();
		String esito = "";
		String descEsito = "";
		try
		{
			rispostaJSON = new JSONArray(rispostaChiamata);
			JSONObject oggettoRisposta = (JSONObject) rispostaJSON.get(0);
			esito = oggettoRisposta.getString("esito");
			descEsito = oggettoRisposta.getString("descEsito");
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
		if (esito.equals("200"))
		{
			Constants.SERVIZIO_STATO_AVANZAMENTO_FORNITURA = rispostaChiamata;
			chiamata = true;
			try
			{
				JSONArray array = new JSONArray(Constants.SERVIZIO_STATO_AVANZAMENTO_FORNITURA);
				popolaPaginaContenuto(array, pager);
				if (animation != null)
				{
					animation.stop();
					imageAnim.setVisibility(View.INVISIBLE);
				}
			} catch (JSONException e)
			{
				e.printStackTrace();
			}
		} else
		{
			Intent intentOops = new Intent(getApplicationContext(), OopsPageAttivazioneFornituraFromSaf.class);
			intentOops.putExtra("CODICE_CLIENTE", parametri.get("codiceCliente"));
			intentOops.putExtra("CODICE_CONTO_CLIENTE", parametri.get("codiceContoCliente"));
			positionSpinnerCurrentSelected = positionSpinnerPrecedentSelected;
			// errori ops page
			if (esito.equals("309"))
			{
				intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
				startActivity(intentOops);
				overridePendingTransition(R.anim.fade, R.anim.hold);

			} else if (esito.equals("400"))
			{
				intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
				startActivity(intentOops);
				overridePendingTransition(R.anim.fade, R.anim.hold);

			} else if (esito.equals("420"))
			{
				intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
				startActivity(intentOops);
				overridePendingTransition(R.anim.fade, R.anim.hold);

			} else if (esito.equals("449"))
			{
				intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
				startActivity(intentOops);
				overridePendingTransition(R.anim.fade, R.anim.hold);

			} else if (esito.equals("451"))
			{
				intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_popup_451));
				startActivity(intentOops);
				overridePendingTransition(R.anim.fade, R.anim.hold);

			}
			// errori popup
			else if (esito.equals("307"))
			{
				Utils.popup(StatoAvanzamentoFornitura.this, getResources().getString(R.string.errore_popup_307), "Riprova");

			} else if (esito.equals("435"))
			{
				Utils.popup(StatoAvanzamentoFornitura.this, getResources().getString(R.string.errore_popup_435), "Riprova");

			} else if (esito.equals("437"))
			{
				Utils.popup(StatoAvanzamentoFornitura.this, getResources().getString(R.string.errore_popup_437), "Riprova");

			} else if (esito.equals("438"))
			{
				Utils.popup(StatoAvanzamentoFornitura.this, getResources().getString(R.string.errore_popup_438), "Riprova");

			} else if (esito.equals("443"))
			{
				Utils.popup(StatoAvanzamentoFornitura.this, getResources().getString(R.string.errore_popup_443), "Riprova");

			} else if (esito.equals("444"))
			{
				Utils.popup(StatoAvanzamentoFornitura.this, getResources().getString(R.string.errore_popup_444), "Riprova");

			} else if (esito.equals("445"))
			{
				Utils.popup(StatoAvanzamentoFornitura.this, getResources().getString(R.string.errore_popup_445), "Riprova");

			} else if (esito.equals("446"))
			{
				Utils.popup(StatoAvanzamentoFornitura.this, getResources().getString(R.string.errore_popup_446), "Riprova");

			} else if (esito.equals("447"))
			{
				Utils.popup(StatoAvanzamentoFornitura.this, getResources().getString(R.string.errore_popup_447), "Riprova");

			} else
			{
				if (descEsito.equals("")) {
					intentOops = new Intent(getApplicationContext(), OopsPageSessioneScaduta.class);
					intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.sessione_scaduta));
				} else{
					intentOops.putExtra("MESSAGGIO", descEsito);
				}
				
				startActivity(intentOops);
				overridePendingTransition(R.anim.fade, R.anim.hold);
			}

		}

	}

	public void postChiamataException(boolean exception, HashMap<String, String> parameterMap)
	{
		if (exception)
		{
			if (animation != null)
			{
				animation.stop();
				imageAnim.setVisibility(View.INVISIBLE);
			}

			final ImageView statoAttivazioneIndietro = (ImageView) findViewById(R.id.stato_attivazione_fornitura_indietro);
			statoAttivazioneIndietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
			statoAttivazioneIndietro.getBackground().invalidateSelf();

			Intent intentOops = new Intent(getApplicationContext(), OopsPageAttivazioneFornituraFromSaf.class);
			intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
			intentOops.putExtra("CODICE_CLIENTE", parameterMap.get("codiceCliente"));
			intentOops.putExtra("CODICE_CONTO_CLIENTE", parameterMap.get("codiceContoCliente"));
			positionSpinnerCurrentSelected = positionSpinnerPrecedentSelected;
			// StatoAttivazioneFornitura.this.finish();
			startActivity(intentOops);
			overridePendingTransition(R.anim.fade, R.anim.hold);
		}
	}

	public void popolaPaginaContenuto(JSONArray stAvForn, RelativeLayout pager)
	{
		try
		{
			// final LinearLayout scroller = (LinearLayout)
			// findViewById(R.id.stato_attivazione_fornitura_scroller);
			pager.removeAllViews();
			// scroller.removeAllViews();
			inflater = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);

			if (stAvForn.length() > 1)
			{
				JSONArray oggettiFornitura = (JSONArray) stAvForn.get(1);

				// controllo se ce ne sono 2 con indirizzo uguale (caso Dual)
				if (oggettiFornitura.length() > 1)
				{
					if (oggettiFornitura.length() == 2)
					{
						// se ce ne sono due allora per forza � caso DUAL
						// (l'ha
						// detto Baraldo)
						JSONObject fornituraPrima = (JSONObject) oggettiFornitura.get(0);
						JSONObject fornituraSeconda = (JSONObject) oggettiFornitura.get(1);
						popolaFornituraDual(pager, fornituraPrima, fornituraSeconda);
					}
				} else
				{
					JSONObject fornitura = (JSONObject) oggettiFornitura.get(0);
					popolaSingolaFornitura(pager, fornitura);
				}

				/*
				 * int numeroPagine = pager.getChildCount(); for (int i = 0; i <
				 * numeroPagine; i++) { ImageView iv = new ImageView(this);
				 * iv.setId(i); LinearLayout.LayoutParams param = new
				 * LinearLayout
				 * .LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
				 * LinearLayout.LayoutParams.WRAP_CONTENT); param.setMargins(10,
				 * 0, 0, 0); iv.setImageResource(R.drawable.dot_not_selected);
				 * iv.setLayoutParams(param); scroller.addView(iv); }
				 */
			} else
			{
				AlertDialog.Builder popup = new AlertDialog.Builder(context);
				popup.setMessage(R.string.nessuna_fornitura_attiva).setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id)
					{
					}
				});
				popup.create().show();
			}

		} catch (JSONException e)
		{
			e.printStackTrace();
		}

	}

	private boolean isNetworkAvailable(Context context)
	{
		ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivityManager == null)
			return false;
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		return activeNetworkInfo != null;
	}

	public void onBackPressed()
	{
		animation = Utilities.animation(StatoAvanzamentoFornitura.this);
		imageAnim = (ImageView) findViewById(R.id.waiting_anim);
		imageAnim.setVisibility(View.VISIBLE);
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN)
		{
			imageAnim.setBackgroundDrawable(animation);
		} else
		{
			imageAnim.setBackground(animation);
		}
		animation.start();
		Intent intentHome = new Intent(getApplicationContext(), Home.class);
		StatoAvanzamentoFornitura.this.finish();
		startActivity(intentHome);
		overridePendingTransition(R.anim.fade, R.anim.hold);
	}

}
