package com.eni.enigaseluce.statoavanzamentofornitura;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.AnimationDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.eni.enigaseluce.R;
import com.eni.enigaseluce.utils.Utilities;

@SuppressLint("NewApi")
public class StatoAvanzamentoFronituraHelp extends Activity {
    AnimationDrawable animation;
    ImageView imageAnim;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.stato_avanzamento_fornitura_help);

        final ImageView help = (ImageView) findViewById(R.id.stato_attivazione_forniture_help_indietro);
        help.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
        help.getBackground().invalidateSelf();
        help.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v1) {
                help.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
                help.getBackground().invalidateSelf();
                animation = Utilities.animation(StatoAvanzamentoFronituraHelp.this);
                imageAnim = (ImageView) findViewById(R.id.waiting_anim);
                imageAnim.setVisibility(View.VISIBLE);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    imageAnim.setBackgroundDrawable(animation);
                } else {
                    imageAnim.setBackground(animation);
                }
                animation.start();
                Intent intentHelp = new Intent(getApplicationContext(), StatoAvanzamentoFornitura.class);
                intentHelp.putExtra("INDICE_SPINNER_ST", getIntent().getIntExtra("INDICE_SPINNER_ST", 0));
                intentHelp.putExtra("CHIAMATA", getIntent().getBooleanExtra("CHIAMATA", false));
                StatoAvanzamentoFronituraHelp.this.finish();
                startActivity(intentHelp);
                overridePendingTransition(R.anim.fade, R.anim.hold);
            }
        });

    }

    public void onBackPressed() {
        animation = Utilities.animation(StatoAvanzamentoFronituraHelp.this);
        imageAnim = (ImageView) findViewById(R.id.waiting_anim);
        imageAnim.setVisibility(View.VISIBLE);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
            imageAnim.setBackgroundDrawable(animation);
        } else {
            imageAnim.setBackground(animation);
        }
        animation.start();
        Intent intentHelp = new Intent(getApplicationContext(), StatoAvanzamentoFornitura.class);
        intentHelp.putExtra("INDICE_SPINNER_ST", getIntent().getIntExtra("INDICE_SPINNER_ST", 0));
        StatoAvanzamentoFronituraHelp.this.finish();
        startActivity(intentHelp);
        overridePendingTransition(R.anim.fade, R.anim.hold);
    }
}
