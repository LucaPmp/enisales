package com.eni.enigaseluce.storicoletture;

import java.util.HashMap;
import java.util.List;

import org.apache.http.cookie.Cookie;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.AnimationDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.eni.enigaseluce.R;
import com.eni.enigaseluce.autolettura.AutoLetturaInsert;
import com.eni.enigaseluce.autolettura.Autolettura;
import com.eni.enigaseluce.error.OopsPageAutoletturaAutolettura;
import com.eni.enigaseluce.error.OopsPageSessioneScaduta;
import com.eni.enigaseluce.error.OopsPageStoricoLettureDetail;
import com.eni.enigaseluce.error.Utils;
import com.eni.enigaseluce.font.EniFont;
import com.eni.enigaseluce.httpcall.Constants;
import com.eni.enigaseluce.httpcall.ServiceCall;
import com.eni.enigaseluce.httpcall.ServiceCallFactory;
import com.eni.enigaseluce.utils.JSONUtility;
import com.eni.enigaseluce.utils.Utilities;

@SuppressLint("NewApi")
public class StoricoLettureDetail extends Activity {
    AnimationDrawable animation;
    ImageView imageAnim;
    /** Called when the activity is first created. */

    public String rispostaLogin = Constants.SERVIZIO_LOGIN;
    public String rispostaStoricoLetture = Constants.SERVIZIO_STORICO_LETTURE;
    public String rispostaUltimeLetture = Constants.SERVIZIO_FORNITURE_GAS_ATTIVE;

    String pdf = "";
    String pdr = "";
    String sistemaBilling = "";
    String societaFornitrice = "";
    String numeroCifreMisuratore = "";
    String codiceEsito;
    static LayoutInflater inflater;

    @Override
    public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.storico_letture_detail);

	Intent intent = this.getIntent();
	pdf = intent.getStringExtra("PDF");
	pdr = intent.getStringExtra("PDR");

	HashMap<String, String> parametri = (HashMap<String, String>) intent.getSerializableExtra("PARAMETRI");

	sistemaBilling = parametri.get("sistemaBilling");
	societaFornitrice = parametri.get("societaFornitrice");
	numeroCifreMisuratore = parametri.get("numeroCifreMisuratore");
    }

    @Override
    protected void onStart() {
	super.onStart();

	LinearLayout layout_tabbar = (LinearLayout) findViewById(R.id.layout_tabbar);

	EniFont tab_autolettura = (EniFont) layout_tabbar.findViewById(R.id.tab_autolettura);
	tab_autolettura.setOnClickListener(new OnClickListener() {

	    @Override
	    public void onClick(View v) {

		// deve essere invocato il servizio web "Ultima lettura"
		// mediante il quale sarÃ  recuperato il valore
		// dellâ€™ultima lettura effettiva per il contatore selezionato.
		// A seguito della ricezione dei dati, sarÃ  visualizzata la
		// pagina AS-IS â€œInserimento auto-letturaâ€�.

		animation = Utilities.animation(StoricoLettureDetail.this);
		imageAnim = (ImageView) findViewById(R.id.waiting_anim);
		imageAnim.setVisibility(View.VISIBLE);
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
		    imageAnim.setBackgroundDrawable(animation);
		}
		else {
		    imageAnim.setBackground(animation);
		}
		animation.start();

		try {
		    String codiceCliente = JSONUtility.getCodiceCliente(rispostaLogin);

		    if (!Utilities.isNetworkAvailable(StoricoLettureDetail.this)) {
			Intent intentOops = new Intent(getApplicationContext(), OopsPageAutoletturaAutolettura.class);
			intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.connessione_assente));
			intentOops.putExtra("PDF", pdf);
			intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
			intentOops.putExtra("SISTEMA_BILLING", sistemaBilling);
			intentOops.putExtra("SOCIETA_FORNITRICE", societaFornitrice);
			intentOops.putExtra("NUMERO_CIFRE_MISURATORE", numeroCifreMisuratore);

			// StatoAttivazioneFornitura.this.finish();
			startActivity(intentOops);
			overridePendingTransition(R.anim.fade, R.anim.hold);
		    }
		    else {
			ServiceCall stAvFornServiceCall = ServiceCallFactory.createServiceCall(StoricoLettureDetail.this);

			HashMap<String, String> parametri = new HashMap<String, String>();
			parametri.put("pdf", pdf);
			parametri.put("codiceCliente", codiceCliente);
			// stAvFornServiceCall.myCookieStore = new
			// PersistentCookieStore(context);

			try {
			    stAvFornServiceCall.executeHttpsGetAutoletturaFromStoricoDetail(Constants.URL_ULTIMA_LETTURA_GAS, parametri, StoricoLettureDetail.this, pdf, pdr, sistemaBilling, societaFornitrice, numeroCifreMisuratore, null);

			    // finish();
			}
			catch (Exception e) {
			    Intent intentOops = new Intent(getApplicationContext(), OopsPageAutoletturaAutolettura.class);
			    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
			    intentOops.putExtra("PDF", pdf);
			    intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
			    intentOops.putExtra("SISTEMA_BILLING", sistemaBilling);
			    intentOops.putExtra("SOCIETA_FORNITRICE", societaFornitrice);
			    intentOops.putExtra("NUMERO_CIFRE_MISURATORE", numeroCifreMisuratore);

			    finish();
			    startActivity(intentOops);
			    overridePendingTransition(R.anim.fade, R.anim.hold);
			}
		    }
		}
		catch (Exception e) {
		    // Intent intentOops = new
		    // Intent(getApplicationContext(),
		    // OopsPageAutoletturaAutolettura.class);
		    // intentOops.putExtra("MESSAGGIO",
		    // getResources().getString(R.string.servizio_non_disponibile));
		    // intentOops.putExtra("PDF", pdf);
		    // intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
		    // intentOops.putExtra("SISTEMA_BILLING",
		    // sistemaBilling);
		    // intentOops.putExtra("SOCIETA_FORNITRICE",
		    // societaFornitrice);
		    // intentOops.putExtra("NUMERO_CIFRE_MISURATORE",
		    // numeroCifreMisuratore);

		    // StatoAttivazioneFornitura.this.finish();
		    // startActivity(intentOops);
		    // overridePendingTransition(R.anim.fade, R.anim.hold);
		    e.printStackTrace();
		}
	    }
	});

	EniFont tab_storico = (EniFont) layout_tabbar.findViewById(R.id.tab_storico);
	tab_storico.setSelected(true);
	tab_storico.setEnabled(false);
	tab_storico.setTextColor(Color.GRAY);

	final ImageView storicoDetIndietro = (ImageView) findViewById(R.id.storico_letture_det_indietro);
	storicoDetIndietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
	storicoDetIndietro.getBackground().invalidateSelf();
	storicoDetIndietro.setOnClickListener(new View.OnClickListener() {
	    public void onClick(View v1) {
		storicoDetIndietro.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
		storicoDetIndietro.getBackground().invalidateSelf();
		animation = Utilities.animation(StoricoLettureDetail.this);
		imageAnim = (ImageView) findViewById(R.id.waiting_anim);
		imageAnim.setVisibility(View.VISIBLE);
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
		    imageAnim.setBackgroundDrawable(animation);
		}
		else {
		    imageAnim.setBackground(animation);
		}
		animation.start();
		Intent intentHome = new Intent(getApplicationContext(), Autolettura.class);
		StoricoLettureDetail.this.finish();
		startActivity(intentHome);
		overridePendingTransition(R.anim.fade, R.anim.hold);
	    }
	});

	try {
	    final JSONArray loginJSON = new JSONArray(rispostaLogin);
	    // JSONObject esito = loginJSON.getJSONObject(0);

	    JSONObject datiAnagr = loginJSON.getJSONObject(1);
	    // ultimo accesso
	    String ultimoaccesso = datiAnagr.getString("UltimoAccesso");
	    TextView ultimoAccesso = (TextView) findViewById(R.id.ultimo_accesso);
	    // ultimoAccesso.setTextSize(11);
	    ultimoAccesso.setText(ultimoaccesso);
	    // nome cliente
	    String nomeClienteString = datiAnagr.getString("NomeCliente");
	    String cognomeClienteString = datiAnagr.getString("CognomeCliente");
	    TextView nomeCliente = (TextView) findViewById(R.id.nome_cliente);
	    String nomeInteroCliente = nomeClienteString + " " + cognomeClienteString;
	    nomeCliente.setText(nomeInteroCliente);

	    TextView pdrText = (TextView) findViewById(R.id.storico_letture_det_pdr);
	    pdrText.setText(pdr);

	    TextView name = (TextView) findViewById(R.id.storico_letture_det_nome);
	    name.setText(nomeInteroCliente);

	    String indirzzo = cercaIndirizzoFornitura(new JSONArray(rispostaLogin), pdf);
	    TextView indirizzo1 = (TextView) findViewById(R.id.storico_letture_det_via);
	    indirizzo1.setText(indirzzo);

	    JSONArray storicoLetture = new JSONArray(rispostaStoricoLetture);
	    JSONArray listaAutoletture = storicoLetture.getJSONArray(1);
	    LinearLayout storico_letture_det_entry = (LinearLayout) findViewById(R.id.storico_letture_det_entry);
	    storico_letture_det_entry.removeAllViews();
	    inflater = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);

	    for (int i = 0; i < listaAutoletture.length(); i++) {
		JSONObject ogettoAutolettura = listaAutoletture.getJSONObject(i);

		View autolettura_entry = inflater.inflate(R.layout.storico_letture_detail_entry, null);
		TextView data = (TextView) autolettura_entry.findViewById(R.id.storico_letture_det_data);
		data.setText(ogettoAutolettura.getString("DataLettura"));
		TextView valore = (TextView) autolettura_entry.findViewById(R.id.storico_letture_det_valore);
		valore.setText(ogettoAutolettura.getString("ValoreLettura"));
		TextView storico_letture_det_stato = (TextView) autolettura_entry.findViewById(R.id.storico_letture_det_stato);
		storico_letture_det_stato.setText(ogettoAutolettura.getString("StatoLettura"));
		if (ogettoAutolettura.getString("StatoLettura").equals("Valida")) {
		    storico_letture_det_stato.setTextColor(getResources().getColor(R.color.green1));
		}
		else {
		    storico_letture_det_stato.setTextColor(getResources().getColor(R.color.red1));
		}
		storico_letture_det_entry.addView(autolettura_entry);
	    }

	}
	catch (JSONException e) {
	    e.printStackTrace();
	}

    }

    public String cercaIndirizzoFornitura(JSONArray array, String pdf) {
	String result = "";
	String pdfIesimo = "";
	JSONObject anagrExtraContoIesimo = new JSONObject();
	try {
	    for (int i = 2; i < array.length(); i++) {
		JSONArray contoIesimo;
		contoIesimo = array.getJSONArray(i);
		// codice conto
		if (contoIesimo.length() == 1) {
		    continue;
		}
		else {
		    anagrExtraContoIesimo = contoIesimo.getJSONObject(1);
		    pdfIesimo = anagrExtraContoIesimo.getString("Pdf");

		    if (!pdfIesimo.equals(pdf)) {
			// codice conto diverso
			continue;
		    }
		    else {
			result = anagrExtraContoIesimo.getString("IndirizzoCompleto");
		    }
		}

	    }
	}
	catch (JSONException e) {
	    e.printStackTrace();
	}
	return result;
    }

    public void postChiamata(String rispostaChiamata, List<Cookie> lista, String pdf, String pdr, String sistemaBilling, String societaFornitrice, String numeroCifreMisuratore, String codiceCliente) {
	JSONArray rispostaJSON = new JSONArray();
	String esito = "";
	String descEsito = "";
	if (animation != null) {
	    animation.stop();
	    imageAnim.setVisibility(View.INVISIBLE);
	}

	final ImageView autoletturaIndietro = (ImageView) findViewById(R.id.storico_letture_det_indietro);
	autoletturaIndietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
	autoletturaIndietro.getBackground().invalidateSelf();

	try {
	    rispostaJSON = new JSONArray(rispostaChiamata);
	    JSONObject oggettoRisposta = (JSONObject) rispostaJSON.get(0);
	    esito = oggettoRisposta.getString("esito");
	    descEsito = oggettoRisposta.getString("descEsito");
	}
	catch (JSONException e) {
	    e.printStackTrace();
	}
	if (esito.equals("200")) {
	    Constants.SERVIZIO_ULTIMA_LETTURA = rispostaChiamata;
	    Intent intentAutoletturaInsert = new Intent(getApplicationContext(), AutoLetturaInsert.class);
	    intentAutoletturaInsert.putExtra("PDF", pdf);
	    intentAutoletturaInsert.putExtra("PDR", pdr);

	    JSONArray loginJSON;
	    try {
		loginJSON = new JSONArray(rispostaLogin);
		final String codiceConto = JSONUtility.cercaCodiceConto(loginJSON, pdf);
		intentAutoletturaInsert.putExtra("CURRENT_CODICE_CONTO", codiceConto);
	    }
	    catch (JSONException e) {
		e.printStackTrace();
	    }

	    intentAutoletturaInsert.putExtra("SISTEMABILLING", sistemaBilling);
	    intentAutoletturaInsert.putExtra("SOCIETAFORNITRICE", societaFornitrice);
	    intentAutoletturaInsert.putExtra("NUMCIFREMISURATORE", numeroCifreMisuratore);
	    startActivity(intentAutoletturaInsert);
	    finish();
	    overridePendingTransition(R.anim.fade, R.anim.hold);
	}
	else {
	    Intent intentOops = new Intent(getApplicationContext(), OopsPageStoricoLettureDetail.class);

	    // errori ops page
	    if (esito.equals("309")) {
		intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
		startActivity(intentOops);
		overridePendingTransition(R.anim.fade, R.anim.hold);
	    }
	    // check applicative error codes 4xx family retrieving error codes
	    // from resources
	    else if (Utilities.arrayContainsString(getResources().getStringArray(R.array.execute_domiciliazione_error_codes), esito)) {
		Utils.popup(StoricoLettureDetail.this, descEsito, "Riprova");
	    }
	    else {
	    	if (descEsito.equals("")) {
				intentOops = new Intent(getApplicationContext(), OopsPageSessioneScaduta.class);
				intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.sessione_scaduta));
			} else{
				intentOops.putExtra("MESSAGGIO", descEsito);
			}
			
			startActivity(intentOops);
			overridePendingTransition(R.anim.fade, R.anim.hold);
	    }
	}
    }

    public void postChiamataException(boolean exception, String pdf, String sistemaBilling, String societaFornitrice, String numeroCifreMisuratore, String codiceCliente) {
	if (exception) {
	    if (animation != null) {
		animation.stop();
		imageAnim.setVisibility(View.INVISIBLE);
	    }

	    final ImageView autoletturaIndietro = (ImageView) findViewById(R.id.storico_letture_det_indietro);
	    autoletturaIndietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
	    autoletturaIndietro.getBackground().invalidateSelf();

	    Intent intentOops = new Intent(getApplicationContext(), OopsPageAutoletturaAutolettura.class);
	    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));

	    startActivity(intentOops);
	    overridePendingTransition(R.anim.fade, R.anim.hold);
	}
    }

    @Override
    public void onBackPressed() {
	animation = Utilities.animation(StoricoLettureDetail.this);
	imageAnim = (ImageView) findViewById(R.id.waiting_anim);
	imageAnim.setVisibility(View.VISIBLE);
	if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
	    imageAnim.setBackgroundDrawable(animation);
	}
	else {
	    imageAnim.setBackground(animation);
	}
	animation.start();
	Intent intentHome = new Intent(getApplicationContext(), Autolettura.class);
	StoricoLettureDetail.this.finish();
	startActivity(intentHome);
	overridePendingTransition(R.anim.fade, R.anim.hold);
    }

}
