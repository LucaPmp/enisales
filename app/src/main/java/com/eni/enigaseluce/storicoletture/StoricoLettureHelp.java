package com.eni.enigaseluce.storicoletture;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.AnimationDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.eni.enigaseluce.R;
import com.eni.enigaseluce.httpcall.Constants;
import com.eni.enigaseluce.utils.Utilities;

@SuppressLint("NewApi")
public class StoricoLettureHelp extends Activity {
    AnimationDrawable animation;
    ImageView imageAnim;
    String origine;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.storico_letture_help);

	origine = getIntent().getStringExtra("ORIGINE");
	final ImageView help = (ImageView) findViewById(R.id.storico_letture_help_indietro);
	help.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
	help.getBackground().invalidateSelf();
	help.setOnClickListener(new View.OnClickListener() {
	    public void onClick(View v1) {
		help.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
		help.getBackground().invalidateSelf();
		animation = Utilities.animation(StoricoLettureHelp.this);
		imageAnim = (ImageView) findViewById(R.id.waiting_anim);
		imageAnim.setVisibility(View.VISIBLE);
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
		    imageAnim.setBackgroundDrawable(animation);
		}
		else {
		    imageAnim.setBackground(animation);
		}
		animation.start();
		Intent intentHelp = new Intent();
		if (origine.equals(Constants.ORIGINE_STORICO_LETTURE)) {
		    intentHelp = new Intent(getApplicationContext(), StoricoLetture.class);
		}
		else if (origine.equals(Constants.ORIGINE_STORICO_LETTURE_DETAIL)) {
		    intentHelp = new Intent(getApplicationContext(), StoricoLettureDetail.class);
		    intentHelp.putExtra("PDF", getIntent().getStringExtra("PDF"));
		    intentHelp.putExtra("PDR", getIntent().getStringExtra("PDR"));
		}
		StoricoLettureHelp.this.finish();
		startActivity(intentHelp);
		overridePendingTransition(R.anim.fade, R.anim.hold);
	    }
	});

    }

    public void onBackPressed() {
	animation = Utilities.animation(StoricoLettureHelp.this);
	imageAnim = (ImageView) findViewById(R.id.waiting_anim);
	imageAnim.setVisibility(View.VISIBLE);
	if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
	    imageAnim.setBackgroundDrawable(animation);
	}
	else {
	    imageAnim.setBackground(animation);
	}
	animation.start();
	Intent intentHelp = new Intent();
	if (origine.equals(Constants.ORIGINE_STORICO_LETTURE)) {
	    intentHelp = new Intent(getApplicationContext(), StoricoLetture.class);
	}
	else if (origine.equals(Constants.ORIGINE_STORICO_LETTURE_DETAIL)) {
	    intentHelp = new Intent(getApplicationContext(), StoricoLettureDetail.class);
	    intentHelp.putExtra("PDF", getIntent().getStringExtra("PDF"));
	    intentHelp.putExtra("PDR", getIntent().getStringExtra("PDR"));
	}
	StoricoLettureHelp.this.finish();
	startActivity(intentHelp);
	overridePendingTransition(R.anim.fade, R.anim.hold);
    }
}
