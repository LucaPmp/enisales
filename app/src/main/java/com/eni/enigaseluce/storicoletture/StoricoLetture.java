package com.eni.enigaseluce.storicoletture;

import java.util.HashMap;
import java.util.List;

import org.apache.http.cookie.Cookie;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.AnimationDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.eni.enigaseluce.R;
import com.eni.enigaseluce.autolettura.AutoLetturaInsert;
import com.eni.enigaseluce.error.OopsPageSessioneScaduta;
import com.eni.enigaseluce.error.OopsPageStoricoLetture;
import com.eni.enigaseluce.error.Utils;
import com.eni.enigaseluce.home.Home;
import com.eni.enigaseluce.httpcall.Constants;
import com.eni.enigaseluce.httpcall.ServiceCall;
import com.eni.enigaseluce.httpcall.ServiceCallFactory;
import com.eni.enigaseluce.pager.HorizontalPager;
import com.eni.enigaseluce.utils.JSONUtility;
import com.eni.enigaseluce.utils.Utilities;

@SuppressLint("NewApi")
public class StoricoLetture extends Activity {
    AnimationDrawable animation;
    ImageView imageAnim;
    private Context context;
    static Context staticContext;
    /** Called when the activity is first created. */

    public String rispostaLogin = Constants.SERVIZIO_LOGIN;
    public String rispostaUltimeLetture = Constants.SERVIZIO_FORNITURE_GAS_ATTIVE;
    String codiceEsito;

    @Override
    public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.storico_letture);
	context = this;
	StoricoLetture.staticContext = this;

    }

    public static Context getContext() {
	return StoricoLetture.staticContext;
    }

    @Override
    protected void onStart() {
	super.onStart();

	if (true) throw new RuntimeException("This activity must not be excecuted");

	final ImageView domiciliazioneIndietro = (ImageView) findViewById(R.id.storico_letture_indietro);
	domiciliazioneIndietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
	domiciliazioneIndietro.getBackground().invalidateSelf();
	domiciliazioneIndietro.setOnClickListener(new View.OnClickListener() {
	    public void onClick(View v1) {
		domiciliazioneIndietro.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
		domiciliazioneIndietro.getBackground().invalidateSelf();
		animation = Utilities.animation(StoricoLetture.this);
		imageAnim = (ImageView) findViewById(R.id.waiting_anim);
		imageAnim.setVisibility(View.VISIBLE);
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
		    imageAnim.setBackgroundDrawable(animation);
		}
		else {
		    imageAnim.setBackground(animation);
		}
		animation.start();
		Intent intentHome = new Intent(getApplicationContext(), Home.class);
		StoricoLetture.this.finish();
		startActivity(intentHome);
		overridePendingTransition(R.anim.fade, R.anim.hold);
	    }
	});

	final ImageView domiciliazioneHelp = (ImageView) findViewById(R.id.storico_letture_help);
	domiciliazioneHelp.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
	domiciliazioneHelp.getBackground().invalidateSelf();
	domiciliazioneHelp.setOnClickListener(new View.OnClickListener() {
	    public void onClick(View v1) {
		domiciliazioneHelp.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
		domiciliazioneHelp.getBackground().invalidateSelf();
		animation = Utilities.animation(StoricoLetture.this);
		imageAnim = (ImageView) findViewById(R.id.waiting_anim);
		imageAnim.setVisibility(View.VISIBLE);
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
		    imageAnim.setBackgroundDrawable(animation);
		}
		else {
		    imageAnim.setBackground(animation);
		}
		animation.start();
		Intent intentHome = new Intent(getApplicationContext(), StoricoLettureHelp.class);
		intentHome.putExtra("ORIGINE", Constants.ORIGINE_STORICO_LETTURE);
		StoricoLetture.this.finish();
		startActivity(intentHome);
		overridePendingTransition(R.anim.fade, R.anim.hold);
	    }
	});

	final HorizontalPager contatori_pager = (HorizontalPager) findViewById(R.id.storico_letture_pager);
	contatori_pager.removeAllViews();
	LayoutInflater inflater = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);
	try {
	    final JSONArray loginJSON = new JSONArray(rispostaLogin);
	    // JSONObject esito = loginJSON.getJSONObject(0);

	    JSONObject datiAnagr = loginJSON.getJSONObject(1);
	    // ultimo accesso
	    String ultimoaccesso = datiAnagr.getString("UltimoAccesso");
	    TextView ultimoAccesso = (TextView) findViewById(R.id.ultimo_accesso);
	    // ultimoAccesso.setTextSize(11);
	    ultimoAccesso.setText(ultimoaccesso);
	    // nome cliente
	    String nomeClienteString = datiAnagr.getString("NomeCliente");
	    String cognomeClienteString = datiAnagr.getString("CognomeCliente");
	    TextView nomeCliente = (TextView) findViewById(R.id.nome_cliente);
	    String nomeInteroCliente = nomeClienteString + " " + cognomeClienteString;
	    nomeCliente.setText(nomeInteroCliente);

	    // parte del contattore
	    // quante forniture da visualizza?
	    JSONArray ultimeLettureJSON = new JSONArray(rispostaUltimeLetture);
	    int numeroOggetti = ultimeLettureJSON.length() - 1;

	    // parto da secondo oggetto
	    for (int i = 1; i < numeroOggetti + 1; i++) {
		JSONObject contatoreIesimo = ultimeLettureJSON.getJSONObject(i);
		View entry_storico_letture = inflater.inflate(R.layout.storico_letture_entry, null);
		TextView matricola = (TextView) entry_storico_letture.findViewById(R.id.matricola);
		matricola.setText(contatoreIesimo.getString("CodiceContatore"));

		TextView indirizzoInstallazione = (TextView) entry_storico_letture.findViewById(R.id.installato_a_indirizzo);
		String indirizzoFornituraIesimo = cercaIndirizzoFornitura(loginJSON, contatoreIesimo.getString("Pdf"));
		if (indirizzoFornituraIesimo.length() > 36) {
		    indirizzoInstallazione.setText(indirizzoFornituraIesimo.substring(0, 36) + "...");
		}
		else {
		    indirizzoInstallazione.setText(indirizzoFornituraIesimo);
		}

		final String pdr = contatoreIesimo.getString("Pdr");
		final String pdf = contatoreIesimo.getString("Pdf");

		String codiceConto = JSONUtility.cercaCodiceConto(loginJSON, pdf);
		final String sistemaBilling = JSONUtility.cercaSistemaBilling(loginJSON, pdf);
		final String societaFornitrice = JSONUtility.cercaSocietaFornitrice(loginJSON, pdf);
		final String numeroCifreMisuratore = contatoreIesimo.getString("NumCifreMisuratore");

		TextView numeroCliente = (TextView) entry_storico_letture.findViewById(R.id.num_cliente_valore);
		numeroCliente.setText(codiceConto);

		RelativeLayout contatore = (RelativeLayout) entry_storico_letture.findViewById(R.id.storico_letture_installato);
		contatore.setOnClickListener(new View.OnClickListener() {
		    public void onClick(View v1) {
			// parte la chiamata al dettaglio dello storico
			animation = Utilities.animation(StoricoLetture.this);
			imageAnim = (ImageView) findViewById(R.id.waiting_anim);
			imageAnim.setVisibility(View.VISIBLE);
			if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
			    imageAnim.setBackgroundDrawable(animation);
			}
			else {
			    imageAnim.setBackground(animation);
			}
			animation.start();
			/************************************ Storico letture gas ************************************/
			if (!isNetworkAvailable(context)) {
			    Intent intentOops = new Intent(getApplicationContext(), OopsPageStoricoLetture.class);
			    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.connessione_assente));
			    intentOops.putExtra("PDF", pdf);
			    intentOops.putExtra("SISTEMA_BILLING", sistemaBilling);
			    intentOops.putExtra("SOCIETA_FORNITRICE", societaFornitrice);
			    intentOops.putExtra("PDR", pdr);
			    // StatoAttivazioneFornitura.this.finish();
			    startActivity(intentOops);
			    if (animation != null) {
				animation.stop();
				imageAnim.setVisibility(View.INVISIBLE);
			    }
			    overridePendingTransition(R.anim.fade, R.anim.hold);
			}
			else {
			    ServiceCall stAvFornServiceCall = ServiceCallFactory.createServiceCall(context);
			    // stAvFornServiceCall.myCookieStore = new
			    // PersistentCookieStore(context);

			    HashMap<String, String> parametri = new HashMap<String, String>();
			    parametri.put("pdf", pdf);
			    parametri.put("sistemaBilling", sistemaBilling);
			    parametri.put("societaFornitrice", societaFornitrice);
			    parametri.put("numeroCifreMisuratore", numeroCifreMisuratore);

			    try {
				stAvFornServiceCall.executeHttpsGetStoricoLetture(Constants.URL_STORICO_LETTURE_GAS, parametri, (AutoLetturaInsert) AutoLetturaInsert.getContext(), pdf, pdr, null);
			    }
			    catch (Exception e) {
				Intent intentOops = new Intent(getApplicationContext(), OopsPageStoricoLetture.class);
				intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
				intentOops.putExtra("PDF", pdf);
				intentOops.putExtra("SISTEMA_BILLING", sistemaBilling);
				intentOops.putExtra("SOCIETA_FORNITRICE", societaFornitrice);
				intentOops.putExtra("PDR", pdr);

				// StatoAttivazioneFornitura.this.finish();
				startActivity(intentOops);
				if (animation != null) {
				    animation.stop();
				    imageAnim.setVisibility(View.INVISIBLE);
				}
				overridePendingTransition(R.anim.fade, R.anim.hold);
			    }
			}
			/*******************************************************************************************/
		    }
		});

		contatori_pager.addView(entry_storico_letture);
	    }

	}
	catch (JSONException e) {
	    e.printStackTrace();
	}

	final LinearLayout scroller = (LinearLayout) findViewById(R.id.contatori_scroller);
	scroller.removeAllViews();
	int numeroPagine = contatori_pager.getChildCount();
	for (int i = 0; i < numeroPagine; i++) {
	    ImageView iv = new ImageView(this);
	    iv.setId(i);
	    LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
	    param.setMargins(10, 0, 0, 0);
	    iv.setImageResource(R.drawable.dot_not_selected);
	    iv.setLayoutParams(param);
	    scroller.addView(iv);

	    int idFocusedPage = contatori_pager.getCurrentPage();
	    ImageView pageFocused = (ImageView) findViewById(idFocusedPage);
	    pageFocused.setImageResource(R.drawable.dot_selected);

	}

	contatori_pager.addOnScrollListener(new HorizontalPager.OnScrollListener() {
	    int numeroPagine = contatori_pager.getChildCount();

	    public void onScroll(int scrollX) {
		for (int i = 0; i < numeroPagine; i++) {
		    ImageView iv = (ImageView) findViewById(i);
		    iv.setImageResource(R.drawable.dot_not_selected);
		}
		int idFocusedPage = contatori_pager.getCurrentPage();
		ImageView pageFocused = (ImageView) findViewById(idFocusedPage);
		pageFocused.setImageResource(R.drawable.dot_selected);
	    }

	    public void onViewScrollFinished(int currentPage) {
		for (int i = 0; i < numeroPagine; i++) {
		    ImageView iv = (ImageView) findViewById(i);
		    iv.setImageResource(R.drawable.dot_not_selected);
		}
		int idFocusedPage = contatori_pager.getCurrentPage();
		ImageView pageFocused = (ImageView) findViewById(idFocusedPage);
		pageFocused.setImageResource(R.drawable.dot_selected);
	    }
	});

    }

    public String cercaIndirizzoFornitura(JSONArray array, String pdf) {
	String result = "";
	String pdfIesimo = "";
	JSONObject anagrExtraContoIesimo = new JSONObject();
	JSONObject anagrContoIesimo = new JSONObject();
	try {
	    for (int i = 2; i < array.length(); i++) {
		JSONArray contoIesimo;
		contoIesimo = array.getJSONArray(i);
		// codice conto
		if (contoIesimo.length() == 1) {
		    continue;
		}
		else {
		    anagrExtraContoIesimo = contoIesimo.getJSONObject(1);
		    pdfIesimo = anagrExtraContoIesimo.getString("Pdf");

		    if (!pdfIesimo.equals(pdf)) {
			// codice conto diverso
			continue;
		    }
		    else {
			anagrContoIesimo = contoIesimo.getJSONObject(0);
			if (!(anagrContoIesimo.getString("StatoConto")).equals("UTILIZZABILE")) {
			    continue;
			}
			else {
			    result = anagrExtraContoIesimo.getString("IndirizzoCompleto");
			}
		    }
		}

	    }
	}
	catch (JSONException e) {
	    e.printStackTrace();
	}
	return result;
    }

    public String cercaCodiceConto(JSONArray array, String pdf) {
	String result = "";
	String pdfIesimo = "";
	JSONObject anagrContoIesimo = new JSONObject();
	JSONObject anagrExtraContoIesimo = new JSONObject();
	try {
	    for (int i = 2; i < array.length(); i++) {
		JSONArray contoIesimo;
		contoIesimo = array.getJSONArray(i);
		// codice conto
		if (contoIesimo.length() == 1) {
		    continue;
		}
		else {
		    anagrExtraContoIesimo = contoIesimo.getJSONObject(1);
		    pdfIesimo = anagrExtraContoIesimo.getString("Pdf");
		    if (!pdfIesimo.equals(pdf)) {
			// codice conto diverso
			continue;
		    }
		    else {
			anagrContoIesimo = contoIesimo.getJSONObject(0);
			if (!(anagrContoIesimo.getString("StatoConto")).equals("UTILIZZABILE")) {
			    continue;
			}
			else {
			    result = anagrContoIesimo.getString("CodiceConto");
			}
		    }
		}

	    }
	}
	catch (JSONException e) {
	    e.printStackTrace();
	}
	return result;
    }

    public String cercaSistemaBilling(JSONArray array, String pdf) {
	String result = "";
	String pdfIesimo = "";
	JSONObject anagrContoIesimo = new JSONObject();
	JSONObject anagrExtraContoIesimo = new JSONObject();
	try {
	    for (int i = 2; i < array.length(); i++) {
		JSONArray contoIesimo;
		contoIesimo = array.getJSONArray(i);
		// codice conto
		if (contoIesimo.length() == 1) {
		    continue;
		}
		else {
		    anagrExtraContoIesimo = contoIesimo.getJSONObject(1);
		    pdfIesimo = anagrExtraContoIesimo.getString("Pdf");
		    if (!pdfIesimo.equals(pdf)) {
			// codice conto diverso
			continue;
		    }
		    else {
			anagrContoIesimo = contoIesimo.getJSONObject(0);
			if (!(anagrContoIesimo.getString("StatoConto")).equals("UTILIZZABILE")) {
			    continue;
			}
			else {
			    result = anagrContoIesimo.getString("SistemaBilling");
			}
		    }
		}

	    }
	}
	catch (JSONException e) {
	    e.printStackTrace();
	}
	return result;
    }

    public String cercaSocietaFornitrice(JSONArray array, String pdf) {
	String result = "";
	String pdfIesimo = "";
	JSONObject anagrContoIesimo = new JSONObject();
	JSONObject anagrExtraContoIesimo = new JSONObject();
	try {
	    for (int i = 2; i < array.length(); i++) {
		JSONArray contoIesimo;
		contoIesimo = array.getJSONArray(i);
		// codice conto
		if (contoIesimo.length() == 1) {
		    continue;
		}
		else {
		    anagrExtraContoIesimo = contoIesimo.getJSONObject(1);
		    pdfIesimo = anagrExtraContoIesimo.getString("Pdf");
		    if (!pdfIesimo.equals(pdf)) {
			// codice conto diverso
			continue;
		    }
		    else {
			anagrContoIesimo = contoIesimo.getJSONObject(0);
			if (!(anagrContoIesimo.getString("StatoConto")).equals("UTILIZZABILE")) {
			    continue;
			}
			else {
			    result = anagrContoIesimo.getString("SocietaFornitrice");
			}
		    }
		}
	    }
	}
	catch (JSONException e) {
	    e.printStackTrace();
	}
	return result;
    }

    public void postChiamata(String rispostaChiamata, List<Cookie> lista, String pdf, String pdr, HashMap<String, String> parametri) {
	JSONArray rispostaJSON = new JSONArray();
	String esito = "";
	String descEsito = "";
	if (animation != null) {
	    animation.stop();
	    imageAnim.setVisibility(View.INVISIBLE);
	}

	final ImageView domiciliazioneIndietro = (ImageView) findViewById(R.id.storico_letture_indietro);
	domiciliazioneIndietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
	domiciliazioneIndietro.getBackground().invalidateSelf();

	try {
	    rispostaJSON = new JSONArray(rispostaChiamata);
	    JSONObject oggettoRisposta = (JSONObject) rispostaJSON.get(0);
	    esito = oggettoRisposta.getString("esito");
	    descEsito = oggettoRisposta.getString("descEsito");
	}
	catch (JSONException e) {
	    e.printStackTrace();
	}
	if (esito.equals("200")) {
	    Constants.SERVIZIO_STORICO_LETTURE = rispostaChiamata;
	    Intent intentStoricoLetture = new Intent(getApplicationContext(), StoricoLettureDetail.class);
	    intentStoricoLetture.putExtra("PDF", pdf);
	    intentStoricoLetture.putExtra("PDR", pdr);
	    intentStoricoLetture.putExtra("PARAMETRI", parametri);
	    StoricoLetture.this.finish();
	    startActivity(intentStoricoLetture);
	    overridePendingTransition(R.anim.fade, R.anim.hold);
	}
	else {
	    Intent intentOops = new Intent(getApplicationContext(), OopsPageStoricoLetture.class);
	    // errori ops page
	    if (esito.equals("309")) {
		intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
		startActivity(intentOops);
		overridePendingTransition(R.anim.fade, R.anim.hold);

	    }
	    else if (esito.equals("400")) {
		intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
		startActivity(intentOops);
		overridePendingTransition(R.anim.fade, R.anim.hold);

	    }
	    else if (esito.equals("420")) {
		intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
		startActivity(intentOops);
		overridePendingTransition(R.anim.fade, R.anim.hold);

	    }
	    else if (esito.equals("449")) {
		intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
		startActivity(intentOops);
		overridePendingTransition(R.anim.fade, R.anim.hold);

	    }
	    else if (esito.equals("451")) {
		intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_popup_451));
		startActivity(intentOops);
		overridePendingTransition(R.anim.fade, R.anim.hold);

	    }
	    // errori popup
	    else if (esito.equals("307")) {
		Utils.popup(StoricoLetture.this, getResources().getString(R.string.errore_popup_307), "Riprova");

	    }
	    else if (esito.equals("435")) {
		Utils.popup(StoricoLetture.this, getResources().getString(R.string.errore_popup_435), "Riprova");

	    }
	    else if (esito.equals("437")) {
		Utils.popup(StoricoLetture.this, getResources().getString(R.string.errore_popup_437), "Riprova");

	    }
	    else if (esito.equals("438")) {
		Utils.popup(StoricoLetture.this, getResources().getString(R.string.errore_popup_438), "Riprova");

	    }
	    else if (esito.equals("443")) {
		Utils.popup(StoricoLetture.this, getResources().getString(R.string.errore_popup_443), "Riprova");

	    }
	    else if (esito.equals("444")) {
		Utils.popup(StoricoLetture.this, getResources().getString(R.string.errore_popup_444), "Riprova");

	    }
	    else if (esito.equals("445")) {
		Utils.popup(StoricoLetture.this, getResources().getString(R.string.errore_popup_445), "Riprova");

	    }
	    else if (esito.equals("446")) {
		Utils.popup(StoricoLetture.this, getResources().getString(R.string.errore_popup_446), "Riprova");

	    }
	    else if (esito.equals("447")) {
		Utils.popup(StoricoLetture.this, getResources().getString(R.string.errore_popup_447), "Riprova");

	    }
	    else {
	    	if (descEsito.equals("")) {
				intentOops = new Intent(getApplicationContext(), OopsPageSessioneScaduta.class);
				intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.sessione_scaduta));
			} else{
				intentOops.putExtra("MESSAGGIO", descEsito);
			}
			
			startActivity(intentOops);
			overridePendingTransition(R.anim.fade, R.anim.hold);
	    }

	}

    }

    public void postChiamataException(boolean exception) {
	if (exception) {
	    if (animation != null) {
		animation.stop();
		imageAnim.setVisibility(View.INVISIBLE);
	    }

	    final ImageView domiciliazioneIndietro = (ImageView) findViewById(R.id.storico_letture_indietro);
	    domiciliazioneIndietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
	    domiciliazioneIndietro.getBackground().invalidateSelf();

	    Intent intentOops = new Intent(getApplicationContext(), OopsPageStoricoLetture.class);
	    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
	    // StatoAttivazioneFornitura.this.finish();
	    startActivity(intentOops);
	    overridePendingTransition(R.anim.fade, R.anim.hold);
	}
    }

    private boolean isNetworkAvailable(Context context) {
	ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
	if (connectivityManager == null) return false;
	NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
	return activeNetworkInfo != null;
    }

    public void onBackPressed() {
	animation = Utilities.animation(StoricoLetture.this);
	imageAnim = (ImageView) findViewById(R.id.waiting_anim);
	imageAnim.setVisibility(View.VISIBLE);
	if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
	    imageAnim.setBackgroundDrawable(animation);
	}
	else {
	    imageAnim.setBackground(animation);
	}
	animation.start();
	Intent intentHome = new Intent(getApplicationContext(), Home.class);
	StoricoLetture.this.finish();
	startActivity(intentHome);
	overridePendingTransition(R.anim.fade, R.anim.hold);
    }

}
