package com.eni.enigaseluce.domiciliazione;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.AnimationDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.eni.enigaseluce.R;
import com.eni.enigaseluce.home.Home;
import com.eni.enigaseluce.httpcall.Constants;
import com.eni.enigaseluce.utils.JSONUtility;
import com.eni.enigaseluce.utils.Log;

@SuppressLint("NewApi")
public class DomiciliazioneClientDetail extends Activity {
    String codiceConto = "";
    String indirizzoFornitura = "";

    public String rispostaLogin = Constants.SERVIZIO_LOGIN;
    public String rispostavisualizzaDomiciliazione = Constants.SERVIZIO_VISUALIZZA_FORNITURE_DOMICILIAZIONE;

    AnimationDrawable animation;
    ImageView imageAnim;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.domiciliazione_detail);
	Intent intent = getIntent();
	codiceConto = intent.getStringExtra("CodiceConto");
	indirizzoFornitura = intent.getStringExtra("IndirizzoFornitura");

	Log.d("DOMICILIAZIONECLIENTDETAIL","DomiciliazioneClientDetail.onCreate()\nCodiceConto: " + codiceConto + "\nIndirizzoFornitura: " + indirizzoFornitura);

	final ImageView indietro = (ImageView) findViewById(R.id.domiciliazione_detail_indietro);
	indietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
	indietro.getBackground().invalidateSelf();
	indietro.setOnClickListener(new View.OnClickListener() {
	    public void onClick(View v1) {
		indietro.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
		indietro.getBackground().invalidateSelf();
		animation = new AnimationDrawable();
		animation.addFrame(getResources().getDrawable(R.drawable.anim_load2), 300);
		animation.addFrame(getResources().getDrawable(R.drawable.anim_load3), 300);
		animation.addFrame(getResources().getDrawable(R.drawable.anim_load4), 300);
		animation.setOneShot(false);
		imageAnim = (ImageView) findViewById(R.id.waiting_anim);
		imageAnim.setVisibility(View.VISIBLE);
		if (Build.VERSION.SDK_INT < 16) {
		    imageAnim.setBackgroundDrawable(animation);
		}
		else {
		    imageAnim.setBackground(animation);
		}
		// start the animation!
		animation.start();
		Intent intentDom = new Intent(getApplicationContext(), Home.class);
		DomiciliazioneClientDetail.this.finish();
		startActivity(intentDom);
		overridePendingTransition(R.anim.fade, R.anim.hold);
	    }
	});

	final ImageView help = (ImageView) findViewById(R.id.domiciliazione_detail_help);
	help.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
	help.getBackground().invalidateSelf();
	help.setOnClickListener(new View.OnClickListener() {
	    public void onClick(View v1) {
		help.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
		help.getBackground().invalidateSelf();
		animation = new AnimationDrawable();
		animation.addFrame(getResources().getDrawable(R.drawable.anim_load2), 300);
		animation.addFrame(getResources().getDrawable(R.drawable.anim_load3), 300);
		animation.addFrame(getResources().getDrawable(R.drawable.anim_load4), 300);
		animation.setOneShot(false);
		imageAnim = (ImageView) findViewById(R.id.waiting_anim);
		imageAnim.setVisibility(View.VISIBLE);
		if (Build.VERSION.SDK_INT < 16) {
		    imageAnim.setBackgroundDrawable(animation);
		}
		else {
		    imageAnim.setBackground(animation);
		}
		// start the animation!
		animation.start();
		Intent intentHelp = new Intent(getApplicationContext(), DomiciliazioneHelp.class);
		intentHelp.putExtra("ORIGINE", Constants.ORIGINE_DOMICILIAZIONE_DETAIL);
		intentHelp.putExtra("CodiceConto", codiceConto);
		intentHelp.putExtra("IndirizzoFornitura", indirizzoFornitura);
		DomiciliazioneClientDetail.this.finish();
		startActivity(intentHelp);
		overridePendingTransition(R.anim.fade, R.anim.hold);
	    }
	});

    }

    @Override
    protected void onStart() {
	super.onStart();
	rispostavisualizzaDomiciliazione = Constants.SERVIZIO_VISUALIZZA_FORNITURE_DOMICILIAZIONE;
	try {
	    final JSONArray domicilLogin = new JSONArray(rispostaLogin);
	    JSONObject datiAnagr = domicilLogin.getJSONObject(1);
	    // ultimo accesso
	    String ultimoaccesso = datiAnagr.getString("UltimoAccesso");
	    final TextView ultimoAccesso = (TextView) findViewById(R.id.ultimo_accesso);
	    ultimoAccesso.setText(ultimoaccesso);
	    // nome cliente
	    String nomeClienteString = datiAnagr.getString("NomeCliente");
	    String cognomeClienteString = datiAnagr.getString("CognomeCliente");
	    final TextView nomeCliente = (TextView) findViewById(R.id.nome_cliente);
	    String nomeInteroCliente = nomeClienteString + " " + cognomeClienteString;
	    nomeCliente.setText(nomeInteroCliente);

	    JSONArray rispostaDomiciliazioneJSON = new JSONArray(rispostavisualizzaDomiciliazione);

	    JSONObject contoDomiciliazione = JSONUtility.trovaOggettoDomiciliazione(rispostaDomiciliazioneJSON, codiceConto);
	    String contoCorrente = contoDomiciliazione.getString("ContoCorrente");
	    final TextView domiciliazione_detail_numero_cc_text = (TextView) findViewById(R.id.domiciliazione_detail_numero_cc_text);
	    domiciliazione_detail_numero_cc_text.setText(contoCorrente);

	    final TextView domiciliazione_detail_nome_banca_text = (TextView) findViewById(R.id.domiciliazione_detail_nome_banca_text);
	    domiciliazione_detail_nome_banca_text.setText(contoDomiciliazione.getString("NomeBanca"));

	    final TextView domiciliazione_detail_nome_text = (TextView) findViewById(R.id.domiciliazione_detail_nome_text);
	    domiciliazione_detail_nome_text.setText(contoDomiciliazione.getString("NomeIntestatario"));

	    final TextView domiciliazione_detail_cognome_text = (TextView) findViewById(R.id.domiciliazione_detail_cognome_text);
	    domiciliazione_detail_cognome_text.setText(contoDomiciliazione.getString("CognomeIntestatario"));

	    final TextView domiciliazione_detail_numero_cliente_text = (TextView) findViewById(R.id.domiciliazione_detail_numero_cliente_text);
	    domiciliazione_detail_numero_cliente_text.setText(contoDomiciliazione.getString("CodiceContoCliente"));

	    final TextView tipoProdotto = (TextView) findViewById(R.id.domiciliazione_detail_prodotto_text);
	    String tipoProdottoText = JSONUtility.trovaTipoProdottoFornitura(new JSONArray(rispostaLogin), codiceConto);
	    tipoProdotto.setText(tipoProdottoText);

	    final TextView domiciliazione_detail_indirizzo_fornitura_text1 = (TextView) findViewById(R.id.domiciliazione_detail_indirizzo_fornitura_text);
	    domiciliazione_detail_indirizzo_fornitura_text1.setText(indirizzoFornitura);

	    ImageView lucina = (ImageView) findViewById(R.id.domiciliazione_detail_lucina);
	    final TextView statoDesc = (TextView) findViewById(R.id.domiciliazione_detail_descrizione);
	    String stato = contoDomiciliazione.getString("StatoDomiciliazione");
	    if (stato.equals("ATTIVA") || stato.equals("APPROVATA")) {
		// green
		statoDesc.setText(R.string.domiciliazione_attiva);
		lucina.setImageResource(R.drawable.green_dot);

	    }
	    else if (stato.equals("INVIATA") || stato.equals("DA INVIARE")) {
		// gialla
		statoDesc.setText(R.string.richiesta_domiciliazione_in_corso);
		lucina.setImageResource(R.drawable.yellow_dot);
	    }
	}
	catch (JSONException e) {
	    e.printStackTrace();
	}
    }

    public void onBackPressed() {
	animation = new AnimationDrawable();
	animation.addFrame(getResources().getDrawable(R.drawable.anim_load2), 300);
	animation.addFrame(getResources().getDrawable(R.drawable.anim_load3), 300);
	animation.addFrame(getResources().getDrawable(R.drawable.anim_load4), 300);
	animation.setOneShot(false);
	imageAnim = (ImageView) findViewById(R.id.waiting_anim);
	imageAnim.setVisibility(View.VISIBLE);
	if (Build.VERSION.SDK_INT < 16) {
	    imageAnim.setBackgroundDrawable(animation);
	}
	else {
	    imageAnim.setBackground(animation);
	}
	// start the animation!
	animation.start();
	Intent intentDom = new Intent(getApplicationContext(), Home.class);
	DomiciliazioneClientDetail.this.finish();
	startActivity(intentDom);
	overridePendingTransition(R.anim.fade, R.anim.hold);
    }
}
