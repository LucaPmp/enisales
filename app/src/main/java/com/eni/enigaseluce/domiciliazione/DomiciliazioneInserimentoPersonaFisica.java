package com.eni.enigaseluce.domiciliazione;

import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.http.cookie.Cookie;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.AnimationDrawable;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;

import com.eni.enigaseluce.R;
import com.eni.enigaseluce.error.OopsPageDomiciliazioneInserimento;
import com.eni.enigaseluce.error.OopsPageDomiciliazioneInserimentoHome;
import com.eni.enigaseluce.error.OopsPageSessioneScaduta;
import com.eni.enigaseluce.error.Utils;
import com.eni.enigaseluce.font.EniFont;
import com.eni.enigaseluce.httpcall.Constants;
import com.eni.enigaseluce.httpcall.ServiceCall;
import com.eni.enigaseluce.httpcall.ServiceCallAssembler;
import com.eni.enigaseluce.httpcall.ServiceCallFactory;
import com.eni.enigaseluce.model.InfoUtenzaEni;
import com.eni.enigaseluce.utils.CustomAlertDialog;
import com.eni.enigaseluce.utils.JSONUtility;
import com.eni.enigaseluce.utils.Utilities;

// questa classe gestisce l'inserimento dei dati per le utenze residenziali che sono persone fisiche
@SuppressLint("NewApi")
public class DomiciliazioneInserimentoPersonaFisica extends Activity {

    private Context context;
    private static Context staticContext;
    AnimationDrawable animation;
    ImageView imageAnim;
    String codiceConto;
    CheckBox matchIntestatari;

    private ServiceCall stAvDomFornServiceCall;
    private HashMap<String, String> parametriDomForn;

    private InfoUtenzaEni utenzaEni;

    public String rispostaLogin = Constants.SERVIZIO_LOGIN;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.domiciliazione_inserimento_persona_fisica);
	Intent intent = getIntent();

	codiceConto = intent.getStringExtra("CodiceConto");
	context = this;
	DomiciliazioneInserimentoPersonaFisica.staticContext = this;

	// create Eni user object to store parameters to send to the service
	utenzaEni = new InfoUtenzaEni();

	// click back button
	final ImageView domiciliazioneIndietro = (ImageView) findViewById(R.id.domiciliazione_inserimento_indietro);
	domiciliazioneIndietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
	domiciliazioneIndietro.getBackground().invalidateSelf();
	domiciliazioneIndietro.setOnClickListener(new View.OnClickListener() {
	    public void onClick(View v1) {
		domiciliazioneIndietro.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
		domiciliazioneIndietro.getBackground().invalidateSelf();
		animation = new AnimationDrawable();
		animation.addFrame(getResources().getDrawable(R.drawable.anim_load2), 300);
		animation.addFrame(getResources().getDrawable(R.drawable.anim_load3), 300);
		animation.addFrame(getResources().getDrawable(R.drawable.anim_load4), 300);
		animation.setOneShot(false);
		imageAnim = (ImageView) findViewById(R.id.waiting_anim);
		imageAnim.setVisibility(View.VISIBLE);
		if (Build.VERSION.SDK_INT < 16) {
		    imageAnim.setBackgroundDrawable(animation);
		}
		else {
		    imageAnim.setBackground(animation);
		}
		// start the animation!
		animation.start();
		Intent intentIndietro = new Intent(getApplicationContext(), DomiciliazioneActivateResidenziali.class);
		intentIndietro.putExtra("CodiceConto", getIntent().getStringExtra("CodiceConto"));
		intentIndietro.putExtra("TIPOLOGIA", getIntent().getStringExtra("TIPOLOGIA"));
		DomiciliazioneInserimentoPersonaFisica.this.finish();
		startActivity(intentIndietro);
		overridePendingTransition(R.anim.fade, R.anim.hold);
	    }
	});

	// click help button
	final ImageView domiciliazioneHelp = (ImageView) findViewById(R.id.domiciliazione_inserimento_help);
	domiciliazioneHelp.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
	domiciliazioneHelp.getBackground().invalidateSelf();
	domiciliazioneHelp.setOnClickListener(new View.OnClickListener() {
	    public void onClick(View v1) {
		domiciliazioneHelp.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
		domiciliazioneHelp.getBackground().invalidateSelf();
		animation = new AnimationDrawable();
		animation.addFrame(getResources().getDrawable(R.drawable.anim_load2), 300);
		animation.addFrame(getResources().getDrawable(R.drawable.anim_load3), 300);
		animation.addFrame(getResources().getDrawable(R.drawable.anim_load4), 300);
		animation.setOneShot(false);
		imageAnim = (ImageView) findViewById(R.id.waiting_anim);
		imageAnim.setVisibility(View.VISIBLE);
		if (Build.VERSION.SDK_INT < 16) {
		    imageAnim.setBackgroundDrawable(animation);
		}
		else {
		    imageAnim.setBackground(animation);
		}
		// start the animation!
		animation.start();
		Intent intentHelp = new Intent(getApplicationContext(), DomiciliazioneHelp.class);
		intentHelp.putExtra("ORIGINE", Constants.ORIGINE_DOMICILIAZIONE_INSERIMENTO_PF);
		intentHelp.putExtra("CodiceConto", codiceConto);
		intentHelp.putExtra("TIPOLOGIA", getIntent().getStringExtra("TIPOLOGIA"));
		DomiciliazioneInserimentoPersonaFisica.this.finish();
		startActivity(intentHelp);
		overridePendingTransition(R.anim.fade, R.anim.hold);
	    }
	});

	final EditText nome = (EditText) findViewById(R.id.domiciliazione_inserimento_nome);
	final EditText cognome = (EditText) findViewById(R.id.domiciliazione_inserimento_cognome);
	final EditText codicefiscale = (EditText) findViewById(R.id.domiciliazione_inserimento_codice_fiscale);
	final EditText iban = (EditText) findViewById(R.id.domiciliazione_inserimento_iban);
	iban.setEnabled(true);

	matchIntestatari = (CheckBox) findViewById(R.id.check_match_intestatari);

	// click "attiva domiciliazione" button
	final EniFont btn_attiva_domiciliazione = (EniFont) findViewById(R.id.domiciliazione_inserimento_button);
	btn_attiva_domiciliazione.setBackgroundResource(R.drawable.attiva_la_domiciliazione_disabled_bckgnd);
	btn_attiva_domiciliazione.setEnabled(false);
	btn_attiva_domiciliazione.setOnClickListener(new View.OnClickListener() {
	    public void onClick(View v1) {
		btn_attiva_domiciliazione.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
		btn_attiva_domiciliazione.getBackground().invalidateSelf();
		animation = new AnimationDrawable();
		animation.addFrame(getResources().getDrawable(R.drawable.anim_load2), 300);
		animation.addFrame(getResources().getDrawable(R.drawable.anim_load3), 300);
		animation.addFrame(getResources().getDrawable(R.drawable.anim_load4), 300);
		animation.setOneShot(false);
		imageAnim = (ImageView) findViewById(R.id.waiting_anim);
		imageAnim.setVisibility(View.VISIBLE);
		if (Build.VERSION.SDK_INT < 16) {
		    imageAnim.setBackgroundDrawable(animation);
		}
		else {
		    imageAnim.setBackground(animation);
		}
		// start the animation!
		animation.start();

		utenzaEni.setNomeClienteCC(nome.getText().toString().toUpperCase());
		utenzaEni.setCognomeClienteCC(cognome.getText().toString().toUpperCase());
		utenzaEni.setCodiceFiscaleClienteCC(codicefiscale.getText().toString().toUpperCase());
		utenzaEni.setIbanClienteCC(iban.getText().toString().toUpperCase());

		/************** Execute Domiciliazione Conto *************/
		chiamataEseguiDomiciliazione();
	    }
	});

	// second, we create the TextWatcher
	TextWatcher textWatcherNome = new TextWatcher() {
	    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

	    }

	    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

	    }

	    public void afterTextChanged(Editable editable) {

		if (!nome.getText().toString().trim().equals("") && !cognome.getText().toString().trim().equals("") && !codicefiscale.getText().toString().trim().equals("") && !iban.getText().toString().trim().equals("")) {

		    btn_attiva_domiciliazione.setBackgroundResource(R.drawable.attiva_la_domiciliazione_bckgnd);
		    btn_attiva_domiciliazione.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
		    btn_attiva_domiciliazione.getBackground().invalidateSelf();
		    btn_attiva_domiciliazione.setEnabled(true);

		}
		else {
		    btn_attiva_domiciliazione.setBackgroundResource(R.drawable.attiva_la_domiciliazione_disabled_bckgnd);
		    btn_attiva_domiciliazione.setEnabled(false);
		}
	    }
	};

	nome.addTextChangedListener(textWatcherNome);
	cognome.addTextChangedListener(textWatcherNome);
	codicefiscale.addTextChangedListener(textWatcherNome);
	iban.addTextChangedListener(textWatcherNome);

    }

    @Override
    protected void onResume() {
	super.onResume();

	final EniFont btn_attiva_domiciliazione = (EniFont) findViewById(R.id.domiciliazione_inserimento_button);
	btn_attiva_domiciliazione.getBackground().invalidateSelf();

	final ImageView domiciliazioneIndietro = (ImageView) findViewById(R.id.domiciliazione_inserimento_indietro);
	domiciliazioneIndietro.getBackground().invalidateSelf();
    }

    public void onCheckBoxClicked(View view) {

	final EditText nome = (EditText) findViewById(R.id.domiciliazione_inserimento_nome);
	final EditText cognome = (EditText) findViewById(R.id.domiciliazione_inserimento_cognome);
	final EditText codicefiscale = (EditText) findViewById(R.id.domiciliazione_inserimento_codice_fiscale);
	final EditText iban = (EditText) findViewById(R.id.domiciliazione_inserimento_iban);

	boolean checked = ((CheckBox) view).isChecked();

	switch (view.getId()) {
	case R.id.check_match_intestatari:

	    if (checked) {

		nome.setText(JSONUtility.getNomeCliente(rispostaLogin));
		nome.setEnabled(false);

		cognome.setText(JSONUtility.getCognomeCliente(rispostaLogin));
		cognome.setEnabled(false);

		boolean cFCorretto = JSONUtility.isCodiceFiscaleCorretto(rispostaLogin);

		if (cFCorretto) {
		    codicefiscale.setText(JSONUtility.getCodiceFiscaleCliente(rispostaLogin));
		    codicefiscale.setEnabled(false);

		    iban.requestFocus();
		}
		else {

		    CustomAlertDialog.showAlertDialogNoButton(context, getString(R.string.domiciliazione_alert_codice_fiscale), getString(R.string.spannable_cod_fis), R.layout.alert_view_domiciliazione);

		    codicefiscale.setText("");
		    codicefiscale.setEnabled(true);

		    codicefiscale.requestFocus();

		}
	    }
	    else {

		nome.setText("");
		nome.setEnabled(true);

		nome.requestFocus();

		cognome.setText("");
		cognome.setEnabled(true);

		codicefiscale.setText("");
		codicefiscale.setEnabled(true);

		iban.setText("");

	    }

	    break;
	}
    }

    public static Context getContext() {
	return DomiciliazioneInserimentoPersonaFisica.staticContext;
    }

    @Override
    protected void onStart() {
	super.onStart();

	Timer timer = new Timer();
	timer.schedule(new TimerTask() {
	    @Override
	    public void run() {
		InputMethodManager m = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
		m.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
	    }
	}, 300);

    }

    public void chiamataEseguiDomiciliazione() {
	animation = new AnimationDrawable();
	animation.addFrame(getResources().getDrawable(R.drawable.anim_load2), 300);
	animation.addFrame(getResources().getDrawable(R.drawable.anim_load3), 300);
	animation.addFrame(getResources().getDrawable(R.drawable.anim_load4), 300);
	animation.setOneShot(false);
	imageAnim = (ImageView) findViewById(R.id.waiting_anim);
	imageAnim.setVisibility(View.VISIBLE);
	if (Build.VERSION.SDK_INT < 16) {
	    imageAnim.setBackgroundDrawable(animation);
	}
	else {
	    imageAnim.setBackground(animation);
	}
	// start the animation!
	animation.start();

	utenzaEni.setCodiceCliente(JSONUtility.getCodiceCliente(rispostaLogin));
	utenzaEni.setCodiceConto(codiceConto);
	utenzaEni.setUserId(JSONUtility.getUserId(rispostaLogin));
	utenzaEni.setTipologia(JSONUtility.getTipologiaUtenza(rispostaLogin));
	utenzaEni.setTipoIntestatario(Constants.PFISICA);
	utenzaEni.setTelefonoPrimario(JSONUtility.getTelefonoPrimario(rispostaLogin));
	utenzaEni.setNomeCliente(JSONUtility.getNomeCliente(rispostaLogin));
	utenzaEni.setCognomeCliente(JSONUtility.getCognomeCliente(rispostaLogin));
	utenzaEni.setCodiceFiscaleCliente(JSONUtility.getCodiceFiscaleCliente(rispostaLogin));
	utenzaEni.setPartitaIvaCliente(JSONUtility.getPartitaIVACliente(rispostaLogin) != null ? JSONUtility.getPartitaIVACliente(rispostaLogin) : "");
	utenzaEni.setTipoIndirizzo(JSONUtility.getTipoIndirizzo(rispostaLogin));
	utenzaEni.setStrada(JSONUtility.getStrada(rispostaLogin));
	utenzaEni.setNumCivico(JSONUtility.getNumCivico(rispostaLogin));
	utenzaEni.setEstensioneNumCivico(JSONUtility.getEstensioneNumCivico(rispostaLogin));
	utenzaEni.setCitta(JSONUtility.getCitta(rispostaLogin));
	utenzaEni.setCap(JSONUtility.getCap(rispostaLogin));
	utenzaEni.setProvincia(JSONUtility.getProvincia(rispostaLogin));

	parametriDomForn = ServiceCallAssembler.fillParametersExecuteDomiciliazione(utenzaEni, matchIntestatari.isChecked() && !JSONUtility.isCodiceFiscaleCorretto(rispostaLogin) ? true : false, Constants.TIPOLOGIA_PERSONA_FISICA);

	if (!Utilities.isNetworkAvailable(context)) {
	    Intent intentOops = new Intent(getApplicationContext(), OopsPageDomiciliazioneInserimento.class);
	    intentOops.putExtra(Constants.MESSAGGIO, getResources().getString(R.string.connessione_assente));
	    intentOops.putExtra(Constants.TIPOLOGIA_UTENZA, "PERSONA_FISICA");
	    intentOops.putExtra(Constants.CODICE_CLIENTE, utenzaEni.getCodiceCliente());
	    intentOops.putExtra(Constants.CODICE_CONTO, utenzaEni.getCodiceConto());
	    intentOops.putExtra(Constants.CODICE_FISCALE_CLIENTE_CC, utenzaEni.getCodiceFiscaleClienteCC());
	    intentOops.putExtra(Constants.NOME_CLIENTE_CC, utenzaEni.getNomeClienteCC());
	    intentOops.putExtra(Constants.COGNOME_CLIENTE_CC, utenzaEni.getCognomeClienteCC());
	    intentOops.putExtra(Constants.IBAN_CLIENTE_CC, utenzaEni.getIbanClienteCC());
	    intentOops.putExtra(Constants.USER_ID, utenzaEni.getUserId());
	    intentOops.putExtra(Constants.NOME_CLIENTE, utenzaEni.getNomeCliente());
	    intentOops.putExtra(Constants.COGNOME_CLIENTE, utenzaEni.getCognomeCliente());
	    intentOops.putExtra(Constants.CODICE_FISCALE_CLIENTE, utenzaEni.getCodiceFiscaleCliente());
	    intentOops.putExtra(Constants.CF_MODIFICATO, utenzaEni.getCf_Modificato());

	    if (!utenzaEni.getTipoIndirizzo().equals("")) {
		intentOops.putExtra(Constants.TIPO_INDIRIZZO, utenzaEni.getTipoIndirizzo());
	    }
	    if (!utenzaEni.getStrada().equals("")) {
		intentOops.putExtra(Constants.STRADA, utenzaEni.getStrada());
	    }
	    if (!utenzaEni.getNumCivico().equals("")) {
		intentOops.putExtra(Constants.NUM_CIVICO, utenzaEni.getNumCivico());
	    }
	    if (!utenzaEni.getEstensioneNumCivico().equals("")) {
		intentOops.putExtra(Constants.ESTENSIONE_NUM_CIVICO, utenzaEni.getEstensioneNumCivico());
	    }
	    if (!utenzaEni.getCitta().equals("")) {
		intentOops.putExtra(Constants.CITTA, utenzaEni.getCitta());
	    }
	    if (!utenzaEni.getCap().equals("")) {
		intentOops.putExtra(Constants.CAP, utenzaEni.getCap());
	    }
	    if (!utenzaEni.getProvincia().equals("")) {
		intentOops.putExtra(Constants.PROVINCIA, utenzaEni.getProvincia());
	    }

	    // StatoAttivazioneFornitura.this.finish();
	    startActivity(intentOops);
	    if (animation != null) {
		animation.stop();
		imageAnim.setVisibility(View.INVISIBLE);
	    }

	    overridePendingTransition(R.anim.fade, R.anim.hold);
	}
	else {

	    /********************* Chiamata execute domiciliazione conto Fornitura ******************************/
	    stAvDomFornServiceCall = ServiceCallFactory.createServiceCall(context);

	    // Parametri obbligatori: codiceCliente, codiceConto,
	    // nomeClienteCC,cognomeClienteCC,codiceFiscaleClienteCC,ibanClienteCC,userId,
	    // nomeCliente,cognomeCliente,codiceFiscaleCliente
	    // parametri opzionali: tipoIndirizzo, strada, numCivico,
	    // estensioneNumCivico, citta, cap, provincia

	    try {

		if (matchIntestatari.isChecked()) stAvDomFornServiceCall.executeHttpsGetDomiciliazione(Constants.URL_EXECUTE_DOMICILIAZIONE_CONTO, null, parametriDomForn, DomiciliazioneInserimentoPersonaFisica.this, 1, null);
		else {

		    // TODO alert dialog per l'avviso che non si è intestatari
		    // del conto e che si risponderà in prima persona dei
		    // mancati pagamenti

		    Intent intentOops = new Intent(getApplicationContext(), OopsPageDomiciliazioneInserimento.class);
		    intentOops.putExtra(Constants.MESSAGGIO, getResources().getString(R.string.connessione_assente));
		    intentOops.putExtra(Constants.TIPOLOGIA_UTENZA, "PERSONA_FISICA");
		    intentOops.putExtra(Constants.CODICE_CLIENTE, utenzaEni.getCodiceCliente());
		    intentOops.putExtra(Constants.CODICE_CONTO, utenzaEni.getCodiceConto());
		    intentOops.putExtra(Constants.CODICE_FISCALE_CLIENTE_CC, utenzaEni.getCodiceFiscaleClienteCC());
		    intentOops.putExtra(Constants.NOME_CLIENTE_CC, utenzaEni.getNomeClienteCC());
		    intentOops.putExtra(Constants.COGNOME_CLIENTE_CC, utenzaEni.getCognomeClienteCC());
		    intentOops.putExtra(Constants.IBAN_CLIENTE_CC, utenzaEni.getIbanClienteCC());
		    intentOops.putExtra(Constants.USER_ID, utenzaEni.getUserId());
		    intentOops.putExtra(Constants.NOME_CLIENTE, utenzaEni.getNomeCliente());
		    intentOops.putExtra(Constants.COGNOME_CLIENTE, utenzaEni.getCognomeCliente());
		    intentOops.putExtra(Constants.CODICE_FISCALE_CLIENTE, utenzaEni.getCodiceFiscaleCliente());
		    intentOops.putExtra(Constants.CF_MODIFICATO, utenzaEni.getCf_Modificato());
		    if (!utenzaEni.getTipoIndirizzo().equals("")) {
			intentOops.putExtra(Constants.TIPO_INDIRIZZO, utenzaEni.getTipoIndirizzo());
		    }
		    if (!utenzaEni.getStrada().equals("")) {
			intentOops.putExtra(Constants.STRADA, utenzaEni.getStrada());
		    }
		    if (!utenzaEni.getNumCivico().equals("")) {
			intentOops.putExtra(Constants.NUM_CIVICO, utenzaEni.getNumCivico());
		    }
		    if (!utenzaEni.getEstensioneNumCivico().equals("")) {
			intentOops.putExtra(Constants.ESTENSIONE_NUM_CIVICO, utenzaEni.getEstensioneNumCivico());
		    }
		    if (!utenzaEni.getCitta().equals("")) {
			intentOops.putExtra(Constants.CITTA, utenzaEni.getCitta());
		    }
		    if (!utenzaEni.getCap().equals("")) {
			intentOops.putExtra(Constants.CAP, utenzaEni.getCap());
		    }
		    if (!utenzaEni.getProvincia().equals("")) {
			intentOops.putExtra(Constants.PROVINCIA, utenzaEni.getProvincia());
		    }

		    CustomAlertDialog.showAlertDialogWithButton(context, getString(R.string.domiciliazione_alert_intestatario), getString(R.string.spannable_confermi), R.layout.alert_view_button_domiciliazione, null, Constants.URL_EXECUTE_DOMICILIAZIONE_CONTO, parametriDomForn, DomiciliazioneInserimentoPersonaFisica.this, 1, null, intentOops, animation, imageAnim);
		}
	    }
	    catch (Exception e) {
		Intent intentOops = new Intent(getApplicationContext(), OopsPageDomiciliazioneInserimento.class);
		intentOops.putExtra(Constants.MESSAGGIO, getResources().getString(R.string.connessione_assente));
		intentOops.putExtra(Constants.TIPOLOGIA_UTENZA, "PERSONA_FISICA");
		intentOops.putExtra(Constants.CODICE_CLIENTE, utenzaEni.getCodiceCliente());
		intentOops.putExtra(Constants.CODICE_CONTO, utenzaEni.getCodiceConto());
		intentOops.putExtra(Constants.CODICE_FISCALE_CLIENTE_CC, utenzaEni.getCodiceFiscaleClienteCC());
		intentOops.putExtra(Constants.NOME_CLIENTE_CC, utenzaEni.getNomeClienteCC());
		intentOops.putExtra(Constants.COGNOME_CLIENTE_CC, utenzaEni.getCognomeClienteCC());
		intentOops.putExtra(Constants.IBAN_CLIENTE_CC, utenzaEni.getIbanClienteCC());
		intentOops.putExtra(Constants.USER_ID, utenzaEni.getUserId());
		intentOops.putExtra(Constants.NOME_CLIENTE, utenzaEni.getNomeCliente());
		intentOops.putExtra(Constants.COGNOME_CLIENTE, utenzaEni.getCognomeCliente());
		intentOops.putExtra(Constants.CODICE_FISCALE_CLIENTE, utenzaEni.getCodiceFiscaleCliente());
		intentOops.putExtra(Constants.CF_MODIFICATO, utenzaEni.getCf_Modificato());
		if (!utenzaEni.getTipoIndirizzo().equals("")) {
		    intentOops.putExtra(Constants.TIPO_INDIRIZZO, utenzaEni.getTipoIndirizzo());
		}
		if (!utenzaEni.getStrada().equals("")) {
		    intentOops.putExtra(Constants.STRADA, utenzaEni.getStrada());
		}
		if (!utenzaEni.getNumCivico().equals("")) {
		    intentOops.putExtra(Constants.NUM_CIVICO, utenzaEni.getNumCivico());
		}
		if (!utenzaEni.getEstensioneNumCivico().equals("")) {
		    intentOops.putExtra(Constants.ESTENSIONE_NUM_CIVICO, utenzaEni.getEstensioneNumCivico());
		}
		if (!utenzaEni.getCitta().equals("")) {
		    intentOops.putExtra(Constants.CITTA, utenzaEni.getCitta());
		}
		if (!utenzaEni.getCap().equals("")) {
		    intentOops.putExtra(Constants.CAP, utenzaEni.getCap());
		}
		if (!utenzaEni.getProvincia().equals("")) {
		    intentOops.putExtra(Constants.PROVINCIA, utenzaEni.getProvincia());
		}

		// DomiciliazioneInserimento.this.finish();
		startActivity(intentOops);
		overridePendingTransition(R.anim.fade, R.anim.hold);
	    }
	    // sorpassando la chiamata
	    /*
	     * AlertDialog.Builder builder = new AlertDialog.Builder(context);
	     * builder.setMessage(R.string.chiamata_OK).setCancelable(false).
	     * setPositiveButton("OK", new DialogInterface.OnClickListener() {
	     * public void onClick(DialogInterface dialog, int id) { //chiamata
	     * visualizza domiciliazione fornitura e vado alla schermata
	     * Domiciliazione chiamataVisualizzaDomiciliazione(); } });
	     * builder.create().show();
	     */
	}

    }

    public void postChiamata(String rispostaChiamata, List<Cookie> lista, int code) {
	if (animation != null) {
	    animation.stop();
	    imageAnim.setVisibility(View.INVISIBLE);
	}

	final ImageView domiciliazioneIndietro = (ImageView) findViewById(R.id.domiciliazione_inserimento_indietro);
	domiciliazioneIndietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
	domiciliazioneIndietro.getBackground().invalidateSelf();

	JSONArray rispostaJSON = new JSONArray();
	String esito = "";
	String descEsito = "";
	try {
	    rispostaJSON = new JSONArray(rispostaChiamata);
	    JSONObject oggettoRisposta = (JSONObject) rispostaJSON.get(0);
	    esito = oggettoRisposta.getString("esito");
	    descEsito = oggettoRisposta.getString("descEsito");
	}
	catch (JSONException e) {
	    e.printStackTrace();
	}
	if (esito.equals("200")) {
	    switch (code) {
	    case 1: // esecuzioneDomiciliazione PostChiamata

		AlertDialog.Builder builder = new AlertDialog.Builder(context);

		builder.setTitle(R.string.domiciliazione).setMessage(R.string.chiamata_OK).setCancelable(false).setPositiveButton("Chiudi", new DialogInterface.OnClickListener() {
		    public void onClick(DialogInterface dialog, int id) {
			// chiamata visualizza domiciliazione fornitura
			// e vado alla schermata Domiciliazione
			chiamataVisualizzaDomiciliazione();
		    }
		});

		builder.create().show();

		break;

	    case 2: // visualizza dettaglio conto PostChiamata
		JSONArray domicilLogin = null;
		try {
		    domicilLogin = new JSONArray(rispostaLogin);
		}
		catch (JSONException e) {
		    e.printStackTrace();
		}
		final String indirizzoFornituraIesimo = JSONUtility.cercaIndirizzoFornitura(domicilLogin, codiceConto);

		Constants.SERVIZIO_VISUALIZZA_FORNITURE_DOMICILIAZIONE = rispostaChiamata;
		Intent intentDomiciliazione = new Intent(getApplicationContext(), Domiciliazione.class);
		intentDomiciliazione.putExtra("CodiceConto", codiceConto);
		intentDomiciliazione.putExtra("IndirizzoFornitura", indirizzoFornituraIesimo);
		DomiciliazioneInserimentoPersonaFisica.this.finish();
		startActivity(intentDomiciliazione);
		overridePendingTransition(R.anim.fade, R.anim.hold);

		break;
	    }
	}
	else {
	    Intent intentOops = new Intent();
	    // prepare default opps page about both scenario
	    // esecuzioneDomiciliazione and visualizzaDettaglio conto
	    switch (code) {
	    case 1:
		intentOops = new Intent(getApplicationContext(), OopsPageDomiciliazioneInserimento.class);
		intentOops.putExtra(Constants.MESSAGGIO, getResources().getString(R.string.connessione_assente));
		intentOops.putExtra(Constants.TIPOLOGIA_UTENZA, "PERSONA_FISICA");
		intentOops.putExtra(Constants.CODICE_CLIENTE, utenzaEni.getCodiceCliente());
		intentOops.putExtra(Constants.CODICE_CONTO, utenzaEni.getCodiceConto());
		intentOops.putExtra(Constants.CODICE_FISCALE_CLIENTE_CC, utenzaEni.getCodiceFiscaleClienteCC());
		intentOops.putExtra(Constants.NOME_CLIENTE_CC, utenzaEni.getNomeClienteCC());
		intentOops.putExtra(Constants.COGNOME_CLIENTE_CC, utenzaEni.getCognomeClienteCC());
		intentOops.putExtra(Constants.IBAN_CLIENTE_CC, utenzaEni.getIbanClienteCC());
		intentOops.putExtra(Constants.USER_ID, utenzaEni.getUserId());
		intentOops.putExtra(Constants.NOME_CLIENTE, utenzaEni.getNomeCliente());
		intentOops.putExtra(Constants.COGNOME_CLIENTE, utenzaEni.getCognomeCliente());
		intentOops.putExtra(Constants.CODICE_FISCALE_CLIENTE, utenzaEni.getCodiceFiscaleCliente());
		intentOops.putExtra(Constants.CF_MODIFICATO, utenzaEni.getCf_Modificato());
		if (!utenzaEni.getTipoIndirizzo().equals("")) {
		    intentOops.putExtra(Constants.TIPO_INDIRIZZO, utenzaEni.getTipoIndirizzo());
		}
		if (!utenzaEni.getStrada().equals("")) {
		    intentOops.putExtra(Constants.STRADA, utenzaEni.getStrada());
		}
		if (!utenzaEni.getNumCivico().equals("")) {
		    intentOops.putExtra(Constants.NUM_CIVICO, utenzaEni.getNumCivico());
		}
		if (!utenzaEni.getEstensioneNumCivico().equals("")) {
		    intentOops.putExtra(Constants.ESTENSIONE_NUM_CIVICO, utenzaEni.getEstensioneNumCivico());
		}
		if (!utenzaEni.getCitta().equals("")) {
		    intentOops.putExtra(Constants.CITTA, utenzaEni.getCitta());
		}
		if (!utenzaEni.getCap().equals("")) {
		    intentOops.putExtra(Constants.CAP, utenzaEni.getCap());
		}
		if (!utenzaEni.getProvincia().equals("")) {
		    intentOops.putExtra(Constants.PROVINCIA, utenzaEni.getProvincia());
		}

		break;
	    case 2:
		intentOops = new Intent(getApplicationContext(), OopsPageDomiciliazioneInserimentoHome.class);
		JSONArray array;
		String parametri = "";
		try {
		    array = new JSONArray(rispostaLogin);
		    String arrayCodiciConti[] = elencoCodiciContiAttiviCompleti(array);
		    for (int i = 0; i < arrayCodiciConti.length; i++) {
			parametri = parametri + "&codiciContoCliente=" + arrayCodiciConti[i];
		    }
		}
		catch (JSONException e1) {
		    e1.printStackTrace();
		}
		intentOops.putExtra("Parametri", parametri);
	    }

	    // errori ops page
	    if (esito.equals("309")) {
		intentOops.putExtra(Constants.MESSAGGIO, getResources().getString(R.string.errore_generico_opspage));
		startActivityForResult(intentOops, OopsPageDomiciliazioneInserimento.ESCI);
		overridePendingTransition(R.anim.fade, R.anim.hold);

	    }
	    // else if (esito.equals("400"))
	    // {
	    // intentOops.putExtra(Constants.MESSAGGIO,
	    // getResources().getString(R.string.errore_generico_opspage));
	    // startActivityForResult(intentOops,
	    // OopsPageDomiciliazioneInserimento.ESCI);
	    // overridePendingTransition(R.anim.fade, R.anim.hold);
	    //
	    // } else if (esito.equals("420"))
	    // {
	    // intentOops.putExtra(Constants.MESSAGGIO,
	    // getResources().getString(R.string.errore_generico_opspage));
	    // startActivityForResult(intentOops,
	    // OopsPageDomiciliazioneInserimento.ESCI);
	    // overridePendingTransition(R.anim.fade, R.anim.hold);
	    //
	    // } else if (esito.equals("449"))
	    // {
	    // intentOops.putExtra(Constants.MESSAGGIO,
	    // getResources().getString(R.string.errore_generico_opspage));
	    // startActivityForResult(intentOops,
	    // OopsPageDomiciliazioneInserimento.ESCI);
	    // overridePendingTransition(R.anim.fade, R.anim.hold);
	    //
	    // } else if (esito.equals("451"))
	    // {
	    // intentOops.putExtra(Constants.MESSAGGIO,
	    // getResources().getString(R.string.errore_popup_451));
	    // startActivityForResult(intentOops,
	    // OopsPageDomiciliazioneInserimento.ESCI);
	    // overridePendingTransition(R.anim.fade, R.anim.hold);
	    // }

	    // check applicative error codes 4xx family retrieving error codes
	    // from resources
	    else if (Utilities.arrayContainsString(getResources().getStringArray(R.array.execute_domiciliazione_error_codes), esito)) {
		Utils.popup(DomiciliazioneInserimentoPersonaFisica.this, descEsito, "Riprova");
	    }

	    // // errori popup
	    // else if (esito.equals("307"))
	    // {
	    // Utils.popup(DomiciliazioneInserimentoPersonaFisica.this,
	    // getResources().getString(R.string.errore_popup_307), "Riprova");
	    // } else if (esito.equals("435"))
	    // {
	    // Utils.popup(DomiciliazioneInserimentoPersonaFisica.this,
	    // getResources().getString(R.string.errore_popup_435), "Riprova");
	    // } else if (esito.equals("436"))
	    // {
	    // Utils.popup(DomiciliazioneInserimentoPersonaFisica.this,
	    // getResources().getString(R.string.errore_popup_436), "Riprova");
	    // } else if (esito.equals("437"))
	    // {
	    // Utils.popup(DomiciliazioneInserimentoPersonaFisica.this,
	    // getResources().getString(R.string.errore_popup_437), "Riprova");
	    // } else if (esito.equals("438"))
	    // {
	    // Utils.popup(DomiciliazioneInserimentoPersonaFisica.this,
	    // getResources().getString(R.string.errore_popup_438), "Riprova");
	    // } else if (esito.equals("443"))
	    // {
	    // Utils.popup(DomiciliazioneInserimentoPersonaFisica.this,
	    // getResources().getString(R.string.errore_popup_443), "Riprova");
	    // } else if (esito.equals("444"))
	    // {
	    // Utils.popup(DomiciliazioneInserimentoPersonaFisica.this,
	    // getResources().getString(R.string.errore_popup_444), "Riprova");
	    // } else if (esito.equals("445"))
	    // {
	    // Utils.popup(DomiciliazioneInserimentoPersonaFisica.this,
	    // getResources().getString(R.string.errore_popup_445), "Riprova");
	    // } else if (esito.equals("446"))
	    // {
	    // Utils.popup(DomiciliazioneInserimentoPersonaFisica.this,
	    // getResources().getString(R.string.errore_popup_446), "Riprova");
	    // } else if (esito.equals("447"))
	    // {
	    // Utils.popup(DomiciliazioneInserimentoPersonaFisica.this,
	    // getResources().getString(R.string.errore_popup_447), "Riprova");
	    // } else if (esito.equals("448"))
	    // {
	    // Utils.popup(DomiciliazioneInserimentoPersonaFisica.this,
	    // getResources().getString(R.string.errore_popup_307), "Riprova");
	    // }

	    else {
	    	if (descEsito.equals("")) {
				intentOops = new Intent(getApplicationContext(), OopsPageSessioneScaduta.class);
				intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.sessione_scaduta));
			} else{
				intentOops.putExtra("MESSAGGIO", descEsito);
			}
			
			startActivity(intentOops);
			overridePendingTransition(R.anim.fade, R.anim.hold);
	    }

	    final EniFont button = (EniFont) findViewById(R.id.domiciliazione_inserimento_button);
	    button.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
	    button.getBackground().invalidateSelf();
	}

    }

    public void postChiamataException(boolean exception) {
	if (exception) {
	    if (animation != null) {
		animation.stop();
		imageAnim.setVisibility(View.INVISIBLE);
	    }

	    final ImageView domiciliazioneIndietro = (ImageView) findViewById(R.id.domiciliazione_inserimento_indietro);
	    domiciliazioneIndietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
	    domiciliazioneIndietro.getBackground().invalidateSelf();

	    Intent intentOops = new Intent(getApplicationContext(), OopsPageDomiciliazioneInserimento.class);
	    intentOops.putExtra(Constants.MESSAGGIO, getResources().getString(R.string.servizio_non_disponibile));
	    // DomiciliazioneInseriemento.this.finish();
	    startActivityForResult(intentOops, OopsPageDomiciliazioneInserimento.ESCI);
	    overridePendingTransition(R.anim.fade, R.anim.hold);
	}
    }

    public void chiamataVisualizzaDomiciliazione() {
	animation = new AnimationDrawable();
	animation.addFrame(getResources().getDrawable(R.drawable.anim_load2), 300);
	animation.addFrame(getResources().getDrawable(R.drawable.anim_load3), 300);
	animation.addFrame(getResources().getDrawable(R.drawable.anim_load4), 300);
	animation.setOneShot(false);
	imageAnim = (ImageView) findViewById(R.id.waiting_anim);
	imageAnim.setVisibility(View.VISIBLE);
	if (Build.VERSION.SDK_INT < 16) {
	    imageAnim.setBackgroundDrawable(animation);
	}
	else {
	    imageAnim.setBackground(animation);
	}
	// start the animation!
	animation.start();
	if (!Utilities.isNetworkAvailable(context)) {
	    Intent intentOops = new Intent(getApplicationContext(), OopsPageDomiciliazioneInserimentoHome.class);
	    intentOops.putExtra(Constants.MESSAGGIO, getResources().getString(R.string.connessione_assente));
	    // DomiciliazioneInserimento.this.finish();
	    startActivity(intentOops);
	    overridePendingTransition(R.anim.fade, R.anim.hold);
	}
	else {
	    /********************* Chiamata visualizza domiciliazione Fornitura ******************************/
	    // ServiceCall stAvFornServiceCall =
	    // ServiceCallFactory.createServiceCall(context);

	    ServiceCall visFornDomicilServiceCall = ServiceCallFactory.createServiceCall(context);

	    JSONArray array;
	    String parametri = "";
	    try {
		array = new JSONArray(rispostaLogin);
		String arrayCodiciConti[] = elencoCodiciContiAttiviCompleti(array);
		for (int i = 0; i < arrayCodiciConti.length; i++) {
		    parametri = parametri + "&codiciContoCliente=" + arrayCodiciConti[i];
		}
	    }
	    catch (JSONException e1) {
		e1.printStackTrace();
	    }
	    try {
		visFornDomicilServiceCall.executeHttpsGetDomiciliazione(Constants.URL_VISUALIZZA_FORNITURE_DOMICILIAZIONE, parametri, null, DomiciliazioneInserimentoPersonaFisica.this, 2, null);
	    }
	    catch (Exception e) {
		Intent intentOops = new Intent(getApplicationContext(), OopsPageDomiciliazioneInserimentoHome.class);
		intentOops.putExtra(Constants.MESSAGGIO, getResources().getString(R.string.servizio_non_disponibile));
		// DomiciliazioneInserimento.this.finish();
		startActivity(intentOops);
		overridePendingTransition(R.anim.fade, R.anim.hold);
	    }
	    // sorpassando lo statoAvanzamentoForniture
	    /*
	     * Intent intentDomiciliazione = new Intent(getApplicationContext(),
	     * Domiciliazione.class); DomiciliazioneInserimento.this.finish();
	     * startActivity(intentDomiciliazione);
	     * overridePendingTransition(R.anim.fade, R.anim.hold);
	     */
	}

    }

    public String[] elencoCodiciContiAttiviCompleti(JSONArray array) {
	int lunghezza = numeroCodiciContoDiversiAttiviCompleti(array);
	String result[] = new String[lunghezza];
	String codiceContoConfrontato = "";
	String codiceContoIesimo = "";
	String codiciConcatenati = "";
	int count = 0;
	for (int i = 2; i < array.length(); i++) {
	    JSONArray contoIesimo;
	    try {
		contoIesimo = array.getJSONArray(i);
		// codice conto
		JSONObject anagrContoIesimo = contoIesimo.getJSONObject(0);
		codiceContoIesimo = anagrContoIesimo.getString("CodiceConto");

		if (!codiceContoIesimo.equals(codiceContoConfrontato)) {
		    String stato = anagrContoIesimo.getString("StatoConto");
		    if (!stato.equals("UTILIZZABILE")) {
			continue;
		    }
		    else {
			// codice conto utilizzabile diverso, controllo non sia
			// vuoto
			if (contoIesimo.length() > 1) {
			    // che non sia gia' presente
			    boolean isPresente = codiciConcatenati.contains("-" + codiceContoIesimo + "-");
			    if (isPresente) {
				continue;
			    }
			    else {
				codiceContoConfrontato = codiceContoIesimo;
				result[count] = codiceContoConfrontato;
				codiciConcatenati = codiciConcatenati + "-" + codiceContoIesimo + "-";
				count++;
			    }
			}
			else {
			    continue;
			}
		    }
		}
	    }
	    catch (JSONException e) {
		e.printStackTrace();
	    }
	}
	return result;
    }

    public int numeroCodiciContoDiversiAttiviCompleti(JSONArray array) {
	int result = 0;
	String codiceContoConfrontato = "";
	String codiceContoIesimo = "";
	String codiciConcatenati = "";
	for (int i = 2; i < array.length(); i++) {
	    JSONArray contoIesimo;
	    try {
		contoIesimo = array.getJSONArray(i);
		// codice conto
		JSONObject anagrContoIesimo = contoIesimo.getJSONObject(0);
		codiceContoIesimo = anagrContoIesimo.getString("CodiceConto");

		if (!codiceContoIesimo.equals(codiceContoConfrontato)) {
		    String stato = anagrContoIesimo.getString("StatoConto");
		    if (!stato.equals("UTILIZZABILE")) {
			continue;
		    }
		    else {
			// codice conto utilizzabile diverso, controllo non sia
			// vuoto
			if (contoIesimo.length() > 1) {
			    if (!codiciConcatenati.contains("-" + codiceContoIesimo)) {
				// codice conto diverso
				codiceContoConfrontato = codiceContoIesimo;
				codiciConcatenati = codiciConcatenati + "-" + codiceContoIesimo;
				result++;
			    }
			}
			else {
			    continue;
			}
		    }
		}
	    }
	    catch (JSONException e) {
		e.printStackTrace();
	    }
	}
	return result;
    }

    public void onBackPressed() {
	animation = new AnimationDrawable();
	animation.addFrame(getResources().getDrawable(R.drawable.anim_load2), 300);
	animation.addFrame(getResources().getDrawable(R.drawable.anim_load3), 300);
	animation.addFrame(getResources().getDrawable(R.drawable.anim_load4), 300);
	animation.setOneShot(false);
	imageAnim = (ImageView) findViewById(R.id.waiting_anim);
	imageAnim.setVisibility(View.VISIBLE);
	if (Build.VERSION.SDK_INT < 16) {
	    imageAnim.setBackgroundDrawable(animation);
	}
	else {
	    imageAnim.setBackground(animation);
	}
	// start the animation!
	animation.start();
	Intent intentIndietro = new Intent(getApplicationContext(), DomiciliazioneActivateResidenziali.class);
	intentIndietro.putExtra("CodiceConto", getIntent().getStringExtra("CodiceConto"));
	intentIndietro.putExtra("TIPOLOGIA", getIntent().getStringExtra("TIPOLOGIA"));
	DomiciliazioneInserimentoPersonaFisica.this.finish();
	startActivity(intentIndietro);
	overridePendingTransition(R.anim.fade, R.anim.hold);
    }
}
