package com.eni.enigaseluce.domiciliazione;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.AnimationDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.eni.enigaseluce.R;
import com.eni.enigaseluce.font.EniFont;
import com.eni.enigaseluce.httpcall.Constants;

// questa classe farà riferimento alla tipologia di utenza NON residenziale
@SuppressLint("NewApi")
public class DomiciliazioneActivateNonResidenziali extends Activity {

    AnimationDrawable animation;
    ImageView imageAnim;
    String codiceConto;
    String tipologia;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.attiva_domiciliazione);

	Intent intent = getIntent();
	codiceConto = intent.getStringExtra("CodiceConto");
	tipologia = intent.getStringExtra("TIPOLOGIA");

	final ImageView domiciliazioneIndietro = (ImageView) findViewById(R.id.domiciliazione_attiva_indietro);
	domiciliazioneIndietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
	domiciliazioneIndietro.getBackground().invalidateSelf();
	domiciliazioneIndietro.setOnClickListener(new View.OnClickListener() {
	    public void onClick(View v1) {
		domiciliazioneIndietro.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
		domiciliazioneIndietro.getBackground().invalidateSelf();
		animation = new AnimationDrawable();
		animation.addFrame(getResources().getDrawable(R.drawable.anim_load2), 300);
		animation.addFrame(getResources().getDrawable(R.drawable.anim_load3), 300);
		animation.addFrame(getResources().getDrawable(R.drawable.anim_load4), 300);
		animation.setOneShot(false);
		imageAnim = (ImageView) findViewById(R.id.waiting_anim);
		imageAnim.setVisibility(View.VISIBLE);
		if (Build.VERSION.SDK_INT < 16) {
		    imageAnim.setBackgroundDrawable(animation);
		}
		else {
		    imageAnim.setBackground(animation);
		}
		// start the animation!
		animation.start();
		Intent intentIndietro = new Intent(getApplicationContext(), Domiciliazione.class);
		DomiciliazioneActivateNonResidenziali.this.finish();
		startActivity(intentIndietro);
		overridePendingTransition(R.anim.fade, R.anim.hold);
	    }
	});

	final ImageView domiciliazioneHelp = (ImageView) findViewById(R.id.domiciliazione_attiva_help);
	domiciliazioneHelp.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
	domiciliazioneHelp.getBackground().invalidateSelf();
	domiciliazioneHelp.setOnClickListener(new View.OnClickListener() {
	    public void onClick(View v1) {
		domiciliazioneHelp.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
		domiciliazioneHelp.getBackground().invalidateSelf();
		animation = new AnimationDrawable();
		animation.addFrame(getResources().getDrawable(R.drawable.anim_load2), 300);
		animation.addFrame(getResources().getDrawable(R.drawable.anim_load3), 300);
		animation.addFrame(getResources().getDrawable(R.drawable.anim_load4), 300);
		animation.setOneShot(false);
		imageAnim = (ImageView) findViewById(R.id.waiting_anim);
		imageAnim.setVisibility(View.VISIBLE);
		if (Build.VERSION.SDK_INT < 16) {
		    imageAnim.setBackgroundDrawable(animation);
		}
		else {
		    imageAnim.setBackground(animation);
		}
		// start the animation!
		animation.start();
		Intent intentHelp = new Intent(getApplicationContext(), DomiciliazioneHelp.class);
		intentHelp.putExtra("ORIGINE", Constants.ORIGINE_DOMICILIAZIONE_ACTIVATE_NON_RESIDENZIALE);
		intentHelp.putExtra("CodiceConto", codiceConto);
		intentHelp.putExtra("TIPOLOGIA", tipologia);
		DomiciliazioneActivateNonResidenziali.this.finish();
		startActivity(intentHelp);
		overridePendingTransition(R.anim.fade, R.anim.hold);
	    }
	});

	final EniFont domiciliazioneAttiva = (EniFont) findViewById(R.id.attiva_domiciliazione_button);
	domiciliazioneAttiva.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
	domiciliazioneAttiva.getBackground().invalidateSelf();
	domiciliazioneAttiva.setOnClickListener(new View.OnClickListener() {
	    public void onClick(View v1) {
		domiciliazioneAttiva.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
		domiciliazioneAttiva.getBackground().invalidateSelf();
		animation = new AnimationDrawable();
		animation.addFrame(getResources().getDrawable(R.drawable.anim_load2), 300);
		animation.addFrame(getResources().getDrawable(R.drawable.anim_load3), 300);
		animation.addFrame(getResources().getDrawable(R.drawable.anim_load4), 300);
		animation.setOneShot(false);
		imageAnim = (ImageView) findViewById(R.id.waiting_anim);
		imageAnim.setVisibility(View.VISIBLE);
		if (Build.VERSION.SDK_INT < 16) {
		    imageAnim.setBackgroundDrawable(animation);
		}
		else {
		    imageAnim.setBackground(animation);
		}
		// start the animation!
		animation.start();
		Intent intentInserimento = new Intent(getApplicationContext(), DomiciliazioneInserimentoNonResidenziali.class);
		intentInserimento.putExtra("CodiceConto", codiceConto);
		intentInserimento.putExtra("TIPOLOGIA", tipologia);
		DomiciliazioneActivateNonResidenziali.this.finish();
		startActivity(intentInserimento);
		overridePendingTransition(R.anim.fade, R.anim.hold);
	    }
	});

	TextView numeroCliente = (TextView) findViewById(R.id.text_numero_cliente_attiva_domiciliazione);
	numeroCliente.setText(codiceConto);

	ImageView iconaLuce = (ImageView) findViewById(R.id.domiciliazione_luce_icona);
	ImageView iconaGas = (ImageView) findViewById(R.id.domiciliazione_gas_icona);
	if (tipologia.equals("DUAL")) {
	    iconaLuce.setBackgroundResource(R.drawable.light_yes);
	    iconaGas.setBackgroundResource(R.drawable.gas_yes);
	}
	else if (tipologia.equals("LUCE")) {
	    iconaLuce.setBackgroundResource(R.drawable.light_yes);
	    iconaGas.setBackgroundResource(R.drawable.gas_no);
	}
	else {
	    iconaLuce.setBackgroundResource(R.drawable.light_no);
	    iconaGas.setBackgroundResource(R.drawable.gas_yes);
	}
    }

    public void onBackPressed() {
	animation = new AnimationDrawable();
	animation.addFrame(getResources().getDrawable(R.drawable.anim_load2), 300);
	animation.addFrame(getResources().getDrawable(R.drawable.anim_load3), 300);
	animation.addFrame(getResources().getDrawable(R.drawable.anim_load4), 300);
	animation.setOneShot(false);
	imageAnim = (ImageView) findViewById(R.id.waiting_anim);
	imageAnim.setVisibility(View.VISIBLE);
	if (Build.VERSION.SDK_INT < 16) {
	    imageAnim.setBackgroundDrawable(animation);
	}
	else {
	    imageAnim.setBackground(animation);
	}
	// start the animation!
	animation.start();
	Intent intentIndietro = new Intent(getApplicationContext(), Domiciliazione.class);
	DomiciliazioneActivateNonResidenziali.this.finish();
	startActivity(intentIndietro);
	overridePendingTransition(R.anim.fade, R.anim.hold);
    }
}
