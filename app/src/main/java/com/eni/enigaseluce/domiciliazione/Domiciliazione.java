package com.eni.enigaseluce.domiciliazione;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.AnimationDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.eni.enigaseluce.EniApplication;
import com.eni.enigaseluce.R;
import com.eni.enigaseluce.home.Home;
import com.eni.enigaseluce.httpcall.Constants;
import com.eni.enigaseluce.pager.HorizontalPager;
import com.eni.enigaseluce.utils.JSONUtility;

@SuppressLint("NewApi")
public class Domiciliazione extends Activity {
    AnimationDrawable animation;
    ImageView imageAnim;
    /**
     * Called when the activity is first created.
     */

    public String rispostaLogin = Constants.SERVIZIO_LOGIN;
    public String rispostaDomiciliazione = Constants.SERVIZIO_VISUALIZZA_FORNITURE_DOMICILIAZIONE;
    String codiceEsito;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.domiciliazione);

        ((EniApplication) getApplication()).sendAnalyticsEvent("[Android] - domiciliazione");

        final ImageView domiciliazioneIndietro = (ImageView) findViewById(R.id.domiciliazione_indietro);
        domiciliazioneIndietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
        domiciliazioneIndietro.getBackground().invalidateSelf();
        domiciliazioneIndietro.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v1) {
                domiciliazioneIndietro.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
                domiciliazioneIndietro.getBackground().invalidateSelf();
                animation = new AnimationDrawable();
                animation.addFrame(getResources().getDrawable(R.drawable.anim_load2), 300);
                animation.addFrame(getResources().getDrawable(R.drawable.anim_load3), 300);
                animation.addFrame(getResources().getDrawable(R.drawable.anim_load4), 300);
                animation.setOneShot(false);
                imageAnim = (ImageView) findViewById(R.id.waiting_anim);
                imageAnim.setVisibility(View.VISIBLE);
                if (Build.VERSION.SDK_INT < 16) {
                    if (Build.VERSION.SDK_INT < 16) {
                        imageAnim.setBackgroundDrawable(animation);
                    } else {
                        imageAnim.setBackground(animation);
                    }
                } else {
                    imageAnim.setBackground(animation);
                }
                // start the animation!
                animation.start();
                Intent intentHome = new Intent(getApplicationContext(), Home.class);
                Domiciliazione.this.finish();
                startActivity(intentHome);
                overridePendingTransition(R.anim.fade, R.anim.hold);
            }
        });

        final ImageView domiciliazioneHelp = (ImageView) findViewById(R.id.domiciliazione_help);
        domiciliazioneHelp.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
        domiciliazioneHelp.getBackground().invalidateSelf();
        domiciliazioneHelp.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v1) {
                domiciliazioneHelp.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
                domiciliazioneHelp.getBackground().invalidateSelf();
                animation = new AnimationDrawable();
                animation.addFrame(getResources().getDrawable(R.drawable.anim_load2), 300);
                animation.addFrame(getResources().getDrawable(R.drawable.anim_load3), 300);
                animation.addFrame(getResources().getDrawable(R.drawable.anim_load4), 300);
                animation.setOneShot(false);
                imageAnim = (ImageView) findViewById(R.id.waiting_anim);
                imageAnim.setVisibility(View.VISIBLE);
                if (Build.VERSION.SDK_INT < 16) {
                    imageAnim.setBackgroundDrawable(animation);
                } else {
                    imageAnim.setBackground(animation);
                }
                // start the animation!
                animation.start();
                Intent intentHome = new Intent(getApplicationContext(), DomiciliazioneHelp.class);
                intentHome.putExtra("ORIGINE", Constants.ORIGINE_DOMICILIAZIONE);
                Domiciliazione.this.finish();
                startActivity(intentHome);
                overridePendingTransition(R.anim.fade, R.anim.hold);
            }
        });
        final HorizontalPager domiciliazione_pager = (HorizontalPager) findViewById(R.id.domiciliazione_pager);
        domiciliazione_pager.removeAllViews();
        // RelativeLayout domiciliazione_page =
        // (RelativeLayout)findViewById(R.id.domiciliazione_page);
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);

        try {
            final JSONArray domicilLogin = new JSONArray(rispostaLogin);
            JSONObject esito = domicilLogin.getJSONObject(0);
            codiceEsito = (String) esito.getString("esito");
            if (codiceEsito.equals("200")) {
                JSONObject datiAnagr = domicilLogin.getJSONObject(1);
                // ultimo accesso
                String ultimoaccesso = datiAnagr.getString("UltimoAccesso");
                TextView ultimoAccesso = (TextView) findViewById(R.id.ultimo_accesso);
                // ultimoAccesso.setTextSize(11);
                ultimoAccesso.setText(ultimoaccesso);
                // nome cliente
                String nomeClienteString = datiAnagr.getString("NomeCliente");
                String cognomeClienteString = datiAnagr.getString("CognomeCliente");
                TextView nomeCliente = (TextView) findViewById(R.id.nome_cliente);
                String nomeInteroCliente = nomeClienteString + " " + cognomeClienteString;
                nomeCliente.setText(nomeInteroCliente);

                JSONArray arrayDomiciliazione = new JSONArray(rispostaDomiciliazione);

                int conti = arrayDomiciliazione.length() - 1;
                String[] elencoContiDomic = JSONUtility.elencoCodiciConti(arrayDomiciliazione, conti);

                int top;
                View domiciliazione_pagina = (View) inflater.inflate(R.layout.domiciliazione_pagina, null);
                RelativeLayout domiciliazione_page = new RelativeLayout(this);
                domiciliazione_page.removeAllViews();
                for (int i = 0; i < conti; i++) {
                    if ((i % 2) == 0) {
                        top = 0;
                        domiciliazione_page = new RelativeLayout(this);
                        RelativeLayout.LayoutParams param_page = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                        param_page.setMargins(0, 250, 0, 0);
                        domiciliazione_page.setLayoutParams(param_page);
                    } else {
                        top = 1;
                    }
                    domiciliazione_pagina = (View) inflater.inflate(R.layout.domiciliazione_pagina, null);
                    View account_domiciliazione = inflater.inflate(R.layout.account_domiciliazione, null);

                    RelativeLayout.LayoutParams param = null;
                    param = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

                    param.setMargins((int) getResources().getDimension(R.dimen.domiciliazione_entry_margin_rightLeft), (int) (1.05 * top * (int) getResources().getDimension(R.dimen.height_domiciliazione) + ((int) getResources().getDimension(R.dimen.top_domiciliazione))), (int) getResources().getDimension(R.dimen.domiciliazione_entry_margin_rightLeft), 0);

                    account_domiciliazione.setLayoutParams(param);

                    TextView numero_cliente_iesimo = (TextView) account_domiciliazione.findViewById(R.id.number_client_iesimo);
                    final String codiceConto = elencoContiDomic[i];
                    numero_cliente_iesimo.setText(codiceConto);
                    // nome cliente
                    TextView client_iesimo = (TextView) account_domiciliazione.findViewById(R.id.name_client_iesimo);
                    client_iesimo.setText(nomeInteroCliente);

                    final String indirizzoFornituraIesimo = JSONUtility.cercaIndirizzoFornitura(domicilLogin, codiceConto);
                    TextView indirizzo_fornitura = (TextView) account_domiciliazione.findViewById(R.id.indirizzo_fornitura_iesimo);
                    indirizzo_fornitura.setText(indirizzoFornituraIesimo);

                    final ImageView luce_icona = (ImageView) account_domiciliazione.findViewById(R.id.domiciliazione_iesima_luce_icona);
                    final ImageView gas_icona = (ImageView) account_domiciliazione.findViewById(R.id.domiciliazione_iesima_gas_icona);
                    boolean isDual = JSONUtility.isDual(domicilLogin, codiceConto);
                    if (isDual) {
                        luce_icona.setImageResource(R.drawable.light_yes);
                        gas_icona.setImageResource(R.drawable.gas_yes);
                    } else {
                        String tipologia = JSONUtility.cercaTipologiaFornitura(domicilLogin, codiceConto);
                        if (tipologia.equals("POWER")) {
                            luce_icona.setImageResource(R.drawable.light_yes);
                            gas_icona.setImageResource(R.drawable.gas_no);
                        } else {
                            luce_icona.setImageResource(R.drawable.light_no);
                            gas_icona.setImageResource(R.drawable.gas_yes);
                        }
                    }

                    String statoDomiciliazioneIesima = JSONUtility.cercaStato(arrayDomiciliazione, codiceConto);
                    TextView stato_domiciliazione_iesima = (TextView) account_domiciliazione.findViewById(R.id.stato_domiciliazione_iesima_descricrizione);
                    ImageView stato_domiciliazione_dot = (ImageView) account_domiciliazione.findViewById(R.id.stato_domiciliazione_iesima);
                    // verifica se la domiciliazione non � attiva in tal caso
                    // va nella pagina di attivazione altrimenti va nella pagina
                    // dei dettagli:
                    if (statoDomiciliazioneIesima.equals("ATTIVA") || statoDomiciliazioneIesima.equals("APPROVATA")) {
                        // green
                        stato_domiciliazione_iesima.setText(R.string.domiciliazione_attiva);
                        stato_domiciliazione_iesima.setTextColor(getResources().getColor(R.color.green1));
                        stato_domiciliazione_dot.setImageResource(R.drawable.green_dot);

                    } else if (statoDomiciliazioneIesima.equals("ANNULLATA") || statoDomiciliazioneIesima.equals("REVOCATA") || statoDomiciliazioneIesima.equals("RESPINTA") || statoDomiciliazioneIesima.equals("NON ATTIVA")) {

                        // rosso
                        stato_domiciliazione_iesima.setVisibility(View.GONE);
                        // stato_domiciliazione_iesima.setText(R.string.domiciliazione_non_attiva);
                        // stato_domiciliazione_iesima.setTextColor(getResources().getColor(R.color.red1));
                        stato_domiciliazione_dot.setImageResource(R.drawable.red_dot);

                        final ImageView stato_domiciliazione_iesima_descricrizione_btn = (ImageView) account_domiciliazione.findViewById(R.id.stato_domiciliazione_iesima_descricrizione_btn);
                        stato_domiciliazione_iesima_descricrizione_btn.setVisibility(View.VISIBLE);
                        stato_domiciliazione_iesima_descricrizione_btn.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
                        stato_domiciliazione_iesima_descricrizione_btn.getBackground().invalidateSelf();
                        stato_domiciliazione_iesima_descricrizione_btn.setOnClickListener(new OnClickListener() {
                            public void onClick(View v) {

                                // stato_domiciliazione_iesima_descricrizione_btn.getBackground().setColorFilter(0xFF999999,
                                // PorterDuff.Mode.MULTIPLY);
                                // stato_domiciliazione_iesima_descricrizione_btn.getBackground().invalidateSelf();
                                animation = new AnimationDrawable();
                                animation.addFrame(getResources().getDrawable(R.drawable.anim_load2), 300);
                                animation.addFrame(getResources().getDrawable(R.drawable.anim_load3), 300);
                                animation.addFrame(getResources().getDrawable(R.drawable.anim_load4), 300);
                                animation.setOneShot(false);
                                imageAnim = (ImageView) findViewById(R.id.waiting_anim);
                                imageAnim.setVisibility(View.VISIBLE);
                                if (Build.VERSION.SDK_INT < 16) {
                                    imageAnim.setBackgroundDrawable(animation);
                                } else {
                                    imageAnim.setBackground(animation);
                                }
                                // start the animation!
                                animation.start();

                                // Intent intentHome = new
                                // Intent(getApplicationContext(),DomiciliazioneActivate.class);
                                Intent intentHome = new Intent();

                                // check Residential or Not
                                JSONObject item = domicilLogin.optJSONObject(1);

                                // come da specifica GP-SEPA-U224-00
                                if (item.optString("TipologiaCliente").equals(Constants.TIPOLOGIA_RETAIL))
                                    intentHome.setClass(getApplicationContext(), DomiciliazioneActivateResidenziali.class);
                                else
                                    intentHome.setClass(getApplicationContext(), DomiciliazioneActivateNonResidenziali.class);

                                intentHome.putExtra("CodiceConto", codiceConto);
                                boolean isDual = JSONUtility.isDual(domicilLogin, codiceConto);
                                if (isDual) {
                                    intentHome.putExtra("TIPOLOGIA", "DUAL");
                                } else {
                                    String tipologia = JSONUtility.cercaTipologiaFornitura(domicilLogin, codiceConto);
                                    if (tipologia.equals("POWER")) {
                                        intentHome.putExtra("TIPOLOGIA", "LUCE");
                                    } else {
                                        intentHome.putExtra("TIPOLOGIA", "GAS");
                                    }
                                }

                                Domiciliazione.this.finish();
                                startActivity(intentHome);
                                overridePendingTransition(R.anim.fade, R.anim.hold);
                            }
                        });
                    } else {
                        // gialla
                        stato_domiciliazione_iesima.setText(R.string.domiciliazione_in_corso);
                        stato_domiciliazione_iesima.setTextColor(getResources().getColor(R.color.yellow1));
                        stato_domiciliazione_dot.setImageResource(R.drawable.yellow_dot);
                        // account_domiciliazione.setOnClickListener(new
                        // OnClickListener() {
                        // public void onClick(View v)
                        // {
                        // animation = new AnimationDrawable();
                        // animation.addFrame(getResources().getDrawable(R.drawable.anim_load2),
                        // 300);
                        // animation.addFrame(getResources().getDrawable(R.drawable.anim_load3),
                        // 300);
                        // animation.addFrame(getResources().getDrawable(R.drawable.anim_load4),
                        // 300);
                        // animation.setOneShot(false);
                        // imageAnim = (ImageView)
                        // findViewById(R.id.waiting_anim);
                        // imageAnim.setVisibility(View.VISIBLE);
                        // if (Build.VERSION.SDK_INT < 16)
                        // {
                        // imageAnim.setBackgroundDrawable(animation);
                        // } else
                        // {
                        // imageAnim.setBackground(animation);
                        // }
                        // // start the animation!
                        // animation.start();
                        // Intent intentHome = new
                        // Intent(getApplicationContext(),
                        // DomiciliazioneClientDetail.class);
                        // intentHome.putExtra("CodiceConto", codiceConto);
                        // intentHome.putExtra("IndirizzoFornitura",
                        // indirizzoFornituraIesimo);
                        // Domiciliazione.this.finish();
                        // startActivity(intentHome);
                        // overridePendingTransition(R.anim.fade, R.anim.hold);
                        //
                        // }
                        // });
                    }
                    domiciliazione_page.addView(account_domiciliazione);

                    if ((i % 2) != 0 && (i != 0)) {
                        LinearLayout pagina_domiciliazione = (LinearLayout) domiciliazione_pagina.findViewById(R.id.domiciliazione_pagina);
                        pagina_domiciliazione.removeAllViews();
                        pagina_domiciliazione.addView(domiciliazione_page);
                        domiciliazione_pager.addView(domiciliazione_pagina);
                    }
                }

                if ((conti % 2) != 0) {
                    LinearLayout pagina_domiciliazione = (LinearLayout) domiciliazione_pagina.findViewById(R.id.domiciliazione_pagina);
                    pagina_domiciliazione.removeAllViews();
                    pagina_domiciliazione.addView(domiciliazione_page);
                    domiciliazione_pager.addView(domiciliazione_pagina);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        final LinearLayout scroller = (LinearLayout) findViewById(R.id.domiciliazione_scroller);
        int numeroPagine = domiciliazione_pager.getChildCount();
        for (int i = 0; i < numeroPagine; i++) {
            ImageView iv = new ImageView(this);
            iv.setId(i);
            LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            param.setMargins(10, 0, 0, 0);
            iv.setImageResource(R.drawable.dot_not_selected);
            iv.setLayoutParams(param);
            scroller.addView(iv);

            int idFocusedPage = domiciliazione_pager.getCurrentPage();
            ImageView pageFocused = (ImageView) findViewById(idFocusedPage);
            pageFocused.setImageResource(R.drawable.dot_selected);

        }

        domiciliazione_pager.addOnScrollListener(new HorizontalPager.OnScrollListener() {
            int numeroPagine = domiciliazione_pager.getChildCount();

            public void onScroll(int scrollX) {
                for (int i = 0; i < numeroPagine; i++) {
                    ImageView iv = (ImageView) findViewById(i);
                    iv.setImageResource(R.drawable.dot_not_selected);
                }
                int idFocusedPage = domiciliazione_pager.getCurrentPage();
                ImageView pageFocused = (ImageView) findViewById(idFocusedPage);
                pageFocused.setImageResource(R.drawable.dot_selected);
            }

            public void onViewScrollFinished(int currentPage) {
                for (int i = 0; i < numeroPagine; i++) {
                    ImageView iv = (ImageView) findViewById(i);
                    iv.setImageResource(R.drawable.dot_not_selected);
                }
                int idFocusedPage = domiciliazione_pager.getCurrentPage();
                ImageView pageFocused = (ImageView) findViewById(idFocusedPage);
                pageFocused.setImageResource(R.drawable.dot_selected);
            }
        });

    }

    // private int numeroDomiciliazioni(JSONArray array){
    // return array.length()-1;
    // }

    public void onBackPressed() {
        animation = new AnimationDrawable();
        animation.addFrame(getResources().getDrawable(R.drawable.anim_load2), 300);
        animation.addFrame(getResources().getDrawable(R.drawable.anim_load3), 300);
        animation.addFrame(getResources().getDrawable(R.drawable.anim_load4), 300);
        animation.setOneShot(false);
        imageAnim = (ImageView) findViewById(R.id.waiting_anim);
        imageAnim.setVisibility(View.VISIBLE);
        if (Build.VERSION.SDK_INT < 16) {
            imageAnim.setBackgroundDrawable(animation);
        } else {
            imageAnim.setBackground(animation);
        }
        // start the animation!
        animation.start();
        Intent intentHome = new Intent(getApplicationContext(), Home.class);
        Domiciliazione.this.finish();
        startActivity(intentHome);
        overridePendingTransition(R.anim.fade, R.anim.hold);
    }

}
