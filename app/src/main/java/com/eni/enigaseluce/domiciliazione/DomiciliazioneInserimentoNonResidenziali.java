package com.eni.enigaseluce.domiciliazione;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.http.cookie.Cookie;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.AnimationDrawable;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.eni.enigaseluce.R;
import com.eni.enigaseluce.error.OopsPageDomiciliazioneInserimento;
import com.eni.enigaseluce.error.OopsPageDomiciliazioneInserimentoHome;
import com.eni.enigaseluce.error.OopsPageSessioneScaduta;
import com.eni.enigaseluce.error.Utils;
import com.eni.enigaseluce.font.EniFont;
import com.eni.enigaseluce.httpcall.Constants;
import com.eni.enigaseluce.httpcall.ServiceCall;
import com.eni.enigaseluce.httpcall.ServiceCallAssembler;
import com.eni.enigaseluce.httpcall.ServiceCallFactory;
import com.eni.enigaseluce.model.InfoUtenzaEni;
import com.eni.enigaseluce.utils.CustomAlertDialog;
import com.eni.enigaseluce.utils.JSONUtility;
import com.eni.enigaseluce.utils.Utilities;

//questa classe gestisce l'inserimento dei dati per le utenze residenziali che sono persone giuridiche o per le utenze non residenziali
@SuppressLint("NewApi")
public class DomiciliazioneInserimentoNonResidenziali extends Activity {

	private Context context;
	private static Context staticContext;
	AnimationDrawable animation;
	ImageView imageAnim;
	String codiceConto;
	CheckBox matchIntestatari;

	private ServiceCall stAvDomFornServiceCall;
	private HashMap<String, String> parametriDomForn;

	private InfoUtenzaEni utenzaEni;

	public String rispostaLogin = Constants.SERVIZIO_LOGIN;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.domiciliazione_inserimento_persona_giuridica);

		Intent intent = getIntent();

		codiceConto = intent.getStringExtra("CodiceConto");
		context = this;
		staticContext = this;

		utenzaEni = new InfoUtenzaEni();

		final ImageView domiciliazioneIndietro = (ImageView) findViewById(R.id.domiciliazione_inserimento_indietro);
		domiciliazioneIndietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
		domiciliazioneIndietro.getBackground().invalidateSelf();
		domiciliazioneIndietro.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v1)
			{
				domiciliazioneIndietro.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
				domiciliazioneIndietro.getBackground().invalidateSelf();
				animation = new AnimationDrawable();
				animation.addFrame(getResources().getDrawable(R.drawable.anim_load2), 300);
				animation.addFrame(getResources().getDrawable(R.drawable.anim_load3), 300);
				animation.addFrame(getResources().getDrawable(R.drawable.anim_load4), 300);
				animation.setOneShot(false);
				imageAnim = (ImageView) findViewById(R.id.waiting_anim);
				imageAnim.setVisibility(View.VISIBLE);
				if (Build.VERSION.SDK_INT < 16)
				{
					imageAnim.setBackgroundDrawable(animation);
				} else
				{
					imageAnim.setBackground(animation);
				}
				// start the animation!
				animation.start();
				Intent intentIndietro = new Intent(getApplicationContext(), DomiciliazioneActivateNonResidenziali.class);
				intentIndietro.putExtra("CodiceConto", getIntent().getStringExtra("CodiceConto"));
				intentIndietro.putExtra("TIPOLOGIA", getIntent().getStringExtra("TIPOLOGIA"));
				DomiciliazioneInserimentoNonResidenziali.this.finish();
				startActivity(intentIndietro);
				overridePendingTransition(R.anim.fade, R.anim.hold);
			}
		});

		final ImageView domiciliazioneHelp = (ImageView) findViewById(R.id.domiciliazione_inserimento_help);
		domiciliazioneHelp.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
		domiciliazioneHelp.getBackground().invalidateSelf();
		domiciliazioneHelp.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v1)
			{
				domiciliazioneHelp.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
				domiciliazioneHelp.getBackground().invalidateSelf();
				animation = new AnimationDrawable();
				animation.addFrame(getResources().getDrawable(R.drawable.anim_load2), 300);
				animation.addFrame(getResources().getDrawable(R.drawable.anim_load3), 300);
				animation.addFrame(getResources().getDrawable(R.drawable.anim_load4), 300);
				animation.setOneShot(false);
				imageAnim = (ImageView) findViewById(R.id.waiting_anim);
				imageAnim.setVisibility(View.VISIBLE);
				if (Build.VERSION.SDK_INT < 16)
				{
					imageAnim.setBackgroundDrawable(animation);
				} else
				{
					imageAnim.setBackground(animation);
				}
				// start the animation!
				animation.start();
				Intent intentHelp = new Intent(getApplicationContext(), DomiciliazioneHelp.class);
				intentHelp.putExtra("ORIGINE", Constants.ORIGINE_DOMICILIAZIONE_INSERIMENTO_NON_RESIDENZIALI);
				intentHelp.putExtra("CodiceConto", codiceConto);
				intentHelp.putExtra("TIPOLOGIA", getIntent().getStringExtra("TIPOLOGIA"));
				DomiciliazioneInserimentoNonResidenziali.this.finish();
				startActivity(intentHelp);
				overridePendingTransition(R.anim.fade, R.anim.hold);
			}
		});

		RelativeLayout rl_1_bis = (RelativeLayout) findViewById(R.id.rl_1_bis);
		rl_1_bis.setVisibility(View.GONE);

		final EditText ragioneSociale = (EditText) findViewById(R.id.edit_ragione_sociale);
		final EditText pIVA = (EditText) findViewById(R.id.edit_p_iva);
		final EditText iban = (EditText) findViewById(R.id.domiciliazione_inserimento_iban);

		final EditText nome = (EditText) findViewById(R.id.domiciliazione_inserimento_nome);
		final EditText cognome = (EditText) findViewById(R.id.domiciliazione_inserimento_cognome);
		final EditText codicefiscale = (EditText) findViewById(R.id.domiciliazione_inserimento_codice_fiscale);

		RelativeLayout layoutMatchIntestatari = (RelativeLayout) findViewById(R.id.layout_match_intestatari);
		layoutMatchIntestatari.setVisibility(View.VISIBLE);

		matchIntestatari = (CheckBox) layoutMatchIntestatari.findViewById(R.id.check_match_intestatari);

		final EniFont button = (EniFont) findViewById(R.id.domiciliazione_inserimento_button);
		button.setBackgroundResource(R.drawable.attiva_la_domiciliazione_disabled_bckgnd);
		button.setEnabled(false);
		button.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v1)
			{
				button.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
				button.getBackground().invalidateSelf();
				animation = new AnimationDrawable();
				animation.addFrame(getResources().getDrawable(R.drawable.anim_load2), 300);
				animation.addFrame(getResources().getDrawable(R.drawable.anim_load3), 300);
				animation.addFrame(getResources().getDrawable(R.drawable.anim_load4), 300);
				animation.setOneShot(false);
				imageAnim = (ImageView) findViewById(R.id.waiting_anim);
				imageAnim.setVisibility(View.VISIBLE);
				if (Build.VERSION.SDK_INT < 16)
				{
					imageAnim.setBackgroundDrawable(animation);
				} else
				{
					imageAnim.setBackground(animation);
				}
				// start the animation!
				animation.start();

				utenzaEni.setNomeSottoscrittoreCC(nome.getText().toString().toUpperCase(Locale.ITALIAN));
				utenzaEni.setCognomeSottoscrittoreCC(cognome.getText().toString().toUpperCase(Locale.ITALIAN));
				utenzaEni.setCodiceFiscaleSottoscrittoreCC(codicefiscale.getText().toString().toUpperCase(Locale.ITALIAN));
				utenzaEni.setIbanClienteCC(iban.getText().toString().toUpperCase(Locale.ITALIAN));
				utenzaEni.setPartitaIvaClienteCC(pIVA.getText().toString().toUpperCase(Locale.ITALIAN));
				utenzaEni.setRagioneSocialeClienteCC(ragioneSociale.getText().toString().toUpperCase(Locale.ITALIAN));

				/************** Execute Domiciliazione Conto *************/
				chiamataEseguiDomiciliazione();
			}
		});

		TextWatcher textWatcherNome = new TextWatcher() {
			public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2)
			{

			}

			public void onTextChanged(CharSequence charSequence, int i, int i1, int i2)
			{

			}

			public void afterTextChanged(Editable editable)
			{
				if (!nome.getText().toString().trim().equals("") && !cognome.getText().toString().trim().equals("")
						&& !codicefiscale.getText().toString().trim().equals("") && !iban.getText().toString().trim().equals("")
						&& !pIVA.getText().toString().trim().equals("") && !ragioneSociale.getText().toString().trim().equals(""))
				{

					button.setBackgroundResource(R.drawable.attiva_la_domiciliazione_bckgnd);
					button.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
					button.getBackground().invalidateSelf();
					button.setEnabled(true);

				} else
				{
					button.setBackgroundResource(R.drawable.attiva_la_domiciliazione_disabled_bckgnd);
					button.setEnabled(false);
				}
			}
		};

		nome.addTextChangedListener(textWatcherNome);
		cognome.addTextChangedListener(textWatcherNome);
		codicefiscale.addTextChangedListener(textWatcherNome);
		iban.addTextChangedListener(textWatcherNome);
		pIVA.addTextChangedListener(textWatcherNome);
		ragioneSociale.addTextChangedListener(textWatcherNome);
	}

	@Override
	protected void onResume()
	{
		super.onResume();

		final EniFont btn_attiva_domiciliazione = (EniFont) findViewById(R.id.domiciliazione_inserimento_button);
		btn_attiva_domiciliazione.getBackground().invalidateSelf();

		final ImageView domiciliazioneIndietro = (ImageView) findViewById(R.id.domiciliazione_inserimento_indietro);
		domiciliazioneIndietro.getBackground().invalidateSelf();
	}

	public void onCheckBoxClicked(View view)
	{

		final EditText ragioneSociale = (EditText) findViewById(R.id.edit_ragione_sociale);
		final EditText pIVA = (EditText) findViewById(R.id.edit_p_iva);
		final EditText iban = (EditText) findViewById(R.id.domiciliazione_inserimento_iban);

		final EditText nome = (EditText) findViewById(R.id.domiciliazione_inserimento_nome);
		final EditText cognome = (EditText) findViewById(R.id.domiciliazione_inserimento_cognome);
		final EditText codicefiscale = (EditText) findViewById(R.id.domiciliazione_inserimento_codice_fiscale);

		boolean checked = ((CheckBox) view).isChecked();

		switch (view.getId()) {
		case R.id.check_match_intestatari:

			if (checked)
			{
				ragioneSociale.setText(JSONUtility.getRagioneSocialeCliente(rispostaLogin));
				ragioneSociale.setEnabled(false);

				iban.setText("");

				boolean pIVACorretta = JSONUtility.isPartitaIVACorretta(rispostaLogin);

				if (pIVACorretta)
				{
					pIVA.setText(JSONUtility.getPartitaIVACliente(rispostaLogin));
					pIVA.setEnabled(false);

					iban.requestFocus();
				} else
				{

					// TODO mostrare un pop-up di avviso e rendere editabile
					// l'EditText della partita IVA

					CustomAlertDialog.showAlertDialogNoButton(context, getString(R.string.domiciliazione_alert_partita_iva),
							getString(R.string.spannable_partita_iva), R.layout.alert_view_domiciliazione);

					pIVA.setText("");
					pIVA.setEnabled(true);
					pIVA.requestFocus();
				}
			} else
			{

				ragioneSociale.setText("");
				ragioneSociale.setEnabled(true);
				ragioneSociale.requestFocus();

				iban.setText("");

				nome.setText("");
				nome.setEnabled(true);

				cognome.setText("");
				cognome.setEnabled(true);

				codicefiscale.setText("");
				codicefiscale.setEnabled(true);

				pIVA.setText("");
				pIVA.setEnabled(true);
			}

			break;
		}
	}

	public static Context getContext()
	{
		return DomiciliazioneInserimentoNonResidenziali.staticContext;
	}

	@Override
	protected void onStart()
	{
		super.onStart();

		Timer timer = new Timer();
		timer.schedule(new TimerTask() {
			@Override
			public void run()
			{
				InputMethodManager m = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
				m.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
			}
		}, 300);
	}

	public void chiamataEseguiDomiciliazione()
	{

		animation = new AnimationDrawable();
		animation.addFrame(getResources().getDrawable(R.drawable.anim_load2), 300);
		animation.addFrame(getResources().getDrawable(R.drawable.anim_load3), 300);
		animation.addFrame(getResources().getDrawable(R.drawable.anim_load4), 300);
		animation.setOneShot(false);
		imageAnim = (ImageView) findViewById(R.id.waiting_anim);
		imageAnim.setVisibility(View.VISIBLE);
		if (Build.VERSION.SDK_INT < 16)
		{
			imageAnim.setBackgroundDrawable(animation);
		} else
		{
			imageAnim.setBackground(animation);
		}
		// start the animation!
		animation.start();

		utenzaEni.setCodiceCliente(JSONUtility.getCodiceCliente(rispostaLogin));
		utenzaEni.setCodiceConto(codiceConto);
		utenzaEni.setUserId(JSONUtility.getUserId(rispostaLogin));
		utenzaEni.setTipologia(JSONUtility.getTipologiaUtenza(rispostaLogin));
		utenzaEni.setTelefonoPrimario(JSONUtility.getTelefonoPrimario(rispostaLogin));
		utenzaEni.setNomeCliente(JSONUtility.getNomeCliente(rispostaLogin));
		utenzaEni.setCognomeCliente(JSONUtility.getRagioneSocialeCliente(rispostaLogin));
		utenzaEni.setCodiceFiscaleCliente(JSONUtility.getCodiceFiscaleCliente(rispostaLogin));
		utenzaEni.setPartitaIvaCliente(JSONUtility.getPartitaIVACliente(rispostaLogin));
		utenzaEni.setTipoIndirizzo(JSONUtility.getTipoIndirizzo(rispostaLogin));
		utenzaEni.setStrada(JSONUtility.getStrada(rispostaLogin));
		utenzaEni.setNumCivico(JSONUtility.getNumCivico(rispostaLogin));
		utenzaEni.setEstensioneNumCivico(JSONUtility.getEstensioneNumCivico(rispostaLogin));
		utenzaEni.setCitta(JSONUtility.getCitta(rispostaLogin));
		utenzaEni.setCap(JSONUtility.getCap(rispostaLogin));
		utenzaEni.setProvincia(JSONUtility.getProvincia(rispostaLogin));

		parametriDomForn = ServiceCallAssembler.fillParametersExecuteDomiciliazione(utenzaEni,
				matchIntestatari.isChecked() && !JSONUtility.isPartitaIVACorretta(rispostaLogin) ? true : false, Constants.TIPOLOGIA_NON_RESIDENZIALE);

		if (!Utilities.isNetworkAvailable(context))
		{
			Intent intentOops = new Intent(getApplicationContext(), OopsPageDomiciliazioneInserimento.class);
			intentOops.putExtra(Constants.TIPOLOGIA_UTENZA, "NON_RESIDENZIALI");
			intentOops.putExtra(Constants.MESSAGGIO, getResources().getString(R.string.connessione_assente));
			intentOops.putExtra(Constants.CODICE_CLIENTE, utenzaEni.getCodiceCliente());
			intentOops.putExtra(Constants.CODICE_CONTO, utenzaEni.getCodiceConto());
			intentOops.putExtra(Constants.COGNOME_CLIENTE_CC, utenzaEni.getRagioneSocialeClienteCC());
			intentOops.putExtra(Constants.CODICE_FISCALE_SOTTOSCRITTORE_CC, utenzaEni.getCodiceFiscaleSottoscrittoreCC());
			intentOops.putExtra(Constants.NOME_SOTTOSCRITTORE_CC, utenzaEni.getNomeSottoscrittoreCC());
			intentOops.putExtra(Constants.COGNOME_SOTTOSCRITTORE_CC, utenzaEni.getCognomeSottoscrittoreCC());
			intentOops.putExtra(Constants.IBAN_CLIENTE_CC, utenzaEni.getIbanClienteCC());
			intentOops.putExtra(Constants.USER_ID, utenzaEni.getUserId());
			intentOops.putExtra(Constants.NOME_CLIENTE, utenzaEni.getNomeCliente());
			intentOops.putExtra(Constants.COGNOME_CLIENTE, utenzaEni.getCognomeCliente());
			intentOops.putExtra(Constants.CODICE_FISCALE_CLIENTE, utenzaEni.getCodiceFiscaleCliente());
			intentOops.putExtra(Constants.PIVA_MODIFICATA, utenzaEni.getPiva_Modificata());

			if (!utenzaEni.getTipoIndirizzo().equals(""))
			{
				intentOops.putExtra(Constants.TIPO_INDIRIZZO, utenzaEni.getTipoIndirizzo());
			}
			if (!utenzaEni.getStrada().equals(""))
			{
				intentOops.putExtra(Constants.STRADA, utenzaEni.getStrada());
			}
			if (!utenzaEni.getNumCivico().equals(""))
			{
				intentOops.putExtra(Constants.NUM_CIVICO, utenzaEni.getNumCivico());
			}
			if (!utenzaEni.getEstensioneNumCivico().equals(""))
			{
				intentOops.putExtra(Constants.ESTENSIONE_NUM_CIVICO, utenzaEni.getEstensioneNumCivico());
			}
			if (!utenzaEni.getCitta().equals(""))
			{
				intentOops.putExtra(Constants.CITTA, utenzaEni.getCitta());
			}
			if (!utenzaEni.getCap().equals(""))
			{
				intentOops.putExtra(Constants.CAP, utenzaEni.getCap());
			}
			if (!utenzaEni.getProvincia().equals(""))
			{
				intentOops.putExtra(Constants.PROVINCIA, utenzaEni.getProvincia());
			}

			// StatoAttivazioneFornitura.this.finish();
			startActivity(intentOops);
			if (animation != null)
			{
				animation.stop();
				imageAnim.setVisibility(View.INVISIBLE);
			}

			overridePendingTransition(R.anim.fade, R.anim.hold);
		} else
		{

			/********************* Chiamata execute domiciliazione conto Fornitura ******************************/
			stAvDomFornServiceCall = ServiceCallFactory.createServiceCall(context);

			try
			{

				if (matchIntestatari.isChecked())
					stAvDomFornServiceCall.executeHttpsGetDomiciliazione(Constants.URL_EXECUTE_DOMICILIAZIONE_CONTO, null, parametriDomForn,
							DomiciliazioneInserimentoNonResidenziali.this, 1, null);
				else
				{

					Intent intentOops = new Intent(getApplicationContext(), OopsPageDomiciliazioneInserimento.class);
					intentOops.putExtra(Constants.TIPOLOGIA_UTENZA, "NON_RESIDENZIALI");
					intentOops.putExtra(Constants.MESSAGGIO, getResources().getString(R.string.connessione_assente));
					intentOops.putExtra(Constants.CODICE_CLIENTE, utenzaEni.getCodiceCliente());
					intentOops.putExtra(Constants.CODICE_CONTO, utenzaEni.getCodiceConto());
					intentOops.putExtra(Constants.COGNOME_CLIENTE_CC, utenzaEni.getRagioneSocialeClienteCC());
					intentOops.putExtra(Constants.CODICE_FISCALE_SOTTOSCRITTORE_CC, utenzaEni.getCodiceFiscaleSottoscrittoreCC());
					intentOops.putExtra(Constants.NOME_SOTTOSCRITTORE_CC, utenzaEni.getNomeSottoscrittoreCC());
					intentOops.putExtra(Constants.COGNOME_SOTTOSCRITTORE_CC, utenzaEni.getCognomeSottoscrittoreCC());
					intentOops.putExtra(Constants.IBAN_CLIENTE_CC, utenzaEni.getIbanClienteCC());
					intentOops.putExtra(Constants.USER_ID, utenzaEni.getUserId());
					intentOops.putExtra(Constants.NOME_CLIENTE, utenzaEni.getNomeCliente());
					intentOops.putExtra(Constants.COGNOME_CLIENTE, utenzaEni.getCognomeCliente());
					intentOops.putExtra(Constants.CODICE_FISCALE_CLIENTE, utenzaEni.getCodiceFiscaleCliente());
					intentOops.putExtra(Constants.PIVA_MODIFICATA, utenzaEni.getPiva_Modificata());

					if (!utenzaEni.getTipoIndirizzo().equals(""))
					{
						intentOops.putExtra(Constants.TIPO_INDIRIZZO, utenzaEni.getTipoIndirizzo());
					}
					if (!utenzaEni.getStrada().equals(""))
					{
						intentOops.putExtra(Constants.STRADA, utenzaEni.getStrada());
					}
					if (!utenzaEni.getNumCivico().equals(""))
					{
						intentOops.putExtra(Constants.NUM_CIVICO, utenzaEni.getNumCivico());
					}
					if (!utenzaEni.getEstensioneNumCivico().equals(""))
					{
						intentOops.putExtra(Constants.ESTENSIONE_NUM_CIVICO, utenzaEni.getEstensioneNumCivico());
					}
					if (!utenzaEni.getCitta().equals(""))
					{
						intentOops.putExtra(Constants.CITTA, utenzaEni.getCitta());
					}
					if (!utenzaEni.getCap().equals(""))
					{
						intentOops.putExtra(Constants.CAP, utenzaEni.getCap());
					}
					if (!utenzaEni.getProvincia().equals(""))
					{
						intentOops.putExtra(Constants.PROVINCIA, utenzaEni.getProvincia());
					}

					CustomAlertDialog.showAlertDialogWithButton(context, getString(R.string.domiciliazione_alert_intestatario),
							getString(R.string.spannable_confermi), R.layout.alert_view_button_domiciliazione, null,
							Constants.URL_EXECUTE_DOMICILIAZIONE_CONTO, parametriDomForn, DomiciliazioneInserimentoNonResidenziali.this, 1, null, intentOops,
							animation, imageAnim);
				}
			} catch (Exception e)
			{

				Intent intentOops = new Intent(getApplicationContext(), OopsPageDomiciliazioneInserimento.class);
				intentOops.putExtra(Constants.TIPOLOGIA_UTENZA, "NON_RESIDENZIALI");
				intentOops.putExtra(Constants.MESSAGGIO, getResources().getString(R.string.connessione_assente));
				intentOops.putExtra(Constants.CODICE_CLIENTE, utenzaEni.getCodiceCliente());
				intentOops.putExtra(Constants.CODICE_CONTO, utenzaEni.getCodiceConto());
				intentOops.putExtra(Constants.COGNOME_CLIENTE_CC, utenzaEni.getRagioneSocialeClienteCC());
				intentOops.putExtra(Constants.CODICE_FISCALE_SOTTOSCRITTORE_CC, utenzaEni.getCodiceFiscaleSottoscrittoreCC());
				intentOops.putExtra(Constants.NOME_SOTTOSCRITTORE_CC, utenzaEni.getNomeSottoscrittoreCC());
				intentOops.putExtra(Constants.COGNOME_SOTTOSCRITTORE_CC, utenzaEni.getCognomeSottoscrittoreCC());
				intentOops.putExtra(Constants.IBAN_CLIENTE_CC, utenzaEni.getIbanClienteCC());
				intentOops.putExtra(Constants.USER_ID, utenzaEni.getUserId());
				intentOops.putExtra(Constants.NOME_CLIENTE, utenzaEni.getNomeCliente());
				intentOops.putExtra(Constants.COGNOME_CLIENTE, utenzaEni.getCognomeCliente());
				intentOops.putExtra(Constants.CODICE_FISCALE_CLIENTE, utenzaEni.getCodiceFiscaleCliente());
				intentOops.putExtra(Constants.PIVA_MODIFICATA, utenzaEni.getPiva_Modificata());

				if (!utenzaEni.getTipoIndirizzo().equals(""))
				{
					intentOops.putExtra(Constants.TIPO_INDIRIZZO, utenzaEni.getTipoIndirizzo());
				}
				if (!utenzaEni.getStrada().equals(""))
				{
					intentOops.putExtra(Constants.STRADA, utenzaEni.getStrada());
				}
				if (!utenzaEni.getNumCivico().equals(""))
				{
					intentOops.putExtra(Constants.NUM_CIVICO, utenzaEni.getNumCivico());
				}
				if (!utenzaEni.getEstensioneNumCivico().equals(""))
				{
					intentOops.putExtra(Constants.ESTENSIONE_NUM_CIVICO, utenzaEni.getEstensioneNumCivico());
				}
				if (!utenzaEni.getCitta().equals(""))
				{
					intentOops.putExtra(Constants.CITTA, utenzaEni.getCitta());
				}
				if (!utenzaEni.getCap().equals(""))
				{
					intentOops.putExtra(Constants.CAP, utenzaEni.getCap());
				}
				if (!utenzaEni.getProvincia().equals(""))
				{
					intentOops.putExtra(Constants.PROVINCIA, utenzaEni.getProvincia());
				}

				// DomiciliazioneInserimento.this.finish();
				startActivity(intentOops);
				overridePendingTransition(R.anim.fade, R.anim.hold);
			}
		}
	}

	public void postChiamata(String rispostaChiamata, List<Cookie> lista, int code)
	{

		if (animation != null)
		{
			animation.stop();
			imageAnim.setVisibility(View.INVISIBLE);
		}

		final ImageView domiciliazioneIndietro = (ImageView) findViewById(R.id.domiciliazione_inserimento_indietro);
		domiciliazioneIndietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
		domiciliazioneIndietro.getBackground().invalidateSelf();

		JSONArray rispostaJSON = new JSONArray();
		String esito = "";
		String descEsito = "";

		try
		{
			rispostaJSON = new JSONArray(rispostaChiamata);
			JSONObject oggettoRisposta = (JSONObject) rispostaJSON.get(0);
			esito = oggettoRisposta.getString("esito");
			descEsito = oggettoRisposta.getString("descEsito");
		} catch (JSONException e)
		{
			e.printStackTrace();
		}

		if (esito.equals("200"))
		{
			switch (code) {
			case 1:
				AlertDialog.Builder builder = new AlertDialog.Builder(context);
				builder.setTitle(R.string.domiciliazione).setMessage(R.string.chiamata_OK).setCancelable(false)
						.setPositiveButton("Chiudi", new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id)
							{
								// chiamata visualizza domiciliazione
								// fornitura e vado alla schermata
								// Domiciliazione
								chiamataVisualizzaDomiciliazione();
							}
						});
				builder.create().show();

				break;
			case 2:

				JSONArray domicilLogin = null;
				try
				{
					domicilLogin = new JSONArray(rispostaLogin);
				} catch (JSONException e)
				{
					e.printStackTrace();
				}

				final String indirizzoFornituraIesimo = JSONUtility.cercaIndirizzoFornitura(domicilLogin, codiceConto);

				Constants.SERVIZIO_VISUALIZZA_FORNITURE_DOMICILIAZIONE = rispostaChiamata;
				Intent intentDomiciliazione = new Intent(getApplicationContext(), Domiciliazione.class);
				intentDomiciliazione.putExtra("CodiceConto", codiceConto);
				intentDomiciliazione.putExtra("IndirizzoFornitura", indirizzoFornituraIesimo);
				DomiciliazioneInserimentoNonResidenziali.this.finish();
				startActivity(intentDomiciliazione);
				overridePendingTransition(R.anim.fade, R.anim.hold);

				break;
			}
		} else
		{
			Intent intentOops = new Intent();
			switch (code) {
			case 1:
				intentOops = new Intent(getApplicationContext(), OopsPageDomiciliazioneInserimento.class);
				intentOops.putExtra(Constants.TIPOLOGIA_UTENZA, "NON_RESIDENZIALI");
				intentOops.putExtra(Constants.MESSAGGIO, getResources().getString(R.string.connessione_assente));
				intentOops.putExtra(Constants.CODICE_CLIENTE, utenzaEni.getCodiceCliente());
				intentOops.putExtra(Constants.CODICE_CONTO, utenzaEni.getCodiceConto());
				intentOops.putExtra(Constants.COGNOME_CLIENTE_CC, utenzaEni.getRagioneSocialeClienteCC());
				intentOops.putExtra(Constants.CODICE_FISCALE_SOTTOSCRITTORE_CC, utenzaEni.getCodiceFiscaleSottoscrittoreCC());
				intentOops.putExtra(Constants.NOME_SOTTOSCRITTORE_CC, utenzaEni.getNomeSottoscrittoreCC());
				intentOops.putExtra(Constants.COGNOME_SOTTOSCRITTORE_CC, utenzaEni.getCognomeSottoscrittoreCC());
				intentOops.putExtra(Constants.IBAN_CLIENTE_CC, utenzaEni.getIbanClienteCC());
				intentOops.putExtra(Constants.USER_ID, utenzaEni.getUserId());
				intentOops.putExtra(Constants.NOME_CLIENTE, utenzaEni.getNomeCliente());
				intentOops.putExtra(Constants.COGNOME_CLIENTE, utenzaEni.getCognomeCliente());
				intentOops.putExtra(Constants.CODICE_FISCALE_CLIENTE, utenzaEni.getCodiceFiscaleCliente());
				intentOops.putExtra(Constants.PIVA_MODIFICATA, utenzaEni.getPiva_Modificata());

				if (!utenzaEni.getTipoIndirizzo().equals(""))
				{
					intentOops.putExtra(Constants.TIPO_INDIRIZZO, utenzaEni.getTipoIndirizzo());
				}
				if (!utenzaEni.getStrada().equals(""))
				{
					intentOops.putExtra(Constants.STRADA, utenzaEni.getStrada());
				}
				if (!utenzaEni.getNumCivico().equals(""))
				{
					intentOops.putExtra(Constants.NUM_CIVICO, utenzaEni.getNumCivico());
				}
				if (!utenzaEni.getEstensioneNumCivico().equals(""))
				{
					intentOops.putExtra(Constants.ESTENSIONE_NUM_CIVICO, utenzaEni.getEstensioneNumCivico());
				}
				if (!utenzaEni.getCitta().equals(""))
				{
					intentOops.putExtra(Constants.CITTA, utenzaEni.getCitta());
				}
				if (!utenzaEni.getCap().equals(""))
				{
					intentOops.putExtra(Constants.CAP, utenzaEni.getCap());
				}
				if (!utenzaEni.getProvincia().equals(""))
				{
					intentOops.putExtra(Constants.PROVINCIA, utenzaEni.getProvincia());
				}

				break;
			case 2:
				intentOops = new Intent(getApplicationContext(), OopsPageDomiciliazioneInserimentoHome.class);
				JSONArray array;
				String parametri = "";
				try
				{
					array = new JSONArray(rispostaLogin);
					String arrayCodiciConti[] = elencoCodiciContiAttiviCompleti(array);
					for (int i = 0; i < arrayCodiciConti.length; i++)
					{
						parametri = parametri + "&codiciContoCliente=" + arrayCodiciConti[i];
					}
				} catch (JSONException e1)
				{
					e1.printStackTrace();
				}
				intentOops.putExtra("Parametri", parametri);
			}

			// errori ops page
			if (esito.equals("309"))
			{
				intentOops.putExtra(Constants.MESSAGGIO, getResources().getString(R.string.errore_generico_opspage));
				startActivityForResult(intentOops, OopsPageDomiciliazioneInserimento.ESCI);
				overridePendingTransition(R.anim.fade, R.anim.hold);

			}
			// else if (esito.equals("400"))
			// {
			// intentOops.putExtra(Constants.MESSAGGIO,
			// getResources().getString(R.string.errore_generico_opspage));
			// startActivityForResult(intentOops,
			// OopsPageDomiciliazioneInserimento.ESCI);
			// overridePendingTransition(R.anim.fade, R.anim.hold);
			//
			// } else if (esito.equals("420"))
			// {
			// intentOops.putExtra(Constants.MESSAGGIO,
			// getResources().getString(R.string.errore_generico_opspage));
			// startActivityForResult(intentOops,
			// OopsPageDomiciliazioneInserimento.ESCI);
			// overridePendingTransition(R.anim.fade, R.anim.hold);
			//
			// } else if (esito.equals("449"))
			// {
			// intentOops.putExtra(Constants.MESSAGGIO,
			// getResources().getString(R.string.errore_generico_opspage));
			// startActivityForResult(intentOops,
			// OopsPageDomiciliazioneInserimento.ESCI);
			// overridePendingTransition(R.anim.fade, R.anim.hold);
			//
			// } else if (esito.equals("451"))
			// {
			// intentOops.putExtra(Constants.MESSAGGIO,
			// getResources().getString(R.string.errore_popup_451));
			// startActivityForResult(intentOops,
			// OopsPageDomiciliazioneInserimento.ESCI);
			// overridePendingTransition(R.anim.fade, R.anim.hold);
			// }

			// check applicative error codes retrieving error codes from
			// resources
			else if (Utilities.arrayContainsString(getResources().getStringArray(R.array.execute_domiciliazione_error_codes), esito))
			{
				Utils.popup(DomiciliazioneInserimentoNonResidenziali.this, descEsito, "Riprova");
			}

			// // errori popup
			// else if (esito.equals("307")) {
			// Utils.popup(DomiciliazioneInserimentoNonResidenziali.this,
			// getResources().getString(R.string.errore_popup_307),
			// "Riprova");
			// } else if (esito.equals("435")) {
			// Utils.popup(DomiciliazioneInserimentoNonResidenziali.this,
			// getResources().getString(R.string.errore_popup_435),
			// "Riprova");
			// } else if (esito.equals("437")) {
			// Utils.popup(DomiciliazioneInserimentoNonResidenziali.this,
			// getResources().getString(R.string.errore_popup_437),
			// "Riprova");
			// } else if (esito.equals("438")) {
			// Utils.popup(DomiciliazioneInserimentoNonResidenziali.this,
			// getResources().getString(R.string.errore_popup_438),
			// "Riprova");
			// } else if (esito.equals("443")) {
			// Utils.popup(DomiciliazioneInserimentoNonResidenziali.this,
			// getResources().getString(R.string.errore_popup_443),
			// "Riprova");
			// } else if (esito.equals("444")) {
			// Utils.popup(DomiciliazioneInserimentoNonResidenziali.this,
			// getResources().getString(R.string.errore_popup_444),
			// "Riprova");
			// } else if (esito.equals("445")) {
			// Utils.popup(DomiciliazioneInserimentoNonResidenziali.this,
			// getResources().getString(R.string.errore_popup_445),
			// "Riprova");
			// } else if (esito.equals("446")) {
			// Utils.popup(DomiciliazioneInserimentoNonResidenziali.this,
			// getResources().getString(R.string.errore_popup_446),
			// "Riprova");
			// } else if (esito.equals("447")) {
			// Utils.popup(DomiciliazioneInserimentoNonResidenziali.this,
			// getResources().getString(R.string.errore_popup_447),
			// "Riprova");
			// } else if (esito.equals("448")) {
			// Utils.popup(DomiciliazioneInserimentoNonResidenziali.this,
			// getResources().getString(R.string.errore_popup_307),
			// "Riprova");
			//
			// }
			else {
		    	if (descEsito.equals("")) {
					intentOops = new Intent(getApplicationContext(), OopsPageSessioneScaduta.class);
					intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.sessione_scaduta));
				} else{
					intentOops.putExtra("MESSAGGIO", descEsito);
				}
				
				startActivity(intentOops);
				overridePendingTransition(R.anim.fade, R.anim.hold);
		    }
			final EniFont button = (EniFont) findViewById(R.id.domiciliazione_inserimento_button);
			button.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
			button.getBackground().invalidateSelf();
		}
	}

	public void postChiamataException(boolean exception)
	{
		if (exception)
		{
			if (animation != null)
			{
				animation.stop();
				imageAnim.setVisibility(View.INVISIBLE);
			}

			final ImageView domiciliazioneIndietro = (ImageView) findViewById(R.id.domiciliazione_inserimento_indietro);
			domiciliazioneIndietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
			domiciliazioneIndietro.getBackground().invalidateSelf();

			Intent intentOops = new Intent(getApplicationContext(), OopsPageDomiciliazioneInserimento.class);
			intentOops.putExtra(Constants.MESSAGGIO, getResources().getString(R.string.servizio_non_disponibile));
			// DomiciliazioneInseriemento.this.finish();
			startActivityForResult(intentOops, OopsPageDomiciliazioneInserimento.ESCI);
			overridePendingTransition(R.anim.fade, R.anim.hold);
		}
	}

	public void chiamataVisualizzaDomiciliazione()
	{
		animation = new AnimationDrawable();
		animation.addFrame(getResources().getDrawable(R.drawable.anim_load2), 300);
		animation.addFrame(getResources().getDrawable(R.drawable.anim_load3), 300);
		animation.addFrame(getResources().getDrawable(R.drawable.anim_load4), 300);
		animation.setOneShot(false);
		imageAnim = (ImageView) findViewById(R.id.waiting_anim);
		imageAnim.setVisibility(View.VISIBLE);
		if (Build.VERSION.SDK_INT < 16)
		{
			imageAnim.setBackgroundDrawable(animation);
		} else
		{
			imageAnim.setBackground(animation);
		}
		// start the animation!
		animation.start();
		if (!Utilities.isNetworkAvailable(context))
		{
			Intent intentOops = new Intent(getApplicationContext(), OopsPageDomiciliazioneInserimentoHome.class);
			intentOops.putExtra(Constants.MESSAGGIO, getResources().getString(R.string.connessione_assente));
			// DomiciliazioneInserimento.this.finish();
			startActivity(intentOops);
			overridePendingTransition(R.anim.fade, R.anim.hold);
		} else
		{
			/********************* Chiamata visualizza domiciliazione Fornitura ******************************/
			ServiceCall visFornDomicilServiceCall = ServiceCallFactory.createServiceCall(context);
			JSONArray array;
			String parametri = "";
			try
			{
				array = new JSONArray(rispostaLogin);
				String arrayCodiciConti[] = elencoCodiciContiAttiviCompleti(array);
				for (int i = 0; i < arrayCodiciConti.length; i++)
				{
					parametri = parametri + "&codiciContoCliente=" + arrayCodiciConti[i];
				}
			} catch (JSONException e1)
			{
				e1.printStackTrace();
			}
			try
			{
				visFornDomicilServiceCall.executeHttpsGetDomiciliazione(Constants.URL_VISUALIZZA_FORNITURE_DOMICILIAZIONE, parametri, null,
						DomiciliazioneInserimentoNonResidenziali.this, 2, null);
			} catch (Exception e)
			{
				Intent intentOops = new Intent(getApplicationContext(), OopsPageDomiciliazioneInserimentoHome.class);
				intentOops.putExtra(Constants.MESSAGGIO, getResources().getString(R.string.servizio_non_disponibile));
				// DomiciliazioneInserimento.this.finish();
				startActivity(intentOops);
				overridePendingTransition(R.anim.fade, R.anim.hold);
			}
			// sorpassando lo statoAvanzamentoForniture
			/*
			 * Intent intentDomiciliazione = new Intent(getApplicationContext(),
			 * Domiciliazione.class); DomiciliazioneInserimento.this.finish();
			 * startActivity(intentDomiciliazione);
			 * overridePendingTransition(R.anim.fade, R.anim.hold);
			 */
		}

	}

	public String[] elencoCodiciContiAttiviCompleti(JSONArray array)
	{
		int lunghezza = numeroCodiciContoDiversiAttiviCompleti(array);
		String result[] = new String[lunghezza];
		String codiceContoConfrontato = "";
		String codiceContoIesimo = "";
		String codiciConcatenati = "";
		int count = 0;
		for (int i = 2; i < array.length(); i++)
		{
			JSONArray contoIesimo;
			try
			{
				contoIesimo = array.getJSONArray(i);
				// codice conto
				JSONObject anagrContoIesimo = contoIesimo.getJSONObject(0);
				codiceContoIesimo = anagrContoIesimo.getString("CodiceConto");

				if (!codiceContoIesimo.equals(codiceContoConfrontato))
				{
					String stato = anagrContoIesimo.getString("StatoConto");
					if (!stato.equals("UTILIZZABILE"))
					{
						continue;
					} else
					{
						// codice conto utilizzabile diverso, controllo non sia
						// vuoto
						if (contoIesimo.length() > 1)
						{
							// che non sia gia' presente
							boolean isPresente = codiciConcatenati.contains("-" + codiceContoIesimo + "-");
							if (isPresente)
							{
								continue;
							} else
							{
								codiceContoConfrontato = codiceContoIesimo;
								result[count] = codiceContoConfrontato;
								codiciConcatenati = codiciConcatenati + "-" + codiceContoIesimo + "-";
								count++;
							}
						} else
						{
							continue;
						}
					}
				}
			} catch (JSONException e)
			{
				e.printStackTrace();
			}
		}
		return result;
	}

	public int numeroCodiciContoDiversiAttiviCompleti(JSONArray array)
	{
		int result = 0;
		String codiceContoConfrontato = "";
		String codiceContoIesimo = "";
		String codiciConcatenati = "";
		for (int i = 2; i < array.length(); i++)
		{
			JSONArray contoIesimo;
			try
			{
				contoIesimo = array.getJSONArray(i);
				// codice conto
				JSONObject anagrContoIesimo = contoIesimo.getJSONObject(0);
				codiceContoIesimo = anagrContoIesimo.getString("CodiceConto");

				if (!codiceContoIesimo.equals(codiceContoConfrontato))
				{
					String stato = anagrContoIesimo.getString("StatoConto");
					if (!stato.equals("UTILIZZABILE"))
					{
						continue;
					} else
					{
						// codice conto utilizzabile diverso, controllo non sia
						// vuoto
						if (contoIesimo.length() > 1)
						{
							if (!codiciConcatenati.contains("-" + codiceContoIesimo))
							{
								// codice conto diverso
								codiceContoConfrontato = codiceContoIesimo;
								codiciConcatenati = codiciConcatenati + "-" + codiceContoIesimo;
								result++;
							}
						} else
						{
							continue;
						}
					}
				}
			} catch (JSONException e)
			{
				e.printStackTrace();
			}
		}
		return result;
	}

	@Override
	public void onBackPressed()
	{
		animation = new AnimationDrawable();
		animation.addFrame(getResources().getDrawable(R.drawable.anim_load2), 300);
		animation.addFrame(getResources().getDrawable(R.drawable.anim_load3), 300);
		animation.addFrame(getResources().getDrawable(R.drawable.anim_load4), 300);
		animation.setOneShot(false);
		imageAnim = (ImageView) findViewById(R.id.waiting_anim);
		imageAnim.setVisibility(View.VISIBLE);
		if (Build.VERSION.SDK_INT < 16)
		{
			imageAnim.setBackgroundDrawable(animation);
		} else
		{
			imageAnim.setBackground(animation);
		}
		// start the animation!
		animation.start();
		Intent intentIndietro = new Intent(getApplicationContext(), DomiciliazioneActivateNonResidenziali.class);
		intentIndietro.putExtra("CodiceConto", getIntent().getStringExtra("CodiceConto"));
		intentIndietro.putExtra("TIPOLOGIA", getIntent().getStringExtra("TIPOLOGIA"));
		DomiciliazioneInserimentoNonResidenziali.this.finish();
		startActivity(intentIndietro);
		overridePendingTransition(R.anim.fade, R.anim.hold);
	}
}
