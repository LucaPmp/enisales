package com.eni.enigaseluce.domiciliazione;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.AnimationDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.eni.enigaseluce.R;
import com.eni.enigaseluce.httpcall.Constants;

@SuppressLint("NewApi")
public class DomiciliazioneHelp extends Activity {
    AnimationDrawable animation;
    ImageView imageAnim;
    String origine;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.domiciliazione_help);
	origine = getIntent().getStringExtra("ORIGINE");
	final ImageView help = (ImageView) findViewById(R.id.domiciliazione_help_indietro);
	help.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
	help.getBackground().invalidateSelf();
	help.setOnClickListener(new View.OnClickListener() {
	    public void onClick(View v1) {
		help.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
		help.getBackground().invalidateSelf();
		animation = new AnimationDrawable();
		animation.addFrame(getResources().getDrawable(R.drawable.anim_load2), 300);
		animation.addFrame(getResources().getDrawable(R.drawable.anim_load3), 300);
		animation.addFrame(getResources().getDrawable(R.drawable.anim_load4), 300);
		animation.setOneShot(false);
		imageAnim = (ImageView) findViewById(R.id.waiting_anim);
		imageAnim.setVisibility(View.VISIBLE);
		if (Build.VERSION.SDK_INT < 16) {
		    imageAnim.setBackgroundDrawable(animation);
		}
		else {
		    imageAnim.setBackground(animation);
		}
		// start the animation!
		animation.start();
		Intent intentHelp = new Intent();
		if (origine.equals(Constants.ORIGINE_DOMICILIAZIONE)) {
		    intentHelp = new Intent(getApplicationContext(), Domiciliazione.class);
		}
		else if (origine.equals(Constants.ORIGINE_DOMICILIAZIONE_ACTIVATE_NON_RESIDENZIALE)) {
		    intentHelp = new Intent(getApplicationContext(), DomiciliazioneActivateNonResidenziali.class);
		    intentHelp.putExtra("CodiceConto", getIntent().getStringExtra("CodiceConto"));
		    intentHelp.putExtra("TIPOLOGIA", getIntent().getStringExtra("TIPOLOGIA"));
		}
		else if (origine.equals(Constants.ORIGINE_DOMICILIAZIONE_DETAIL)) {
		    intentHelp = new Intent(getApplicationContext(), DomiciliazioneClientDetail.class);
		    intentHelp.putExtra("CodiceConto", getIntent().getStringExtra("CodiceConto"));
		    intentHelp.putExtra("IndirizzoFornitura", getIntent().getStringExtra("IndirizzoFornitura"));
		}
		else if (origine.equals(Constants.ORIGINE_DOMICILIAZIONE_INSERIMENTO_PG)) {
		    intentHelp = new Intent(getApplicationContext(), DomiciliazioneInserimentoPersonaGiuridica.class);
		    intentHelp.putExtra("CodiceConto", getIntent().getStringExtra("CodiceConto"));
		    intentHelp.putExtra("TIPOLOGIA", getIntent().getStringExtra("TIPOLOGIA"));
		}
		else if (origine.equals(Constants.ORIGINE_DOMICILIAZIONE_INSERIMENTO_PF)) {
		    intentHelp = new Intent(getApplicationContext(), DomiciliazioneInserimentoPersonaFisica.class);
		    intentHelp.putExtra("CodiceConto", getIntent().getStringExtra("CodiceConto"));
		    intentHelp.putExtra("TIPOLOGIA", getIntent().getStringExtra("TIPOLOGIA"));
		}
		else if (origine.equals(Constants.ORIGINE_DOMICILIAZIONE_INSERIMENTO_NON_RESIDENZIALI)) {
		    intentHelp = new Intent(getApplicationContext(), DomiciliazioneInserimentoNonResidenziali.class);
		    intentHelp.putExtra("CodiceConto", getIntent().getStringExtra("CodiceConto"));
		    intentHelp.putExtra("TIPOLOGIA", getIntent().getStringExtra("TIPOLOGIA"));
		}
		else {
		    intentHelp = new Intent(getApplicationContext(), DomiciliazioneActivateResidenziali.class);
		    intentHelp.putExtra("CodiceConto", getIntent().getStringExtra("CodiceConto"));
		    intentHelp.putExtra("TIPOLOGIA", getIntent().getStringExtra("TIPOLOGIA"));
		}

		DomiciliazioneHelp.this.finish();
		startActivity(intentHelp);
		overridePendingTransition(R.anim.fade, R.anim.hold);
	    }
	});

    }

    public void onBackPressed() {
	this.performBackAction();
    }

    private void performBackAction() {
	animation = new AnimationDrawable();
	animation.addFrame(getResources().getDrawable(R.drawable.anim_load2), 300);
	animation.addFrame(getResources().getDrawable(R.drawable.anim_load3), 300);
	animation.addFrame(getResources().getDrawable(R.drawable.anim_load4), 300);
	animation.setOneShot(false);
	imageAnim = (ImageView) findViewById(R.id.waiting_anim);
	imageAnim.setVisibility(View.VISIBLE);
	if (Build.VERSION.SDK_INT < 16) {
	    imageAnim.setBackgroundDrawable(animation);
	}
	else {
	    imageAnim.setBackground(animation);
	}
	// start the animation!
	animation.start();
	Intent intentHelp = new Intent();
	if (origine.equals(Constants.ORIGINE_DOMICILIAZIONE)) {
	    intentHelp = new Intent(getApplicationContext(), Domiciliazione.class);
	}
	else if (origine.equals(Constants.ORIGINE_DOMICILIAZIONE_ACTIVATE_RESIDENZIALE)) {
	    intentHelp = new Intent(getApplicationContext(), DomiciliazioneActivateResidenziali.class);
	    intentHelp.putExtra("CodiceConto", getIntent().getStringExtra("CodiceConto"));
	    intentHelp.putExtra("TIPOLOGIA", getIntent().getStringExtra("TIPOLOGIA"));
	}
	else if (origine.equals(Constants.ORIGINE_DOMICILIAZIONE_ACTIVATE_NON_RESIDENZIALE)) {
	    intentHelp = new Intent(getApplicationContext(), DomiciliazioneActivateNonResidenziali.class);
	    intentHelp.putExtra("CodiceConto", getIntent().getStringExtra("CodiceConto"));
	    intentHelp.putExtra("TIPOLOGIA", getIntent().getStringExtra("TIPOLOGIA"));
	}
	else if (origine.equals(Constants.ORIGINE_DOMICILIAZIONE_INSERIMENTO_PF)) {
	    intentHelp = new Intent(getApplicationContext(), DomiciliazioneInserimentoPersonaFisica.class);
	    intentHelp.putExtra("CodiceConto", getIntent().getStringExtra("CodiceConto"));
	    intentHelp.putExtra("TIPOLOGIA", getIntent().getStringExtra("TIPOLOGIA"));
	}
	else if (origine.equals(Constants.ORIGINE_DOMICILIAZIONE_INSERIMENTO_PG)) {
	    intentHelp = new Intent(getApplicationContext(), DomiciliazioneInserimentoPersonaGiuridica.class);
	    intentHelp.putExtra("CodiceConto", getIntent().getStringExtra("CodiceConto"));
	    intentHelp.putExtra("TIPOLOGIA", getIntent().getStringExtra("TIPOLOGIA"));
	}
	else if (origine.equals(Constants.ORIGINE_DOMICILIAZIONE_INSERIMENTO_NON_RESIDENZIALI)) {
	    intentHelp = new Intent(getApplicationContext(), DomiciliazioneInserimentoNonResidenziali.class);
	    intentHelp.putExtra("CodiceConto", getIntent().getStringExtra("CodiceConto"));
	    intentHelp.putExtra("TIPOLOGIA", getIntent().getStringExtra("TIPOLOGIA"));
	}
	else if (origine.equals(Constants.ORIGINE_DOMICILIAZIONE_DETAIL)) {
	    intentHelp = new Intent(getApplicationContext(), DomiciliazioneClientDetail.class);
	    intentHelp.putExtra("CodiceConto", getIntent().getStringExtra("CodiceConto"));
	    intentHelp.putExtra("IndirizzoFornitura", getIntent().getStringExtra("IndirizzoFornitura"));
	}

	DomiciliazioneHelp.this.finish();
	startActivity(intentHelp);
	overridePendingTransition(R.anim.fade, R.anim.hold);
    }
}
