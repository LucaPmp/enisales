package com.eni.enigaseluce.showcase;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import com.eni.enigaseluce.R;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources.NotFoundException;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewAnimator;

import com.eni.enigaseluce.showcase.animation.AnimationFactory;
import com.eni.enigaseluce.showcase.animation.AnimationFactory.FlipDirection;
import com.eni.enigaseluce.utils.Log;

public class Showcase {

	public static String language;
	public String TAG = "EniShowcase";
	public Context appContext;
	//test
	//public String XmlConfigUrl = "http://www.eni.com/mobile/it_IT/templatedata/ENI-mobile-template/mobile-cross-content/data/app-business-non-utilizzare.xml?rnd=" + Math.random();
	//prod
	public String XmlConfigUrl = "http://www.eni.com/mobile/it_IT/templatedata/ENI-mobile-template/mobile-cross-content/data/app-business.xml?rnd=" + Math.random();
	
	//test
	//public String XmlConfigUrl_en = "http://www.eni.com/mobile/it_IT/templatedata/ENI-mobile-template/mobile-cross-content/data/app-business-eng-non-utilizzare.xml?rnd=" + Math.random();
	//prod
	public String XmlConfigUrl_en = "http://www.eni.com/mobile/it_IT/templatedata/ENI-mobile-template/mobile-cross-content/data/app-business-eng.xml?rnd=" + Math.random();
	public String HtmlConfigUrl = "http://www.eni.com/mobi-app/cross-content/index.html";
	public WebView browser;
	public Boolean firstLoad = true, htmlLoaded = false, xmlLoaded = false, animating = false;
	public ArrayList<String> appList = new ArrayList<String>();
	public ArrayList<String> mjsonArray = new ArrayList<String>();
	final String BTN_ACTIVE_BACKGROUND = "#f9d607";
	final String SHARED_PREFERENCES_NAME = "EniShowcasePreferences";
	public Activity activity;
	public ViewAnimator viewAnimator;
	public TimerTask scanTask;
	public String installedApps = "", mjson = "";
	final ImageView switcher;
	public ProgressBar loader;
	public RelativeLayout loaderContainer;

	public Showcase(Context mAppContext, Activity mActivity) {
		Log.v(TAG, "Showcase, showcase");

		language = "it";

		activity = mActivity;
		appContext = mAppContext;

		viewAnimator = (ViewAnimator) activity.findViewById(R.id.viewFlipper);
		switcher = (ImageView) activity.findViewById(R.id.btnToStore);
		switcher.setVisibility(View.VISIBLE);
		// inizializzo il bottone
		switcher.setOnClickListener(null);
		this.initSwitcher();
		// creao la web view che conterr� il catalogo
		this.initBrowserInstance();

		// controllo lo stato della connettività
		BroadcastReceiver networkStateReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				boolean noConnectivity = intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, false);

				if (noConnectivity) {
					setSwitcherState(false);
				} else {
					setSwitcherState(true);
				}
			}
		};
		IntentFilter filter = new IntentFilter();
		filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
		appContext.registerReceiver(networkStateReceiver, filter);

		initXmlAppList();
	}

	public void onResume() {

		if (htmlLoaded == true) {
			Log.v(TAG, "Resume dell'app");
			this.destroy();
			this.initBrowserInstance();
		}
	}

	// WEB VIEW
	@SuppressLint("ParserError")
	private void initBrowserInstance() {
		Log.v(TAG, "Showcase, createBrowserIstance");

		RelativeLayout webViewContainer = new RelativeLayout(appContext);
		loaderContainer = new RelativeLayout(appContext);

		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
		params.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);

		loader = new ProgressBar(appContext);
		loaderContainer.addView(loader);
		browser = new WebView(appContext);

		webViewContainer.addView(browser);
		webViewContainer.addView(loaderContainer, params);

		viewAnimator.addView(webViewContainer);

		WebSettings webSettings = browser.getSettings();
		webSettings.setJavaScriptEnabled(true);
		webSettings.setBuiltInZoomControls(false);
		browser.setWebViewClient(new WebViewClient() {

			public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
				Log.v(TAG, "Showcase, onReceivedError, errorCode: " + errorCode + ", description: " + description + ", failingUrl: " + failingUrl);
				Toast.makeText(appContext, description, Toast.LENGTH_SHORT).show();
			}

			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				Log.v(TAG, "Showcase, shouldOverrideUrlLoading, url: " + url);

				if (url.contains("closeView")) {
					runAnimation();
					return true;
				}
				if (url.contains("eniapp-")) {
					String appPackage = url.replace("eniapp-", "").replace("://", "");

					Intent i = new Intent(Intent.ACTION_MAIN);
					PackageManager manager = activity.getPackageManager();
					i = manager.getLaunchIntentForPackage(appPackage);
					i.addCategory(Intent.CATEGORY_LAUNCHER);
					activity.startActivity(i);
					return true;
				}

				url = url.replace("https://play.google.com/store/apps/", "market://");
				if (url.contains("market://")) {
					try {
						Intent intent = new Intent(Intent.ACTION_VIEW);
						intent.setData(Uri.parse(url));
						activity.startActivity(intent);
					} catch (Exception e) { // google play app is not installed
						//
					}
					return true;
				}

				if (url.contains(".apk")) {
					try {
						Intent intent = new Intent(Intent.ACTION_VIEW);
						intent.setData(Uri.parse(url));
						activity.startActivity(intent);
					} catch (Exception e) { // google play app is not installed

					}
					return true;
				}

				return false;
			}

			@Override
			public void onLoadResource(WebView view, String url) {
				if (firstLoad == false) {
					if (url.contains("/closeView"))
						runAnimation();
				}
			}

			@Override
			public void onPageFinished(WebView view, String url) {
				Log.v(TAG, "Showcase, onPageFinished - return");
				if (firstLoad == true) {
					loaderContainer.setVisibility(View.GONE);
					firstLoad = false;
					htmlLoaded = true;
					Log.v(TAG, "Showcase, onPageFinished mjson: " + mjson);

					browser.loadUrl("javascript:eniapps.resetAppList();");
					browser.loadUrl("javascript:eniapps.setLang('" + language + "');"); // TODO
					Log.d("TESTING", "Lingua " + language);
					for (int j = 0; j < mjsonArray.size(); j++) {
						browser.loadUrl("javascript:eniapps.appendDea (" + mjsonArray.get(j) + "); ");
					}
					browser.loadUrl("javascript:eniapps.defineInteractions ();");
					Log.v(TAG, "Showcase, installedApps: " + installedApps);
					browser.loadUrl("javascript:eniapps.initWebview (" + installedApps + ");");

					super.onPageFinished(view, url);
				}
			}
		});
	}

	// GESTIONE XML
	public void initXmlAppList() {
		Thread t = new Thread(new Runnable() {

			public void run() {
				try {
					Log.v(TAG, "Showcase, buildXmlAppList");
					// LOAD XML
					String xml = getConfigXml();

					DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
					DocumentBuilder db = null;
					InputStream in = new ByteArrayInputStream(xml.getBytes());

					db = dbf.newDocumentBuilder();
					Document document = db.parse(in);
					parseXML(document);
					installedApps = getInstalledApps();

					// THREAD INTERFACCIA GRAFICA
					activity.runOnUiThread(new Runnable() {

						public void run() {
							setSwitcherState(true);
							checkUpdates();
							xmlLoaded = true;
							Log.v(TAG, "Showcase, buildXmlAppList - caricato XML");
							// CARICO HTML
							Log.v(TAG, "Showcase, buildXmlAppList - carico HTML");
							browser.loadUrl(HtmlConfigUrl);
							browser.loadUrl("javascript:webview = true;");
						}
					});

				} catch (XPathExpressionException e) {
				} catch (NotFoundException e) {
				} catch (ParserConfigurationException e) {
				} catch (SAXException e) {
				} catch (IOException e) {
				}

			}

		});

		t.start();
	}

	public String getConfigXml() {
		Log.v(TAG, "Showcase, getConfigXml");

		String stringXML = "";
		URL url;
		try {
			// Create a URL for the desire d page
			// TODO
			Log.d(TAG, ">>>>>>>> " + language);
			if (language.equals("en"))
				url = new URL(XmlConfigUrl_en);
			else
				url = new URL(XmlConfigUrl);

			BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
			String str;
			while ((str = in.readLine()) != null) {
				stringXML += str;
			}
			in.close();
		} catch (MalformedURLException e) {
		} catch (IOException e) {
		}
		Log.v(TAG, "Showcase, getConfigXml - return");
		return stringXML;
	}

	public void parseXML(Document xmldoc) throws XPathExpressionException, UnsupportedEncodingException {

		XPath xpath = XPathFactory.newInstance().newXPath();
		XPathExpression expr = xpath.compile("//link-element/*");

		Object result = expr.evaluate(xmldoc, XPathConstants.NODESET);
		NodeList LinkElement = (NodeList) result;

		NodeList linkElements = xmldoc.getElementsByTagName("link-element");
		for (int j = 0; j < linkElements.getLength(); j++) {

			mjson = "{";
			Node linkElement = linkElements.item(j);
			NodeList linkElementSubs = linkElement.getChildNodes();

			String dea_ios_stores = "'dea_ios_stores': [  ";
			String dea_android_stores = "'dea_android_stores': [  ";
			String dea_win_stores = "'dea_win_stores': [  ";

			for (int a = 0; a < linkElementSubs.getLength(); a++) {
				Node subNode = linkElementSubs.item(a);

				if (subNode.getNodeName().equalsIgnoreCase("devices")) {
					mjson += "'dea_smartphone' : '" + fixString(subNode.getChildNodes().item(0).getTextContent()) + "', ";
					mjson += "'dea_tablet' : '" + fixString(subNode.getChildNodes().item(1).getTextContent()) + "', ";
				}
				if (subNode.getNodeName().equalsIgnoreCase("os")) {
					mjson += "'dea_ios' : '" + fixString(subNode.getChildNodes().item(0).getTextContent()) + "', ";
					mjson += "'dea_android' : '" + fixString(subNode.getChildNodes().item(1).getTextContent()) + "', ";
					mjson += "'dea_win' : '" + fixString(subNode.getChildNodes().item(2).getTextContent()) + "', ";
				}
				if (subNode.getNodeName().equalsIgnoreCase("category"))
					mjson += "'dea_category' : '" + fixString(subNode.getTextContent()) + "', ";
				if (subNode.getNodeName().equalsIgnoreCase("status"))
					mjson += "'dea_status' : '" + fixString(subNode.getTextContent()) + "', ";
				if (subNode.getNodeName().equalsIgnoreCase("image"))
					mjson += "'dea_image' : '" + fixString(subNode.getTextContent()) + "', ";
				if (subNode.getNodeName().equalsIgnoreCase("image-big"))
					mjson += "'dea_image_big' : '" + fixString(subNode.getTextContent()) + "', ";
				if (subNode.getNodeName().equalsIgnoreCase("title"))
					mjson += "'dea_title' : '" + fixString(subNode.getTextContent()) + "', ";
				if (subNode.getNodeName().equalsIgnoreCase("abstract"))
					mjson += "'dea_abstract' : '" + fixString(subNode.getTextContent()) + "', ";
				if (subNode.getNodeName().equalsIgnoreCase("descrizione"))
					mjson += "'dea_description' : '" + fixString(subNode.getTextContent()) + "', ";
				if (subNode.getNodeName().equalsIgnoreCase("nameAndroid")) {
					mjson += "'dea_name' : '" + fixString(subNode.getTextContent()) + "', ";
					appList.add(subNode.getTextContent());
				}
				if (subNode.getNodeName().equalsIgnoreCase("stores")) {
					NodeList stores = subNode.getChildNodes();
					for (int k = 0; k < stores.getLength(); k++) {

						Node store = stores.item(k);
						NodeList os = store.getChildNodes();

						String os_name = "";
						String os_type = "";
						String os_link = "";
						for (int h = 0; h < os.getLength(); h++) {
							Node osnode = os.item(h);

							if (osnode.getNodeName().equalsIgnoreCase("os"))
								os_name = osnode.getTextContent();
							if (osnode.getNodeName().equalsIgnoreCase("type"))
								os_type = osnode.getTextContent();
							if (osnode.getNodeName().equalsIgnoreCase("link"))
								os_link = osnode.getTextContent();
						}

						String obj = "{ 'type': '" + os_type + "', 'link': '" + os_link + "' }, ";
						Log.v(TAG, "Store name: " + os_name);
						if (os_name.equalsIgnoreCase("ios"))
							dea_ios_stores += obj;
						if (os_name.equalsIgnoreCase("android"))
							dea_android_stores += obj;
						if (os_name.equalsIgnoreCase("win"))
							dea_win_stores += obj;

					}
					dea_ios_stores = dea_ios_stores.substring(0, dea_ios_stores.length() - 2) + "]";
					dea_android_stores = dea_android_stores.substring(0, dea_android_stores.length() - 2) + "]";
					dea_win_stores = dea_win_stores.substring(0, dea_win_stores.length() - 2) + "]";
					Log.v(TAG, "dea_android_stores: " + dea_android_stores);

					mjson += dea_ios_stores + ", " + dea_android_stores + ", " + dea_win_stores + ", ";
				}

			}
			mjson = mjson.substring(0, mjson.length() - 2);
			mjson += "}";
			mjsonArray.add(mjson);
			mjson = URLEncoder.encode(mjson, "UTF-8");
		}
	}

	public void checkUpdates() {
		SharedPreferences sharedPref = appContext.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
		if (sharedPref != null) {
			int tot_app_cache = sharedPref.getInt("app_cache", 0);
			TextView lbl_update = (TextView) activity.findViewById(R.id.newAppCounter);
			FrameLayout fl = (FrameLayout) activity.findViewById(R.id.updateCounter);
			if (appList.size() > tot_app_cache) {
				fl.setVisibility(View.VISIBLE);
				lbl_update.setText("" + (appList.size() - tot_app_cache));
			} else
				fl.setVisibility(View.GONE);

		}
	}

	public String getInstalledApps() {
		String jsonInstalledApps = "{\"apps\":{";
		final PackageManager pm = activity.getPackageManager();

		Log.v(TAG, "Applist: " + appList);

		List<ApplicationInfo> packages = pm.getInstalledApplications(PackageManager.GET_META_DATA);

		int i = 0;
		for (ApplicationInfo packageInfo : packages) {
			Log.v(TAG, packageInfo.packageName.toString());
			if (appList.contains(packageInfo.packageName.toString())) {
				if (i > 0)
					jsonInstalledApps += ",";
				else
					i += 1;
				jsonInstalledApps += "\"" + packageInfo.packageName.toString() + "\":\"true\"";
			}
		}
		/*
		 * if (jsonInstalledApps.length() > 1) jsonInstalledApps =
		 * jsonInstalledApps.substring(0, jsonInstalledApps.length() - 2);
		 */
		DisplayMetrics dm = new DisplayMetrics();
		activity.getWindowManager().getDefaultDisplay().getRealMetrics(dm);
		float widthDPI= dm.xdpi;
		float heightDPI= dm.ydpi;
		int width=dm.widthPixels;
		int height=dm.heightPixels;
//		Log.d("TESTING", "Pix width "+width + " widthDPI "+widthDPI);
//		Log.d("TESTING", "Pix height "+height + " heightDPI "+heightDPI);
		float screenDensity = heightDPI;
		double x = Math.pow(width/widthDPI,2);
		double y = Math.pow(height/heightDPI,2);
		float screenInches = (float) Math.sqrt(x+y);
//		Log.d("TESTING","Screen inches : " + screenInches);
		
		jsonInstalledApps += "},\"self\":\"" + appContext.getPackageName() + "\"";
		jsonInstalledApps += ", \"screen\":{ \"width\":\""+width+"\", \"height\" :\""+height+"\", \"density\": \""+(int)screenDensity+"\", \"inches\": \""+(int)screenInches+ "\"}";
		jsonInstalledApps += "}"; 
//		Log.d("TESTING", "JSON "+installedApps);

		return jsonInstalledApps;
	}

	public boolean isOnline() {
		ConnectivityManager cm = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting())
			return true;
		return false;
	}

	// GESTIONE BOTTONE SWITCHER
	public void setSwitcherState(boolean enabled) {
		if (!enabled) {
			switcher.setEnabled(false);
			switcher.setAlpha(150);
		} else {
			switcher.setEnabled(true);
			switcher.setAlpha(255);
		}
	}

	private void initSwitcher() {
		setSwitcherState(false);
		switcher.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				runAnimation();
				// Salvo le notifiche
				SharedPreferences sharedPref = appContext.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
				Editor editor = sharedPref.edit();
				editor.putInt("app_cache", appList.size());
				editor.commit();
				Log.v(TAG, "Salvato le preferenze");
			}

		});
	}

	// GESTIONE ANIMAZIONE
	private void runAnimation() {
		if (animating == false) {
			animating = true;
			AnimationFactory.flipTransition(viewAnimator, FlipDirection.LEFT_RIGHT, 400);

			checkUpdates();

			final Handler handler = new Handler();
			Timer t = new Timer();
			scanTask = new TimerTask() {
				public void run() {
					handler.post(new Runnable() {
						public void run() {
							animating = false;
							scanTask.cancel();
						}
					});
				}
			};
			t.schedule(scanTask, 800, 1000);
		}
	}

	// DESTROY
	public void destroy() {
		if (browser != null) {
			viewAnimator.removeView(browser);
			browser.removeAllViews();
			browser.destroy();
		}
		this.activity.finish();
	}

	public String fixString(String str) {
		str = str.replace("'", "\\'");
		return str;
	}

}
