package com.eni.enigaseluce.consumigas;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.http.cookie.Cookie;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.PorterDuff;
import android.graphics.drawable.AnimationDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.eni.enigaseluce.R;
import com.eni.enigaseluce.autolettura.Autolettura;
import com.eni.enigaseluce.error.OopsPageGraficoGasAutolettura;
import com.eni.enigaseluce.error.OopsPageSessioneScaduta;
import com.eni.enigaseluce.error.Utils;
import com.eni.enigaseluce.font.EniFont;
import com.eni.enigaseluce.httpcall.Constants;
import com.eni.enigaseluce.httpcall.ServiceCall;
import com.eni.enigaseluce.httpcall.ServiceCallFactory;
import com.eni.enigaseluce.utils.Utilities;

@SuppressLint("NewApi")
public class ConsumiGasGraficoConsumi extends Activity {

    private Context context;
    private static Context staticContext;
    AnimationDrawable animation;
    ImageView imageAnim;

    static LayoutInflater inflaterDialog;

    String primoConto;
    String classificazione;
    String riferimento;
    String category;
    int periodo;
    int coloreGrafico;
    String utilizzo;
    String[] arrayMesi = {"Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre"};

    public String rispostaLogin = Constants.SERVIZIO_LOGIN;
    public String rispostaConsumiLuce = Constants.SERVIZIO_CONSUMI_GAS;

    String datiGrafico = "";
    private EniFont goto_grafico_gas_button;

    public static Context getContext() {
        return ConsumiGasGraficoConsumi.staticContext;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.consumi_gas_grafico_consumi);

        context = this;
        ConsumiGasGraficoConsumi.staticContext = this;
        primoConto = getIntent().getStringExtra("PRIMOCONTO");
        classificazione = getIntent().getStringExtra("Classificazione");
        periodo = getIntent().getIntExtra("PERIODO", Constants.RETAIL_PERIODO_MESI_6);
        riferimento = getIntent().getStringExtra("RIFERIMENTO");
        category = getIntent().getStringExtra("CATEGORY");
        coloreGrafico = getIntent().getIntExtra("coloreGrafico", 0);
        utilizzo = getIntent().getStringExtra("utilizzo");
    }

    @Override
    protected void onStart() {
        super.onStart();
        primoConto = getIntent().getStringExtra("PRIMOCONTO");

        boolean landscape = context.getResources().getBoolean(R.bool.is_landscape);
        if (!landscape) {
            portrait();
        } else {
            landscape();

            ImageView icona_land = (ImageView) findViewById(R.id.icona_land);
            icona_land.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                }
            });
        }

    }

    private void portrait() {
        final ImageView indietro = (ImageView) findViewById(R.id.consumi_luce_indietro);
        indietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
        indietro.getBackground().invalidateSelf();
        indietro.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                indietro.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
                indietro.getBackground().invalidateSelf();

                animation = Utilities.animation(ConsumiGasGraficoConsumi.this);
                imageAnim = (ImageView) findViewById(R.id.waiting_anim);
                imageAnim.setVisibility(View.VISIBLE);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    imageAnim.setBackgroundDrawable(animation);
                } else {
                    imageAnim.setBackground(animation);
                }
                animation.start();

                Intent intentHome = new Intent(getApplicationContext(), ConsumiGas.class);
                intentHome.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
                intentHome.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
                intentHome.putExtra("CATEGORY", getIntent().getStringExtra("CATEGORY"));
                intentHome.putExtra("Classificazione", getIntent().getStringExtra("Classificazione"));
                intentHome.putExtra("PERIODO", getIntent().getIntExtra("PERIODO", Constants.RETAIL_PERIODO_MESI_6));
                intentHome.putExtra("RIFERIMENTO", getIntent().getStringExtra("RIFERIMENTO"));
                ConsumiGasGraficoConsumi.this.finish();
                startActivity(intentHome);
                overridePendingTransition(R.anim.fade, R.anim.hold);
            }
        });

		
		/*final ImageView help = (ImageView) findViewById(R.id.consumi_luce_help);
        help.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
		help.getBackground().invalidateSelf();
		help.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				help.getBackground().setColorFilter(0xFF999999,	PorterDuff.Mode.MULTIPLY);
				help.getBackground().invalidateSelf();
				animation = new AnimationDrawable();
				animation.addFrame(getResources().getDrawable(R.drawable.anim_load2), 300);
				animation.addFrame(getResources().getDrawable(R.drawable.anim_load3), 300);
				animation.addFrame(getResources().getDrawable(R.drawable.anim_load4), 300);
				animation.setOneShot(false);
				imageAnim = (ImageView) findViewById(R.id.waiting_consumi_luce);
				imageAnim.setVisibility(View.VISIBLE);
				if (Build.VERSION.SDK_INT < 16) {
					imageAnim.setBackgroundDrawable(animation);
				} else {
					imageAnim.setBackground(animation);
				} // start the animation!
				animation.start();

				Intent intentHelp = new Intent(getApplicationContext(),	ConsumiGasHelp.class);
				intentHelp.putExtra("ORIGINE",Constants.ORIGINE_CONSUMI_GAS_GRAFICO_CONSUMI);
				intentHelp.putExtra("Classificazione", getIntent().getStringExtra("Classificazione"));
				intentHelp.putExtra("CATEGORY",	getIntent().getStringExtra("CATEGORY"));
				intentHelp.putExtra("RIFERIMENTO", getIntent().getStringExtra("RIFERIMENTO"));
				intentHelp.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
				intentHelp.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
				intentHelp.putExtra("PERIODO", getIntent().getIntExtra("PERIODO", Constants.RETAIL_PERIODO_MESI_6));
				intentHelp.putExtra("coloreGrafico", coloreGrafico);
				intentHelp.putExtra("utilizzo", getIntent().getStringExtra("utilizzo"));
				ConsumiGasGraficoConsumi.this.finish();
				startActivity(intentHelp);
				overridePendingTransition(R.anim.fade, R.anim.hold);
			}
		});*/


        inflaterDialog = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);

        goto_grafico_gas_button = (EniFont) findViewById(R.id.goto_grafico_gas_button);
        goto_grafico_gas_button.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
        goto_grafico_gas_button.getBackground().invalidateSelf();
        goto_grafico_gas_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                goto_grafico_gas_button.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
                goto_grafico_gas_button.getBackground().invalidateSelf();
                chiamataLoadFornitureGas();
            }
        });

        final TextView testo_dinamico_1 = (TextView) findViewById(R.id.testo_dinamico_1);
        final TextView testo_dinamico_2 = (TextView) findViewById(R.id.testo_dinamico_2);
        if (classificazione.equals("RETAIL")) {
            testo_dinamico_1.setText(Html.fromHtml("Periodo " + "<b>" + periodo + " mesi</b>"));
            testo_dinamico_2.setText(Html.fromHtml("Utilizzo " + "<b>" + utilizzo + "</b>"));
        } else if (classificazione.equals("PMI")) {
            testo_dinamico_1.setText(Html.fromHtml("Periodo " + "<b>" + periodo + " mesi</b>"));
            testo_dinamico_2.setText(Html.fromHtml("Settore merceologico " + "<b>" + category + "</b>"));
        }
        final WebView webView = (WebView) findViewById(R.id.webView);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);

        // webView.getSettings().setBuiltInZoomControls(true);
        if (Build.VERSION.SDK_INT >= 11) {
            webView.loadUrl("file:///android_asset/grafici/index_gas.htm");
            datiGrafico = "$(document).ready(function(){";
            String categories = "";

            int limite = calcolaLimitePeriodo(rispostaConsumiLuce, classificazione, periodo);

            if (classificazione.equals("RETAIL")) {
                String consumo = "";
                String areaData = "";
                String nonDisp = "";
                try {
                    JSONArray consumiLuce = new JSONArray(rispostaConsumiLuce);
                    JSONObject consumi = consumiLuce.getJSONObject(1);
                    JSONArray arrayConsumi = consumi.getJSONArray("Consumi");
                    for (int i = (arrayConsumi.length() - limite); i <= arrayConsumi.length() - 1; i++) {
                        JSONObject consumoIesimo = arrayConsumi.getJSONObject(i);
                        String anno = consumoIesimo.getString("Anno");
                        String mese = consumoIesimo.getString("Mese");
                        String consumoI = consumoIesimo.getString("Consumo");
                        int consumoInt = arrotondaFloat(Float.parseFloat(consumoI));
                        String consumoS = "" + (consumoInt);
                        if (i == arrayConsumi.length() - 1) {
                            categories = categories + "'" + ((mese.substring(0, 3)) + " " + anno) + "'";
                            if (consumoInt > 0) {
                                consumo = consumo + consumoS;
                                nonDisp = nonDisp + "null";
                            } else {
                                consumo = consumo + "null";
                                nonDisp = nonDisp + "0";
                            }
                        } else {
                            categories = categories + "'" + ((mese.substring(0, 3)) + " " + anno) + "',";
                            if (consumoInt > 0) {
                                consumo = consumo + consumoS + ",";
                                nonDisp = nonDisp + "null,";
                            } else {
                                consumo = consumo + "null,";
                                nonDisp = nonDisp + "0,";
                            }
                        }
                    }
                    JSONObject riferimentoObject = consumiLuce.getJSONObject(2);
                    JSONArray arrayRiferimento = riferimentoObject.getJSONArray("Riferimento");
                    JSONArray arrayProfilo = new JSONArray();
                    if (riferimento.equals(Constants.RETAIL_RIFERIMENTO_MINI)) {
                        JSONObject objectRiferimentoMini = arrayRiferimento.getJSONObject(0);
                        arrayProfilo = objectRiferimentoMini.getJSONArray("RiferimentoMini");
                    } else if (riferimento.equals(Constants.RETAIL_RIFERIMENTO_MIDI)) {
                        JSONObject objectRiferimentoMini = arrayRiferimento.getJSONObject(0);
                        arrayProfilo = objectRiferimentoMini.getJSONArray("RiferimentoMidi");
                    } else if (riferimento.equals(Constants.RETAIL_RIFERIMENTO_MAXI)) {
                        JSONObject objectRiferimentoMini = arrayRiferimento.getJSONObject(0);
                        arrayProfilo = objectRiferimentoMini.getJSONArray("RiferimentoMaxi");
                    }
                    for (int j = (arrayProfilo.length() - limite); j <= arrayProfilo.length() - 1; j++) {
                        JSONObject consumoProfiloIesimo = arrayProfilo.getJSONObject(j);
                        String consumoProfiloI = consumoProfiloIesimo.getString("Consumo");
                        if (j == arrayProfilo.length() - 1) {
                            areaData = areaData + consumoProfiloI;
                        } else {
                            areaData = areaData + consumoProfiloI + ",";
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                datiGrafico += "categories = [" + categories + "];"
                        + "consumo = [" + consumo + "];"
                        + "nonDisp = [" + nonDisp + "];"
                        + "areaData = [ " + areaData + "];"
                        + "coloreGrafico = " + coloreGrafico + ";";
                datiGrafico = datiGrafico + "loadGraphRetail();";
                datiGrafico = datiGrafico + "});";

            } else if (classificazione.equals("PMI")) {
                String consumo = "";
                String areaData = "";
                String nonDisp = "";
                try {
                    JSONArray consumiLuce = new JSONArray(rispostaConsumiLuce);
                    JSONObject consumi = consumiLuce.getJSONObject(1);
                    JSONArray arrayConsumi = consumi.getJSONArray("Consumi");
                    for (int i = (arrayConsumi.length() - limite); i <= arrayConsumi.length() - 1; i++) {
                        JSONObject consumoIesimo = arrayConsumi.getJSONObject(i);
                        String anno = consumoIesimo.getString("Anno");
                        String mese = consumoIesimo.getString("Mese");
                        String consumoI = consumoIesimo.getString("Consumo");
                        int consumoInt = arrotondaFloat(Float.parseFloat(consumoI));
                        String consumoS = "" + (consumoInt);
                        if (i == arrayConsumi.length() - 1) {
                            categories = categories + "'" + ((mese.substring(0, 3)) + " " + anno) + "'";
                            if (consumoInt > 0) {
                                consumo = consumo + consumoS;
                                nonDisp = nonDisp + "null";
                            } else {
                                consumo = consumo + "null";
                                nonDisp = nonDisp + "0";
                            }
                        } else {
                            categories = categories + "'" + ((mese.substring(0, 3)) + " " + anno) + "',";
                            if (consumoInt > 0) {
                                consumo = consumo + consumoS + ",";
                                nonDisp = nonDisp + "null,";
                            } else {
                                consumo = consumo + "null,";
                                nonDisp = nonDisp + "0,";
                            }
                        }
                    }
                    JSONObject riferimentoObject = consumiLuce.getJSONObject(2);
                    JSONArray arrayRiferimento = riferimentoObject.getJSONArray("Riferimento");
                    JSONArray arrayProfilo = new JSONArray();
                    if (riferimento.equals(Constants.PMI_RIFERIMENTO_CIVILE)) {
                        JSONObject objectRiferimentoBaseload = arrayRiferimento.getJSONObject(0);
                        arrayProfilo = objectRiferimentoBaseload.getJSONArray("RiferimentoCivile");
                    } else if (riferimento.equals(Constants.PMI_RIFERIMENTO_CONDOMINIO)) {
                        JSONObject objectRiferimentoPeak = arrayRiferimento.getJSONObject(1);
                        arrayProfilo = objectRiferimentoPeak.getJSONArray("RiferimentoCondominio");
                    } else if (riferimento.equals(Constants.PMI_RIFERIMENTO_PARTITAIVA)) {
                        JSONObject objectRiferimentoOffpeak = arrayRiferimento.getJSONObject(2);
                        arrayProfilo = objectRiferimentoOffpeak.getJSONArray("RiferimentoPartitaIva");
                    }
                    for (int j = (arrayProfilo.length() - limite); j <= arrayProfilo.length() - 1; j++) {
                        JSONObject consumoProfiloIesimo = arrayProfilo.getJSONObject(j);
                        String consumoProfiloI = consumoProfiloIesimo.getString("Consumo");
                        if (j == arrayProfilo.length() - 1) {
                            areaData = areaData + consumoProfiloI;
                        } else {
                            areaData = areaData + consumoProfiloI + ",";
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                datiGrafico += "categories = [" + categories + "];"
                        + "consumo = [" + consumo + "];"
                        + "nonDisp = [" + nonDisp + "];"
                        + "areaData = [ " + areaData + "];"
                        + "coloreGrafico = " + coloreGrafico + ";";
                datiGrafico = datiGrafico + "loadGraphRetail();";
                datiGrafico = datiGrafico + "});";
            }

        } else {
            webView.loadUrl("file:///android_asset/grafici/index_luce_no_graph.htm");
        }

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                loadJavascript(view, datiGrafico);
            }
        });
    }


    private void landscape() {
        final WebView webView = (WebView) findViewById(R.id.webView);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        // webView.getSettings().setBuiltInZoomControls(true);
        ImageView legenda_img = (ImageView) findViewById(R.id.img_sopra_gas);

        if (coloreGrafico == 0) {
            // verde
            legenda_img.setBackgroundResource(R.drawable.green_dot);
        } else if (coloreGrafico == 1) {
            // giallo
            legenda_img.setBackgroundResource(R.drawable.yellow_dot);
        } else if (coloreGrafico == 2) {
            // grosso
            legenda_img.setBackgroundResource(R.drawable.red_dot);
        }
        if (Build.VERSION.SDK_INT >= 11) {
            webView.loadUrl("file:///android_asset/grafici/index_gas_land.htm");
            datiGrafico = "$(document).ready(function(){";
            String categories = "";

            int limite = calcolaLimitePeriodo(rispostaConsumiLuce, classificazione, periodo);

            if (classificazione.equals("RETAIL")) {
                String consumo = "";
                String areaData = "";
                String nonDisp = "";
                try {
                    JSONArray consumiLuce = new JSONArray(rispostaConsumiLuce);
                    JSONObject consumi = consumiLuce.getJSONObject(1);
                    JSONArray arrayConsumi = consumi.getJSONArray("Consumi");
                    for (int i = (arrayConsumi.length() - limite); i <= arrayConsumi.length() - 1; i++) {
                        JSONObject consumoIesimo = arrayConsumi.getJSONObject(i);
                        String anno = consumoIesimo.getString("Anno");
                        String mese = consumoIesimo.getString("Mese");
                        String consumoI = consumoIesimo.getString("Consumo");
                        int consumoInt = arrotondaFloat(Float.parseFloat(consumoI));
                        String consumoS = "" + (consumoInt);
                        if (i == arrayConsumi.length() - 1) {
                            categories = categories + "'" + ((mese.substring(0, 3)) + " " + anno) + "'";
                            if (consumoInt > 0) {
                                consumo = consumo + consumoS;
                                nonDisp = nonDisp + "null";
                            } else {
                                consumo = consumo + "null";
                                nonDisp = nonDisp + "0";
                            }
                        } else {
                            categories = categories + "'" + ((mese.substring(0, 3)) + " " + anno) + "',";
                            if (consumoInt > 0) {
                                consumo = consumo + consumoS + ",";
                                nonDisp = nonDisp + "null,";
                            } else {
                                consumo = consumo + "null,";
                                nonDisp = nonDisp + "0,";
                            }
                        }
                    }
                    JSONObject riferimentoObject = consumiLuce.getJSONObject(2);
                    JSONArray arrayRiferimento = riferimentoObject.getJSONArray("Riferimento");
                    JSONArray arrayProfilo = new JSONArray();
                    if (riferimento.equals(Constants.RETAIL_RIFERIMENTO_MINI)) {
                        JSONObject objectRiferimentoMini = arrayRiferimento.getJSONObject(0);
                        arrayProfilo = objectRiferimentoMini.getJSONArray("RiferimentoMini");
                    } else if (riferimento.equals(Constants.RETAIL_RIFERIMENTO_MIDI)) {
                        JSONObject objectRiferimentoMini = arrayRiferimento.getJSONObject(0);
                        arrayProfilo = objectRiferimentoMini.getJSONArray("RiferimentoMidi");
                    } else if (riferimento.equals(Constants.RETAIL_RIFERIMENTO_MAXI)) {
                        JSONObject objectRiferimentoMini = arrayRiferimento.getJSONObject(0);
                        arrayProfilo = objectRiferimentoMini.getJSONArray("RiferimentoMaxi");
                    }
                    for (int j = (arrayProfilo.length() - limite); j <= arrayProfilo.length() - 1; j++) {
                        JSONObject consumoProfiloIesimo = arrayProfilo.getJSONObject(j);
                        String consumoProfiloI = consumoProfiloIesimo.getString("Consumo");
                        if (j == arrayProfilo.length() - 1) {
                            areaData = areaData + consumoProfiloI;
                        } else {
                            areaData = areaData + consumoProfiloI + ",";
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                datiGrafico += "categories = [" + categories + "];"
                        + "consumo = [" + consumo + "];"
                        + "nonDisp = [" + nonDisp + "];"
                        + "areaData = [ " + areaData + "];"
                        + "coloreGrafico = " + coloreGrafico + ";";
                datiGrafico = datiGrafico + "loadGraphRetail();";
                datiGrafico = datiGrafico + "});";
            } else if (classificazione.equals("PMI")) {
                String consumo = "";
                String areaData = "";
                String nonDisp = "";
                try {
                    JSONArray consumiLuce = new JSONArray(rispostaConsumiLuce);
                    JSONObject consumi = consumiLuce.getJSONObject(1);
                    JSONArray arrayConsumi = consumi.getJSONArray("Consumi");
                    for (int i = (arrayConsumi.length() - limite); i <= arrayConsumi.length() - 1; i++) {
                        JSONObject consumoIesimo = arrayConsumi.getJSONObject(i);
                        String anno = consumoIesimo.getString("Anno");
                        String mese = consumoIesimo.getString("Mese");
                        String consumoI = consumoIesimo.getString("Consumo");
                        int consumoInt = arrotondaFloat(Float.parseFloat(consumoI));
                        String consumoS = "" + (consumoInt);
                        if (i == arrayConsumi.length() - 1) {
                            categories = categories + "'" + ((mese.substring(0, 3)) + " " + anno) + "'";
                            if (consumoInt > 0) {
                                consumo = consumo + consumoS;
                                nonDisp = nonDisp + "null";
                            } else {
                                consumo = consumo + "null";
                                nonDisp = nonDisp + "0";
                            }
                        } else {
                            categories = categories + "'" + ((mese.substring(0, 3)) + " " + anno) + "',";
                            if (consumoInt > 0) {
                                consumo = consumo + consumoS + ",";
                                nonDisp = nonDisp + "null,";
                            } else {
                                consumo = consumo + "null,";
                                nonDisp = nonDisp + "0,";
                            }
                        }
                    }
                    JSONObject riferimentoObject = consumiLuce.getJSONObject(2);
                    JSONArray arrayRiferimento = riferimentoObject.getJSONArray("Riferimento");
                    JSONArray arrayProfilo = new JSONArray();
                    if (riferimento.equals(Constants.PMI_RIFERIMENTO_CIVILE)) {
                        JSONObject objectRiferimentoBaseload = arrayRiferimento.getJSONObject(0);
                        arrayProfilo = objectRiferimentoBaseload.getJSONArray("RiferimentoCivile");
                    } else if (riferimento.equals(Constants.PMI_RIFERIMENTO_CONDOMINIO)) {
                        JSONObject objectRiferimentoPeak = arrayRiferimento.getJSONObject(1);
                        arrayProfilo = objectRiferimentoPeak.getJSONArray("RiferimentoCondominio");
                    } else if (riferimento.equals(Constants.PMI_RIFERIMENTO_PARTITAIVA)) {
                        JSONObject objectRiferimentoOffpeak = arrayRiferimento.getJSONObject(2);
                        arrayProfilo = objectRiferimentoOffpeak.getJSONArray("RiferimentoPartitaIva");
                    }
                    for (int j = (arrayProfilo.length() - limite); j <= arrayProfilo.length() - 1; j++) {
                        JSONObject consumoProfiloIesimo = arrayProfilo.getJSONObject(j);
                        String consumoProfiloI = consumoProfiloIesimo.getString("Consumo");
                        if (j == arrayProfilo.length() - 1) {
                            areaData = areaData + consumoProfiloI;
                        } else {
                            areaData = areaData + consumoProfiloI + ",";
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                datiGrafico += "categories = [" + categories + "];"
                        + "consumo = [" + consumo + "];"
                        + "nonDisp = [" + nonDisp + "];"
                        + "areaData = [ " + areaData + "];"
                        + "coloreGrafico = " + coloreGrafico + ";";
                datiGrafico = datiGrafico + "loadGraphRetail();";
                datiGrafico = datiGrafico + "});";
            }
        } else {
            webView.loadUrl("file:///android_asset/grafici/index_luce_no_graph.htm");
        }

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                loadJavascript(view, datiGrafico);
            }

        });
    }

    private int calcolaLimitePeriodo(String rispostaConsumiLuce, String classificazione, int periodo) {
        int result = 0;
        Calendar now = Calendar.getInstance();
        now.setTime(new Date());
        String meseCorrente = arrayMesi[now.get(Calendar.MONTH)];
        int annoCorrente = now.get(Calendar.YEAR);
        try {
            JSONArray array = new JSONArray(rispostaConsumiLuce);
            JSONObject consumi = array.getJSONObject(1);
            JSONArray arrayConsumi = consumi.getJSONArray("Consumi");
            JSONObject ultimoObject = arrayConsumi.getJSONObject(arrayConsumi.length() - 1);
            String annoUltimo = ultimoObject.getString("Anno");
            String meseUltimo = ultimoObject.getString("Mese");
            int meseUltimoIndex = getMeseIndex(meseUltimo);
            int annoUltimoInt = Integer.valueOf(annoUltimo);

            if (arrayConsumi.length() >= periodo) {
                int limite = 0;
                if (annoUltimoInt == annoCorrente) {
                    if (meseCorrente.equals(meseUltimo)) {
                        limite = periodo;
                    } else {
                        int differenza = now.get(Calendar.MONTH) - meseUltimoIndex;
                        limite = periodo - differenza;
                    }
                } else if (annoUltimoInt < annoCorrente) {
                    int differenza = 12 - meseUltimoIndex;
                    limite = periodo - differenza;
                }
                result = limite;
            } else {
                int limite = 0;
                limite = arrayConsumi.length();
                result = limite;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    private int getMeseIndex(String mese) {
        int result = -1;
        for (int i = 0; i < arrayMesi.length; i++) {
            if (arrayMesi[i].equals(mese)) {
                result = i;
            }
        }
        return result;
    }

    private int arrotondaFloat(float numero) {
        int result = 0;

        result = (int) (numero + 0.5);
        return result;
    }

    private void chiamataLoadFornitureGas() {

        animation = Utilities.animation(this);
        imageAnim = (ImageView) findViewById(R.id.waiting_anim);
        imageAnim.setVisibility(View.VISIBLE);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
            imageAnim.setBackgroundDrawable(animation);
        } else {
            imageAnim.setBackground(animation);
        }
        animation.start();

        String codiceCliente = getCodiceCliente(rispostaLogin);
        String elencoCodiciCompleti[];
        String parametri = "&codiceCliente=" + codiceCliente;
        try {
            elencoCodiciCompleti = elencoCodiciContiAttiviCompleti(new JSONArray(rispostaLogin));
            for (int i = 0; i < elencoCodiciCompleti.length; i++) {
                String pdf = getPdfGas(new JSONArray(rispostaLogin), elencoCodiciCompleti[i]);
                if (!pdf.equals("")) {
                    parametri = parametri + "&" + "pdf=" + pdf;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (!isNetworkAvailable(context)) {
            if (animation != null) {
                animation.stop();
                imageAnim.setVisibility(View.INVISIBLE);
            }
            Intent intentOops = new Intent(getApplicationContext(), OopsPageGraficoGasAutolettura.class);
            intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.connessione_assente));
            intentOops.putExtra("PARAMETER", parametri);
            startActivity(intentOops);
            overridePendingTransition(R.anim.fade, R.anim.hold);
        } else {
            /********************* loadFornitureInfoGas ******************************/
            ServiceCall autoletturaServiceCall = ServiceCallFactory.createServiceCall(context);
            // autoletturaServiceCall.myCookieStore = new
            // PersistentCookieStore(context);
            /*****************************************************************************/
            try {
                autoletturaServiceCall.executeHttpsGetConsumiGasGrafico(Constants.URL_FORNITURE_GAS_ATTIVE, parametri, ConsumiGasGraficoConsumi.this, null);
            } catch (Exception e) {
                if (animation != null) {
                    animation.stop();
                    imageAnim.setVisibility(View.INVISIBLE);
                }
                Intent intentOops = new Intent(getApplicationContext(), OopsPageGraficoGasAutolettura.class);
                intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
                intentOops.putExtra("PARAMETER", parametri);
                startActivity(intentOops);
                overridePendingTransition(R.anim.fade, R.anim.hold);
            }
            /************************************************************************************/
        }

    }

    public String getCodiceCliente(String rispostaLogin) {
        String result = "";
        try {
            JSONArray loginJson = new JSONArray(rispostaLogin);
            JSONObject datiCliente = (JSONObject) loginJson.get(1);
            result = datiCliente.getString("CodiceCliente");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public String[] elencoCodiciContiAttiviCompleti(JSONArray array) {
        int lunghezza = numeroCodiciContoDiversiAttiviCompleti(array);
        String result[] = new String[lunghezza];
        String codiceContoConfrontato = "";
        String codiceContoIesimo = "";
        String codiciConcatenati = "";
        int count = 0;
        for (int i = 2; i < array.length(); i++) {
            JSONArray contoIesimo;
            try {
                contoIesimo = array.getJSONArray(i);
                // codice conto
                JSONObject anagrContoIesimo = contoIesimo.getJSONObject(0);
                codiceContoIesimo = anagrContoIesimo.getString("CodiceConto");

                if (!codiceContoIesimo.equals(codiceContoConfrontato)) {
                    String stato = anagrContoIesimo.getString("StatoConto");
                    if (!stato.equals("UTILIZZABILE")) {
                        continue;
                    } else {
                        // codice conto utilizzabile diverso, controllo non sia
                        // vuoto
                        if (contoIesimo.length() > 1) {
                            // che non sia gia' presente
                            boolean isPresente = codiciConcatenati.contains("-" + codiceContoIesimo + "-");
                            if (isPresente) {
                                continue;
                            } else {
                                codiceContoConfrontato = codiceContoIesimo;
                                result[count] = codiceContoConfrontato;
                                codiciConcatenati = codiciConcatenati + "-" + codiceContoIesimo + "-";
                                count++;
                            }
                        } else {
                            continue;
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public int numeroCodiciContoDiversiAttiviCompleti(JSONArray array) {
        int result = 0;
        String codiceContoConfrontato = "";
        String codiceContoIesimo = "";
        String codiciConcatenati = "";
        for (int i = 2; i < array.length(); i++) {
            JSONArray contoIesimo;
            try {
                contoIesimo = array.getJSONArray(i);
                // codice conto
                JSONObject anagrContoIesimo = contoIesimo.getJSONObject(0);
                codiceContoIesimo = anagrContoIesimo.getString("CodiceConto");

                if (!codiceContoIesimo.equals(codiceContoConfrontato)) {
                    String stato = anagrContoIesimo.getString("StatoConto");
                    if (!stato.equals("UTILIZZABILE")) {
                        continue;
                    } else {
                        // codice conto utilizzabile diverso, controllo non sia
                        // vuoto
                        if (contoIesimo.length() > 1) {
                            if (!codiciConcatenati.contains("-" + codiceContoIesimo)) {
                                // codice conto diverso
                                codiceContoConfrontato = codiceContoIesimo;
                                codiciConcatenati = codiciConcatenati + "-" + codiceContoIesimo;
                                result++;
                            }
                        } else {
                            continue;
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public String getPdfGas(JSONArray arrayFornitureLogin, String codiceConto) {
        String result = "";
        JSONObject anagrContoIesimo = new JSONObject();
        JSONObject anagrExtraContoIesimo = new JSONObject();
        try {
            for (int i = 2; i < arrayFornitureLogin.length(); i++) {
                JSONArray contoIesimo;
                contoIesimo = arrayFornitureLogin.getJSONArray(i);
                anagrContoIesimo = contoIesimo.getJSONObject(0);

                String codiceContoIesimo = anagrContoIesimo.getString("CodiceConto");
                if (!codiceContoIesimo.equals(codiceConto)) {
                    continue;
                } else {
                    // ho trovato il conto codiceConto
                    anagrExtraContoIesimo = contoIesimo.getJSONObject(1);
                    String tipologia = anagrExtraContoIesimo.getString("Tipologia");
                    if (!tipologia.equals("GAS")) {
                        continue;
                    } else {
                        result = anagrExtraContoIesimo.getString("Pdf");
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;

    }

    public void postChiamata(String rispostaChiamata, List<Cookie> lista, HashMap<String, String> parametri) {
        if (animation != null) {
            animation.stop();
            imageAnim.setVisibility(View.INVISIBLE);
        }
        final ImageView indietro = (ImageView) findViewById(R.id.consumi_luce_indietro);
        indietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
        indietro.getBackground().invalidateSelf();

        JSONArray rispostaJSON = new JSONArray();
        String esito = "";
        try {
            rispostaJSON = new JSONArray(rispostaChiamata);
            JSONObject oggettoRisposta = (JSONObject) rispostaJSON.get(0);
            esito = oggettoRisposta.getString("esito");

        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (esito.equals("200")) {
            Constants.SERVIZIO_FORNITURE_GAS_ATTIVE = rispostaChiamata;
            Intent intentAutolettura = new Intent(getApplicationContext(), Autolettura.class);
            ConsumiGasGraficoConsumi.this.finish();
            startActivity(intentAutolettura);
            overridePendingTransition(R.anim.fade, R.anim.hold);
        } else {
            Intent intentOops = new Intent(getApplicationContext(), OopsPageGraficoGasAutolettura.class);
            intentOops.putExtra("PARAMETER", parametri);
            // errori ops page
            if (esito.equals("309")) {
                intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
                startActivity(intentOops);
                overridePendingTransition(R.anim.fade, R.anim.hold);

            } else if (esito.equals("400")) {
                intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
                startActivity(intentOops);
                overridePendingTransition(R.anim.fade, R.anim.hold);

            } else if (esito.equals("420")) {
                intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
                startActivity(intentOops);
                overridePendingTransition(R.anim.fade, R.anim.hold);

            } else if (esito.equals("449")) {
                intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
                startActivity(intentOops);
                overridePendingTransition(R.anim.fade, R.anim.hold);

            } else if (esito.equals("451")) {
                intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_popup_451));
                startActivity(intentOops);
                overridePendingTransition(R.anim.fade, R.anim.hold);

            }
            // errori popup
            else if (esito.equals("307")) {
                Utils.popup(ConsumiGasGraficoConsumi.this, getResources().getString(R.string.errore_popup_307), "Riprova");

            } else if (esito.equals("435")) {
                Utils.popup(ConsumiGasGraficoConsumi.this, getResources().getString(R.string.errore_popup_435), "Riprova");

            } else if (esito.equals("437")) {
                Utils.popup(ConsumiGasGraficoConsumi.this, getResources().getString(R.string.errore_popup_437), "Riprova");

            } else if (esito.equals("438")) {
                Utils.popup(ConsumiGasGraficoConsumi.this, getResources().getString(R.string.errore_popup_438), "Riprova");

            } else if (esito.equals("443")) {
                Utils.popup(ConsumiGasGraficoConsumi.this, getResources().getString(R.string.errore_popup_443), "Riprova");

            } else if (esito.equals("444")) {
                Utils.popup(ConsumiGasGraficoConsumi.this, getResources().getString(R.string.errore_popup_444), "Riprova");

            } else if (esito.equals("445")) {
                Utils.popup(ConsumiGasGraficoConsumi.this, getResources().getString(R.string.errore_popup_445), "Riprova");

            } else if (esito.equals("446")) {
                Utils.popup(ConsumiGasGraficoConsumi.this, getResources().getString(R.string.errore_popup_446), "Riprova");

            } else if (esito.equals("447")) {
                Utils.popup(ConsumiGasGraficoConsumi.this, getResources().getString(R.string.errore_popup_447), "Riprova");
            } else {
                if (esito.equals("")) {
                    intentOops = new Intent(getApplicationContext(), OopsPageSessioneScaduta.class);
                    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.sessione_scaduta));
                    startActivity(intentOops);
                    overridePendingTransition(R.anim.fade, R.anim.hold);
                }
            }
        }
    }

    public void postChiamataException(boolean exception, String parametri, HashMap<String, String> parametriHash) {
        if (exception) {
            if (animation != null) {
                animation.stop();
                imageAnim.setVisibility(View.INVISIBLE);
            }

            final ImageView indietro = (ImageView) findViewById(R.id.bollette_pagamenti_indietro);
            indietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
            indietro.getBackground().invalidateSelf();

            Intent intentOops = new Intent(getApplicationContext(), OopsPageGraficoGasAutolettura.class);
            intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
            intentOops.putExtra("PARAMETER", parametri);
            startActivity(intentOops);
            overridePendingTransition(R.anim.fade, R.anim.hold);
        }
    }

    private boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager == null)
            return false;
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }

    public void infoPcs(View v) {
        final Dialog dialog = new Dialog(ConsumiGasGraficoConsumi.this, android.R.style.Theme_Translucent_NoTitleBar);
        // final View info_pcs = inflaterDialog.inflate(R.layout.popup_info_pcs, null);
        dialog.setContentView(R.layout.popup_info_pcs);
        dialog.setCancelable(true);
        final LinearLayout chiudi = (LinearLayout) dialog.findViewById(R.id.chiudi);
        chiudi.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public void onBackPressed() {

        animation = Utilities.animation(this);
        imageAnim = (ImageView) findViewById(R.id.waiting_anim);
        imageAnim.setVisibility(View.VISIBLE);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
            imageAnim.setBackgroundDrawable(animation);
        } else {
            imageAnim.setBackground(animation);
        }
        animation.start();

        Intent intentHome = new Intent(getApplicationContext(), ConsumiGas.class);
        intentHome.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
        intentHome.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
        intentHome.putExtra("CATEGORY", getIntent().getStringExtra("CATEGORY"));
        intentHome.putExtra("Classificazione", getIntent().getStringExtra("Classificazione"));
        intentHome.putExtra("PERIODO", getIntent().getIntExtra("PERIODO", Constants.RETAIL_PERIODO_MESI_6));
        intentHome.putExtra("RIFERIMENTO", getIntent().getStringExtra("RIFERIMENTO"));
        ConsumiGasGraficoConsumi.this.finish();
        startActivity(intentHome);
        overridePendingTransition(R.anim.fade, R.anim.hold);
    }

    private void loadJavascript(WebView webView, String datiGrafico) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            webView.evaluateJavascript(datiGrafico, null);
        } else {
            webView.loadUrl("javascript:" + datiGrafico);
        }
    }
}
