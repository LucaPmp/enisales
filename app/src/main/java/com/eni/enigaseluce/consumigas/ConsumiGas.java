package com.eni.enigaseluce.consumigas;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.http.cookie.Cookie;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.AnimationDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.eni.enigaseluce.EniApplication;
import com.eni.enigaseluce.R;
import com.eni.enigaseluce.consumiluce.link.CheckUpEnergetico;
import com.eni.enigaseluce.error.OopsPageConsumiGasFromConsumiGas;
import com.eni.enigaseluce.font.EniFont;
import com.eni.enigaseluce.home.Home;
import com.eni.enigaseluce.httpcall.Constants;
import com.eni.enigaseluce.httpcall.ServiceCall;
import com.eni.enigaseluce.httpcall.ServiceCallFactory;
import com.eni.enigaseluce.login.help.TrovaEni;
import com.eni.enigaseluce.utils.Utilities;

@SuppressLint("NewApi")
public class ConsumiGas extends Activity {

    private Context context;
    private static Context staticContext;
    AnimationDrawable animation;
    ImageView imageAnim;

    static int positionSpinnerCurrentSelected;
    static int positionSpinnerPrecedentSelected;
    static int positionSpinnerRiprovaSelected;

    static ScrollView pager;
    static LayoutInflater inflater;
    static LayoutInflater inflaterDialog;

    String primoConto;
    String classificazione;
    String riferimento;
    int periodo;
    String category;
    String utilizzo;
    String[] arrayMesi = {"Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre"};
    ArrayList<String> array_valori_riferimenti;
    public String rispostaLogin = Constants.SERVIZIO_LOGIN;
    public String rispostaConsumiGas = Constants.SERVIZIO_CONSUMI_GAS;

    public static String nomeInteroCliente = "";
    private String array_conti_spinner[];
    ArrayAdapter<String> adapterSpinnerConti;

    private RelativeLayout tendinaRiga6, tendinaRiga12, tendinaRiga24, tendinaChoosePeriodo;
    private ImageView choose_periodo_selected;
    private View consumi_luce_entry;

    private RelativeLayout semaforo_verde_retail, semaforo_giallo_retail, semaforo_rosso_retail, semaforo_verde_pmi, semaforo_giallo_pmi, semaforo_rosso_pmi;

    private int coloreGrafico;

    public static Context getContext() {
        return ConsumiGas.staticContext;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.consumi_gas);

        ((EniApplication)getApplication()).sendAnalyticsEvent("[Android] - consumi gas");

        positionSpinnerCurrentSelected = getIntent().getIntExtra("INDICE_SPINNER", 0);
        positionSpinnerPrecedentSelected = getIntent().getIntExtra("INDICE_SPINNER", 0);
        positionSpinnerRiprovaSelected = getIntent().getIntExtra("INDICE_SPINNER", 0);

        context = this;
        ConsumiGas.staticContext = this;
        classificazione = getIntent().getStringExtra("Classificazione");
        inizializzaArrayUtilizzoGas();

    }

    @Override
    protected void onStart() {
        super.onStart();
        primoConto = getIntent().getStringExtra("PRIMOCONTO");

        final ImageView indietro = (ImageView) findViewById(R.id.consumi_luce_indietro);
        indietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
        indietro.getBackground().invalidateSelf();
        indietro.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                indietro.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
                indietro.getBackground().invalidateSelf();

                animation = Utilities.animation(ConsumiGas.this);
                imageAnim = (ImageView) findViewById(R.id.waiting_anim);
                imageAnim.setVisibility(View.VISIBLE);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    imageAnim.setBackgroundDrawable(animation);
                } else {
                    imageAnim.setBackground(animation);
                }
                animation.start();

                Intent intentHome = new Intent(getApplicationContext(), Home.class);
                ConsumiGas.this.finish();
                startActivity(intentHome);
                overridePendingTransition(R.anim.fade, R.anim.hold);
            }
        });

        final ImageView help = (ImageView) findViewById(R.id.consumi_luce_help);
        help.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
        help.getBackground().invalidateSelf();
        help.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                help.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
                help.getBackground().invalidateSelf();

                animation = Utilities.animation(ConsumiGas.this);
                imageAnim = (ImageView) findViewById(R.id.waiting_anim);
                imageAnim.setVisibility(View.VISIBLE);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    imageAnim.setBackgroundDrawable(animation);
                } else {
                    imageAnim.setBackground(animation);
                }
                animation.start();

                Intent intentHelp = new Intent(getApplicationContext(), ConsumiGasHelp.class);
                intentHelp.putExtra("ORIGINE", Constants.ORIGINE_CONSUMI_GAS);
                intentHelp.putExtra("Classificazione", classificazione);
                intentHelp.putExtra("PRIMOCONTO", array_conti_spinner[positionSpinnerCurrentSelected].trim());
                intentHelp.putExtra("INDICE_SPINNER", positionSpinnerCurrentSelected);
                intentHelp.putExtra("CATEGORY", category);
                intentHelp.putExtra("PERIODO", periodo);
                intentHelp.putExtra("RIFERIMENTO", riferimento);
                intentHelp.putExtra("utilizzo", utilizzo);
                ConsumiGas.this.finish();
                startActivity(intentHelp);
                overridePendingTransition(R.anim.fade, R.anim.hold);
            }
        });
        // final RelativeLayout consumi_luce_navigation_bar =
        // (RelativeLayout)findViewById(R.id.consumi_luce_navigation_bar);
        // consumi_luce_navigation_bar.setBackgroundResource(R.drawable.pulsanti_footer_on);
        try {
            rispostaLogin = Constants.SERVIZIO_LOGIN;
            final JSONArray servizioLogin = new JSONArray(rispostaLogin);
            int lunghezzaSpinner = numeroCodiciContoGasDiversiUtilizabiliInteri(servizioLogin);

            JSONObject datiAnagr = servizioLogin.getJSONObject(1);
            // ultimo accesso
            String ultimoaccesso = datiAnagr.getString("UltimoAccesso");
            TextView ultimoAccesso = (TextView) findViewById(R.id.ultimo_accesso);
            // ultimoAccesso.setTextSize(11);
            ultimoAccesso.setText(ultimoaccesso);
            // nome cliente
            String nomeClienteString = datiAnagr.getString("NomeCliente");
            String cognomeClienteString = datiAnagr.getString("CognomeCliente");
            TextView nomeCliente = (TextView) findViewById(R.id.nome_cliente);
            nomeInteroCliente = nomeClienteString + " " + cognomeClienteString;
            nomeCliente.setText(nomeClienteString + " " + cognomeClienteString);

            array_conti_spinner = new String[lunghezzaSpinner];
            array_conti_spinner = arrayCodiciContoGasDiversiUtilizabiliInteri(servizioLogin, lunghezzaSpinner);
            final Spinner codiciConti = (Spinner) findViewById(R.id.numero_cliente);
            // final ArrayAdapter<String> adapterSpinnerConti = new
            // ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,
            // array_conti_spinner);
            final ArrayAdapter<String> adapterSpinnerConti = new ArrayAdapter<String>(this, R.layout.spinner_dialog_riga, R.id.spinner_riga_text, array_conti_spinner);
            codiciConti.setAdapter(adapterSpinnerConti);
            codiciConti.setPrompt(getResources().getString(R.string.seleziona_il_conto_cliente));
            // codiciConti.setSelection((positionSpinnerCurrentSelected ==
            // positionSpinnerRiprovaSelected)? positionSpinnerCurrentSelected :
            // positionSpinnerRiprovaSelected);
            codiciConti.setSelection(positionSpinnerCurrentSelected);

            pager = (ScrollView) findViewById(R.id.consumi_luce_pager);
            pager.removeAllViews();
            inflater = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);
            inflaterDialog = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);
            rispostaConsumiGas = Constants.SERVIZIO_CONSUMI_GAS;

            if (!rispostaConsumiGas.equals("")) {
                popolaPaginaContenuto(servizioLogin, new JSONArray(rispostaConsumiGas), pager, primoConto.trim());
            }

            codiciConti.setOnItemSelectedListener(new OnItemSelectedListener() {
                public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                    Object obj = (Object) parent.getSelectedItem();
                    String codiceContoCliente = obj.toString().trim();
                    if (!codiceContoCliente.equals(adapterSpinnerConti.getItem(positionSpinnerCurrentSelected).toString().trim())) {
                        positionSpinnerPrecedentSelected = positionSpinnerCurrentSelected;
                        positionSpinnerCurrentSelected = codiciConti.getLastVisiblePosition();
                        positionSpinnerRiprovaSelected = codiciConti.getLastVisiblePosition();

                        animation = Utilities.animation(ConsumiGas.this);
                        imageAnim = (ImageView) findViewById(R.id.waiting_anim);
                        imageAnim.setVisibility(View.VISIBLE);
                        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                            imageAnim.setBackgroundDrawable(animation);
                        } else {
                            imageAnim.setBackground(animation);
                        }
                        animation.start();

                        String parametri = "";
                        String codiceCliente = getCodiceCliente(rispostaLogin);
                        parametri = parametri + "&codiceCliente=" + codiceCliente;
                        String segmento = getSegmentoCliente(rispostaLogin);
                        parametri = parametri + "&segmento=" + segmento;
                        String tipologiaPdfRegioneProdottoDataattivazione = getTipologiaPdfRegioneUtilizzoGasDataAttivazione(rispostaLogin, codiceContoCliente, segmento);
                        parametri = parametri + tipologiaPdfRegioneProdottoDataattivazione;
                        if (!isNetworkAvailable(context)) {
                            Intent intentOops = new Intent(getApplicationContext(), OopsPageConsumiGasFromConsumiGas.class);
                            intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.connessione_assente));
                            // intentOops.putExtra("PRIMOCONTO",
                            // codiceContoCliente);
                            intentOops.putExtra("PARAMETER", parametri);
                            positionSpinnerCurrentSelected = positionSpinnerPrecedentSelected;
                            startActivity(intentOops);
                            if (animation != null) {
                                animation.stop();
                                imageAnim.setVisibility(View.INVISIBLE);
                            }
                            overridePendingTransition(R.anim.fade, R.anim.hold);
                        } else {
                            /********************* Chiamata consumi gas ******************************/
                            ServiceCall consumiLuceServiceCall = ServiceCallFactory.createServiceCall(context);
                            // consumiLuceServiceCall.myCookieStore = new
                            // PersistentCookieStore(context);

                            try {
                                consumiLuceServiceCall.executeHttpsGetConsumiGas(Constants.URL_CONSUMI_GAS, parametri, ConsumiGas.this, null);
                            } catch (Exception e) {
                                Intent intentOops = new Intent(getApplicationContext(), OopsPageConsumiGasFromConsumiGas.class);
                                intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
                                // intentOops.putExtra("PRIMOCONTO",
                                // codiceContoCliente);
                                intentOops.putExtra("PARAMETER", parametri);
                                positionSpinnerCurrentSelected = positionSpinnerPrecedentSelected;
                                startActivity(intentOops);
                                overridePendingTransition(R.anim.fade, R.anim.hold);
                            }
                        }
                    }
                }

                public void onNothingSelected(AdapterView<?> arg0) {
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void popolaPaginaContenuto(final JSONArray consumiLuceLogin, JSONArray consumiLuce, ScrollView pager, final String primoConto) {
        // try {
        pager.removeAllViews();

        periodo = getIntent().getIntExtra("PERIODO", Constants.RETAIL_PERIODO_MESI_6);

        consumi_luce_entry = inflater.inflate(R.layout.consumi_gas_entry, null);

        LinearLayout sezione_verifica_consumi_retail = (LinearLayout) consumi_luce_entry.findViewById(R.id.sezione_verifica_consumi_retail);
        LinearLayout sezione_verifica_consumi_pmi = (LinearLayout) consumi_luce_entry.findViewById(R.id.sezione_verifica_consumi_pmi);

        semaforo_verde_retail  = (RelativeLayout) consumi_luce_entry.findViewById(R.id.semaforo_verde_contenuto_retail);
        semaforo_giallo_retail = (RelativeLayout) consumi_luce_entry.findViewById(R.id.semaforo_giallo_contenuto_retail);
        semaforo_rosso_retail  = (RelativeLayout) consumi_luce_entry.findViewById(R.id.semaforo_rosso_contenuto_retail);

        semaforo_verde_pmi  = (RelativeLayout) consumi_luce_entry.findViewById(R.id.semaforo_verde_contenuto_pmi);
        semaforo_giallo_pmi = (RelativeLayout) consumi_luce_entry.findViewById(R.id.semaforo_giallo_contenuto_pmi);
        semaforo_rosso_pmi  = (RelativeLayout) consumi_luce_entry.findViewById(R.id.semaforo_rosso_contenuto_pmi);

        tendinaChoosePeriodo = (RelativeLayout) consumi_luce_entry.findViewById(R.id.tendina_choose_periodo);
        tendinaRiga6 = (RelativeLayout) consumi_luce_entry.findViewById(R.id.tendina_riga_6);
        tendinaRiga12 = (RelativeLayout) consumi_luce_entry.findViewById(R.id.tendina_riga_12);
        tendinaRiga24 = (RelativeLayout) consumi_luce_entry.findViewById(R.id.tendina_riga_24);

        tendinaRiga6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tendinaRigaClick(6);
            }
        });

        tendinaRiga12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tendinaRigaClick(12);
            }
        });

        tendinaRiga24.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tendinaRigaClick(24);
            }
        });

        if (classificazione.equals("RETAIL")) {
            sezione_verifica_consumi_retail.setVisibility(View.VISIBLE);
            sezione_verifica_consumi_pmi.setVisibility(View.GONE);

            TextView consumi_luce_fornitura_valore = (TextView) consumi_luce_entry.findViewById(R.id.consumi_luce_fornitura_retail_valore);
            String fornitura = getPdfFromCodiceConto(Constants.SERVIZIO_LOGIN, primoConto);
            consumi_luce_fornitura_valore.setText(fornitura);

            TextView consumi_luce_indirizzo = (TextView) consumi_luce_entry.findViewById(R.id.consumi_luce_retail_indirizzo);
            String indirizzoFornitura = cercaIndirizzoFornituraTipologia(consumiLuceLogin, primoConto, "GAS");
            consumi_luce_indirizzo.setText(indirizzoFornitura);

            TextView utilizzo_valore = (TextView) consumi_luce_entry.findViewById(R.id.utilizzo_valore);
            String tipoUtilizzo = getUtilizzoGas(rispostaLogin, primoConto);
            String tipoUtilizzoText = formattaUtilizzoGas(tipoUtilizzo);
            utilizzo_valore.setText(tipoUtilizzoText);
            utilizzo = tipoUtilizzoText;

            TextView data_attivazione_valore = (TextView) consumi_luce_entry.findViewById(R.id.data_attivazione_valore);
            String dataAttivazione = getDataAttivazione(rispostaLogin, primoConto, "GAS");
            String dataAttivazioneFormattata = convertiData(dataAttivazione);
            data_attivazione_valore.setText(dataAttivazioneFormattata);

            // String riferimentoFromUtilizzoGas = getProfilo(tipoUtilizzo);
            String riferimentoFromConsumiGas = getProfilo(rispostaConsumiGas);
            riferimento = ((getIntent().getStringExtra("RIFERIMENTO") != null) ? ((getIntent().getStringExtra("RIFERIMENTO"))) : riferimentoFromConsumiGas);

            float consumo_medio_luce = calcolaConsumoMedio(Constants.SERVIZIO_CONSUMI_GAS, classificazione, riferimento, periodo);

            semaforo_verde_pmi.setVisibility(View.GONE);
            semaforo_giallo_pmi.setVisibility(View.GONE);
            semaforo_rosso_pmi.setVisibility(View.GONE);

            popolaSemaforoRetail(consumo_medio_luce);



            choose_periodo_selected = (ImageView) consumi_luce_entry.findViewById(R.id.choose_periodo_selected);
            if (periodo == Constants.RETAIL_PERIODO_MESI_6) {
                choose_periodo_selected.setBackgroundResource(R.drawable.choose_6_mesi);
            } else if (periodo == Constants.RETAIL_PERIODO_MESI_12) {
                choose_periodo_selected.setBackgroundResource(R.drawable.choose_12_mesi);
            } else if (periodo == Constants.RETAIL_PERIODO_MESI_24) {
                choose_periodo_selected.setBackgroundResource(R.drawable.choose_24_mesi);
            }
            choose_periodo_selected.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
            choose_periodo_selected.getBackground().invalidateSelf();
            choose_periodo_selected.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    tendinaChoosePeriodo.setVisibility(tendinaChoosePeriodo.getVisibility() == View.GONE ? View.VISIBLE : View.GONE);
                }
            });

        } else if (classificazione.equals("PMI")) {
            riferimento = ((getIntent().getStringExtra("RIFERIMENTO") != null) ? ((getIntent().getStringExtra("RIFERIMENTO"))) : (Constants.PMI_RIFERIMENTO_PARTITAIVA));
            sezione_verifica_consumi_retail.setVisibility(View.GONE);
            sezione_verifica_consumi_pmi.setVisibility(View.VISIBLE);

            TextView consumi_luce_fornitura_valore = (TextView) consumi_luce_entry.findViewById(R.id.consumi_luce_fornitura_pmi_valore);
            String fornitura = getPdfFromCodiceConto(Constants.SERVIZIO_LOGIN, primoConto);
            consumi_luce_fornitura_valore.setText(fornitura);

            TextView consumi_luce_indirizzo = (TextView) consumi_luce_entry.findViewById(R.id.consumi_luce_pmi_indirizzo);
            String indirizzoFornitura = cercaIndirizzoFornituraTipologia(consumiLuceLogin, primoConto, "GAS");
            consumi_luce_indirizzo.setText(indirizzoFornitura);

            float consumo_medio_luce = calcolaConsumoMedio(Constants.SERVIZIO_CONSUMI_GAS, classificazione, riferimento, periodo);
            // test
            //BAccus
           // consumo_medio_luce = 142;
            semaforo_verde_retail.setVisibility(View.GONE);
            semaforo_giallo_retail.setVisibility(View.GONE);
            semaforo_rosso_retail.setVisibility(View.GONE);
            // consumo_medio_luce = (float) 130.0;
            if (consumo_medio_luce <= 120) {
                // verde
                semaforo_verde_pmi.setVisibility(View.VISIBLE);
                semaforo_giallo_pmi.setVisibility(View.GONE);
                semaforo_rosso_pmi.setVisibility(View.GONE);
                coloreGrafico = 0;
            } else if (consumo_medio_luce > 120 && consumo_medio_luce <= 140) {
                // giallo
                semaforo_verde_pmi.setVisibility(View.GONE);
                semaforo_giallo_pmi.setVisibility(View.VISIBLE);
                semaforo_rosso_pmi.setVisibility(View.GONE);
                coloreGrafico = 1;
            } else if (consumo_medio_luce > 140) {
                // rosso
                semaforo_verde_pmi.setVisibility(View.GONE);
                semaforo_giallo_pmi.setVisibility(View.GONE);
                semaforo_rosso_pmi.setVisibility(View.VISIBLE);
                coloreGrafico = 2;
            }

            EniFont settore_merceologico_selected = (EniFont) consumi_luce_entry.findViewById(R.id.settore_merceologico_selected);
            category = ((getIntent().getStringExtra("CATEGORY") != null) ? getIntent().getStringExtra("CATEGORY") : getResources().getString(R.string.settore_merceologico_gas_category_10));
            settore_merceologico_selected.setText(category);
            // quando click su 'i' deve aparire il popup determinazione retail
            final ImageView sett_merceologico_icon = (ImageView) consumi_luce_entry.findViewById(R.id.sett_merceologico_icon);
            sett_merceologico_icon.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
            sett_merceologico_icon.getBackground().invalidateSelf();
            sett_merceologico_icon.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    sett_merceologico_icon.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
                    sett_merceologico_icon.getBackground().invalidateSelf();
                    Intent intentSettoreMerceologico = new Intent(getApplicationContext(), SettoreMerceologicoGas.class);
                    intentSettoreMerceologico.putExtra("Classificazione", classificazione);
                    intentSettoreMerceologico.putExtra("PRIMOCONTO", array_conti_spinner[positionSpinnerCurrentSelected].trim());
                    intentSettoreMerceologico.putExtra("INDICE_SPINNER", positionSpinnerCurrentSelected);
                    intentSettoreMerceologico.putExtra("PERIODO", periodo);
                    intentSettoreMerceologico.putExtra("RIFERIMENTO", riferimento);
                    intentSettoreMerceologico.putExtra("CATEGORY", category);
                    ConsumiGas.this.finish();
                    startActivity(intentSettoreMerceologico);
                    overridePendingTransition(R.anim.fade, R.anim.hold);
                }
            });
            choose_periodo_selected = (ImageView) consumi_luce_entry.findViewById(R.id.choose_periodo_selected);
            if (periodo == Constants.RETAIL_PERIODO_MESI_6) {
                choose_periodo_selected.setBackgroundResource(R.drawable.choose_6_mesi);
            } else if (periodo == Constants.RETAIL_PERIODO_MESI_12) {
                choose_periodo_selected.setBackgroundResource(R.drawable.choose_12_mesi);
            } else if (periodo == Constants.RETAIL_PERIODO_MESI_24) {
                choose_periodo_selected.setBackgroundResource(R.drawable.choose_24_mesi);
            }
            choose_periodo_selected.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
            choose_periodo_selected.getBackground().invalidateSelf();
            choose_periodo_selected.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    tendinaChoosePeriodo.setVisibility(tendinaChoosePeriodo.getVisibility() == View.GONE ? View.VISIBLE : View.GONE);
                }
            });
        }
        final ImageView goto_grafico_gas_button = (ImageView) consumi_luce_entry.findViewById(R.id.goto_grafico_gas_button);
        goto_grafico_gas_button.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
        goto_grafico_gas_button.getBackground().invalidateSelf();
        goto_grafico_gas_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                goto_grafico_gas_button.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
                goto_grafico_gas_button.getBackground().invalidateSelf();
                Intent intentGraficoGas = new Intent(getApplicationContext(), ConsumiGasGraficoConsumi.class);
                intentGraficoGas.putExtra("PROVENIENZA", ConsumiGas.class.getCanonicalName());
                intentGraficoGas.putExtra("PERIODO", periodo);
                intentGraficoGas.putExtra("Classificazione", classificazione);
                intentGraficoGas.putExtra("CATEGORY", category);
                intentGraficoGas.putExtra("RIFERIMENTO", riferimento);
                intentGraficoGas.putExtra("PRIMOCONTO", array_conti_spinner[positionSpinnerCurrentSelected].trim());
                intentGraficoGas.putExtra("INDICE_SPINNER", positionSpinnerCurrentSelected);
                intentGraficoGas.putExtra("coloreGrafico", coloreGrafico);
                intentGraficoGas.putExtra("utilizzo", utilizzo);
                ConsumiGas.this.finish();
                startActivity(intentGraficoGas);
                overridePendingTransition(R.anim.fade, R.anim.hold);
            }
        });
        pager.addView(consumi_luce_entry);
        // } catch (JSONException e) {
        // e.printStackTrace();
        // }
    }

    private int arrotondaFloat(float numero) {
        int result = 0;

        result = (int) (numero + 0.5);
        return result;
    }
    private void popolaSemaforoPmi(float consumo_medio_luce) {
        if (consumo_medio_luce <= 120) {
            // verde
            semaforo_verde_pmi.setVisibility(View.VISIBLE);
            semaforo_giallo_pmi.setVisibility(View.GONE);
            semaforo_rosso_pmi.setVisibility(View.GONE);
            coloreGrafico = 0;
        } else if (consumo_medio_luce > 120 && consumo_medio_luce <= 140) {
            // giallo
            semaforo_verde_pmi.setVisibility(View.GONE);
            semaforo_giallo_pmi.setVisibility(View.VISIBLE);
            semaforo_rosso_pmi.setVisibility(View.GONE);
            coloreGrafico = 1;
        } else if (consumo_medio_luce > 140) {
            // rosso
            semaforo_verde_pmi.setVisibility(View.GONE);
            semaforo_giallo_pmi.setVisibility(View.GONE);
            semaforo_rosso_pmi.setVisibility(View.VISIBLE);
            coloreGrafico = 2;
        }
    }
    private void popolaSemaforoRetail(float consumo_medio_luce) {
        //BAccus
        //consumo_medio_luce = 141;
        if (consumo_medio_luce <= 120) {
            // verde
            semaforo_verde_retail.setVisibility(View.VISIBLE);
            semaforo_giallo_retail.setVisibility(View.GONE);
            semaforo_rosso_retail.setVisibility(View.GONE);
            coloreGrafico = 0;
        } else if (consumo_medio_luce > 120 && consumo_medio_luce <= 140) {
            // giallo
            semaforo_verde_retail.setVisibility(View.GONE);
            semaforo_giallo_retail.setVisibility(View.VISIBLE);
            semaforo_rosso_retail.setVisibility(View.GONE);

            final RelativeLayout desc_giallo = (RelativeLayout) consumi_luce_entry.findViewById(R.id.desc_giallo_retail);
            desc_giallo.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
            desc_giallo.getBackground().invalidateSelf();
            desc_giallo.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    desc_giallo.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
                    desc_giallo.getBackground().invalidateSelf();
                    final Dialog dialog = new Dialog(ConsumiGas.this, android.R.style.Theme_Translucent_NoTitleBar);
                    dialog.setContentView(R.layout.popup_consumi_luce_stato_giallo);
                    dialog.setCancelable(true);

                    TextView titolo = (TextView) dialog.findViewById(R.id.sotto_titolo);
                    titolo.setText(getResources().getString(R.string.consumi_gas_non_in_linea));

                    final LinearLayout chiudi = (LinearLayout) dialog.findViewById(R.id.chiudi);
                    chiudi.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                    ImageView check_up_button = (ImageView) dialog.findViewById(R.id.check_up_button);
                    check_up_button.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            Intent intentCheckUp = new Intent(getApplicationContext(), CheckUpEnergetico.class);
                            intentCheckUp.putExtra("PAGINA_DI_PROVENIENZA", ConsumiGas.class.getCanonicalName());
                            intentCheckUp.putExtra("Classificazione", classificazione);
                            intentCheckUp.putExtra("PRIMOCONTO", array_conti_spinner[positionSpinnerCurrentSelected].trim());
                            intentCheckUp.putExtra("INDICE_SPINNER", positionSpinnerCurrentSelected);
                            intentCheckUp.putExtra("PERIODO", periodo);
                            intentCheckUp.putExtra("RIFERIMENTO", riferimento);
                            intentCheckUp.putExtra("CATEGORY", category);
                            dialog.dismiss();
                            ConsumiGas.this.finish();
                            startActivity(intentCheckUp);
                            overridePendingTransition(R.anim.fade, R.anim.hold);
                        }
                    });
                    dialog.show();
                    desc_giallo.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
                    desc_giallo.getBackground().invalidateSelf();
                }
            });
            coloreGrafico = 1;
        } else if (consumo_medio_luce > 140) {
            // rosso
            semaforo_verde_retail.setVisibility(View.GONE);
            semaforo_giallo_retail.setVisibility(View.GONE);
            semaforo_rosso_retail.setVisibility(View.VISIBLE);

            final RelativeLayout desc_rosso = (RelativeLayout) consumi_luce_entry.findViewById(R.id.desc_rosso_retail);
            desc_rosso.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
            desc_rosso.getBackground().invalidateSelf();
            desc_rosso.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    desc_rosso.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
                    desc_rosso.getBackground().invalidateSelf();
                    final Dialog dialog = new Dialog(ConsumiGas.this, android.R.style.Theme_Translucent_NoTitleBar);
                    dialog.setContentView(R.layout.popup_consumi_luce_stato_rosso);
                    dialog.setCancelable(true);

                    TextView titolo = (TextView) dialog.findViewById(R.id.sotto_titolo);
                    titolo.setText(getResources().getString(R.string.consumi_gas_non_in_linea));

                    final LinearLayout chiudi = (LinearLayout) dialog.findViewById(R.id.chiudi);
                    chiudi.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                    final ImageView goto_energy_store_button = (ImageView) dialog.findViewById(R.id.goto_energy_store_button);
                    goto_energy_store_button.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            goto_energy_store_button.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
                            goto_energy_store_button.getBackground().invalidateSelf();
                            Intent intentEnergyStoreLocator = new Intent(getApplicationContext(), TrovaEni.class);
                            goto_energy_store_button.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
                            goto_energy_store_button.getBackground().invalidateSelf();
                            intentEnergyStoreLocator.putExtra("PAGINA_DI_PROVENIENZA", ConsumiGas.class.getCanonicalName());
                            intentEnergyStoreLocator.putExtra("Classificazione", classificazione);
                            intentEnergyStoreLocator.putExtra("PRIMOCONTO", array_conti_spinner[positionSpinnerCurrentSelected].trim());
                            intentEnergyStoreLocator.putExtra("INDICE_SPINNER", positionSpinnerCurrentSelected);
                            intentEnergyStoreLocator.putExtra("PERIODO", periodo);
                            intentEnergyStoreLocator.putExtra("RIFERIMENTO", riferimento);
                            intentEnergyStoreLocator.putExtra("CATEGORY", category);
                            dialog.dismiss();
                            ConsumiGas.this.finish();
                            startActivity(intentEnergyStoreLocator);
                            overridePendingTransition(R.anim.fade, R.anim.hold);
                        }
                    });

                    dialog.show();
                    desc_rosso.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
                    desc_rosso.getBackground().invalidateSelf();
                }
            });
            coloreGrafico = 2;
        }

    }

    private int calcolaLimitePeriodo(String rispostaConsumiLuce, String classificazione, int periodo) {
        int result = 0;
        Calendar now = Calendar.getInstance();
        now.setTime(new Date());
        String meseCorrente = arrayMesi[now.get(Calendar.MONTH)];
        int annoCorrente = now.get(Calendar.YEAR);
        try {
            JSONArray array = new JSONArray(rispostaConsumiLuce);
            JSONObject consumi = array.getJSONObject(1);
            JSONArray arrayConsumi = consumi.getJSONArray("Consumi");
            JSONObject ultimoObject = arrayConsumi.getJSONObject(arrayConsumi.length() - 1);
            String annoUltimo = ultimoObject.getString("Anno");
            String meseUltimo = ultimoObject.getString("Mese");
            int meseUltimoIndex = getMeseIndex(meseUltimo);
            int annoUltimoInt = Integer.valueOf(annoUltimo);

            if (arrayConsumi.length() >= periodo) {
                int limite = 0;
                if (annoUltimoInt == annoCorrente) {
                    if (meseCorrente.equals(meseUltimo)) {
                        limite = periodo;
                    } else {
                        int differenza = now.get(Calendar.MONTH) - meseUltimoIndex;
                        limite = periodo - differenza;
                    }
                } else if (annoUltimoInt < annoCorrente) {
                    int differenza = 12 - meseUltimoIndex;
                    limite = periodo - differenza;
                }
                result = limite;
            } else {
                int limite = 0;
                limite = arrayConsumi.length();
                result = limite;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    private float calcolaConsumoMedio(String rispostaConsumiLuce, String classificazione, String riferimento, int periodo) {
        float result = 0;
        int limite = calcolaLimitePeriodo(rispostaConsumiLuce, classificazione, periodo);

        try {
            JSONArray array = new JSONArray(rispostaConsumiLuce);

            JSONObject consumi = array.getJSONObject(1);
            JSONArray arrayConsumi = consumi.getJSONArray("Consumi");

            if (classificazione.equals("RETAIL")) {
                JSONObject riferimentoObject = array.getJSONObject(2);
                JSONArray arrayRiferimento = riferimentoObject.getJSONArray("Riferimento");
                JSONArray arrayProfilo = new JSONArray();
                if (riferimento.equals(Constants.RETAIL_RIFERIMENTO_MINI)) {
                    JSONObject objectRiferimentoMini = arrayRiferimento.getJSONObject(0);
                    arrayProfilo = objectRiferimentoMini.getJSONArray("RiferimentoMini");
                } else if (riferimento.equals(Constants.RETAIL_RIFERIMENTO_MIDI)) {
                    JSONObject objectRiferimentoMini = arrayRiferimento.getJSONObject(0);
                    arrayProfilo = objectRiferimentoMini.getJSONArray("RiferimentoMidi");
                } else if (riferimento.equals(Constants.RETAIL_RIFERIMENTO_MAXI)) {
                    JSONObject objectRiferimentoMini = arrayRiferimento.getJSONObject(0);
                    arrayProfilo = objectRiferimentoMini.getJSONArray("RiferimentoMaxi");
                }
                float sommaConsumiFascie = 0;
                float sommaConsumiProfili = 0;
                for (int i = arrayConsumi.length() - 1; i >= (arrayConsumi.length() - limite); i--) {
                    JSONObject consumoIesimo = arrayConsumi.getJSONObject(i);
                    String consumo = consumoIesimo.getString("Consumo");
                    int consumoInt = arrotondaFloat(Float.valueOf(consumo));
                    sommaConsumiFascie = sommaConsumiFascie + consumoInt;
                    if (consumoInt > 0) {
                        JSONObject consumoProfiloIesimo = arrayProfilo.getJSONObject(i);
                        String consumoProfiloI = consumoProfiloIesimo.getString("Consumo");
                        sommaConsumiProfili = sommaConsumiProfili + arrotondaFloat(Float.valueOf(consumoProfiloI));
                    }
                }
                if (sommaConsumiProfili > 0) {
                    result = 100 * (sommaConsumiFascie / sommaConsumiProfili);
                } else {
                    result = 0;
                }
            } else if (classificazione.equals("PMI")) {
                JSONObject riferimentoObject = array.getJSONObject(2);
                JSONArray arrayRiferimento = riferimentoObject.getJSONArray("Riferimento");
                JSONArray arrayProfilo = new JSONArray();
                if (riferimento.equals(Constants.PMI_RIFERIMENTO_CIVILE)) {
                    JSONObject objectRiferimentoMini = arrayRiferimento.getJSONObject(0);
                    arrayProfilo = objectRiferimentoMini.getJSONArray("RiferimentoCivile");
                } else if (riferimento.equals(Constants.PMI_RIFERIMENTO_CONDOMINIO)) {
                    JSONObject objectRiferimentoMini = arrayRiferimento.getJSONObject(1);
                    arrayProfilo = objectRiferimentoMini.getJSONArray("RiferimentoCondominio");
                } else if (riferimento.equals(Constants.PMI_RIFERIMENTO_PARTITAIVA)) {
                    JSONObject objectRiferimentoMini = arrayRiferimento.getJSONObject(2);
                    arrayProfilo = objectRiferimentoMini.getJSONArray("RiferimentoPartitaIva");
                }
                float sommaConsumiFascie = 0;
                float sommaConsumiProfili = 0;
                for (int i = arrayConsumi.length() - 1; i >= (arrayConsumi.length() - limite); i--) {
                    JSONObject consumoIesimo = arrayConsumi.getJSONObject(i);
                    String consumo = consumoIesimo.getString("Consumo");
                    int consumoInt = arrotondaFloat(Float.valueOf(consumo));
                    sommaConsumiFascie = sommaConsumiFascie + consumoInt;
                    if (consumoInt > 0) {
                        JSONObject consumoProfiloIesimo = arrayProfilo.getJSONObject(i);
                        String consumoProfiloI = consumoProfiloIesimo.getString("Consumo");
                        sommaConsumiProfili = sommaConsumiProfili + arrotondaFloat(Float.valueOf(consumoProfiloI));
                    }
                }
                if (sommaConsumiProfili > 0) {
                    result = 100 * (sommaConsumiFascie / sommaConsumiProfili);
                } else {
                    result = 0;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    private int getMeseIndex(String mese) {
        int result = -1;
        for (int i = 0; i < arrayMesi.length; i++) {
            if (arrayMesi[i].equals(mese)) {
                result = i;
            }
        }
        return result;
    }

    private String cercaIndirizzoFornituraTipologia(JSONArray array, String codiceConto, String tipologia) {
        String result = "";
        String codiceContoIesimo = "";
        JSONObject anagrContoIesimo = new JSONObject();
        try {
            for (int i = 2; i < array.length(); i++) {
                JSONArray contoIesimo;

                contoIesimo = array.getJSONArray(i);
                // codice conto
                anagrContoIesimo = contoIesimo.getJSONObject(0);
                codiceContoIesimo = anagrContoIesimo.getString("CodiceConto");

                if (!codiceContoIesimo.equals(codiceConto)) {
                    // codice conto diverso
                    continue;
                } else {
                    // codice conto utilizzabile diverso, controllo non sia
                    // vuoto
                    if (contoIesimo.length() < 2) {
                        continue;
                    } else {
                        JSONObject anagrExtraContoIesimo = contoIesimo.getJSONObject(1);
                        String tipologiaConto = anagrExtraContoIesimo.getString("Tipologia");
                        if (!tipologiaConto.equals(tipologia)) {
                            // codice conto diverso
                            continue;
                        } else {
                            result = anagrExtraContoIesimo.getString("IndirizzoCompleto");
                        }
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public int numeroCodiciContoGasDiversiUtilizabiliInteri(JSONArray array) {
        int result = 0;
        String codiceContoIesimo = "";
        String codiciConcatenati = "";
        for (int i = 2; i < array.length(); i++) {
            JSONArray contoIesimo;
            try {
                contoIesimo = array.getJSONArray(i);
                // codice conto
                JSONObject anagrContoIesimo = contoIesimo.getJSONObject(0);
                if (contoIesimo.length() < 2) {
                    continue;
                } else {
                    String stato = anagrContoIesimo.getString("StatoConto");
                    if (!stato.equals("UTILIZZABILE")) {
                        continue;
                    } else {
                        JSONObject anagrExtraContoIesimo = contoIesimo.getJSONObject(1);
                        String tipologia = anagrExtraContoIesimo.getString("Tipologia");
                        if (!tipologia.equals("GAS")) {
                            continue;
                        } else {
                            // trovato conto luce utilizzabile pieno
                            codiceContoIesimo = anagrContoIesimo.getString("CodiceConto");
                            boolean isPresente = codiciConcatenati.contains("-" + codiceContoIesimo + "-");
                            if (isPresente) {
                                continue;
                            } else {
                                codiciConcatenati = codiciConcatenati + "-" + codiceContoIesimo + "-";
                                result++;
                            }
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public String[] arrayCodiciContoGasDiversiUtilizabiliInteri(JSONArray array, int lunghezza) {
        String result[] = new String[lunghezza];
        String codiceContoIesimo = "";
        String codiciConcatenati = "";
        int count = 0;
        for (int i = 2; i < array.length(); i++) {
            JSONArray contoIesimo;
            try {
                contoIesimo = array.getJSONArray(i);
                // codice conto
                JSONObject anagrContoIesimo = contoIesimo.getJSONObject(0);
                if (contoIesimo.length() < 2) {
                    continue;
                } else {
                    String stato = anagrContoIesimo.getString("StatoConto");
                    if (!stato.equals("UTILIZZABILE")) {
                        continue;
                    } else {
                        JSONObject anagrExtraContoIesimo = contoIesimo.getJSONObject(1);
                        String tipologia = anagrExtraContoIesimo.getString("Tipologia");
                        if (!tipologia.equals("GAS")) {
                            continue;
                        } else {
                            // trovato conto luce utilizzabile pieno
                            codiceContoIesimo = anagrContoIesimo.getString("CodiceConto");
                            boolean isPresente = codiciConcatenati.contains("-" + codiceContoIesimo + "-");
                            if (isPresente) {
                                continue;
                            } else {
                                result[count] = "  " + codiceContoIesimo;
                                codiciConcatenati = codiciConcatenati + "-" + codiceContoIesimo + "-";
                                count++;
                            }
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public String getCodiceCliente(String rispostaLogin) {
        String result = "";
        try {
            JSONArray loginJson = new JSONArray(rispostaLogin);
            JSONObject datiCliente = (JSONObject) loginJson.get(1);
            result = datiCliente.getString("CodiceCliente");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public String getSegmentoCliente(String rispostaLogin) {
        String result = "";
        try {
            JSONArray loginJson = new JSONArray(rispostaLogin);
            JSONObject datiCliente = (JSONObject) loginJson.get(1);
            result = datiCliente.getString("SegmentoCliente");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public String getTipologiaPdfRegioneUtilizzoGasDataAttivazione(String stringLogin, String codiceContoGas, String segmento) {
        String result = "";
        JSONObject anagrContoIesimo = new JSONObject();
        JSONObject anagrExtraContoIesimo = new JSONObject();
        try {
            JSONArray arrayLogin = new JSONArray(stringLogin);
            JSONObject anagrCliente = arrayLogin.getJSONObject(1);
            String tipologia = anagrCliente.getString("TipologiaCliente");
            result = result + "&tipologia=" + tipologia;
            for (int i = 2; i < arrayLogin.length(); i++) {
                JSONArray contoIesimo;
                contoIesimo = arrayLogin.getJSONArray(i);
                anagrContoIesimo = contoIesimo.getJSONObject(0);
                String codiceConto = anagrContoIesimo.getString("CodiceConto");
                if (!codiceConto.equals(codiceContoGas)) {
                    continue;
                } else {
                    if (contoIesimo.length() < 2) {
                        continue;
                    } else {
                        anagrExtraContoIesimo = contoIesimo.getJSONObject(1);
                        String tipologiaLuceGase = anagrExtraContoIesimo.getString("Tipologia");
                        if (!tipologiaLuceGase.equals("GAS")) {
                            continue;
                        } else {
                            // trovato il conto
                            // tipologia, pdf, regione, prodotto,
                            // dataAttivazione
                            String pdf = anagrExtraContoIesimo.getString("Pdf");
                            result = result + "&pdf=" + pdf;
                            String regione = anagrExtraContoIesimo.getString("Regione");
                            result = result + "&regione=" + regione;
                            String utilizzoGas = getUtilizzoGas(stringLogin, codiceContoGas);
                            if (!utilizzoGas.equals("") && (tipologia.equals("RETAIL") && segmento.equals("RESIDENTIAL"))) {
                                result = result + "&tipoUtilizzoGas=" + utilizzoGas;
                            }
                            String dataAttivazione = getDataAttivazione(stringLogin, codiceContoGas, "GAS");
                            if (!dataAttivazione.equals("")) {
                                result = result + "&dataAttivazione=" + dataAttivazione;
                            }
                        }
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public String getUtilizzoGas(String stringLogin, String codiceContoLuce) {
        String result = "";
        JSONObject anagrContoIesimo = new JSONObject();
        JSONObject anagrExtraContoIesimo = new JSONObject();
        try {
            JSONArray arrayLogin = new JSONArray(stringLogin);
            for (int i = 2; i < arrayLogin.length(); i++) {
                JSONArray contoIesimo;
                contoIesimo = arrayLogin.getJSONArray(i);
                anagrContoIesimo = contoIesimo.getJSONObject(0);
                if (contoIesimo.length() < 2) {
                    continue;
                } else {
                    anagrExtraContoIesimo = contoIesimo.getJSONObject(1);
                    String codiceConto = anagrContoIesimo.getString("CodiceConto");
                    String tipologia = anagrExtraContoIesimo.getString("Tipologia");
                    if (!tipologia.equals("GAS")) {
                        continue;
                    } else {
                        if (!codiceConto.equals(codiceContoLuce)) {
                            continue;
                        } else {
                            // trovato il conto
                            result = anagrExtraContoIesimo.getString("TipoUtilizzoGas");
                            result = Uri.encode(result);
                        }
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public String getDataAttivazione(String stringLogin, String codiceContoLuce, String luceOgas) {
        String result = "";
        JSONObject anagrContoIesimo = new JSONObject();
        JSONObject anagrExtraContoIesimo = new JSONObject();
        try {
            JSONArray arrayLogin = new JSONArray(stringLogin);
            for (int i = 2; i < arrayLogin.length(); i++) {
                JSONArray contoIesimo;
                contoIesimo = arrayLogin.getJSONArray(i);
                anagrContoIesimo = contoIesimo.getJSONObject(0);
                if (contoIesimo.length() < 2) {
                    continue;
                } else {
                    anagrExtraContoIesimo = contoIesimo.getJSONObject(1);
                    String codiceConto = anagrContoIesimo.getString("CodiceConto");
                    if (!codiceConto.equals(codiceContoLuce)) {
                        continue;
                    } else {
                        // trovato il conto
                        String tipologia = anagrExtraContoIesimo.getString("Tipologia");
                        if (!tipologia.equals(luceOgas)) {
                            continue;
                        } else {
                            result = anagrExtraContoIesimo.getString("DataAttivazione");
                        }
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    private boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager == null) return false;
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }

    public void postChiamata(String rispostaChiamata, List<Cookie> lista, String parametri) {
        if (animation != null) {
            animation.stop();
            imageAnim.setVisibility(View.INVISIBLE);
        }

        final ImageView indietro = (ImageView) findViewById(R.id.consumi_luce_indietro);
        indietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
        indietro.getBackground().invalidateSelf();

        JSONArray rispostaJSON = new JSONArray();
        String esito = "";
        String descEsito = "";
        try {
            rispostaJSON = new JSONArray(rispostaChiamata);
            JSONObject oggettoRisposta = (JSONObject) rispostaJSON.get(0);
            esito = oggettoRisposta.getString("esito");
            descEsito = oggettoRisposta.getString("descEsito");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (esito.equals("200")) {
            Constants.SERVIZIO_CONSUMI_GAS = rispostaChiamata;
            rispostaConsumiGas = rispostaChiamata;
            try {
                String codiceConto = getCodiceContoFromPdf(rispostaLogin, parametri.substring(parametri.indexOf("pdf=") + 4, parametri.indexOf("&regione=")));
                popolaPaginaContenuto(new JSONArray(rispostaLogin), new JSONArray(Constants.SERVIZIO_CONSUMI_GAS), pager, codiceConto);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            Intent intentOops = new Intent(getApplicationContext(), OopsPageConsumiGasFromConsumiGas.class);
            positionSpinnerCurrentSelected = positionSpinnerPrecedentSelected;
            intentOops.putExtra("PARAMETER", parametri);
            // String codiceConto = getCodiceContoFromPdf(rispostaLogin,
            // parametri.substring(parametri.indexOf("pdf=")+ 4,
            // parametri.indexOf("&regione=")));
            // intentOops.putExtra("PRIMOCONTO", codiceConto);
            intentOops.putExtra("MESSAGGIO", descEsito);
            startActivity(intentOops);
            overridePendingTransition(R.anim.fade, R.anim.hold);
        }

    }

    public void postChiamataException(boolean exception, String parametri) {
        if (exception) {
            if (animation != null) {
                animation.stop();
                imageAnim.setVisibility(View.INVISIBLE);
            }
            final ImageView indietro = (ImageView) findViewById(R.id.bollette_pagamenti_indietro);
            indietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
            indietro.getBackground().invalidateSelf();

            Intent intentOops = new Intent(getApplicationContext(), OopsPageConsumiGasFromConsumiGas.class);
            positionSpinnerCurrentSelected = positionSpinnerPrecedentSelected;
            intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
            intentOops.putExtra("PARAMETER", parametri);
            // String codiceConto = getCodiceContoFromPdf(rispostaLogin,
            // parametri.substring(parametri.indexOf("pdf=")+ 4,
            // parametri.indexOf("&regione=")));
            // intentOops.putExtra("PRIMOCONTO", codiceConto);
            startActivity(intentOops);
            overridePendingTransition(R.anim.fade, R.anim.hold);
        }
    }

    public String getCodiceContoFromPdf(String stringLogin, String pdfConto) {
        String result = "";
        JSONObject anagrContoIesimo = new JSONObject();
        JSONObject anagrExtraContoIesimo = new JSONObject();
        try {
            JSONArray arrayLogin = new JSONArray(stringLogin);
            for (int i = 2; i < arrayLogin.length(); i++) {
                JSONArray contoIesimo;
                contoIesimo = arrayLogin.getJSONArray(i);
                anagrContoIesimo = contoIesimo.getJSONObject(0);
                String statoconto = anagrContoIesimo.getString("StatoConto");
                if (contoIesimo.length() < 2 || !statoconto.equals("UTILIZZABILE")) {
                    continue;
                } else {
                    anagrExtraContoIesimo = contoIesimo.getJSONObject(1);
                    String tipologiaLuceGas = anagrExtraContoIesimo.getString("Tipologia");
                    if (!tipologiaLuceGas.equals("GAS")) {
                        continue;
                    } else {
                        String pdf = anagrExtraContoIesimo.getString("Pdf");
                        if (!pdf.equals(pdfConto)) {
                            continue;
                        } else {
                            // trovato il conto
                            String codice = anagrContoIesimo.getString("CodiceConto");
                            result = codice;
                            // return result;
                        }
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public String getPdfFromCodiceConto(String stringLogin, String codiceConto) {
        String result = "";
        JSONObject anagrContoIesimo = new JSONObject();
        JSONObject anagrExtraContoIesimo = new JSONObject();
        try {
            JSONArray arrayLogin = new JSONArray(stringLogin);
            for (int i = 2; i < arrayLogin.length(); i++) {
                JSONArray contoIesimo;
                contoIesimo = arrayLogin.getJSONArray(i);
                anagrContoIesimo = contoIesimo.getJSONObject(0);
                if (contoIesimo.length() < 2) {
                    continue;
                } else {
                    anagrExtraContoIesimo = contoIesimo.getJSONObject(1);
                    String tipologia = anagrExtraContoIesimo.getString("Tipologia");
                    if (!tipologia.equals("GAS")) {
                        continue;
                    } else {
                        String codiceContoIesimo = anagrContoIesimo.getString("CodiceConto");
                        if (!codiceContoIesimo.equals(codiceConto)) {
                            continue;
                        } else {
                            // trovato il conto luce
                            result = anagrExtraContoIesimo.getString("Pdf");
                        }
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    private void inizializzaArrayUtilizzoGas() {
        array_valori_riferimenti = new ArrayList<String>();
        array_valori_riferimenti.add(getResources().getString(R.string.tipologia_utilizzo_gas_1));
        array_valori_riferimenti.add(getResources().getString(R.string.tipologia_utilizzo_gas_2));
        array_valori_riferimenti.add(getResources().getString(R.string.tipologia_utilizzo_gas_3));
        array_valori_riferimenti.add(getResources().getString(R.string.tipologia_utilizzo_gas_4));
        array_valori_riferimenti.add(getResources().getString(R.string.tipologia_utilizzo_gas_5));
        array_valori_riferimenti.add(getResources().getString(R.string.tipologia_utilizzo_gas_6));
        array_valori_riferimenti.add(getResources().getString(R.string.tipologia_utilizzo_gas_7));
        array_valori_riferimenti.add(getResources().getString(R.string.tipologia_utilizzo_gas_8));
        array_valori_riferimenti.add(getResources().getString(R.string.tipologia_utilizzo_gas_9));
        array_valori_riferimenti.add(getResources().getString(R.string.tipologia_utilizzo_gas_10));
        array_valori_riferimenti.add(getResources().getString(R.string.tipologia_utilizzo_gas_11));
        array_valori_riferimenti.add(getResources().getString(R.string.tipologia_utilizzo_gas_12));
        array_valori_riferimenti.add(getResources().getString(R.string.tipologia_utilizzo_gas_13));
        array_valori_riferimenti.add(getResources().getString(R.string.tipologia_utilizzo_gas_14));
        array_valori_riferimenti.add(getResources().getString(R.string.tipologia_utilizzo_gas_15));
        array_valori_riferimenti.add(getResources().getString(R.string.tipologia_utilizzo_gas_16));
        array_valori_riferimenti.add(getResources().getString(R.string.tipologia_utilizzo_gas_17));
        array_valori_riferimenti.add(getResources().getString(R.string.tipologia_utilizzo_gas_18));
        array_valori_riferimenti.add(getResources().getString(R.string.tipologia_utilizzo_gas_19));
        array_valori_riferimenti.add(getResources().getString(R.string.tipologia_utilizzo_gas_20));
        array_valori_riferimenti.add(getResources().getString(R.string.tipologia_utilizzo_gas_21));
        array_valori_riferimenti.add(getResources().getString(R.string.tipologia_utilizzo_gas_22));
        array_valori_riferimenti.add(getResources().getString(R.string.tipologia_utilizzo_gas_23));
        array_valori_riferimenti.add(getResources().getString(R.string.tipologia_utilizzo_gas_24));
        array_valori_riferimenti.add(getResources().getString(R.string.tipologia_utilizzo_gas_25));
        array_valori_riferimenti.add(getResources().getString(R.string.tipologia_utilizzo_gas_26));
        array_valori_riferimenti.add(getResources().getString(R.string.tipologia_utilizzo_gas_27));
        array_valori_riferimenti.add(getResources().getString(R.string.tipologia_utilizzo_gas_28));
        array_valori_riferimenti.add(getResources().getString(R.string.tipologia_utilizzo_gas_29));
        array_valori_riferimenti.add(getResources().getString(R.string.tipologia_utilizzo_gas_30));
    }

    /*
     * private String getProfilo(String utilizzoGas){ String result = "";
     * 
     * if(
     * utilizzoGas.equals(getResources().getString(R.string.tipologia_utilizzo_gas_1
     * )) ||
     * utilizzoGas.equals(getResources().getString(R.string.tipologia_utilizzo_gas_2
     * )) ||
     * utilizzoGas.equals(getResources().getString(R.string.tipologia_utilizzo_gas_6
     * )) ||
     * utilizzoGas.equals(getResources().getString(R.string.tipologia_utilizzo_gas_9
     * )) || utilizzoGas.equals(getResources().getString(R.string.
     * tipologia_utilizzo_gas_13)) ||
     * utilizzoGas.equals(getResources().getString
     * (R.string.tipologia_utilizzo_gas_18)) ||
     * utilizzoGas.equals(getResources()
     * .getString(R.string.tipologia_utilizzo_gas_19)) ||
     * utilizzoGas.equals(getResources
     * ().getString(R.string.tipologia_utilizzo_gas_21)) ||
     * utilizzoGas.equals(getResources
     * ().getString(R.string.tipologia_utilizzo_gas_22))){ result =
     * Constants.RETAIL_RIFERIMENTO_MINI; }else if(
     * utilizzoGas.equals(getResources
     * ().getString(R.string.tipologia_utilizzo_gas_4)) ||
     * utilizzoGas.equals(getResources
     * ().getString(R.string.tipologia_utilizzo_gas_5)) ||
     * utilizzoGas.equals(getResources
     * ().getString(R.string.tipologia_utilizzo_gas_10)) ||
     * utilizzoGas.equals(getResources
     * ().getString(R.string.tipologia_utilizzo_gas_11)) ||
     * utilizzoGas.equals(getResources
     * ().getString(R.string.tipologia_utilizzo_gas_16)) ||
     * utilizzoGas.equals(getResources
     * ().getString(R.string.tipologia_utilizzo_gas_17)) ||
     * utilizzoGas.equals(getResources
     * ().getString(R.string.tipologia_utilizzo_gas_20)) ||
     * utilizzoGas.equals(getResources
     * ().getString(R.string.tipologia_utilizzo_gas_23)) ||
     * utilizzoGas.equals(getResources
     * ().getString(R.string.tipologia_utilizzo_gas_24)) ||
     * utilizzoGas.equals(getResources
     * ().getString(R.string.tipologia_utilizzo_gas_26)) ||
     * utilizzoGas.equals(getResources
     * ().getString(R.string.tipologia_utilizzo_gas_27)) ||
     * utilizzoGas.equals(getResources
     * ().getString(R.string.tipologia_utilizzo_gas_28))){ result =
     * Constants.RETAIL_RIFERIMENTO_MIDI; }else if(
     * utilizzoGas.equals(getResources
     * ().getString(R.string.tipologia_utilizzo_gas_3)) ||
     * utilizzoGas.equals(getResources
     * ().getString(R.string.tipologia_utilizzo_gas_7)) ||
     * utilizzoGas.equals(getResources
     * ().getString(R.string.tipologia_utilizzo_gas_8)) ||
     * utilizzoGas.equals(getResources
     * ().getString(R.string.tipologia_utilizzo_gas_12)) ||
     * utilizzoGas.equals(getResources
     * ().getString(R.string.tipologia_utilizzo_gas_14)) ||
     * utilizzoGas.equals(getResources
     * ().getString(R.string.tipologia_utilizzo_gas_15)) ||
     * utilizzoGas.equals(getResources
     * ().getString(R.string.tipologia_utilizzo_gas_25)) ||
     * utilizzoGas.equals(getResources
     * ().getString(R.string.tipologia_utilizzo_gas_29)) ||
     * utilizzoGas.equals(getResources
     * ().getString(R.string.tipologia_utilizzo_gas_30))){ result =
     * Constants.RETAIL_RIFERIMENTO_MAXI; }
     * 
     * return result;
     * 
     * }
     */
    private String getProfilo(String risposta_consumi_gas) {
        String result = "";
        try {
            JSONArray array = new JSONArray(risposta_consumi_gas);
            JSONObject riferimentoObject = array.getJSONObject(2);
            JSONArray arrayRiferimento = riferimentoObject.getJSONArray("Riferimento");
            JSONObject objectRiferimento = arrayRiferimento.getJSONObject(0);
            JSONArray nomiKeys = objectRiferimento.names();
            // nomiKeys e' sempre 1 (c'� sempre solo 1 oggetto dentro l'array)
            String nomeProfiloRiferimento = (String) nomiKeys.get(0);
            String profiloS = nomeProfiloRiferimento.substring(11, nomeProfiloRiferimento.length());
            if (profiloS.equals("Mini")) {
                result = Constants.RETAIL_RIFERIMENTO_MINI;
            } else if (profiloS.equals("Midi")) {
                result = Constants.RETAIL_RIFERIMENTO_MIDI;
            } else if (profiloS.equals("Maxi")) {
                result = Constants.RETAIL_RIFERIMENTO_MAXI;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    private String formattaUtilizzoGas(String utilizzoGas) {
        String result = "";

        String senzaSpazioGet = utilizzoGas.replaceAll("%20", " ");

        // String senzaPiu = senzaSpazioGet.replaceAll("\\+", ", ");

        String primaLettera = senzaSpazioGet.substring(0, 1);

        String restoStringa = senzaSpazioGet.substring(1, senzaSpazioGet.length());

        String restoStringaMinuscolo = restoStringa.toLowerCase();

        result = primaLettera.concat(restoStringaMinuscolo);

        result = result.replaceAll("%2b", "+");

        return result;
    }

    private String convertiData(String data) {
        String result;
        // SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH);
        Date dataEmis = new Date();
        try {
            dataEmis = sdf.parse(data);
        } catch (ParseException e) {
            // e.printStackTrace();
        }

        SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
        result = sdf1.format(dataEmis);
        return result;
    }

    public void onBackPressed() {

        animation = Utilities.animation(this);
        imageAnim = (ImageView) findViewById(R.id.waiting_anim);
        imageAnim.setVisibility(View.VISIBLE);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
            imageAnim.setBackgroundDrawable(animation);
        } else {
            imageAnim.setBackground(animation);
        }
        animation.start();

        Intent intentHome = new Intent(getApplicationContext(), Home.class);
        ConsumiGas.this.finish();
        startActivity(intentHome);
        overridePendingTransition(R.anim.fade, R.anim.hold);
    }

    private void tendinaRigaClick(int riga){
        tendinaChoosePeriodo.setVisibility(View.GONE);

        switch (riga){
            case 6:
                choose_periodo_selected.setBackgroundResource(R.drawable.choose_6_mesi);
                periodo = Constants.RETAIL_PERIODO_MESI_6;
                break;
            case 12:
                choose_periodo_selected.setBackgroundResource(R.drawable.choose_12_mesi);
                periodo = Constants.RETAIL_PERIODO_MESI_12;
                break;
            case 24:
                choose_periodo_selected.setBackgroundResource(R.drawable.choose_24_mesi);
                periodo = Constants.RETAIL_PERIODO_MESI_24;
                break;
        }

        float consumo_medio_luce = calcolaConsumoMedio(Constants.SERVIZIO_CONSUMI_GAS, classificazione, riferimento, periodo);
        if (classificazione.compareToIgnoreCase("retail")==0 ){
            popolaSemaforoRetail(consumo_medio_luce);
        }else if (classificazione.compareToIgnoreCase("pmi")==0 ){
            popolaSemaforoPmi(consumo_medio_luce);
        }
    }
}
