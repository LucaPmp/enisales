package com.eni.enigaseluce.autolettura;

import java.util.HashMap;
import java.util.List;

import org.apache.http.cookie.Cookie;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.AnimationDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.eni.enigaseluce.EniApplication;
import com.eni.enigaseluce.R;
import com.eni.enigaseluce.bollettePagamenti.BollettePagamenti;
import com.eni.enigaseluce.bollettePagamenti.BollettePagamentiProssimaBolletta;
import com.eni.enigaseluce.error.OopsPageAutoletturaAutolettura;
import com.eni.enigaseluce.error.OopsPageSessioneScaduta;
import com.eni.enigaseluce.error.Utils;
import com.eni.enigaseluce.home.Home;
import com.eni.enigaseluce.httpcall.Constants;
import com.eni.enigaseluce.httpcall.ServiceCall;
import com.eni.enigaseluce.httpcall.ServiceCallFactory;
import com.eni.enigaseluce.pager.HorizontalPager;
import com.eni.enigaseluce.utils.JSONUtility;
import com.eni.enigaseluce.utils.Utilities;

@SuppressLint("NewApi")
public class Autolettura extends Activity {

    public final static String SOURCE_PAGE = "sourcePage";

    AnimationDrawable animation;
    ImageView imageAnim;
    Context context;
    static Context staticContext;
    /**
     * Called when the activity is first created.
     */

    public String rispostaLogin = Constants.SERVIZIO_LOGIN;
    public String rispostaUltimeLetture = Constants.SERVIZIO_FORNITURE_GAS_ATTIVE;

    String codiceEsito;

    String currentCodiceConto;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.autolettura);

        ((EniApplication)getApplication()).sendAnalyticsEvent("[Android] - autolettura gas");

        context = this;
        Autolettura.staticContext = this;

    }

    public static Context getContext() {
        return Autolettura.staticContext;
    }

    @Override
    protected void onStart() {
        super.onStart();

        final ImageView autoletturaIndietro = (ImageView) findViewById(R.id.autolettura_indietro);
        autoletturaIndietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
        autoletturaIndietro.getBackground().invalidateSelf();
        autoletturaIndietro.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v1) {
                autoletturaIndietro.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
                autoletturaIndietro.getBackground().invalidateSelf();
                animation = Utilities.animation(Autolettura.this);
                imageAnim = (ImageView) findViewById(R.id.waiting_anim);
                imageAnim.setVisibility(View.VISIBLE);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    imageAnim.setBackgroundDrawable(animation);
                } else {
                    imageAnim.setBackground(animation);
                }
                animation.start();

                // Custom logic for back navigation
                String sourcePage = getIntent().getStringExtra(SOURCE_PAGE);
                Intent intentHome = new Intent(getApplicationContext(), Home.class);
                if (Utilities.isNotEmptyString(sourcePage) && sourcePage.equalsIgnoreCase(BollettePagamentiProssimaBolletta.class.getName())) {
                    intentHome = new Intent(getApplicationContext(), BollettePagamentiProssimaBolletta.class);
                    intentHome.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
                    intentHome.putExtra("TIPOLOGIA", getIntent().getStringExtra("TIPOLOGIA"));
                    intentHome.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
                    intentHome.putExtra("SOURCE_ACTIVITY", BollettePagamenti.class.getName());
                }
                Autolettura.this.finish();
                startActivity(intentHome);
                overridePendingTransition(R.anim.fade, R.anim.hold);
            }
        });

        final ImageView autoletturaHelp = (ImageView) findViewById(R.id.autolettura_help);
        autoletturaHelp.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
        autoletturaHelp.getBackground().invalidateSelf();
        autoletturaHelp.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v1) {
                autoletturaHelp.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
                autoletturaHelp.getBackground().invalidateSelf();

                animation = Utilities.animation(Autolettura.this);
                imageAnim = (ImageView) findViewById(R.id.waiting_anim);
                imageAnim.setVisibility(View.VISIBLE);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    imageAnim.setBackgroundDrawable(animation);
                } else {
                    imageAnim.setBackground(animation);
                }
                animation.start();

                Intent intentHome = new Intent(getApplicationContext(), AutoletturaHelp.class);
                intentHome.putExtra("ORIGINE", Constants.ORIGINE_AUTOLETTURA);
                Autolettura.this.finish();
                startActivity(intentHome);
                overridePendingTransition(R.anim.fade, R.anim.hold);
            }
        });

        final HorizontalPager contatori_pager = (HorizontalPager) findViewById(R.id.autolettura_pager);
        contatori_pager.removeAllViews();
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);
        try {
            final JSONArray loginJSON = new JSONArray(rispostaLogin);
            // JSONObject esito = loginJSON.getJSONObject(0);

            JSONObject datiAnagr = loginJSON.getJSONObject(1);
            // ultimo accesso
            String ultimoaccesso = datiAnagr.getString("UltimoAccesso");
            TextView ultimoAccesso = (TextView) findViewById(R.id.ultimo_accesso);
            // ultimoAccesso.setTextSize(11);
            ultimoAccesso.setText(ultimoaccesso);
            // nome cliente
            String nomeClienteString = datiAnagr.getString("NomeCliente");
            String cognomeClienteString = datiAnagr.getString("CognomeCliente");
            TextView nomeCliente = (TextView) findViewById(R.id.nome_cliente);
            String nomeInteroCliente = nomeClienteString + " " + cognomeClienteString;
            nomeCliente.setText(nomeInteroCliente);

            // parte del contattore
            // quante forniture da visualizza?
            JSONArray ultimeLettureJSON = new JSONArray(rispostaUltimeLetture);
            int numeroOggetti = ultimeLettureJSON.length() - 1;

            // parto da secondo oggetto
            for (int i = 1; i < numeroOggetti + 1; i++) {
                JSONObject contatoreIesimo = ultimeLettureJSON.getJSONObject(i);
                View entry_autolettura = inflater.inflate(R.layout.autolettura_entry, null);
                TextView matricola = (TextView) entry_autolettura.findViewById(R.id.matricola);
                //matricola.setText(contatoreIesimo.getString("CodiceContatore"));
                setTextAutoFitFont(matricola, contatoreIesimo.getString("CodiceContatore"));


                final String codiceConto = JSONUtility.cercaCodiceConto(loginJSON, contatoreIesimo.getString("Pdf"));

                TextView indirizzoInstallazione = (TextView) entry_autolettura.findViewById(R.id.installato_a_indirizzo);
                String indirizzoFornituraIesimo = JSONUtility.cercaIndirizzoFornitura(loginJSON, codiceConto);
                // if (indirizzoFornituraIesimo.length() > 36) {
                // indirizzoInstallazione.setText(indirizzoFornituraIesimo.substring(0,
                // 36) + "...");
                // }
                // else {
                indirizzoInstallazione.setText(indirizzoFornituraIesimo);
                // }

                final String pdr = contatoreIesimo.getString("Pdr");
                final String pdf = contatoreIesimo.getString("Pdf");
                final String sistemaBilling = JSONUtility.cercaSistemaBilling(loginJSON, pdf);
                final String societaFornitrice = JSONUtility.cercaSocietaFornitrice(loginJSON, pdf);
                final String numeroCifreMisuratore = contatoreIesimo.getString("NumCifreMisuratore");

                TextView numeroCliente = (TextView) entry_autolettura.findViewById(R.id.num_cliente_valore);
                numeroCliente.setText(codiceConto);

                final RelativeLayout contatore = (RelativeLayout) entry_autolettura.findViewById(R.id.autolettura_installato);
                contatore.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
                contatore.getBackground().invalidateSelf();
                contatore.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v1) {
                        // parte la chiamata al dettaglio dello storico

                        animation = Utilities.animation(Autolettura.this);
                        imageAnim = (ImageView) findViewById(R.id.waiting_anim);
                        imageAnim.setVisibility(View.VISIBLE);
                        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                            imageAnim.setBackgroundDrawable(animation);
                        } else {
                            imageAnim.setBackground(animation);
                        }
                        animation.start();

                        String codiceCliente = JSONUtility.getCodiceCliente(rispostaLogin);
                        HashMap<String, String> parametri = new HashMap<String, String>();
                        parametri.put("pdf", pdf);
                        parametri.put("codiceCliente", codiceCliente);
                        // parametri.put("codiceContoCliente", codiceConto);

                        currentCodiceConto = codiceConto;
                        parametri.put("pdr", pdr);

                        /************************************ Ultima lettura gas ************************************/
                        if (!Utilities.isNetworkAvailable(context)) {
                            Intent intentOops = new Intent(getApplicationContext(), OopsPageAutoletturaAutolettura.class);
                            intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.connessione_assente));
                            intentOops.putExtra("PDF", pdf);
                            intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
                            intentOops.putExtra("SISTEMA_BILLING", sistemaBilling);
                            intentOops.putExtra("SOCIETA_FORNITRICE", societaFornitrice);
                            intentOops.putExtra("NUMERO_CIFRE_MISURATORE", numeroCifreMisuratore);

                            // StatoAttivazioneFornitura.this.finish();
                            startActivity(intentOops);
                            overridePendingTransition(R.anim.fade, R.anim.hold);
                        } else {
                            ServiceCall stAvFornServiceCall = ServiceCallFactory.createServiceCall(context);
                            // stAvFornServiceCall.myCookieStore = new
                            // PersistentCookieStore(context);

                            try {
                                stAvFornServiceCall.executeHttpsGetAutolettura(Constants.URL_ULTIMA_LETTURA_GAS, parametri, Autolettura.this, pdf, pdr, sistemaBilling, societaFornitrice, numeroCifreMisuratore, null);
                            } catch (Exception e) {
                                Intent intentOops = new Intent(getApplicationContext(), OopsPageAutoletturaAutolettura.class);
                                intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
                                intentOops.putExtra("PDF", pdf);
                                intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
                                intentOops.putExtra("SISTEMA_BILLING", sistemaBilling);
                                intentOops.putExtra("SOCIETA_FORNITRICE", societaFornitrice);
                                intentOops.putExtra("NUMERO_CIFRE_MISURATORE", numeroCifreMisuratore);

                                // StatoAttivazioneFornitura.this.finish();
                                startActivity(intentOops);
                                overridePendingTransition(R.anim.fade, R.anim.hold);
                            }
                        }
                        /*******************************************************************************************/
                    }
                });

                contatori_pager.addView(entry_autolettura);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final LinearLayout scroller = (LinearLayout) findViewById(R.id.autolettura_scroller);
        scroller.removeAllViews();
        int numeroPagine = contatori_pager.getChildCount();
        for (int i = 0; i < numeroPagine; i++) {
            ImageView iv = new ImageView(this);
            iv.setId(i);
            LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            param.setMargins(10, 0, 0, 0);
            iv.setImageResource(R.drawable.dot_not_selected);
            iv.setLayoutParams(param);
            scroller.addView(iv);

            int idFocusedPage = contatori_pager.getCurrentPage();
            ImageView pageFocused = (ImageView) findViewById(idFocusedPage);
            pageFocused.setImageResource(R.drawable.dot_selected);

        }

        contatori_pager.addOnScrollListener(new HorizontalPager.OnScrollListener() {
            int numeroPagine = contatori_pager.getChildCount();

            public void onScroll(int scrollX) {
                for (int i = 0; i < numeroPagine; i++) {
                    ImageView iv = (ImageView) findViewById(i);
                    iv.setImageResource(R.drawable.dot_not_selected);
                }
                int idFocusedPage = contatori_pager.getCurrentPage();
                ImageView pageFocused = (ImageView) findViewById(idFocusedPage);
                pageFocused.setImageResource(R.drawable.dot_selected);
            }

            public void onViewScrollFinished(int currentPage) {
                for (int i = 0; i < numeroPagine; i++) {
                    ImageView iv = (ImageView) findViewById(i);
                    iv.setImageResource(R.drawable.dot_not_selected);
                }
                int idFocusedPage = contatori_pager.getCurrentPage();
                ImageView pageFocused = (ImageView) findViewById(idFocusedPage);
                pageFocused.setImageResource(R.drawable.dot_selected);
            }
        });

    }

    public void postChiamata(String rispostaChiamata, List<Cookie> lista, String pdf, String pdr, String sistemaBilling, String societaFornitrice, String numeroCifreMisuratore, String codiceCliente) {
        JSONArray rispostaJSON = new JSONArray();
        String esito = "";
        String descEsito = "";
        if (animation != null) {
            animation.stop();
            imageAnim.setVisibility(View.INVISIBLE);
        }

        final ImageView autoletturaIndietro = (ImageView) findViewById(R.id.autolettura_indietro);
        autoletturaIndietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
        autoletturaIndietro.getBackground().invalidateSelf();

        try {
            rispostaJSON = new JSONArray(rispostaChiamata);
            JSONObject oggettoRisposta = (JSONObject) rispostaJSON.get(0);
            esito = oggettoRisposta.getString("esito");
            descEsito = oggettoRisposta.getString("descEsito");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (esito.equals("200")) {
            Constants.SERVIZIO_ULTIMA_LETTURA = rispostaChiamata;
            Intent intentAutoletturaInsert = new Intent(getApplicationContext(), AutoLetturaInsert.class);
            intentAutoletturaInsert.putExtra("PDF", pdf);
            intentAutoletturaInsert.putExtra("PDR", pdr);
            intentAutoletturaInsert.putExtra("CURRENT_CODICE_CONTO", currentCodiceConto);
            intentAutoletturaInsert.putExtra("SISTEMABILLING", sistemaBilling);
            intentAutoletturaInsert.putExtra("SOCIETAFORNITRICE", societaFornitrice);
            intentAutoletturaInsert.putExtra("NUMCIFREMISURATORE", numeroCifreMisuratore);
            startActivity(intentAutoletturaInsert);
            finish();
            overridePendingTransition(R.anim.fade, R.anim.hold);
        } else {
            Intent intentOops = new Intent(getApplicationContext(), OopsPageAutoletturaAutolettura.class);
            intentOops.putExtra("PDF", pdf);
            intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
            intentOops.putExtra("SISTEMA_BILLING", sistemaBilling);
            intentOops.putExtra("SOCIETA_FORNITRICE", societaFornitrice);
            intentOops.putExtra("NUMERO_CIFRE_MISURATORE", numeroCifreMisuratore);

            // errori ops page
            if (esito.equals("309")) {
                intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
                startActivity(intentOops);
                overridePendingTransition(R.anim.fade, R.anim.hold);

            } else if (esito.equals("400")) {
                intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
                startActivity(intentOops);
                overridePendingTransition(R.anim.fade, R.anim.hold);

            } else if (esito.equals("420")) {
                intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
                startActivity(intentOops);
                overridePendingTransition(R.anim.fade, R.anim.hold);

            } else if (esito.equals("449")) {
                intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
                startActivity(intentOops);
                overridePendingTransition(R.anim.fade, R.anim.hold);

            } else if (esito.equals("451")) {
                intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_popup_451));
                startActivity(intentOops);
                overridePendingTransition(R.anim.fade, R.anim.hold);

            }// errori popup
            else if (esito.equals("307")) {
                Utils.popup(Autolettura.this, getResources().getString(R.string.errore_popup_307), "Riprova");

            } else if (esito.equals("435")) {
                Utils.popup(Autolettura.this, getResources().getString(R.string.errore_popup_435), "Riprova");

            } else if (esito.equals("437")) {
                Utils.popup(Autolettura.this, getResources().getString(R.string.errore_popup_437), "Riprova");

            } else if (esito.equals("438")) {
                Utils.popup(Autolettura.this, getResources().getString(R.string.errore_popup_438), "Riprova");

            } else if (esito.equals("443")) {
                Utils.popup(Autolettura.this, getResources().getString(R.string.errore_popup_443), "Riprova");

            } else if (esito.equals("444")) {
                Utils.popup(Autolettura.this, getResources().getString(R.string.errore_popup_444), "Riprova");

            } else if (esito.equals("445")) {
                Utils.popup(Autolettura.this, getResources().getString(R.string.errore_popup_445), "Riprova");

            } else if (esito.equals("446")) {
                Utils.popup(Autolettura.this, getResources().getString(R.string.errore_popup_446), "Riprova");

            } else if (esito.equals("447")) {
                Utils.popup(Autolettura.this, getResources().getString(R.string.errore_popup_447), "Riprova");

            } else {
                if (descEsito.equals("")) {
                    intentOops = new Intent(getApplicationContext(), OopsPageSessioneScaduta.class);
                    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.sessione_scaduta));
                } else {
                    intentOops.putExtra("MESSAGGIO", descEsito);
                }

                startActivity(intentOops);
                overridePendingTransition(R.anim.fade, R.anim.hold);
            }
        }
    }

    public void postChiamataException(boolean exception, String pdf, String sistemaBilling, String societaFornitrice, String numeroCifreMisuratore, String codiceCliente) {
        if (exception) {
            if (animation != null) {
                animation.stop();
                imageAnim.setVisibility(View.INVISIBLE);
            }

            final ImageView autoletturaIndietro = (ImageView) findViewById(R.id.autolettura_indietro);
            autoletturaIndietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
            autoletturaIndietro.getBackground().invalidateSelf();

            Intent intentOops = new Intent(getApplicationContext(), OopsPageAutoletturaAutolettura.class);
            intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
            intentOops.putExtra("PDF", pdf);
            intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
            intentOops.putExtra("SISTEMA_BILLING", sistemaBilling);
            intentOops.putExtra("SOCIETA_FORNITRICE", societaFornitrice);
            intentOops.putExtra("NUMERO_CIFRE_MISURATORE", numeroCifreMisuratore);

            // StatoAttivazioneFornitura.this.finish();
            startActivity(intentOops);
            overridePendingTransition(R.anim.fade, R.anim.hold);
        }
    }

    private void setTextAutoFitFont(TextView textView, String contenuto) {
        switch (contenuto.length()) {
            case 11:
                textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
                break;
            case 12:
                textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
                break;
            case 13:
                textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                break;
            case 14:
                textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
                break;
            case 15:
                textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
                break;
            case 16:
                textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
                break;
            case 17:
                textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 13);
                break;
            case 18:
                textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
                break;
            case 19:
                textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 11);
                break;
            case 20:
                textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 11);
                break;
            default:
                textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 21);
                break;
        }
        textView.setText(contenuto);
    }

    public void onBackPressed() {

        animation = Utilities.animation(Autolettura.this);
        imageAnim = (ImageView) findViewById(R.id.waiting_anim);
        imageAnim.setVisibility(View.VISIBLE);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
            imageAnim.setBackgroundDrawable(animation);
        } else {
            imageAnim.setBackground(animation);
        }
        animation.start();

        // Custom logic for back navigation
        String sourcePage = getIntent().getStringExtra(SOURCE_PAGE);
        Intent intentHome = new Intent(getApplicationContext(), Home.class);
        if (Utilities.isNotEmptyString(sourcePage) && sourcePage.equalsIgnoreCase(BollettePagamentiProssimaBolletta.class.getName())) {
            intentHome = new Intent(getApplicationContext(), BollettePagamentiProssimaBolletta.class);
            intentHome.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
            intentHome.putExtra("TIPOLOGIA", getIntent().getStringExtra("TIPOLOGIA"));
            intentHome.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
            intentHome.putExtra("SOURCE_ACTIVITY", BollettePagamenti.class.getName());
        }
        Autolettura.this.finish();
        startActivity(intentHome);
        overridePendingTransition(R.anim.fade, R.anim.hold);
    }

}
