package com.eni.enigaseluce.autolettura;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.AnimationDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.eni.enigaseluce.R;
import com.eni.enigaseluce.httpcall.Constants;
import com.eni.enigaseluce.utils.Utilities;

@SuppressLint("NewApi")
public class AutoletturaHelp extends Activity {
    AnimationDrawable animation;
    ImageView imageAnim;
    String origine;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.autolettura_help);
	origine = getIntent().getStringExtra("ORIGINE");
	final ImageView help = (ImageView) findViewById(R.id.autolettura_help_indietro);
	help.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
	help.getBackground().invalidateSelf();
	help.setOnClickListener(new View.OnClickListener() {
	    public void onClick(View v1) {
		help.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
		help.getBackground().invalidateSelf();

		animation = Utilities.animation(AutoletturaHelp.this);
		imageAnim = (ImageView) findViewById(R.id.waiting_anim);
		imageAnim.setVisibility(View.VISIBLE);
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
		    imageAnim.setBackgroundDrawable(animation);
		}
		else {
		    imageAnim.setBackground(animation);
		}
		animation.start();

		Intent intentHelp = new Intent();
		if (origine.equals(Constants.ORIGINE_AUTOLETTURA)) {
		    intentHelp = new Intent(getApplicationContext(), Autolettura.class);
		}
		else if (origine.equals(Constants.ORIGINE_AUTOLETTURA_INSERT)) {
		    intentHelp = new Intent(getApplicationContext(), AutoLetturaInsert.class);
		    intentHelp.putExtra("PDF", getIntent().getStringExtra("PDF"));
		    intentHelp.putExtra("SISTEMABILLING", getIntent().getStringExtra("SISTEMABILLING"));
		    intentHelp.putExtra("SOCIETAFORNITRICE", getIntent().getStringExtra("SOCIETAFORNITRICE"));
		    intentHelp.putExtra("NUMCIFREMISURATORE", getIntent().getStringExtra("NUMCIFREMISURATORE"));
		}
		AutoletturaHelp.this.finish();
		startActivity(intentHelp);
		overridePendingTransition(R.anim.fade, R.anim.hold);
	    }
	});

    }

    public void onBackPressed() {

	animation = Utilities.animation(this);
	imageAnim = (ImageView) findViewById(R.id.waiting_anim);
	imageAnim.setVisibility(View.VISIBLE);
	if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
	    imageAnim.setBackgroundDrawable(animation);
	}
	else {
	    imageAnim.setBackground(animation);
	}
	animation.start();

	Intent intentHelp = new Intent();
	if (origine.equals(Constants.ORIGINE_AUTOLETTURA)) {
	    intentHelp = new Intent(getApplicationContext(), Autolettura.class);
	}
	else if (origine.equals(Constants.ORIGINE_AUTOLETTURA_INSERT)) {
	    intentHelp = new Intent(getApplicationContext(), AutoLetturaInsert.class);
	    intentHelp.putExtra("PDF", getIntent().getStringExtra("PDF"));
	    intentHelp.putExtra("SISTEMABILLING", getIntent().getStringExtra("SISTEMABILLING"));
	    intentHelp.putExtra("SOCIETAFORNITRICE", getIntent().getStringExtra("SOCIETAFORNITRICE"));
	    intentHelp.putExtra("NUMCIFREMISURATORE", getIntent().getStringExtra("NUMCIFREMISURATORE"));
	}
	AutoletturaHelp.this.finish();
	startActivity(intentHelp);
	overridePendingTransition(R.anim.fade, R.anim.hold);
    }
}
