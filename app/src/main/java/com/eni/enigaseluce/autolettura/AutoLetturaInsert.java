package com.eni.enigaseluce.autolettura;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.http.cookie.Cookie;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.AnimationDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.eni.enigaseluce.NumberPicker;
import com.eni.enigaseluce.R;
import com.eni.enigaseluce.bollettePagamenti.BollettePagamentiProssimaBolletta;
import com.eni.enigaseluce.error.OopsPageAutoletturaInsert;
import com.eni.enigaseluce.error.OopsPageAutoletturaInsertAnnulla;
import com.eni.enigaseluce.error.OopsPageAutoletturaInsertConferma;
import com.eni.enigaseluce.error.OopsPageBollettePagamentiProssimaBolletta;
import com.eni.enigaseluce.error.OopsPageProssimaBollettaAutolettura;
import com.eni.enigaseluce.error.OopsPageSessioneScaduta;
import com.eni.enigaseluce.error.OopsPageStoricoLetture;
import com.eni.enigaseluce.error.Utils;
import com.eni.enigaseluce.font.EniFont;
import com.eni.enigaseluce.httpcall.Constants;
import com.eni.enigaseluce.httpcall.ServiceCall;
import com.eni.enigaseluce.httpcall.ServiceCallFactory;
import com.eni.enigaseluce.storicoletture.StoricoLettureDetail;
import com.eni.enigaseluce.utils.JSONUtility;
import com.eni.enigaseluce.utils.Log;
import com.eni.enigaseluce.utils.MyHandler;
import com.eni.enigaseluce.utils.Utilities;

@SuppressLint("NewApi")
public class AutoLetturaInsert extends Activity {
    AnimationDrawable animation;
    ImageView imageAnim;
    Context context;
    static Context staticContext;

    private NumberPicker np1;
    private NumberPicker np2;
    private NumberPicker np3;
    private NumberPicker np4;
    private NumberPicker np5;
    private NumberPicker np6;
    private NumberPicker np7;
    private NumberPicker np8;
    private static final int minPickerValue = 0;
    private static final int maxPickerValue = 9;

    public String rispostaLogin = Constants.SERVIZIO_LOGIN;
    public String rispostaUltimaLettura = Constants.SERVIZIO_ULTIMA_LETTURA;
    // public String rispostaUltimaLettura = "["+
    // "{\"descEsito\": \"akhfkwguwrghe\", \"esito\": 200},"+
    // "{"+
    // "\"DataLettura\":\"24/10/2012\","+
    // "\"StatoLettura\":\"VALIDA\","+
    // "\"ValoreLettura\":\"12345678\""+
    // "}"+
    // "]";
    String pdf;
    String pdr;
    String codiceCliente;
    String sistemaBilling;
    String societaFornitrice;
    String numeroCifreMisuratore;
    String scarto = "";
    String valoreDaInserireNeiPickers = "";
    private static String valoreAutolettura = "";
    private static String idOperationInvio = "";

    String codiceConto;
    String tipologia;

    // String numeroCifreMisuratore = "8";

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.autolettura_insert);
        context = this;
        AutoLetturaInsert.staticContext = this;

        Intent intent = this.getIntent();
        pdf = intent.getStringExtra("PDF");
        pdr = intent.getStringExtra("PDR");
        sistemaBilling = intent.getStringExtra("SISTEMABILLING");
        societaFornitrice = intent.getStringExtra("SOCIETAFORNITRICE");
        numeroCifreMisuratore = intent.getStringExtra("NUMCIFREMISURATORE");
        codiceConto = intent.getStringExtra("CURRENT_CODICE_CONTO");
        codiceCliente = getCodiceCliente(rispostaLogin);
        tipologia = intent.getStringExtra("TIPOLOGIA");
    }

    public static Context getContext() {
        return AutoLetturaInsert.staticContext;
    }

    @Override
    protected void onStart() {
        super.onStart();
        //
        // try {
        // final JSONArray bollettePagamentiLogin = new
        // JSONArray(rispostaLogin);
        // }
        // catch (JSONException e1) {
        // e1.printStackTrace();
        // }

        LinearLayout layout_tabbar = (LinearLayout) findViewById(R.id.layout_tabbar);

        EniFont tab_autolettura = (EniFont) layout_tabbar.findViewById(R.id.tab_autolettura);
        tab_autolettura.setSelected(true);
        tab_autolettura.setEnabled(false);
        tab_autolettura.setTextColor(Color.GRAY);

        EniFont tab_storico = (EniFont) layout_tabbar.findViewById(R.id.tab_storico);
        tab_storico.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                animation = Utilities.animation(AutoLetturaInsert.this);
                imageAnim = (ImageView) findViewById(R.id.waiting_anim);
                imageAnim.setVisibility(View.VISIBLE);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    imageAnim.setBackgroundDrawable(animation);
                } else {
                    imageAnim.setBackground(animation);
                }
                animation.start();

                // startActivity(new Intent(AutoLetturaInsert.this,
                // StoricoLetture.class));

                ServiceCall stAvFornServiceCall = ServiceCallFactory.createServiceCall(context);
                // stAvFornServiceCall.myCookieStore = new
                // PersistentCookieStore(context);

                HashMap<String, String> parametri = new HashMap<String, String>();
                parametri.put("pdf", pdf);
                parametri.put("pdr", pdr);
                parametri.put("sistemaBilling", sistemaBilling);
                parametri.put("societaFornitrice", societaFornitrice);
                parametri.put("numeroCifreMisuratore", numeroCifreMisuratore);

                try {

                    if (!isNetworkAvailable(context)) {
                        Intent intentOops = new Intent(getApplicationContext(), OopsPageStoricoLetture.class);
                        intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.connessione_assente));
                        intentOops.putExtra("PDF", pdf);
                        intentOops.putExtra("SISTEMA_BILLING", sistemaBilling);
                        intentOops.putExtra("SOCIETA_FORNITRICE", societaFornitrice);
                        intentOops.putExtra("NUM_CIFRE_MISURATORE", numeroCifreMisuratore);
                        intentOops.putExtra("PDR", pdr);
                        // finish();
                        startActivity(intentOops);
                        if (animation != null) {
                            animation.stop();
                            imageAnim.setVisibility(View.INVISIBLE);
                        }
                        overridePendingTransition(R.anim.fade, R.anim.hold);
                    } else {
                        stAvFornServiceCall.executeHttpsGetStoricoLetture(Constants.URL_STORICO_LETTURE_GAS, parametri, AutoLetturaInsert.this, pdf, pdr, null);
                    }
                } catch (Exception e) {
                    Intent intentOops = new Intent(getApplicationContext(), OopsPageStoricoLetture.class);
                    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
                    intentOops.putExtra("PDF", pdf);
                    intentOops.putExtra("SISTEMA_BILLING", sistemaBilling);
                    intentOops.putExtra("SOCIETA_FORNITRICE", societaFornitrice);
                    intentOops.putExtra("NUM_CIFRE_MISURATORE", numeroCifreMisuratore);
                    intentOops.putExtra("PDR", pdr);

                    startActivity(intentOops);
                    if (animation != null) {
                        animation.stop();
                        imageAnim.setVisibility(View.INVISIBLE);
                    }
                    overridePendingTransition(R.anim.fade, R.anim.hold);
                }
            }
        });

        final ImageView autoLetturaInsert = (ImageView) findViewById(R.id.autolettura_insert_indietro);
        autoLetturaInsert.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
        autoLetturaInsert.getBackground().invalidateSelf();
        autoLetturaInsert.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v1) {
                autoLetturaInsert.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
                autoLetturaInsert.getBackground().invalidateSelf();

                animation = Utilities.animation(AutoLetturaInsert.this);
                imageAnim = (ImageView) findViewById(R.id.waiting_anim);
                imageAnim.setVisibility(View.VISIBLE);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    imageAnim.setBackgroundDrawable(animation);
                } else {
                    imageAnim.setBackground(animation);
                }
                animation.start();

                Intent intentHome = new Intent(getApplicationContext(), Autolettura.class);
                AutoLetturaInsert.this.finish();
                startActivity(intentHome);
                overridePendingTransition(R.anim.fade, R.anim.hold);
            }
        });

        final ImageView autoLetturaInsertHelp = (ImageView) findViewById(R.id.autolettura_insert_help);
        autoLetturaInsertHelp.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
        autoLetturaInsertHelp.getBackground().invalidateSelf();
        autoLetturaInsertHelp.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v1) {
                autoLetturaInsertHelp.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
                autoLetturaInsertHelp.getBackground().invalidateSelf();

                animation = Utilities.animation(AutoLetturaInsert.this);
                imageAnim = (ImageView) findViewById(R.id.waiting_anim);
                imageAnim.setVisibility(View.VISIBLE);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    imageAnim.setBackgroundDrawable(animation);
                } else {
                    imageAnim.setBackground(animation);
                }
                animation.start();

                Intent intentHome = new Intent(getApplicationContext(), AutoletturaHelp.class);
                intentHome.putExtra("ORIGINE", Constants.ORIGINE_AUTOLETTURA_INSERT);
                intentHome.putExtra("PDF", pdf);
                intentHome.putExtra("SISTEMABILLING", sistemaBilling);
                intentHome.putExtra("SOCIETAFORNITRICE", societaFornitrice);
                intentHome.putExtra("NUMCIFREMISURATORE", numeroCifreMisuratore);
                AutoLetturaInsert.this.finish();
                startActivity(intentHome);
                overridePendingTransition(R.anim.fade, R.anim.hold);
            }
        });

        Button verifica_periodo_autolettura = (Button) findViewById(R.id.verifica_periodo_autolettura);
        verifica_periodo_autolettura.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                animation = Utilities.animation(AutoLetturaInsert.this);
                imageAnim = (ImageView) findViewById(R.id.waiting_anim);
                imageAnim.setVisibility(View.VISIBLE);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    imageAnim.setBackgroundDrawable(animation);
                } else {
                    imageAnim.setBackground(animation);
                }
                animation.start();

                if (!isNetworkAvailable(context)) {
                    Intent intentOops = new Intent(getApplicationContext(), OopsPageProssimaBollettaAutolettura.class);
                    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.connessione_assente));
                    intentOops.putExtra("PDF", pdf);
                    intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
                    intentOops.putExtra("CODICE_CONTO", codiceConto);
                    intentOops.putExtra("VALORE_AUTOLETTURA", Integer.valueOf(valoreAutolettura) + "");
                    intentOops.putExtra("SISTEMA_BILLING", sistemaBilling);
                    intentOops.putExtra("SOCIETA_FORNITRICE", societaFornitrice);
                    intentOops.putExtra("NUMERO_CIFRE_MISURATORE", numeroCifreMisuratore);
                    intentOops.putExtra("CANALE", "APPS");
                    intentOops.putExtra("SOURCE_CLASS", AutoLetturaInsert.this.getClass().getName());
                    intentOops.putExtra("MODALITA", "MANUALE");
                    // finish();
                    startActivity(intentOops);
                    if (animation != null) {
                        animation.stop();
                        imageAnim.setVisibility(View.INVISIBLE);
                    }
                    overridePendingTransition(R.anim.fade, R.anim.hold);
                } else {

                    final ServiceCall stAvFornServiceCall = ServiceCallFactory.createServiceCall(context);

                    // JSONArray bollettePagamenti = null;
                    // try {
                    // bollettePagamenti = new
                    // JSONArray(Constants.SERVIZIO_LOGIN);
                    // }
                    // catch (JSONException e) {
                    //
                    // e.printStackTrace();
                    // }

                    HashMap<String, String> parametri = new HashMap<String, String>();
                    parametri.put("codiceCliente", codiceCliente);
                    parametri.put("codiceContoCliente", codiceConto);

                    try {

                        stAvFornServiceCall.executeHttpsGetProssimaBolletta(Constants.URL_INFO_PROSSIMA_FATTURA, parametri, AutoLetturaInsert.this, null, true,
                                false);
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                }
            }
        });

        try {
            JSONArray ultimaLetturaJSON = new JSONArray(rispostaUltimaLettura);
            JSONObject lettura = ultimaLetturaJSON.getJSONObject(1);

            // String dataLettura = lettura.getString("DataLettura");
            // Date dataLetturaDate = new Date(dataLettura);
            // String diferenzaData = differenzaData(dataLetturaDate);

            //TextView ultimaLetturaData = (TextView) findViewById(R.id.autolettura_ultima_lettura);
            //ultimaLetturaData.setText(lettura.getString("DataLettura"));



            TextView dataLetturaAtuuale = (TextView) findViewById(R.id.autolettura_insert_lettura_attuale);



            dataLetturaAtuuale.setText(Html.fromHtml("Lettura attuale " + new SimpleDateFormat("dd LLLL yyyy").format(Calendar.getInstance().getTime()).toLowerCase() + ":"));

            final String valoreLetturaRicevuta = lettura.getString("ValoreLettura");
            valoreAutolettura = valoreLetturaRicevuta;
            //((TextView)findViewById(R.id.autolettura_insert_metri_cubi)).setText(valoreAutolettura);
            try {
                Date autoletturaDate = new SimpleDateFormat("dd/MM/yyyy").parse(lettura.getString("DataLettura"));
                String autoletturaDateString = new SimpleDateFormat("dd LLLL yyyy").format(autoletturaDate);
                //ultimaLetturaData.setText(autoletturaDateString.toLowerCase());
                ((TextView)findViewById(R.id.autolettura_insert_metri_cubi)).setText(Html.fromHtml("Ultima lettura " +autoletturaDateString.toLowerCase()+ " : <b>" + String.format("%0" + numeroCifreMisuratore + "d", Integer.parseInt(valoreLetturaRicevuta)) + " m<sup>3</sup></b>"));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            int numeroCifreMisuratoreInteger = Integer.valueOf(numeroCifreMisuratore);

            final LinearLayout pickers = (LinearLayout) findViewById(R.id.pickers);
            pickers.removeAllViews();

            // normalize numeroCifreMisuratore to 8
            int tmpCifreMisuratore = numeroCifreMisuratoreInteger <= 8 ? numeroCifreMisuratoreInteger : 8;

            for (int l = 0; l < tmpCifreMisuratore; l++) {
                np5 = new NumberPicker(context);
                np5.setMin(minPickerValue);
                np5.setMax(maxPickerValue);
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams((int) getResources().getDimension(R.dimen.picker_width),
                        LinearLayout.LayoutParams.WRAP_CONTENT);
                params.setMargins(0, (int) getResources().getDimension(R.dimen.picker_top), 0, 0);
                np5.setLayoutParams(params);
                pickers.addView(np5);
            }
            if (getIntent().getStringExtra("VALORE_AUTOLETTURA") != null) {
                valoreDaInserireNeiPickers = getIntent().getStringExtra("VALORE_AUTOLETTURA");

                int tempValues = Integer.parseInt(valoreDaInserireNeiPickers);
                String tempValuesString = String.valueOf(tempValues);

                for (int j = 0; j < numeroCifreMisuratoreInteger - valoreLetturaRicevuta.length(); j++) {
                    tempValuesString = "0" + tempValuesString;
                }

                valoreAutolettura = tempValuesString;

                valoreDaInserireNeiPickers = valoreAutolettura;
            } else {
                if (valoreLetturaRicevuta.length() < numeroCifreMisuratoreInteger) {
                    for (int j = 0; j < numeroCifreMisuratoreInteger - valoreLetturaRicevuta.length(); j++) {
                        valoreAutolettura = "0" + valoreAutolettura;
                    }
                    valoreDaInserireNeiPickers = valoreAutolettura;
                } else {
                    valoreDaInserireNeiPickers = valoreAutolettura;
                }
                if (valoreAutolettura.length() > 8) {
                    scarto = valoreAutolettura.substring(0, valoreAutolettura.length() - 8);
                    valoreDaInserireNeiPickers = valoreAutolettura.substring(valoreAutolettura.length() - 8, valoreAutolettura.length());
                    valoreAutolettura = valoreDaInserireNeiPickers;
                }
            }

            impostaPickers(pickers, valoreDaInserireNeiPickers);




   //Baccus
           //final LinearLayout valoreLettura = (LinearLayout) findViewById(R.id.numeri_contattore);
           // valoreLettura.removeAllViews();
            for (int k = 0; k < tmpCifreMisuratore; k++) {
                ImageView numeroIesimo = new ImageView(context);
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, (int) getResources().getDimension(
                        R.dimen.picker_width));
                numeroIesimo.setLayoutParams(params);
            //    valoreLettura.addView(numeroIesimo);
            }
            //impostaValoreLettura(valoreLettura, valoreAutolettura);

			/*
             * final ImageView salva =
			 * (ImageView)findViewById(R.id.autolettura_insert_salva);
			 * salva.setOnClickListener(new View.OnClickListener() { public void
			 * onClick(View v) { animation = new AnimationDrawable();
			 * animation.addFrame
			 * (getResources().getDrawable(R.drawable.anim_load2),300);
			 * animation
			 * .addFrame(getResources().getDrawable(R.drawable.anim_load3),
			 * 300);
			 * animation.addFrame(getResources().getDrawable(R.drawable.anim_load4
			 * ),300); animation.setOneShot(false); imageAnim = (ImageView)
			 * findViewById(R.id.waiting_autolettura_insert);
			 * imageAnim.setVisibility(View.VISIBLE); if(Build.VERSION.SDK_INT <
			 * 16){ imageAnim.setBackgroundDrawable(animation); }else{
			 * imageAnim.setBackground(animation); } // start the animation!
			 * animation.start(); String letturaInserita =
			 * getLetturaFromPickers(pickers); int letturaInseritaInt = new
			 * Integer(letturaInserita); int ultimaletturaInt = new
			 * Integer(valoreLetturaRicevuta); if(!(letturaInseritaInt >
			 * ultimaletturaInt)){ AlertDialog.Builder builder = new
			 * AlertDialog.Builder(context);
			 * builder.setMessage(R.string.lettura_non_conforme
			 * ).setCancelable(false).setPositiveButton("OK", new
			 * DialogInterface.OnClickListener() { public void
			 * onClick(DialogInterface dialog, int id) {
			 * 
			 * } }); builder.create().show(); if (animation != null) {
			 * animation.stop(); imageAnim.setVisibility(View.INVISIBLE); }
			 * }else{ // valoreLettura.setText(letturaInserita);
			 * impostaValoreLettura(valoreLettura, letturaInserita);
			 * valoreAutolettura = ""+ letturaInseritaInt; if (animation !=
			 * null) { animation.stop();
			 * imageAnim.setVisibility(View.INVISIBLE); } } } });
			 * 
			 * final ImageView cancel =
			 * (ImageView)findViewById(R.id.autolettura_insert_cancel);
			 * cancel.setOnClickListener(new View.OnClickListener() { public
			 * void onClick(View v) { riazzeraPickers(); } });
			 */
            final Button inviaAutolettura = (Button) findViewById(R.id.autolettura_insert_invio);
            inviaAutolettura.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    // chiamata invio autolettura URL_INVIA_AUTOLETTURA_GAS

                    animation = Utilities.animation(AutoLetturaInsert.this);
                    imageAnim = (ImageView) findViewById(R.id.waiting_anim);
                    imageAnim.setVisibility(View.VISIBLE);
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                        imageAnim.setBackgroundDrawable(animation);
                    } else {
                        imageAnim.setBackground(animation);
                    }
                    animation.start();

                    String letturaInserita = getLetturaFromPickers(pickers);
                    int letturaInseritaInt = Integer.valueOf(scarto + letturaInserita);
                    int ultimaletturaInt = Integer.valueOf(valoreLetturaRicevuta);
                    if (!(letturaInseritaInt >= ultimaletturaInt)) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setMessage(R.string.lettura_non_conforme).setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                            }
                        });
                        builder.create().show();
                        if (animation != null) {
                            animation.stop();
                            imageAnim.setVisibility(View.INVISIBLE);
                        }
                    } else {
                        // valoreLettura.setText(letturaInserita);
                        // impostaValoreLettura(valoreLettura, letturaInserita);
                        valoreAutolettura = "" + letturaInseritaInt;
                        /************************************ invia lettura gas ************************************/
                        if (!isNetworkAvailable(context)) {
                            Intent intentOops = new Intent(getApplicationContext(), OopsPageAutoletturaInsert.class);
                            intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.connessione_assente));
                            intentOops.putExtra("PDF", pdf);
                            intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
                            intentOops.putExtra("VALORE_AUTOLETTURA", Integer.valueOf(valoreAutolettura) + "");
                            intentOops.putExtra("SISTEMA_BILLING", sistemaBilling);
                            intentOops.putExtra("SOCIETA_FORNITRICE", societaFornitrice);
                            intentOops.putExtra("NUMERO_CIFRE_MISURATORE", numeroCifreMisuratore);
                            intentOops.putExtra("CANALE", "APPS");
                            intentOops.putExtra("MODALITA", "MANUALE");
                            // AutoLetturaInsert.this.finish();
                            startActivity(intentOops);
                            if (animation != null) {
                                animation.stop();
                                imageAnim.setVisibility(View.INVISIBLE);
                            }
                            overridePendingTransition(R.anim.fade, R.anim.hold);
                        } else {

                            MyHandler handler = new MyHandler() {

                                @Override
                                public void handleMessage(Message msg) {

                                    if (msg.what == Constants.MAINTENANCE_DISATTIVA) {

                                        Log.d("AUTOLETTURA", "URL_INVIA_AUTOLETTURA_GAS - MAINTENANCE_DISATTIVA");

                                        final ServiceCall stAvFornServiceCall = ServiceCallFactory.createServiceCall(context);

                                        codiceCliente = getCodiceCliente(rispostaLogin);
                                        final HashMap<String, String> parametri = new HashMap<String, String>();
                                        parametri.put("pdf", pdf);
                                        parametri.put("codiceCliente", codiceCliente);
                                        parametri.put("valoreAutolettura", Integer.valueOf(valoreAutolettura) + "");
                                        parametri.put("sistemaBilling", sistemaBilling);
                                        parametri.put("societaFornitrice", societaFornitrice);
                                        parametri.put("numCifreMisuratore", numeroCifreMisuratore);
                                        parametri.put("canale", "APPS");
                                        parametri.put("modalita", "MANUALE");

                                        try {

                                            stAvFornServiceCall.executeHttpsGetAutoletturaInsert(Constants.URL_INVIA_AUTOLETTURA_GAS, parametri,
                                                    AutoLetturaInsert.this, null);
                                        } catch (Exception e) {
                                            if (animation != null) {
                                                animation.stop();
                                                imageAnim.setVisibility(View.INVISIBLE);
                                            }
                                            Intent intentOops = new Intent(getApplicationContext(), OopsPageAutoletturaInsert.class);
                                            intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
                                            intentOops.putExtra("PDF", pdf);
                                            intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
                                            intentOops.putExtra("VALORE_AUTOLETTURA", Integer.valueOf(valoreAutolettura) + "");
                                            intentOops.putExtra("SISTEMA_BILLING", sistemaBilling);
                                            intentOops.putExtra("SOCIETA_FORNITRICE", societaFornitrice);
                                            intentOops.putExtra("NUMERO_CIFRE_MISURATORE", numeroCifreMisuratore);
                                            intentOops.putExtra("CANALE", "APPS");
                                            intentOops.putExtra("MODALITA", "MANUALE");

                                            // StatoAttivazioneFornitura.this.finish();
                                            startActivity(intentOops);
                                            overridePendingTransition(R.anim.fade, R.anim.hold);
                                        }
                                    }
                                }
                            };

                            // maintencance mode
                            if (!Constants.STUB_ENABLED) {
                                Utilities.maintenance(AutoLetturaInsert.this, null, handler);
                            }

                            // final ServiceCall stAvFornServiceCall =
                            // ServiceCallFactory.createServiceCall(context);
                            //
                            // codiceCliente = getCodiceCliente(rispostaLogin);
                            // final HashMap<String, String> parametri = new
                            // HashMap<String, String>();
                            // parametri.put("pdf", pdf);
                            // parametri.put("codiceCliente", codiceCliente);
                            // parametri.put("valoreAutolettura",
                            // Integer.valueOf(valoreAutolettura) + "");
                            // parametri.put("sistemaBilling", sistemaBilling);
                            // parametri.put("societaFornitrice",
                            // societaFornitrice);
                            // parametri.put("numCifreMisuratore",
                            // numeroCifreMisuratore);
                            // parametri.put("canale", "APPS");
                            // parametri.put("modalita", "MANUALE");
                            //
                            // try {
                            //
                            // stAvFornServiceCall.executeHttpsGetAutoletturaInsert(Constants.URL_INVIA_AUTOLETTURA_GAS,
                            // parametri, AutoLetturaInsert.this, null);
                            // }
                            // catch (Exception e) {
                            // if (animation != null) {
                            // animation.stop();
                            // imageAnim.setVisibility(View.INVISIBLE);
                            // }
                            // Intent intentOops = new
                            // Intent(getApplicationContext(),
                            // OopsPageAutoletturaInsert.class);
                            // intentOops.putExtra("MESSAGGIO",
                            // getResources().getString(R.string.servizio_non_disponibile));
                            // intentOops.putExtra("PDF", pdf);
                            // intentOops.putExtra("CODICE_CLIENTE",
                            // codiceCliente);
                            // intentOops.putExtra("VALORE_AUTOLETTURA",
                            // Integer.valueOf(valoreAutolettura) + "");
                            // intentOops.putExtra("SISTEMA_BILLING",
                            // sistemaBilling);
                            // intentOops.putExtra("SOCIETA_FORNITRICE",
                            // societaFornitrice);
                            // intentOops.putExtra("NUMERO_CIFRE_MISURATORE",
                            // numeroCifreMisuratore);
                            // intentOops.putExtra("CANALE", "APPS");
                            // intentOops.putExtra("MODALITA", "MANUALE");
                            //
                            // // StatoAttivazioneFornitura.this.finish();
                            // startActivity(intentOops);
                            // overridePendingTransition(R.anim.fade,
                            // R.anim.hold);
                            // }
                        }
                        /*******************************************************************************************/
                    }

                }
            });

        } catch (NumberFormatException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager == null)
            return false;
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }

    public void postChiamata(String rispostaChiamata, List<Cookie> lista, HashMap<String, String> parametri, Activity ops) {
        if (animation != null) {
            animation.stop();
            imageAnim.setVisibility(View.INVISIBLE);
        }

        final ImageView autoLetturaInsert = (ImageView) findViewById(R.id.autolettura_insert_indietro);
        autoLetturaInsert.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
        autoLetturaInsert.getBackground().invalidateSelf();

        JSONObject rispostaJSON = new JSONObject();
        String esito = "";
        String codiceEsito = "";
        String descEsito = "";

        if (rispostaChiamata.indexOf("esito") == -1) {

            animation = Utilities.animation(this);
            imageAnim = (ImageView) findViewById(R.id.waiting_anim);
            imageAnim.setVisibility(View.VISIBLE);
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                imageAnim.setBackgroundDrawable(animation);
            } else {
                imageAnim.setBackground(animation);
            }
            animation.start();

            Intent intentOops = new Intent(getApplicationContext(), OopsPageAutoletturaInsert.class);
            intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
            intentOops.putExtra("PDF", pdf);
            intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
            intentOops.putExtra("VALORE_AUTOLETTURA", parametri.get("valoreAutolettura"));
            intentOops.putExtra("SISTEMA_BILLING", sistemaBilling);
            intentOops.putExtra("SOCIETA_FORNITRICE", societaFornitrice);
            intentOops.putExtra("NUMERO_CIFRE_MISURATORE", numeroCifreMisuratore);
            intentOops.putExtra("CANALE", "APPS");
            intentOops.putExtra("MODALITA", "MANUALE");
            startActivity(intentOops);
            overridePendingTransition(R.anim.fade, R.anim.hold);
        } else {
            try {
                rispostaJSON = new JSONObject(rispostaChiamata);
                esito = rispostaJSON.getString("esito");
                codiceEsito = rispostaJSON.getString("codiceEsito");
                descEsito = rispostaJSON.getString("descEsito");
                if (descEsito.indexOf("lultima") > 0 || descEsito.indexOf("l ultima") > 0) {
                    descEsito = descEsito.replaceAll("lultima", "l'ultima");
                    descEsito = descEsito.replaceAll("l ultima", "l'ultima");
                }
                idOperationInvio = rispostaJSON.getString("idOperation");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (esito.equals("200")) {
                if (codiceEsito.equals("ALTA_WARNING") || codiceEsito.equals("BASSA_WARNING")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setMessage(descEsito).setCancelable(true).setPositiveButton("Conferma", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // CONFERMA viene invocato il servizio
                            // confermaAutoletturaPlist

                            animation = Utilities.animation(AutoLetturaInsert.this);
                            imageAnim = (ImageView) findViewById(R.id.waiting_anim);
                            imageAnim.setVisibility(View.VISIBLE);
                            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                                imageAnim.setBackgroundDrawable(animation);
                            } else {
                                imageAnim.setBackground(animation);
                            }
                            animation.start();

                            /************************************ conferma invio lettura gas ************************************/
                            if (!isNetworkAvailable(context)) {
                                Intent intentOops = new Intent(getApplicationContext(), OopsPageAutoletturaInsertConferma.class);
                                intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.connessione_assente));
                                intentOops.putExtra("PDF", pdf);
                                intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
                                intentOops.putExtra("VALORE_AUTOLETTURA", valoreAutolettura);
                                intentOops.putExtra("SISTEMA_BILLING", sistemaBilling);
                                intentOops.putExtra("SOCIETA_FORNITRICE", societaFornitrice);
                                intentOops.putExtra("CANALE", "APPS");
                                intentOops.putExtra("MODALITA", "MANUALE");
                                // AutoLetturaInsert.this.finish();
                                startActivity(intentOops);
                                if (animation != null) {
                                    animation.stop();
                                    imageAnim.setVisibility(View.INVISIBLE);
                                }
                                overridePendingTransition(R.anim.fade, R.anim.hold);
                            } else {
                                ServiceCall stAvFornServiceCall = ServiceCallFactory.createServiceCall(context);
                                // stAvFornServiceCall.myCookieStore = new
                                // PersistentCookieStore(context);

                                HashMap<String, String> parametri = new HashMap<String, String>();
                                parametri.put("pdf", pdf);
                                parametri.put("valoreAutolettura", valoreAutolettura);
                                parametri.put("sistemaBilling", sistemaBilling);
                                parametri.put("societaFornitrice", societaFornitrice);
                                parametri.put("canale", "APPS");
                                parametri.put("codiceCliente", codiceCliente);
                                parametri.put("modalita", "MANUALE");
                                try {
                                    stAvFornServiceCall.executeHttpsGetAutoletturaInsert(Constants.URL_CONFERMA_AUTOLETTURA_GAS, parametri,
                                            AutoLetturaInsert.this, "CONFERMA", null);
                                } catch (Exception e) {
                                    // OopsPage per conferma invio
                                    Intent intentOops = new Intent(getApplicationContext(), OopsPageAutoletturaInsertConferma.class);
                                    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
                                    intentOops.putExtra("PDF", pdf);
                                    intentOops.putExtra("VALORE_AUTOLETTURA", valoreAutolettura);
                                    intentOops.putExtra("SISTEMA_BILLING", sistemaBilling);
                                    intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
                                    intentOops.putExtra("SOCIETA_FORNITRICE", societaFornitrice);
                                    intentOops.putExtra("NUMERO_CIFRE_MISURATORE", numeroCifreMisuratore);
                                    intentOops.putExtra("CANALE", "APPS");
                                    intentOops.putExtra("MODALITA", "MANUALE");
                                    // StatoAttivazioneFornitura.this.finish();
                                    startActivity(intentOops);
                                    overridePendingTransition(R.anim.fade, R.anim.hold);
                                }
                            }
                            /*******************************************************************************************/
                        }
                    }).setNegativeButton("Annulla", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // ANNULLA viene invocato il servizio
                            // annullaAutoletturaPlist

                            animation = Utilities.animation(AutoLetturaInsert.this);
                            imageAnim = (ImageView) findViewById(R.id.waiting_anim);
                            imageAnim.setVisibility(View.VISIBLE);
                            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                                imageAnim.setBackgroundDrawable(animation);
                            } else {
                                imageAnim.setBackground(animation);
                            }
                            animation.start();

                            /************************************ annulla invio lettura gas ************************************/
                            // chiamata annulla - commentata perche' non
                            // funziona ed aggiunto l'Alert sotto
                            if (!isNetworkAvailable(context)) {
                                Intent intentOops = new Intent(getApplicationContext(), OopsPageAutoletturaInsertAnnulla.class);
                                intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.connessione_assente));
                                intentOops.putExtra("ID_OPERATION", idOperationInvio);
                                intentOops.putExtra("PDF", pdf);
                                intentOops.putExtra("VALORE_AUTOLETTURA", valoreAutolettura);
                                intentOops.putExtra("SISTEMA_BILLING", sistemaBilling);
                                intentOops.putExtra("SOCIETA_FORNITRICE", societaFornitrice);
                                intentOops.putExtra("CANALE", "APPS");
                                intentOops.putExtra("MODALITA", "MANUALE");
                                intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
                                // AutoLetturaInsert.this.finish();
                                startActivity(intentOops);
                                if (animation != null) {
                                    animation.stop();
                                    imageAnim.setVisibility(View.INVISIBLE);
                                }
                                overridePendingTransition(R.anim.fade, R.anim.hold);
                            } else {
                                ServiceCall stAvFornServiceCall = ServiceCallFactory.createServiceCall(context);
                                // stAvFornServiceCall.myCookieStore = new
                                // PersistentCookieStore(context);

                                HashMap<String, String> parametri = new HashMap<String, String>();
                                parametri.put("pdf", pdf);
                                parametri.put("idOperation", idOperationInvio);
                                parametri.put("valoreAutolettura", valoreAutolettura);
                                parametri.put("sistemaBilling", sistemaBilling);
                                parametri.put("societaFornitrice", societaFornitrice);
                                parametri.put("canale", "APPS");
                                parametri.put("modalita", "MANUALE");
                                parametri.put("codiceCliente", codiceCliente);
                                try {
                                    stAvFornServiceCall.executeHttpsGetAutoletturaInsert(Constants.URL_ANNULLA_AUTOLETTURA_GAS, parametri,
                                            AutoLetturaInsert.this, "ANNULLA", null);
                                } catch (Exception e) {
                                    // OopsPage per annulla invio
                                    Intent intentOops = new Intent(getApplicationContext(), OopsPageAutoletturaInsertAnnulla.class);
                                    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
                                    intentOops.putExtra("ID_OPERATION", idOperationInvio);
                                    intentOops.putExtra("PDF", pdf);
                                    intentOops.putExtra("VALORE_AUTOLETTURA", valoreAutolettura);
                                    intentOops.putExtra("SISTEMA_BILLING", sistemaBilling);
                                    intentOops.putExtra("SOCIETA_FORNITRICE", societaFornitrice);
                                    intentOops.putExtra("CANALE", "APPS");
                                    intentOops.putExtra("MODALITA", "MANUALE");
                                    intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
                                    // StatoAttivazioneFornitura.this.finish();
                                    startActivity(intentOops);
                                    overridePendingTransition(R.anim.fade, R.anim.hold);
                                }
                            }
                            /*******************************************************************************************/
                            // Alert da commentare e sopra da scommentare se si
                            // vuole fare la chiamata per annulla autolettura
                            /*
							 * AlertDialog.Builder builder2 = new
							 * AlertDialog.Builder(context);
							 * builder2.setMessage(
							 * getResources().getString(R.string
							 * .annulla_invio)).
							 * setCancelable(false).setPositiveButton("OK", new
							 * DialogInterface.OnClickListener() { public void
							 * onClick(DialogInterface dialog, int id) { Intent
							 * intentAutoletturaConttatori = new
							 * Intent(getApplicationContext(),
							 * Autolettura.class);
							 * AutoLetturaInsert.this.finish();
							 * startActivity(intentAutoletturaConttatori);
							 * overridePendingTransition(R.anim.fade,
							 * R.anim.hold); } }); builder2.create().show();
							 */
                        }
                    });
                    builder.create().show();
                } else {
                    if (codiceEsito.equals("ALTA_BLOCCANTE") || codiceEsito.equals("BASSA_BLOCCANTE")) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setMessage(descEsito).setCancelable(false).setPositiveButton("Annulla", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                // ANNULLA viene invocato il servizio
                                // annullaAutoletturaPlist

                                animation = Utilities.animation(AutoLetturaInsert.this);
                                imageAnim = (ImageView) findViewById(R.id.waiting_anim);
                                imageAnim.setVisibility(View.VISIBLE);
                                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                                    imageAnim.setBackgroundDrawable(animation);
                                } else {
                                    imageAnim.setBackground(animation);
                                }
                                animation.start();

                                /************************************ annulla invio lettura gas ************************************/
                                // chiamata annulla - commentata perche' non
                                // funziona ed aggiunto l'Alert sotto
                                if (!isNetworkAvailable(context)) {
                                    Intent intentOops = new Intent(getApplicationContext(), OopsPageAutoletturaInsertAnnulla.class);
                                    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.connessione_assente));
                                    intentOops.putExtra("ID_OPERATION", idOperationInvio);
                                    intentOops.putExtra("PDF", pdf);
                                    intentOops.putExtra("VALORE_AUTOLETTURA", valoreAutolettura);
                                    intentOops.putExtra("SISTEMA_BILLING", sistemaBilling);
                                    intentOops.putExtra("SOCIETA_FORNITRICE", societaFornitrice);
                                    intentOops.putExtra("CANALE", "APPS");
                                    intentOops.putExtra("MODALITA", "MANUALE");
                                    intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
                                    startActivity(intentOops);
                                    if (animation != null) {
                                        animation.stop();
                                        imageAnim.setVisibility(View.INVISIBLE);
                                    }
                                    overridePendingTransition(R.anim.fade, R.anim.hold);
                                } else {
                                    ServiceCall stAvFornServiceCall = ServiceCallFactory.createServiceCall(context);
                                    // stAvFornServiceCall.myCookieStore = new
                                    // PersistentCookieStore(context);

                                    HashMap<String, String> parametri = new HashMap<String, String>();
                                    parametri.put("pdf", pdf);
                                    parametri.put("idOperation", idOperationInvio);
                                    parametri.put("valoreAutolettura", valoreAutolettura);
                                    parametri.put("sistemaBilling", sistemaBilling);
                                    parametri.put("societaFornitrice", societaFornitrice);
                                    parametri.put("canale", "APPS");
                                    parametri.put("modalita", "MANUALE");
                                    parametri.put("codiceCliente", codiceCliente);
                                    try {
                                        stAvFornServiceCall.executeHttpsGetAutoletturaInsert(Constants.URL_ANNULLA_AUTOLETTURA_GAS, parametri,
                                                (AutoLetturaInsert) AutoLetturaInsert.getContext(), "ANNULLA", null);
                                    } catch (Exception e) {
                                        // OopsPage per annulla invio
                                        Intent intentOops = new Intent(getApplicationContext(), OopsPageAutoletturaInsertAnnulla.class);
                                        intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
                                        intentOops.putExtra("ID_OPERATION", idOperationInvio);
                                        intentOops.putExtra("PDF", pdf);
                                        intentOops.putExtra("VALORE_AUTOLETTURA", valoreAutolettura);
                                        intentOops.putExtra("SISTEMA_BILLING", sistemaBilling);
                                        intentOops.putExtra("SOCIETA_FORNITRICE", societaFornitrice);
                                        intentOops.putExtra("CANALE", "APPS");
                                        intentOops.putExtra("MODALITA", "MANUALE");
                                        intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
                                        startActivity(intentOops);
                                        overridePendingTransition(R.anim.fade, R.anim.hold);
                                    }
                                }
                                /*******************************************************************************************/
                                // Alert da commentare e sopra da scommentare se
                                // si vuole fare la chiamata per annulla
                                // autolettura
								/*
								 * AlertDialog.Builder builder2 = new
								 * AlertDialog.Builder(context);
								 * builder2.setMessage
								 * (getResources().getString(R
								 * .string.annulla_invio
								 * )).setCancelable(false).setPositiveButton
								 * ("OK", new DialogInterface.OnClickListener()
								 * { public void onClick(DialogInterface dialog,
								 * int id) { Intent intentAutoletturaConttatori
								 * = new Intent(getApplicationContext(),
								 * Autolettura.class);
								 * AutoLetturaInsert.this.finish();
								 * startActivity(intentAutoletturaConttatori);
								 * overridePendingTransition(R.anim.fade,
								 * R.anim.hold); } }); builder2.create().show();
								 */

                            }
                        });
                        builder.create().show();
                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setMessage(getResources().getString(R.string.conferma_invio)).setCancelable(false)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        Intent intentAutoletturaConttatori = new Intent(getApplicationContext(), Autolettura.class);
                                        AutoLetturaInsert.this.finish();
                                        startActivity(intentAutoletturaConttatori);
                                        overridePendingTransition(R.anim.fade, R.anim.hold);
                                    }
                                });
                        builder.create().show();
                    }
                }
            } else if (esito.equals("424")) {
                if (codiceEsito.equals("OK_2")) {
                    if (ops == null) {
                        if (descEsito.contains("possibile effettuare solo un")) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(context);
                            builder.setMessage("E' possibile effettuare solo un'operazione di autolettura al giorno").setCancelable(false)
                                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            Intent intentAutoletturaConttatori = new Intent(getApplicationContext(), Autolettura.class);
                                            AutoLetturaInsert.this.finish();
                                            startActivity(intentAutoletturaConttatori);
                                            overridePendingTransition(R.anim.fade, R.anim.hold);
                                        }
                                    });
                            builder.create().show();
                        } else {
                            AlertDialog.Builder builder = new AlertDialog.Builder(context);
                            builder.setMessage(descEsito).setCancelable(false).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    Intent intentAutoletturaConttatori = new Intent(getApplicationContext(), Autolettura.class);
                                    AutoLetturaInsert.this.finish();
                                    startActivity(intentAutoletturaConttatori);
                                    overridePendingTransition(R.anim.fade, R.anim.hold);
                                }
                            });
                            builder.create().show();
                        }
                    } else {
                        Intent intentOops = new Intent(getApplicationContext(), OopsPageAutoletturaInsert.class);
                        if (descEsito.contains("possibile effettuare solo un")) {
                            intentOops.putExtra("MESSAGGIO", "E' possibile effettuare solo un'operazione di autolettura al giorno");
                        } else {
                            intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
                        }
                        intentOops.putExtra("PDF", pdf);
                        intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
                        intentOops.putExtra("VALORE_AUTOLETTURA", valoreAutolettura);
                        intentOops.putExtra("SISTEMA_BILLING", sistemaBilling);
                        intentOops.putExtra("SOCIETA_FORNITRICE", societaFornitrice);
                        intentOops.putExtra("NUMERO_CIFRE_MISURATORE", numeroCifreMisuratore);
                        intentOops.putExtra("CANALE", "APPS");
                        intentOops.putExtra("MODALITA", "MANUALE");
                        startActivity(intentOops);
                        overridePendingTransition(R.anim.fade, R.anim.hold);
                    }
                } else {
                    Intent intentOops = new Intent(getApplicationContext(), OopsPageAutoletturaInsert.class);
                    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
                    intentOops.putExtra("PDF", pdf);
                    intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
                    intentOops.putExtra("VALORE_AUTOLETTURA", valoreAutolettura);
                    intentOops.putExtra("SISTEMA_BILLING", sistemaBilling);
                    intentOops.putExtra("SOCIETA_FORNITRICE", societaFornitrice);
                    intentOops.putExtra("NUMERO_CIFRE_MISURATORE", numeroCifreMisuratore);
                    intentOops.putExtra("CANALE", "APPS");
                    intentOops.putExtra("MODALITA", "MANUALE");
                    startActivity(intentOops);
                    overridePendingTransition(R.anim.fade, R.anim.hold);
                }
            } else {
                // Gestione OopsPage per chiamata invio autolettura
                Intent intentOops = new Intent(getApplicationContext(), OopsPageAutoletturaInsertAnnulla.class);
                intentOops.putExtra("ID_OPERATION", idOperationInvio);
                intentOops.putExtra("PDF", pdf);
                intentOops.putExtra("VALORE_AUTOLETTURA", valoreAutolettura);
                intentOops.putExtra("SISTEMA_BILLING", sistemaBilling);
                intentOops.putExtra("SOCIETA_FORNITRICE", societaFornitrice);
                intentOops.putExtra("CANALE", "APPS");
                intentOops.putExtra("MODALITA", "MANUALE");
                intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
                // errori ops page
                if (esito.equals("309")) {
                    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
                    startActivity(intentOops);
                    overridePendingTransition(R.anim.fade, R.anim.hold);
                } else if (esito.equals("400")) {
                    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
                    startActivity(intentOops);
                    overridePendingTransition(R.anim.fade, R.anim.hold);
                } else if (esito.equals("420")) {
                    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
                    startActivity(intentOops);
                    overridePendingTransition(R.anim.fade, R.anim.hold);
                } else if (esito.equals("449")) {
                    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
                    startActivity(intentOops);
                    overridePendingTransition(R.anim.fade, R.anim.hold);
                } else if (esito.equals("451")) {
                    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_popup_451));
                    startActivity(intentOops);
                    overridePendingTransition(R.anim.fade, R.anim.hold);
                }
                // errori popup
                else if (esito.equals("307")) {
                    Utils.popup(AutoLetturaInsert.this, getResources().getString(R.string.errore_popup_307), "Riprova");
                } else if (esito.equals("435")) {
                    Utils.popup(AutoLetturaInsert.this, getResources().getString(R.string.errore_popup_435), "Riprova");
                } else if (esito.equals("437")) {
                    Utils.popup(AutoLetturaInsert.this, getResources().getString(R.string.errore_popup_437), "Riprova");
                } else if (esito.equals("438")) {
                    Utils.popup(AutoLetturaInsert.this, getResources().getString(R.string.errore_popup_438), "Riprova");
                } else if (esito.equals("443")) {
                    Utils.popup(AutoLetturaInsert.this, getResources().getString(R.string.errore_popup_443), "Riprova");
                } else if (esito.equals("444")) {
                    Utils.popup(AutoLetturaInsert.this, getResources().getString(R.string.errore_popup_444), "Riprova");
                } else if (esito.equals("445")) {
                    Utils.popup(AutoLetturaInsert.this, getResources().getString(R.string.errore_popup_445), "Riprova");
                } else if (esito.equals("446")) {
                    Utils.popup(AutoLetturaInsert.this, getResources().getString(R.string.errore_popup_446), "Riprova");
                } else if (esito.equals("447")) {
                    Utils.popup(AutoLetturaInsert.this, getResources().getString(R.string.errore_popup_447), "Riprova");
                } else {
                    if (descEsito.equals("")) {
                        intentOops = new Intent(getApplicationContext(), OopsPageSessioneScaduta.class);
                        intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.sessione_scaduta));
                    } else {
                        intentOops.putExtra("MESSAGGIO", descEsito);
                    }

                    startActivity(intentOops);
                    overridePendingTransition(R.anim.fade, R.anim.hold);
                }
            }
        }

    }

    public void postChiamata(String rispostaChiamata, List<Cookie> lista, HashMap<String, String> parametri, String annullaOrConferma) {
        if (animation != null) {
            animation.stop();
            imageAnim.setVisibility(View.INVISIBLE);
        }
        final ImageView autoLetturaInsert = (ImageView) findViewById(R.id.autolettura_insert_indietro);
        autoLetturaInsert.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
        autoLetturaInsert.getBackground().invalidateSelf();

        JSONObject rispostaJSON = new JSONObject();
        String esito = "";
        String codiceEsito = "";
        String descEsito = "";

        try {
            rispostaJSON = new JSONObject(rispostaChiamata);
            esito = rispostaJSON.getString("esito");
            codiceEsito = rispostaJSON.getString("codiceEsito");
            descEsito = rispostaJSON.getString("descEsito");
            idOperationInvio = parametri.get("idOperation");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (esito.equals("200")) {
            if (annullaOrConferma.equals("ANNULLA")) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage(getResources().getString(R.string.annulla_invio)).setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent intentAutolettura = new Intent(getApplicationContext(), Autolettura.class);
                                AutoLetturaInsert.this.finish();
                                startActivity(intentAutolettura);
                                overridePendingTransition(R.anim.fade, R.anim.hold);
                            }
                        });
                builder.create().show();
            }
            if (annullaOrConferma.equals("CONFERMA")) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage(getResources().getString(R.string.conferma_invio)).setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent intentAutolettura = new Intent(getApplicationContext(), Autolettura.class);
                                AutoLetturaInsert.this.finish();
                                startActivity(intentAutolettura);
                                overridePendingTransition(R.anim.fade, R.anim.hold);
                            }
                        });
                builder.create().show();
            }
        } else {
            if (annullaOrConferma.equals("ANNULLA")) {
                // RAMO ANNULLA per esito diverso da 200

                Intent intentOops = new Intent(getApplicationContext(), OopsPageAutoletturaInsertAnnulla.class);
                intentOops.putExtra("ID_OPERATION", idOperationInvio);
                intentOops.putExtra("PDF", pdf);
                intentOops.putExtra("VALORE_AUTOLETTURA", valoreAutolettura);
                intentOops.putExtra("SISTEMA_BILLING", sistemaBilling);
                intentOops.putExtra("SOCIETA_FORNITRICE", societaFornitrice);
                intentOops.putExtra("CANALE", "APPS");
                intentOops.putExtra("MODALITA", "MANUALE");
                intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
                // errori ops page
                if (esito.equals("309")) {
                    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
                    startActivity(intentOops);
                    overridePendingTransition(R.anim.fade, R.anim.hold);
                } else if (esito.equals("400")) {
                    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
                    startActivity(intentOops);
                    overridePendingTransition(R.anim.fade, R.anim.hold);
                } else if (esito.equals("420")) {
                    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
                    startActivity(intentOops);
                    overridePendingTransition(R.anim.fade, R.anim.hold);
                } else if (esito.equals("449")) {
                    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
                    startActivity(intentOops);
                    overridePendingTransition(R.anim.fade, R.anim.hold);
                } else if (esito.equals("451")) {
                    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_popup_451));
                    startActivity(intentOops);
                    overridePendingTransition(R.anim.fade, R.anim.hold);
                }
                // errori popup
                else if (esito.equals("307")) {
                    Utils.popup(AutoLetturaInsert.this, getResources().getString(R.string.errore_popup_307), "Riprova");
                } else if (esito.equals("435")) {
                    Utils.popup(AutoLetturaInsert.this, getResources().getString(R.string.errore_popup_435), "Riprova");
                } else if (esito.equals("437")) {
                    Utils.popup(AutoLetturaInsert.this, getResources().getString(R.string.errore_popup_437), "Riprova");
                } else if (esito.equals("438")) {
                    Utils.popup(AutoLetturaInsert.this, getResources().getString(R.string.errore_popup_438), "Riprova");
                } else if (esito.equals("443")) {
                    Utils.popup(AutoLetturaInsert.this, getResources().getString(R.string.errore_popup_443), "Riprova");
                } else if (esito.equals("444")) {
                    Utils.popup(AutoLetturaInsert.this, getResources().getString(R.string.errore_popup_444), "Riprova");
                } else if (esito.equals("445")) {
                    Utils.popup(AutoLetturaInsert.this, getResources().getString(R.string.errore_popup_445), "Riprova");
                } else if (esito.equals("446")) {
                    Utils.popup(AutoLetturaInsert.this, getResources().getString(R.string.errore_popup_446), "Riprova");
                } else if (esito.equals("447")) {
                    Utils.popup(AutoLetturaInsert.this, getResources().getString(R.string.errore_popup_447), "Riprova");
                } else {
                    if (descEsito.equals("")) {
                        intentOops = new Intent(getApplicationContext(), OopsPageSessioneScaduta.class);
                        intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.sessione_scaduta));
                    } else {
                        intentOops.putExtra("MESSAGGIO", descEsito);
                    }

                    startActivity(intentOops);
                    overridePendingTransition(R.anim.fade, R.anim.hold);
                }
            }
            if (annullaOrConferma.equals("CONFERMA")) {
                // RAMO CONFERMA per esito diverso da 200

                Intent intentOops = new Intent(getApplicationContext(), OopsPageAutoletturaInsertConferma.class);
                intentOops.putExtra("PDF", pdf);
                intentOops.putExtra("VALORE_AUTOLETTURA", valoreAutolettura);
                intentOops.putExtra("SISTEMA_BILLING", sistemaBilling);
                intentOops.putExtra("SOCIETA_FORNITRICE", societaFornitrice);
                intentOops.putExtra("NUMERO_CIFRE_MISURATORE", numeroCifreMisuratore);
                intentOops.putExtra("CANALE", "APPS");
                intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
                intentOops.putExtra("MODALITA", "MANUALE");
                // errori ops page
                if (esito.equals("309")) {
                    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
                    startActivity(intentOops);
                    overridePendingTransition(R.anim.fade, R.anim.hold);
                } else if (esito.equals("400")) {
                    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
                    startActivity(intentOops);
                    overridePendingTransition(R.anim.fade, R.anim.hold);
                } else if (esito.equals("420")) {
                    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
                    startActivity(intentOops);
                    overridePendingTransition(R.anim.fade, R.anim.hold);
                } else if (esito.equals("449")) {
                    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
                    startActivity(intentOops);
                    overridePendingTransition(R.anim.fade, R.anim.hold);
                } else if (esito.equals("451")) {
                    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_popup_451));
                    startActivity(intentOops);
                    overridePendingTransition(R.anim.fade, R.anim.hold);
                }
                // errori popup
                else if (esito.equals("307")) {
                    Utils.popup(AutoLetturaInsert.this, getResources().getString(R.string.errore_popup_307), "Riprova");
                } else if (esito.equals("435")) {
                    Utils.popup(AutoLetturaInsert.this, getResources().getString(R.string.errore_popup_435), "Riprova");
                } else if (esito.equals("437")) {
                    Utils.popup(AutoLetturaInsert.this, getResources().getString(R.string.errore_popup_437), "Riprova");
                } else if (esito.equals("438")) {
                    Utils.popup(AutoLetturaInsert.this, getResources().getString(R.string.errore_popup_438), "Riprova");
                } else if (esito.equals("443")) {
                    Utils.popup(AutoLetturaInsert.this, getResources().getString(R.string.errore_popup_443), "Riprova");
                } else if (esito.equals("444")) {
                    Utils.popup(AutoLetturaInsert.this, getResources().getString(R.string.errore_popup_444), "Riprova");
                } else if (esito.equals("445")) {
                    Utils.popup(AutoLetturaInsert.this, getResources().getString(R.string.errore_popup_445), "Riprova");
                } else if (esito.equals("446")) {
                    Utils.popup(AutoLetturaInsert.this, getResources().getString(R.string.errore_popup_446), "Riprova");
                } else if (esito.equals("447")) {
                    Utils.popup(AutoLetturaInsert.this, getResources().getString(R.string.errore_popup_447), "Riprova");
                }
            }
        }

    }

    public void postChiamataException(boolean exception, String annullaOrConferma) {
        if (exception) {
            if (animation != null) {
                animation.stop();
                imageAnim.setVisibility(View.INVISIBLE);
            }

            final ImageView autoLetturaInsert = (ImageView) findViewById(R.id.autolettura_insert_indietro);
            autoLetturaInsert.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
            autoLetturaInsert.getBackground().invalidateSelf();
            Intent intentOops = null;

            if (annullaOrConferma != null && annullaOrConferma.equals("CONFERMA")) {
                intentOops = new Intent(getApplicationContext(), OopsPageAutoletturaInsertConferma.class);
                intentOops.putExtra("PDF", pdf);
                intentOops.putExtra("VALORE_AUTOLETTURA", valoreAutolettura);
                intentOops.putExtra("SISTEMA_BILLING", sistemaBilling);
                intentOops.putExtra("SOCIETA_FORNITRICE", societaFornitrice);
                intentOops.putExtra("NUMERO_CIFRE_MISURATORE", numeroCifreMisuratore);
                intentOops.putExtra("CANALE", "APPS");
                intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
                intentOops.putExtra("MODALITA", "MANUALE");
            } else if (annullaOrConferma != null && annullaOrConferma.equals("ANNULLA")) {
                intentOops = new Intent(getApplicationContext(), OopsPageAutoletturaInsertAnnulla.class);
                intentOops.putExtra("ID_OPERATION", idOperationInvio);
                intentOops.putExtra("PDF", pdf);
                intentOops.putExtra("VALORE_AUTOLETTURA", valoreAutolettura);
                intentOops.putExtra("SISTEMA_BILLING", sistemaBilling);
                intentOops.putExtra("SOCIETA_FORNITRICE", societaFornitrice);
                intentOops.putExtra("CANALE", "APPS");
                intentOops.putExtra("MODALITA", "MANUALE");
                intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
            } else {
                intentOops = new Intent(getApplicationContext(), OopsPageAutoletturaInsert.class);
                intentOops.putExtra("PDF", pdf);
                intentOops.putExtra("VALORE_AUTOLETTURA", valoreAutolettura);
                intentOops.putExtra("SISTEMA_BILLING", sistemaBilling);
                intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
                intentOops.putExtra("SOCIETA_FORNITRICE", societaFornitrice);
                intentOops.putExtra("NUMERO_CIFRE_MISURATORE", numeroCifreMisuratore);
                intentOops.putExtra("CANALE", "APPS");
                intentOops.putExtra("MODALITA", "MANUALE");
            }

            intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
            // AutoLetturaInsert.this.finish();
            startActivity(intentOops);
            overridePendingTransition(R.anim.fade, R.anim.hold);
        }
    }

    // ===============================================
    // sezione verifica periodo autolettura post chiamata
    // ===============================================

    public void postChiamataProssimaBolletta(String rispostaChiamata, List<Cookie> lista, HashMap<String, String> parametri, boolean prossimaBolletta,
                                             boolean bolleteRateizzabili) {
        if (animation != null) {
            animation.stop();
            imageAnim.setVisibility(View.INVISIBLE);
        }

        final ImageView indietro = (ImageView) findViewById(R.id.autolettura_insert_indietro);
        indietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
        indietro.getBackground().invalidateSelf();

        JSONArray rispostaJSON = new JSONArray();
        String esito = "";
        String descEsito = "";
        try {
            rispostaJSON = new JSONArray(rispostaChiamata);
            JSONObject oggettoRisposta = (JSONObject) rispostaJSON.get(0);
            esito = oggettoRisposta.getString("esito");
            descEsito = oggettoRisposta.getString("descEsito");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (esito.equals("200")) {
            Constants.SERVIZIO_INFO_PROSSIMA_FATTURA = rispostaChiamata;
            Intent intentBollettePagamentiProxBol = new Intent(getApplicationContext(), BollettePagamentiProssimaBolletta.class);
            intentBollettePagamentiProxBol.putExtra("PRIMOCONTO", parametri.get("codiceContoCliente"));
            intentBollettePagamentiProxBol.putExtra("SOURCE_ACTIVITY", AutoLetturaInsert.class.getName());
            intentBollettePagamentiProxBol.putExtra("NUMCIFREMISURATORE", numeroCifreMisuratore);
            intentBollettePagamentiProxBol.putExtra("PDF", pdf);
            intentBollettePagamentiProxBol.putExtra("PDR", pdr);
            intentBollettePagamentiProxBol.putExtra("SISTEMABILLING", sistemaBilling);
            intentBollettePagamentiProxBol.putExtra("SOCIETAFORNITRICE", societaFornitrice);
            intentBollettePagamentiProxBol.putExtra("CURRENT_CODICE_CONTO", codiceConto);

            try {
                String tipologiaConto = JSONUtility.cercaTipologiaFornitura(new JSONArray(rispostaLogin), codiceConto);

                boolean isDual = isDual(new JSONArray(rispostaLogin), codiceConto);

                Log.d("AUTOLETTURA", "AutoLetturaInsert - tipologiaConto: " + tipologiaConto + " - isDual: " + isDual);

                if (isDual) {

                    tipologia = "DUAL";
                } else {
                    if (tipologiaConto.equals("GAS")) {
                        tipologia = "GAS";
                    } else if (tipologiaConto.equals("POWER")) {
                        tipologia = "LUCE";
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            intentBollettePagamentiProxBol.putExtra("TIPOLOGIA", tipologia);

            // intentBollettePagamentiProxBol.putExtra("TIPOLOGIA", tipologia);
            // intentBollettePagamentiProxBol.putExtra("INDICE_SPINNER",
            // positionSpinnerCurrentSelected);
            this.finish();
            startActivity(intentBollettePagamentiProxBol);
            overridePendingTransition(R.anim.fade, R.anim.hold);
        } else {
            Intent intentOops = new Intent();
            intentOops = new Intent(getApplicationContext(), OopsPageBollettePagamentiProssimaBolletta.class);
            // intentOops.putExtra("TIPOLOGIA", tipologia);
            intentOops.putExtra("CODICE_CLIENTE", parametri.get("codiceCliente"));
            intentOops.putExtra("CODICE_CONTO_CLIENTE", parametri.get("codiceContoCliente"));

            // errori popup
            if (esito.equals("307")) {
                intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_popup_307));
                startActivity(intentOops);
                overridePendingTransition(R.anim.fade, R.anim.hold);
            } else if (esito.equals("309")) {
                intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
                startActivity(intentOops);
                overridePendingTransition(R.anim.fade, R.anim.hold);
            } else if (esito.equals("451")) {
                intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_popup_451));
                startActivity(intentOops);
                overridePendingTransition(R.anim.fade, R.anim.hold);
            } else if (Utilities.arrayContainsString(getResources().getStringArray(R.array.execute_domiciliazione_error_codes), esito)) {
                Utils.popup(AutoLetturaInsert.this, descEsito, "Riprova");
            } else {
                intentOops.putExtra("MESSAGGIO", descEsito);
                startActivity(intentOops);
                overridePendingTransition(R.anim.fade, R.anim.hold);
            }
        }
    }

    public void postChiamataProssimaBollettaException(boolean exception, HashMap<String, String> parameterMap, boolean prossimaFattura,
                                                      boolean bolleteRateizzabili) {
        if (exception) {
            if (animation != null) {
                animation.stop();
                imageAnim.setVisibility(View.INVISIBLE);
            }

            final ImageView indietro = (ImageView) findViewById(R.id.autolettura_insert_indietro);
            indietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
            indietro.getBackground().invalidateSelf();

            Intent intentOops = new Intent();

            intentOops = new Intent(getApplicationContext(), OopsPageBollettePagamentiProssimaBolletta.class);
            // intentOops.putExtra("TIPOLOGIA", tipologia);

            intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
            intentOops.putExtra("CODICE_CLIENTE", parameterMap.get("codiceCliente"));
            intentOops.putExtra("CODICE_CONTO_CLIENTE", parameterMap.get("codiceContoCliente"));

            // BollettePagamenti.this.finish();
            startActivity(intentOops);
            overridePendingTransition(R.anim.fade, R.anim.hold);
        }
    }

    // ===============================================
    // sezione storicoletture post chiamata
    // ===============================================

    public void postChiamataStoricoLetture(String rispostaChiamata, List<Cookie> lista, String pdf, String pdr, HashMap<String, String> parametri) {
        JSONArray rispostaJSON = new JSONArray();
        String esito = "";
        String descEsito = "";
        if (animation != null) {
            animation.stop();
            imageAnim.setVisibility(View.INVISIBLE);
        }

        final ImageView domiciliazioneIndietro = (ImageView) findViewById(R.id.autolettura_insert_indietro);
        domiciliazioneIndietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
        domiciliazioneIndietro.getBackground().invalidateSelf();

        try {
            rispostaJSON = new JSONArray(rispostaChiamata);
            JSONObject oggettoRisposta = (JSONObject) rispostaJSON.get(0);
            esito = oggettoRisposta.getString("esito");
            descEsito = oggettoRisposta.getString("descEsito");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (esito.equals("200")) {
            Constants.SERVIZIO_STORICO_LETTURE = rispostaChiamata;
            Intent intentStoricoLetture = new Intent(getApplicationContext(), StoricoLettureDetail.class);
            intentStoricoLetture.putExtra("PDF", pdf);
            intentStoricoLetture.putExtra("PDR", pdr);
            intentStoricoLetture.putExtra("PARAMETRI", parametri);

            startActivity(intentStoricoLetture);
            finish();
            overridePendingTransition(R.anim.fade, R.anim.hold);
        } else {
            Intent intentOops = new Intent(getApplicationContext(), OopsPageStoricoLetture.class);
            // errori ops page
            if (esito.equals("309")) {
                intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
                startActivity(intentOops);
                overridePendingTransition(R.anim.fade, R.anim.hold);

            } else if (esito.equals("400")) {
                intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
                startActivity(intentOops);
                overridePendingTransition(R.anim.fade, R.anim.hold);

            } else if (esito.equals("420")) {
                intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
                startActivity(intentOops);
                overridePendingTransition(R.anim.fade, R.anim.hold);

            } else if (esito.equals("449")) {
                intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
                startActivity(intentOops);
                overridePendingTransition(R.anim.fade, R.anim.hold);

            } else if (esito.equals("451")) {
                intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_popup_451));
                startActivity(intentOops);
                overridePendingTransition(R.anim.fade, R.anim.hold);

            }
            // errori popup
            else if (esito.equals("307")) {
                Utils.popup(this, getResources().getString(R.string.errore_popup_307), "Riprova");

            } else if (esito.equals("435")) {
                Utils.popup(this, getResources().getString(R.string.errore_popup_435), "Riprova");

            } else if (esito.equals("437")) {
                Utils.popup(this, getResources().getString(R.string.errore_popup_437), "Riprova");

            } else if (esito.equals("438")) {
                Utils.popup(this, getResources().getString(R.string.errore_popup_438), "Riprova");

            } else if (esito.equals("443")) {
                Utils.popup(this, getResources().getString(R.string.errore_popup_443), "Riprova");

            } else if (esito.equals("444")) {
                Utils.popup(this, getResources().getString(R.string.errore_popup_444), "Riprova");

            } else if (esito.equals("445")) {
                Utils.popup(this, getResources().getString(R.string.errore_popup_445), "Riprova");

            } else if (esito.equals("446")) {
                Utils.popup(this, getResources().getString(R.string.errore_popup_446), "Riprova");

            } else if (esito.equals("447")) {
                Utils.popup(this, getResources().getString(R.string.errore_popup_447), "Riprova");

            } else {
                intentOops.putExtra("MESSAGGIO", descEsito);
                startActivity(intentOops);
                overridePendingTransition(R.anim.fade, R.anim.hold);
            }

        }

    }

    public void postChiamataStoricoLettureException(boolean exception) {
        if (exception) {
            if (animation != null) {
                animation.stop();
                imageAnim.setVisibility(View.INVISIBLE);
            }

            final ImageView domiciliazioneIndietro = (ImageView) findViewById(R.id.autolettura_insert_indietro);
            domiciliazioneIndietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
            domiciliazioneIndietro.getBackground().invalidateSelf();

            Intent intentOops = new Intent(getApplicationContext(), OopsPageStoricoLetture.class);
            intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
            intentOops.putExtra("PDF", pdf);
            intentOops.putExtra("SISTEMA_BILLING", sistemaBilling);
            intentOops.putExtra("SOCIETA_FORNITRICE", societaFornitrice);
            intentOops.putExtra("NUM_CIFRE_MISURATORE", numeroCifreMisuratore);
            intentOops.putExtra("PDR", pdr);
            // StatoAttivazioneFornitura.this.finish();
            startActivity(intentOops);
            overridePendingTransition(R.anim.fade, R.anim.hold);
        }
    }

    public void impostaValoreLettura(LinearLayout valoreLettura, String valoreAutoletturaString) {
        for (int m = 0; m < valoreAutoletturaString.length(); m++) {
            String numeroIesimoLettura = valoreAutoletturaString.substring(m, m + 1);
            int numeroIesimoLetturaInt = Integer.valueOf(numeroIesimoLettura);
            switch (numeroIesimoLetturaInt) {
                case 0:
                    ((ImageView) valoreLettura.getChildAt(m)).setImageResource(R.drawable.numero_0);
                    break;
                case 1:
                    ((ImageView) valoreLettura.getChildAt(m)).setImageResource(R.drawable.numero_1);
                    break;
                case 2:
                    ((ImageView) valoreLettura.getChildAt(m)).setImageResource(R.drawable.numero_2);
                    break;
                case 3:
                    ((ImageView) valoreLettura.getChildAt(m)).setImageResource(R.drawable.numero_3);
                    break;
                case 4:
                    ((ImageView) valoreLettura.getChildAt(m)).setImageResource(R.drawable.numero_4);
                    break;
                case 5:
                    ((ImageView) valoreLettura.getChildAt(m)).setImageResource(R.drawable.numero_5);
                    break;
                case 6:
                    ((ImageView) valoreLettura.getChildAt(m)).setImageResource(R.drawable.numero_6);
                    break;
                case 7:
                    ((ImageView) valoreLettura.getChildAt(m)).setImageResource(R.drawable.numero_7);
                    break;
                case 8:
                    ((ImageView) valoreLettura.getChildAt(m)).setImageResource(R.drawable.numero_8);
                    break;
                case 9:
                    ((ImageView) valoreLettura.getChildAt(m)).setImageResource(R.drawable.numero_9);
                    break;
            }

        }
    }

    public String getCodiceCliente(String rispostaLogin) {
        String result = "";
        try {
            JSONArray loginJson = new JSONArray(rispostaLogin);
            JSONObject datiCliente = (JSONObject) loginJson.get(1);
            result = datiCliente.getString("CodiceCliente");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public String differenzaData(Date data) {
        String result = "";

        Date adesso = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        sdf.format(adesso);

        return result;
    }

    public String formattaDataLetturaDaInviare() {
        String result = "";
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date adesso = new Date();
        String adessoString = sdf.format(adesso);
        String giorno = adessoString.substring(0, 2);
        String mese = adessoString.substring(3, 5);
        String anno = adessoString.substring(6, adessoString.length());
        result = result + giorno + " ";
        int meseInt = Integer.valueOf(mese);
        switch (meseInt) {
            case 1:
                result = result + "gennaio ";
                break;
            case 2:
                result = result + "febbraio ";
                break;
            case 3:
                result = result + "marzo ";
                break;
            case 4:
                result = result + "aprile ";
                break;
            case 5:
                result = result + "maggio ";
                break;
            case 6:
                result = result + "giugno ";
                break;
            case 7:
                result = result + "luglio ";
                break;
            case 8:
                result = result + "agosto ";
                break;
            case 9:
                result = result + "settembre ";
                break;
            case 10:
                result = result + "ottobre ";
                break;
            case 11:
                result = result + "novembre ";
                break;
            case 12:
                result = result + "dicembre ";
                break;
        }
        result = result + anno + ":";
        return result;
    }

    public void impostaPickers(LinearLayout pickers, String lettura) {
        for (int l = 0; l < lettura.length(); l++) {
            String carattere = lettura.substring(((lettura.length() - 1) - l), ((lettura.length()) - l));
            int numero = Integer.valueOf(carattere);
            switch (l) {
                case 0:
                    ((NumberPicker) pickers.getChildAt(lettura.length() - 1)).setCurrent(numero);
                    break;
                case 1:
                    ((NumberPicker) pickers.getChildAt(lettura.length() - 2)).setCurrent(numero);
                    break;
                case 2:
                    ((NumberPicker) pickers.getChildAt(lettura.length() - 3)).setCurrent(numero);
                    break;
                case 3:
                    ((NumberPicker) pickers.getChildAt(lettura.length() - 4)).setCurrent(numero);
                    break;
                case 4:
                    ((NumberPicker) pickers.getChildAt(lettura.length() - 5)).setCurrent(numero);
                    break;
                case 5:
                    ((NumberPicker) pickers.getChildAt(lettura.length() - 6)).setCurrent(numero);
                    break;
                case 6:
                    ((NumberPicker) pickers.getChildAt(lettura.length() - 7)).setCurrent(numero);
                    break;
                case 7:
                    ((NumberPicker) pickers.getChildAt(lettura.length() - 8)).setCurrent(numero);
                    break;

            }

        }
		/*
		 * if(lettura.length() < 8){ for(int k = 1; k <= 8 - lettura.length();
		 * k++){ switch(k){ case 8: ((NumberPicker) pickers.getChildAt(8 -
		 * lettura.length() -8)).setCurrent(0); break; case 7: ((NumberPicker)
		 * pickers.getChildAt(8 - lettura.length()-6)).setCurrent(0); break;
		 * case 6: ((NumberPicker) pickers.getChildAt(8 - lettura.length()
		 * -5)).setCurrent(0); break; case 5: ((NumberPicker)
		 * pickers.getChildAt(8 - lettura.length() -4)).setCurrent(0); break;
		 * case 4: ((NumberPicker) pickers.getChildAt(8 - lettura.length()
		 * -3)).setCurrent(0); break; case 3: ((NumberPicker)
		 * pickers.getChildAt(8 - lettura.length() -2)).setCurrent(0); break;
		 * case 2: ((NumberPicker) pickers.getChildAt(8 - lettura.length()
		 * -1)).setCurrent(0); break; case 1: ((NumberPicker)
		 * pickers.getChildAt(8 - lettura.length())).setCurrent(0); break; } } }
		 */

    }

    public void riazzeraPickers() {
        np6.setCurrent(0);
        np5.setCurrent(0);
        np4.setCurrent(0);
        np3.setCurrent(0);
        np2.setCurrent(0);
        np1.setCurrent(0);

    }

    public String getLetturaFromPickers(LinearLayout pickers) {
        String result = "";
        int numeroFigli = pickers.getChildCount();
        for (int l = 0; l < numeroFigli; l++) {
            int numero = 0;
            switch (l) {
                case 7:
                    numero = ((NumberPicker) pickers.getChildAt(7)).getCurrent();
                    result = result + "" + numero;
                    break;
                case 6:
                    numero = ((NumberPicker) pickers.getChildAt(6)).getCurrent();
                    result = result + "" + numero;
                    break;
                case 5:
                    numero = ((NumberPicker) pickers.getChildAt(5)).getCurrent();
                    result = result + "" + numero;
                    break;
                case 4:
                    numero = ((NumberPicker) pickers.getChildAt(4)).getCurrent();
                    result = result + "" + numero;
                    break;
                case 3:
                    numero = ((NumberPicker) pickers.getChildAt(3)).getCurrent();
                    result = result + "" + numero;
                    break;
                case 2:
                    numero = ((NumberPicker) pickers.getChildAt(2)).getCurrent();
                    result = result + "" + numero;
                    break;
                case 1:
                    numero = ((NumberPicker) pickers.getChildAt(1)).getCurrent();
                    result = result + "" + numero;
                    break;
                case 0:
                    numero = ((NumberPicker) pickers.getChildAt(0)).getCurrent();
                    result = result + "" + numero;
                    break;
            }

        }
        return result;
    }

    private boolean isDual(JSONArray arrayFornitureLogin, String codiceattoreo) {
        boolean result = false;
        int count = 0;
        JSONObject anagrContoIesimo = new JSONObject();
        try {
            for (int i = 2; i < arrayFornitureLogin.length(); i++) {
                JSONArray contoIesimo;
                contoIesimo = arrayFornitureLogin.getJSONArray(i);
                anagrContoIesimo = contoIesimo.getJSONObject(0);

                String codiceContoIesimo = anagrContoIesimo.getString("CodiceConto");
                if (!codiceContoIesimo.equals(codiceConto)) {
                    continue;
                } else {
                    // ho trovato il conto codiceConto
                    count++;
                }
            }
            if (count == 2) {
                result = true;
            } else {
                result = false;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return result;
    }

    public void onBackPressed() {

        animation = Utilities.animation(this);
        imageAnim = (ImageView) findViewById(R.id.waiting_anim);
        imageAnim.setVisibility(View.VISIBLE);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
            imageAnim.setBackgroundDrawable(animation);
        } else {
            imageAnim.setBackground(animation);
        }
        animation.start();

        Intent intentHome = new Intent(getApplicationContext(), Autolettura.class);
        AutoLetturaInsert.this.finish();
        startActivity(intentHome);
        overridePendingTransition(R.anim.fade, R.anim.hold);
    }
}
