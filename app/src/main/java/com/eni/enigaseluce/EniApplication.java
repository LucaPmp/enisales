package com.eni.enigaseluce;

import android.app.Application;

import com.crittercism.app.Crittercism;
import com.eni.enigaseluce.httpcall.Constants;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

/**
 * Created by luca.quaranta on 08/10/2015.
 */
public class EniApplication extends Application {

    public static GoogleAnalytics analytics;
    public static Tracker tracker;

    @Override
    public void onCreate() {
        super.onCreate();

        analytics = GoogleAnalytics.getInstance(this);
        analytics.setLocalDispatchPeriod(1800);

//        TODO LQ: Da inserire il seguente tracker per ENI: "UA-68597500-6"
        if (Constants.TEST_ENV) {
            tracker = analytics.newTracker("UA-68597500-6"); // Replace with actual tracker id   // Rinaldo: UA-68106193-3
        } else {
            tracker = analytics.newTracker("UA-68597500-5");
        }
        tracker.enableExceptionReporting(false);
        tracker.enableAdvertisingIdCollection(false);
        tracker.enableAutoActivityTracking(false);

        if (Constants.TEST_ENV) {
//            Crittercism.initialize(getApplicationContext(), "56125690d224ac0a00ed3f24");
        }

    }

    public void sendAnalyticsEvent(String screenName){
//        tracker.send(new HitBuilders.EventBuilder()
//                .setCategory(category)
//                .setAction(action)
//                .setLabel(label)
//                .build());

        tracker.setScreenName(screenName);
        tracker.send(new HitBuilders.ScreenViewBuilder().build());
//        Esempio di chiamata
//        ((EniApplication)getApplication()).sendAnalyticsEvent("categoria", "action", "label");
    }

}
