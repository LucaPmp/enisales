package com.eni.enigaseluce.bollettePagamenti;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.http.cookie.Cookie;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.AnimationDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.eni.enigaseluce.R;
import com.eni.enigaseluce.error.OopsPageBollettePagamentiRate;
import com.eni.enigaseluce.error.OopsPageSessioneScaduta;
import com.eni.enigaseluce.error.Utils;
import com.eni.enigaseluce.httpcall.Constants;
import com.eni.enigaseluce.httpcall.ServiceCall;
import com.eni.enigaseluce.httpcall.ServiceCallFactory;
import com.eni.enigaseluce.utils.Utilities;

@SuppressLint("NewApi")
public class BollettePagamentiRateizzate extends Activity {
	private Context context;
	private static Context staticContext;
	AnimationDrawable animation;
	ImageView imageAnim;

	static LinearLayout pager;

	public String rispostaLogin = Constants.SERVIZIO_LOGIN;
	public String rispostaInfoFattureRateizzabili = Constants.SERVIZIO_INFO_FATTURE_RATEIZZABILI;

	private static LinearLayout waiting_download;
	LinearLayout contenuto;

	public static String nomeInteroCliente = "";
	static LayoutInflater inflater;
	static LayoutInflater inflaterDialog;

	String codiceEsito;
	String primoConto;

	boolean domiciliazioneApprovata = false;
	boolean paginaDomiciliazioneApprovataInfoVisualizzata = false;

	public static Context getContext() {
		return BollettePagamentiRateizzate.staticContext;
	}

	/**
	 * @return the waiting_download
	 */
	public static LinearLayout getWaiting_download() {
		return waiting_download;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.bollette_pagamenti_rate);

		context = this;
		BollettePagamentiRateizzate.staticContext = this;
		primoConto = getIntent().getStringExtra("PRIMOCONTO");

		paginaDomiciliazioneApprovataInfoVisualizzata = getIntent().getBooleanExtra("infoDomiciliazioneApprovata", false);
	}

	@Override
	protected void onStart() {
		super.onStart();

		final ImageView indietro = (ImageView) findViewById(R.id.bollette_pagamenti_indietro);
		indietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
		indietro.getBackground().invalidateSelf();
		indietro.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				indietro.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
				indietro.getBackground().invalidateSelf();

				animation = Utilities.animation(BollettePagamentiRateizzate.this);
				imageAnim = (ImageView) findViewById(R.id.waiting_anim);
				imageAnim.setVisibility(View.VISIBLE);
				if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
					imageAnim.setBackgroundDrawable(animation);
				} else {
					imageAnim.setBackground(animation);
				}
				animation.start();

				Intent intentBollettePagamenti = new Intent(getApplicationContext(), BollettePagamenti.class);
				intentBollettePagamenti.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
				intentBollettePagamenti.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
				BollettePagamentiRateizzate.this.finish();
				startActivity(intentBollettePagamenti);
				overridePendingTransition(R.anim.fade, R.anim.hold);
			}
		});

		/*final ImageView help = (ImageView) findViewById(R.id.bollette_pagamenti_help);
		help.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
		help.getBackground().invalidateSelf();
		help.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				help.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
				help.getBackground().invalidateSelf();
				animation = new AnimationDrawable();
				animation.addFrame(getResources().getDrawable(R.drawable.anim_load2), 300);
				animation.addFrame(getResources().getDrawable(R.drawable.anim_load3), 300);
				animation.addFrame(getResources().getDrawable(R.drawable.anim_load4), 300);
				animation.setOneShot(false);
				imageAnim = (ImageView) findViewById(R.id.waiting_bollette_rate_pagamenti);
				imageAnim.setVisibility(View.VISIBLE);
				if (Build.VERSION.SDK_INT < 16) {
					imageAnim.setBackgroundDrawable(animation);
				} else {
					imageAnim.setBackground(animation);
				} // start the animation!
				animation.start();

				Intent intentHelp = new Intent(getApplicationContext(), BollettePagamentiHelp.class);
				intentHelp.putExtra("ORIGINE", Constants.ORIGINE_BOLLETTE_RATEIZZABILI);
				intentHelp.putExtra("PRIMOCONTO", primoConto);
				intentHelp.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
				intentHelp.putExtra("tipoFornitura", getIntent().getStringExtra("tipoFornitura"));
				BollettePagamentiRateizzate.this.finish();
				startActivity(intentHelp);
				overridePendingTransition(R.anim.fade, R.anim.hold);
			}
		});*/

		try {
			final JSONArray bollettePagamentiLogin = new JSONArray(Constants.SERVIZIO_LOGIN);
			JSONObject esito = bollettePagamentiLogin.getJSONObject(0);
			codiceEsito = esito.getString("esito");
			if (codiceEsito.equals("200")) {
				JSONObject datiAnagr = bollettePagamentiLogin.getJSONObject(1);
				// ultimo accesso
				String ultimoaccesso = datiAnagr.getString("UltimoAccesso");
				TextView ultimoAccesso = (TextView) findViewById(R.id.ultimo_accesso);
				// ultimoAccesso.setTextSize(11);
				ultimoAccesso.setText(ultimoaccesso);
				// nome cliente
				String nomeClienteString = datiAnagr.getString("NomeCliente");
				String cognomeClienteString = datiAnagr.getString("CognomeCliente");
				TextView nomeCliente = (TextView) findViewById(R.id.nome_cliente);
				nomeInteroCliente = nomeClienteString + " " + cognomeClienteString;
				nomeCliente.setText(nomeClienteString + " " + cognomeClienteString);

				final TextView codiceConto = (TextView) findViewById(R.id.numero_cliente);
				codiceConto.setText(primoConto);

				final JSONArray bolletterateizzabili = new JSONArray(Constants.SERVIZIO_INFO_FATTURE_RATEIZZABILI);
				JSONObject esitoBolletteRateizzabili = bolletterateizzabili.getJSONObject(0);
				String codiceEsitoBolletteRateizzabili = esitoBolletteRateizzabili.getString("esito");

				if (("461").equals(codiceEsitoBolletteRateizzabili)) {

					if(("APPROVATA").equals(getIntent().getStringExtra("STATO_DOMICILIAZIONE"))) {
						
						pager = (LinearLayout) findViewById(R.id.bollette_pagamenti_pager);
						pager.removeAllViews();
						inflater = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);
						inflaterDialog = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);
						
						final Dialog dialog = new Dialog(BollettePagamentiRateizzate.this, android.R.style.Theme_Translucent_NoTitleBar);
						final View det_retail = inflaterDialog.inflate(R.layout.popup_bollette_ratteizzabili_domiciliati, null);
						dialog.setContentView(det_retail);
						dialog.setCancelable(true);
						
						final LinearLayout chiudi = (LinearLayout) dialog.findViewById(R.id.chiudi);
						chiudi.setOnClickListener(new View.OnClickListener() {
							public void onClick(View v) {
								dialog.dismiss();
							}
						});
						dialog.show();

					}
					

					RelativeLayout titolo_lista_bollette = (RelativeLayout) findViewById(R.id.titolo_lista_bollette);
					RelativeLayout stato_domiciliazione_approvata = (RelativeLayout) findViewById(R.id.stato_domiciliazione_approvata);
					RelativeLayout stato_domiciliazione_non_approvata_lista_vuota = (RelativeLayout) findViewById(R.id.stato_domiciliazione_non_approvata_lista_vuota);
					titolo_lista_bollette.setVisibility(View.GONE);
					stato_domiciliazione_approvata.setVisibility(View.GONE);
					stato_domiciliazione_non_approvata_lista_vuota.setVisibility(View.VISIBLE);


				} else {
					pager = (LinearLayout) findViewById(R.id.bollette_pagamenti_pager);
					pager.removeAllViews();
					inflater = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);
					inflaterDialog = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);
					if (!rispostaInfoFattureRateizzabili.equals("")) {
						popolaPaginaContenuto(new JSONArray(rispostaInfoFattureRateizzabili), pager, primoConto);
					}
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

	}

	private void popolaPaginaContenuto(JSONArray rispostaInfoFattureRateizzabiliJSON, LinearLayout pager, String primoConto) {
		try {
			pager.removeAllViews();
			String statoDomiciliazione = rispostaInfoFattureRateizzabiliJSON.getJSONObject(1).getString("statoDomiciliazione");
			RelativeLayout titolo_lista_bollette = (RelativeLayout) findViewById(R.id.titolo_lista_bollette);
			RelativeLayout stato_domiciliazione_approvata = (RelativeLayout) findViewById(R.id.stato_domiciliazione_approvata);
			RelativeLayout stato_domiciliazione_non_approvata_lista_vuota = (RelativeLayout) findViewById(R.id.stato_domiciliazione_non_approvata_lista_vuota);

			final JSONArray listaFattureRateizzabili = rispostaInfoFattureRateizzabiliJSON.getJSONObject(2).getJSONArray("fattureRateizzabili");
			if (statoDomiciliazione.equals("APPROVATA")) {
				//TODO domiciliati
				domiciliazioneApprovata = true;
				if(!paginaDomiciliazioneApprovataInfoVisualizzata){
					final Dialog dialog = new Dialog(BollettePagamentiRateizzate.this, android.R.style.Theme_Translucent_NoTitleBar);
					final View det_retail = inflaterDialog.inflate(R.layout.popup_bollette_ratteizzabili_domiciliati, null);
					dialog.setContentView(det_retail);
					dialog.setCancelable(true);

					final LinearLayout chiudi = (LinearLayout) dialog.findViewById(R.id.chiudi);
					chiudi.setOnClickListener(new View.OnClickListener() {
						public void onClick(View v) {
							dialog.dismiss();
							if (listaFattureRateizzabili.length() == 0) {
								//TODO vai indietro alla pagina bollette e pagamenti
								animation = Utilities.animation(BollettePagamentiRateizzate.this);
								imageAnim = (ImageView) findViewById(R.id.waiting_anim);
								imageAnim.setVisibility(View.VISIBLE);
								if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
									imageAnim.setBackgroundDrawable(animation);
								} else {
									imageAnim.setBackground(animation);
								}
								animation.start();

								Intent intentBollettePagamenti = new Intent(getApplicationContext(), BollettePagamenti.class);
								intentBollettePagamenti.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
								intentBollettePagamenti.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
								BollettePagamentiRateizzate.this.finish();
								startActivity(intentBollettePagamenti);
								overridePendingTransition(R.anim.fade, R.anim.hold);
							}
						}
					});
					dialog.show();
					paginaDomiciliazioneApprovataInfoVisualizzata = true;
				}

			}

			titolo_lista_bollette.setVisibility(View.GONE);
			stato_domiciliazione_approvata.setVisibility(View.GONE);

			//JSONArray listaFattureRateizzabili = rispostaInfoFattureRateizzabiliJSON.getJSONObject(2).getJSONArray("fattureRateizzabili");
			if (listaFattureRateizzabili.length() == 0) {
				stato_domiciliazione_non_approvata_lista_vuota.setVisibility(View.VISIBLE);
			} else if (listaFattureRateizzabili.length() > 0) {
				titolo_lista_bollette.setVisibility(View.VISIBLE);
				stato_domiciliazione_approvata.setVisibility(View.GONE);
				stato_domiciliazione_non_approvata_lista_vuota.setVisibility(View.GONE);

				final ImageView icona_info = (ImageView) findViewById(R.id.icona_info);
				icona_info.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
				icona_info.getBackground().invalidateSelf();
				icona_info.setOnClickListener(new View.OnClickListener() {
					public void onClick(View v) {
						icona_info.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
						icona_info.getBackground().invalidateSelf();

						final Dialog dialog = new Dialog(BollettePagamentiRateizzate.this, android.R.style.Theme_Translucent_NoTitleBar);
						final View det_retail = inflaterDialog.inflate(R.layout.popup_bollette_rateizzabili_info, null);
						dialog.setContentView(det_retail);
						dialog.setCancelable(true);

						final LinearLayout chiudi = (LinearLayout) dialog.findViewById(R.id.chiudi);
						chiudi.setOnClickListener(new View.OnClickListener() {
							public void onClick(View v) {
								icona_info.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
								icona_info.getBackground().invalidateSelf();
								dialog.dismiss();
							}
						});
						dialog.show();
					}
				});

				// costruire la lista delle rate
				int aggiunte = 0;
				for (int i = 0; i < listaFattureRateizzabili.length(); i++) {
					final JSONObject fatturaRateizzabileIesima = listaFattureRateizzabili.getJSONObject(i);
					String causalePrincipale = fatturaRateizzabileIesima.getString("codiceCausaleRateizzabilita");
					String causaleAlternativa = fatturaRateizzabileIesima.getString("codiceCausaleRateizzabilitaAlternativa");

					View bollette_pagamenti_rate_ricerca_entry = inflater.inflate(R.layout.bollette_pagamenti_rate_ricerca_entry, null);
					// i vari casi
					if (causalePrincipale.equals("IN_CORSO") || causaleAlternativa.equals("IN_CORSO")) {
						// scenario 1. rateizzazione in corso: freccia grigia, elemento non clickabile, label rateizzazione in corso
						RelativeLayout bolletta = (RelativeLayout) bollette_pagamenti_rate_ricerca_entry.findViewById(R.id.bolletta);
						if (aggiunte % 2 == 0) {
							bolletta.setBackgroundResource(R.drawable.info_bolletta_rate_black);
						} else {
							bolletta.setBackgroundResource(R.drawable.info_bolletta_rate_white);
						}
						popolaElementoLista(bollette_pagamenti_rate_ricerca_entry, fatturaRateizzabileIesima, true);

						pager.addView(bollette_pagamenti_rate_ricerca_entry);
						aggiunte++;
					} else if ((causalePrincipale.equals("01") && !causaleAlternativa.equals("02")) 
							|| (causaleAlternativa.equals("01") && !causalePrincipale.equals("02"))
							|| (causalePrincipale.equals("02") && !causaleAlternativa.equals("01"))
							|| (causaleAlternativa.equals("02") && !causalePrincipale.equals("01"))) {
						// scenario 2. Rateizzabile solo per delibera o solo per ritardo di fatturazione
						RelativeLayout bolletta = (RelativeLayout) bollette_pagamenti_rate_ricerca_entry.findViewById(R.id.bolletta);
						if (aggiunte % 2 == 0) {
							bolletta.setBackgroundResource(R.drawable.info_bolletta_rate_black);
						} else {
							bolletta.setBackgroundResource(R.drawable.info_bolletta_rate_white);
						}
						popolaElementoLista(bollette_pagamenti_rate_ricerca_entry, fatturaRateizzabileIesima, false);
						// caso di singola opzione di che tipo? delibera 229 : 01 oppure ritardo fatturazione: 02
						if (causalePrincipale.equals("01") || causaleAlternativa.equals("01")) {
							// delibera 229
							bolletta.setOnClickListener(new View.OnClickListener() {
								public void onClick(View v) {
									chiamataCalcolaProposta(fatturaRateizzabileIesima, Constants.PIANO_RATEIZZAZIONE_SINGOLO);
								}
							});
						} else if (causalePrincipale.equals("02") || causaleAlternativa.equals("02")) {
							// ritardo fatturazione
							bolletta.setOnClickListener(new View.OnClickListener() {
								public void onClick(View v) {
									chiamataCalcolaProposta(fatturaRateizzabileIesima, Constants.PIANO_RATEIZZAZIONE_SINGOLO);
								}
							});
						}
						pager.addView(bollette_pagamenti_rate_ricerca_entry);
						aggiunte++;
					} else if ((causalePrincipale.equals("01") && causaleAlternativa.equals("02"))
							|| (causalePrincipale.equals("02") && causaleAlternativa.equals("01"))) {
						// scenario 3. Rateizzabile sia per delibera che per ritardo di fatturazione
						RelativeLayout bolletta = (RelativeLayout) bollette_pagamenti_rate_ricerca_entry.findViewById(R.id.bolletta);
						if (aggiunte % 2 == 0) {
							bolletta.setBackgroundResource(R.drawable.info_bolletta_rate_black);
						} else {
							bolletta.setBackgroundResource(R.drawable.info_bolletta_rate_white);
						}
						popolaElementoLista(bollette_pagamenti_rate_ricerca_entry, fatturaRateizzabileIesima, false);

						// caso di doppia opzione
						bolletta.setOnClickListener(new View.OnClickListener() {
							public void onClick(View v) {
								chiamataCalcolaProposta(fatturaRateizzabileIesima, Constants.PIANO_RATEIZZAZIONE_DOPPIO);
							}
						});
						pager.addView(bollette_pagamenti_rate_ricerca_entry);
						aggiunte++;
					} else {
						// scenario 4. dati sporchi: l'elemento non va aggiunto

					}
				}
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}

	}

	private void popolaElementoLista(View view, JSONObject fatturaRateizzabileIesima, boolean inCorso) {
		try {
			TextView bolletta_n_valore = (TextView) view.findViewById(R.id.bolletta_n_valore);
			bolletta_n_valore.setText(fatturaRateizzabileIesima.getString("codiceFattura"));

			TextView bolletta_del_data = (TextView) view.findViewById(R.id.bolletta_del_data);
			bolletta_del_data.setText(convertiData(fatturaRateizzabileIesima.getString("dataEmissione")));

			TextView bolletta_scadenza_data = (TextView) view.findViewById(R.id.bolletta_scadenza_data);
			bolletta_scadenza_data.setText(convertiData(fatturaRateizzabileIesima.getString("dataScadenza")));

			TextView bolletta_da_pagare_valore = (TextView) view.findViewById(R.id.bolletta_da_pagare_valore);
			String importoRateizzabile = fatturaRateizzabileIesima.getString("importoRateizzabile")	+ " " + getResources().getString(R.string.euro);
			bolletta_da_pagare_valore.setText(importoRateizzabile);

			ImageView bollette_pagamenti_freccia_icon = (ImageView) view.findViewById(R.id.bollette_pagamenti_freccia_icon);
			TextView bolletta_n_stato = (TextView) view.findViewById(R.id.bolletta_n_stato);
			if (inCorso) {
				bolletta_n_stato.setVisibility(View.VISIBLE);
				bollette_pagamenti_freccia_icon.setBackgroundResource(R.drawable.rata_arrow_grey);
			} else {
				bolletta_n_stato.setVisibility(View.GONE);
				bollette_pagamenti_freccia_icon.setBackgroundResource(R.drawable.rata_arrow_black);
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	private void chiamataCalcolaProposta(JSONObject fatturaIesima, int tipoPiano) {

		animation = Utilities.animation(this);
		imageAnim = (ImageView) findViewById(R.id.waiting_anim);
		imageAnim.setVisibility(View.VISIBLE);
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
			imageAnim.setBackgroundDrawable(animation);
		} else {
			imageAnim.setBackground(animation);
		}
		animation.start();

		try {
			String codiceFattura = fatturaIesima.getString("codiceFattura");
			String dataEmissione = fatturaIesima.getString("dataEmissione");
			String dataScadenza = fatturaIesima.getString("dataScadenza");
			String importoRateizzabile = fatturaIesima.getString("importoRateizzabile");

			String codiceCliente = getCodiceCliente(rispostaLogin);
			String identificativoFattura = fatturaIesima.getString("identificativoFattura");
			String codiceCausaleRateizzabilita = fatturaIesima.getString("codiceCausaleRateizzabilita");

			HashMap<String, String> parametri = new HashMap<String, String>();
			parametri.put("codiceCliente", codiceCliente);
			parametri.put("identificativoFattura", identificativoFattura);
			parametri.put("codiceCausaleRateizzabilita", codiceCausaleRateizzabilita);
			parametri.put("codiceFattura", codiceFattura);

			HashMap<String, String> parametriSecondario = new HashMap<String, String>();
			parametriSecondario.put("codiceFattura", codiceFattura);
			parametriSecondario.put("dataEmissione", dataEmissione);
			parametriSecondario.put("dataScadenza", dataScadenza);
			parametriSecondario.put("importoRateizzabile", importoRateizzabile);

			String codiceCausaleRateizzabilitaAlternativa = "";
			if (tipoPiano == 2) {
				codiceCausaleRateizzabilitaAlternativa = fatturaIesima.getString("codiceCausaleRateizzabilitaAlternativa");
				parametri.put("codiceCausaleRateizzabilitaAlternativa", codiceCausaleRateizzabilitaAlternativa);
			}
			if (!isNetworkAvailable(context)) {
				Intent intentOops = new Intent(getApplicationContext(), OopsPageBollettePagamentiRate.class);
				intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.connessione_assente));
				intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
				intentOops.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
				intentOops.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
				intentOops.putExtra("tipoFornitura", getIntent().getStringExtra("tipoFornitura"));

				intentOops.putExtra("identificativoFattura", identificativoFattura);
				intentOops.putExtra("codiceCausaleRateizzabilita", codiceCausaleRateizzabilita);
				intentOops.putExtra("codiceFattura", codiceFattura);
				intentOops.putExtra("tipoPiano", tipoPiano);

				intentOops.putExtra("dataEmissione", dataEmissione);
				intentOops.putExtra("dataScadenza", dataScadenza);
				intentOops.putExtra("importoRateizzabile", importoRateizzabile);
				if (tipoPiano == 2) {
					intentOops.putExtra("codiceCausaleRateizzabilitaAlternativa", codiceCausaleRateizzabilitaAlternativa);
				}
				startActivity(intentOops);
				if (animation != null) {
					animation.stop();
					imageAnim.setVisibility(View.INVISIBLE);
				}
				overridePendingTransition(R.anim.fade, R.anim.hold);
			} else {
				/********************* chiamata calcola proposta ******************************/
				ServiceCall stAvFornServiceCall = ServiceCallFactory.createServiceCall(context);

				try {
					stAvFornServiceCall.executeHttpsGetBollettePagamentiCalcolaPropostaRate(Constants.URL_CALCOLA_PROPOSTA_RATE, parametri, parametriSecondario, BollettePagamentiRateizzate.this, null, tipoPiano);
				} catch (Exception e) {
					Intent intentOops = new Intent(getApplicationContext(), OopsPageBollettePagamentiRate.class);
					intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
					intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
					intentOops.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
					intentOops.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
					intentOops.putExtra("tipoFornitura", getIntent().getStringExtra("tipoFornitura"));

					intentOops.putExtra("identificativoFattura", identificativoFattura);
					intentOops.putExtra("codiceCausaleRateizzabilita", codiceCausaleRateizzabilita);
					intentOops.putExtra("codiceFattura", codiceFattura);
					intentOops.putExtra("tipoPiano", tipoPiano);

					intentOops.putExtra("dataEmissione", dataEmissione);
					intentOops.putExtra("dataScadenza", dataScadenza);
					intentOops.putExtra("importoRateizzabile", importoRateizzabile);

					if (tipoPiano == 2) {
						intentOops.putExtra("codiceCausaleRateizzabilitaAlternativa", codiceCausaleRateizzabilitaAlternativa);
					}
					// intentOops.putExtra("CODICE_CONTO_CLIENTE", codiceContoCliente);
					startActivity(intentOops);
					overridePendingTransition(R.anim.fade, R.anim.hold);
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public String getCodiceCliente(String rispostaLogin) {
		String result = "";
		try {
			JSONArray loginJson = new JSONArray(rispostaLogin);
			JSONObject datiCliente = (JSONObject) loginJson.get(1);
			result = datiCliente.getString("CodiceCliente");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return result;
	}

	public int numeroCodiciContoDiversiAttiviCompletieNonAttivi(JSONArray array) {
		int result = 0;
		String codiceContoConfrontato = "";
		String codiceContoIesimo = "";
		String codiciConcatenati = "";
		for (int i = 2; i < array.length(); i++) {
			JSONArray contoIesimo;
			try {
				contoIesimo = array.getJSONArray(i);
				// codice conto
				JSONObject anagrContoIesimo = contoIesimo.getJSONObject(0);
				codiceContoIesimo = anagrContoIesimo.getString("CodiceConto");

				if (!codiceContoIesimo.equals(codiceContoConfrontato)) {
					String stato = anagrContoIesimo.getString("StatoConto");
					if (!stato.equals("UTILIZZABILE")) {
						// prendo anche i conti non utilizzabili
						if (!codiciConcatenati.contains("-" + codiceContoIesimo)) {
							// codice conto diverso
							codiceContoConfrontato = codiceContoIesimo;
							codiciConcatenati = codiciConcatenati + "-" + codiceContoIesimo;
							result++;
						}
					} else {
						// codice conto utilizzabile diverso, controllo non sia
						// vuoto
						if (contoIesimo.length() > 1) {
							if (!codiciConcatenati.contains("-" + codiceContoIesimo)) {
								// codice conto diverso
								codiceContoConfrontato = codiceContoIesimo;
								codiciConcatenati = codiciConcatenati + "-" + codiceContoIesimo;
								result++;
							}
						} else {
							continue;
						}
					}
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		return result;
	}

	public String[] arrayCodiciContoDiversiUtilizabiliInteriENonUtilizzabili(JSONArray array, int lunghezza) {
		String result[] = new String[lunghezza];
		String codiceContoConfrontato = "";
		String codiceContoIesimo = "";
		String codiciConcatenati = "";
		int count = 0;
		for (int i = 2; i < array.length(); i++) {
			JSONArray contoIesimo;
			try {
				contoIesimo = array.getJSONArray(i);
				// codice conto
				JSONObject anagrContoIesimo = contoIesimo.getJSONObject(0);
				codiceContoIesimo = anagrContoIesimo.getString("CodiceConto");

				if (!codiceContoIesimo.equals(codiceContoConfrontato)) {
					String stato = anagrContoIesimo.getString("StatoConto");
					if (!stato.equals("UTILIZZABILE")) {
						// il conto e' non utilizzabile, prendo anche questi che non sia gia' presente
						boolean isPresente = codiciConcatenati.contains("-" + codiceContoIesimo + "-");
						if (isPresente) {
							continue;
						} else {
							codiceContoConfrontato = codiceContoIesimo;
							result[count] = "  " + codiceContoConfrontato;
							codiciConcatenati = codiciConcatenati + "-" + codiceContoIesimo + "-";
							count++;
						}
					} else {
						// codice conto utilizzabile diverso, controllo non sia vuoto
						if (contoIesimo.length() > 1) {
							// che non sia gia' presente
							boolean isPresente = codiciConcatenati.contains("-" + codiceContoIesimo + "-");
							if (isPresente) {
								continue;
							} else {
								codiceContoConfrontato = codiceContoIesimo;
								result[count] = "  " + codiceContoConfrontato;
								codiciConcatenati = codiciConcatenati + "-" + codiceContoIesimo + "-";
								count++;
							}
						} else {
							continue;
						}
					}
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		return result;
	}

	public void postChiamata(String rispostaChiamata, List<Cookie> lista, HashMap<String, String> parametri, HashMap<String, String> parametriSecondario, int tipoPiano) {
		if (animation != null) {
			animation.stop();
			imageAnim.setVisibility(View.INVISIBLE);
		}
		JSONArray rispostaJSON = new JSONArray();
		String esito = "";
		String descEsito = "";
		try {

			rispostaJSON = new JSONArray(rispostaChiamata);
			JSONObject oggettoRisposta = (JSONObject) rispostaJSON.get(0);
			esito = oggettoRisposta.getString("esito");
			descEsito = oggettoRisposta.getString("descEsito");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		if (esito.equals("200")) {
			Constants.SERVIZIO_CALCOLA_PROPOSTA_RATE = rispostaChiamata;
			switch (tipoPiano) {
			case Constants.PIANO_RATEIZZAZIONE_SINGOLO:
				// portare alla pagina del dettaglio
				Intent intentDettaglioPiano = new Intent(getApplicationContext(), BollettePagamentiDettaglioPiano.class);
				intentDettaglioPiano.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
				intentDettaglioPiano.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
				intentDettaglioPiano.putExtra("tipoFornitura", getIntent().getStringExtra("tipoFornitura"));
				intentDettaglioPiano.putExtra("tipoPiano", Constants.PIANO_RATEIZZAZIONE_SINGOLO);
				intentDettaglioPiano.putExtra("identificativoFattura", parametri.get("identificativoFattura"));

				intentDettaglioPiano.putExtra("codiceFattura", parametriSecondario.get("codiceFattura"));
				intentDettaglioPiano.putExtra("dataEmissione", parametriSecondario.get("dataEmissione"));
				intentDettaglioPiano.putExtra("dataScadenza", parametriSecondario.get("dataScadenza"));
				intentDettaglioPiano.putExtra("importoRateizzabile", parametriSecondario.get("importoRateizzabile"));
				intentDettaglioPiano.putExtra("codiceCausaleRateizzabilita", parametri.get("codiceCausaleRateizzabilita"));

				intentDettaglioPiano.putExtra("domiciliazioneApprovata", domiciliazioneApprovata);
				intentDettaglioPiano.putExtra("infoDomiciliazioneApprovata", paginaDomiciliazioneApprovataInfoVisualizzata);

				BollettePagamentiRateizzate.this.finish();
				startActivity(intentDettaglioPiano);
				overridePendingTransition(R.anim.fade, R.anim.hold);
				break;
			case Constants.PIANO_RATEIZZAZIONE_DOPPIO:
				// portare alla pagina della scelta del piano
				Intent intentSceltaPiano = new Intent(getApplicationContext(), BollettePagamentiSceltaPiano.class);
				intentSceltaPiano.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
				intentSceltaPiano.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
				intentSceltaPiano.putExtra("tipoFornitura", getIntent().getStringExtra("tipoFornitura"));
				intentSceltaPiano.putExtra("tipoPiano", Constants.PIANO_RATEIZZAZIONE_DOPPIO);
				intentSceltaPiano.putExtra("identificativoFattura", parametri.get("identificativoFattura"));
				intentSceltaPiano.putExtra("codiceCausaleRateizzabilitaAlternativa", parametri.get("codiceCausaleRateizzabilitaAlternativa"));

				intentSceltaPiano.putExtra("codiceFattura", parametriSecondario.get("codiceFattura"));
				intentSceltaPiano.putExtra("dataEmissione", parametriSecondario.get("dataEmissione"));
				intentSceltaPiano.putExtra("dataScadenza", parametriSecondario.get("dataScadenza"));
				intentSceltaPiano.putExtra("importoRateizzabile", parametriSecondario.get("importoRateizzabile"));
				intentSceltaPiano.putExtra("codiceCausaleRateizzabilita", parametri.get("codiceCausaleRateizzabilita"));

				intentSceltaPiano.putExtra("domiciliazioneApprovata", domiciliazioneApprovata);
				intentSceltaPiano.putExtra("infoDomiciliazioneApprovata", paginaDomiciliazioneApprovataInfoVisualizzata);

				BollettePagamentiRateizzate.this.finish();
				startActivity(intentSceltaPiano);
				overridePendingTransition(R.anim.fade, R.anim.hold);
				break;
			}
		} else {
			Intent intentOops = new Intent();
			intentOops = new Intent(getApplicationContext(), OopsPageBollettePagamentiRate.class);
			intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
			intentOops.putExtra("CODICE_CLIENTE", parametri.get("codiceCliente"));
			intentOops.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
			intentOops.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
			intentOops.putExtra("tipoFornitura", getIntent().getStringExtra("tipoFornitura"));

			intentOops.putExtra("identificativoFattura", parametri.get("identificativoFattura"));
			intentOops.putExtra("codiceCausaleRateizzabilita", parametri.get("codiceCausaleRateizzabilita"));
			intentOops.putExtra("codiceFattura", parametri.get("codiceFattura"));
			intentOops.putExtra("tipoPiano", tipoPiano);

			if (tipoPiano == 2) {
				intentOops.putExtra("codiceCausaleRateizzabilitaAlternativa", parametri.get("codiceCausaleRateizzabilitaAlternativa"));
			}

			intentOops.putExtra("domiciliazioneApprovata", domiciliazioneApprovata);
			intentOops.putExtra("infoDomiciliazioneApprovata", paginaDomiciliazioneApprovataInfoVisualizzata);

			intentOops.putExtra("dataEmissione", parametriSecondario.get("dataEmissione"));
			intentOops.putExtra("dataScadenza", parametriSecondario.get("dataScadenza"));
			intentOops.putExtra("importoRateizzabile", parametriSecondario.get("importoRateizzabile"));
			// errori ops page
			if (esito.equals("309")) {
				intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
				BollettePagamentiRateizzate.this.finish();
				startActivity(intentOops);
				overridePendingTransition(R.anim.fade, R.anim.hold);

			} else if (esito.equals("400")) {
				intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
				BollettePagamentiRateizzate.this.finish();
				startActivity(intentOops);
				overridePendingTransition(R.anim.fade, R.anim.hold);

			} else if (esito.equals("420")) {
				intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
				BollettePagamentiRateizzate.this.finish();
				startActivity(intentOops);
				overridePendingTransition(R.anim.fade, R.anim.hold);

			} else if (esito.equals("449")) {
				intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
				BollettePagamentiRateizzate.this.finish();
				startActivity(intentOops);
				overridePendingTransition(R.anim.fade, R.anim.hold);

			} else if (esito.equals("451")) {
				intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_popup_451));
				BollettePagamentiRateizzate.this.finish();
				startActivity(intentOops);
				overridePendingTransition(R.anim.fade, R.anim.hold);
			}
			// errori popup
			else if (esito.equals("307")) {
				Utils.popup(BollettePagamentiRateizzate.this, getResources().getString(R.string.errore_popup_307), "Riprova");

			} else if (esito.equals("435")) {
				Utils.popup(BollettePagamentiRateizzate.this, getResources().getString(R.string.errore_popup_435), "Riprova");

			} else if (esito.equals("437")) {
				Utils.popup(BollettePagamentiRateizzate.this, getResources().getString(R.string.errore_popup_437), "Riprova");

			} else if (esito.equals("438")) {
				Utils.popup(BollettePagamentiRateizzate.this, getResources().getString(R.string.errore_popup_438), "Riprova");

			} else if (esito.equals("443")) {
				Utils.popup(BollettePagamentiRateizzate.this, getResources().getString(R.string.errore_popup_443), "Riprova");

			} else if (esito.equals("444")) {
				Utils.popup(BollettePagamentiRateizzate.this, getResources().getString(R.string.errore_popup_444), "Riprova");

			} else if (esito.equals("445")) {
				Utils.popup(BollettePagamentiRateizzate.this, getResources().getString(R.string.errore_popup_445), "Riprova");

			} else if (esito.equals("446")) {
				Utils.popup(BollettePagamentiRateizzate.this, getResources().getString(R.string.errore_popup_446), "Riprova");

			} else if (esito.equals("447")) {
				Utils.popup(BollettePagamentiRateizzate.this, getResources().getString(R.string.errore_popup_447), "Riprova");

			} else {
				if (descEsito.equals("")) {
					intentOops = new Intent(getApplicationContext(), OopsPageSessioneScaduta.class);
					intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.sessione_scaduta));
				} else {
					intentOops.putExtra("MESSAGGIO", descEsito);
				}
			}
			startActivity(intentOops);
			overridePendingTransition(R.anim.fade, R.anim.hold);
		}

	}

	public void postChiamataException(boolean exception, HashMap<String, String> parameterMap, HashMap<String, String> parametriSecondario, int tipoPiano) {
		if (exception) {
			if (animation != null) {
				animation.stop();
				imageAnim.setVisibility(View.INVISIBLE);
			}

			Intent intentOops = new Intent(getApplicationContext(), OopsPageBollettePagamentiRate.class);
			intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
			intentOops.putExtra("CODICE_CLIENTE", parameterMap.get("codiceCliente"));
			intentOops.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
			intentOops.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
			intentOops.putExtra("tipoFornitura", getIntent().getStringExtra("tipoFornitura"));

			intentOops.putExtra("identificativoFattura", parameterMap.get("identificativoFattura"));
			intentOops.putExtra("codiceCausaleRateizzabilita", parameterMap.get("codiceCausaleRateizzabilita"));
			intentOops.putExtra("codiceFattura", parameterMap.get("codiceFattura"));
			intentOops.putExtra("tipoPiano", tipoPiano);

			intentOops.putExtra("dataEmissione", parametriSecondario.get("dataEmissione"));
			intentOops.putExtra("dataScadenza", parametriSecondario.get("dataScadenza"));
			intentOops.putExtra("importoRateizzabile", parametriSecondario.get("importoRateizzabile"));
			if (tipoPiano == 2) {
				intentOops.putExtra("codiceCausaleRateizzabilitaAlternativa", parameterMap.get("codiceCausaleRateizzabilitaAlternativa"));
			}
			startActivity(intentOops);
			overridePendingTransition(R.anim.fade, R.anim.hold);
		}
	}

	private boolean isNetworkAvailable(Context context) {
		ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivityManager == null)
			return false;
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		return activeNetworkInfo != null;
	}

	public void onBackPressed() {

		animation = Utilities.animation(this);
		imageAnim = (ImageView) findViewById(R.id.waiting_anim);
		imageAnim.setVisibility(View.VISIBLE);
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
			imageAnim.setBackgroundDrawable(animation);
		} else {
			imageAnim.setBackground(animation);
		}
		animation.start();

		Intent intentBollettePagamenti = new Intent(getApplicationContext(), BollettePagamenti.class);
		intentBollettePagamenti.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
		intentBollettePagamenti.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
		BollettePagamentiRateizzate.this.finish();
		startActivity(intentBollettePagamenti);
		overridePendingTransition(R.anim.fade, R.anim.hold);
	}

	private String convertiData(String data) {
		String result;
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date dataEmis = new Date();
		try {
			dataEmis = sdf.parse(data);
		} catch (ParseException e) {
			// e.printStackTrace();
		}
		SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MM/yyyy");
		result = sdf1.format(dataEmis);
		return result;
	}

}
