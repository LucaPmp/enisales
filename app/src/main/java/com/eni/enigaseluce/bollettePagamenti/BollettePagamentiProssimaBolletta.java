package com.eni.enigaseluce.bollettePagamenti;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import org.apache.http.cookie.Cookie;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.AnimationDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.provider.CalendarContract.Events;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.eni.enigaseluce.R;
import com.eni.enigaseluce.autolettura.AutoLetturaInsert;
import com.eni.enigaseluce.autolettura.Autolettura;
import com.eni.enigaseluce.error.OopsPageProssimaBollettaAutolettura;
import com.eni.enigaseluce.error.OopsPageSessioneScaduta;
import com.eni.enigaseluce.error.Utils;
import com.eni.enigaseluce.font.EniFont;
import com.eni.enigaseluce.httpcall.Constants;
import com.eni.enigaseluce.httpcall.Constants.TIPOLOGIA_COMMODITY;
import com.eni.enigaseluce.httpcall.ServiceCall;
import com.eni.enigaseluce.httpcall.ServiceCallFactory;
import com.eni.enigaseluce.utils.Log;
import com.eni.enigaseluce.utils.Utilities;

@SuppressLint("NewApi")
public class BollettePagamentiProssimaBolletta extends Activity {
	private Context context;
	private static Context staticContext;
	AnimationDrawable animation;
	ImageView imageAnim;

	public String rispostaLogin = Constants.SERVIZIO_LOGIN;
	public String rispostaShowListaFatture = Constants.SERVIZIO_SHOW_LISTA_FATTURE;
	public String rispostaProssimaFattura = Constants.SERVIZIO_INFO_PROSSIMA_FATTURA;

	public static String nomeInteroCliente = "";
	String codiceEsito;
	String primoConto;
	String tipologia;

	String sourceActivity;
	String numeroCifreMisuratore;
	String pdf;
	String pdr;
	String codiceConto;
	String sistemaBilling;
	String societaFornitrice;

	Date dataFineTmp;

	public static Context getContext()
	{
		return BollettePagamentiProssimaBolletta.staticContext;
	}

	// **************************************************
	// variabili per la gestione dell'annota data - start
	// **************************************************
	// private static final int EVENT_TOKEN = 1;
	// private static final int REMINDER_TOKEN = 2;
	// private static final int CALENDAR_TOKEN = 3;
	//
	// // The indices for the projection array above.
	// private static final int PROJECTION_ID_INDEX = 0;
	//
	// // private static final String PREFERENCES = "LOCAL_PREFS";
	// private static final String CALENDAR_CREATED = "CAL_CREATED";
	// private static final String PREFS_CALENDARE_ACCOUNT_NAME =
	// "ACCOUNT_NAME";
	//
	// private Uri eventID;
	// private long calID = 0;
	// private AsyncQueryHandler handlerEvent;
	//
	// private String account_name = "Eni-SelfCare";
	//
	// static Uri asSyncAdapter(Uri uri, String account, String accountType) {
	// return
	// uri.buildUpon().appendQueryParameter(android.provider.CalendarContract.CALLER_IS_SYNCADAPTER,
	// "true").appendQueryParameter(Calendars.ACCOUNT_NAME,
	// account).appendQueryParameter(Calendars.ACCOUNT_TYPE,
	// accountType).build();
	// }

	// ************************************************
	// variabili per la gestione dell'annota data - end
	// ************************************************

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.bollette_pagamenti_prossima_bolletta);

		context = this;
		BollettePagamentiProssimaBolletta.staticContext = this;

		primoConto = getIntent().getStringExtra("PRIMOCONTO");
		tipologia = getIntent().getStringExtra("TIPOLOGIA");

		sourceActivity = getIntent().getStringExtra("SOURCE_ACTIVITY");
		numeroCifreMisuratore = getIntent().getStringExtra("NUMCIFREMISURATORE");
		pdf = getIntent().getStringExtra("PDF");
		pdr = getIntent().getStringExtra("PDR");
		codiceConto = getIntent().getStringExtra("CURRENT_CODICE_CONTO");
		sistemaBilling = getIntent().getStringExtra("SISTEMABILLING");
		societaFornitrice = getIntent().getStringExtra("SOCIETAFORNITRICE");

		Log.d("BOLLETTEPAGAMENTIPROXBOLL", "SOURCE_ACTIVITY : " + sourceActivity);

		// creazione del calendario locale in cui salvare gli eventi per la
		// comunicazione dell'autolettura

		// final SharedPreferences prefs =
		// getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
		//
		// final Editor edit = prefs.edit();
		//
		// edit.putString(PREFS_CALENDARE_ACCOUNT_NAME, account_name);
		// if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
		// edit.apply();
		// }
		// else {
		// new Thread(new Runnable() {
		//
		// @Override
		// public void run() {
		//
		// edit.commit();
		// }
		// }).start();
		// }

		// AsyncQueryHandler handlerCalendar = new
		// AsyncQueryHandler(getContentResolver()) {
		//
		// @Override
		// protected void onInsertComplete(int token, Object cookie, Uri uri) {
		//
		// switch (token) {
		// case CALENDAR_TOKEN:
		//
		// calID = Long.parseLong(uri.getLastPathSegment());
		//
		// // Log.d(SAMPLE_DEBUG, "Local Calendar created with ID: " +
		// // calID);
		//
		// // Toast.makeText(MainActivity.this,
		// // "Nuovo Calendario locale creato",
		// // Toast.LENGTH_SHORT).show();
		//
		// final Editor edit = prefs.edit();
		//
		// edit.putBoolean(CALENDAR_CREATED, true);
		//
		// if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD)
		// edit.apply();
		// else {
		// new Thread(new Runnable() {
		//
		// @Override
		// public void run() {
		// edit.commit();
		// }
		// }).start();
		// }
		//
		// break;
		// }
		// }
		//
		// @Override
		// protected void onQueryComplete(int token, Object cookie, Cursor
		// cursor) {
		//
		// switch (token) {
		// case CALENDAR_TOKEN:
		//
		// if (cursor.isBeforeFirst()) {
		// boolean ok = cursor.moveToFirst();
		//
		// if (ok) {
		// calID = cursor.getLong(PROJECTION_ID_INDEX);
		// }
		// else {
		//
		// // Log.e(SAMPLE_DEBUG,
		// // "Errore nella lettura dei dai del Calendario");
		// }
		// }
		//
		// break;
		// }
		// }
		// };

		// if (!prefs.getBoolean(CALENDAR_CREATED, false)) {
		//
		// Uri calendarUri = asSyncAdapter(Calendars.CONTENT_URI, account_name,
		// CalendarContract.ACCOUNT_TYPE_LOCAL);
		//
		// ContentValues valuesCalendar = new ContentValues();
		//
		// valuesCalendar.put(Calendars.ACCOUNT_NAME, account_name);
		// valuesCalendar.put(Calendars.ACCOUNT_TYPE,
		// CalendarContract.ACCOUNT_TYPE_LOCAL);
		// valuesCalendar.put(CalendarContract.Calendars.NAME, "Eni");
		// valuesCalendar.put(CalendarContract.Calendars.CALENDAR_DISPLAY_NAME,
		// "Eni");
		// valuesCalendar.put(CalendarContract.Calendars.SYNC_EVENTS, 1);
		// valuesCalendar.put(CalendarContract.Calendars.VISIBLE, 1);
		// valuesCalendar.put(CalendarContract.Calendars.CALENDAR_COLOR,
		// Color.rgb(254, 208, 9));
		// valuesCalendar.put(CalendarContract.Calendars.CALENDAR_ACCESS_LEVEL,
		// CalendarContract.Calendars.CAL_ACCESS_OWNER);
		// valuesCalendar.put(CalendarContract.Calendars.OWNER_ACCOUNT,
		// account_name);
		// valuesCalendar.put(CalendarContract.Calendars.CALENDAR_TIME_ZONE,
		// "Europe/Rome");
		// valuesCalendar.put(CalendarContract.Calendars.ALLOWED_REMINDERS,
		// Reminders.METHOD_DEFAULT + "," + Reminders.METHOD_ALARM + "," +
		// Reminders.METHOD_ALERT + "," + Reminders.METHOD_EMAIL + "," +
		// Reminders.METHOD_SMS);
		//
		// handlerCalendar.startInsert(CALENDAR_TOKEN, null, calendarUri,
		// valuesCalendar);
		// }
		// else {
		//
		// String[] EVENT_PROJECTION = new String[] { Calendars._ID,
		// Calendars.CALENDAR_DISPLAY_NAME, Calendars.OWNER_ACCOUNT };
		//
		// Uri uri = Calendars.CONTENT_URI;
		// String selection = Calendars.ACCOUNT_TYPE + " = ? AND " +
		// Calendars.OWNER_ACCOUNT + " = ?";
		// String[] selectionArgs = new String[] {
		// CalendarContract.ACCOUNT_TYPE_LOCAL,
		// prefs.getString(PREFS_CALENDARE_ACCOUNT_NAME, "") };
		//
		// handlerCalendar.startQuery(CALENDAR_TOKEN, null, uri,
		// EVENT_PROJECTION, selection, selectionArgs, null);
		// }
	}

	@Override
	protected void onStart()
	{
		super.onStart();
		tipologia = getIntent().getStringExtra("TIPOLOGIA");

		Log.d("BOLLETTEPAGAMENTIPROXBOLL", this.getClass().getName() + " - tipologia: " + tipologia);

		final ImageView indietro = (ImageView) findViewById(R.id.bollette_pagamenti_indietro);
		indietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
		indietro.getBackground().invalidateSelf();
		indietro.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v)
			{

				indietro.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
				indietro.getBackground().invalidateSelf();

				onBackPressed();
			}
		});
		/*
		 * final ImageView help =
		 * (ImageView)findViewById(R.id.bollette_pagamenti_help);
		 * help.getBackground().setColorFilter(0xFFFFFFFF,
		 * PorterDuff.Mode.MULTIPLY); help.getBackground().invalidateSelf();
		 * help.setOnClickListener(new View.OnClickListener() { public void
		 * onClick(View v) { help.getBackground().setColorFilter(0xFF999999,
		 * PorterDuff.Mode.MULTIPLY); help.getBackground().invalidateSelf();
		 * animation = new AnimationDrawable();
		 * animation.addFrame(getResources().getDrawable(R.drawable.anim_load2),
		 * 300);
		 * animation.addFrame(getResources().getDrawable(R.drawable.anim_load3),
		 * 300);
		 * animation.addFrame(getResources().getDrawable(R.drawable.anim_load4),
		 * 300); animation.setOneShot(false); imageAnim = (ImageView)
		 * findViewById(R.id.waiting_bollette_pagamenti_prossima_bolletta);
		 * imageAnim.setVisibility(View.VISIBLE); if(Build.VERSION.SDK_INT <
		 * 16){ imageAnim.setBackgroundDrawable(animation); }else{
		 * imageAnim.setBackground(animation); } // start the animation!
		 * animation.start();
		 * 
		 * Intent intentHelp = new Intent(getApplicationContext(),
		 * BollettePagamentiHelp.class); intentHelp.putExtra("ORIGINE",
		 * Constants.ORIGINE_BOLLETTE_PAGAMENTI_PROSSIMA_BOLLETTA);
		 * intentHelp.putExtra("TIPOLOGIA", tipologia);
		 * intentHelp.putExtra("PRIMOCONTO", primoConto);
		 * intentHelp.putExtra("INDICE_SPINNER",
		 * getIntent().getIntExtra("INDICE_SPINNER",0));
		 * BollettePagamentiProssimaBolletta.this.finish();
		 * startActivity(intentHelp); overridePendingTransition(R.anim.fade,
		 * R.anim.hold);
		 * 
		 * } });
		 */
		try
		{
			final JSONArray bollettePagamentiLogin = new JSONArray(rispostaLogin);
			JSONObject esito = bollettePagamentiLogin.getJSONObject(0);
			codiceEsito = (String) esito.getString("esito");
			if (codiceEsito.equals("200"))
			{
				JSONObject datiAnagr = bollettePagamentiLogin.getJSONObject(1);
				// ultimo accesso
				String ultimoaccesso = datiAnagr.getString("UltimoAccesso");
				TextView ultimoAccesso = (TextView) findViewById(R.id.ultimo_accesso);
				// ultimoAccesso.setTextSize(11);
				ultimoAccesso.setText(ultimoaccesso);
				// nome cliente
				String nomeClienteString = datiAnagr.getString("NomeCliente");
				String cognomeClienteString = datiAnagr.getString("CognomeCliente");
				TextView nomeCliente = (TextView) findViewById(R.id.nome_cliente);
				nomeInteroCliente = nomeClienteString + " " + cognomeClienteString;
				nomeCliente.setText(nomeClienteString + " " + cognomeClienteString);

				final TextView codiceConto = (TextView) findViewById(R.id.numero_cliente);
				codiceConto.setText(primoConto);

				if (!rispostaProssimaFattura.equals(""))
				{
					JSONArray rispostaProssimaFatturaJSON = new JSONArray(rispostaProssimaFattura);
					final JSONObject infoProssimaFattura = rispostaProssimaFatturaJSON.getJSONObject(1);

					String esitoInfo = infoProssimaFattura.getString("esitoInfo");

					LinearLayout info_dual = (LinearLayout) findViewById(R.id.info_dual);
					LinearLayout info_luce = (LinearLayout) findViewById(R.id.info_luce);
					LinearLayout info_gas = (LinearLayout) findViewById(R.id.info_gas);
					RelativeLayout no_info = (RelativeLayout) findViewById(R.id.no_info);

					if (esitoInfo.equals("true"))
					{
						no_info.setVisibility(View.GONE);

						// nuova gestione delle commodity CR R4 e R5
						if (tipologia != null
								&& (tipologia.equalsIgnoreCase(TIPOLOGIA_COMMODITY.DUAL.name()) || tipologia.equalsIgnoreCase(TIPOLOGIA_COMMODITY.GAS.name())))
						{

							// commodity TIPOLOGIA_COMMODITY.GAS or
							// TIPOLOGIA_COMMODITY.DUAL

							TextView bollette_pagamenti_emissione_prevista_valore = null;
							TextView bollette_pagamenti_periodo_riferimento_valore = null;
							TextView fatturazione_date = null;
							ImageView annota_data = null;
							EniFont autolettura = null;

							if (tipologia.equalsIgnoreCase(TIPOLOGIA_COMMODITY.GAS.name()))
							{

								info_dual.setVisibility(View.GONE);
								info_luce.setVisibility(View.GONE);
								info_gas.setVisibility(View.VISIBLE);
								no_info.setVisibility(View.GONE);

								bollette_pagamenti_emissione_prevista_valore = (TextView) findViewById(R.id.bollette_pagamenti_emissione_prevista_gas_valore);
								bollette_pagamenti_periodo_riferimento_valore = (TextView) findViewById(R.id.bollette_pagamenti_periodo_riferimento_gas_valore);
								annota_data = (ImageView) findViewById(R.id.annota_data_gas);
								fatturazione_date = (TextView) findViewById(R.id.fatturazione_date_gas);

								autolettura = (EniFont) findViewById(R.id.bollette_pagamenti_gas_comunica_autolettura_btn);
							} else
							{

								info_dual.setVisibility(View.VISIBLE);
								info_luce.setVisibility(View.GONE);
								info_gas.setVisibility(View.GONE);
								no_info.setVisibility(View.GONE);

								bollette_pagamenti_emissione_prevista_valore = (TextView) findViewById(R.id.bollette_pagamenti_emissione_prevista_valore);
								bollette_pagamenti_periodo_riferimento_valore = (TextView) findViewById(R.id.bollette_pagamenti_periodo_riferimento_valore);
								annota_data = (ImageView) findViewById(R.id.annota_data_dual);
								fatturazione_date = (TextView) findViewById(R.id.fatturazione_date_dual);

								autolettura = (EniFont) findViewById(R.id.bollette_pagamenti_dual_comunica_autolettura_btn);
							}

							bollette_pagamenti_emissione_prevista_valore.setText(convertiData(infoProssimaFattura.getString("dataEmissioneProssimaFattura")));

							String dataDal = convertiData(infoProssimaFattura.getString("periodoRiferimentoDal"));
							String dataAl = convertiData(infoProssimaFattura.getString("periodoRiferimentoA"));

							bollette_pagamenti_periodo_riferimento_valore.setText("dal " + dataDal + " al " + dataAl);

							dataFineTmp = dataFineDate(infoProssimaFattura.getString("dataEmissioneProssimaFattura"));

							// click sul tasto "Annota la data" introdotto nella
							// CR R4 e R5
							annota_data.setOnClickListener(new OnClickListener() {

								@Override
								public void onClick(View v)
								{

									if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH)
									{

										// implementazione della logica per la
										// creazione di un evento sull'app
										// Calendario

										if (new Date().after(dataFineTmp))
										{
											AlertDialog.Builder builder = new AlertDialog.Builder(context);
											builder.setMessage(R.string.periodo_autolettura_terminato).setCancelable(false)
													.setPositiveButton("Chiudi", new DialogInterface.OnClickListener() {
														public void onClick(DialogInterface dialog, int id)
														{
															dialog.dismiss();
														}
													});
											builder.create().show();
										} else
										{
											addNewEventAutolettura(infoProssimaFattura);
										}
									} else
									{

										// popup reminder non supportato

										AlertDialog.Builder noReminder = new Builder(BollettePagamentiProssimaBolletta.this);
										noReminder.setTitle(R.string.no_reminder_title).setMessage(R.string.no_reminder_message)
												.setPositiveButton(R.string.chiudi, new DialogInterface.OnClickListener() {

													@Override
													public void onClick(DialogInterface dialog, int which)
													{

														dialog.dismiss();
													}
												});

										noReminder.create().show();
									}
								}
							});

							// TextView fatturazione_dal_label =
							// (TextView)findViewById(R.id.fatturazione_gas_dal_label);
							// fatturazione_dal_label.setText(convertiData(dataInizio(infoProssimaFattura.getString("dataEmissioneProssimaFattura"))));
							// TextView fatturazione_al_label =
							// (TextView)findViewById(R.id.fatturazione_gas_al_label);
							// fatturazione_al_label.setText(convertiData(dataFine(infoProssimaFattura.getString("dataEmissioneProssimaFattura"))));

							String dataInizio = convertiData(dataInizio(infoProssimaFattura.getString("dataEmissioneProssimaFattura")));
							String dataFine = convertiData(dataFine(infoProssimaFattura.getString("dataEmissioneProssimaFattura")));

							String fatturazioneDate = "dal <b>" + dataInizio + "</b>" + " al <b>" + dataFine + "</b>";

							fatturazione_date.setText(Html.fromHtml(fatturazioneDate));

							autolettura.setOnClickListener(new View.OnClickListener() {

								public void onClick(View v)
								{
									chiamataLoadFornitureGas();
								}
							});
						} else
						{

							// commodity TIPOLOGIA_COMMODITY.LUCE

							TextView bollette_pagamenti_emissione_prevista_valore = (TextView) findViewById(R.id.bollette_pagamenti_emissione_prevista_luce_valore);
							bollette_pagamenti_emissione_prevista_valore.setText(convertiData(infoProssimaFattura.getString("dataEmissioneProssimaFattura")));

							TextView bollette_pagamenti_periodo_riferimento_valore = (TextView) findViewById(R.id.bollette_pagamenti_periodo_riferimento_luce_valore);
							bollette_pagamenti_periodo_riferimento_valore.setText("dal " + convertiData(infoProssimaFattura.getString("periodoRiferimentoDal"))
									+ " al " + convertiData(infoProssimaFattura.getString("periodoRiferimentoA")));

							info_dual.setVisibility(View.GONE);
							info_luce.setVisibility(View.VISIBLE);
							info_gas.setVisibility(View.GONE);
							no_info.setVisibility(View.GONE);
						}

						// vecchia gestione commodity
						// if(tipologia.equals("LUCE")){
						// TextView bollette_pagamenti_emissione_prevista_valore
						// =
						// (TextView)findViewById(R.id.bollette_pagamenti_emissione_prevista_luce_valore);
						// bollette_pagamenti_emissione_prevista_valore.setText(convertiData(infoProssimaFattura.getString("dataEmissioneProssimaFattura")));
						//
						// TextView
						// bollette_pagamenti_periodo_riferimento_valore =
						// (TextView)findViewById(R.id.bollette_pagamenti_periodo_riferimento_luce_valore);
						// bollette_pagamenti_periodo_riferimento_valore.setText("dal "
						// +
						// convertiData(infoProssimaFattura.getString("periodoRiferimentoDal"))
						// + " al " +
						// convertiData(infoProssimaFattura.getString("periodoRiferimentoA")));
						//
						// info_dual.setVisibility(View.GONE);
						// info_luce.setVisibility(View.VISIBLE);
						// info_gas.setVisibility(View.GONE);
						//
						// }else if(tipologia.equals("GAS")){
						// TextView bollette_pagamenti_emissione_prevista_valore
						// =
						// (TextView)findViewById(R.id.bollette_pagamenti_emissione_prevista_gas_valore);
						// bollette_pagamenti_emissione_prevista_valore.setText(convertiData(infoProssimaFattura.getString("dataEmissioneProssimaFattura")));
						//
						// TextView
						// bollette_pagamenti_periodo_riferimento_valore =
						// (TextView)findViewById(R.id.bollette_pagamenti_periodo_riferimento_gas_valore);
						// bollette_pagamenti_periodo_riferimento_valore.setText("dal "
						// +
						// convertiData(infoProssimaFattura.getString("periodoRiferimentoDal"))
						// + " al " +
						// convertiData(infoProssimaFattura.getString("periodoRiferimentoA")));
						//
						// info_dual.setVisibility(View.GONE);
						// info_luce.setVisibility(View.GONE);
						// info_gas.setVisibility(View.VISIBLE);
						//
						// TextView fatturazione_dal_label =
						// (TextView)findViewById(R.id.fatturazione_gas_dal_label);
						// fatturazione_dal_label.setText(convertiData(dataInizio(infoProssimaFattura.getString("dataEmissioneProssimaFattura"))));
						// TextView fatturazione_al_label =
						// (TextView)findViewById(R.id.fatturazione_gas_al_label);
						// fatturazione_al_label.setText(convertiData(dataFine(infoProssimaFattura.getString("dataEmissioneProssimaFattura"))));
						//
						// ImageView autolettura =
						// (ImageView)findViewById(R.id.bollette_pagamenti_gas_comunica_autolettura_btn);
						// autolettura.setOnClickListener(new
						// View.OnClickListener() {
						// public void onClick(View v) {
						// chiamataLoadFornitureGas();
						// }
						// });
						// }else{
						// //dual
						// info_dual.setVisibility(View.VISIBLE);
						// info_luce.setVisibility(View.GONE);
						// info_gas.setVisibility(View.GONE);
						//
						// TextView bollette_pagamenti_emissione_prevista_valore
						// =
						// (TextView)findViewById(R.id.bollette_pagamenti_emissione_prevista_valore);
						// bollette_pagamenti_emissione_prevista_valore.setText(convertiData(infoProssimaFattura.getString("dataEmissioneProssimaFattura")));
						//
						// TextView
						// bollette_pagamenti_periodo_riferimento_valore =
						// (TextView)findViewById(R.id.bollette_pagamenti_periodo_riferimento_valore);
						// bollette_pagamenti_periodo_riferimento_valore.setText("dal "
						// +
						// convertiData(infoProssimaFattura.getString("periodoRiferimentoDal"))
						// + " al " +
						// convertiData(infoProssimaFattura.getString("periodoRiferimentoA")));
						//
						//
						// TextView fatturazione_dal_label =
						// (TextView)findViewById(R.id.fatturazione_dual_dal_label);
						// fatturazione_dal_label.setText(convertiData(dataInizio(infoProssimaFattura.getString("dataEmissioneProssimaFattura"))));
						// TextView fatturazione_al_label =
						// (TextView)findViewById(R.id.fatturazione_dual_al_label);
						// fatturazione_al_label.setText(convertiData(dataFine(infoProssimaFattura.getString("dataEmissioneProssimaFattura"))));
						//
						// ImageView autolettura =
						// (ImageView)findViewById(R.id.bollette_pagamenti_dual_comunica_autolettura_btn);
						// autolettura.setOnClickListener(new
						// View.OnClickListener() {
						// public void onClick(View v) {
						// chiamataLoadFornitureGas();
						// }
						// });
						// }
					} else if (esitoInfo.equals("false"))
					{
						info_dual.setVisibility(View.GONE);
						info_luce.setVisibility(View.GONE);
						info_gas.setVisibility(View.GONE);
						no_info.setVisibility(View.VISIBLE);
						TextView esitoInfoLabel = (TextView)findViewById(R.id.bollette_pagamenti_no_info_label);
						if (tipologia != null
								&& (tipologia.equalsIgnoreCase(TIPOLOGIA_COMMODITY.DUAL.name()) || tipologia.equalsIgnoreCase(TIPOLOGIA_COMMODITY.GAS.name())))
						{
							esitoInfoLabel.setText(R.string.bollette_pagamenti_prossima_bolletta_no_info_autolettura);
						} else{
							esitoInfoLabel.setText(R.string.bollette_pagamenti_prossima_bolletta_no_info);
						}
					}
				}

			}
		} catch (JSONException e)
		{
			e.printStackTrace();
		}

	}

	private String dataInizio(String dataEmissione)
	{
		String result;
		// SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH);
		Date dataEmis = new Date();
		try
		{
			dataEmis = sdf.parse(dataEmissione);
		} catch (ParseException e)
		{
			e.printStackTrace();
		}
//		result = sdf.format(new Date(dataEmis.getTime() - 7 * 24 * 60 * 60 * 1000));
//		result = sdf.format(new Date(dataEmis.getTime() - 9 * 24 * 60 * 60 * 1000));
		result = sdf.format(new Date(dataEmis.getTime() - 6 * 24 * 60 * 60 * 1000));
		return result;
	}

	private Date dataInizioEventoDate(String dataEmissione)
	{
		Date result;
		// SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH);
		Date dataEmis = new Date();
		try
		{
			dataEmis = sdf.parse(dataEmissione);
		} catch (ParseException e)
		{
			e.printStackTrace();
		}
		Date now = new Date();
		if (now.after(dataEmis))
		{
			result = now;
		} else
		{
//			result = new Date(dataEmis.getTime() - 7 * 24 * 60 * 60 * 1000);
			result = new Date(dataEmis.getTime() - 9 * 24 * 60 * 60 * 1000);
		}

		Calendar cal = Calendar.getInstance();
		cal.setTime(result);
		cal.set(Calendar.HOUR_OF_DAY, Calendar.getInstance().get(Calendar.HOUR_OF_DAY) + 1);

		result = cal.getTime();
		return result;
	}

	private String dataFine(String dataEmissione)
	{
		String result;
		// SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH);
		result = sdf.format(dataFineDate(dataEmissione));
		return result;
	}

	private Date dataFineDate(String dataEmissione)
	{
		Date result;
		// SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH);
		Date dataEmis = new Date();
		try
		{
			dataEmis = sdf.parse(dataEmissione);
		} catch (ParseException e)
		{
			e.printStackTrace();
		}
		result = new Date(dataEmis.getTime() - 3 * 24 * 60 * 60 * 1000);
//		result = new Date(dataEmis.getTime() - 5 * 24 * 60 * 60 * 1000);
		return result;
	}

	private String convertiData(String data)
	{
		String result;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH);
		Date dataEmis = new Date();
		try
		{
			dataEmis = sdf.parse(data);
		} catch (ParseException e)
		{
			// e.printStackTrace();
		}

		SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MM/yyyy", Locale.ITALY);
		result = sdf1.format(dataEmis);
		return result;
	}

	public String getCodiceCliente(String rispostaLogin)
	{
		String result = "";
		try
		{
			JSONArray loginJson = new JSONArray(rispostaLogin);
			JSONObject datiCliente = (JSONObject) loginJson.get(1);
			result = datiCliente.getString("CodiceCliente");
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
		return result;
	}

	public String[] elencoCodiciContiAttiviCompleti(JSONArray array)
	{
		int lunghezza = numeroCodiciContoDiversiAttiviCompleti(array);
		String result[] = new String[lunghezza];
		String codiceContoConfrontato = "";
		String codiceContoIesimo = "";
		String codiciConcatenati = "";
		int count = 0;
		for (int i = 2; i < array.length(); i++)
		{
			JSONArray contoIesimo;
			try
			{
				contoIesimo = array.getJSONArray(i);
				// codice conto
				JSONObject anagrContoIesimo = contoIesimo.getJSONObject(0);
				codiceContoIesimo = anagrContoIesimo.getString("CodiceConto");

				if (!codiceContoIesimo.equals(codiceContoConfrontato))
				{
					String stato = anagrContoIesimo.getString("StatoConto");
					if (!stato.equals("UTILIZZABILE"))
					{
						continue;
					} else
					{
						// codice conto utilizzabile diverso, controllo non sia
						// vuoto
						if (contoIesimo.length() > 1)
						{
							// che non sia gia' presente
							boolean isPresente = codiciConcatenati.contains("-" + codiceContoIesimo + "-");
							if (isPresente)
							{
								continue;
							} else
							{
								codiceContoConfrontato = codiceContoIesimo;
								result[count] = codiceContoConfrontato;
								codiciConcatenati = codiciConcatenati + "-" + codiceContoIesimo + "-";
								count++;
							}
						} else
						{
							continue;
						}
					}
				}
			} catch (JSONException e)
			{
				e.printStackTrace();
			}
		}
		return result;
	}

	public int numeroCodiciContoDiversiAttiviCompleti(JSONArray array)
	{
		int result = 0;
		String codiceContoConfrontato = "";
		String codiceContoIesimo = "";
		String codiciConcatenati = "";
		for (int i = 2; i < array.length(); i++)
		{
			JSONArray contoIesimo;
			try
			{
				contoIesimo = array.getJSONArray(i);
				// codice conto
				JSONObject anagrContoIesimo = contoIesimo.getJSONObject(0);
				codiceContoIesimo = anagrContoIesimo.getString("CodiceConto");

				if (!codiceContoIesimo.equals(codiceContoConfrontato))
				{
					String stato = anagrContoIesimo.getString("StatoConto");
					if (!stato.equals("UTILIZZABILE"))
					{
						continue;
					} else
					{
						// codice conto utilizzabile diverso, controllo non sia
						// vuoto
						if (contoIesimo.length() > 1)
						{
							if (!codiciConcatenati.contains("-" + codiceContoIesimo))
							{
								// codice conto diverso
								codiceContoConfrontato = codiceContoIesimo;
								codiciConcatenati = codiciConcatenati + "-" + codiceContoIesimo;
								result++;
							}
						} else
						{
							continue;
						}
					}
				}
			} catch (JSONException e)
			{
				e.printStackTrace();
			}
		}
		return result;
	}

	public String getPdfGas(JSONArray arrayFornitureLogin, String codiceConto)
	{
		String result = "";
		JSONObject anagrContoIesimo = new JSONObject();
		JSONObject anagrExtraContoIesimo = new JSONObject();
		try
		{
			for (int i = 2; i < arrayFornitureLogin.length(); i++)
			{
				JSONArray contoIesimo;
				contoIesimo = arrayFornitureLogin.getJSONArray(i);
				anagrContoIesimo = contoIesimo.getJSONObject(0);

				String codiceContoIesimo = anagrContoIesimo.getString("CodiceConto");
				if (!codiceContoIesimo.equals(codiceConto))
				{
					continue;
				} else
				{
					// ho trovato il conto codiceConto
					anagrExtraContoIesimo = contoIesimo.getJSONObject(1);
					String tipologia = anagrExtraContoIesimo.getString("Tipologia");
					if (!tipologia.equals("GAS"))
					{
						continue;
					} else
					{
						result = anagrExtraContoIesimo.getString("Pdf");
					}
				}
			}
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
		return result;

	}

	public void postChiamata(String rispostaChiamata, List<Cookie> lista, HashMap<String, String> parametri)
	{
		if (animation != null)
		{
			animation.stop();
			imageAnim.setVisibility(View.INVISIBLE);
		}
		final ImageView indietro = (ImageView) findViewById(R.id.bollette_pagamenti_indietro);
		indietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
		indietro.getBackground().invalidateSelf();

		JSONArray rispostaJSON = new JSONArray();
		String esito = "";
		try
		{
			rispostaJSON = new JSONArray(rispostaChiamata);
			JSONObject oggettoRisposta = (JSONObject) rispostaJSON.get(0);
			esito = oggettoRisposta.getString("esito");

		} catch (JSONException e)
		{
			e.printStackTrace();
		}
		if (esito.equals("200"))
		{
			Constants.SERVIZIO_FORNITURE_GAS_ATTIVE = rispostaChiamata;
			Intent intentAutolettura = new Intent(getApplicationContext(), Autolettura.class);
			intentAutolettura.putExtra(Autolettura.SOURCE_PAGE, BollettePagamentiProssimaBolletta.class.getName());

			intentAutolettura.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
			intentAutolettura.putExtra("TIPOLOGIA", getIntent().getStringExtra("TIPOLOGIA"));
			intentAutolettura.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
			intentAutolettura.putExtra("SOURCE_ACTIVITY", BollettePagamenti.class.getName());

			finish();
			startActivity(intentAutolettura);
			overridePendingTransition(R.anim.fade, R.anim.hold);
		} else
		{
			Intent intentOops = new Intent(getApplicationContext(), OopsPageProssimaBollettaAutolettura.class);
			intentOops.putExtra("PARAMETER", parametri);
			intentOops.putExtra("PRIMOCONTO", primoConto);
			intentOops.putExtra("TIPOLOGIA", tipologia);

			intentOops.putExtra("PDR", pdr);
			intentOops.putExtra("PDF", pdf);
			intentOops.putExtra("CURRENT_CODICE_CONTO", codiceConto);
			intentOops.putExtra("SISTEMABILLING", sistemaBilling);
			intentOops.putExtra("SOCIETAFORNITRICE", societaFornitrice);
			intentOops.putExtra("NUMCIFREMISURATORE", numeroCifreMisuratore);
			// errori ops page
			if (esito.equals("309"))
			{
				intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
				startActivity(intentOops);
				overridePendingTransition(R.anim.fade, R.anim.hold);

			} else if (esito.equals("400"))
			{
				intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
				startActivity(intentOops);
				overridePendingTransition(R.anim.fade, R.anim.hold);

			} else if (esito.equals("420"))
			{
				intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
				startActivity(intentOops);
				overridePendingTransition(R.anim.fade, R.anim.hold);

			} else if (esito.equals("449"))
			{
				intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
				startActivity(intentOops);
				overridePendingTransition(R.anim.fade, R.anim.hold);

			} else if (esito.equals("451"))
			{
				intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_popup_451));
				startActivity(intentOops);
				overridePendingTransition(R.anim.fade, R.anim.hold);

			}
			// errori popup
			else if (esito.equals("307"))
			{
				Utils.popup(BollettePagamentiProssimaBolletta.this, getResources().getString(R.string.errore_popup_307), "Riprova");

			} else if (esito.equals("435"))
			{
				Utils.popup(BollettePagamentiProssimaBolletta.this, getResources().getString(R.string.errore_popup_435), "Riprova");

			} else if (esito.equals("437"))
			{
				Utils.popup(BollettePagamentiProssimaBolletta.this, getResources().getString(R.string.errore_popup_437), "Riprova");

			} else if (esito.equals("438"))
			{
				Utils.popup(BollettePagamentiProssimaBolletta.this, getResources().getString(R.string.errore_popup_438), "Riprova");

			} else if (esito.equals("443"))
			{
				Utils.popup(BollettePagamentiProssimaBolletta.this, getResources().getString(R.string.errore_popup_443), "Riprova");

			} else if (esito.equals("444"))
			{
				Utils.popup(BollettePagamentiProssimaBolletta.this, getResources().getString(R.string.errore_popup_444), "Riprova");

			} else if (esito.equals("445"))
			{
				Utils.popup(BollettePagamentiProssimaBolletta.this, getResources().getString(R.string.errore_popup_445), "Riprova");

			} else if (esito.equals("446"))
			{
				Utils.popup(BollettePagamentiProssimaBolletta.this, getResources().getString(R.string.errore_popup_446), "Riprova");

			} else if (esito.equals("447"))
			{
				Utils.popup(BollettePagamentiProssimaBolletta.this, getResources().getString(R.string.errore_popup_447), "Riprova");

			}else {
		    	if (esito.equals("")) {
					intentOops = new Intent(getApplicationContext(), OopsPageSessioneScaduta.class);
					intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.sessione_scaduta));
					startActivity(intentOops);
					overridePendingTransition(R.anim.fade, R.anim.hold);
				} 
		    }

		}

	}

	public void postChiamataException(boolean exception, String parametri, HashMap<String, String> parametriHash)
	{
		if (exception)
		{
			if (animation != null)
			{
				animation.stop();
				imageAnim.setVisibility(View.INVISIBLE);
			}

			final ImageView indietro = (ImageView) findViewById(R.id.bollette_pagamenti_indietro);
			indietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
			indietro.getBackground().invalidateSelf();

			Intent intentOops = new Intent(getApplicationContext(), OopsPageProssimaBollettaAutolettura.class);
			intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
			intentOops.putExtra("PARAMETER", parametri);
			intentOops.putExtra("PDR", pdr);
			intentOops.putExtra("PDF", pdf);
			intentOops.putExtra("CURRENT_CODICE_CONTO", codiceConto);
			intentOops.putExtra("SISTEMABILLING", sistemaBilling);
			intentOops.putExtra("SOCIETAFORNITRICE", societaFornitrice);
			intentOops.putExtra("NUMCIFREMISURATORE", numeroCifreMisuratore);
			startActivity(intentOops);
			overridePendingTransition(R.anim.fade, R.anim.hold);
		}
	}

	private void chiamataLoadFornitureGas()
	{

		animation = Utilities.animation(this);
		imageAnim = (ImageView) findViewById(R.id.waiting_anim);
		imageAnim.setVisibility(View.VISIBLE);
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN)
		{
			imageAnim.setBackgroundDrawable(animation);
		} else
		{
			imageAnim.setBackground(animation);
		}
		animation.start();

		String codiceCliente = getCodiceCliente(rispostaLogin);
		String elencoCodiciCompleti[];
		String parametri = "&codiceCliente=" + codiceCliente;
		try
		{
			elencoCodiciCompleti = elencoCodiciContiAttiviCompleti(new JSONArray(rispostaLogin));
			for (int i = 0; i < elencoCodiciCompleti.length; i++)
			{
				String pdf = getPdfGas(new JSONArray(rispostaLogin), elencoCodiciCompleti[i]);
				if (!pdf.equals(""))
				{
					parametri = parametri + "&" + "pdf=" + pdf;
				}
			}
		} catch (JSONException e)
		{
			e.printStackTrace();
		}

		if (!isNetworkAvailable(context))
		{
			Intent intentOops = new Intent(getApplicationContext(), OopsPageProssimaBollettaAutolettura.class);
			intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.connessione_assente));
			intentOops.putExtra("PARAMETER", parametri);
			intentOops.putExtra("PDR", pdr);
			intentOops.putExtra("PDF", pdf);
			intentOops.putExtra("CURRENT_CODICE_CONTO", codiceConto);
			intentOops.putExtra("SISTEMABILLING", sistemaBilling);
			intentOops.putExtra("SOCIETAFORNITRICE", societaFornitrice);
			intentOops.putExtra("NUMCIFREMISURATORE", numeroCifreMisuratore);
			startActivity(intentOops);
			overridePendingTransition(R.anim.fade, R.anim.hold);
		} else
		{
			/********************* loadFornitureInfoGas ******************************/
			ServiceCall autoletturaServiceCall = ServiceCallFactory.createServiceCall(context);
			// autoletturaServiceCall.myCookieStore = new
			// PersistentCookieStore(context);
			/*****************************************************************************/
			try
			{
				autoletturaServiceCall.executeHttpsGetProssimaBolletta(Constants.URL_FORNITURE_GAS_ATTIVE, parametri, this, null);
			} catch (Exception e)
			{
				Intent intentOops = new Intent(getApplicationContext(), OopsPageProssimaBollettaAutolettura.class);
				intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
				intentOops.putExtra("PARAMETER", parametri);
				intentOops.putExtra("PDR", pdr);
				intentOops.putExtra("PDF", pdf);
				intentOops.putExtra("CURRENT_CODICE_CONTO", codiceConto);
				intentOops.putExtra("SISTEMABILLING", sistemaBilling);
				intentOops.putExtra("SOCIETAFORNITRICE", societaFornitrice);
				intentOops.putExtra("NUMCIFREMISURATORE", numeroCifreMisuratore);
				startActivity(intentOops);
				overridePendingTransition(R.anim.fade, R.anim.hold);
			}
			/************************************************************************************/
		}

	}

	private boolean isNetworkAvailable(Context context)
	{
		ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivityManager == null)
			return false;
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		return activeNetworkInfo != null;
	}

	public void onBackPressed()
	{

		animation = Utilities.animation(this);
		imageAnim = (ImageView) findViewById(R.id.waiting_anim);
		imageAnim.setVisibility(View.VISIBLE);
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN)
		{
			imageAnim.setBackgroundDrawable(animation);
		} else
		{
			imageAnim.setBackground(animation);
		}
		animation.start();

		Intent intentBollettePagamenti = null;

		if (sourceActivity.equals(AutoLetturaInsert.class.getName()))
		{
			intentBollettePagamenti = new Intent(getApplicationContext(), AutoLetturaInsert.class);
			intentBollettePagamenti.putExtra("NUMCIFREMISURATORE", numeroCifreMisuratore);
			intentBollettePagamenti.putExtra("PDF", pdf);
			intentBollettePagamenti.putExtra("PDR", pdr);
			intentBollettePagamenti.putExtra("SISTEMABILLING", sistemaBilling);
			intentBollettePagamenti.putExtra("SOCIETAFORNITRICE", societaFornitrice);
			intentBollettePagamenti.putExtra("CURRENT_CODICE_CONTO", codiceConto);
		} else
			intentBollettePagamenti = new Intent(getApplicationContext(), BollettePagamenti.class);

		intentBollettePagamenti.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
		intentBollettePagamenti.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
		BollettePagamentiProssimaBolletta.this.finish();
		startActivity(intentBollettePagamenti);
		overridePendingTransition(R.anim.fade, R.anim.hold);
	}

	public void addNewEventAutolettura(final JSONObject infoProssimaFattura)
	{

		String eventName = getString(R.string.event_name);

		Date endEvent = null;
		try
		{
			endEvent = new SimpleDateFormat("dd/MM/yyyy", Locale.ITALY).parse(convertiData(infoProssimaFattura.getString("dataEmissioneProssimaFattura")));
		} catch (ParseException e)
		{
			e.printStackTrace();
		} catch (JSONException e)
		{
			e.printStackTrace();
		}

//		long eventMillis = endEvent.getTime() - 7 * 24 * 60 * 60 * 1000;
		long eventMillis = endEvent.getTime() - 9 * 24 * 60 * 60 * 1000;
		Date startEventDate = new Date(eventMillis);
		Date now = new Date();

		Date tmpStartEvent = startEventDate;
		if( now.after(startEventDate) )
		{
			tmpStartEvent = now;
		}
				
		// current hour
		int hour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
		int minutes = Calendar.getInstance().get(Calendar.MINUTE);

		Calendar calStart = Calendar.getInstance();
		calStart.setTime(tmpStartEvent);
		calStart.set(Calendar.HOUR_OF_DAY, hour + 1);
		calStart.set(Calendar.MINUTE, minutes);

		Calendar calEnd = Calendar.getInstance();
		calEnd.setTime(calStart.getTime());
		calEnd.add(Calendar.MINUTE, 30);

		Log.d("EVENT_REMINDER","calStart: " + calStart.getTime());
		Log.d("EVENT_REMINDER","calEnd: " + calEnd.getTime());
		
		final long startDate = calStart.getTimeInMillis();
		final long endDate = calEnd.getTimeInMillis();

//		final String recurrenecRules = "FREQ=DAILY;UNTIL=" + new SimpleDateFormat("yyyyMMdd", Locale.getDefault()).format(new Date(endEvent.getTime() - 3 * 24 * 60 * 60 * 1000));
		final String recurrenecRules = "FREQ=DAILY;UNTIL=" + new SimpleDateFormat("yyyyMMdd", Locale.getDefault()).format(new Date(endEvent.getTime() - 5 * 24 * 60 * 60 * 1000));
		Log.d("EVENT_REMINDER","recurrenecRules: " + recurrenecRules);

		Intent add_event = new Intent(Intent.ACTION_INSERT).setData(Events.CONTENT_URI).putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, startDate)
				.putExtra(CalendarContract.EXTRA_EVENT_END_TIME, endDate).putExtra(Events.TITLE, eventName).putExtra(Events.HAS_ALARM, true)
				.putExtra(Events.RRULE, recurrenecRules);

		startActivity(add_event);

		// final AsyncQueryHandler handlerReminder = new
		// AsyncQueryHandler(getContentResolver()) {
		//
		// @Override
		// protected void onInsertComplete(int token, Object cookie, Uri uri) {
		//
		// switch (token) {
		// case REMINDER_TOKEN:
		//
		// Intent editEvent = new Intent(Intent.ACTION_VIEW).setData(eventID);
		// editEvent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME,
		// startDate);
		// editEvent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME, endDate);
		//
		// startActivity(editEvent);
		//
		// break;
		//
		// default:
		// break;
		// }
		// }
		// };
		//
		// handlerEvent = new AsyncQueryHandler(getContentResolver()) {
		//
		// @Override
		// protected void onInsertComplete(int token, Object cookie, Uri uri) {
		//
		// switch (token) {
		// case EVENT_TOKEN:
		//
		// eventID = uri;
		//
		// ContentValues cvReminder = new ContentValues();
		// cvReminder.put(Reminders.EVENT_ID,
		// Long.parseLong(eventID.getLastPathSegment()));
		// cvReminder.put(Reminders.MINUTES, 1440 * 2);
		// cvReminder.put(Reminders.METHOD, Reminders.METHOD_DEFAULT);
		//
		// handlerReminder.startInsert(REMINDER_TOKEN, null,
		// Reminders.CONTENT_URI, cvReminder);
		//
		// break;
		// }
		// }
		// };
		//
		// ContentValues cvEvent = new ContentValues();
		//
		// cvEvent.put(Events.DTSTART, startDate);
		// cvEvent.put(Events.DTEND, endDate);
		// cvEvent.put(Events.TITLE, eventName);
		// cvEvent.put(Events.CALENDAR_ID, calID);
		// cvEvent.put(Events.EVENT_TIMEZONE, "Europe/Rome");
		// cvEvent.put(Events.HAS_ALARM, true);
		//
		// handlerEvent.startInsert(EVENT_TOKEN, null, Events.CONTENT_URI,
		// cvEvent);
	}
}
