package com.eni.enigaseluce.bollettePagamenti;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.AnimationDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.eni.enigaseluce.R;
import com.eni.enigaseluce.httpcall.Constants;
import com.eni.enigaseluce.utils.Utilities;

@SuppressLint("NewApi")
public class BollettePagamentiSceltaPiano extends Activity {
    private Context context;
    private static Context staticContext;
    AnimationDrawable animation;
    ImageView imageAnim;

    public String rispostaLogin = Constants.SERVIZIO_LOGIN;

    public static String nomeInteroCliente = "";
    static LayoutInflater inflater;
    String codiceEsito;
    String tipologia;

    static LayoutInflater inflaterDialog;

    public static Context getContext() {
	return BollettePagamentiSceltaPiano.staticContext;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.bollette_pagamenti_scelta_piani);

	context = this;
	BollettePagamentiSceltaPiano.staticContext = this;

    }

    @Override
    protected void onStart() {
	super.onStart();
	final ImageView indietro = (ImageView) findViewById(R.id.bollette_pagamenti_indietro);
	indietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
	indietro.getBackground().invalidateSelf();
	indietro.setOnClickListener(new View.OnClickListener() {
	    public void onClick(View v) {
		indietro.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
		indietro.getBackground().invalidateSelf();

		animation = Utilities.animation(BollettePagamentiSceltaPiano.this);
		imageAnim = (ImageView) findViewById(R.id.waiting_anim);
		imageAnim.setVisibility(View.VISIBLE);
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
		    imageAnim.setBackgroundDrawable(animation);
		}
		else {
		    imageAnim.setBackground(animation);
		}
		animation.start();

		Intent intentHome = new Intent(getApplicationContext(), BollettePagamentiRateizzate.class);
		intentHome.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
		intentHome.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
		intentHome.putExtra("tipoFornitura", getIntent().getStringExtra("tipoFornitura"));
		
		intentHome.putExtra("domiciliazioneApprovata", getIntent().getBooleanExtra("domiciliazioneApprovata", false));
	    intentHome.putExtra("infoDomiciliazioneApprovata",  getIntent().getBooleanExtra("infoDomiciliazioneApprovata", false));
	    
		BollettePagamentiSceltaPiano.this.finish();
		startActivity(intentHome);
		overridePendingTransition(R.anim.fade, R.anim.hold);

	    }
	});

	/*
	 * final ImageView help =
	 * (ImageView)findViewById(R.id.bollette_pagamenti_help);
	 * help.getBackground().setColorFilter(0xFFFFFFFF,
	 * PorterDuff.Mode.MULTIPLY); help.getBackground().invalidateSelf();
	 * help.setOnClickListener(new View.OnClickListener() { public void
	 * onClick(View v) { help.getBackground().setColorFilter(0xFF999999,
	 * PorterDuff.Mode.MULTIPLY); help.getBackground().invalidateSelf();
	 * animation = new AnimationDrawable();
	 * animation.addFrame(getResources().getDrawable(R.drawable.anim_load2),
	 * 300);
	 * animation.addFrame(getResources().getDrawable(R.drawable.anim_load3),
	 * 300);
	 * animation.addFrame(getResources().getDrawable(R.drawable.anim_load4),
	 * 300); animation.setOneShot(false); imageAnim = (ImageView)
	 * findViewById(R.id.waiting_bollette_pagamenti);
	 * imageAnim.setVisibility(View.VISIBLE); if(Build.VERSION.SDK_INT <
	 * 16){ imageAnim.setBackgroundDrawable(animation); }else{
	 * imageAnim.setBackground(animation); } // start the animation!
	 * animation.start();
	 * 
	 * Intent intentHelp = new Intent(getApplicationContext(),
	 * BollettePagamentiHelp.class); intentHelp.putExtra("ORIGINE",
	 * Constants.ORIGINE_BOLLETTE_RATEIZZABILI_SELEZIONE_PIANO);
	 * intentHelp.putExtra("PRIMOCONTO",
	 * getIntent().getStringExtra("PRIMOCONTO"));
	 * intentHelp.putExtra("INDICE_SPINNER",
	 * getIntent().getIntExtra("INDICE_SPINNER", 0));
	 * intentHelp.putExtra("tipoFornitura",
	 * getIntent().getStringExtra("tipoFornitura"));
	 * BollettePagamentiSceltaPiano.this.finish();
	 * startActivity(intentHelp); overridePendingTransition(R.anim.fade,
	 * R.anim.hold); } });
	 */
	try {
	    rispostaLogin = Constants.SERVIZIO_LOGIN;
	    final JSONArray bollettePagamentiLogin = new JSONArray(rispostaLogin);

	    JSONObject datiAnagr = bollettePagamentiLogin.getJSONObject(1);
	    // ultimo accesso
	    String ultimoaccesso = datiAnagr.getString("UltimoAccesso");
	    TextView ultimoAccesso = (TextView) findViewById(R.id.ultimo_accesso);
	    // ultimoAccesso.setTextSize(11);
	    ultimoAccesso.setText(ultimoaccesso);
	    // nome cliente
	    String nomeClienteString = datiAnagr.getString("NomeCliente");
	    String cognomeClienteString = datiAnagr.getString("CognomeCliente");
	    TextView nomeCliente = (TextView) findViewById(R.id.nome_cliente);
	    nomeInteroCliente = nomeClienteString + " " + cognomeClienteString;
	    if (nomeInteroCliente.length() > 18) {
		nomeCliente.setText(nomeInteroCliente.substring(0, 17) + "...");
	    }
	    else {
		nomeCliente.setText(nomeInteroCliente);
	    }

	    final ImageView piano_secondo_delibera = (ImageView) findViewById(R.id.piano_secondo_delibera);
	    piano_secondo_delibera.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
	    piano_secondo_delibera.getBackground().invalidateSelf();
	    piano_secondo_delibera.setOnClickListener(new View.OnClickListener() {
		public void onClick(View v) {
		    piano_secondo_delibera.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
		    piano_secondo_delibera.getBackground().invalidateSelf();
		    // porta alla pagina di dettaglio con piano secondo delibera
		    // put extra popup sovvraposizione true
		    Intent intentDettaglioPiano = new Intent(getApplicationContext(), BollettePagamentiDettaglioPiano.class);
		    intentDettaglioPiano.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
		    intentDettaglioPiano.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
		    intentDettaglioPiano.putExtra("tipoFornitura", getIntent().getStringExtra("tipoFornitura"));
		    intentDettaglioPiano.putExtra("tipoPiano", Constants.PIANO_RATEIZZAZIONE_DOPPIO);
		    intentDettaglioPiano.putExtra("identificativoFattura", getIntent().getStringExtra("identificativoFattura"));

		    intentDettaglioPiano.putExtra("pianoSelezionato", Constants.PIANO_SECONDO_DELIBERA);
		    intentDettaglioPiano.putExtra("codiceFattura", getIntent().getStringExtra("codiceFattura"));
		    intentDettaglioPiano.putExtra("dataEmissione", getIntent().getStringExtra("dataEmissione"));
		    intentDettaglioPiano.putExtra("dataScadenza", getIntent().getStringExtra("dataScadenza"));
		    intentDettaglioPiano.putExtra("importoRateizzabile", getIntent().getStringExtra("importoRateizzabile"));
		    intentDettaglioPiano.putExtra("codiceCausaleRateizzabilita", "01");
		    intentDettaglioPiano.putExtra("codiceCausaleRateizzabilitaAlternativa", "02");
		    intentDettaglioPiano.putExtra("PAGINA_PROVENIENZA", BollettePagamentiSceltaPiano.class.getCanonicalName());
		    
		    intentDettaglioPiano.putExtra("domiciliazioneApprovata", getIntent().getBooleanExtra("domiciliazioneApprovata", false));
		    intentDettaglioPiano.putExtra("infoDomiciliazioneApprovata",  getIntent().getBooleanExtra("infoDomiciliazioneApprovata", false));
		    
		    BollettePagamentiSceltaPiano.this.finish();
		    startActivity(intentDettaglioPiano);
		    overridePendingTransition(R.anim.fade, R.anim.hold);
		}
	    });
	    final ImageView piano_ritardo_fatturazione = (ImageView) findViewById(R.id.piano_ritardo_fatturazione);
	    piano_ritardo_fatturazione.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
	    piano_ritardo_fatturazione.getBackground().invalidateSelf();
	    piano_ritardo_fatturazione.setOnClickListener(new View.OnClickListener() {
		public void onClick(View v) {
		    piano_ritardo_fatturazione.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
		    piano_ritardo_fatturazione.getBackground().invalidateSelf();
		    // porta alla pagina di dettaglio con piano ritardo di fatturazione put extra popup sovvraposizione true
		    Intent intentDettaglioPiano = new Intent(getApplicationContext(), BollettePagamentiDettaglioPiano.class);
		    intentDettaglioPiano.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
		    intentDettaglioPiano.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
		    intentDettaglioPiano.putExtra("tipoFornitura", getIntent().getStringExtra("tipoFornitura"));
		    intentDettaglioPiano.putExtra("tipoPiano", Constants.PIANO_RATEIZZAZIONE_DOPPIO);
		    intentDettaglioPiano.putExtra("identificativoFattura", getIntent().getStringExtra("identificativoFattura"));

		    intentDettaglioPiano.putExtra("pianoSelezionato", Constants.PIANO_RITARDO_FATTURAZIONE);
		    intentDettaglioPiano.putExtra("codiceFattura", getIntent().getStringExtra("codiceFattura"));
		    intentDettaglioPiano.putExtra("dataEmissione", getIntent().getStringExtra("dataEmissione"));
		    intentDettaglioPiano.putExtra("dataScadenza", getIntent().getStringExtra("dataScadenza"));
		    intentDettaglioPiano.putExtra("importoRateizzabile", getIntent().getStringExtra("importoRateizzabile"));
		    intentDettaglioPiano.putExtra("codiceCausaleRateizzabilita", "02");
		    intentDettaglioPiano.putExtra("codiceCausaleRateizzabilitaAlternativa", "01");
		    intentDettaglioPiano.putExtra("PAGINA_PROVENIENZA", BollettePagamentiSceltaPiano.class.getCanonicalName());
		    
		    intentDettaglioPiano.putExtra("domiciliazioneApprovata", getIntent().getBooleanExtra("domiciliazioneApprovata", false));
		    intentDettaglioPiano.putExtra("infoDomiciliazioneApprovata",  getIntent().getBooleanExtra("infoDomiciliazioneApprovata", false));
		    
		    BollettePagamentiSceltaPiano.this.finish();
		    startActivity(intentDettaglioPiano);
		    overridePendingTransition(R.anim.fade, R.anim.hold);
		}
	    });
	    inflaterDialog = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);

	    final ImageView icona_info_delibera = (ImageView) findViewById(R.id.icona_info_delibera);
	    icona_info_delibera.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
	    icona_info_delibera.getBackground().invalidateSelf();
			icona_info_delibera.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					icona_info_delibera.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
					icona_info_delibera.getBackground().invalidateSelf();
					// pagina in sovvraposizione
					final Dialog dialog = new Dialog(BollettePagamentiSceltaPiano.this, android.R.style.Theme_Translucent_NoTitleBar);
					final View det_retail = inflaterDialog.inflate(R.layout.popup_info_piani_rateizzazione, null);
					dialog.setContentView(det_retail);

					dialog.setCancelable(true);

					final LinearLayout chiudi = (LinearLayout) dialog.findViewById(R.id.chiudi);
					chiudi.setOnClickListener(new View.OnClickListener() {
						public void onClick(View v) {
							icona_info_delibera.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
							icona_info_delibera.getBackground().invalidateSelf();
							dialog.dismiss();
						}
					});
					final RelativeLayout piano_secondo_delibera = (RelativeLayout) dialog.findViewById(R.id.piano_secondo_delibera);
					final RelativeLayout piano_ritardo_fatturazione = (RelativeLayout) dialog.findViewById(R.id.piano_ritardo_fatturazione);
					
					final TextView piano_secondo_delibera_textview = (TextView)dialog.findViewById(R.id.piano_secondo_delibera_textview);
					boolean domApprovata = getIntent().getBooleanExtra("domiciliazioneApprovata", false);
					if(domApprovata){
						piano_secondo_delibera_textview.setText(getResources().getString(R.string.piano_secondo_delibera_domiciliati_text));
					}else{
						piano_secondo_delibera_textview.setText(getResources().getString(R.string.piano_secondo_delibera_text));
					}
					piano_secondo_delibera.setVisibility(View.VISIBLE);
					piano_ritardo_fatturazione.setVisibility(View.GONE);

					dialog.show();
				}
			});

	    final ImageView icona_info_ritardo = (ImageView) findViewById(R.id.icona_info_ritardo);
	    icona_info_ritardo.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
	    icona_info_ritardo.getBackground().invalidateSelf();
			icona_info_ritardo.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					icona_info_ritardo.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
					icona_info_ritardo.getBackground().invalidateSelf();

					// pagina in sovvraposizione
					final Dialog dialog = new Dialog(BollettePagamentiSceltaPiano.this, android.R.style.Theme_Translucent_NoTitleBar);
					final View det_retail = inflaterDialog.inflate(R.layout.popup_info_piani_rateizzazione, null);
					dialog.setContentView(det_retail);

					dialog.setCancelable(true);

					final LinearLayout chiudi = (LinearLayout) dialog.findViewById(R.id.chiudi);
					chiudi.setOnClickListener(new View.OnClickListener() {
						public void onClick(View v) {
							icona_info_ritardo.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
							icona_info_ritardo.getBackground().invalidateSelf();
							dialog.dismiss();
						}
					});
					final RelativeLayout piano_secondo_delibera = (RelativeLayout) dialog.findViewById(R.id.piano_secondo_delibera);
					final RelativeLayout piano_ritardo_fatturazione = (RelativeLayout) dialog.findViewById(R.id.piano_ritardo_fatturazione);
					
					final TextView piano_ritardo_fatturazione_textview = (TextView)dialog.findViewById(R.id.piano_ritardo_fatturazione_textview);
					boolean domApprovata = getIntent().getBooleanExtra("domiciliazioneApprovata", false);
					if(domApprovata){
						piano_ritardo_fatturazione_textview.setText(getResources().getString(R.string.piano_ritardo_fatturazione_domiciliati_text));
					}else{
						piano_ritardo_fatturazione_textview.setText(getResources().getString(R.string.piano_ritardo_fatturazione_text));
					}
					piano_secondo_delibera.setVisibility(View.GONE);
					piano_ritardo_fatturazione.setVisibility(View.VISIBLE);

					dialog.show();
				}
			});

	}
		catch (JSONException e) {
		    e.printStackTrace();
		}
    }

    public void onBackPressed() {

	animation = Utilities.animation(this);
	imageAnim = (ImageView) findViewById(R.id.waiting_anim);
	imageAnim.setVisibility(View.VISIBLE);
	if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
	    imageAnim.setBackgroundDrawable(animation);
	}
	else {
	    imageAnim.setBackground(animation);
	}
	animation.start();

	Intent intentHome = new Intent(getApplicationContext(), BollettePagamentiRateizzate.class);
	intentHome.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
	intentHome.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
	intentHome.putExtra("tipoFornitura", getIntent().getStringExtra("tipoFornitura"));
	
	intentHome.putExtra("domiciliazioneApprovata", getIntent().getBooleanExtra("domiciliazioneApprovata", false));
	intentHome.putExtra("infoDomiciliazioneApprovata",  getIntent().getBooleanExtra("infoDomiciliazioneApprovata", false));
    
	
	BollettePagamentiSceltaPiano.this.finish();
	startActivity(intentHome);
	overridePendingTransition(R.anim.fade, R.anim.hold);
    }
}
