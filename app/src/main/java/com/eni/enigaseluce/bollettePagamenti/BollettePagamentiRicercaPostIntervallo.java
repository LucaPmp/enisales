package com.eni.enigaseluce.bollettePagamenti;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import org.apache.http.cookie.Cookie;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.AnimationDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.eni.enigaseluce.R;
import com.eni.enigaseluce.error.OopsPageBollettePagamentiDownloadBollettaIntervallo;
import com.eni.enigaseluce.error.OopsPageSessioneScaduta;
import com.eni.enigaseluce.error.Utils;
import com.eni.enigaseluce.httpcall.Constants;
import com.eni.enigaseluce.httpcall.ServiceCall;
import com.eni.enigaseluce.httpcall.ServiceCallFactory;
import com.eni.enigaseluce.login.help.GuidaBolletta;
import com.eni.enigaseluce.utils.Utilities;

@SuppressLint("NewApi")
public class BollettePagamentiRicercaPostIntervallo extends Activity {
    private Context context;
    private static Context staticContext;
    AnimationDrawable animation;
    ImageView imageAnim;

    static LinearLayout pager;

    public String rispostaLogin = Constants.SERVIZIO_LOGIN;
    public String rispostaShowListaFatture = Constants.SERVIZIO_SHOW_LISTA_FATTURE;

    public static String nomeInteroCliente = "";
    static LayoutInflater inflater;
    String codiceEsito;
    String primoConto;
    String dataInizio;
    String dataFine;

    LinearLayout waiting_download;
    LinearLayout contenuto;

    public static Context getContext() {
        return BollettePagamentiRicercaPostIntervallo.staticContext;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bollette_pagamenti_ricerca_con_intervallo);

        context = this;
        BollettePagamentiRicercaPostIntervallo.staticContext = this;
        primoConto = getIntent().getStringExtra("PRIMOCONTO");
        dataInizio = getIntent().getStringExtra("DATA_INIZIO");
        dataFine = getIntent().getStringExtra("DATA_FINE");

    }

    @Override
    protected void onStart() {
        super.onStart();

        final ImageView indietro = (ImageView) findViewById(R.id.bollette_pagamenti_indietro);
        indietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
        indietro.getBackground().invalidateSelf();
        indietro.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                indietro.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
                indietro.getBackground().invalidateSelf();

                animation = Utilities.animation(BollettePagamentiRicercaPostIntervallo.this);
                imageAnim = (ImageView) findViewById(R.id.waiting_anim);
                imageAnim.setVisibility(View.VISIBLE);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    imageAnim.setBackgroundDrawable(animation);
                } else {
                    imageAnim.setBackground(animation);
                }
                animation.start();

                // si va alla pagina BollettePagamenti
                // devo fare la chiamata per sovrascrivere gli ultimi 2 anni
                String codiceCliente = getCodiceCliente(rispostaLogin);
                if (!isNetworkAvailable(context)) {
                    Intent intentOops = new Intent(getApplicationContext(), OopsPageBollettePagamentiDownloadBollettaIntervallo.class);
                    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.connessione_assente));
                    intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
                    intentOops.putExtra("CODICE_CONTO_CLIENTE", primoConto);
                    // BollettePagamentiRicercaPostIntervallo.this.finish();
                    startActivity(intentOops);
                    if (animation != null) {
                        animation.stop();
                        imageAnim.setVisibility(View.INVISIBLE);
                    }
                    overridePendingTransition(R.anim.fade, R.anim.hold);
                } else {
                    /********************* Chiamata show lista forniture ******************************/
                    ServiceCall listaFornitureServiceCall = ServiceCallFactory.createServiceCall(context);
                    // listaFornitureServiceCall.myCookieStore = new
                    // PersistentCookieStore(context);

                    HashMap<String, String> parametri = new HashMap<String, String>();
                    parametri.put("codiceCliente", codiceCliente);
                    parametri.put("codiceContoCliente", primoConto);

                    try {
                        listaFornitureServiceCall.executeHttpsGetBollettePagamentiRicercaPostIntervallo(Constants.URL_SHOW_LISTA_FATTURE, parametri, BollettePagamentiRicercaPostIntervallo.this, null, false, "");
                    } catch (Exception e) {
                        Intent intentOops = new Intent(getApplicationContext(), OopsPageBollettePagamentiDownloadBollettaIntervallo.class);
                        intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
                        intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
                        intentOops.putExtra("CODICE_CONTO_CLIENTE", primoConto);
                        // BollettePagamentiRicercaPostIntervallo.this.finish();
                        startActivity(intentOops);
                        overridePendingTransition(R.anim.fade, R.anim.hold);
                    }
                }
            }
        });

        final ImageView help = (ImageView) findViewById(R.id.bollette_pagamenti_help);
        help.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
        help.getBackground().invalidateSelf();
        help.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                help.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
                help.getBackground().invalidateSelf();

                animation = Utilities.animation(BollettePagamentiRicercaPostIntervallo.this);
                imageAnim = (ImageView) findViewById(R.id.waiting_anim);
                imageAnim.setVisibility(View.VISIBLE);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    imageAnim.setBackgroundDrawable(animation);
                } else {
                    imageAnim.setBackground(animation);
                }
                animation.start();

                Intent intentHelp = new Intent(getApplicationContext(), BollettePagamentiHelp.class);
                intentHelp.putExtra("ORIGINE", Constants.ORIGINE_BOLLETTE_PAGAMENTI_RICERCA_POST_INTERVALLO);
                intentHelp.putExtra("PRIMOCONTO", primoConto);
                intentHelp.putExtra("DATA_INIZIO", dataInizio);
                intentHelp.putExtra("DATA_FINE", dataFine);
                intentHelp.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
                BollettePagamentiRicercaPostIntervallo.this.finish();
                startActivity(intentHelp);
                overridePendingTransition(R.anim.fade, R.anim.hold);

            }
        });
        try {
            final JSONArray bollettePagamentiLogin = new JSONArray(rispostaLogin);
            JSONObject esito = bollettePagamentiLogin.getJSONObject(0);
            codiceEsito = (String) esito.getString("esito");
            if (codiceEsito.equals("200")) {
                TextView dal = (TextView) findViewById(R.id.visualizzazione_bollette_dal_valore);
                dal.setText(dataInizio);
                TextView al = (TextView) findViewById(R.id.visualizzazione_bollette_al_valore);
                al.setText(dataFine);

                ImageView bollette_pagamenti_cerca_btn = (ImageView) findViewById(R.id.bollette_pagamenti_cerca_btn);
                bollette_pagamenti_cerca_btn.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {

                        animation = Utilities.animation(BollettePagamentiRicercaPostIntervallo.this);
                        imageAnim = (ImageView) findViewById(R.id.waiting_anim);
                        imageAnim.setVisibility(View.VISIBLE);
                        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                            imageAnim.setBackgroundDrawable(animation);
                        } else {
                            imageAnim.setBackground(animation);
                        }
                        animation.start();

                        Intent intentBollettePagamentiRicercaIntervallo = new Intent(getApplicationContext(), BollettePagamentiRicercaIntervallo.class);
                        intentBollettePagamentiRicercaIntervallo.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
                        intentBollettePagamentiRicercaIntervallo.putExtra("ORIGINE", Constants.ORIGINE_BOLLETTE_PAGAMENTI_RICERCA_POST_INTERVALLO);
                        intentBollettePagamentiRicercaIntervallo.putExtra("DATA_INIZIO", getIntent().getStringExtra("DATA_INIZIO"));
                        intentBollettePagamentiRicercaIntervallo.putExtra("DATA_FINE", getIntent().getStringExtra("DATA_FINE"));
                        intentBollettePagamentiRicercaIntervallo.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
                        BollettePagamentiRicercaPostIntervallo.this.finish();
                        startActivity(intentBollettePagamentiRicercaIntervallo);
                        overridePendingTransition(R.anim.fade, R.anim.hold);
                    }
                });

                pager = (LinearLayout) findViewById(R.id.bollette_pagamenti_lista_pager);

                inflater = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);
                if (!rispostaShowListaFatture.equals("")) {
                    popolaPaginaContenuto(new JSONArray(rispostaShowListaFatture), pager, primoConto);
                }
                if (getIntent().getBooleanExtra("OopsPage", false)) {
                    // far apparire la finestrella nera e rifare la chiamata
                    // chiamata download
                    contenuto = (LinearLayout) findViewById(R.id.contenuto);
                    contenuto.setClickable(false);
                    waiting_download = (LinearLayout) findViewById(R.id.waiting_download);
                    waiting_download.setVisibility(View.VISIBLE);
                    TextView download_status = (TextView) findViewById(R.id.download_status);
                    download_status.setText("download in corso");
                    TextView download_file = (TextView) findViewById(R.id.download_file);
                    download_file.setText("blt_" + getIntent().getStringExtra("NumeroFattura") + ".pdf");
                    Button download_apri = (Button) findViewById(R.id.download_apri);
                    download_apri.setVisibility(View.GONE);

                    //Baccus
                    Button download_chiudi = (Button) findViewById(R.id.download_chiudi);
                    download_chiudi.setVisibility(View.GONE);

                    animation = Utilities.animation(this);
                    imageAnim = (ImageView) findViewById(R.id.waiting_anim);
                    imageAnim.setVisibility(View.VISIBLE);
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                        imageAnim.setBackgroundDrawable(animation);
                    } else {
                        imageAnim.setBackground(animation);
                    }
                    animation.start();

                    if (!isNetworkAvailable(context)) {
                        Intent intentOops = new Intent(getApplicationContext(), OopsPageBollettePagamentiDownloadBollettaIntervallo.class);
                        intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.connessione_assente));
                        intentOops.putExtra("ID_PDF", getIntent().getStringExtra("ID_PDF"));
                        intentOops.putExtra("NumeroFattura", getIntent().getStringExtra("NumeroFattura"));
                        intentOops.putExtra("isDownload", true);
                        intentOops.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
                        intentOops.putExtra("ORIGINE", Constants.ORIGINE_BOLLETTE_PAGAMENTI_RICERCA_POST_INTERVALLO);
                        intentOops.putExtra("DATA_INIZIO", getIntent().getStringExtra("DATA_INIZIO"));
                        intentOops.putExtra("DATA_FINE", getIntent().getStringExtra("DATA_FINE"));
                        intentOops.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
                        BollettePagamentiRicercaPostIntervallo.this.finish();
                        startActivity(intentOops);
                        if (animation != null) {
                            animation.stop();
                            imageAnim.setVisibility(View.INVISIBLE);
                        }
                        overridePendingTransition(R.anim.fade, R.anim.hold);
                    } else {
                        /********************************** Chiamata Download Pdf *********************************************/
                        ServiceCall downloadServiceCall = ServiceCallFactory.createServiceCall(context);
                        // downloadServiceCall.myCookieStore = new
                        // PersistentCookieStore(context);

                        HashMap<String, String> parametri = new HashMap<String, String>();
                        parametri.put("idProtocolloDocPdf", getIntent().getStringExtra("ID_PDF"));

                        try {
                            downloadServiceCall.executeHttpsGetBollettePagamentiRicercaPostIntervallo(Constants.URL_DOWNLOAD_PDF, parametri, BollettePagamentiRicercaPostIntervallo.this, null, true, getIntent().getStringExtra("NumeroFattura"));
                        } catch (Exception e) {
                            Intent intentOops = new Intent(getApplicationContext(), OopsPageBollettePagamentiDownloadBollettaIntervallo.class);
                            intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
                            intentOops.putExtra("ID_PDF", getIntent().getStringExtra("ID_PDF"));
                            intentOops.putExtra("NumeroFattura", getIntent().getStringExtra("NumeroFattura"));
                            intentOops.putExtra("isDownload", true);
                            intentOops.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
                            intentOops.putExtra("ORIGINE", Constants.ORIGINE_BOLLETTE_PAGAMENTI_RICERCA_POST_INTERVALLO);
                            intentOops.putExtra("DATA_INIZIO", getIntent().getStringExtra("DATA_INIZIO"));
                            intentOops.putExtra("DATA_FINE", getIntent().getStringExtra("DATA_FINE"));
                            intentOops.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
                            BollettePagamentiRicercaPostIntervallo.this.finish();
                            startActivity(intentOops);
                            overridePendingTransition(R.anim.fade, R.anim.hold);
                        }
                    }

                    /**************************************************************************************/
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void popolaPaginaContenuto(JSONArray rispostaShowListaFattureJSON, LinearLayout pager, final String primoConto) {
        try {
            pager.removeAllViews();
            if (rispostaShowListaFattureJSON.length() > 2) {
                JSONArray listaFatture = rispostaShowListaFattureJSON.getJSONArray(2);
                for (int i = 1; i <= listaFatture.length(); i++) {
                    JSONObject fatturaIesima = listaFatture.getJSONObject(i - 1);
                    View bollette_pagamenti_ricerca_entry = inflater.inflate(R.layout.bollette_pagamenti_ricerca_entry, null);

                    RelativeLayout bolletta = (RelativeLayout) bollette_pagamenti_ricerca_entry.findViewById(R.id.bolletta);
                    if (i % 2 == 0) {
                        bolletta.setBackgroundResource(R.drawable.info_bolletta_black);
                    } else {
                        bolletta.setBackgroundResource(R.drawable.info_bolletta_white);
                    }
                    TextView bolletta_n_valore = (TextView) bollette_pagamenti_ricerca_entry.findViewById(R.id.bolletta_n_valore);
                    bolletta_n_valore.setText(fatturaIesima.getString("NumeroFattura"));

                    TextView bolletta_n_stato = (TextView) bollette_pagamenti_ricerca_entry.findViewById(R.id.bolletta_n_stato);
                    String statoFattura = fatturaIesima.getString("StatoPagamento");
                    bolletta_n_stato.setText(statoFattura);
                    if (statoFattura.equals("PAGATA")) {
                        bolletta_n_stato.setTextColor(getResources().getColor(R.color.green1));
                    } else {
                        bolletta_n_stato.setTextColor(getResources().getColor(R.color.red1));
                    }

                    TextView bolletta_del_data = (TextView) bollette_pagamenti_ricerca_entry.findViewById(R.id.bolletta_del_data);
                    bolletta_del_data.setText(fatturaIesima.getString("DataEmissione"));

                    TextView bolletta_scadenza_data = (TextView) bollette_pagamenti_ricerca_entry.findViewById(R.id.bolletta_scadenza_data);
                    bolletta_scadenza_data.setText(fatturaIesima.getString("Scadenza"));

                    TextView bolletta_importo_valore = (TextView) bollette_pagamenti_ricerca_entry.findViewById(R.id.bolletta_importo_valore);
                    bolletta_importo_valore.setText(fatturaIesima.getString("Importo").concat(getResources().getString(R.string.euro)));

                    TextView bolletta_da_pagare_valore = (TextView) bollette_pagamenti_ricerca_entry.findViewById(R.id.bolletta_da_pagare_valore);
                    bolletta_da_pagare_valore.setText(fatturaIesima.getString("ImportoDaPagare").concat(getResources().getString(R.string.euro)));

                    ImageView bollette_pagamenti_pdf_icon = (ImageView) bollette_pagamenti_ricerca_entry.findViewById(R.id.bollette_pagamenti_pdf_icon);
                    String statoWeb = fatturaIesima.getString("StatoDocFatturaWeb");
                    if (statoWeb.equals("OFFLINE")) {
                        bollette_pagamenti_pdf_icon.setBackgroundResource(R.drawable.pdf_icon_no);
                    } else if (statoWeb.equals("ONLINE")) {
                        bollette_pagamenti_pdf_icon.setBackgroundResource(R.drawable.pdf_icon);
                        final String IdProtocolloDocPdf = fatturaIesima.getString("IdProtocolloDocPdf");
                        final String numeroFattura = fatturaIesima.getString("NumeroFattura");
                        bollette_pagamenti_pdf_icon.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View v) {
                                // chiamata download
                                contenuto = (LinearLayout) findViewById(R.id.contenuto);
                                contenuto.setClickable(false);
                                waiting_download = (LinearLayout) findViewById(R.id.waiting_download);
                                waiting_download.setVisibility(View.VISIBLE);
                                TextView download_status = (TextView) findViewById(R.id.download_status);
                                download_status.setText("download in corso");
                                TextView download_file = (TextView) findViewById(R.id.download_file);
                                download_file.setText("blt_" + numeroFattura + ".pdf");

                                Button download_apri = (Button) findViewById(R.id.download_apri);
                                download_apri.setVisibility(View.GONE);

                                Button download_chiudi = (Button) findViewById(R.id.download_chiudi);
                                download_chiudi.setVisibility(View.GONE);


                                animation = Utilities.animation(BollettePagamentiRicercaPostIntervallo.this);
                                imageAnim = (ImageView) findViewById(R.id.waiting_anim);
                                imageAnim.setVisibility(View.VISIBLE);
                                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                                    imageAnim.setBackgroundDrawable(animation);
                                } else {
                                    imageAnim.setBackground(animation);
                                }
                                animation.start();

                                if (!isNetworkAvailable(context)) {
                                    Intent intentOops = new Intent(getApplicationContext(), OopsPageBollettePagamentiDownloadBollettaIntervallo.class);
                                    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.connessione_assente));
                                    intentOops.putExtra("ID_PDF", IdProtocolloDocPdf);
                                    intentOops.putExtra("isDownload", true);
                                    intentOops.putExtra("NumeroFattura", numeroFattura);
                                    intentOops.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
                                    intentOops.putExtra("ORIGINE", Constants.ORIGINE_BOLLETTE_PAGAMENTI_RICERCA_POST_INTERVALLO);
                                    intentOops.putExtra("DATA_INIZIO", getIntent().getStringExtra("DATA_INIZIO"));
                                    intentOops.putExtra("DATA_FINE", getIntent().getStringExtra("DATA_FINE"));
                                    intentOops.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
                                    BollettePagamentiRicercaPostIntervallo.this.finish();
                                    startActivity(intentOops);
                                    if (animation != null) {
                                        animation.stop();
                                        imageAnim.setVisibility(View.INVISIBLE);
                                    }
                                    overridePendingTransition(R.anim.fade, R.anim.hold);
                                } else {
                                    /********************************** Chiamata Download Pdf *********************************************/
                                    ServiceCall downloadServiceCall = ServiceCallFactory.createServiceCall(context);
                                    // downloadServiceCall.myCookieStore = new
                                    // PersistentCookieStore(context);

                                    HashMap<String, String> parametri = new HashMap<String, String>();
                                    parametri.put("idProtocolloDocPdf", IdProtocolloDocPdf);

                                    try {
                                        downloadServiceCall.executeHttpsGetBollettePagamentiRicercaPostIntervallo(Constants.URL_DOWNLOAD_PDF, parametri, BollettePagamentiRicercaPostIntervallo.this, null, true, numeroFattura);
                                    } catch (Exception e) {
                                        Intent intentOops = new Intent(getApplicationContext(), OopsPageBollettePagamentiDownloadBollettaIntervallo.class);
                                        intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
                                        intentOops.putExtra("ID_PDF", IdProtocolloDocPdf);
                                        intentOops.putExtra("isDownload", true);
                                        intentOops.putExtra("NumeroFattura", numeroFattura);
                                        intentOops.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
                                        intentOops.putExtra("ORIGINE", Constants.ORIGINE_BOLLETTE_PAGAMENTI_RICERCA_POST_INTERVALLO);
                                        intentOops.putExtra("DATA_INIZIO", getIntent().getStringExtra("DATA_INIZIO"));
                                        intentOops.putExtra("DATA_FINE", getIntent().getStringExtra("DATA_FINE"));
                                        intentOops.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
                                        BollettePagamentiRicercaPostIntervallo.this.finish();
                                        startActivity(intentOops);
                                        overridePendingTransition(R.anim.fade, R.anim.hold);
                                    }
                                }

                                /**************************************************************************************/
                            }
                        });
                    }

                    pager.addView(bollette_pagamenti_ricerca_entry);
                }

            } else {
                RelativeLayout stato_elenco_bollette_lista_vuota = (RelativeLayout) findViewById(R.id.stato_elenco_bollette_lista_vuota);
                stato_elenco_bollette_lista_vuota.setVisibility(View.VISIBLE);
            }

            View bollette_pagamenti_ricerca_banner_guida_bolletta = inflater.inflate(R.layout.bollette_pagamenti_ricerca_banner_guida_bolletta, null);
            bollette_pagamenti_ricerca_banner_guida_bolletta.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Intent intentHelp = new Intent(getApplicationContext(), GuidaBolletta.class);
                    intentHelp.putExtra("ORIGINE", Constants.ORIGINE_BOLLETTE_PAGAMENTI_RICERCA_POST_INTERVALLO);
                    intentHelp.putExtra("PRIMOCONTO", primoConto);
                    intentHelp.putExtra("DATA_INIZIO", dataInizio);
                    intentHelp.putExtra("DATA_FINE", dataFine);
                    intentHelp.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
                    BollettePagamentiRicercaPostIntervallo.this.finish();
                    startActivity(intentHelp);
                    overridePendingTransition(R.anim.fade, R.anim.hold);
                }
            });
            pager.addView(bollette_pagamenti_ricerca_banner_guida_bolletta);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void postChiamata(String rispostaChiamata, List<Cookie> lista, HashMap<String, String> parametri, boolean isDownload, String numeroFattura) {
        if (animation != null) {
            animation.stop();
            imageAnim.setVisibility(View.INVISIBLE);
        }

        final ImageView indietro = (ImageView) findViewById(R.id.bollette_pagamenti_indietro);
        indietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
        indietro.getBackground().invalidateSelf();

        JSONArray rispostaJSON = new JSONArray();
        String esito = "";
        try {
            rispostaJSON = new JSONArray(rispostaChiamata);
            JSONObject oggettoRisposta = (JSONObject) rispostaJSON.get(0);
            esito = oggettoRisposta.getString("esito");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (esito.equals("200")) {

            if (isDownload) {
                // download
                Constants.SERVIZIO_DOWNLOAD_PDF = rispostaChiamata;
                TextView download_status = (TextView) findViewById(R.id.download_status);
                download_status.setText("download completato");
                if (animation != null) {
                    animation.stop();
                    imageAnim.setVisibility(View.GONE);
                }
                final Button download_apri = (Button) findViewById(R.id.download_apri);
                download_apri.setVisibility(View.VISIBLE);
                download_apri.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
                download_apri.getBackground().invalidateSelf();
                //Baccus
                //Baccus
                final Button download_chiudi = (Button) findViewById(R.id.download_chiudi);
                download_chiudi.setVisibility(View.VISIBLE);
                download_chiudi.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
                download_chiudi.getBackground().invalidateSelf();
                download_chiudi.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        download_chiudi.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
                        download_chiudi.getBackground().invalidateSelf();
                        waiting_download.setVisibility(View.GONE);
                    }
                });


                download_apri.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        download_apri.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
                        download_apri.getBackground().invalidateSelf();
                        String pdfString = "";
                        try {
                            JSONArray rispostaPDFJSON = new JSONArray(Constants.SERVIZIO_DOWNLOAD_PDF);
                            JSONObject pdfJSON = rispostaPDFJSON.getJSONObject(1);
                            pdfString = pdfJSON.getString("Pdf");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        byte[] pdfAsBytes = Base64.decode(pdfString, 0);

                        File fileDirectory = new File(Environment.getExternalStorageDirectory() + "/ENI");
                        File filePath = new File(Environment.getExternalStorageDirectory() + "/ENI/enibolletta.pdf");
                        if (!fileDirectory.exists()) {
                            boolean makedDir = fileDirectory.mkdir();
                            if (makedDir) {
                                // filePath = new
                                // File(Environment.getExternalStorageDirectory()+"/ENI/enibolletta.pdf");
                                if (filePath.length() != 0) {
                                    boolean deleted = filePath.delete();
                                    if (deleted) {
                                        FileOutputStream os;
                                        try {
                                            os = new FileOutputStream(filePath, true);
                                            os.write(pdfAsBytes);
                                            os.close();
                                        } catch (FileNotFoundException e) {
                                            e.printStackTrace();
                                        } catch (IOException e1) {
                                            e1.printStackTrace();
                                        }
                                    }
                                } else {
                                    FileOutputStream os;
                                    try {
                                        os = new FileOutputStream(filePath, true);
                                        os.write(pdfAsBytes);
                                        os.close();
                                    } catch (FileNotFoundException e) {
                                        e.printStackTrace();
                                    } catch (IOException e1) {
                                        e1.printStackTrace();
                                    }
                                }
                            }
                        } else {
                            if (filePath.length() != 0) {
                                boolean deleted = filePath.delete();
                                if (deleted) {
                                    FileOutputStream os;
                                    try {
                                        os = new FileOutputStream(filePath, true);
                                        os.write(pdfAsBytes);
                                        os.close();
                                    } catch (FileNotFoundException e) {
                                        e.printStackTrace();
                                    } catch (IOException e1) {
                                        e1.printStackTrace();
                                    }
                                }
                            } else {
                                FileOutputStream os;
                                try {
                                    os = new FileOutputStream(filePath, true);
                                    os.write(pdfAsBytes);
                                    os.close();
                                } catch (FileNotFoundException e) {
                                    e.printStackTrace();
                                } catch (IOException e1) {
                                    e1.printStackTrace();
                                }
                            }
                        }

                        // apertura
                        Uri selectedPdfUri = Uri.fromFile(filePath);
                        viewPdf(selectedPdfUri);

                    }
                });
            } else {
                // chiamata agli ultimi 2 anni
                Constants.SERVIZIO_SHOW_LISTA_FATTURE = rispostaChiamata;
                Intent intentBollettePagamenti = new Intent(getApplicationContext(), BollettePagamenti.class);
                intentBollettePagamenti.putExtra("PRIMOCONTO", parametri.get("codiceContoCliente"));
                intentBollettePagamenti.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
                BollettePagamentiRicercaPostIntervallo.this.finish();
                startActivity(intentBollettePagamenti);
                overridePendingTransition(R.anim.fade, R.anim.hold);
            }

        } else {
            Intent intentOops = new Intent(getApplicationContext(), OopsPageBollettePagamentiDownloadBollettaIntervallo.class);
            if (isDownload) {
                intentOops.putExtra("ID_PDF", parametri.get("idProtocolloDocPdf"));
                intentOops.putExtra("NumeroFattura", numeroFattura);
                intentOops.putExtra("isDownload", true);
                intentOops.putExtra("PRIMOCONTO", primoConto);
                intentOops.putExtra("ORIGINE", Constants.ORIGINE_BOLLETTE_PAGAMENTI_RICERCA_POST_INTERVALLO);
                intentOops.putExtra("DATA_INIZIO", getIntent().getStringExtra("DATA_INIZIO"));
                intentOops.putExtra("DATA_FINE", getIntent().getStringExtra("DATA_FINE"));
                intentOops.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
                // errori ops page
                if (esito.equals("309")) {
                    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
                    BollettePagamentiRicercaPostIntervallo.this.finish();
                    startActivity(intentOops);
                    overridePendingTransition(R.anim.fade, R.anim.hold);

                } else if (esito.equals("400")) {
                    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
                    BollettePagamentiRicercaPostIntervallo.this.finish();
                    startActivity(intentOops);
                    overridePendingTransition(R.anim.fade, R.anim.hold);

                } else if (esito.equals("420")) {
                    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
                    BollettePagamentiRicercaPostIntervallo.this.finish();
                    startActivity(intentOops);
                    overridePendingTransition(R.anim.fade, R.anim.hold);

                } else if (esito.equals("449")) {
                    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
                    BollettePagamentiRicercaPostIntervallo.this.finish();
                    startActivity(intentOops);
                    overridePendingTransition(R.anim.fade, R.anim.hold);

                } else if (esito.equals("451")) {
                    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_popup_451));
                    BollettePagamentiRicercaPostIntervallo.this.finish();
                    startActivity(intentOops);
                    overridePendingTransition(R.anim.fade, R.anim.hold);
                }
                // errori popup
                else if (esito.equals("307")) {
                    Utils.popup(BollettePagamentiRicercaPostIntervallo.this, getResources().getString(R.string.errore_popup_307), "Riprova");

                } else if (esito.equals("435")) {
                    Utils.popup(BollettePagamentiRicercaPostIntervallo.this, getResources().getString(R.string.errore_popup_435), "Riprova");

                } else if (esito.equals("437")) {
                    Utils.popup(BollettePagamentiRicercaPostIntervallo.this, getResources().getString(R.string.errore_popup_437), "Riprova");

                } else if (esito.equals("438")) {
                    Utils.popup(BollettePagamentiRicercaPostIntervallo.this, getResources().getString(R.string.errore_popup_438), "Riprova");

                } else if (esito.equals("443")) {
                    Utils.popup(BollettePagamentiRicercaPostIntervallo.this, getResources().getString(R.string.errore_popup_443), "Riprova");

                } else if (esito.equals("444")) {
                    Utils.popup(BollettePagamentiRicercaPostIntervallo.this, getResources().getString(R.string.errore_popup_444), "Riprova");

                } else if (esito.equals("445")) {
                    Utils.popup(BollettePagamentiRicercaPostIntervallo.this, getResources().getString(R.string.errore_popup_445), "Riprova");

                } else if (esito.equals("446")) {
                    Utils.popup(BollettePagamentiRicercaPostIntervallo.this, getResources().getString(R.string.errore_popup_446), "Riprova");

                } else if (esito.equals("447")) {
                    Utils.popup(BollettePagamentiRicercaPostIntervallo.this, getResources().getString(R.string.errore_popup_447), "Riprova");

                } else {
                    if (esito.equals("")) {
                        intentOops = new Intent(getApplicationContext(), OopsPageSessioneScaduta.class);
                        intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.sessione_scaduta));
                        startActivity(intentOops);
                        overridePendingTransition(R.anim.fade, R.anim.hold);
                    }
                }
            } else {
                // chiamata degli ultimi 2 anni
                intentOops.putExtra("CODICE_CLIENTE", parametri.get("CODICE_CLIENTE"));
                intentOops.putExtra("CODICE_CONTO_CLIENTE", parametri.get("CODICE_CONTO_CLIENTE"));
                // errori ops page
                if (esito.equals("309")) {
                    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
                    startActivity(intentOops);
                    overridePendingTransition(R.anim.fade, R.anim.hold);

                } else if (esito.equals("400")) {
                    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
                    startActivity(intentOops);
                    overridePendingTransition(R.anim.fade, R.anim.hold);

                } else if (esito.equals("420")) {
                    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
                    startActivity(intentOops);
                    overridePendingTransition(R.anim.fade, R.anim.hold);

                } else if (esito.equals("449")) {
                    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
                    startActivity(intentOops);
                    overridePendingTransition(R.anim.fade, R.anim.hold);

                } else if (esito.equals("451")) {
                    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_popup_451));
                    startActivity(intentOops);
                    overridePendingTransition(R.anim.fade, R.anim.hold);
                }
                // errori popup
                else if (esito.equals("307")) {
                    Utils.popup(BollettePagamentiRicercaPostIntervallo.this, getResources().getString(R.string.errore_popup_307), "Riprova");

                } else if (esito.equals("435")) {
                    Utils.popup(BollettePagamentiRicercaPostIntervallo.this, getResources().getString(R.string.errore_popup_435), "Riprova");

                } else if (esito.equals("437")) {
                    Utils.popup(BollettePagamentiRicercaPostIntervallo.this, getResources().getString(R.string.errore_popup_437), "Riprova");

                } else if (esito.equals("438")) {
                    Utils.popup(BollettePagamentiRicercaPostIntervallo.this, getResources().getString(R.string.errore_popup_438), "Riprova");

                } else if (esito.equals("443")) {
                    Utils.popup(BollettePagamentiRicercaPostIntervallo.this, getResources().getString(R.string.errore_popup_443), "Riprova");

                } else if (esito.equals("444")) {
                    Utils.popup(BollettePagamentiRicercaPostIntervallo.this, getResources().getString(R.string.errore_popup_444), "Riprova");

                } else if (esito.equals("445")) {
                    Utils.popup(BollettePagamentiRicercaPostIntervallo.this, getResources().getString(R.string.errore_popup_445), "Riprova");

                } else if (esito.equals("446")) {
                    Utils.popup(BollettePagamentiRicercaPostIntervallo.this, getResources().getString(R.string.errore_popup_446), "Riprova");

                } else if (esito.equals("447")) {
                    Utils.popup(BollettePagamentiRicercaPostIntervallo.this, getResources().getString(R.string.errore_popup_447), "Riprova");

                }
            }
        }

    }

    public void postChiamataException(boolean exception, HashMap<String, String> parameterMap, boolean isDownload, String numeroFattura) {
        if (exception) {
            if (animation != null) {
                animation.stop();
                imageAnim.setVisibility(View.INVISIBLE);
            }

            final ImageView indietro = (ImageView) findViewById(R.id.bollette_pagamenti_indietro);
            indietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
            indietro.getBackground().invalidateSelf();

            Intent intentOops = new Intent(getApplicationContext(), OopsPageBollettePagamentiDownloadBollettaIntervallo.class);
            if (isDownload) {
                intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
                intentOops.putExtra("ID_PDF", parameterMap.get("idProtocolloDocPdf"));
                intentOops.putExtra("isDownload", true);
                intentOops.putExtra("NumeroFattura", numeroFattura);
                intentOops.putExtra("PRIMOCONTO", primoConto);
                intentOops.putExtra("ORIGINE", Constants.ORIGINE_BOLLETTE_PAGAMENTI_RICERCA_POST_INTERVALLO);
                intentOops.putExtra("DATA_INIZIO", getIntent().getStringExtra("DATA_INIZIO"));
                intentOops.putExtra("DATA_FINE", getIntent().getStringExtra("DATA_FINE"));
                intentOops.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
                BollettePagamentiRicercaPostIntervallo.this.finish();
                startActivity(intentOops);
                overridePendingTransition(R.anim.fade, R.anim.hold);
            } else {
                intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
                intentOops.putExtra("CODICE_CLIENTE", parameterMap.get("CODICE_CLIENTE"));
                intentOops.putExtra("CODICE_CONTO_CLIENTE", parameterMap.get("CODICE_CONTO_CLIENTE"));
                // BollettePagamentiRicercaPostIntervallo.this.finish();
                startActivity(intentOops);
                overridePendingTransition(R.anim.fade, R.anim.hold);

            }

        }
    }

    private void viewPdf(Uri file) {
        Intent intent;
        intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(file, "application/pdf");
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("OopsPage", false);
        try {
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            // No application to view, ask to download one
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Nessuna Applicazione Trovata");
            builder.setMessage("Scarica applicazione da Google Play?");
            builder.setPositiveButton("Si, Prego", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Intent marketIntent = new Intent(Intent.ACTION_VIEW);
                    marketIntent.setData(Uri.parse("market://details?id=com.adobe.reader"));
                    startActivity(marketIntent);
                }
            });
            builder.setNegativeButton("No, Grazie", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    waiting_download.setVisibility(View.GONE);
                    contenuto.setClickable(true);
                }
            });
            builder.create().show();
        }
    }

    private boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager == null) return false;
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }

    public String getCodiceCliente(String rispostaLogin) {
        String result = "";
        try {
            JSONArray loginJson = new JSONArray(rispostaLogin);
            JSONObject datiCliente = (JSONObject) loginJson.get(1);
            result = datiCliente.getString("CodiceCliente");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public void onBackPressed() {

        animation = Utilities.animation(this);
        imageAnim = (ImageView) findViewById(R.id.waiting_anim);
        imageAnim.setVisibility(View.VISIBLE);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
            imageAnim.setBackgroundDrawable(animation);
        } else {
            imageAnim.setBackground(animation);
        }
        animation.start();

        // si va alla pagina BollettePagamenti
        // devo fare la chiamata per sovrascrivere gli ultimi 2 anni
        String codiceCliente = getCodiceCliente(rispostaLogin);
        if (!isNetworkAvailable(context)) {
            Intent intentOops = new Intent(getApplicationContext(), OopsPageBollettePagamentiDownloadBollettaIntervallo.class);
            intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.connessione_assente));
            intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
            intentOops.putExtra("CODICE_CONTO_CLIENTE", primoConto);
            // BollettePagamentiRicercaPostIntervallo.this.finish();
            startActivity(intentOops);
            if (animation != null) {
                animation.stop();
                imageAnim.setVisibility(View.INVISIBLE);
            }
            overridePendingTransition(R.anim.fade, R.anim.hold);
        } else {
            /********************* Chiamata show lista forniture ******************************/
            ServiceCall listaFornitureServiceCall = ServiceCallFactory.createServiceCall(context);
            // listaFornitureServiceCall.myCookieStore = new
            // PersistentCookieStore(context);

            HashMap<String, String> parametri = new HashMap<String, String>();
            parametri.put("codiceCliente", codiceCliente);
            parametri.put("codiceContoCliente", primoConto);

            try {
                listaFornitureServiceCall.executeHttpsGetBollettePagamentiRicercaPostIntervallo(Constants.URL_SHOW_LISTA_FATTURE, parametri, BollettePagamentiRicercaPostIntervallo.this, null, false, "");
            } catch (Exception e) {
                Intent intentOops = new Intent(getApplicationContext(), OopsPageBollettePagamentiDownloadBollettaIntervallo.class);
                intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
                intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
                intentOops.putExtra("CODICE_CONTO_CLIENTE", primoConto);
                // BollettePagamentiRicercaPostIntervallo.this.finish();
                startActivity(intentOops);
                overridePendingTransition(R.anim.fade, R.anim.hold);
            }
        }
    }

    public void onResume() {
        super.onResume();
        if (!getIntent().getBooleanExtra("OopsPage", false)) {
            if (waiting_download != null) {
                waiting_download.setVisibility(View.GONE);
            }
        }
    }
}
