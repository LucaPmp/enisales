package com.eni.enigaseluce.bollettePagamenti;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.http.cookie.Cookie;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.AnimationDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.eni.enigaseluce.R;
import com.eni.enigaseluce.error.OopsPageBollettePagamentiRicercaIntervallo;
import com.eni.enigaseluce.error.OopsPageSessioneScaduta;
import com.eni.enigaseluce.error.Utils;
import com.eni.enigaseluce.httpcall.Constants;
import com.eni.enigaseluce.httpcall.ServiceCall;
import com.eni.enigaseluce.httpcall.ServiceCallFactory;
import com.eni.enigaseluce.utils.Utilities;

@SuppressLint("NewApi")
public class BollettePagamentiRicercaIntervallo extends Activity {
    private Context context;
    private static Context staticContext;
    AnimationDrawable animation;
    ImageView imageAnim;

    public String rispostaLogin = Constants.SERVIZIO_LOGIN;
    public String rispostaShowListaFatture = Constants.SERVIZIO_SHOW_LISTA_FATTURE;

    public static String nomeInteroCliente = "";
    static LayoutInflater inflater;
    String codiceEsito;
    String primoConto;
    String origine;

    String[] arrayMesi = { "Gen", "Feb", "Mar", "Apr", "Mag", "Giu", "Lug", "Ago", "Set", "Ott", "Nov", "Dic" };
    int numeroAnni = 10;
    int[] arrayAnni = new int[numeroAnni];

    public static Context getContext() {
	return BollettePagamentiRicercaIntervallo.staticContext;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.bollette_pagamenti_ricerca_intervallo);

	for (int i = 0; i < numeroAnni; i++) {
	    arrayAnni[i] = 2012 - (numeroAnni - (i + 1));
	}

	context = this;
	BollettePagamentiRicercaIntervallo.staticContext = this;
	primoConto = getIntent().getStringExtra("PRIMOCONTO");
	origine = getIntent().getStringExtra("ORIGINE");

	impostaDatePickers();
    }

    @Override
    protected void onStart() {
	super.onStart();

	final ImageView indietro = (ImageView) findViewById(R.id.bollette_pagamenti_indietro);
	indietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
	indietro.getBackground().invalidateSelf();
	indietro.setOnClickListener(new View.OnClickListener() {
	    public void onClick(View v) {
		indietro.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
		indietro.getBackground().invalidateSelf();

		animation = Utilities.animation(BollettePagamentiRicercaIntervallo.this);
		imageAnim = (ImageView) findViewById(R.id.waiting_anim);
		imageAnim.setVisibility(View.VISIBLE);
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
		    imageAnim.setBackgroundDrawable(animation);
		}
		else {
		    imageAnim.setBackground(animation);
		}
		animation.start();

		if (origine.equals(Constants.ORIGINE_BOLLETTE_PAGAMENTI_RICERCA_POST_INTERVALLO)) {
		    Intent intentBollettePagamentiPostIntervallo = new Intent(getApplicationContext(), BollettePagamentiRicercaPostIntervallo.class);
		    intentBollettePagamentiPostIntervallo.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
		    intentBollettePagamentiPostIntervallo.putExtra("DATA_INIZIO", getIntent().getStringExtra("DATA_INIZIO"));
		    intentBollettePagamentiPostIntervallo.putExtra("DATA_FINE", getIntent().getStringExtra("DATA_FINE"));
		    intentBollettePagamentiPostIntervallo.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
		    BollettePagamentiRicercaIntervallo.this.finish();
		    startActivity(intentBollettePagamentiPostIntervallo);
		    overridePendingTransition(R.anim.fade, R.anim.hold);
		}
		else if (origine.equals(Constants.ORIGINE_BOLLETTE_PAGAMENTI_RICERCA)) {
		    // rifaccio la chiamata degli ultimi 2 anni
		    String codiceCliente = getCodiceCliente(rispostaLogin);
		    if (!isNetworkAvailable(context)) {
			Intent intentOops = new Intent(getApplicationContext(), OopsPageBollettePagamentiRicercaIntervallo.class);
			intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.connessione_assente));
			intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
			intentOops.putExtra("CODICE_CONTO_CLIENTE", primoConto);
			// Home.this.finish();
			startActivity(intentOops);
			if (animation != null) {
			    animation.stop();
			    imageAnim.setVisibility(View.INVISIBLE);
			}
			overridePendingTransition(R.anim.fade, R.anim.hold);
		    }
		    else {
			/********************* Chiamata show Lista fatture ******************************/
			ServiceCall stAvFornServiceCall = ServiceCallFactory.createServiceCall(context);
			// stAvFornServiceCall.myCookieStore = new
			// PersistentCookieStore(context);

			HashMap<String, String> parametri = new HashMap<String, String>();
			parametri.put("codiceCliente", codiceCliente);
			parametri.put("codiceContoCliente", primoConto);
			try {
			    stAvFornServiceCall.executeHttpsGetBollettePagamentiRicercaIntervallo(Constants.URL_SHOW_LISTA_FATTURE, parametri, BollettePagamentiRicercaIntervallo.this, null);
			}
			catch (Exception e) {
			    Intent intentOops = new Intent(getApplicationContext(), OopsPageBollettePagamentiRicercaIntervallo.class);
			    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
			    intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
			    intentOops.putExtra("CODICE_CONTO_CLIENTE", primoConto);
			    // BollettePagamentiRicercaIntervallo.this.finish();
			    startActivity(intentOops);
			    overridePendingTransition(R.anim.fade, R.anim.hold);
			}

		    }
		}
	    }
	});
	/*
	 * final ImageView help =
	 * (ImageView)findViewById(R.id.bollette_pagamenti_help);
	 * help.getBackground().setColorFilter(0xFFFFFFFF,
	 * PorterDuff.Mode.MULTIPLY); help.getBackground().invalidateSelf();
	 * help.setOnClickListener(new View.OnClickListener() { public void
	 * onClick(View v) { help.getBackground().setColorFilter(0xFF999999,
	 * PorterDuff.Mode.MULTIPLY); help.getBackground().invalidateSelf();
	 * animation = new AnimationDrawable();
	 * animation.addFrame(getResources().getDrawable(R.drawable.anim_load2),
	 * 300);
	 * animation.addFrame(getResources().getDrawable(R.drawable.anim_load3),
	 * 300);
	 * animation.addFrame(getResources().getDrawable(R.drawable.anim_load4),
	 * 300); animation.setOneShot(false); imageAnim = (ImageView)
	 * findViewById(R.id.waiting_bollette_pagamenti_ricerca_intervallo);
	 * imageAnim.setVisibility(View.VISIBLE); if(Build.VERSION.SDK_INT <
	 * 16){ imageAnim.setBackgroundDrawable(animation); }else{
	 * imageAnim.setBackground(animation); } // start the animation!
	 * animation.start();
	 * 
	 * Intent intentHelp = new Intent(getApplicationContext(),
	 * BollettePagamentiHelp.class); intentHelp.putExtra("ORIGINE",
	 * Constants.ORIGINE_BOLLETTE_PAGAMENTI_RICERCA_INTERVALLO);
	 * intentHelp.putExtra("origine", origine);
	 * intentHelp.putExtra("PRIMOCONTO", primoConto);
	 * intentHelp.putExtra("DATA_INIZIO",
	 * getIntent().getStringExtra("DATA_INIZIO"));
	 * intentHelp.putExtra("DATA_FINE",
	 * getIntent().getStringExtra("DATA_FINE"));
	 * intentHelp.putExtra("INDICE_SPINNER",
	 * getIntent().getIntExtra("INDICE_SPINNER",0));
	 * BollettePagamentiRicercaIntervallo.this.finish();
	 * startActivity(intentHelp); overridePendingTransition(R.anim.fade,
	 * R.anim.hold);
	 * 
	 * } });
	 */
	try {
	    final JSONArray bollettePagamentiLogin = new JSONArray(rispostaLogin);
	    JSONObject esito = bollettePagamentiLogin.getJSONObject(0);
	    codiceEsito = (String) esito.getString("esito");
	    if (codiceEsito.equals("200")) {
		// JSONObject datiAnagr =
		// bollettePagamentiLogin.getJSONObject(1);
		TextView codice_cliente = (TextView) findViewById(R.id.codice_cliente_valore);
		codice_cliente.setText(primoConto);

		// JSONArray conti = bollettePagamentiLogin.getJSONArray(2);
		String statoConto = statoConto(bollettePagamentiLogin, primoConto);
		TextView stato_cliente = (TextView) findViewById(R.id.stato_cliente_valore);
		stato_cliente.setText(statoConto);

		ImageView bollette_pagamenti_cerca_btn = (ImageView) findViewById(R.id.conferma_btn);

		ImageView da_mese_up = (ImageView) findViewById(R.id.da_mese_up);
		da_mese_up.setOnClickListener(new View.OnClickListener() {

		    public void onClick(View v) {
			//meseInizioUp();
				meseInizioDown();
		    }
		});

		ImageView da_anno_up = (ImageView) findViewById(R.id.da_anno_up);
		da_anno_up.setOnClickListener(new View.OnClickListener() {

		    public void onClick(View v) {
			//annoInizioUp();
				annoInizioDown();
		    }
		});

		ImageView a_mese_up = (ImageView) findViewById(R.id.a_mese_up);
		a_mese_up.setOnClickListener(new View.OnClickListener() {

		    public void onClick(View v) {
			//meseFineUp();
				meseFineDown();
		    }
		});

		ImageView a_anno_up = (ImageView) findViewById(R.id.a_anno_up);
		a_anno_up.setOnClickListener(new View.OnClickListener() {

		    public void onClick(View v) {
			//annoFineUp();
				annoFineDown();
		    }
		});

		ImageView da_mese_down = (ImageView) findViewById(R.id.da_mese_down);
		da_mese_down.setOnClickListener(new View.OnClickListener() {

		    public void onClick(View v) {
			//meseInizioDown();
				meseInizioUp();
		    }
		});

		ImageView da_anno_down = (ImageView) findViewById(R.id.da_anno_down);
		da_anno_down.setOnClickListener(new View.OnClickListener() {

		    public void onClick(View v) {
			//annoInizioDown();
				annoInizioUp();
		    }
		});

		ImageView a_mese_down = (ImageView) findViewById(R.id.a_mese_down);
		a_mese_down.setOnClickListener(new View.OnClickListener() {

		    public void onClick(View v) {
			//meseFineDown();
				meseFineUp();
		    }
		});

		ImageView a_anno_down = (ImageView) findViewById(R.id.a_anno_down);
		a_anno_down.setOnClickListener(new View.OnClickListener() {

		    public void onClick(View v) {
			//annoFineDown();
				annoFineUp();
		    }
		});

		bollette_pagamenti_cerca_btn.setOnClickListener(new View.OnClickListener() {
		    public void onClick(View v) {
			// controllo della validit� dell'intervallo
			boolean intervalloCorretto = controllaIntervalloInserito();
			if (!intervalloCorretto) {
			    AlertDialog.Builder popupIntervalloNonCorretto = new AlertDialog.Builder(context);
			    popupIntervalloNonCorretto.setMessage(R.string.intervallo_non_valido).setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
				}
			    }).setTitle("Attenzione");
			    popupIntervalloNonCorretto.create().show();
			}
			else {
			    // faccio chiamata showListaFatture con intervallo
			    // valorizzato
			    String codiceCliente = getCodiceCliente(rispostaLogin);

			    animation = Utilities.animation(BollettePagamentiRicercaIntervallo.this);
			    imageAnim = (ImageView) findViewById(R.id.waiting_anim);
			    imageAnim.setVisibility(View.VISIBLE);
			    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
				imageAnim.setBackgroundDrawable(animation);
			    }
			    else {
				imageAnim.setBackground(animation);
			    }
			    animation.start();

			    if (!isNetworkAvailable(context)) {
				Intent intentOops = new Intent(getApplicationContext(), OopsPageBollettePagamentiRicercaIntervallo.class);
				intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.connessione_assente));
				intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
				intentOops.putExtra("CODICE_CONTO_CLIENTE", primoConto);
				intentOops.putExtra("DATA_INIZIO", getDataInizio());
				intentOops.putExtra("DATA_FINE", getDataFine());
				// Home.this.finish();
				startActivity(intentOops);
				if (animation != null) {
				    animation.stop();
				    imageAnim.setVisibility(View.INVISIBLE);
				}
				overridePendingTransition(R.anim.fade, R.anim.hold);
			    }
			    else {
				/********************* Chiamata show Lista fatture ******************************/
				ServiceCall stAvFornServiceCall = ServiceCallFactory.createServiceCall(context);
				// stAvFornServiceCall.myCookieStore = new
				// PersistentCookieStore(context);

				HashMap<String, String> parametri = new HashMap<String, String>();
				parametri.put("codiceCliente", codiceCliente);
				parametri.put("codiceContoCliente", primoConto);
				parametri.put("dataInizio", getDataInizio());
				parametri.put("dataFine", getDataFine());
				try {
				    stAvFornServiceCall.executeHttpsGetBollettePagamentiRicercaIntervallo(Constants.URL_SHOW_LISTA_FATTURE, parametri, BollettePagamentiRicercaIntervallo.this, null);
				}
				catch (Exception e) {
				    Intent intentOops = new Intent(getApplicationContext(), OopsPageBollettePagamentiRicercaIntervallo.class);
				    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
				    intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
				    intentOops.putExtra("CODICE_CONTO_CLIENTE", primoConto);
				    intentOops.putExtra("DATA_INIZIO", getDataInizio());
				    intentOops.putExtra("DATA_FINE", getDataFine());
				    // BollettePagamentiRicercaIntervallo.this.finish();
				    startActivity(intentOops);
				    overridePendingTransition(R.anim.fade, R.anim.hold);
				}

			    }
			}
		    }
		});

	    }
	}
	catch (JSONException e) {
	    e.printStackTrace();
	}

    }

    public void postChiamata(String rispostaChiamata, List<Cookie> lista, HashMap<String, String> parametri) {
	if (animation != null) {
	    animation.stop();
	    imageAnim.setVisibility(View.INVISIBLE);
	}

	final ImageView indietro = (ImageView) findViewById(R.id.bollette_pagamenti_indietro);
	indietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
	indietro.getBackground().invalidateSelf();

	JSONArray rispostaJSON = new JSONArray();
	String esito = "";
	try {
	    rispostaJSON = new JSONArray(rispostaChiamata);
	    JSONObject oggettoRisposta = (JSONObject) rispostaJSON.get(0);
	    esito = oggettoRisposta.getString("esito");
	}
	catch (JSONException e) {
	    e.printStackTrace();
	}
	if (esito.equals("200")) {

	    Constants.SERVIZIO_SHOW_LISTA_FATTURE = rispostaChiamata;
	    if (parametri.get("dataInizio") != null && parametri.get("dataFine") != null) {
		Intent intentBollettePagamentiRicercaPostInetrvallo = new Intent(getApplicationContext(), BollettePagamentiRicercaPostIntervallo.class);
		intentBollettePagamentiRicercaPostInetrvallo.putExtra("PRIMOCONTO", parametri.get("codiceContoCliente"));
		intentBollettePagamentiRicercaPostInetrvallo.putExtra("DATA_INIZIO", parametri.get("dataInizio"));
		intentBollettePagamentiRicercaPostInetrvallo.putExtra("DATA_FINE", parametri.get("dataFine"));
		intentBollettePagamentiRicercaPostInetrvallo.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
		BollettePagamentiRicercaIntervallo.this.finish();
		startActivity(intentBollettePagamentiRicercaPostInetrvallo);
		overridePendingTransition(R.anim.fade, R.anim.hold);
	    }
	    else {
		Intent intentBollettePagamentiRicerca = new Intent(getApplicationContext(), BollettePagamentiRicerca.class);
		intentBollettePagamentiRicerca.putExtra("PRIMOCONTO", parametri.get("codiceContoCliente"));
		intentBollettePagamentiRicerca.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
		BollettePagamentiRicercaIntervallo.this.finish();
		startActivity(intentBollettePagamentiRicerca);
		overridePendingTransition(R.anim.fade, R.anim.hold);
	    }

	}
	else {
	    Intent intentOops = new Intent();
	    intentOops = new Intent(getApplicationContext(), OopsPageBollettePagamentiRicercaIntervallo.class);

	    intentOops.putExtra("CODICE_CLIENTE", parametri.get("codiceCliente"));
	    intentOops.putExtra("CODICE_CONTO_CLIENTE", parametri.get("codiceContoCliente"));
	    if (parametri.get("dataInizio") != null && parametri.get("dataFine") != null) {
		intentOops.putExtra("DATA_INIZIO", parametri.get("dataInizio"));
		intentOops.putExtra("DATA_FINE", parametri.get("dataFine"));
	    }

	    // errori ops page
	    if (esito.equals("309")) {
		intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
		startActivity(intentOops);
		overridePendingTransition(R.anim.fade, R.anim.hold);

	    }
	    else if (esito.equals("400")) {
		intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
		startActivity(intentOops);
		overridePendingTransition(R.anim.fade, R.anim.hold);

	    }
	    else if (esito.equals("420")) {
		intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
		startActivity(intentOops);
		overridePendingTransition(R.anim.fade, R.anim.hold);

	    }
	    else if (esito.equals("449")) {
		intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
		startActivity(intentOops);
		overridePendingTransition(R.anim.fade, R.anim.hold);

	    }
	    else if (esito.equals("451")) {
		intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_popup_451));
		startActivity(intentOops);
		overridePendingTransition(R.anim.fade, R.anim.hold);

	    }
	    // errori popup
	    else if (esito.equals("307")) {
		Utils.popup(BollettePagamentiRicercaIntervallo.this, getResources().getString(R.string.errore_popup_307), "Riprova");

	    }
	    else if (esito.equals("435")) {
		Utils.popup(BollettePagamentiRicercaIntervallo.this, getResources().getString(R.string.errore_popup_435), "Riprova");

	    }
	    else if (esito.equals("437")) {
		Utils.popup(BollettePagamentiRicercaIntervallo.this, getResources().getString(R.string.errore_popup_437), "Riprova");

	    }
	    else if (esito.equals("438")) {
		Utils.popup(BollettePagamentiRicercaIntervallo.this, getResources().getString(R.string.errore_popup_438), "Riprova");

	    }
	    else if (esito.equals("443")) {
		Utils.popup(BollettePagamentiRicercaIntervallo.this, getResources().getString(R.string.errore_popup_443), "Riprova");

	    }
	    else if (esito.equals("444")) {
		Utils.popup(BollettePagamentiRicercaIntervallo.this, getResources().getString(R.string.errore_popup_444), "Riprova");

	    }
	    else if (esito.equals("445")) {
		Utils.popup(BollettePagamentiRicercaIntervallo.this, getResources().getString(R.string.errore_popup_445), "Riprova");

	    }
	    else if (esito.equals("446")) {
		Utils.popup(BollettePagamentiRicercaIntervallo.this, getResources().getString(R.string.errore_popup_446), "Riprova");

	    }
	    else if (esito.equals("447")) {
		Utils.popup(BollettePagamentiRicercaIntervallo.this, getResources().getString(R.string.errore_popup_447), "Riprova");

	    }else {
	    	if (esito.equals("")) {
				intentOops = new Intent(getApplicationContext(), OopsPageSessioneScaduta.class);
				intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.sessione_scaduta));
				startActivity(intentOops);
				overridePendingTransition(R.anim.fade, R.anim.hold);
			}
	    }

	}

    }

    public void postChiamataException(boolean exception, HashMap<String, String> parameterMap) {
	if (exception) {
	    if (animation != null) {
		animation.stop();
		imageAnim.setVisibility(View.INVISIBLE);
	    }

	    final ImageView indietro = (ImageView) findViewById(R.id.bollette_pagamenti_indietro);
	    indietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
	    indietro.getBackground().invalidateSelf();

	    Intent intentOops = new Intent(getApplicationContext(), OopsPageBollettePagamentiRicercaIntervallo.class);
	    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
	    intentOops.putExtra("CODICE_CLIENTE", parameterMap.get("codiceCliente"));
	    intentOops.putExtra("CODICE_CONTO_CLIENTE", parameterMap.get("codiceContoCliente"));
	    if (parameterMap.get("dataInizio") != null && parameterMap.get("dataFine") != null) {
		intentOops.putExtra("DATA_INIZIO", parameterMap.get("dataInizio"));
		intentOops.putExtra("DATA_FINE", parameterMap.get("dataFine"));
	    }

	    // BollettePagamenti.this.finish();
	    startActivity(intentOops);
	    overridePendingTransition(R.anim.fade, R.anim.hold);
	}
    }

    private String getDataInizio() {
	String result = "01";
	TextView da_mese_corrente = (TextView) findViewById(R.id.da_mese_corrente);
	String daMeseCorrente = da_mese_corrente.getText().toString();
	int daMeseInt = 0;
	for (int i = 0; i < arrayMesi.length; i++) {
	    if (daMeseCorrente.equals(arrayMesi[i])) {
		daMeseInt = i + 1;
	    }
	}
	result = result + "/" + ((("" + daMeseInt).length() == 1) ? ("0" + daMeseInt) : ("" + daMeseInt));
	TextView da_anno_corrente = (TextView) findViewById(R.id.da_anno_corrente);
	String daAnnoCorrente = da_anno_corrente.getText().toString();
	result = result + "/" + daAnnoCorrente;
	return result;
    }

    private String getDataFine() {
	String result = "01";
	TextView a_mese_sucessivo = (TextView) findViewById(R.id.a_mese_sucessivo);
	String aMeseSuccessivo = a_mese_sucessivo.getText().toString();
	int aMeseInt = 0;
	for (int i = 0; i < arrayMesi.length; i++) {
	    if (aMeseSuccessivo.equals(arrayMesi[i])) {
		aMeseInt = i + 1;
	    }
	    else if (aMeseSuccessivo.equals("")) {
		aMeseInt = 1;
	    }
	}
	result = result + "/" + ((("" + aMeseInt).length() == 1) ? ("0" + aMeseInt) : ("" + aMeseInt));
	TextView a_anno_corrente = (TextView) findViewById(R.id.a_anno_corrente);
	String aAnnoCorrente = a_anno_corrente.getText().toString();
	result = result + "/" + ((!aMeseSuccessivo.equals("")) ? aAnnoCorrente : ("" + (Integer.valueOf(aAnnoCorrente) + 1)));
	return result;
    }

    public String getCodiceCliente(String rispostaLogin) {
	String result = "";
	try {
	    JSONArray loginJson = new JSONArray(rispostaLogin);
	    JSONObject datiCliente = (JSONObject) loginJson.get(1);
	    result = datiCliente.getString("CodiceCliente");
	}
	catch (JSONException e) {
	    e.printStackTrace();
	}
	return result;
    }

    private boolean isNetworkAvailable(Context context) {
	ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
	if (connectivityManager == null) return false;
	NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
	return activeNetworkInfo != null;
    }

    public boolean controllaIntervalloInserito() {
	boolean result = false;

	TextView da_mese_corrente = (TextView) findViewById(R.id.da_mese_corrente);
	TextView da_anno_corrente = (TextView) findViewById(R.id.da_anno_corrente);
	TextView a_mese_corrente = (TextView) findViewById(R.id.a_mese_corrente);
	TextView a_anno_corrente = (TextView) findViewById(R.id.a_anno_corrente);

	String daMeseCorrente = da_mese_corrente.getText().toString();
	String aMeseCorrente = a_mese_corrente.getText().toString();
	int daMese = 0;
	int aMese = 0;
	for (int i = 0; i < arrayMesi.length; i++) {
	    if (daMeseCorrente.equals(arrayMesi[i])) {
		daMese = i;
	    }
	    if (aMeseCorrente.equals(arrayMesi[i])) {
		aMese = i;
	    }
	}

	String daAnnoCorrente = da_anno_corrente.getText().toString();
	int daAnnoCorrenteInt = Integer.valueOf(daAnnoCorrente);
	String aAnnoCorrente = a_anno_corrente.getText().toString();
	int aAnnoCorrenteInt = Integer.valueOf(aAnnoCorrente);
	if (daAnnoCorrenteInt == aAnnoCorrenteInt) {
	    if (daMese <= aMese) {
		result = true;
	    }
	    else {
		result = false;
	    }
	}
	else if (daAnnoCorrenteInt > aAnnoCorrenteInt) {
	    result = false;
	}
	else {
	    result = true;
	}

	return result;
    }

    public void impostaDatePickers() {
	// Da
	Calendar now = Calendar.getInstance();
	now.setTime(new Date());

	TextView da_mese_corrente = (TextView) findViewById(R.id.da_mese_corrente);
	da_mese_corrente.setText("" + arrayMesi[now.get(Calendar.MONTH)]);

	TextView da_mese_precedente = (TextView) findViewById(R.id.da_mese_precedente);
	if (now.get(Calendar.MONTH) > 0) {
	    da_mese_precedente.setText(arrayMesi[now.get(Calendar.MONTH) - 1]);
	}
	else {
	    da_mese_precedente.setText("");
	}

	TextView da_mese_successivo = (TextView) findViewById(R.id.da_mese_sucessivo);
	if (now.get(Calendar.MONTH) < 11) {
	    da_mese_successivo.setText(arrayMesi[now.get(Calendar.MONTH) + 1]);
	}
	else {
	    da_mese_successivo.setText("");
	}

	TextView da_anno_corrente = (TextView) findViewById(R.id.da_anno_corrente);
	da_anno_corrente.setText("" + (now.get(Calendar.YEAR) - 2));

	TextView da_anno_precedente = (TextView) findViewById(R.id.da_anno_precedente);
	da_anno_precedente.setText("" + (now.get(Calendar.YEAR) - 3));

	TextView da_anno_sucessivo = (TextView) findViewById(R.id.da_anno_sucessivo);
	da_anno_sucessivo.setText("" + (now.get(Calendar.YEAR) - 1));

	// A
	TextView a_mese_corrente = (TextView) findViewById(R.id.a_mese_corrente);
	a_mese_corrente.setText("" + arrayMesi[now.get(Calendar.MONTH)]);

	TextView a_mese_precedente = (TextView) findViewById(R.id.a_mese_precedente);
	if (now.get(Calendar.MONTH) > 0) {
	    a_mese_precedente.setText(arrayMesi[now.get(Calendar.MONTH) - 1]);
	}
	else {
	    a_mese_precedente.setText("");
	}

	TextView a_mese_successivo = (TextView) findViewById(R.id.a_mese_sucessivo);
	if (now.get(Calendar.MONTH) < 11) {
	    a_mese_successivo.setText(arrayMesi[now.get(Calendar.MONTH) + 1]);
	}
	else {
	    a_mese_successivo.setText("");
	}

	TextView a_anno_corrente = (TextView) findViewById(R.id.a_anno_corrente);
	a_anno_corrente.setText("" + now.get(Calendar.YEAR));

	TextView a_anno_precedente = (TextView) findViewById(R.id.a_anno_precedente);
	a_anno_precedente.setText("" + (now.get(Calendar.YEAR) - 1));

	TextView a_anno_sucessivo = (TextView) findViewById(R.id.a_anno_sucessivo);
	a_anno_sucessivo.setText("");

    }

    public void meseInizioUp() {
	TextView damesecorrente = (TextView) findViewById(R.id.da_mese_corrente);
	TextView dameseprecedente = (TextView) findViewById(R.id.da_mese_precedente);
	TextView damesesucessivo = (TextView) findViewById(R.id.da_mese_sucessivo);
	if (!dameseprecedente.getText().toString().equals("")) {
	    String damesecorrenteAttuale = damesecorrente.getText().toString();
	    String damesecorrenteModificato = diminuisciMese(damesecorrenteAttuale);
	    damesecorrente.setText(damesecorrenteModificato);

	    String dameseprecedenteAttuale = dameseprecedente.getText().toString();
	    String dameseprecedenteModificato = diminuisciMese(dameseprecedenteAttuale);
	    dameseprecedente.setText(dameseprecedenteModificato);

	    String damesesucessivoAttuale = damesesucessivo.getText().toString();
	    String damesesucessivoModificato = diminuisciMese(damesesucessivoAttuale);
	    damesesucessivo.setText(damesesucessivoModificato);
	}
    }

    public void meseInizioDown() {
	Calendar now = Calendar.getInstance();
	now.setTime(new Date());

	TextView damesecorrente = (TextView) findViewById(R.id.da_mese_corrente);
	TextView dameseprecedente = (TextView) findViewById(R.id.da_mese_precedente);
	TextView damesesucessivo = (TextView) findViewById(R.id.da_mese_sucessivo);

	TextView daannocorrente = (TextView) findViewById(R.id.da_anno_corrente);
	String daAnno = daannocorrente.getText().toString();
	int daAnnoInt = Integer.valueOf(daAnno);

	String daMese = damesecorrente.getText().toString();
	int daMeseInt = 0;
	for (int i = 0; i < arrayMesi.length; i++) {
	    if (daMese.equals(arrayMesi[i])) {
		daMeseInt = i;
	    }
	}

	if (!damesesucessivo.getText().toString().equals("") && !(daAnnoInt == now.get(Calendar.YEAR) && daMeseInt == now.get(Calendar.MONTH))) {
	    String damesecorrenteAttuale = damesecorrente.getText().toString();
	    String damesecorrenteModificato = aumentaMese(damesecorrenteAttuale);
	    damesecorrente.setText(damesecorrenteModificato);

	    String dameseprecedenteAttuale = dameseprecedente.getText().toString();
	    String dameseprecedenteModificato = aumentaMese(dameseprecedenteAttuale);
	    dameseprecedente.setText(dameseprecedenteModificato);

	    String damesesucessivoAttuale = damesesucessivo.getText().toString();
	    String damesesucessivoModificato = aumentaMese(damesesucessivoAttuale);
	    damesesucessivo.setText(damesesucessivoModificato);
	}

    }

    public void meseFineUp() {
	TextView damesecorrente = (TextView) findViewById(R.id.a_mese_corrente);
	TextView dameseprecedente = (TextView) findViewById(R.id.a_mese_precedente);
	TextView damesesucessivo = (TextView) findViewById(R.id.a_mese_sucessivo);

	if (!dameseprecedente.getText().toString().equals("")) {
	    String damesecorrenteAttuale = damesecorrente.getText().toString();
	    String damesecorrenteModificato = diminuisciMese(damesecorrenteAttuale);
	    damesecorrente.setText(damesecorrenteModificato);

	    String dameseprecedenteAttuale = dameseprecedente.getText().toString();
	    String dameseprecedenteModificato = diminuisciMese(dameseprecedenteAttuale);
	    dameseprecedente.setText(dameseprecedenteModificato);

	    String damesesucessivoAttuale = damesesucessivo.getText().toString();
	    String damesesucessivoModificato = diminuisciMese(damesesucessivoAttuale);
	    damesesucessivo.setText(damesesucessivoModificato);
	}

    }

    public void meseFineDown() {
	Calendar now = Calendar.getInstance();
	now.setTime(new Date());

	TextView damesecorrente = (TextView) findViewById(R.id.a_mese_corrente);
	TextView dameseprecedente = (TextView) findViewById(R.id.a_mese_precedente);
	TextView damesesucessivo = (TextView) findViewById(R.id.a_mese_sucessivo);

	TextView aannocorrente = (TextView) findViewById(R.id.a_anno_corrente);
	String aAnno = aannocorrente.getText().toString();
	int aAnnoInt = Integer.valueOf(aAnno);

	String aMese = damesecorrente.getText().toString();
	int aMeseInt = 0;
	for (int i = 0; i < arrayMesi.length; i++) {
	    if (aMese.equals(arrayMesi[i])) {
		aMeseInt = i;
	    }
	}
	if (!damesesucessivo.getText().toString().equals("") && !(aAnnoInt == now.get(Calendar.YEAR) && aMeseInt == now.get(Calendar.MONTH))) {
	    String damesecorrenteAttuale = damesecorrente.getText().toString();
	    String damesecorrenteModificato = aumentaMese(damesecorrenteAttuale);
	    damesecorrente.setText(damesecorrenteModificato);

	    String dameseprecedenteAttuale = dameseprecedente.getText().toString();
	    String dameseprecedenteModificato = aumentaMese(dameseprecedenteAttuale);
	    dameseprecedente.setText(dameseprecedenteModificato);

	    String damesesucessivoAttuale = damesesucessivo.getText().toString();
	    String damesesucessivoModificato = aumentaMese(damesesucessivoAttuale);
	    damesesucessivo.setText(damesesucessivoModificato);
	}

    }

    public void annoInizioUp() {
	TextView daannocorrente = (TextView) findViewById(R.id.da_anno_corrente);
	TextView daannoprecedente = (TextView) findViewById(R.id.da_anno_precedente);
	TextView daannosucessivo = (TextView) findViewById(R.id.da_anno_sucessivo);

	if (!daannoprecedente.getText().toString().equals("")) {
	    String daannocorrenteAttuale = daannocorrente.getText().toString();
	    String daannocorrenteModificato = diminuisciAnno(daannocorrenteAttuale);
	    daannocorrente.setText(daannocorrenteModificato);

	    String daannoprecedenteAttuale = daannoprecedente.getText().toString();
	    String daannoprecedenteModificato = diminuisciAnno(daannoprecedenteAttuale);
	    daannoprecedente.setText(daannoprecedenteModificato);

	    String daannosucessivoAttuale = daannosucessivo.getText().toString();
	    String daannosucessivoModificato = diminuisciAnno(daannosucessivoAttuale);
	    daannosucessivo.setText(daannosucessivoModificato);
	}

    }

    public void annoInizioDown() {
	Calendar now = Calendar.getInstance();
	now.setTime(new Date());

	TextView daannocorrente = (TextView) findViewById(R.id.da_anno_corrente);
	TextView daannoprecedente = (TextView) findViewById(R.id.da_anno_precedente);
	TextView daannosucessivo = (TextView) findViewById(R.id.da_anno_sucessivo);

	TextView damesecorrente = (TextView) findViewById(R.id.da_mese_corrente);
	String daAnno = daannocorrente.getText().toString();
	int daAnnoInt = Integer.valueOf(daAnno);

	String daMese = damesecorrente.getText().toString();
	int daMeseInt = 0;
	for (int i = 0; i < arrayMesi.length; i++) {
	    if (daMese.equals(arrayMesi[i])) {
		daMeseInt = i;
	    }
	}

	if (!daannosucessivo.getText().toString().equals("") && !(daMeseInt > now.get(Calendar.MONTH) && daAnnoInt == (now.get(Calendar.YEAR) - 1))) {
	    String daannocorrenteAttuale = daannocorrente.getText().toString();
	    String daannocorrenteModificato = aumentaAnno(daannocorrenteAttuale);
	    daannocorrente.setText(daannocorrenteModificato);

	    String daannoprecedenteAttuale = daannoprecedente.getText().toString();
	    String daannoprecedenteModificato = aumentaAnno(daannoprecedenteAttuale);
	    daannoprecedente.setText(daannoprecedenteModificato);

	    String daannosucessivoAttuale = daannosucessivo.getText().toString();
	    String daannosucessivoModificato = aumentaAnno(daannosucessivoAttuale);
	    daannosucessivo.setText(daannosucessivoModificato);
	}

    }

    public void annoFineUp() {
	TextView daannocorrente = (TextView) findViewById(R.id.a_anno_corrente);
	TextView daannoprecedente = (TextView) findViewById(R.id.a_anno_precedente);
	TextView daannosucessivo = (TextView) findViewById(R.id.a_anno_sucessivo);

	if (!daannoprecedente.getText().toString().equals("")) {
	    String daannocorrenteAttuale = daannocorrente.getText().toString();
	    String daannocorrenteModificato = diminuisciAnno(daannocorrenteAttuale);
	    daannocorrente.setText(daannocorrenteModificato);

	    String daannoprecedenteAttuale = daannoprecedente.getText().toString();
	    String daannoprecedenteModificato = diminuisciAnno(daannoprecedenteAttuale);
	    daannoprecedente.setText(daannoprecedenteModificato);

	    String daannosucessivoAttuale = daannosucessivo.getText().toString();
	    String daannosucessivoModificato = diminuisciAnno(daannosucessivoAttuale);
	    daannosucessivo.setText(daannosucessivoModificato);
	}

    }

    public void annoFineDown() {
	Calendar now = Calendar.getInstance();
	now.setTime(new Date());

	TextView daannocorrente = (TextView) findViewById(R.id.a_anno_corrente);
	TextView daannoprecedente = (TextView) findViewById(R.id.a_anno_precedente);
	TextView daannosucessivo = (TextView) findViewById(R.id.a_anno_sucessivo);

	TextView damesecorrente = (TextView) findViewById(R.id.a_mese_corrente);
	String daAnno = daannocorrente.getText().toString();
	int daAnnoInt = Integer.valueOf(daAnno);

	String daMese = damesecorrente.getText().toString();
	int daMeseInt = 0;
	for (int i = 0; i < arrayMesi.length; i++) {
	    if (daMese.equals(arrayMesi[i])) {
		daMeseInt = i;
	    }
	}

	if (!daannosucessivo.getText().toString().equals("") && !(daMeseInt > now.get(Calendar.MONTH) && daAnnoInt == (now.get(Calendar.YEAR) - 1))) {
	    String daannocorrenteAttuale = daannocorrente.getText().toString();
	    String daannocorrenteModificato = aumentaAnno(daannocorrenteAttuale);
	    daannocorrente.setText(daannocorrenteModificato);

	    String daannoprecedenteAttuale = daannoprecedente.getText().toString();
	    String daannoprecedenteModificato = aumentaAnno(daannoprecedenteAttuale);
	    daannoprecedente.setText(daannoprecedenteModificato);

	    String daannosucessivoAttuale = daannosucessivo.getText().toString();
	    String daannosucessivoModificato = aumentaAnno(daannosucessivoAttuale);
	    daannosucessivo.setText(daannosucessivoModificato);
	}

    }

    public String diminuisciMese(String mese) {
	String result = "";

	if (mese.equals("Gen")) {
	    result = "";
	}
	else if (mese.equals("Feb")) {
	    result = "Gen";
	}
	else if (mese.equals("Mar")) {
	    result = "Feb";
	}
	else if (mese.equals("Apr")) {
	    result = "Mar";
	}
	else if (mese.equals("Mag")) {
	    result = "Apr";
	}
	else if (mese.equals("Giu")) {
	    result = "Mag";
	}
	else if (mese.equals("Lug")) {
	    result = "Giu";
	}
	else if (mese.equals("Ago")) {
	    result = "Lug";
	}
	else if (mese.equals("Set")) {
	    result = "Ago";
	}
	else if (mese.equals("Ott")) {
	    result = "Set";
	}
	else if (mese.equals("Nov")) {
	    result = "Ott";
	}
	else if (mese.equals("Dic")) {
	    result = "Nov";
	}
	else if (mese.equals("")) {
	    result = "Dic";
	}

	return result;
    }

    public String aumentaMese(String mese) {
	String result = "";

	if (mese.equals("")) {
	    result = "Gen";
	}
	else if (mese.equals("Gen")) {
	    result = "Feb";
	}
	else if (mese.equals("Feb")) {
	    result = "Mar";
	}
	else if (mese.equals("Mar")) {
	    result = "Apr";
	}
	else if (mese.equals("Apr")) {
	    result = "Mag";
	}
	else if (mese.equals("Mag")) {
	    result = "Giu";
	}
	else if (mese.equals("Giu")) {
	    result = "Lug";
	}
	else if (mese.equals("Lug")) {
	    result = "Ago";
	}
	else if (mese.equals("Ago")) {
	    result = "Set";
	}
	else if (mese.equals("Set")) {
	    result = "Ott";
	}
	else if (mese.equals("Ott")) {
	    result = "Nov";
	}
	else if (mese.equals("Nov")) {
	    result = "Dic";
	}
	else if (mese.equals("Dic")) {
	    result = "";
	}

	return result;
    }

    public String diminuisciAnno(String anno) {
	String result = "";
	Calendar now = Calendar.getInstance();
	now.setTime(new Date());

	if (anno.equals("")) {
	    return "" + (now.get(Calendar.YEAR));
	}
	else {
	    int annoInt = Integer.valueOf(anno);

	    if (annoInt - 1 >= (now.get(Calendar.YEAR) - (numeroAnni - 1))) result = "" + (annoInt - 1);
	    else
		result = "";
	    return result;
	}
    }

    public String aumentaAnno(String anno) {
	String result = "";
	Calendar now = Calendar.getInstance();
	now.setTime(new Date());

	if (anno.equals("")) {
	    return "" + (now.get(Calendar.YEAR) - (numeroAnni - 1));
	}
	else {
	    int annoInt = Integer.valueOf(anno);
	    if (annoInt + 1 <= now.get(Calendar.YEAR)) result = "" + (annoInt + 1);
	    else
		result = "";
	    return result;
	}
    }

    private String statoConto(JSONArray login, String codiceConto) {
	String result = "";
	JSONArray conto;
	JSONObject anagrConto;
	try {
	    for (int i = 2; i < login.length(); i++) {
		conto = login.getJSONArray(i);
		anagrConto = conto.getJSONObject(0);
		String codice = anagrConto.getString("CodiceConto");
		if (codice.equals(codiceConto)) {
		    String statoConto = anagrConto.getString("StatoConto");
		    if (statoConto.equals("UTILIZZABILE")) {
			result = "Attivo";
		    }
		    else if (statoConto.equals("NON UTILIZZABILE")) {
			result = "Cessato";
		    }
		}
		else {
		    continue;
		}
	    }
	}
	catch (JSONException e) {
	    e.printStackTrace();
	}
	return result;

    }

    public void onBackPressed() {

	animation = Utilities.animation(this);
	imageAnim = (ImageView) findViewById(R.id.waiting_anim);
	imageAnim.setVisibility(View.VISIBLE);
	if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
	    imageAnim.setBackgroundDrawable(animation);
	}
	else {
	    imageAnim.setBackground(animation);
	}
	animation.start();

	Intent intentBollettePagamenti = new Intent(getApplicationContext(), BollettePagamentiRicerca.class);
	intentBollettePagamenti.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
	intentBollettePagamenti.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
	BollettePagamentiRicercaIntervallo.this.finish();
	startActivity(intentBollettePagamenti);
	overridePendingTransition(R.anim.fade, R.anim.hold);
    }
}
