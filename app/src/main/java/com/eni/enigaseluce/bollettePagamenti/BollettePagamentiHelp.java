package com.eni.enigaseluce.bollettePagamenti;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.AnimationDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.eni.enigaseluce.R;
import com.eni.enigaseluce.httpcall.Constants;
import com.eni.enigaseluce.utils.Utilities;

@SuppressLint("NewApi")
public class BollettePagamentiHelp extends Activity {
    AnimationDrawable animation;
    ImageView imageAnim;
    String origine;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.bollette_pagamenti_help);
	origine = getIntent().getStringExtra("ORIGINE");

	final TextView contenuto = (TextView) findViewById(R.id.contenuto_help);
	if (origine.equals(Constants.ORIGINE_BOLLETTE_PAGAMENTI)) {
	    contenuto.setText(getResources().getString(R.string.help_estratto_conto));
	}
	else if (origine.equals(Constants.ORIGINE_BOLLETTE_PAGAMENTI_PROSSIMA_BOLLETTA)) {
	    contenuto.setText(getResources().getString(R.string.help_prossima_bolletta));
	}
	else if (origine.equals(Constants.ORIGINE_BOLLETTE_RATEIZZABILI)) {
	    contenuto.setText(getResources().getString(R.string.help_bollette_rateizzabili));
	    contenuto.setTextSize(Float.valueOf(18));
	}
	else if (origine.equals(Constants.ORIGINE_BOLLETTE_RATEIZZABILI_SELEZIONE_PIANO)) {
	    contenuto.setText(getResources().getString(R.string.help_bollette_rateizzabili));
	    contenuto.setTextSize(Float.valueOf(18));
	}
	else if (origine.equals(Constants.ORIGINE_BOLLETTE_RATEIZZABILI_DETTAGLIO_PIANO)) {
	    contenuto.setText(getResources().getString(R.string.help_bollette_rateizzabili));
	    contenuto.setTextSize(Float.valueOf(18));
	}
	else {
	    contenuto.setText(getResources().getString(R.string.help_elenco_bollette));
	}

	final ImageView help = (ImageView) findViewById(R.id.bollette_pagamenti_help_indietro);
	help.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
	help.getBackground().invalidateSelf();
	help.setOnClickListener(new View.OnClickListener() {
	    public void onClick(View v1) {
		help.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
		help.getBackground().invalidateSelf();

		animation = Utilities.animation(BollettePagamentiHelp.this);
		imageAnim = (ImageView) findViewById(R.id.waiting_anim);
		imageAnim.setVisibility(View.VISIBLE);
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
		    imageAnim.setBackgroundDrawable(animation);
		}
		else {
		    imageAnim.setBackground(animation);
		}
		animation.start();

		Intent intentHelp = new Intent();
		if (origine.equals(Constants.ORIGINE_BOLLETTE_PAGAMENTI)) {
		    intentHelp = new Intent(getApplicationContext(), BollettePagamenti.class);
		    intentHelp.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
		    intentHelp.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
		}
		else if (origine.equals(Constants.ORIGINE_BOLLETTE_PAGAMENTI_PROSSIMA_BOLLETTA)) {
		    intentHelp = new Intent(getApplicationContext(), BollettePagamentiProssimaBolletta.class);
		    intentHelp.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
		    intentHelp.putExtra("TIPOLOGIA", getIntent().getStringExtra("TIPOLOGIA"));
		    intentHelp.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
		}
		else if (origine.equals(Constants.ORIGINE_BOLLETTE_PAGAMENTI_RICERCA)) {
		    intentHelp = new Intent(getApplicationContext(), BollettePagamentiRicerca.class);
		    intentHelp.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
		    intentHelp.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
		}
		else if (origine.equals(Constants.ORIGINE_BOLLETTE_PAGAMENTI_RICERCA_POST_INTERVALLO)) {
		    intentHelp = new Intent(getApplicationContext(), BollettePagamentiRicercaPostIntervallo.class);
		    intentHelp.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
		    intentHelp.putExtra("DATA_INIZIO", getIntent().getStringExtra("DATA_INIZIO"));
		    intentHelp.putExtra("DATA_FINE", getIntent().getStringExtra("DATA_FINE"));
		    intentHelp.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
		}
		else if (origine.equals(Constants.ORIGINE_BOLLETTE_PAGAMENTI_RICERCA_INTERVALLO)) {
		    intentHelp = new Intent(getApplicationContext(), BollettePagamentiRicercaIntervallo.class);
		    intentHelp.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
		    intentHelp.putExtra("ORIGINE", getIntent().getStringExtra("origine"));
		    intentHelp.putExtra("DATA_INIZIO", getIntent().getStringExtra("DATA_INIZIO"));
		    intentHelp.putExtra("DATA_FINE", getIntent().getStringExtra("DATA_FINE"));
		    intentHelp.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
		}
		else if (origine.equals(Constants.ORIGINE_BOLLETTE_RATEIZZABILI)) {
		    intentHelp = new Intent(getApplicationContext(), BollettePagamentiRateizzate.class);
		    intentHelp.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
		    intentHelp.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
		    intentHelp.putExtra("tipoFornitura", getIntent().getStringExtra("tipoFornitura"));
		}
		else if (origine.equals(Constants.ORIGINE_BOLLETTE_RATEIZZABILI_SELEZIONE_PIANO)) {
		    intentHelp = new Intent(getApplicationContext(), BollettePagamentiSceltaPiano.class);
		    intentHelp.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
		    intentHelp.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
		    intentHelp.putExtra("tipoFornitura", getIntent().getStringExtra("tipoFornitura"));
		    
		    intentHelp.putExtra("domiciliazioneApprovata", getIntent().getBooleanExtra("domiciliazioneApprovata", false));
		}
		else if (origine.equals(Constants.ORIGINE_BOLLETTE_RATEIZZABILI_DETTAGLIO_PIANO)) {
			intentHelp = new Intent(getApplicationContext(), BollettePagamentiDettaglioPiano.class);
			intentHelp.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
			intentHelp.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
			intentHelp.putExtra("tipoFornitura", getIntent().getStringExtra("tipoFornitura"));
			intentHelp.putExtra("identificativoFattura", getIntent().getStringExtra("identificativoFattura"));
			if (getIntent().getStringExtra("pianoSelezionato") != null) {
				intentHelp.putExtra("pianoSelezionato", getIntent().getStringExtra("pianoSelezionato"));
			}
			intentHelp.putExtra("codiceFattura", getIntent().getStringExtra("codiceFattura"));
			intentHelp.putExtra("dataEmissione", getIntent().getStringExtra("dataEmissione"));
			intentHelp.putExtra("dataScadenza", getIntent().getStringExtra("dataScadenza"));
			intentHelp.putExtra("importoRateizzabile", getIntent().getStringExtra("importoRateizzabile"));
			intentHelp.putExtra("helpPage", true);
			
			intentHelp.putExtra("domiciliazioneApprovata", getIntent().getBooleanExtra("domiciliazioneApprovata", false));
		}

		BollettePagamentiHelp.this.finish();
		startActivity(intentHelp);
		overridePendingTransition(R.anim.fade, R.anim.hold);
	    }
	});

    }

    public void onBackPressed() {

	animation = Utilities.animation(this);
	imageAnim = (ImageView) findViewById(R.id.waiting_anim);
	imageAnim.setVisibility(View.VISIBLE);
	if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
	    imageAnim.setBackgroundDrawable(animation);
	}
	else {
	    imageAnim.setBackground(animation);
	}
	animation.start();

	Intent intentHelp = new Intent();
	if (origine.equals(Constants.ORIGINE_BOLLETTE_PAGAMENTI)) {
	    intentHelp = new Intent(getApplicationContext(), BollettePagamenti.class);
	    intentHelp.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
	    intentHelp.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
	}
	else if (origine.equals(Constants.ORIGINE_BOLLETTE_PAGAMENTI_PROSSIMA_BOLLETTA)) {
	    intentHelp = new Intent(getApplicationContext(), BollettePagamentiProssimaBolletta.class);
	    intentHelp.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
	    intentHelp.putExtra("TIPOLOGIA", getIntent().getStringExtra("TIPOLOGIA"));
	    intentHelp.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
	}
	else if (origine.equals(Constants.ORIGINE_BOLLETTE_PAGAMENTI_RICERCA)) {
	    intentHelp = new Intent(getApplicationContext(), BollettePagamentiRicerca.class);
	    intentHelp.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
	    intentHelp.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
	}
	else if (origine.equals(Constants.ORIGINE_BOLLETTE_PAGAMENTI_RICERCA_POST_INTERVALLO)) {
	    intentHelp = new Intent(getApplicationContext(), BollettePagamentiRicercaPostIntervallo.class);
	    intentHelp.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
	    intentHelp.putExtra("DATA_INIZIO", getIntent().getStringExtra("DATA_INIZIO"));
	    intentHelp.putExtra("DATA_FINE", getIntent().getStringExtra("DATA_FINE"));
	    intentHelp.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
	}
	else if (origine.equals(Constants.ORIGINE_BOLLETTE_PAGAMENTI_RICERCA_INTERVALLO)) {
	    intentHelp = new Intent(getApplicationContext(), BollettePagamentiRicercaIntervallo.class);
	    intentHelp.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
	    intentHelp.putExtra("ORIGINE", getIntent().getStringExtra("origine"));
	    intentHelp.putExtra("DATA_INIZIO", getIntent().getStringExtra("DATA_INIZIO"));
	    intentHelp.putExtra("DATA_FINE", getIntent().getStringExtra("DATA_FINE"));
	    intentHelp.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
	}
	else if (origine.equals(Constants.ORIGINE_BOLLETTE_RATEIZZABILI)) {
	    intentHelp = new Intent(getApplicationContext(), BollettePagamentiRateizzate.class);
	    intentHelp.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
	    intentHelp.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
	    intentHelp.putExtra("tipoFornitura", getIntent().getStringExtra("tipoFornitura"));
	}
	else if (origine.equals(Constants.ORIGINE_BOLLETTE_RATEIZZABILI_SELEZIONE_PIANO)) {
	    intentHelp = new Intent(getApplicationContext(), BollettePagamentiSceltaPiano.class);
	    intentHelp.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
	    intentHelp.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
	    intentHelp.putExtra("tipoFornitura", getIntent().getStringExtra("tipoFornitura"));
	    
	    intentHelp.putExtra("domiciliazioneApprovata", getIntent().getBooleanExtra("domiciliazioneApprovata", false));
	}
	else if (origine.equals(Constants.ORIGINE_BOLLETTE_RATEIZZABILI_DETTAGLIO_PIANO)) {
	    intentHelp = new Intent(getApplicationContext(), BollettePagamentiDettaglioPiano.class);
	    intentHelp.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
	    intentHelp.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
	    intentHelp.putExtra("tipoFornitura", getIntent().getStringExtra("tipoFornitura"));
	    intentHelp.putExtra("identificativoFattura", getIntent().getStringExtra("identificativoFattura"));
	    if (getIntent().getStringExtra("pianoSelezionato") != null) {
	    	intentHelp.putExtra("pianoSelezionato", getIntent().getStringExtra("pianoSelezionato"));
	    }
	    intentHelp.putExtra("codiceFattura", getIntent().getStringExtra("codiceFattura"));
	    intentHelp.putExtra("dataEmissione", getIntent().getStringExtra("dataEmissione"));
	    intentHelp.putExtra("dataScadenza", getIntent().getStringExtra("dataScadenza"));
	    intentHelp.putExtra("importoRateizzabile", getIntent().getStringExtra("importoRateizzabile"));
	    intentHelp.putExtra("helpPage", true);
	    
	    intentHelp.putExtra("domiciliazioneApprovata", getIntent().getBooleanExtra("domiciliazioneApprovata", false));
	}

	BollettePagamentiHelp.this.finish();
	startActivity(intentHelp);
	overridePendingTransition(R.anim.fade, R.anim.hold);
    }
}
