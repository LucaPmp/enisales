package com.eni.enigaseluce.bollettePagamenti;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.http.cookie.Cookie;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.AnimationDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.eni.enigaseluce.R;
import com.eni.enigaseluce.error.OopsPageBollettePagamentiDownloadBolletta;
import com.eni.enigaseluce.error.OopsPageSessioneScaduta;
import com.eni.enigaseluce.error.Utils;
import com.eni.enigaseluce.font.EniFont;
import com.eni.enigaseluce.httpcall.Constants;
import com.eni.enigaseluce.httpcall.ServiceCall;
import com.eni.enigaseluce.httpcall.ServiceCallFactory;
import com.eni.enigaseluce.login.help.GuidaBolletta;
import com.eni.enigaseluce.utils.Utilities;

@SuppressLint("NewApi")
public class BollettePagamentiRicerca extends Activity {
    private Context context;
    private static Context staticContext;
    AnimationDrawable animation;
    ImageView imageAnim;

    static LinearLayout pager;

    public String rispostaLogin = Constants.SERVIZIO_LOGIN;
    public String rispostaShowListaFatture = Constants.SERVIZIO_SHOW_LISTA_FATTURE;

    private static LinearLayout waiting_download;
    LinearLayout contenuto;

    public static String nomeInteroCliente = "";
    static LayoutInflater inflater;
    String codiceEsito;
    String primoConto;

    public static Context getContext() {
        return BollettePagamentiRicerca.staticContext;
    }

    /**
     * @return the waiting_download
     */
    public static LinearLayout getWaiting_download() {
        return waiting_download;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bollette_pagamenti_ricerca);

        context = this;
        BollettePagamentiRicerca.staticContext = this;
        primoConto = getIntent().getStringExtra("PRIMOCONTO");

    }

    @Override
    protected void onStart() {
        super.onStart();

        final ImageView indietro = (ImageView) findViewById(R.id.bollette_pagamenti_indietro);
        indietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
        indietro.getBackground().invalidateSelf();
        indietro.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                indietro.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
                indietro.getBackground().invalidateSelf();

                animation = Utilities.animation(BollettePagamentiRicerca.this);
                imageAnim = (ImageView) findViewById(R.id.waiting_anim);
                imageAnim.setVisibility(View.VISIBLE);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    imageAnim.setBackgroundDrawable(animation);
                } else {
                    imageAnim.setBackground(animation);
                }
                animation.start();

                Intent intentBollettePagamenti = new Intent(getApplicationContext(), BollettePagamenti.class);
                intentBollettePagamenti.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
                intentBollettePagamenti.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
                BollettePagamentiRicerca.this.finish();
                startActivity(intentBollettePagamenti);
                overridePendingTransition(R.anim.fade, R.anim.hold);
            }
        });
//	final ImageView help = (ImageView) findViewById(R.id.bollette_pagamenti_help);
//	help.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
//	help.getBackground().invalidateSelf();
//	help.setOnClickListener(new View.OnClickListener() {
//	    public void onClick(View v) {
//		help.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
//		help.getBackground().invalidateSelf();
//
//		animation = Utilities.animation(BollettePagamentiRicerca.this);
//		imageAnim = (ImageView) findViewById(R.id.waiting_anim);
//		imageAnim.setVisibility(View.VISIBLE);
//		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
//		    imageAnim.setBackgroundDrawable(animation);
//		}
//		else {
//		    imageAnim.setBackground(animation);
//		}
//		animation.start();
//
//		Intent intentHelp = new Intent(getApplicationContext(), BollettePagamentiHelp.class);
//		intentHelp.putExtra("ORIGINE", Constants.ORIGINE_BOLLETTE_PAGAMENTI_RICERCA);
//		intentHelp.putExtra("PRIMOCONTO", primoConto);
//		intentHelp.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
//		BollettePagamentiRicerca.this.finish();
//		startActivity(intentHelp);
//		overridePendingTransition(R.anim.fade, R.anim.hold);
//	    }
//	});
        try {
            final JSONArray bollettePagamentiLogin = new JSONArray(Constants.SERVIZIO_LOGIN);
            JSONObject esito = bollettePagamentiLogin.getJSONObject(0);
            codiceEsito = (String) esito.getString("esito");
            if (codiceEsito.equals("200")) {
                TextView dal = (TextView) findViewById(R.id.visualizzazione_bollette_dal_valore);
                dal.setText(dataDueAnniPrima());
                TextView al = (TextView) findViewById(R.id.visualizzazione_bollette_al_valore);
                al.setText(dataAttuale());

                EniFont bollette_pagamenti_cerca_btn = (EniFont) findViewById(R.id.bollette_pagamenti_cerca_btn);
                bollette_pagamenti_cerca_btn.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {

                        animation = Utilities.animation(BollettePagamentiRicerca.this);
                        imageAnim = (ImageView) findViewById(R.id.waiting_anim);
                        imageAnim.setVisibility(View.VISIBLE);
                        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                            imageAnim.setBackgroundDrawable(animation);
                        } else {
                            imageAnim.setBackground(animation);
                        }
                        animation.start();

                        Intent intentBollettePagamentiIntervallo = new Intent(getApplicationContext(), BollettePagamentiRicercaIntervallo.class);
                        intentBollettePagamentiIntervallo.putExtra("PRIMOCONTO", primoConto);
                        intentBollettePagamentiIntervallo.putExtra("ORIGINE", Constants.ORIGINE_BOLLETTE_PAGAMENTI_RICERCA);
                        intentBollettePagamentiIntervallo.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
                        BollettePagamentiRicerca.this.finish();
                        startActivity(intentBollettePagamentiIntervallo);
                        overridePendingTransition(R.anim.fade, R.anim.hold);

                    }
                });

                pager = (LinearLayout) findViewById(R.id.bollette_pagamenti_lista_pager);

                inflater = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);
                if (!rispostaShowListaFatture.equals("")) {
                    popolaPaginaContenuto(new JSONArray(rispostaShowListaFatture), pager, primoConto);
                }

                // se arrivo dalla oops page
                if (getIntent().getBooleanExtra("OopsPage", false)) {
                    // far apparire la finestrella nera e rifare la chiamata
                    // chiamata download
                    contenuto = (LinearLayout) findViewById(R.id.contenuto);
                    contenuto.setClickable(false);
                    waiting_download = (LinearLayout) findViewById(R.id.waiting_download);
                    waiting_download.setVisibility(View.VISIBLE);
                    TextView download_status = (TextView) findViewById(R.id.download_status);
                    download_status.setText("download in corso");
                    TextView download_file = (TextView) findViewById(R.id.download_file);
                    download_file.setText("blt_" + getIntent().getStringExtra("NumeroFattura") + ".pdf");
                    Button download_apri = (Button) findViewById(R.id.download_apri);
                    download_apri.setVisibility(View.GONE);
                    //Baccus
                    Button download_chiudi = (Button) findViewById(R.id.download_chiudi);
                    download_chiudi.setVisibility(View.GONE);

                    animation = Utilities.animation(this);
                    imageAnim = (ImageView) findViewById(R.id.waiting_anim);
                    imageAnim.setVisibility(View.VISIBLE);
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                        imageAnim.setBackgroundDrawable(animation);
                    } else {
                        imageAnim.setBackground(animation);
                    }
                    animation.start();

                    if (!isNetworkAvailable(context)) {
                        Intent intentOops = new Intent(getApplicationContext(), OopsPageBollettePagamentiDownloadBolletta.class);
                        intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.connessione_assente));
                        intentOops.putExtra("ID_PDF", getIntent().getStringExtra("ID_PDF"));
                        intentOops.putExtra("NumeroFattura", getIntent().getStringExtra("NumeroFattura"));
                        intentOops.putExtra("PRIMOCONTO", primoConto);
                        intentOops.putExtra("ORIGINE", Constants.ORIGINE_BOLLETTE_PAGAMENTI_RICERCA);
                        intentOops.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
                        BollettePagamentiRicerca.this.finish();
                        startActivity(intentOops);
                        waiting_download.setVisibility(View.GONE);
                        if (animation != null) {
                            animation.stop();
                            imageAnim.setVisibility(View.INVISIBLE);
                        }
                        overridePendingTransition(R.anim.fade, R.anim.hold);
                    } else {
                        /********************************** Chiamata Download Pdf *********************************************/
                        ServiceCall downloadServiceCall = ServiceCallFactory.createServiceCall(context);
                        // downloadServiceCall.myCookieStore = new
                        // PersistentCookieStore(context);

                        HashMap<String, String> parametri = new HashMap<String, String>();
                        parametri.put("idProtocolloDocPdf", getIntent().getStringExtra("ID_PDF"));

                        try {
                            downloadServiceCall.executeHttpsGetBollettePagamentiRicerca(Constants.URL_DOWNLOAD_PDF, parametri, BollettePagamentiRicerca.this, null, getIntent().getStringExtra("NumeroFattura"));
                        } catch (Exception e) {
                            Intent intentOops = new Intent(getApplicationContext(), OopsPageBollettePagamentiDownloadBolletta.class);
                            intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
                            intentOops.putExtra("ID_PDF", getIntent().getStringExtra("ID_PDF"));
                            intentOops.putExtra("NumeroFattura", getIntent().getStringExtra("NumeroFattura"));
                            intentOops.putExtra("PRIMOCONTO", primoConto);
                            intentOops.putExtra("ORIGINE", Constants.ORIGINE_BOLLETTE_PAGAMENTI_RICERCA);
                            intentOops.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
                            BollettePagamentiRicerca.this.finish();
                            startActivity(intentOops);
                            overridePendingTransition(R.anim.fade, R.anim.hold);
                        }
                    }

                    /**************************************************************************************/
                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void popolaPaginaContenuto(JSONArray rispostaShowListaFattureJSON, LinearLayout pager, final String primoConto) {
        try {
            pager.removeAllViews();
            if (rispostaShowListaFattureJSON.length() > 2) {
                JSONArray listaFatture = rispostaShowListaFattureJSON.getJSONArray(2);
                for (int i = 1; i <= listaFatture.length(); i++) {
                    JSONObject fatturaIesima = listaFatture.getJSONObject(i - 1);
                    View bollette_pagamenti_ricerca_entry = inflater.inflate(R.layout.bollette_pagamenti_ricerca_entry, null);

                    RelativeLayout bolletta = (RelativeLayout) bollette_pagamenti_ricerca_entry.findViewById(R.id.bolletta);
                    if (i % 2 == 0) {
                        bolletta.setBackgroundResource(R.drawable.info_bolletta_black);
                    } else {
                        bolletta.setBackgroundResource(R.drawable.info_bolletta_white);
                    }
                    TextView bolletta_n_valore = (TextView) bollette_pagamenti_ricerca_entry.findViewById(R.id.bolletta_n_valore);
                    bolletta_n_valore.setText(fatturaIesima.getString("NumeroFattura"));

                    TextView bolletta_n_stato = (TextView) bollette_pagamenti_ricerca_entry.findViewById(R.id.bolletta_n_stato);
                    String statoFattura = fatturaIesima.getString("StatoPagamento");
                    bolletta_n_stato.setText(statoFattura);
                    if (statoFattura.equals("PAGATA")) {
                        bolletta_n_stato.setTextColor(getResources().getColor(R.color.green1));
                    } else {
                        bolletta_n_stato.setTextColor(getResources().getColor(R.color.red1));
                    }

                    TextView bolletta_del_data = (TextView) bollette_pagamenti_ricerca_entry.findViewById(R.id.bolletta_del_data);
                    bolletta_del_data.setText(fatturaIesima.getString("DataEmissione"));

                    TextView bolletta_scadenza_data = (TextView) bollette_pagamenti_ricerca_entry.findViewById(R.id.bolletta_scadenza_data);
                    bolletta_scadenza_data.setText(fatturaIesima.getString("Scadenza"));

                    TextView bolletta_importo_valore = (TextView) bollette_pagamenti_ricerca_entry.findViewById(R.id.bolletta_importo_valore);
                    bolletta_importo_valore.setText(fatturaIesima.getString("Importo").concat(getResources().getString(R.string.euro)));

                    TextView bolletta_da_pagare_valore = (TextView) bollette_pagamenti_ricerca_entry.findViewById(R.id.bolletta_da_pagare_valore);
                    bolletta_da_pagare_valore.setText(fatturaIesima.getString("ImportoDaPagare").concat(getResources().getString(R.string.euro)));

                    ImageView bollette_pagamenti_pdf_icon = (ImageView) bollette_pagamenti_ricerca_entry.findViewById(R.id.bollette_pagamenti_pdf_icon);
                    String statoWeb = fatturaIesima.getString("StatoDocFatturaWeb");
                    if (statoWeb.equals("OFFLINE")) {
                        bollette_pagamenti_pdf_icon.setBackgroundResource(R.drawable.pdf_icon_no);
                    } else if (statoWeb.equals("ONLINE")) {
                        bollette_pagamenti_pdf_icon.setBackgroundResource(R.drawable.pdf_icon);
                        final String IdProtocolloDocPdf = fatturaIesima.getString("IdProtocolloDocPdf");
                        final String numeroFattura = fatturaIesima.getString("NumeroFattura");
                        bollette_pagamenti_pdf_icon.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View v) {
                                // chiamata download
                                contenuto = (LinearLayout) findViewById(R.id.contenuto);
                                contenuto.setClickable(false);
                                waiting_download = (LinearLayout) findViewById(R.id.waiting_download);
                                waiting_download.setVisibility(View.VISIBLE);
                                TextView download_status = (TextView) findViewById(R.id.download_status);
                                download_status.setText("download in corso");
                                TextView download_file = (TextView) findViewById(R.id.download_file);
                                download_file.setText("blt_" + numeroFattura + ".pdf");
                                Button download_apri = (Button) findViewById(R.id.download_apri);
                                download_apri.setVisibility(View.GONE);

                                //Baccus
                                Button download_chiudi = (Button) findViewById(R.id.download_chiudi);
                                download_chiudi.setVisibility(View.GONE);

                                animation = Utilities.animation(BollettePagamentiRicerca.this);
                                imageAnim = (ImageView) findViewById(R.id.waiting_anim);
                                imageAnim.setVisibility(View.VISIBLE);
                                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                                    imageAnim.setBackgroundDrawable(animation);
                                } else {
                                    imageAnim.setBackground(animation);
                                }
                                animation.start();

                                if (!isNetworkAvailable(context)) {
                                    Intent intentOops = new Intent(getApplicationContext(), OopsPageBollettePagamentiDownloadBolletta.class);
                                    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.connessione_assente));
                                    intentOops.putExtra("ID_PDF", IdProtocolloDocPdf);
                                    intentOops.putExtra("NumeroFattura", numeroFattura);
                                    BollettePagamentiRicerca.this.finish();
                                    startActivity(intentOops);
                                    waiting_download.setVisibility(View.GONE);
                                    if (animation != null) {
                                        animation.stop();
                                        imageAnim.setVisibility(View.INVISIBLE);
                                    }
                                    overridePendingTransition(R.anim.fade, R.anim.hold);
                                } else {
                                    /********************************** Chiamata Download Pdf *********************************************/
                                    ServiceCall downloadServiceCall = ServiceCallFactory.createServiceCall(context);
                                    // downloadServiceCall.myCookieStore = new
                                    // PersistentCookieStore(context);

                                    HashMap<String, String> parametri = new HashMap<String, String>();
                                    parametri.put("idProtocolloDocPdf", IdProtocolloDocPdf);

                                    try {
                                        downloadServiceCall.executeHttpsGetBollettePagamentiRicerca(Constants.URL_DOWNLOAD_PDF, parametri, BollettePagamentiRicerca.this, null, numeroFattura);
                                    } catch (Exception e) {
                                        Intent intentOops = new Intent(getApplicationContext(), OopsPageBollettePagamentiDownloadBolletta.class);
                                        intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
                                        intentOops.putExtra("ID_PDF", IdProtocolloDocPdf);
                                        intentOops.putExtra("NumeroFattura", numeroFattura);
                                        BollettePagamentiRicerca.this.finish();
                                        startActivity(intentOops);
                                        overridePendingTransition(R.anim.fade, R.anim.hold);
                                    }
                                }

                                /**************************************************************************************/
                            }
                        });
                    }

                    pager.addView(bollette_pagamenti_ricerca_entry);
                }

            } else {
                RelativeLayout stato_elenco_bollette_lista_vuota = (RelativeLayout) findViewById(R.id.stato_elenco_bollette_lista_vuota);
                stato_elenco_bollette_lista_vuota.setVisibility(View.VISIBLE);
            }

            View bollette_pagamenti_ricerca_banner_guida_bolletta = inflater.inflate(R.layout.bollette_pagamenti_ricerca_banner_guida_bolletta, null);
            bollette_pagamenti_ricerca_banner_guida_bolletta.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    // Auto-generated method stub
                    Intent intentHelp = new Intent(getApplicationContext(), GuidaBolletta.class);
                    intentHelp.putExtra("ORIGINE", Constants.ORIGINE_BOLLETTE_PAGAMENTI_RICERCA);
                    intentHelp.putExtra("PRIMOCONTO", primoConto);
                    intentHelp.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
                    BollettePagamentiRicerca.this.finish();
                    startActivity(intentHelp);
                    overridePendingTransition(R.anim.fade, R.anim.hold);
                }
            });
            pager.addView(bollette_pagamenti_ricerca_banner_guida_bolletta);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void postChiamata(String rispostaChiamata, List<Cookie> lista, HashMap<String, String> parametri, String numeroFattura) {
        if (animation != null) {
            animation.stop();
            imageAnim.setVisibility(View.INVISIBLE);
        }
        final ImageView indietro = (ImageView) findViewById(R.id.bollette_pagamenti_indietro);
        indietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
        indietro.getBackground().invalidateSelf();

        JSONArray rispostaJSON = new JSONArray();
        String esito = "";
        try {
            rispostaJSON = new JSONArray(rispostaChiamata);
            JSONObject oggettoRisposta = (JSONObject) rispostaJSON.get(0);
            esito = oggettoRisposta.getString("esito");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (esito.equals("200")) {

            // download
            Constants.SERVIZIO_DOWNLOAD_PDF = rispostaChiamata;

            TextView download_status = (TextView) findViewById(R.id.download_status);
            download_status.setText("download completato");
            if (animation != null) {
                animation.stop();
                imageAnim.setVisibility(View.GONE);
            }
            final Button download_apri = (Button) findViewById(R.id.download_apri);
            download_apri.setVisibility(View.VISIBLE);
            download_apri.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
            download_apri.getBackground().invalidateSelf();
            //Baccus
            //Baccus
            final Button download_chiudi = (Button) findViewById(R.id.download_chiudi);
            download_chiudi.setVisibility(View.VISIBLE);
            download_chiudi.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
            download_chiudi.getBackground().invalidateSelf();
            download_chiudi.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    download_chiudi.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
                    download_chiudi.getBackground().invalidateSelf();
                    waiting_download.setVisibility(View.GONE);
                }
            });

            download_apri.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    download_apri.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
                    download_apri.getBackground().invalidateSelf();
                    String pdfString = "";
                    try {
                        JSONArray rispostaPDFJSON = new JSONArray(Constants.SERVIZIO_DOWNLOAD_PDF);
                        JSONObject pdfJSON = rispostaPDFJSON.getJSONObject(1);
                        pdfString = pdfJSON.getString("Pdf");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    byte[] pdfAsBytes = Base64.decode(pdfString, 0);
                    File fileDirectory = new File(Environment.getExternalStorageDirectory() + "/ENI");
                    File filePath = new File(Environment.getExternalStorageDirectory() + "/ENI/enibolletta.pdf");
                    if (!fileDirectory.exists()) {
                        boolean makedDir = fileDirectory.mkdir();
                        if (makedDir) {
                            // filePath = new
                            // File(Environment.getExternalStorageDirectory()+"/ENI/enibolletta.pdf");
                            if (filePath.length() != 0) {
                                boolean deleted = filePath.delete();
                                if (deleted) {
                                    FileOutputStream os;
                                    try {
                                        os = new FileOutputStream(filePath, true);
                                        os.write(pdfAsBytes);
                                        os.close();
                                    } catch (FileNotFoundException e) {
                                        e.printStackTrace();
                                    } catch (IOException e1) {
                                        e1.printStackTrace();
                                    }
                                }
                            } else {
                                FileOutputStream os;
                                try {
                                    os = new FileOutputStream(filePath, true);
                                    os.write(pdfAsBytes);
                                    os.close();
                                } catch (FileNotFoundException e) {
                                    e.printStackTrace();
                                } catch (IOException e1) {
                                    e1.printStackTrace();
                                }
                            }
                        }
                    } else {
                        if (filePath.length() != 0) {
                            boolean deleted = filePath.delete();
                            if (deleted) {
                                FileOutputStream os;
                                try {
                                    os = new FileOutputStream(filePath, true);
                                    os.write(pdfAsBytes);
                                    os.close();
                                } catch (FileNotFoundException e) {
                                    e.printStackTrace();
                                } catch (IOException e1) {
                                    e1.printStackTrace();
                                }
                            }
                        } else {
                            FileOutputStream os;
                            try {
                                os = new FileOutputStream(filePath, true);
                                os.write(pdfAsBytes);
                                os.close();
                            } catch (FileNotFoundException e) {
                                e.printStackTrace();
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                        }
                    }
                    // apertura
                    // Intent intent = new Intent(Intent.ACTION_VIEW);
                    Uri selectedPdfUri = Uri.fromFile(filePath);
                    viewPdf(selectedPdfUri);

                }
            });

        } else {
            Intent intentOops = new Intent();
            intentOops = new Intent(getApplicationContext(), OopsPageBollettePagamentiDownloadBolletta.class);

            intentOops.putExtra("ID_PDF", parametri.get("idProtocolloDocPdf"));
            intentOops.putExtra("NumeroFattura", numeroFattura);
            intentOops.putExtra("PRIMOCONTO", primoConto);
            intentOops.putExtra("ORIGINE", Constants.ORIGINE_BOLLETTE_PAGAMENTI_RICERCA);
            intentOops.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
            // errori ops page
            if (esito.equals("309")) {
                intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
                BollettePagamentiRicerca.this.finish();
                startActivity(intentOops);
                overridePendingTransition(R.anim.fade, R.anim.hold);

            } else if (esito.equals("400")) {
                intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
                BollettePagamentiRicerca.this.finish();
                startActivity(intentOops);
                overridePendingTransition(R.anim.fade, R.anim.hold);

            } else if (esito.equals("420")) {
                intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
                BollettePagamentiRicerca.this.finish();
                startActivity(intentOops);
                overridePendingTransition(R.anim.fade, R.anim.hold);

            } else if (esito.equals("449")) {
                intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
                BollettePagamentiRicerca.this.finish();
                startActivity(intentOops);
                overridePendingTransition(R.anim.fade, R.anim.hold);

            } else if (esito.equals("451")) {
                intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_popup_451));
                BollettePagamentiRicerca.this.finish();
                startActivity(intentOops);
                overridePendingTransition(R.anim.fade, R.anim.hold);
            }
            // errori popup
            else if (esito.equals("307")) {
                Utils.popup(BollettePagamentiRicerca.this, getResources().getString(R.string.errore_popup_307), "Riprova");

            } else if (esito.equals("435")) {
                Utils.popup(BollettePagamentiRicerca.this, getResources().getString(R.string.errore_popup_435), "Riprova");

            } else if (esito.equals("437")) {
                Utils.popup(BollettePagamentiRicerca.this, getResources().getString(R.string.errore_popup_437), "Riprova");

            } else if (esito.equals("438")) {
                Utils.popup(BollettePagamentiRicerca.this, getResources().getString(R.string.errore_popup_438), "Riprova");

            } else if (esito.equals("443")) {
                Utils.popup(BollettePagamentiRicerca.this, getResources().getString(R.string.errore_popup_443), "Riprova");

            } else if (esito.equals("444")) {
                Utils.popup(BollettePagamentiRicerca.this, getResources().getString(R.string.errore_popup_444), "Riprova");

            } else if (esito.equals("445")) {
                Utils.popup(BollettePagamentiRicerca.this, getResources().getString(R.string.errore_popup_445), "Riprova");

            } else if (esito.equals("446")) {
                Utils.popup(BollettePagamentiRicerca.this, getResources().getString(R.string.errore_popup_446), "Riprova");

            } else if (esito.equals("447")) {
                Utils.popup(BollettePagamentiRicerca.this, getResources().getString(R.string.errore_popup_447), "Riprova");

            } else {
                if (esito.equals("")) {
                    intentOops = new Intent(getApplicationContext(), OopsPageSessioneScaduta.class);
                    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.sessione_scaduta));
                    startActivity(intentOops);
                    overridePendingTransition(R.anim.fade, R.anim.hold);
                }
            }

        }

    }

    public void postChiamataException(boolean exception, HashMap<String, String> parameterMap, String numeroFattura) {
        if (exception) {
            if (animation != null) {
                animation.stop();
                imageAnim.setVisibility(View.INVISIBLE);
            }

            final ImageView indietro = (ImageView) findViewById(R.id.bollette_pagamenti_indietro);
            indietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
            indietro.getBackground().invalidateSelf();

            Intent intentOops = new Intent(getApplicationContext(), OopsPageBollettePagamentiDownloadBolletta.class);
            intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
            intentOops.putExtra("ID_PDF", parameterMap.get("idProtocolloDocPdf"));
            intentOops.putExtra("NumeroFattura", numeroFattura);
            intentOops.putExtra("PRIMOCONTO", primoConto);
            intentOops.putExtra("ORIGINE", Constants.ORIGINE_BOLLETTE_PAGAMENTI_RICERCA);
            intentOops.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
            BollettePagamentiRicerca.this.finish();
            startActivity(intentOops);
            overridePendingTransition(R.anim.fade, R.anim.hold);
        }
    }

    private void viewPdf(Uri file) {
        Intent intent;
        intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(file, "application/pdf");
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("OopsPage", false);
        try {
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            // No application to view, ask to download one
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Nessuna Applicazione Trovata");
            builder.setMessage("Scarica applicazione da Google Play?");
            builder.setPositiveButton("Si, Prego", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Intent marketIntent = new Intent(Intent.ACTION_VIEW);
                    marketIntent.setData(Uri.parse("market://details?id=com.adobe.reader"));
                    startActivity(marketIntent);
                }
            });
            builder.setNegativeButton("No, Grazie", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    waiting_download.setVisibility(View.GONE);
                    contenuto.setClickable(true);
                }
            });
            builder.create().show();
        }

    }

    private boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager == null) return false;
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }

    public void onBackPressed() {
    /*
	 * if(waiting_download != null && waiting_download.getVisibility()==
	 * View.VISIBLE){ TextView download_status =
	 * (TextView)findViewById(R.id.download_status);
	 * if(download_status.getText().toString().equals("download in corso")){
	 * AlertDialog.Builder builder = new AlertDialog.Builder(context);
	 * builder
	 * .setTitle("Annulla download").setMessage(R.string.annulla_download
	 * ).setCancelable(true).setNegativeButton("No", new
	 * DialogInterface.OnClickListener() { public void
	 * onClick(DialogInterface dialog, int id) { }
	 * }).setPositiveButton("Si", new DialogInterface.OnClickListener() {
	 * public void onClick(DialogInterface dialog, int id) {
	 * waiting_download.setVisibility(View.GONE); animation.stop(); } });
	 * builder.create().show(); }else{
	 * waiting_download.setVisibility(View.GONE); animation.stop(); } }else{
	 * animation = new AnimationDrawable();
	 * animation.addFrame(getResources().getDrawable(R.drawable.anim_load2),
	 * 300);
	 * animation.addFrame(getResources().getDrawable(R.drawable.anim_load3),
	 * 300);
	 * animation.addFrame(getResources().getDrawable(R.drawable.anim_load4),
	 * 300); animation.setOneShot(false); imageAnim = (ImageView)
	 * findViewById(R.id.waiting_bollette_pagamenti_ricerca);
	 * imageAnim.setVisibility(View.VISIBLE); if(Build.VERSION.SDK_INT <
	 * 16){ imageAnim.setBackgroundDrawable(animation); }else{
	 * imageAnim.setBackground(animation); } // start the animation!
	 * animation.start();
	 * 
	 * Intent intentBollettePagamenti = new Intent(getApplicationContext(),
	 * BollettePagamenti.class);
	 * intentBollettePagamenti.putExtra("PRIMOCONTO",
	 * getIntent().getStringExtra("PRIMOCONTO"));
	 * intentBollettePagamenti.putExtra("INDICE_SPINNER",
	 * getIntent().getIntExtra("INDICE_SPINNER",0));
	 * BollettePagamentiRicerca.this.finish();
	 * startActivity(intentBollettePagamenti);
	 * overridePendingTransition(R.anim.fade, R.anim.hold); }
	 */

        animation = Utilities.animation(this);
        imageAnim = (ImageView) findViewById(R.id.waiting_anim);
        imageAnim.setVisibility(View.VISIBLE);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
            imageAnim.setBackgroundDrawable(animation);
        } else {
            imageAnim.setBackground(animation);
        }
        animation.start();

        Intent intentBollettePagamenti = new Intent(getApplicationContext(), BollettePagamenti.class);
        intentBollettePagamenti.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
        intentBollettePagamenti.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
        BollettePagamentiRicerca.this.finish();
        startActivity(intentBollettePagamenti);
        overridePendingTransition(R.anim.fade, R.anim.hold);
    }

    public void onResume() {
        super.onResume();
        if (!getIntent().getBooleanExtra("OopsPage", false)) {
            if (waiting_download != null) {
                waiting_download.setVisibility(View.GONE);
            }
        }
    }

    public String dataAttuale() {
        String result = "";
        Calendar now = Calendar.getInstance();
        now.setTime(new Date());
        result = ("01/" + ((("" + (now.get(Calendar.MONTH))).length() == 1) ? "0" : "") + ((now.get(Calendar.MONTH) != 11) ? ((now.get(Calendar.MONTH) + 2) + "/" + now.get(Calendar.YEAR)) : ("01/" + (now.get(Calendar.YEAR) + 1))));
        return result;
    }

    public String dataDueAnniPrima() {
        String result = "";
        Calendar now = Calendar.getInstance();
        now.setTime(new Date());
        result = ("01/" + ((("" + (now.get(Calendar.MONTH))).length() == 1) ? "0" : "") + (now.get(Calendar.MONTH) + 1) + "/" + (now.get(Calendar.YEAR) - 2));
        return result;
    }
}
