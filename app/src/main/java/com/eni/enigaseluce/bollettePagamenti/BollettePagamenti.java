package com.eni.enigaseluce.bollettePagamenti;

import java.util.HashMap;
import java.util.List;

import org.apache.http.cookie.Cookie;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.AnimationDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.eni.enigaseluce.EniApplication;
import com.eni.enigaseluce.R;
import com.eni.enigaseluce.error.OopsPageBollettePagamentiBolletteRateizzabili;
import com.eni.enigaseluce.error.OopsPageBollettePagamentiFromBp;
import com.eni.enigaseluce.error.OopsPageBollettePagamentiProssimaBolletta;
import com.eni.enigaseluce.error.OopsPageSessioneScaduta;
import com.eni.enigaseluce.error.Utils;
import com.eni.enigaseluce.home.Home;
import com.eni.enigaseluce.httpcall.Constants;
import com.eni.enigaseluce.httpcall.ServiceCall;
import com.eni.enigaseluce.httpcall.ServiceCallFactory;
import com.eni.enigaseluce.utils.Log;
import com.eni.enigaseluce.utils.Utilities;

@SuppressLint("NewApi")
public class BollettePagamenti extends Activity {
	private Context context;
	private static Context staticContext;
	AnimationDrawable animation;
	ImageView imageAnim;

	static int positionSpinnerCurrentSelected;
	static int positionSpinnerPrecedentSelected;
	static ScrollView pager;

	public String rispostaLogin = Constants.SERVIZIO_LOGIN;
	public String rispostaShowListaFatture = Constants.SERVIZIO_SHOW_LISTA_FATTURE;

	public static String nomeInteroCliente = "";
	static LayoutInflater inflater;
	private String array_conti_spinner[];
	String codiceEsito;
	ArrayAdapter<String> adapterSpinnerConti;
	String primoConto;
	String tipologia;
	String rateGasOLuce;
	private static boolean tipologiaCambiata = false;

	public static Context getContext() {
		return BollettePagamenti.staticContext;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.bollette_pagamenti);

		((EniApplication)getApplication()).sendAnalyticsEvent("[Android] - bollette e pagamenti");

		positionSpinnerCurrentSelected = getIntent().getIntExtra("INDICE_SPINNER", 0);
		positionSpinnerPrecedentSelected = getIntent().getIntExtra("INDICE_SPINNER", 0);

		context = this;
		BollettePagamenti.staticContext = this;
		primoConto = getIntent().getStringExtra("PRIMOCONTO");

	}

	@Override
	protected void onStart() {
		super.onStart();

		primoConto = getIntent().getStringExtra("PRIMOCONTO");

		final ImageView indietro = (ImageView) findViewById(R.id.bollette_pagamenti_indietro);
		indietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
		indietro.getBackground().invalidateSelf();
		indietro.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				indietro.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
				indietro.getBackground().invalidateSelf();

				animation = Utilities.animation(BollettePagamenti.this);
				imageAnim = (ImageView) findViewById(R.id.waiting_anim);
				imageAnim.setVisibility(View.VISIBLE);
				if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
					imageAnim.setBackgroundDrawable(animation);
				}
				else {
					imageAnim.setBackground(animation);
				}
				animation.start();

				Intent intentHome = new Intent(getApplicationContext(), Home.class);
				BollettePagamenti.this.finish();
				startActivity(intentHome);
				overridePendingTransition(R.anim.fade, R.anim.hold);
			}
		});

		/*
		 * final ImageView help =
		 * (ImageView)findViewById(R.id.bollette_pagamenti_help);
		 * help.getBackground().setColorFilter(0xFFFFFFFF,
		 * PorterDuff.Mode.MULTIPLY); help.getBackground().invalidateSelf();
		 * help.setOnClickListener(new View.OnClickListener() { public void
		 * onClick(View v) { help.getBackground().setColorFilter(0xFF999999,
		 * PorterDuff.Mode.MULTIPLY); help.getBackground().invalidateSelf();
		 * animation = new AnimationDrawable();
		 * animation.addFrame(getResources().getDrawable(R.drawable.anim_load2),
		 * 300);
		 * animation.addFrame(getResources().getDrawable(R.drawable.anim_load3),
		 * 300);
		 * animation.addFrame(getResources().getDrawable(R.drawable.anim_load4),
		 * 300); animation.setOneShot(false); imageAnim = (ImageView)
		 * findViewById(R.id.waiting_bollette_pagamenti);
		 * imageAnim.setVisibility(View.VISIBLE); if(Build.VERSION.SDK_INT <
		 * 16){ imageAnim.setBackgroundDrawable(animation); }else{
		 * imageAnim.setBackground(animation); } // start the animation!
		 * animation.start();
		 * 
		 * Intent intentHelp = new Intent(getApplicationContext(),
		 * BollettePagamentiHelp.class); intentHelp.putExtra("ORIGINE",
		 * Constants.ORIGINE_BOLLETTE_PAGAMENTI);
		 * intentHelp.putExtra("PRIMOCONTO",
		 * array_conti_spinner[positionSpinnerCurrentSelected].trim());
		 * intentHelp.putExtra("INDICE_SPINNER",
		 * positionSpinnerCurrentSelected); BollettePagamenti.this.finish();
		 * startActivity(intentHelp); overridePendingTransition(R.anim.fade,
		 * R.anim.hold); } });
		 */
		try {
			rispostaLogin = Constants.SERVIZIO_LOGIN;
			final JSONArray bollettePagamentiLogin = new JSONArray(rispostaLogin);
			int lunghezzaStAvForn = numeroCodiciContoDiversiAttiviCompletieNonAttivi(bollettePagamentiLogin);
			JSONObject esito = bollettePagamentiLogin.getJSONObject(0);
			codiceEsito = (String) esito.getString("esito");
			if (codiceEsito.equals("200")) {
				JSONObject datiAnagr = bollettePagamentiLogin.getJSONObject(1);

				// ultimo accesso
				String ultimoaccesso = datiAnagr.getString("UltimoAccesso");
				TextView ultimoAccesso = (TextView) findViewById(R.id.ultimo_accesso);
				// ultimoAccesso.setTextSize(11);
				ultimoAccesso.setText(ultimoaccesso);

				// nome cliente
				String nomeClienteString = datiAnagr.getString("NomeCliente");
				String cognomeClienteString = datiAnagr.getString("CognomeCliente");
				TextView nomeCliente = (TextView) findViewById(R.id.nome_cliente);
				nomeInteroCliente = nomeClienteString + " " + cognomeClienteString;
				nomeCliente.setText(nomeClienteString + " " + cognomeClienteString);

				array_conti_spinner = new String[lunghezzaStAvForn];
				array_conti_spinner = arrayCodiciContoDiversiUtilizabiliInteriENonUtilizzabili(bollettePagamentiLogin, lunghezzaStAvForn);

				final Spinner codiciConti = (Spinner) findViewById(R.id.numero_cliente);
				// final ArrayAdapter<String> adapterSpinnerConti = new
				// ArrayAdapter<String>(this,
				// android.R.layout.simple_spinner_item, array_conti_spinner);
				final ArrayAdapter<String> adapterSpinnerConti = new ArrayAdapter<String>(this, R.layout.spinner_dialog_riga, R.id.spinner_riga_text, array_conti_spinner);
				codiciConti.setAdapter(adapterSpinnerConti);
				codiciConti.setPrompt(getResources().getString(R.string.seleziona_il_conto_cliente));
				codiciConti.setSelection(positionSpinnerCurrentSelected);

				pager = (ScrollView) findViewById(R.id.bollette_pagamenti_pager);
				pager.removeAllViews();
				inflater = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);
				rispostaShowListaFatture = Constants.SERVIZIO_SHOW_LISTA_FATTURE;
				if (!rispostaShowListaFatture.equals("")) {
					popolaPaginaContenuto(bollettePagamentiLogin, new JSONArray(Constants.SERVIZIO_SHOW_LISTA_FATTURE), pager, primoConto.trim());
				}

				codiciConti.setOnItemSelectedListener(new OnItemSelectedListener() {
					public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
						Object obj = (Object) parent.getSelectedItem();
						String codiceContoCliente = obj.toString().trim();
						if (!codiceContoCliente.equals(adapterSpinnerConti.getItem(positionSpinnerCurrentSelected).toString().trim())) {
							positionSpinnerPrecedentSelected = positionSpinnerCurrentSelected;
							positionSpinnerCurrentSelected = codiciConti.getLastVisiblePosition();

							animation = Utilities.animation(BollettePagamenti.this);
							imageAnim = (ImageView) findViewById(R.id.waiting_anim);
							imageAnim.setVisibility(View.VISIBLE);
							if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
								imageAnim.setBackgroundDrawable(animation);
							}
							else {
								imageAnim.setBackground(animation);
							}
							animation.start();

							String codiceCliente = getCodiceCliente(rispostaLogin);
							if (!isNetworkAvailable(context)) {
								Intent intentOops = new Intent(getApplicationContext(), OopsPageBollettePagamentiFromBp.class);
								intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.connessione_assente));
								intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
								intentOops.putExtra("CODICE_CONTO_CLIENTE", codiceContoCliente);
								positionSpinnerCurrentSelected = positionSpinnerPrecedentSelected;
								// StatoAvanzamentoFornitura.this.finish();
								startActivity(intentOops);
								if (animation != null) {
									animation.stop();
									imageAnim.setVisibility(View.INVISIBLE);
								}
								overridePendingTransition(R.anim.fade, R.anim.hold);
							}
							else {
								/********************* Chiamata statoAvanzamento Fornitura ******************************/
								ServiceCall stAvFornServiceCall = ServiceCallFactory.createServiceCall(context);
								// stAvFornServiceCall.myCookieStore = new
								// PersistentCookieStore(context);

								HashMap<String, String> parametri = new HashMap<String, String>();
								parametri.put("codiceCliente", codiceCliente);
								parametri.put("codiceContoCliente", codiceContoCliente);

								try {
									stAvFornServiceCall.executeHttpsGetBollettePagamenti(Constants.URL_SHOW_LISTA_FATTURE, parametri, BollettePagamenti.this, null, false, false);
								}
								catch (Exception e) {
									Intent intentOops = new Intent(getApplicationContext(), OopsPageBollettePagamentiFromBp.class);
									intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
									intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
									intentOops.putExtra("CODICE_CONTO_CLIENTE", codiceContoCliente);
									positionSpinnerCurrentSelected = positionSpinnerPrecedentSelected;
									startActivity(intentOops);
									overridePendingTransition(R.anim.fade, R.anim.hold);
								}
							}
						}
					}

					public void onNothingSelected(AdapterView<?> arg0) {
					}
				});

			}
			else {
				Intent intentOops = new Intent(getApplicationContext(), OopsPageBollettePagamentiFromBp.class);
				intentOops.putExtra("CODICE_CLIENTE", getCodiceCliente(rispostaLogin));
				intentOops.putExtra("CODICE_CONTO_CLIENTE", primoConto);
				positionSpinnerCurrentSelected = positionSpinnerPrecedentSelected;
				// errori ops page
				if (esito.equals("309")) {
					intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
					startActivity(intentOops);
					overridePendingTransition(R.anim.fade, R.anim.hold);
				}
				else if (esito.equals("400")) {
					intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
					startActivity(intentOops);
					overridePendingTransition(R.anim.fade, R.anim.hold);
				}
				else if (esito.equals("420")) {
					intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
					startActivity(intentOops);
					overridePendingTransition(R.anim.fade, R.anim.hold);
				}
				else if (esito.equals("449")) {
					intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
					startActivity(intentOops);
					overridePendingTransition(R.anim.fade, R.anim.hold);
				}
				else if (esito.equals("451")) {
					intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_popup_451));
					startActivity(intentOops);
					overridePendingTransition(R.anim.fade, R.anim.hold);
				}
				// errori popup
				else if (esito.equals("307")) {
					Utils.popup(BollettePagamenti.this, getResources().getString(R.string.errore_popup_307), "Riprova");
				}
				else if (esito.equals("435")) {
					Utils.popup(BollettePagamenti.this, getResources().getString(R.string.errore_popup_435), "Riprova");
				}
				else if (esito.equals("437")) {
					Utils.popup(BollettePagamenti.this, getResources().getString(R.string.errore_popup_437), "Riprova");
				}
				else if (esito.equals("438")) {
					Utils.popup(BollettePagamenti.this, getResources().getString(R.string.errore_popup_438), "Riprova");
				}
				else if (esito.equals("443")) {
					Utils.popup(BollettePagamenti.this, getResources().getString(R.string.errore_popup_443), "Riprova");
				}
				else if (esito.equals("444")) {
					Utils.popup(BollettePagamenti.this, getResources().getString(R.string.errore_popup_444), "Riprova");
				}
				else if (esito.equals("445")) {
					Utils.popup(BollettePagamenti.this, getResources().getString(R.string.errore_popup_445), "Riprova");
				}
				else if (esito.equals("446")) {
					Utils.popup(BollettePagamenti.this, getResources().getString(R.string.errore_popup_446), "Riprova");
				}
				else if (esito.equals("447")) {
					Utils.popup(BollettePagamenti.this, getResources().getString(R.string.errore_popup_447), "Riprova");
				}
				else {
					intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_popup_451));
					startActivity(intentOops);
					overridePendingTransition(R.anim.fade, R.anim.hold);
				}
			}
		}
		catch (JSONException e) {
			e.printStackTrace();
		}
	}

	private void popolaPaginaContenuto(final JSONArray bollettePagamentiLogin, JSONArray bolletteLista, ScrollView pager, final String primoConto) {
		try {
			pager.removeAllViews();
			JSONObject estrattoConto = bolletteLista.getJSONObject(1);

			final View bollette_pagamenti_entry = inflater.inflate(R.layout.bollette_pagamenti_entry, null);

			TextView bollette_pagamenti_non_scaduto_valore = (TextView) bollette_pagamenti_entry.findViewById(R.id.bollette_pagamenti_non_scaduto_valore);
			bollette_pagamenti_non_scaduto_valore.setText(estrattoConto.getString("DebitoNonScaduto") + getResources().getString(R.string.euro));

			TextView bollette_pagamenti_scaduto_valore = (TextView) bollette_pagamenti_entry.findViewById(R.id.bollette_pagamenti_scaduto_valore);
			bollette_pagamenti_scaduto_valore.setText(estrattoConto.getString("DebitoScaduto") + getResources().getString(R.string.euro));

			TextView bollette_pagamenti_debito_totale_valore = (TextView) bollette_pagamenti_entry.findViewById(R.id.bollette_pagamenti_debito_totale_valore);
			bollette_pagamenti_debito_totale_valore.setText(estrattoConto.getString("DebitoTotale") + getResources().getString(R.string.euro));

			TextView bollette_pagamenti_saldo_valore = (TextView) bollette_pagamenti_entry.findViewById(R.id.bollette_pagamenti_saldo_valore);
			bollette_pagamenti_saldo_valore.setText(estrattoConto.getString("Saldo") + getResources().getString(R.string.euro));

			TextView bollette_pagamenti_credito_valore = (TextView) bollette_pagamenti_entry.findViewById(R.id.bollette_pagamenti_credito_valore);
			bollette_pagamenti_credito_valore.setText(estrattoConto.getString("CreditoTotale") + getResources().getString(R.string.euro));

			final boolean isDual = isDual(bollettePagamentiLogin, primoConto);
			final ImageView luce_icona = (ImageView) bollette_pagamenti_entry.findViewById(R.id.bollette_pagamenti_light_icona);
			final ImageView gas_icona = (ImageView) bollette_pagamenti_entry.findViewById(R.id.bollette_pagamenti_gas_icona);
			final TextView bollette_pagamenti_indirizzo = (TextView) bollette_pagamenti_entry.findViewById(R.id.bollette_pagamenti_indirizzo);
			final TextView bollette_pagamenti_tipologia_valore = (TextView) bollette_pagamenti_entry.findViewById(R.id.bollette_pagamenti_tipologia_valore);

			final String tipologiaConto = cercaTipologiaFornitura(bollettePagamentiLogin, primoConto);

			// inizializzazione della tipologia
			if (isDual) {
				tipologia = "DUAL";
			}
			else if (tipologiaConto.equals("GAS")) {
				tipologia = "GAS";
				rateGasOLuce = "GAS";
			}
			else if (tipologiaConto.equals("POWER")) {
				tipologia = "LUCE";
				rateGasOLuce = "POWER";
			}
			// Tendina
			final RelativeLayout no_tendina = (RelativeLayout) bollette_pagamenti_entry.findViewById(R.id.no_tendina);
			final RelativeLayout tendina_doppia = (RelativeLayout) bollette_pagamenti_entry.findViewById(R.id.tendina_doppia);
			final RelativeLayout tendina_singola = (RelativeLayout) bollette_pagamenti_entry.findViewById(R.id.tendina_singola);

			final RelativeLayout tendina_doppia_entry = (RelativeLayout) bollette_pagamenti_entry.findViewById(R.id.tendina_doppia_entry);
			final RelativeLayout tendina_singola_entry = (RelativeLayout) bollette_pagamenti_entry.findViewById(R.id.tendina_singola_entry);

			final String indirizzoFornitura = cercaIndirizzoFornitura(bollettePagamentiLogin, primoConto);
			final String tipoProdottoText = trovaTipoProdottoFornitura(bollettePagamentiLogin, primoConto);

			no_tendina.setVisibility(View.VISIBLE);
			tendina_singola.setVisibility(View.GONE);
			tendina_singola_entry.setVisibility(View.GONE);

			tendina_doppia.setVisibility(View.GONE);
			tendina_doppia_entry.setVisibility(View.GONE);

			no_tendina.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					if (isDual) {
						tipologia = "DUAL";
						// tendina doppia
						no_tendina.setVisibility(View.GONE);
						tendina_singola.setVisibility(View.GONE);
						tendina_singola_entry.setVisibility(View.GONE);

						tendina_doppia.setVisibility(View.VISIBLE);
						tendina_doppia_entry.setVisibility(View.VISIBLE);

						tendina_doppia.setOnClickListener(new View.OnClickListener() {
							public void onClick(View v) {
								no_tendina.setVisibility(View.VISIBLE);
								tendina_singola.setVisibility(View.GONE);
								tendina_singola_entry.setVisibility(View.GONE);

								tendina_doppia.setVisibility(View.GONE);
								tendina_doppia_entry.setVisibility(View.GONE);

							}
						});

						final String indirizzoFornituraGas = cercaIndirizzoFornituraTipologia(bollettePagamentiLogin, primoConto, "GAS");
						final String indirizzoFornituraLuce = cercaIndirizzoFornituraTipologia(bollettePagamentiLogin, primoConto, "POWER");
						final String tipoProdottoTextGas = trovaTipoProdottoFornituraTipologia(bollettePagamentiLogin, primoConto, "GAS");
						final String tipoProdottoTextLuce = trovaTipoProdottoFornituraTipologia(bollettePagamentiLogin, primoConto, "POWER");

						if (tipologiaConto.equals("POWER")) {

							if (!tipologiaCambiata) {
								// entry luce disabilitata
								ImageView tendina_doppia_icona1 = (ImageView) bollette_pagamenti_entry.findViewById(R.id.tendina_doppia_icona1);
								tendina_doppia_icona1.setBackgroundResource(R.drawable.light_tendina_off);

								final TextView tendina_doppia_text1 = (TextView) bollette_pagamenti_entry.findViewById(R.id.tendina_doppia_text1);
								tendina_doppia_text1.setText("Luce - " + indirizzoFornituraLuce);
								tendina_doppia_text1.setTextColor(getResources().getColor(R.color.grey1));

								ImageView tendina_doppia_arrow1 = (ImageView) bollette_pagamenti_entry.findViewById(R.id.tendina_doppia_arrow1);
								tendina_doppia_arrow1.setBackgroundResource(R.drawable.arrow_disabled);
								// entry gas abilitata
								ImageView tendina_doppia_icona2 = (ImageView) bollette_pagamenti_entry.findViewById(R.id.tendina_doppia_icona2);
								tendina_doppia_icona2.setBackgroundResource(R.drawable.gas_tendina_on);

								final TextView tendina_doppia_text2 = (TextView) bollette_pagamenti_entry.findViewById(R.id.tendina_doppia_text2);
								tendina_doppia_text2.setText("Gas - " + indirizzoFornituraGas);
								tendina_doppia_text2.setTextColor(getResources().getColor(R.color.black));

								ImageView tendina_doppia_arrow2 = (ImageView) bollette_pagamenti_entry.findViewById(R.id.tendina_doppia_arrow2);
								tendina_doppia_arrow2.setBackgroundResource(R.drawable.arrow_enabled);

								tendina_doppia_text2.setOnClickListener(new View.OnClickListener() {
									public void onClick(View v) {
										no_tendina.setVisibility(View.VISIBLE);
										tendina_singola.setVisibility(View.GONE);
										tendina_singola_entry.setVisibility(View.GONE);

										tendina_doppia.setVisibility(View.GONE);
										tendina_doppia_entry.setVisibility(View.GONE);

										bollette_pagamenti_indirizzo.setText(indirizzoFornituraGas);
										luce_icona.setBackgroundResource(R.drawable.light_no);
										gas_icona.setBackgroundResource(R.drawable.gas_yes);
										bollette_pagamenti_tipologia_valore.setText(tipoProdottoTextGas);

										tipologiaCambiata = true;
										rateGasOLuce = "GAS";
									}
								});
								tendina_doppia_text1.setClickable(false);
							}
							else {
								// entry luce abilitata
								ImageView tendina_doppia_icona1 = (ImageView) bollette_pagamenti_entry.findViewById(R.id.tendina_doppia_icona1);
								tendina_doppia_icona1.setBackgroundResource(R.drawable.light_tendina_on);

								final TextView tendina_doppia_text1 = (TextView) bollette_pagamenti_entry.findViewById(R.id.tendina_doppia_text1);
								tendina_doppia_text1.setText("Luce - " + indirizzoFornituraLuce);
								tendina_doppia_text1.setTextColor(getResources().getColor(R.color.black));

								ImageView tendina_doppia_arrow1 = (ImageView) bollette_pagamenti_entry.findViewById(R.id.tendina_doppia_arrow1);
								tendina_doppia_arrow1.setBackgroundResource(R.drawable.arrow_enabled);
								// entry gas disabilitata
								ImageView tendina_doppia_icona2 = (ImageView) bollette_pagamenti_entry.findViewById(R.id.tendina_doppia_icona2);
								tendina_doppia_icona2.setBackgroundResource(R.drawable.gas_tendina_off);

								final TextView tendina_doppia_text2 = (TextView) bollette_pagamenti_entry.findViewById(R.id.tendina_doppia_text2);
								tendina_doppia_text2.setText("Gas - " + indirizzoFornituraGas);
								tendina_doppia_text2.setTextColor(getResources().getColor(R.color.grey1));

								ImageView tendina_doppia_arrow2 = (ImageView) bollette_pagamenti_entry.findViewById(R.id.tendina_doppia_arrow2);
								tendina_doppia_arrow2.setBackgroundResource(R.drawable.arrow_disabled);

								tendina_doppia_text1.setOnClickListener(new View.OnClickListener() {
									public void onClick(View v) {
										no_tendina.setVisibility(View.VISIBLE);
										tendina_singola.setVisibility(View.GONE);
										tendina_singola_entry.setVisibility(View.GONE);

										tendina_doppia.setVisibility(View.GONE);
										tendina_doppia_entry.setVisibility(View.GONE);

										bollette_pagamenti_indirizzo.setText(indirizzoFornituraLuce);
										luce_icona.setBackgroundResource(R.drawable.light_yes);
										gas_icona.setBackgroundResource(R.drawable.gas_no);
										bollette_pagamenti_tipologia_valore.setText(tipoProdottoTextLuce);

										rateGasOLuce = "POWER";
										tipologiaCambiata = false;
									}
								});

								tendina_doppia_text2.setClickable(false);
							}

						}
						else if (tipologiaConto.equals("GAS")) {
							if (!tipologiaCambiata) {
								// entry luce abilitata
								ImageView tendina_doppia_icona1 = (ImageView) bollette_pagamenti_entry.findViewById(R.id.tendina_doppia_icona1);
								tendina_doppia_icona1.setBackgroundResource(R.drawable.light_tendina_on);

								TextView tendina_doppia_text1 = (TextView) bollette_pagamenti_entry.findViewById(R.id.tendina_doppia_text1);
								tendina_doppia_text1.setText("Luce - " + indirizzoFornituraLuce);
								tendina_doppia_text1.setTextColor(getResources().getColor(R.color.black));

								ImageView tendina_doppia_arrow1 = (ImageView) bollette_pagamenti_entry.findViewById(R.id.tendina_doppia_arrow1);
								tendina_doppia_arrow1.setBackgroundResource(R.drawable.arrow_enabled);
								// entry gas disabilitata
								ImageView tendina_doppia_icona2 = (ImageView) bollette_pagamenti_entry.findViewById(R.id.tendina_doppia_icona2);
								tendina_doppia_icona2.setBackgroundResource(R.drawable.gas_tendina_off);

								TextView tendina_doppia_text2 = (TextView) bollette_pagamenti_entry.findViewById(R.id.tendina_doppia_text2);
								tendina_doppia_text2.setText("Gas - " + indirizzoFornituraGas);
								tendina_doppia_text2.setTextColor(getResources().getColor(R.color.grey1));

								ImageView tendina_doppia_arrow2 = (ImageView) bollette_pagamenti_entry.findViewById(R.id.tendina_doppia_arrow2);
								tendina_doppia_arrow2.setBackgroundResource(R.drawable.arrow_disabled);

								tendina_doppia_text1.setOnClickListener(new View.OnClickListener() {
									public void onClick(View v) {
										no_tendina.setVisibility(View.VISIBLE);
										tendina_singola.setVisibility(View.GONE);
										tendina_singola_entry.setVisibility(View.GONE);

										tendina_doppia.setVisibility(View.GONE);
										tendina_doppia_entry.setVisibility(View.GONE);

										bollette_pagamenti_indirizzo.setText(indirizzoFornituraLuce);
										luce_icona.setBackgroundResource(R.drawable.light_yes);
										gas_icona.setBackgroundResource(R.drawable.gas_no);
										bollette_pagamenti_tipologia_valore.setText(tipoProdottoTextLuce);

										rateGasOLuce = "POWER";
										tipologiaCambiata = true;
									}
								});
								tendina_doppia_text2.setClickable(false);
							}
							else {
								// entry luce disabilitata
								ImageView tendina_doppia_icona1 = (ImageView) bollette_pagamenti_entry.findViewById(R.id.tendina_doppia_icona1);
								tendina_doppia_icona1.setBackgroundResource(R.drawable.light_tendina_off);

								TextView tendina_doppia_text1 = (TextView) bollette_pagamenti_entry.findViewById(R.id.tendina_doppia_text1);
								tendina_doppia_text1.setText("Luce - " + indirizzoFornituraLuce);
								tendina_doppia_text1.setTextColor(getResources().getColor(R.color.grey1));

								ImageView tendina_doppia_arrow1 = (ImageView) bollette_pagamenti_entry.findViewById(R.id.tendina_doppia_arrow1);
								tendina_doppia_arrow1.setBackgroundResource(R.drawable.arrow_disabled);
								// entry gas abilitata
								ImageView tendina_doppia_icona2 = (ImageView) bollette_pagamenti_entry.findViewById(R.id.tendina_doppia_icona2);
								tendina_doppia_icona2.setBackgroundResource(R.drawable.gas_tendina_on);

								TextView tendina_doppia_text2 = (TextView) bollette_pagamenti_entry.findViewById(R.id.tendina_doppia_text2);
								tendina_doppia_text2.setText("Gas - " + indirizzoFornituraGas);
								tendina_doppia_text2.setTextColor(getResources().getColor(R.color.black));

								ImageView tendina_doppia_arrow2 = (ImageView) bollette_pagamenti_entry.findViewById(R.id.tendina_doppia_arrow2);
								tendina_doppia_arrow2.setBackgroundResource(R.drawable.arrow_enabled);

								tendina_doppia_text2.setOnClickListener(new View.OnClickListener() {
									public void onClick(View v) {
										no_tendina.setVisibility(View.VISIBLE);
										tendina_singola.setVisibility(View.GONE);
										tendina_singola_entry.setVisibility(View.GONE);

										tendina_doppia.setVisibility(View.GONE);
										tendina_doppia_entry.setVisibility(View.GONE);

										bollette_pagamenti_indirizzo.setText(indirizzoFornituraLuce);
										luce_icona.setBackgroundResource(R.drawable.light_no);
										gas_icona.setBackgroundResource(R.drawable.gas_yes);
										bollette_pagamenti_tipologia_valore.setText(tipoProdottoTextGas);

										rateGasOLuce = "GAS";
										tipologiaCambiata = false;
									}
								});
								tendina_doppia_text1.setClickable(false);
							}
						}

					}
					else {
						// tendina singola
						no_tendina.setVisibility(View.GONE);
						tendina_singola.setVisibility(View.VISIBLE);
						tendina_singola_entry.setVisibility(View.VISIBLE);

						tendina_doppia.setVisibility(View.GONE);
						tendina_doppia_entry.setVisibility(View.GONE);

						tendina_singola.setOnClickListener(new View.OnClickListener() {
							public void onClick(View v) {
								no_tendina.setVisibility(View.VISIBLE);
								tendina_singola.setVisibility(View.GONE);
								tendina_singola_entry.setVisibility(View.GONE);

								tendina_doppia.setVisibility(View.GONE);
								tendina_doppia_entry.setVisibility(View.GONE);

							}
						});

						if (tipologiaConto.equals("POWER")) {
							// tipologia = "LUCE";

							// entry luce disabilitata
							ImageView tendina_singola_icona = (ImageView) bollette_pagamenti_entry.findViewById(R.id.tendina_singola_icona);
							tendina_singola_icona.setBackgroundResource(R.drawable.light_tendina_off);

							TextView tendina_singola_text = (TextView) bollette_pagamenti_entry.findViewById(R.id.tendina_singola_text);
							tendina_singola_text.setText("Luce - " + indirizzoFornitura);
							tendina_singola_text.setTextColor(getResources().getColor(R.color.grey1));

							ImageView tendina_singola_arrow = (ImageView) bollette_pagamenti_entry.findViewById(R.id.tendina_singola_arrow);
							tendina_singola_arrow.setBackgroundResource(R.drawable.arrow_disabled);

							bollette_pagamenti_indirizzo.setText(indirizzoFornitura);
							luce_icona.setBackgroundResource(R.drawable.light_yes);
							gas_icona.setBackgroundResource(R.drawable.gas_no);
							bollette_pagamenti_tipologia_valore.setText(tipoProdottoText);

						}
						else if (tipologiaConto.equals("GAS")) {
							// tipologia = "GAS";

							ImageView tendina_singola_icona = (ImageView) bollette_pagamenti_entry.findViewById(R.id.tendina_singola_icona);
							tendina_singola_icona.setBackgroundResource(R.drawable.gas_tendina_off);

							TextView tendina_singola_text = (TextView) bollette_pagamenti_entry.findViewById(R.id.tendina_singola_text);
							tendina_singola_text.setText("Gas - " + indirizzoFornitura);
							tendina_singola_text.setTextColor(getResources().getColor(R.color.grey1));

							ImageView tendina_singola_arrow = (ImageView) bollette_pagamenti_entry.findViewById(R.id.tendina_singola_arrow);
							tendina_singola_arrow.setBackgroundResource(R.drawable.arrow_disabled);

							bollette_pagamenti_indirizzo.setText(indirizzoFornitura);
							luce_icona.setBackgroundResource(R.drawable.light_no);
							gas_icona.setBackgroundResource(R.drawable.gas_yes);
							bollette_pagamenti_tipologia_valore.setText(tipoProdottoText);
						}
					}
				}
			});

			if (tipologiaConto.equals("POWER")) {
				// tipologia = "LUCE";
				bollette_pagamenti_indirizzo.setText(indirizzoFornitura);
				luce_icona.setBackgroundResource(R.drawable.light_yes);
				gas_icona.setBackgroundResource(R.drawable.gas_no);
				bollette_pagamenti_tipologia_valore.setText(tipoProdottoText);
				no_tendina.setClickable(true);
			}
			else if (tipologiaConto.equals("GAS")) {
				// tipologia = "GAS";
				bollette_pagamenti_indirizzo.setText(indirizzoFornitura);
				luce_icona.setBackgroundResource(R.drawable.light_no);
				gas_icona.setBackgroundResource(R.drawable.gas_yes);
				bollette_pagamenti_tipologia_valore.setText(tipoProdottoText);
				no_tendina.setClickable(true);
			}
			else {
				no_tendina.setClickable(false);
			}

			TextView bollette_pagamenti_pagamenti_valore = (TextView) bollette_pagamenti_entry.findViewById(R.id.bollette_pagamenti_pagamenti_valore);

			float debitoScaduto = Float.valueOf(estrattoConto.getString("DebitoScaduto").replace(".", "").replace(",", "."));
			if (debitoScaduto > 0) {
				bollette_pagamenti_pagamenti_valore.setText("Verifica bollette da pagare");
				bollette_pagamenti_pagamenti_valore.setTextColor(getResources().getColor(R.color.red1));
			}
			else {
				bollette_pagamenti_pagamenti_valore.setText("Regolari");
				bollette_pagamenti_pagamenti_valore.setTextColor(getResources().getColor(R.color.green1));
			}
			final RelativeLayout bollette_pagamenti_navigation_bar = (RelativeLayout) findViewById(R.id.bollette_pagamenti_navigation_bar);

			boolean isContoAttivo = isContoAttivo(bollettePagamentiLogin, primoConto);
			final ImageView bollette_pagamenti_prossima_bolletta_btn = (ImageView) findViewById(R.id.bollette_pagamenti_prossima_bolletta_btn);
			final ImageView bollette_pagamenti_ricerca_bolletta_btn = (ImageView) findViewById(R.id.bollette_pagamenti_ricerca_bolletta_btn);
			final ImageView bollette_pagamenti_bollette_rateizzabili_btn = (ImageView) findViewById(R.id.bollette_pagamenti_bollette_rateizzabili_btn);
			if (isContoAttivo) {
				TextView stato_estrato_conto = (TextView) bollette_pagamenti_entry.findViewById(R.id.bollette_pagamenti_situazione_estratto_stato);
				stato_estrato_conto.setText("Attivo");
				stato_estrato_conto.setTextColor(getResources().getColor(R.color.green1));
				bollette_pagamenti_navigation_bar.setBackgroundResource(R.drawable.bollette_footer_on);

				bollette_pagamenti_ricerca_bolletta_btn.setOnClickListener(new View.OnClickListener() {
					public void onClick(View v) {
						bollette_pagamenti_navigation_bar.setBackgroundResource(R.drawable.bollette_footer_elenco);
						elencoBollette();
					}
				});

				bollette_pagamenti_prossima_bolletta_btn.setOnClickListener(new View.OnClickListener() {
					public void onClick(View v) {
						bollette_pagamenti_navigation_bar.setBackgroundResource(R.drawable.bollette_footer_prox);

						animation = Utilities.animation(BollettePagamenti.this);
						imageAnim = (ImageView) findViewById(R.id.waiting_anim);
						imageAnim.setVisibility(View.VISIBLE);
						if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
							imageAnim.setBackgroundDrawable(animation);
						}
						else {
							imageAnim.setBackground(animation);
						}
						animation.start();

						String codiceCliente = getCodiceCliente(rispostaLogin);
						if (!isNetworkAvailable(context)) {
							Intent intentOops = new Intent(getApplicationContext(), OopsPageBollettePagamentiProssimaBolletta.class);
							intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.connessione_assente));
							intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
							intentOops.putExtra("CODICE_CONTO_CLIENTE", array_conti_spinner[positionSpinnerCurrentSelected].trim());
							intentOops.putExtra("TIPOLOGIA", tipologia);
							// StatoAvanzamentoFornitura.this.finish();
							startActivity(intentOops);
							if (animation != null) {
								animation.stop();
								imageAnim.setVisibility(View.INVISIBLE);
							}
							overridePendingTransition(R.anim.fade, R.anim.hold);
						}
						else {
							/********************************** chiamata info prossima fattura ***********************************/
							ServiceCall stAvFornServiceCall = ServiceCallFactory.createServiceCall(context);
							// stAvFornServiceCall.myCookieStore = new
							// PersistentCookieStore(context);

							HashMap<String, String> parametri = new HashMap<String, String>();
							parametri.put("codiceCliente", codiceCliente);
							parametri.put("codiceContoCliente", array_conti_spinner[positionSpinnerCurrentSelected].trim());

							try {
								stAvFornServiceCall.executeHttpsGetBollettePagamenti(Constants.URL_INFO_PROSSIMA_FATTURA, parametri, BollettePagamenti.this, null, true, false);
							}
							catch (Exception e) {
								Intent intentOops = new Intent(getApplicationContext(), OopsPageBollettePagamentiProssimaBolletta.class);
								intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
								intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
								intentOops.putExtra("CODICE_CONTO_CLIENTE", array_conti_spinner[positionSpinnerCurrentSelected].trim());
								intentOops.putExtra("TIPOLOGIA", tipologia);
								startActivity(intentOops);
								overridePendingTransition(R.anim.fade, R.anim.hold);
							}
						}
						/***************************************************************************************************/

					}
				});

				bollette_pagamenti_bollette_rateizzabili_btn.setOnClickListener(new View.OnClickListener() {
					public void onClick(View v) {
						bollette_pagamenti_navigation_bar.setBackgroundResource(R.drawable.bollette_footer_rate);
						bolletteRateizzabili(primoConto);
					}
				});

			}
			else {
				bollette_pagamenti_navigation_bar.setBackgroundResource(R.drawable.bollette_footer_prox_disabled);
				TextView stato_estrato_conto = (TextView) bollette_pagamenti_entry.findViewById(R.id.bollette_pagamenti_situazione_estratto_stato);
				stato_estrato_conto.setText("Cessato");
				stato_estrato_conto.setTextColor(getResources().getColor(R.color.red1));

				bollette_pagamenti_ricerca_bolletta_btn.setClickable(true);
				bollette_pagamenti_prossima_bolletta_btn.setClickable(false);
				bollette_pagamenti_bollette_rateizzabili_btn.setClickable(false);

				bollette_pagamenti_ricerca_bolletta_btn.setOnClickListener(new View.OnClickListener() {
					public void onClick(View v) {
						bollette_pagamenti_navigation_bar.setBackgroundResource(R.drawable.bollette_footer_prox_disabled_elenco);
						elencoBollette();
					}
				});

			}

			pager.addView(bollette_pagamenti_entry);
		}
		catch (NumberFormatException e) {
			e.printStackTrace();
		}
		catch (JSONException e) {
			e.printStackTrace();
		}

	}

	private void elencoBollette() {

		animation = Utilities.animation(this);
		imageAnim = (ImageView) findViewById(R.id.waiting_anim);
		imageAnim.setVisibility(View.VISIBLE);
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
			imageAnim.setBackgroundDrawable(animation);
		}
		else {
			imageAnim.setBackground(animation);
		}
		animation.start();

		Intent intentBollettePagamentiRicerca = new Intent(getApplicationContext(), BollettePagamentiRicerca.class);
		intentBollettePagamentiRicerca.putExtra("PRIMOCONTO", array_conti_spinner[positionSpinnerCurrentSelected].trim());
		intentBollettePagamentiRicerca.putExtra("INDICE_SPINNER", positionSpinnerCurrentSelected);
		BollettePagamenti.this.finish();
		startActivity(intentBollettePagamentiRicerca);
		overridePendingTransition(R.anim.fade, R.anim.hold);
	}

	private void bolletteRateizzabili(String codiceConto) {

		animation = Utilities.animation(this);
		imageAnim = (ImageView) findViewById(R.id.waiting_anim);
		imageAnim.setVisibility(View.VISIBLE);
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
			imageAnim.setBackgroundDrawable(animation);
		}
		else {
			imageAnim.setBackground(animation);
		}
		animation.start();

		String codiceCliente = getCodiceCliente(rispostaLogin);
		if (!isNetworkAvailable(context)) {
			Intent intentOops = new Intent(getApplicationContext(), OopsPageBollettePagamentiBolletteRateizzabili.class);
			intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.connessione_assente));
			intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
			intentOops.putExtra("CODICE_CONTO_CLIENTE", array_conti_spinner[positionSpinnerCurrentSelected].trim());
			intentOops.putExtra("TIPOLOGIA", tipologia);
			// StatoAvanzamentoFornitura.this.finish();
			startActivity(intentOops);
			if (animation != null) {
				animation.stop();
				imageAnim.setVisibility(View.INVISIBLE);
			}
			overridePendingTransition(R.anim.fade, R.anim.hold);
		}
		else {
			/********************************** chiamata info fatture rateizzabili ***********************************/
			ServiceCall stAvFornServiceCall = ServiceCallFactory.createServiceCall(context);
			// stAvFornServiceCall.myCookieStore = new
			// PersistentCookieStore(context);

			HashMap<String, String> parametri = new HashMap<String, String>();
			// parametri.put("codiceCliente", codiceCliente);
			parametri.put("codiceContoCliente", array_conti_spinner[positionSpinnerCurrentSelected].trim());

			try {
				stAvFornServiceCall.executeHttpsGetBollettePagamenti(Constants.URL_INFO_FATTURE_RATEIZZABILI, parametri, BollettePagamenti.this, null, false, true);
			}
			catch (Exception e) {
				Intent intentOops = new Intent(getApplicationContext(), OopsPageBollettePagamentiBolletteRateizzabili.class);
				intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
				intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
				intentOops.putExtra("CODICE_CONTO_CLIENTE", array_conti_spinner[positionSpinnerCurrentSelected].trim());
				intentOops.putExtra("TIPOLOGIA", tipologia);
				startActivity(intentOops);
				overridePendingTransition(R.anim.fade, R.anim.hold);
			}
		}
		/***************************************************************************************************/

	}

	public void postChiamata(String rispostaChiamata, List<Cookie> lista, HashMap<String, String> parametri, boolean prossimaBolletta, boolean bolleteRateizzabili) {
		if (animation != null) {
			animation.stop();
			imageAnim.setVisibility(View.INVISIBLE);
		}

		final ImageView indietro = (ImageView) findViewById(R.id.bollette_pagamenti_indietro);
		indietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
		indietro.getBackground().invalidateSelf();

		JSONArray rispostaJSON = new JSONArray();
		String esito = "";
		String descEsito = "";
		try {
			rispostaJSON = new JSONArray(rispostaChiamata);
			JSONObject oggettoRisposta = (JSONObject) rispostaJSON.get(0);
			esito = oggettoRisposta.getString("esito");
			descEsito = oggettoRisposta.getString("descEsito");
		}
		catch (JSONException e) {
			e.printStackTrace();
		}
		if (esito.equals("200")) {
			if (!prossimaBolletta) {
				if (bolleteRateizzabili) {
					// bollette rateizzabili
					Constants.SERVIZIO_INFO_FATTURE_RATEIZZABILI = rispostaChiamata;
					Intent intentBollettePagamentiRate = new Intent(getApplicationContext(), BollettePagamentiRateizzate.class);
					intentBollettePagamentiRate.putExtra("PRIMOCONTO", parametri.get("codiceContoCliente"));
					intentBollettePagamentiRate.putExtra("INDICE_SPINNER", positionSpinnerCurrentSelected);
					intentBollettePagamentiRate.putExtra("tipoFornitura", rateGasOLuce);
					BollettePagamenti.this.finish();
					startActivity(intentBollettePagamentiRate);
					overridePendingTransition(R.anim.fade, R.anim.hold);
				} else {
					Constants.SERVIZIO_SHOW_LISTA_FATTURE = rispostaChiamata;
					try {
						popolaPaginaContenuto(new JSONArray(rispostaLogin), new JSONArray(Constants.SERVIZIO_SHOW_LISTA_FATTURE),pager, parametri.get("codiceContoCliente"));

					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
			}
			else {
				Constants.SERVIZIO_INFO_PROSSIMA_FATTURA = rispostaChiamata;
				Intent intentBollettePagamentiProxBol = new Intent(getApplicationContext(), BollettePagamentiProssimaBolletta.class);
				intentBollettePagamentiProxBol.putExtra("PRIMOCONTO", parametri.get("codiceContoCliente"));
				intentBollettePagamentiProxBol.putExtra("TIPOLOGIA", tipologia);
				intentBollettePagamentiProxBol.putExtra("INDICE_SPINNER", positionSpinnerCurrentSelected);
				intentBollettePagamentiProxBol.putExtra("SOURCE_ACTIVITY", BollettePagamenti.class.getName());
				BollettePagamenti.this.finish();
				startActivity(intentBollettePagamentiProxBol);
				overridePendingTransition(R.anim.fade, R.anim.hold);
			}

		}
		else if (esito.equals("461") && bolleteRateizzabili) {

			Constants.SERVIZIO_INFO_FATTURE_RATEIZZABILI = rispostaChiamata;
			
			try {

				ServiceCall visFornDomicilServiceCall = ServiceCallFactory.createServiceCall(context);
				String params = "&codiciContoCliente=" + ((Spinner) findViewById(R.id.numero_cliente)).getSelectedItem().toString().trim();
				visFornDomicilServiceCall.executeHttpsGet(Constants.URL_VISUALIZZA_FORNITURE_DOMICILIAZIONE, params, BollettePagamenti.this,Constants.FUNZIONALITA_DOMICILIAZIONE, null);

			} catch (Exception e) {
				e.printStackTrace();
			}

		}
		else {
			Intent intentOops = new Intent();
			if (!prossimaBolletta) {
				if (bolleteRateizzabili) {
					// bollette rateizzabili
					intentOops = new Intent(getApplicationContext(), OopsPageBollettePagamentiBolletteRateizzabili.class);
					intentOops.putExtra("TIPOLOGIA", tipologia);
				}
				else {
					intentOops = new Intent(getApplicationContext(), OopsPageBollettePagamentiFromBp.class);
					positionSpinnerCurrentSelected = positionSpinnerPrecedentSelected;
				}
			}
			else {
				intentOops = new Intent(getApplicationContext(), OopsPageBollettePagamentiProssimaBolletta.class);
				intentOops.putExtra("TIPOLOGIA", tipologia);
			}

			intentOops.putExtra("CODICE_CLIENTE", parametri.get("codiceCliente"));
			intentOops.putExtra("CODICE_CONTO_CLIENTE", parametri.get("codiceContoCliente"));

			// errori popup
			if (esito.equals("307")) {
				Utils.popup(BollettePagamenti.this, getResources().getString(R.string.errore_popup_307), "Riprova");

			}
			else if (esito.equals("435")) {
				Utils.popup(BollettePagamenti.this, getResources().getString(R.string.errore_popup_435), "Riprova");

			}
			else if (esito.equals("437")) {
				Utils.popup(BollettePagamenti.this, getResources().getString(R.string.errore_popup_437), "Riprova");

			}
			else if (esito.equals("438")) {
				Utils.popup(BollettePagamenti.this, getResources().getString(R.string.errore_popup_438), "Riprova");

			}
			else if (esito.equals("443")) {
				Utils.popup(BollettePagamenti.this, getResources().getString(R.string.errore_popup_443), "Riprova");

			}
			else if (esito.equals("444")) {
				Utils.popup(BollettePagamenti.this, getResources().getString(R.string.errore_popup_444), "Riprova");

			}
			else if (esito.equals("445")) {
				Utils.popup(BollettePagamenti.this, getResources().getString(R.string.errore_popup_445), "Riprova");

			}
			else if (esito.equals("446")) {
				Utils.popup(BollettePagamenti.this, getResources().getString(R.string.errore_popup_446), "Riprova");

			}
			else if (esito.equals("447")) {
				Utils.popup(BollettePagamenti.this, getResources().getString(R.string.errore_popup_447), "Riprova");

			}
			if (esito.equals("451")) {
				intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_popup_451));
				startActivity(intentOops);
				overridePendingTransition(R.anim.fade, R.anim.hold);

			}
			else {
				if (descEsito.equals("")) {
					intentOops = new Intent(getApplicationContext(), OopsPageSessioneScaduta.class);
					intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.sessione_scaduta));
				} else{
					intentOops.putExtra("MESSAGGIO", descEsito);
				}

				startActivity(intentOops);
				overridePendingTransition(R.anim.fade, R.anim.hold);
			}

		}

	}

	public void postChiamataException(boolean exception, HashMap<String, String> parameterMap, boolean prossimaFattura, boolean bolleteRateizzabili) {
		if (exception) {
			if (animation != null) {
				animation.stop();
				imageAnim.setVisibility(View.INVISIBLE);
			}

			final ImageView indietro = (ImageView) findViewById(R.id.bollette_pagamenti_indietro);
			indietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
			indietro.getBackground().invalidateSelf();

			Intent intentOops = new Intent();
			if (!prossimaFattura) {
				if (bolleteRateizzabili) {
					// bollette rateizzabili
					intentOops = new Intent(getApplicationContext(), OopsPageBollettePagamentiBolletteRateizzabili.class);
					intentOops.putExtra("TIPOLOGIA", tipologia);
				}
				else {
					intentOops = new Intent(getApplicationContext(), OopsPageBollettePagamentiFromBp.class);
					positionSpinnerCurrentSelected = positionSpinnerPrecedentSelected;
				}
			}
			else {
				intentOops = new Intent(getApplicationContext(), OopsPageBollettePagamentiProssimaBolletta.class);
				intentOops.putExtra("TIPOLOGIA", tipologia);
			}
			intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
			intentOops.putExtra("CODICE_CLIENTE", parameterMap.get("codiceCliente"));
			intentOops.putExtra("CODICE_CONTO_CLIENTE", parameterMap.get("codiceContoCliente"));

			// BollettePagamenti.this.finish();
			startActivity(intentOops);
			overridePendingTransition(R.anim.fade, R.anim.hold);
		}
	}

	public String[] elencoCodiciContiAttiviCompleti(JSONArray array) {
		int lunghezza = numeroCodiciContoDiversiAttiviCompletieNonAttivi(array);
		String result[] = new String[lunghezza];
		String codiceContoConfrontato = "";
		String codiceContoIesimo = "";
		String codiciConcatenati = "";
		int count = 0;
		for (int i = 2; i < array.length(); i++) {
			JSONArray contoIesimo;
			try {
				contoIesimo = array.getJSONArray(i);
				// codice conto
				JSONObject anagrContoIesimo = contoIesimo.getJSONObject(0);
				codiceContoIesimo = anagrContoIesimo.getString("CodiceConto");

				if (!codiceContoIesimo.equals(codiceContoConfrontato)) {
					String stato = anagrContoIesimo.getString("StatoConto");
					if (!stato.equals("UTILIZZABILE")) {
						continue;
					}
					else {
						// codice conto utilizzabile diverso, controllo non sia
						// vuoto
						if (contoIesimo.length() > 1) {
							// che non sia gia' presente
							boolean isPresente = codiciConcatenati.contains("-" + codiceContoIesimo + "-");
							if (isPresente) {
								continue;
							}
							else {
								codiceContoConfrontato = codiceContoIesimo;
								result[count] = codiceContoConfrontato;
								codiciConcatenati = codiciConcatenati + "-" + codiceContoIesimo + "-";
								count++;
							}
						}
						else {
							continue;
						}
					}
				}
			}
			catch (JSONException e) {
				e.printStackTrace();
			}
		}
		return result;
	}

	public int numeroCodiciContoDiversiAttiviCompletieNonAttivi(JSONArray array) {
		int result = 0;
		String codiceContoConfrontato = "";
		String codiceContoIesimo = "";
		String codiciConcatenati = "";
		for (int i = 2; i < array.length(); i++) {
			JSONArray contoIesimo;
			try {
				contoIesimo = array.getJSONArray(i);
				// codice conto
				JSONObject anagrContoIesimo = contoIesimo.getJSONObject(0);
				codiceContoIesimo = anagrContoIesimo.getString("CodiceConto");

				if (!codiceContoIesimo.equals(codiceContoConfrontato)) {
					String stato = anagrContoIesimo.getString("StatoConto");
					if (!stato.equals("UTILIZZABILE")) {
						// prendo anche i conti non utilizzabili
						if (!codiciConcatenati.contains("-" + codiceContoIesimo)) {
							// codice conto diverso
							codiceContoConfrontato = codiceContoIesimo;
							codiciConcatenati = codiciConcatenati + "-" + codiceContoIesimo;
							result++;
						}
					}
					else {
						// codice conto utilizzabile diverso, controllo non sia
						// vuoto
						if (contoIesimo.length() > 1) {
							if (!codiciConcatenati.contains("-" + codiceContoIesimo)) {
								// codice conto diverso
								codiceContoConfrontato = codiceContoIesimo;
								codiciConcatenati = codiciConcatenati + "-" + codiceContoIesimo;
								result++;
							}
						}
						else {
							continue;
						}
					}
				}
			}
			catch (JSONException e) {
				e.printStackTrace();
			}
		}
		return result;
	}

	public String[] arrayCodiciContoDiversiUtilizabiliInteriENonUtilizzabili(JSONArray array, int lunghezza) {
		String result[] = new String[lunghezza];
		String codiceContoConfrontato = "";
		String codiceContoIesimo = "";
		String codiciConcatenati = "";
		int count = 0;
		for (int i = 2; i < array.length(); i++) {
			JSONArray contoIesimo;
			try {
				contoIesimo = array.getJSONArray(i);
				// codice conto
				JSONObject anagrContoIesimo = contoIesimo.getJSONObject(0);
				codiceContoIesimo = anagrContoIesimo.getString("CodiceConto");

				if (!codiceContoIesimo.equals(codiceContoConfrontato)) {
					String stato = anagrContoIesimo.getString("StatoConto");
					if (!stato.equals("UTILIZZABILE")) {
						// il conto è non utilizzabile, prendo anche questi
						// che non sia gia' presente
						boolean isPresente = codiciConcatenati.contains("-" + codiceContoIesimo + "-");
						if (isPresente) {
							continue;
						}
						else {
							codiceContoConfrontato = codiceContoIesimo;
							result[count] = "  " + codiceContoConfrontato;
							codiciConcatenati = codiciConcatenati + "-" + codiceContoIesimo + "-";
							count++;
						}
					}
					else {
						// codice conto utilizzabile diverso, controllo non sia
						// vuoto
						if (contoIesimo.length() > 1) {
							// che non sia gia' presente
							boolean isPresente = codiciConcatenati.contains("-" + codiceContoIesimo + "-");
							if (isPresente) {
								continue;
							}
							else {
								codiceContoConfrontato = codiceContoIesimo;
								result[count] = "  " + codiceContoConfrontato;
								codiciConcatenati = codiciConcatenati + "-" + codiceContoIesimo + "-";
								count++;
							}
						}
						else {
							continue;
						}
					}
				}
			}
			catch (JSONException e) {
				e.printStackTrace();
			}
		}
		return result;
	}

	public String getCodiceCliente(String rispostaLogin) {
		String result = "";
		try {
			JSONArray loginJson = new JSONArray(rispostaLogin);
			JSONObject datiCliente = (JSONObject) loginJson.get(1);
			result = datiCliente.getString("CodiceCliente");
		}
		catch (JSONException e) {
			e.printStackTrace();
		}
		return result;
	}

	private boolean isDual(JSONArray arrayFornitureLogin, String codiceConto) {
		boolean result = false;
		int count = 0;
		JSONObject anagrContoIesimo = new JSONObject();
		try {
			for (int i = 2; i < arrayFornitureLogin.length(); i++) {
				JSONArray contoIesimo;
				contoIesimo = arrayFornitureLogin.getJSONArray(i);
				anagrContoIesimo = contoIesimo.getJSONObject(0);

				String codiceContoIesimo = anagrContoIesimo.getString("CodiceConto");
				if (!codiceContoIesimo.equals(codiceConto)) {
					continue;
				}
				else {
					// ho trovato il conto codiceConto
					count++;
				}
			}
			if (count == 2) {
				result = true;
			}
			else {
				result = false;
			}

		}
		catch (JSONException e) {
			e.printStackTrace();
		}

		Log.d("BOLLETTE_PAGAMENTI",this.getClass().getName() + " - isDual: " + result);

		return result;
	}

	private String cercaTipologiaFornitura(JSONArray arrayLogin, String codiceConto) {
		String result = "";
		JSONObject anagrContoIesimo = new JSONObject();
		JSONObject anagrExtraContoIesimo = new JSONObject();
		try {
			for (int i = 2; i < arrayLogin.length(); i++) {
				JSONArray contoIesimo;
				contoIesimo = arrayLogin.getJSONArray(i);
				anagrContoIesimo = contoIesimo.getJSONObject(0);

				String codiceContoIesimo = anagrContoIesimo.getString("CodiceConto");
				if (!codiceContoIesimo.equals(codiceConto)) {
					continue;
				}
				else {
					// ho trovato il conto codiceConto
					if (contoIesimo.length() > 1) {
						anagrExtraContoIesimo = contoIesimo.getJSONObject(1);
						result = anagrExtraContoIesimo.getString("Tipologia");
					}
				}
			}
		}
		catch (JSONException e) {
			e.printStackTrace();
		}
		return result;

	}

	public String trovaTipoProdottoFornitura(JSONArray array, String codiceConto) {
		String result = "";
		String codiceContoIesimo = "";
		JSONObject anagrContoIesimo = new JSONObject();
		try {
			for (int i = 2; i < array.length(); i++) {
				JSONArray contoIesimo;
				contoIesimo = array.getJSONArray(i);
				// codice conto
				anagrContoIesimo = contoIesimo.getJSONObject(0);
				codiceContoIesimo = anagrContoIesimo.getString("CodiceConto");
				if (!codiceContoIesimo.equals(codiceConto)) {
					// codice conto diverso
					continue;
				}
				else {
					if (contoIesimo.length() > 1) {
						JSONObject extraContoIesimo = contoIesimo.getJSONObject(1);
						result = extraContoIesimo.getString("TipoProdotto");
					}
				}

			}

		}
		catch (JSONException e) {
			e.printStackTrace();
		}
		return result;
	}

	public String trovaTipoProdottoFornituraTipologia(JSONArray array, String codiceConto, String tipologia) {
		String result = "";
		String codiceContoIesimo = "";
		JSONObject anagrContoIesimo = new JSONObject();
		try {
			for (int i = 2; i < array.length(); i++) {
				JSONArray contoIesimo;
				contoIesimo = array.getJSONArray(i);
				// codice conto
				anagrContoIesimo = contoIesimo.getJSONObject(0);
				codiceContoIesimo = anagrContoIesimo.getString("CodiceConto");
				if (!codiceContoIesimo.equals(codiceConto)) {
					// codice conto diverso
					continue;
				}
				else {
					if (contoIesimo.length() > 1) {
						JSONObject extraContoIesimo = contoIesimo.getJSONObject(1);
						String tipologiaConto = extraContoIesimo.getString("Tipologia");
						if (!tipologiaConto.equals(tipologia)) {
							continue;
						}
						else {
							result = extraContoIesimo.getString("TipoProdotto");
						}
					}
				}
			}
		}
		catch (JSONException e) {
			e.printStackTrace();
		}
		return result;
	}

	private String cercaIndirizzoFornitura(JSONArray array, String codiceConto) {
		String result = "";
		String codiceContoIesimo = "";
		JSONObject anagrContoIesimo = new JSONObject();

		try {
			for (int i = 2; i < array.length(); i++) {
				JSONArray contoIesimo;

				contoIesimo = array.getJSONArray(i);
				// codice conto
				anagrContoIesimo = contoIesimo.getJSONObject(0);
				codiceContoIesimo = anagrContoIesimo.getString("CodiceConto");

				if (!codiceContoIesimo.equals(codiceConto)) {
					// codice conto diverso
					continue;
				}
				else {
					if (contoIesimo.length() > 1) {
						JSONObject anagrExtraContoIesimo = contoIesimo.getJSONObject(1);
						result = anagrExtraContoIesimo.getString("IndirizzoCompleto");
					}
				}
			}
		}
		catch (JSONException e) {
			e.printStackTrace();
		}
		return result;
	}

	private String cercaIndirizzoFornituraTipologia(JSONArray array, String codiceConto, String tipologia) {
		String result = "";
		String codiceContoIesimo = "";
		JSONObject anagrContoIesimo = new JSONObject();
		try {
			for (int i = 2; i < array.length(); i++) {
				JSONArray contoIesimo;

				contoIesimo = array.getJSONArray(i);
				// codice conto
				anagrContoIesimo = contoIesimo.getJSONObject(0);
				codiceContoIesimo = anagrContoIesimo.getString("CodiceConto");

				if (!codiceContoIesimo.equals(codiceConto)) {
					// codice conto diverso
					continue;
				}
				else {
					// codice conto utilizzabile diverso, controllo non sia
					// vuoto
					if (contoIesimo.length() > 1) {
						JSONObject anagrExtraContoIesimo = contoIesimo.getJSONObject(1);
						String tipologiaConto = anagrExtraContoIesimo.getString("Tipologia");
						if (!tipologiaConto.equals(tipologia)) {
							// codice conto diverso
							continue;
						}
						else {
							result = anagrExtraContoIesimo.getString("IndirizzoCompleto");
						}
					}
				}
			}
		}
		catch (JSONException e) {
			e.printStackTrace();
		}
		return result;
	}

	/*
	 * private boolean tuttePagate(JSONArray array){ boolean result = true; try
	 * { if(array.length() > 2){ JSONArray listaBollette =
	 * array.getJSONArray(2); for(int i = 0; i < listaBollette.length(); i++){
	 * JSONObject bollettaIesima = listaBollette.getJSONObject(i); String
	 * statoBolletta = bollettaIesima.getString("StatoPagamento");
	 * if(!statoBolletta.equals("PAGATA")){ return false; } } } } catch
	 * (JSONException e) { e.printStackTrace(); } return result; }
	 * 
	 * private int countBolletteDaPagare(JSONArray array){ int result = 0; try {
	 * JSONArray listaBollette = array.getJSONArray(2); for(int i = 0; i <
	 * listaBollette.length(); i++){ JSONObject bollettaIesima =
	 * listaBollette.getJSONObject(i); String statoBolletta =
	 * bollettaIesima.getString("StatoPagamento");
	 * if(!statoBolletta.equals("PAGATA")){ result++; } } } catch (JSONException
	 * e) { e.printStackTrace(); } return result; }
	 */

	private boolean isContoAttivo(JSONArray login, String codiceConto) {
		boolean result = false;
		JSONArray conto;
		JSONObject anagrConto;
		try {
			for (int i = 2; i < login.length(); i++) {
				conto = login.getJSONArray(i);
				anagrConto = conto.getJSONObject(0);
				String codice = anagrConto.getString("CodiceConto");
				if (codice.equals(codiceConto)) {
					String statoConto = anagrConto.getString("StatoConto");
					if (statoConto.equals("UTILIZZABILE")) {
						result = true;
					}
					else if (statoConto.equals("NON UTILIZZABILE")) {
						result = false;
					}
				}
				else {
					continue;
				}
			}
		}
		catch (JSONException e) {
			e.printStackTrace();
		}
		return result;

	}

	public void postChiamataDomiciliazione(String rispostaChiamata) {
		
		if (animation != null) {
			animation.stop();
			imageAnim.setVisibility(View.INVISIBLE);
		}

		String statoDomiciliazione = "";

		try {

			JSONArray rispostaJSON = new JSONArray(rispostaChiamata);

			statoDomiciliazione = ((JSONObject) rispostaJSON.get(1)).getString("StatoDomiciliazione");

		} catch (JSONException e) {
			e.printStackTrace();
		}

		Intent intentBollettePagamentiRate = new Intent(getApplicationContext(), BollettePagamentiRateizzate.class);
		intentBollettePagamentiRate.putExtra("PRIMOCONTO", ((Spinner) findViewById(R.id.numero_cliente)).getSelectedItem().toString().trim());
		intentBollettePagamentiRate.putExtra("INDICE_SPINNER", positionSpinnerCurrentSelected);
		intentBollettePagamentiRate.putExtra("tipoFornitura", rateGasOLuce);
		intentBollettePagamentiRate.putExtra("STATO_DOMICILIAZIONE", statoDomiciliazione);

		BollettePagamenti.this.finish();
		startActivity(intentBollettePagamentiRate);
		overridePendingTransition(R.anim.fade, R.anim.hold);

	}

	public void postChiamataDomiciliazioneException() {

		if (animation != null) {
			animation.stop();
			imageAnim.setVisibility(View.INVISIBLE);
		}

		RelativeLayout titolo_lista_bollette = (RelativeLayout) findViewById(R.id.titolo_lista_bollette);
		RelativeLayout stato_domiciliazione_approvata = (RelativeLayout) findViewById(R.id.stato_domiciliazione_approvata);
		RelativeLayout stato_domiciliazione_non_approvata_lista_vuota = (RelativeLayout) findViewById(R.id.stato_domiciliazione_non_approvata_lista_vuota);
		titolo_lista_bollette.setVisibility(View.GONE);
		stato_domiciliazione_approvata.setVisibility(View.GONE);
		stato_domiciliazione_non_approvata_lista_vuota.setVisibility(View.VISIBLE);

	}

	private boolean isNetworkAvailable(Context context) {
		ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivityManager == null) return false;
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		return activeNetworkInfo != null;
	}

	public void onBackPressed() {

		animation = Utilities.animation(this);
		imageAnim = (ImageView) findViewById(R.id.waiting_anim);
		imageAnim.setVisibility(View.VISIBLE);
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
			imageAnim.setBackgroundDrawable(animation);
		}
		else {
			imageAnim.setBackground(animation);
		}
		animation.start();

		Intent intentHome = new Intent(getApplicationContext(), Home.class);
		BollettePagamenti.this.finish();
		startActivity(intentHome);
		overridePendingTransition(R.anim.fade, R.anim.hold);
	}
}
