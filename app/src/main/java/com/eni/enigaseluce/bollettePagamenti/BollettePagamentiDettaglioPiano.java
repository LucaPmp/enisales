package com.eni.enigaseluce.bollettePagamenti;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import org.apache.http.cookie.Cookie;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.AnimationDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.eni.enigaseluce.R;
import com.eni.enigaseluce.error.OopsPageBollettePagamentiDettaglioPiano;
import com.eni.enigaseluce.error.OopsPageBollettePagamentiRateModifica;
import com.eni.enigaseluce.error.OopsPageSessioneScaduta;
import com.eni.enigaseluce.error.Utils;
import com.eni.enigaseluce.httpcall.Constants;
import com.eni.enigaseluce.httpcall.ServiceCall;
import com.eni.enigaseluce.httpcall.ServiceCallFactory;
import com.eni.enigaseluce.utils.Utilities;

@SuppressLint("NewApi")
public class BollettePagamentiDettaglioPiano extends Activity {
    private Context context;
    private static Context staticContext;
    AnimationDrawable animation;
    ImageView imageAnim;

    static LinearLayout pager;

    static LayoutInflater inflater;
    static LayoutInflater inflaterDialog;

    public String rispostaLogin = Constants.SERVIZIO_LOGIN;
    public String rispostaCalcolaProposta = Constants.SERVIZIO_CALCOLA_PROPOSTA_RATE;

    public static String nomeInteroCliente = "";
    String codiceEsito;
    String tipologia;

    String totaleRate;
    String numeroRate;
    String periodocita;
    String causale;
    String tasso;

    String[] arrayPeriodicita;
    String[] rate;

    String rataCorrentePicker;
    String periodicitaCorrentePicker;
    String dataAttivabilita;

    int maxRata = -1;
    boolean richiestaAccettata = false;

    boolean info = true;

    public static Context getContext() {
	return BollettePagamentiDettaglioPiano.staticContext;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.bollette_pagamenti_dettaglio_piano);

	context = this;
	BollettePagamentiDettaglioPiano.staticContext = this;

    }

    @Override
    protected void onStart() {
	super.onStart();

	final ImageView indietro = (ImageView) findViewById(R.id.bollette_pagamenti_indietro);
	indietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
	indietro.getBackground().invalidateSelf();
	indietro.setOnClickListener(new View.OnClickListener() {
	    public void onClick(View v) {
		JSONArray proposte = new JSONArray();
		try {
		    JSONArray calcolaPropostaJSON = new JSONArray(Constants.SERVIZIO_CALCOLA_PROPOSTA_RATE);
		    proposte = calcolaPropostaJSON.getJSONObject(1).getJSONArray("propostaRateizzazione");

		}
		catch (JSONException e) {
		    e.printStackTrace();
		}

		indietro.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
		indietro.getBackground().invalidateSelf();

		animation = Utilities.animation(BollettePagamentiDettaglioPiano.this);
		imageAnim = (ImageView) findViewById(R.id.waiting_anim);
		imageAnim.setVisibility(View.VISIBLE);
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
		    imageAnim.setBackgroundDrawable(animation);
		}
		else {
		    imageAnim.setBackground(animation);
		}
		animation.start();

		if (richiestaAccettata) {
		    Intent intentBollettePagamenti = new Intent(getApplicationContext(), BollettePagamenti.class);
		    intentBollettePagamenti.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
		    intentBollettePagamenti.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
		    BollettePagamentiDettaglioPiano.this.finish();
		    startActivity(intentBollettePagamenti);
		    overridePendingTransition(R.anim.fade, R.anim.hold);
		}
		else {
		    if ((getIntent().getStringExtra("PAGINA_PROVENIENZA") != null) && (getIntent().getStringExtra("PAGINA_PROVENIENZA").equals(BollettePagamentiSceltaPiano.class.getCanonicalName()))) {
			if (proposte.length() == 2) {
			    Intent intentSceltaPiano = new Intent(getApplicationContext(), BollettePagamentiSceltaPiano.class);
			    intentSceltaPiano.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
			    intentSceltaPiano.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
			    intentSceltaPiano.putExtra("tipoFornitura", getIntent().getStringExtra("tipoFornitura"));
			    intentSceltaPiano.putExtra("identificativoFattura", getIntent().getStringExtra("identificativoFattura"));
			    intentSceltaPiano.putExtra("codiceCausaleRateizzabilitaAlternativa", getIntent().getStringExtra("codiceCausaleRateizzabilitaAlternativa"));

			    intentSceltaPiano.putExtra("codiceFattura", getIntent().getStringExtra("codiceFattura"));
			    intentSceltaPiano.putExtra("dataEmissione", getIntent().getStringExtra("dataEmissione"));
			    intentSceltaPiano.putExtra("dataScadenza", getIntent().getStringExtra("dataScadenza"));
			    intentSceltaPiano.putExtra("importoRateizzabile", getIntent().getStringExtra("importoRateizzabile"));
			    intentSceltaPiano.putExtra("codiceCausaleRateizzabilita", getIntent().getStringExtra("codiceCausaleRateizzabilita"));
			    
			    intentSceltaPiano.putExtra("domiciliazioneApprovata", getIntent().getBooleanExtra("domiciliazioneApprovata", false));
			    intentSceltaPiano.putExtra("infoDomiciliazioneApprovata",  getIntent().getBooleanExtra("infoDomiciliazioneApprovata", false));
			    
			    // intentSceltaPiano.putExtra("chiamata", chiamata);
			    BollettePagamentiDettaglioPiano.this.finish();
			    startActivity(intentSceltaPiano);
			    overridePendingTransition(R.anim.fade, R.anim.hold);
			}
			else if (proposte.length() == 1) {
			    chiamataCalcolaProposta(Constants.PIANO_RATEIZZAZIONE_DOPPIO, null, null);
			}
		    }
		    else {
			Intent intentHome = new Intent(getApplicationContext(), BollettePagamentiRateizzate.class);
			intentHome.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
			intentHome.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
			intentHome.putExtra("domiciliazioneApprovata", getIntent().getBooleanExtra("domiciliazioneApprovata", false));
		    intentHome.putExtra("infoDomiciliazioneApprovata",  getIntent().getBooleanExtra("infoDomiciliazioneApprovata", false));
		    
			BollettePagamentiDettaglioPiano.this.finish();
			startActivity(intentHome);
			overridePendingTransition(R.anim.fade, R.anim.hold);
		    }
		}
	    }
	});

	/*
	 * final ImageView help =
	 * (ImageView)findViewById(R.id.bollette_pagamenti_help);
	 * help.getBackground().setColorFilter(0xFFFFFFFF,
	 * PorterDuff.Mode.MULTIPLY); help.getBackground().invalidateSelf();
	 * help.setOnClickListener(new View.OnClickListener() { public void
	 * onClick(View v) { help.getBackground().setColorFilter(0xFF999999,
	 * PorterDuff.Mode.MULTIPLY); help.getBackground().invalidateSelf();
	 * animation = new AnimationDrawable();
	 * animation.addFrame(getResources().getDrawable(R.drawable.anim_load2),
	 * 300);
	 * animation.addFrame(getResources().getDrawable(R.drawable.anim_load3),
	 * 300);
	 * animation.addFrame(getResources().getDrawable(R.drawable.anim_load4),
	 * 300); animation.setOneShot(false); imageAnim = (ImageView)
	 * findViewById(R.id.waiting_bollette_pagamenti);
	 * imageAnim.setVisibility(View.VISIBLE); if(Build.VERSION.SDK_INT <
	 * 16){ imageAnim.setBackgroundDrawable(animation); }else{
	 * imageAnim.setBackground(animation); } // start the animation!
	 * animation.start();
	 * 
	 * Intent intentHelp = new Intent(getApplicationContext(),
	 * BollettePagamentiHelp.class); intentHelp.putExtra("ORIGINE",
	 * Constants.ORIGINE_BOLLETTE_RATEIZZABILI_DETTAGLIO_PIANO);
	 * intentHelp.putExtra("PRIMOCONTO",
	 * getIntent().getStringExtra("PRIMOCONTO"));
	 * intentHelp.putExtra("INDICE_SPINNER",
	 * getIntent().getIntExtra("INDICE_SPINNER", 0));
	 * intentHelp.putExtra("tipoFornitura",
	 * getIntent().getStringExtra("tipoFornitura"));
	 * intentHelp.putExtra("identificativoFattura",
	 * getIntent().getStringExtra("identificativoFattura"));
	 * if(getIntent().getStringExtra("pianoSelezionato") != null){
	 * intentHelp.putExtra("pianoSelezionato",
	 * getIntent().getStringExtra("pianoSelezionato")); }
	 * intentHelp.putExtra("codiceFattura",
	 * getIntent().getStringExtra("codiceFattura"));
	 * intentHelp.putExtra("dataEmissione",
	 * getIntent().getStringExtra("dataEmissione"));
	 * intentHelp.putExtra("dataScadenza",
	 * getIntent().getStringExtra("dataScadenza"));
	 * intentHelp.putExtra("importoRateizzabile",
	 * getIntent().getStringExtra("importoRateizzabile"));
	 * 
	 * BollettePagamentiDettaglioPiano.this.finish();
	 * startActivity(intentHelp); overridePendingTransition(R.anim.fade,
	 * R.anim.hold); } });
	 */
	try {
	    rispostaLogin = Constants.SERVIZIO_LOGIN;
	    final JSONArray bollettePagamentiLogin = new JSONArray(rispostaLogin);

	    JSONObject datiAnagr = bollettePagamentiLogin.getJSONObject(1);
	    // ultimo accesso
	    String ultimoaccesso = datiAnagr.getString("UltimoAccesso");
	    TextView ultimoAccesso = (TextView) findViewById(R.id.ultimo_accesso);
	    // ultimoAccesso.setTextSize(11);
	    ultimoAccesso.setText(ultimoaccesso);
	    // nome cliente
	    String nomeClienteString = datiAnagr.getString("NomeCliente");
	    String cognomeClienteString = datiAnagr.getString("CognomeCliente");
	    TextView nomeCliente = (TextView) findViewById(R.id.nome_cliente);
	    nomeInteroCliente = nomeClienteString + " " + cognomeClienteString;
	    nomeCliente.setText(nomeClienteString + " " + cognomeClienteString);

	    pager = (LinearLayout) findViewById(R.id.bollette_pagamenti_pager);
	    pager.removeAllViews();
	    inflater = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);
	    inflaterDialog = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);
	    if (!rispostaCalcolaProposta.equals("")) {
		popolaPaginaContenuto(new JSONArray(rispostaCalcolaProposta), pager, 1);
	    }

	}
	catch (JSONException e) {
	    e.printStackTrace();
	}
    }

    private void chiamataCalcolaProposta(int tipoPiano, String rataSelezionata, String periodicitaSelezionata) {

	animation = Utilities.animation(this);
	imageAnim = (ImageView) findViewById(R.id.waiting_anim);
	imageAnim.setVisibility(View.VISIBLE);
	if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
	    imageAnim.setBackgroundDrawable(animation);
	}
	else {
	    imageAnim.setBackground(animation);
	}
	animation.start();

	boolean reset = false;
	try {
	    String codiceFattura = getIntent().getStringExtra("codiceFattura");

	    String codiceCliente = getCodiceCliente(rispostaLogin);
	    String identificativoFattura = getIntent().getStringExtra("identificativoFattura");
	    String codiceCausaleRateizzabilita = getIntent().getStringExtra("codiceCausaleRateizzabilita");

	    HashMap<String, String> parametri = new HashMap<String, String>();
	    if (rataSelezionata != null) {
		parametri.put("numeroRate", rataSelezionata);
		reset = false;
	    }
	    if (periodicitaSelezionata != null) {
		parametri.put("periodicitaScadenza", periodicitaSelezionata);
		reset = false;
	    }
	    parametri.put("codiceCliente", codiceCliente);
	    parametri.put("identificativoFattura", identificativoFattura);
	    parametri.put("codiceCausaleRateizzabilita", codiceCausaleRateizzabilita);
	    parametri.put("codiceFattura", codiceFattura);

	    String codiceCausaleRateizzabilitaAlternativa = "";
	    if (rataSelezionata == null || periodicitaSelezionata == null) {
		codiceCausaleRateizzabilitaAlternativa = getIntent().getStringExtra("codiceCausaleRateizzabilitaAlternativa");
		parametri.put("codiceCausaleRateizzabilitaAlternativa", codiceCausaleRateizzabilitaAlternativa);
		reset = true;
	    }
	    if (!isNetworkAvailable(context)) {
		Intent intentOops = new Intent(getApplicationContext(), OopsPageBollettePagamentiRateModifica.class);
		intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.connessione_assente));
		intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
		intentOops.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
		intentOops.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
		intentOops.putExtra("tipoFornitura", getIntent().getStringExtra("tipoFornitura"));

		intentOops.putExtra("identificativoFattura", identificativoFattura);
		intentOops.putExtra("codiceCausaleRateizzabilita", codiceCausaleRateizzabilita);
		intentOops.putExtra("codiceFattura", codiceFattura);
		intentOops.putExtra("tipoPiano", tipoPiano);

		intentOops.putExtra("dataEmissione", getIntent().getStringExtra("dataEmissione"));
		intentOops.putExtra("dataScadenza", getIntent().getStringExtra("dataScadenza"));
		intentOops.putExtra("importoRateizzabile", getIntent().getStringExtra("importoRateizzabile"));
		if (rataSelezionata == null || periodicitaSelezionata == null) {
		    intentOops.putExtra("codiceCausaleRateizzabilitaAlternativa", codiceCausaleRateizzabilitaAlternativa);
		}
		else {
		    intentOops.putExtra("numeroRate", rataSelezionata);
		    intentOops.putExtra("periodicitaScadenza", periodicitaSelezionata);
		}
		intentOops.putExtra("reset", reset);
		
		intentOops.putExtra("domiciliazioneApprovata", getIntent().getBooleanExtra("domiciliazioneApprovata", false));
		intentOops.putExtra("infoDomiciliazioneApprovata",  getIntent().getBooleanExtra("infoDomiciliazioneApprovata", false));
		
		startActivity(intentOops);
		if (animation != null) {
		    animation.stop();
		    imageAnim.setVisibility(View.INVISIBLE);
		}
		overridePendingTransition(R.anim.fade, R.anim.hold);
	    }
	    else {
		/********************* chiamata calcola proposta ******************************/
		ServiceCall stAvFornServiceCall = ServiceCallFactory.createServiceCall(context);
		// stAvFornServiceCall.myCookieStore = new
		// PersistentCookieStore(context);

		try {
		    stAvFornServiceCall.executeHttpsGetBollettePagamentiCalcolaPropostaRateDettaglio(Constants.URL_CALCOLA_PROPOSTA_RATE, parametri, BollettePagamentiDettaglioPiano.this, null, tipoPiano, reset);
		}
		catch (Exception e) {
		    e.printStackTrace();
		    Intent intentOops = new Intent(getApplicationContext(), OopsPageBollettePagamentiRateModifica.class);
		    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
		    intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
		    intentOops.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
		    intentOops.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
		    intentOops.putExtra("tipoFornitura", getIntent().getStringExtra("tipoFornitura"));

		    intentOops.putExtra("identificativoFattura", identificativoFattura);
		    intentOops.putExtra("codiceCausaleRateizzabilita", codiceCausaleRateizzabilita);
		    intentOops.putExtra("codiceFattura", codiceFattura);
		    intentOops.putExtra("tipoPiano", tipoPiano);

		    intentOops.putExtra("dataEmissione", getIntent().getStringExtra("dataEmissione"));
		    intentOops.putExtra("dataScadenza", getIntent().getStringExtra("dataScadenza"));
		    intentOops.putExtra("importoRateizzabile", getIntent().getStringExtra("importoRateizzabile"));

		    if (rataSelezionata == null || periodicitaSelezionata == null) {
			intentOops.putExtra("codiceCausaleRateizzabilitaAlternativa", codiceCausaleRateizzabilitaAlternativa);
		    }
		    else {
			intentOops.putExtra("numeroRate", rataSelezionata);
			intentOops.putExtra("periodicitaScadenza", periodicitaSelezionata);
		    }
		    intentOops.putExtra("reset", reset);
		    
		    intentOops.putExtra("domiciliazioneApprovata", getIntent().getBooleanExtra("domiciliazioneApprovata", false));
			intentOops.putExtra("infoDomiciliazioneApprovata",  getIntent().getBooleanExtra("infoDomiciliazioneApprovata", false));
			
		    startActivity(intentOops);
		    overridePendingTransition(R.anim.fade, R.anim.hold);
		}
	    }
	}
	catch (Exception e) {
	    e.printStackTrace();
	}
    }

	private void popolaPaginaContenuto(JSONArray rispostaCalcolaPropostaJSON, LinearLayout pager, int helpInfoPiani) {
		try {
			pager.removeAllViews();
			View bollette_pagamenti_dettaglio_piano_entry = inflater.inflate(R.layout.bollette_pagamenti_dettaglio_piano_entry, null);
			TextView bolletta_n_valore = (TextView) bollette_pagamenti_dettaglio_piano_entry.findViewById(R.id.bolletta_n_valore);
			bolletta_n_valore.setText(getIntent().getStringExtra("codiceFattura"));

			TextView bolletta_del_data = (TextView) bollette_pagamenti_dettaglio_piano_entry.findViewById(R.id.bolletta_del_data);
			bolletta_del_data.setText(convertiData(getIntent().getStringExtra("dataEmissione")));

			TextView bolletta_scadenza_data = (TextView) bollette_pagamenti_dettaglio_piano_entry.findViewById(R.id.bolletta_scadenza_data);
			bolletta_scadenza_data.setText(convertiData(getIntent().getStringExtra("dataScadenza")));

			TextView bolletta_da_pagare_valore = (TextView) bollette_pagamenti_dettaglio_piano_entry.findViewById(R.id.bolletta_da_pagare_valore);
			String importoRateizzabile = getIntent().getStringExtra("importoRateizzabile") + " " + getResources().getString(R.string.euro);
			bolletta_da_pagare_valore.setText(importoRateizzabile);

			totaleRate = getIntent().getStringExtra("importoRateizzabile");

			LinearLayout pagerLista = (LinearLayout) bollette_pagamenti_dettaglio_piano_entry.findViewById(R.id.lista_pager);

			JSONArray arrayProposte = rispostaCalcolaPropostaJSON.getJSONObject(1).getJSONArray("propostaRateizzazione");

			popolaListaPiano(arrayProposte, pagerLista);

			TextView rate_valore = (TextView) bollette_pagamenti_dettaglio_piano_entry.findViewById(R.id.rate_valore);
			rate_valore.setText(numeroRate);

			TextView periodicita_valore = (TextView) bollette_pagamenti_dettaglio_piano_entry.findViewById(R.id.periodicita_valore);
			periodicita_valore.setText(periodocita);

			TextView nome_piano = (TextView) bollette_pagamenti_dettaglio_piano_entry.findViewById(R.id.nome_piano);
			String causaleString = "";

			// pagina in sovvraposizione
			final Dialog dialog = new Dialog(BollettePagamentiDettaglioPiano.this, android.R.style.Theme_Translucent_NoTitleBar);
			final View det_retail = inflaterDialog.inflate(R.layout.popup_info_piani_rateizzazione, null);
			dialog.setContentView(det_retail);

			dialog.setCancelable(true);

			final LinearLayout chiudi = (LinearLayout) dialog.findViewById(R.id.chiudi);
			chiudi.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					info = false;
					dialog.dismiss();
				}
			});
			final RelativeLayout piano_secondo_delibera = (RelativeLayout) dialog.findViewById(R.id.piano_secondo_delibera);
			final RelativeLayout piano_ritardo_fatturazione = (RelativeLayout) dialog.findViewById(R.id.piano_ritardo_fatturazione);
			
			final TextView piano_secondo_delibera_textview = (TextView)dialog.findViewById(R.id.piano_secondo_delibera_textview);
			final TextView piano_ritardo_fatturazione_textview = (TextView)dialog.findViewById(R.id.piano_ritardo_fatturazione_textview);
			boolean domApprovata = getIntent().getBooleanExtra("domiciliazioneApprovata", false);
			if(domApprovata){
				piano_secondo_delibera_textview.setText(getResources().getString(R.string.piano_secondo_delibera_domiciliati_text));
				piano_ritardo_fatturazione_textview.setText(getResources().getString(R.string.piano_ritardo_fatturazione_domiciliati_text));
			}else{
				piano_secondo_delibera_textview.setText(getResources().getString(R.string.piano_secondo_delibera_text));
				piano_ritardo_fatturazione_textview.setText(getResources().getString(R.string.piano_ritardo_fatturazione_text));
			}
			if (causale.equals(Constants.PIANO_SECONDO_DELIBERA)) {
				causaleString = "piano secondo delibera";
				piano_secondo_delibera.setVisibility(View.VISIBLE);
				piano_ritardo_fatturazione.setVisibility(View.GONE);
			} else if (causale.equals(Constants.PIANO_RITARDO_FATTURAZIONE)) {
				causaleString = "piano ritardo di fatturazione";
				piano_secondo_delibera.setVisibility(View.GONE);
				piano_ritardo_fatturazione.setVisibility(View.VISIBLE);
			}
			// solo per piano singolo e viene visualizzato la prima volta
			if (arrayProposte.length() == 1 && helpInfoPiani == 1) {
				if ((getIntent().getBooleanExtra("helpPage", false) == false) && info) {
					dialog.show();
				}
			}
			nome_piano.setText(causaleString);

			if (!tasso.equals("")) {
				TextView tasso_valore = (TextView) bollette_pagamenti_dettaglio_piano_entry.findViewById(R.id.tasso_valore);
				TextView tasso_label = (TextView) bollette_pagamenti_dettaglio_piano_entry.findViewById(R.id.tasso_label);
				tasso_label.setVisibility(View.VISIBLE);
				tasso_valore.setText(tasso);
			} else {
				TextView tasso_label = (TextView) bollette_pagamenti_dettaglio_piano_entry.findViewById(R.id.tasso_label);
				TextView tasso_valore = (TextView) bollette_pagamenti_dettaglio_piano_entry.findViewById(R.id.tasso_valore);
				tasso_label.setVisibility(View.GONE);
				tasso_valore.setVisibility(View.GONE);
			}
			TextView somma_valore = (TextView) bollette_pagamenti_dettaglio_piano_entry.findViewById(R.id.somma_valore);
			somma_valore.setText(totaleRate + " " + getResources().getString(R.string.euro));

			RelativeLayout bottoni = (RelativeLayout) bollette_pagamenti_dettaglio_piano_entry.findViewById(R.id.bottoni);
			RelativeLayout conferma_accetta_piano = (RelativeLayout) findViewById(R.id.conferma_accetta_piano);
			if (richiestaAccettata) {
				// il piano e' stato accettato, si visualizza il dettaglio piano senza i bottoni
				bottoni.setVisibility(View.GONE);
				conferma_accetta_piano.setVisibility(View.VISIBLE);
			} else {
				// il piano non e'stato ancora accettato, si visualizza il dettaglio piano normale (con bottoni e relativi listener)
				conferma_accetta_piano.setVisibility(View.GONE);
				bottoni.setVisibility(View.VISIBLE);
				final ImageView accetta_piano_btn = (ImageView) bollette_pagamenti_dettaglio_piano_entry.findViewById(R.id.accetta_piano_btn);
				accetta_piano_btn.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
				accetta_piano_btn.getBackground().invalidateSelf();
				accetta_piano_btn.setOnClickListener(new View.OnClickListener() {
					public void onClick(View v) {
						accetta_piano_btn.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
						accetta_piano_btn.getBackground().invalidateSelf();
						chiamataAttivaPropostaRateizzazione();
						accetta_piano_btn.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
						accetta_piano_btn.getBackground().invalidateSelf();
					}
				});

				final ImageView modifica_piano_btn = (ImageView) bollette_pagamenti_dettaglio_piano_entry.findViewById(R.id.modifica_piano_btn);
				modifica_piano_btn.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
				modifica_piano_btn.getBackground().invalidateSelf();
				modifica_piano_btn.setOnClickListener(new View.OnClickListener() {
					public void onClick(View v) {
						modifica_piano_btn.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
						modifica_piano_btn.getBackground().invalidateSelf();
						final Dialog dialog = new Dialog(BollettePagamentiDettaglioPiano.this, android.R.style.Theme_Translucent_NoTitleBar);

						final View popup_modifica_piano = inflaterDialog.inflate(R.layout.popup_modifica_piano_rateizzazione, null);

						dialog.setContentView(popup_modifica_piano);
						dialog.setCancelable(true);
						impostaPickers(dialog);

						modifica_piano_btn.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
						modifica_piano_btn.getBackground().invalidateSelf();

						RelativeLayout rate_picker = (RelativeLayout) dialog.findViewById(R.id.rate_picker);
						TextView testo_info_rate_non_diminuibili = (TextView) dialog.findViewById(R.id.testo_info_rate_non_diminuibili);
						if (rate.length == 1) {
							rate_picker.setBackgroundResource(R.drawable.rate_picker_disabled_entry);
							testo_info_rate_non_diminuibili.setVisibility(View.VISIBLE);
						} else {
							testo_info_rate_non_diminuibili.setVisibility(View.GONE);
							ImageView da_mese_up = (ImageView) dialog.findViewById(R.id.da_mese_up);
							da_mese_up.setOnClickListener(new View.OnClickListener() {
								public void onClick(View v) {
									rataUp(dialog);
								}
							});
							ImageView da_mese_down = (ImageView) dialog.findViewById(R.id.da_mese_down);
							da_mese_down.setOnClickListener(new View.OnClickListener() {
								public void onClick(View v) {
									rataDown(dialog);
								}
							});
						}

						RelativeLayout periodicita_picker = (RelativeLayout) dialog.findViewById(R.id.periodicita_picker);
						if (arrayPeriodicita.length == 1) {
							periodicita_picker.setBackgroundResource(R.drawable.periodicita_picker_disabled_entry);
						} else {
							ImageView a_mese_up = (ImageView) dialog.findViewById(R.id.a_mese_up);
							a_mese_up.setOnClickListener(new View.OnClickListener() {
								public void onClick(View v) {
									periodicitaUp(dialog);
								}
							});

							ImageView a_mese_down = (ImageView) dialog.findViewById(R.id.a_mese_down);
							a_mese_down.setOnClickListener(new View.OnClickListener() {
								public void onClick(View v) {
									periodicitaDown(dialog);
								}
							});
						}

						ImageView annulla_piano_btn = (ImageView) dialog.findViewById(R.id.annulla_piano_btn);
						annulla_piano_btn.setOnClickListener(new View.OnClickListener() {
							public void onClick(View v) {
								dialog.dismiss();
							}
						});
						ImageView calcola_piano_btn = (ImageView) dialog.findViewById(R.id.calcola_piano_btn);
						calcola_piano_btn.setOnClickListener(new View.OnClickListener() {
							public void onClick(View v) {
								// inizio modifiche diego

								rataCorrentePicker = ((TextView) dialog.findViewById(R.id.da_mese_corrente)).getText().toString();
								periodicitaCorrentePicker = ((TextView) dialog.findViewById(R.id.a_mese_corrente)).getText().toString();

								int tipoPiano = getIntent().getIntExtra("tipoPiano", Constants.PIANO_RATEIZZAZIONE_SINGOLO);
								dialog.dismiss();
								chiamataCalcolaProposta(tipoPiano, rataCorrentePicker, periodicitaCorrentePicker);
							}
						});
						dialog.show();
					}
				});
			}

			pager.addView(bollette_pagamenti_dettaglio_piano_entry);
		} catch (JSONException e) {
			e.printStackTrace();
		}

	}

    private void popolaListaPiano(JSONArray arrayProposte, LinearLayout pagerLista) {
	try {
	    LayoutInflater inflaterRiga = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);
	    JSONObject proposta = new JSONObject();

	    if (arrayProposte.length() == 1) {
		// singolo
		proposta = arrayProposte.getJSONObject(0);
	    }
	    else {
		// doppio
		String causaleSelezionata = getIntent().getStringExtra("pianoSelezionato");
		for (int p = 0; p < arrayProposte.length(); p++) {
		    proposta = arrayProposte.getJSONObject(p);
		    String causaleI = proposta.getString("causaleRateizzabilita");
		    if (causaleI.equals(causaleSelezionata)) {
			break;
		    }
		}
	    }
	    JSONArray arrayRate = proposta.getJSONArray("pianoRate");
	    // float totaleImportoRate = 0;
	    for (int i = 0; i < arrayRate.length(); i++) {
		View bollette_pagamenti_dettaglio_piano_riga = inflaterRiga.inflate(R.layout.bollette_pagamenti_dettaglio_riga_piano, null);
		RelativeLayout riga_lista_piano = (RelativeLayout) bollette_pagamenti_dettaglio_piano_riga.findViewById(R.id.riga_lista_piano);

		JSONObject rata = arrayRate.getJSONObject(i);
		TextView numero_riga = (TextView) bollette_pagamenti_dettaglio_piano_riga.findViewById(R.id.numero_riga);
		numero_riga.setText(rata.getString("numeroRata"));

		int numero_rata = Integer.valueOf(rata.getString("numeroRata"));
		if (numero_rata % 2 == 0) {
		    riga_lista_piano.setBackgroundResource(R.drawable.riga_tabella_lista_dettaglio_rate_bck_grigio);
		}
		else {
		    riga_lista_piano.setBackgroundResource(R.drawable.riga_tabella_lista_dettaglio_rate_bck_chiaro);
		}
		TextView mese = (TextView) bollette_pagamenti_dettaglio_piano_riga.findViewById(R.id.mese);
		mese.setText(rata.getString("dataScadenza"));

		TextView importo = (TextView) bollette_pagamenti_dettaglio_piano_riga.findViewById(R.id.importo);
		importo.setText(rata.getString("importoRata") + " " + getResources().getString(R.string.euro));
		/*
		 * String importoIesimo =
		 * rata.getString("importoRata").replaceAll(".",
		 * "").replace(",", "."); float importoF = new
		 * Float(importoIesimo); totaleImportoRate += importoF;
		 */

		pagerLista.addView(bollette_pagamenti_dettaglio_piano_riga);
	    }
	    // totaleRate = totaleImportoRate;
	    numeroRate = proposta.getString("numeroRate");
	    int numeroRateI = Integer.valueOf(numeroRate);
	    if (maxRata != -1) {
		rate = new String[(maxRata - 2) + 1];
		for (int r = 0; r < (maxRata - 2) + 1; r++) {
		    rate[r] = "" + (r + 2);
		}
	    }
	    else {
		rate = new String[(numeroRateI - 2) + 1];
		for (int r = 0; r < (numeroRateI - 2) + 1; r++) {
		    rate[r] = "" + (r + 2);
		}
		maxRata = numeroRateI;
	    }

	    periodocita = proposta.getString("periodicitaScadenza").toLowerCase();
	    dataAttivabilita = proposta.getString("dataAttivabilita");
	    JSONArray periodicitAlternativa = proposta.getJSONArray("periodicitaAlternativa");
	    arrayPeriodicita = new String[periodicitAlternativa.length() + 1];
	    arrayPeriodicita[0] = periodocita;
	    for (int p = 0; p < periodicitAlternativa.length(); p++) {
		JSONObject periodicitaIesima = periodicitAlternativa.getJSONObject(p);
		arrayPeriodicita[p + 1] = periodicitaIesima.getString("periodicitaScadenzaAlternativa").toLowerCase();
	    }
	    causale = proposta.getString("causaleRateizzabilita");
	    tasso = proposta.getString("tassoInteresse");
	}
	catch (JSONException e) {
	    e.printStackTrace();
	}
    }

    private void chiamataAttivaPropostaRateizzazione() {

	animation = Utilities.animation(this);
	imageAnim = (ImageView) findViewById(R.id.waiting_anim);
	imageAnim.setVisibility(View.VISIBLE);
	if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
	    imageAnim.setBackgroundDrawable(animation);
	}
	else {
	    imageAnim.setBackground(animation);
	}
	animation.start();

	// obbligatori
	// codice Cliente, codiceContoCliente, userId, identificativoFattura,
	// codiceCausaleRateizzabilita, codice Fattura,
	// numeroRate,periodicitaScadenza, cognome
	// non obbligatori
	// codiceContratto, nome

	String codiceCliente = getCodiceCliente(rispostaLogin);
	String codiceContoCliente = getIntent().getStringExtra("PRIMOCONTO");
	String userId = getUserId(rispostaLogin);
	String identificativoFattura = getIntent().getStringExtra("identificativoFattura");
	String codiceCausaleRateizzabilita = getIntent().getStringExtra("codiceCausaleRateizzabilita");
	String codiceFattura = getIntent().getStringExtra("codiceFattura");
	String numeroRate = getNumeroRate(Constants.SERVIZIO_CALCOLA_PROPOSTA_RATE, codiceCausaleRateizzabilita);
	String periodicitaScadenza = getPeriodicitaScadenza(Constants.SERVIZIO_CALCOLA_PROPOSTA_RATE, codiceCausaleRateizzabilita);
	String cognome = getCognome(rispostaLogin);

	// tipoFornitura � "GAS" o "POWER"
	String tipoFornitura = getIntent().getStringExtra("tipoFornitura");
	String codiceContratto = getCodiceContratto(rispostaLogin, codiceContoCliente, tipoFornitura);
	// String nome = getNome(rispostaLogin);

	HashMap<String, String> parametri = new HashMap<String, String>();
	parametri.put("codiceCliente", codiceCliente);
	parametri.put("codiceContoCliente", codiceContoCliente);
	parametri.put("userId", userId);
	parametri.put("identificativoFattura", identificativoFattura);
	parametri.put("codiceCausaleRateizzabilita", codiceCausaleRateizzabilita);
	parametri.put("codiceFattura", codiceFattura);
	parametri.put("numeroRate", numeroRate);
	parametri.put("periodicitaScadenza", periodicitaScadenza);
	parametri.put("cognome", cognome);
	parametri.put("codiceContratto", codiceContratto);

	if (!isNetworkAvailable(context)) {
	    Intent intentOops = new Intent(getApplicationContext(), OopsPageBollettePagamentiDettaglioPiano.class);
	    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.connessione_assente));
	    intentOops.putExtra("codiceCliente", codiceCliente);
	    intentOops.putExtra("codiceContoCliente", codiceContoCliente);
	    intentOops.putExtra("userId", userId);
	    intentOops.putExtra("identificativoFattura", identificativoFattura);
	    intentOops.putExtra("codiceCausaleRateizzabilita", codiceCausaleRateizzabilita);
	    intentOops.putExtra("codiceFattura", codiceFattura);
	    intentOops.putExtra("numeroRate", numeroRate);
	    intentOops.putExtra("periodicitaScadenza", periodicitaScadenza);
	    intentOops.putExtra("cognome", cognome);
	    intentOops.putExtra("codiceContratto", codiceContratto);
	    intentOops.putExtra("accettaModifica", "ACCETTA");
	    intentOops.putExtra("domiciliazioneApprovata", getIntent().getBooleanExtra("domiciliazioneApprovata", false));
		intentOops.putExtra("infoDomiciliazioneApprovata",  getIntent().getBooleanExtra("infoDomiciliazioneApprovata", false));
		
	    startActivity(intentOops);
	    if (animation != null) {
		animation.stop();
		imageAnim.setVisibility(View.INVISIBLE);
	    }
	    overridePendingTransition(R.anim.fade, R.anim.hold);
	}
	else {
	    /********************* chiamata attiva proposta ******************************/
	    ServiceCall stAvFornServiceCall = ServiceCallFactory.createServiceCall(context);
	    // stAvFornServiceCall.myCookieStore = new
	    // PersistentCookieStore(context);

	    try {
		stAvFornServiceCall.executeHttpsGetBollettePagamentiAttivaPropostaPiano(Constants.URL_ATTIVA_PROPOSTA_RATEIZZAZIONE, parametri, BollettePagamentiDettaglioPiano.this, null, "ACCETTA");
	    }
	    catch (Exception e) {
		Intent intentOops = new Intent(getApplicationContext(), OopsPageBollettePagamentiDettaglioPiano.class);
		intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
		intentOops.putExtra("codiceCliente", codiceCliente);
		intentOops.putExtra("codiceContoCliente", codiceContoCliente);
		intentOops.putExtra("userId", userId);
		intentOops.putExtra("identificativoFattura", identificativoFattura);
		intentOops.putExtra("codiceCausaleRateizzabilita", codiceCausaleRateizzabilita);
		intentOops.putExtra("codiceFattura", codiceFattura);
		intentOops.putExtra("numeroRate", numeroRate);
		intentOops.putExtra("periodicitaScadenza", periodicitaScadenza);
		intentOops.putExtra("cognome", cognome);
		intentOops.putExtra("codiceContratto", codiceContratto);
		intentOops.putExtra("accettaModifica", "ACCETTA");
		intentOops.putExtra("domiciliazioneApprovata", getIntent().getBooleanExtra("domiciliazioneApprovata", false));
		intentOops.putExtra("infoDomiciliazioneApprovata",  getIntent().getBooleanExtra("infoDomiciliazioneApprovata", false));
		startActivity(intentOops);
		overridePendingTransition(R.anim.fade, R.anim.hold);
	    }
	}
    }

    public void postChiamata(String rispostaChiamata, List<Cookie> lista, HashMap<String, String> parametri, int tipoPiano, String accettaOModifica, boolean reset) {
	if (animation != null) {
	    animation.stop();
	    imageAnim.setVisibility(View.INVISIBLE);
	}

	JSONArray rispostaJSON = new JSONArray();
	String esito = "";
	String descEsito = "";
	JSONArray proposte = new JSONArray();
	try {
	    rispostaJSON = new JSONArray(rispostaChiamata);
	    JSONObject oggettoRisposta = (JSONObject) rispostaJSON.get(0);
	    esito = oggettoRisposta.getString("esito");
	    descEsito = oggettoRisposta.getString("descEsito");
	    JSONArray calcolaPropostaJSON = new JSONArray(Constants.SERVIZIO_CALCOLA_PROPOSTA_RATE);
	    proposte = calcolaPropostaJSON.getJSONObject(1).getJSONArray("propostaRateizzazione");
	}
	catch (JSONException e) {
	    e.printStackTrace();
	}
	if (esito.equals("200")) {
	    if (accettaOModifica.equals("ACCETTA")) {
		// accetta � andata a buon fine
		richiestaAccettata = true;
		try {
		    JSONArray rispostaCalcolaPropostaJSON = new JSONArray(Constants.SERVIZIO_CALCOLA_PROPOSTA_RATE);
		    popolaPaginaContenuto(rispostaCalcolaPropostaJSON, pager, 0);
		}
		catch (JSONException e) {
		    e.printStackTrace();
		}
	    }
	    else {
		// modifica
		if (!reset) {
		    Constants.SERVIZIO_CALCOLA_PROPOSTA_RATE = rispostaChiamata;
		    try {
			JSONArray rispostaCalcolaPropostaJSON = new JSONArray(Constants.SERVIZIO_CALCOLA_PROPOSTA_RATE);
			popolaPaginaContenuto(rispostaCalcolaPropostaJSON, pager, 0);
		    }
		    catch (JSONException e) {
			e.printStackTrace();
		    }
		}
		else {
		    // ho aggiornato calcolaProposte ed ho fatto indietro per scelta piani
		    Constants.SERVIZIO_CALCOLA_PROPOSTA_RATE = rispostaChiamata;
		    Intent intentSceltaPiano = new Intent(getApplicationContext(), BollettePagamentiSceltaPiano.class);
		    intentSceltaPiano.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
		    intentSceltaPiano.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
		    intentSceltaPiano.putExtra("tipoFornitura", getIntent().getStringExtra("tipoFornitura"));
		    intentSceltaPiano.putExtra("tipoPiano", Constants.PIANO_RATEIZZAZIONE_DOPPIO);
		    intentSceltaPiano.putExtra("identificativoFattura", parametri.get("identificativoFattura"));
		    intentSceltaPiano.putExtra("codiceCausaleRateizzabilitaAlternativa", parametri.get("codiceCausaleRateizzabilitaAlternativa"));

		    intentSceltaPiano.putExtra("codiceFattura", parametri.get("codiceFattura"));
		    intentSceltaPiano.putExtra("dataEmissione", getIntent().getStringExtra("dataEmissione"));
		    intentSceltaPiano.putExtra("dataScadenza", getIntent().getStringExtra("dataScadenza"));
		    intentSceltaPiano.putExtra("importoRateizzabile", getIntent().getStringExtra("importoRateizzabile"));
		    intentSceltaPiano.putExtra("codiceCausaleRateizzabilita", parametri.get("codiceCausaleRateizzabilita"));

		    intentSceltaPiano.putExtra("domiciliazioneApprovata", getIntent().getBooleanExtra("domiciliazioneApprovata", false));
		    intentSceltaPiano.putExtra("infoDomiciliazioneApprovata",  getIntent().getBooleanExtra("infoDomiciliazioneApprovata", false));
		    BollettePagamentiDettaglioPiano.this.finish();
		    startActivity(intentSceltaPiano);
		    overridePendingTransition(R.anim.fade, R.anim.hold);
		}
	    }
	}
	else if (esito.equals("464")) {
	    if (accettaOModifica.equals("ACCETTA")) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setTitle("Attenzione!").setMessage(descEsito).setCancelable(false).setPositiveButton("Riprova", new DialogInterface.OnClickListener() {
		    public void onClick(DialogInterface dialog, int id) {
			dialog.dismiss();
		    }
		});
		builder.create().show();
	    }
	    else {
		if (!reset) {
		    AlertDialog.Builder builder = new AlertDialog.Builder(context);
		    builder.setTitle("Attenzione!").setMessage(descEsito).setCancelable(false).setPositiveButton("Riprova", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
			    dialog.dismiss();
			}
		    });
		    builder.create().show();
		}
	    }
	}
	else if (esito.equals("467")) {
	    if (accettaOModifica.equals("ACCETTA")) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setTitle("Attenzione!").setMessage(descEsito).setCancelable(false).setPositiveButton("Riprova", new DialogInterface.OnClickListener() {
		    public void onClick(DialogInterface dialog, int id) {
			dialog.dismiss();
		    }
		});
		builder.create().show();
	    }
	}
	else {
	    Intent intentOops = new Intent();
	    if (accettaOModifica.equals("ACCETTA")) {
		intentOops = new Intent(getApplicationContext(), OopsPageBollettePagamentiDettaglioPiano.class);
		intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
		intentOops.putExtra("codiceCliente", parametri.get("codiceCliente"));
		intentOops.putExtra("codiceContoCliente", parametri.get("codiceContoCliente"));
		intentOops.putExtra("userId", parametri.get("userId"));
		intentOops.putExtra("identificativoFattura", parametri.get("identificativoFattura"));
		intentOops.putExtra("codiceCausaleRateizzabilita", parametri.get("codiceCausaleRateizzabilita"));
		intentOops.putExtra("codiceFattura", parametri.get("codiceFattura"));
		intentOops.putExtra("numeroRate", parametri.get("numeroRate"));
		intentOops.putExtra("periodicitaScadenza", parametri.get("periodicitaScadenza"));
		intentOops.putExtra("cognome", parametri.get("cognome"));
		intentOops.putExtra("codiceContratto", parametri.get("codiceContratto"));
		intentOops.putExtra("accettaModifica", accettaOModifica);
		intentOops.putExtra("domiciliazioneApprovata", getIntent().getBooleanExtra("domiciliazioneApprovata", false));
		intentOops.putExtra("infoDomiciliazioneApprovata",  getIntent().getBooleanExtra("infoDomiciliazioneApprovata", false));
	    }
	    else {
		if (!reset) {
		    intentOops = new Intent(getApplicationContext(), OopsPageBollettePagamentiRateModifica.class);
		    intentOops.putExtra("CODICE_CLIENTE", parametri.get("codiceCliente"));
		    intentOops.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
		    intentOops.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));

		    intentOops.putExtra("numeroRate", rataCorrentePicker);
		    intentOops.putExtra("periodicitaScadenza", periodicitaCorrentePicker);
		    intentOops.putExtra("identificativoFattura", parametri.get("identificativoFattura"));
		    intentOops.putExtra("codiceCausaleRateizzabilita", parametri.get("codiceCausaleRateizzabilita"));
		    intentOops.putExtra("codiceFattura", parametri.get("codiceFattura"));

		    intentOops.putExtra("tipoPiano", tipoPiano);
		    intentOops.putExtra("tipoFornitura", getIntent().getStringExtra("tipoFornitura"));

		    intentOops.putExtra("dataEmissione", getIntent().getStringExtra("dataEmissione"));
		    intentOops.putExtra("dataScadenza", getIntent().getStringExtra("dataScadenza"));
		    intentOops.putExtra("importoRateizzabile", getIntent().getStringExtra("importoRateizzabile"));
		    
		    intentOops.putExtra("domiciliazioneApprovata", getIntent().getBooleanExtra("domiciliazioneApprovata", false));
			intentOops.putExtra("infoDomiciliazioneApprovata",  getIntent().getBooleanExtra("infoDomiciliazioneApprovata", false));
		    /*
		     * if(tipoPiano == 2){
		     * intentOops.putExtra("codiceCausaleRateizzabilitaAlternativa"
		     * , getIntent().getStringExtra(
		     * "codiceCausaleRateizzabilitaAlternativa")); }
		     */
		}
		else {
		    intentOops = new Intent(getApplicationContext(), OopsPageBollettePagamentiRateModifica.class);
		    intentOops.putExtra("CODICE_CLIENTE", parametri.get("codiceCliente"));
		    intentOops.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
		    intentOops.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));

		    intentOops.putExtra("identificativoFattura", parametri.get("identificativoFattura"));
		    intentOops.putExtra("codiceCausaleRateizzabilita", parametri.get("codiceCausaleRateizzabilita"));
		    intentOops.putExtra("codiceFattura", parametri.get("codiceFattura"));

		    intentOops.putExtra("tipoPiano", tipoPiano);
		    intentOops.putExtra("tipoFornitura", getIntent().getStringExtra("tipoFornitura"));

		    intentOops.putExtra("dataEmissione", getIntent().getStringExtra("dataEmissione"));
		    intentOops.putExtra("dataScadenza", getIntent().getStringExtra("dataScadenza"));
		    intentOops.putExtra("importoRateizzabile", getIntent().getStringExtra("importoRateizzabile"));
		    intentOops.putExtra("codiceCausaleRateizzabilitaAlternativa", getIntent().getStringExtra("codiceCausaleRateizzabilitaAlternativa"));
		    
		    intentOops.putExtra("domiciliazioneApprovata", getIntent().getBooleanExtra("domiciliazioneApprovata", false));
			intentOops.putExtra("infoDomiciliazioneApprovata",  getIntent().getBooleanExtra("infoDomiciliazioneApprovata", false));
		}

	    }
	    // errori popup
	    if (esito.equals("307")) {
		Utils.popup(BollettePagamentiDettaglioPiano.this, getResources().getString(R.string.errore_popup_307), "Riprova");

	    }
	    else if (esito.equals("435")) {
		Utils.popup(BollettePagamentiDettaglioPiano.this, getResources().getString(R.string.errore_popup_435), "Riprova");

	    }
	    else if (esito.equals("437")) {
		Utils.popup(BollettePagamentiDettaglioPiano.this, getResources().getString(R.string.errore_popup_437), "Riprova");

	    }
	    else if (esito.equals("438")) {
		Utils.popup(BollettePagamentiDettaglioPiano.this, getResources().getString(R.string.errore_popup_438), "Riprova");

	    }
	    else if (esito.equals("443")) {
		Utils.popup(BollettePagamentiDettaglioPiano.this, getResources().getString(R.string.errore_popup_443), "Riprova");

	    }
	    else if (esito.equals("444")) {
		Utils.popup(BollettePagamentiDettaglioPiano.this, getResources().getString(R.string.errore_popup_444), "Riprova");

	    }
	    else if (esito.equals("445")) {
		Utils.popup(BollettePagamentiDettaglioPiano.this, getResources().getString(R.string.errore_popup_445), "Riprova");

	    }
	    else if (esito.equals("446")) {
		Utils.popup(BollettePagamentiDettaglioPiano.this, getResources().getString(R.string.errore_popup_446), "Riprova");

	    }
	    else if (esito.equals("447")) {
		Utils.popup(BollettePagamentiDettaglioPiano.this, getResources().getString(R.string.errore_popup_447), "Riprova");

	    }
	    if (esito.equals("451")) {
		intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_popup_451));
		startActivity(intentOops);
		overridePendingTransition(R.anim.fade, R.anim.hold);
	    }
	    else if (esito.equals("466")) {
		if (accettaOModifica.equals("ACCETTA")) {
		    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_popup_466) + " " + dataAttivabilita);
		    startActivity(intentOops);
		    overridePendingTransition(R.anim.fade, R.anim.hold);
		}
	    }
	    else {
	    	if (descEsito.equals("")) {
				intentOops = new Intent(getApplicationContext(), OopsPageSessioneScaduta.class);
				intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.sessione_scaduta));
			} else{
				intentOops.putExtra("MESSAGGIO", descEsito);
			}
			
			startActivity(intentOops);
			overridePendingTransition(R.anim.fade, R.anim.hold);
	    }
	}
    }

    public void postChiamataException(boolean exception, HashMap<String, String> parameterMap, int tipoPiano, String accettaOModifica, boolean reset) {
	if (exception) {
	    if (animation != null) {
		animation.stop();
		imageAnim.setVisibility(View.INVISIBLE);
	    }
	    if (accettaOModifica.equals("ACCETTA")) {
		Intent intentOops = new Intent(getApplicationContext(), OopsPageBollettePagamentiDettaglioPiano.class);
		intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
		intentOops.putExtra("codiceCliente", parameterMap.get("codiceCliente"));
		intentOops.putExtra("codiceContoCliente", parameterMap.get("codiceContoCliente"));
		intentOops.putExtra("userId", parameterMap.get("userId"));
		intentOops.putExtra("identificativoFattura", parameterMap.get("identificativoFattura"));
		intentOops.putExtra("codiceCausaleRateizzabilita", parameterMap.get("codiceCausaleRateizzabilita"));
		intentOops.putExtra("codiceFattura", parameterMap.get("codiceFattura"));
		intentOops.putExtra("numeroRate", parameterMap.get("numeroRate"));
		intentOops.putExtra("periodicitaScadenza", parameterMap.get("periodicitaScadenza"));
		intentOops.putExtra("cognome", parameterMap.get("cognome"));
		intentOops.putExtra("codiceContratto", parameterMap.get("codiceContratto"));
		intentOops.putExtra("accettaModifica", accettaOModifica);
		
		intentOops.putExtra("domiciliazioneApprovata", getIntent().getBooleanExtra("domiciliazioneApprovata", false));
		intentOops.putExtra("infoDomiciliazioneApprovata",  getIntent().getBooleanExtra("infoDomiciliazioneApprovata", false));
		
		startActivity(intentOops);
		overridePendingTransition(R.anim.fade, R.anim.hold);
	    }
	    else {
		if (!reset) {
		    Intent intentOops = new Intent(getApplicationContext(), OopsPageBollettePagamentiRateModifica.class);
		    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
		    intentOops.putExtra("CODICE_CLIENTE", parameterMap.get("codiceCliente"));
		    intentOops.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
		    intentOops.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));

		    intentOops.putExtra("dataEmissione", getIntent().getStringExtra("dataEmissione"));
		    intentOops.putExtra("dataScadenza", getIntent().getStringExtra("dataScadenza"));
		    intentOops.putExtra("importoRateizzabile", getIntent().getStringExtra("importoRateizzabile"));

		    intentOops.putExtra("numeroRate", rataCorrentePicker);
		    intentOops.putExtra("periodicitaScadenza", periodicitaCorrentePicker);

		    intentOops.putExtra("identificativoFattura", parameterMap.get("identificativoFattura"));
		    intentOops.putExtra("codiceCausaleRateizzabilita", parameterMap.get("codiceCausaleRateizzabilita"));
		    intentOops.putExtra("codiceFattura", parameterMap.get("codiceFattura"));

		    intentOops.putExtra("tipoPiano", tipoPiano);
		    intentOops.putExtra("tipoFornitura", getIntent().getStringExtra("tipoFornitura"));
		    
		    intentOops.putExtra("domiciliazioneApprovata", getIntent().getBooleanExtra("domiciliazioneApprovata", false));
			intentOops.putExtra("infoDomiciliazioneApprovata",  getIntent().getBooleanExtra("infoDomiciliazioneApprovata", false));
			
		    /*
		     * if(tipoPiano == 2){
		     * intentOops.putExtra("codiceCausaleRateizzabilitaAlternativa"
		     * , getIntent().getStringExtra(
		     * "codiceCausaleRateizzabilitaAlternativa")); }
		     */
		    startActivity(intentOops);
		    overridePendingTransition(R.anim.fade, R.anim.hold);
		}
		else {
		    Intent intentOops = new Intent(getApplicationContext(), OopsPageBollettePagamentiRateModifica.class);
		    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
		    intentOops.putExtra("CODICE_CLIENTE", parameterMap.get("codiceCliente"));
		    intentOops.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
		    intentOops.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));

		    intentOops.putExtra("dataEmissione", getIntent().getStringExtra("dataEmissione"));
		    intentOops.putExtra("dataScadenza", getIntent().getStringExtra("dataScadenza"));
		    intentOops.putExtra("importoRateizzabile", getIntent().getStringExtra("importoRateizzabile"));

		    intentOops.putExtra("identificativoFattura", parameterMap.get("identificativoFattura"));
		    intentOops.putExtra("codiceCausaleRateizzabilita", parameterMap.get("codiceCausaleRateizzabilita"));
		    intentOops.putExtra("codiceFattura", parameterMap.get("codiceFattura"));

		    intentOops.putExtra("tipoPiano", tipoPiano);
		    intentOops.putExtra("tipoFornitura", getIntent().getStringExtra("tipoFornitura"));
		    intentOops.putExtra("codiceCausaleRateizzabilitaAlternativa", getIntent().getStringExtra("codiceCausaleRateizzabilitaAlternativa"));
		    
		    intentOops.putExtra("domiciliazioneApprovata", getIntent().getBooleanExtra("domiciliazioneApprovata", false));
			intentOops.putExtra("infoDomiciliazioneApprovata",  getIntent().getBooleanExtra("infoDomiciliazioneApprovata", false));
			
		    startActivity(intentOops);
		    overridePendingTransition(R.anim.fade, R.anim.hold);
		}

	    }

	}
    }

    public String getCodiceCliente(String rispostaLogin) {
	String result = "";
	try {
	    JSONArray loginJson = new JSONArray(rispostaLogin);
	    JSONObject datiCliente = (JSONObject) loginJson.get(1);
	    result = datiCliente.getString("CodiceCliente");
	}
	catch (JSONException e) {
	    e.printStackTrace();
	}
	return result;
    }

    public String getUserId(String rispostaLogin) {
	String result = "";
	try {
	    JSONArray loginJson = new JSONArray(rispostaLogin);
	    JSONObject datiCliente = (JSONObject) loginJson.get(1);
	    result = datiCliente.getString("UserId");
	}
	catch (JSONException e) {
	    e.printStackTrace();
	}
	return result;
    }

    public String getCognome(String rispostaLogin) {
	String result = "";
	try {
	    JSONArray loginJson = new JSONArray(rispostaLogin);
	    JSONObject datiCliente = (JSONObject) loginJson.get(1);
	    result = datiCliente.getString("CognomeCliente");
	}
	catch (JSONException e) {
	    e.printStackTrace();
	}
	return result;
    }

    public String getNome(String rispostaLogin) {
	String result = "";
	try {
	    JSONArray loginJson = new JSONArray(rispostaLogin);
	    JSONObject datiCliente = (JSONObject) loginJson.get(1);
	    result = datiCliente.getString("NomeCliente");
	}
	catch (JSONException e) {
	    e.printStackTrace();
	}
	return result;
    }

    public void impostaPickers(Dialog dialog) {

	// Da

	TextView da_mese_corrente = (TextView) dialog.findViewById(R.id.da_mese_corrente);
	int numeroRateInt = Integer.valueOf(numeroRate);
	da_mese_corrente.setText("" + rate[numeroRateInt - 2]);

	TextView da_mese_precedente = (TextView) dialog.findViewById(R.id.da_mese_precedente);
	if ((numeroRateInt - 2) > 0) {
	    da_mese_precedente.setText(rate[(numeroRateInt - 2) - 1]);
	}
	else {
	    da_mese_precedente.setText("");
	}

	TextView da_mese_successivo = (TextView) dialog.findViewById(R.id.da_mese_sucessivo);
	if ((numeroRateInt - 2) + 1 < rate.length) {
	    da_mese_successivo.setText(rate[(numeroRateInt - 2) + 1]);
	}
	else {
	    da_mese_successivo.setText("");
	}

	// A
	TextView a_mese_corrente = (TextView) dialog.findViewById(R.id.a_mese_corrente);
	a_mese_corrente.setText("" + arrayPeriodicita[0]);

	TextView a_mese_precedente = (TextView) dialog.findViewById(R.id.a_mese_precedente);
	a_mese_precedente.setText("");

	TextView a_mese_successivo = (TextView) dialog.findViewById(R.id.a_mese_sucessivo);
	if (arrayPeriodicita.length > 1) {
	    a_mese_successivo.setText(arrayPeriodicita[1]);
	}
	else {
	    a_mese_successivo.setText("");
	}

    }

    private String getNumeroRate(String rispostaCalcolaProposta, String codiceCausaleRateizzabilita) {
	String result = "";
	try {
	    JSONArray rispostaCalcolaPropostaJSON = new JSONArray(rispostaCalcolaProposta);
	    JSONArray arrayProposte = rispostaCalcolaPropostaJSON.getJSONObject(1).getJSONArray("propostaRateizzazione");
	    JSONObject proposta = new JSONObject();
	    for (int p = 0; p < arrayProposte.length(); p++) {
		proposta = arrayProposte.getJSONObject(p);
		String causaleI = proposta.getString("causaleRateizzabilita");
		if (causaleI.equals(codiceCausaleRateizzabilita)) {
		    break;
		}
	    }
	    result = proposta.getString("numeroRate");
	}
	catch (JSONException e) {
	    e.printStackTrace();
	}
	return result;
    }

    private String getPeriodicitaScadenza(String rispostaCalcolaProposta, String codiceCausaleRateizzabilita) {
	String result = "";
	try {
	    JSONArray rispostaCalcolaPropostaJSON = new JSONArray(rispostaCalcolaProposta);
	    JSONArray arrayProposte = rispostaCalcolaPropostaJSON.getJSONObject(1).getJSONArray("propostaRateizzazione");
	    JSONObject proposta = new JSONObject();
	    for (int p = 0; p < arrayProposte.length(); p++) {
		proposta = arrayProposte.getJSONObject(p);
		String causaleI = proposta.getString("causaleRateizzabilita");
		if (causaleI.equals(codiceCausaleRateizzabilita)) {
		    break;
		}
	    }
	    result = proposta.getString("periodicitaScadenza");
	}
	catch (JSONException e) {
	    e.printStackTrace();
	}
	return result;
    }

    public String getCodiceContratto(String rispostaLogin, String codiceContoCliente, String tipoFornitura) {
	String result = "";
	try {
	    JSONArray loginJson = new JSONArray(rispostaLogin);
	    JSONArray conto = new JSONArray();
	    JSONObject anagrConto = new JSONObject();
	    for (int i = 2; i < loginJson.length(); i++) {
		conto = loginJson.getJSONArray(i);
		anagrConto = conto.getJSONObject(0);
		String codice = anagrConto.getString("CodiceConto");
		if (!codice.equals(codiceContoCliente)) {
		    continue;
		}
		else {
		    if (conto.length() < 2) {
			continue;
		    }
		    else {
			JSONObject anagrExtra = conto.getJSONObject(1);
			String tipoConto = anagrExtra.getString("Tipologia");
			if (!tipoConto.equals(tipoFornitura)) {
			    continue;
			}
			else {
			    result = anagrConto.getString("CodiceContratto");
			}
		    }
		}
	    }
	}
	catch (JSONException e) {
	    e.printStackTrace();
	}
	return result;
    }

    public void rataUp(Dialog dialog) {
	TextView ratacorrente = (TextView) dialog.findViewById(R.id.da_mese_corrente);
	TextView rataprecedente = (TextView) dialog.findViewById(R.id.da_mese_precedente);
	TextView ratasucessivo = (TextView) dialog.findViewById(R.id.da_mese_sucessivo);
	if (!ratasucessivo.getText().toString().equals("")) {
	    String damesecorrenteAttuale = ratacorrente.getText().toString();
	    String damesecorrenteModificato = aumentaRata(damesecorrenteAttuale);
	    ratacorrente.setText(damesecorrenteModificato);

	    String dameseprecedenteAttuale = rataprecedente.getText().toString();
	    String dameseprecedenteModificato = aumentaRata(dameseprecedenteAttuale);
	    rataprecedente.setText(dameseprecedenteModificato);

	    String damesesucessivoAttuale = ratasucessivo.getText().toString();
	    String damesesucessivoModificato = aumentaRata(damesesucessivoAttuale);
	    ratasucessivo.setText(damesesucessivoModificato);

	}
    }

    public void rataDown(Dialog dialog) {
	TextView ratacorrente = (TextView) dialog.findViewById(R.id.da_mese_corrente);
	TextView rataprecedente = (TextView) dialog.findViewById(R.id.da_mese_precedente);
	TextView ratasucessivo = (TextView) dialog.findViewById(R.id.da_mese_sucessivo);
	if (!rataprecedente.getText().toString().equals("")) {
	    String damesecorrenteAttuale = ratacorrente.getText().toString();
	    String damesecorrenteModificato = diminuisciRata(damesecorrenteAttuale);
	    ratacorrente.setText(damesecorrenteModificato);

	    String dameseprecedenteAttuale = rataprecedente.getText().toString();
	    String dameseprecedenteModificato = diminuisciRata(dameseprecedenteAttuale);
	    rataprecedente.setText(dameseprecedenteModificato);

	    String damesesucessivoAttuale = ratasucessivo.getText().toString();
	    String damesesucessivoModificato = diminuisciRata(damesesucessivoAttuale);
	    ratasucessivo.setText(damesesucessivoModificato);
	}
    }

    public void periodicitaUp(Dialog dialog) {
	TextView periodicitacorrente = (TextView) dialog.findViewById(R.id.a_mese_corrente);
	TextView periodicitaprecedente = (TextView) dialog.findViewById(R.id.a_mese_precedente);
	TextView periodicitasucessiva = (TextView) dialog.findViewById(R.id.a_mese_sucessivo);

	if (!periodicitasucessiva.getText().toString().equals("")) {

	    String periodicitacorrenteText = periodicitacorrente.getText().toString();
	    int periodicitacorrenteTextInt = 0;
	    for (int i = 0; i < arrayPeriodicita.length; i++) {
		if (periodicitacorrenteText.equals(arrayPeriodicita[i])) {
		    periodicitacorrenteTextInt = i;
		}
	    }
	    if ((arrayPeriodicita.length - 1) - periodicitacorrenteTextInt == 1) {
		periodicitaprecedente.setText(arrayPeriodicita[periodicitacorrenteTextInt]);
		periodicitacorrente.setText(arrayPeriodicita[periodicitacorrenteTextInt + 1]);
		periodicitasucessiva.setText("");
	    }
	    else {
		periodicitaprecedente.setText(arrayPeriodicita[periodicitacorrenteTextInt]);
		periodicitacorrente.setText(arrayPeriodicita[periodicitacorrenteTextInt + 1]);
		periodicitasucessiva.setText(arrayPeriodicita[periodicitacorrenteTextInt + 2]);
	    }
	}
    }

    public void periodicitaDown(Dialog dialog) {
	TextView periodicitacorrente = (TextView) dialog.findViewById(R.id.a_mese_corrente);
	TextView periodicitaprecedente = (TextView) dialog.findViewById(R.id.a_mese_precedente);
	TextView periodicitasucessiva = (TextView) dialog.findViewById(R.id.a_mese_sucessivo);

	if (!periodicitaprecedente.getText().toString().equals("")) {

	    String periodicitacorrenteText = periodicitacorrente.getText().toString();
	    int periodicitacorrenteTextInt = 0;
	    for (int i = 0; i < arrayPeriodicita.length; i++) {
		if (periodicitacorrenteText.equals(arrayPeriodicita[i])) {
		    periodicitacorrenteTextInt = i;
		}
	    }
	    if (periodicitacorrenteTextInt == 1) {
		periodicitaprecedente.setText("");
		periodicitacorrente.setText(arrayPeriodicita[0]);
		periodicitasucessiva.setText(arrayPeriodicita[periodicitacorrenteTextInt]);
	    }
	    else {
		periodicitaprecedente.setText(arrayPeriodicita[periodicitacorrenteTextInt - 2]);
		periodicitacorrente.setText(arrayPeriodicita[periodicitacorrenteTextInt - 1]);
		periodicitasucessiva.setText(arrayPeriodicita[periodicitacorrenteTextInt]);
	    }
	}
    }

    public String diminuisciRata(String mese) {
	String result = "";
	if (!mese.equals("")) {
	    int meseInt = Integer.valueOf(mese);
	    if ((meseInt - 1) >= 2) {
		result = "" + (meseInt - 1);
	    }
	    else {
		result = "";
	    }
	}
	else {
	    result = rate[rate.length - 1];
	}

	return result;
    }

    public String aumentaRata(String mese) {
	String result = "";

	if (!mese.equals("")) {
	    int meseInt = Integer.valueOf(mese);

	    if ((meseInt + 1) <= Integer.valueOf(rate[rate.length - 1])) {
		result = "" + (meseInt + 1);
	    }
	    else {
		result = "";
	    }
	}
	else {
	    result = rate[0];
	}
	return result;
    }

    private String convertiData(String data) {
	String result;
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH);
	Date dataEmis = new Date();
	try {
	    dataEmis = sdf.parse(data);
	}
	catch (ParseException e) {
	    // e.printStackTrace();
	}
	SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
	result = sdf1.format(dataEmis);
	return result;
    }

    private boolean isNetworkAvailable(Context context) {
	ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
	if (connectivityManager == null) return false;
	NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
	return activeNetworkInfo != null;
    }

    public void onBackPressed() {

	animation = Utilities.animation(this);
	imageAnim = (ImageView) findViewById(R.id.waiting_anim);
	imageAnim.setVisibility(View.VISIBLE);
	if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
	    imageAnim.setBackgroundDrawable(animation);
	}
	else {
	    imageAnim.setBackground(animation);
	}
	animation.start();

	if ((getIntent().getStringExtra("PAGINA_PROVENIENZA") != null) && (getIntent().getStringExtra("PAGINA_PROVENIENZA").equals(BollettePagamentiSceltaPiano.class.getCanonicalName()))) {
	    Intent intentSceltaPiano = new Intent(getApplicationContext(), BollettePagamentiSceltaPiano.class);
	    intentSceltaPiano.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
	    intentSceltaPiano.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
	    intentSceltaPiano.putExtra("tipoFornitura", getIntent().getStringExtra("tipoFornitura"));
	    intentSceltaPiano.putExtra("identificativoFattura", getIntent().getStringExtra("identificativoFattura"));
	    intentSceltaPiano.putExtra("codiceCausaleRateizzabilitaAlternativa", getIntent().getStringExtra("codiceCausaleRateizzabilitaAlternativa"));

	    intentSceltaPiano.putExtra("codiceFattura", getIntent().getStringExtra("codiceFattura"));
	    intentSceltaPiano.putExtra("dataEmissione", getIntent().getStringExtra("dataEmissione"));
	    intentSceltaPiano.putExtra("dataScadenza", getIntent().getStringExtra("dataScadenza"));
	    intentSceltaPiano.putExtra("importoRateizzabile", getIntent().getStringExtra("importoRateizzabile"));
	    intentSceltaPiano.putExtra("codiceCausaleRateizzabilita", getIntent().getStringExtra("codiceCausaleRateizzabilita"));
	    
	    intentSceltaPiano.putExtra("domiciliazioneApprovata", getIntent().getBooleanExtra("domiciliazioneApprovata", false));
	    intentSceltaPiano.putExtra("infoDomiciliazioneApprovata",  getIntent().getBooleanExtra("infoDomiciliazioneApprovata", false));
	    
	    // intentSceltaPiano.putExtra("chiamata", chiamata);
	    BollettePagamentiDettaglioPiano.this.finish();
	    startActivity(intentSceltaPiano);
	    overridePendingTransition(R.anim.fade, R.anim.hold);
	}
	else {
	    Intent intentHome = new Intent(getApplicationContext(), BollettePagamentiRateizzate.class);
	    intentHome.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
	    intentHome.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
	    intentHome.putExtra("domiciliazioneApprovata", getIntent().getBooleanExtra("domiciliazioneApprovata", false));
	    intentHome.putExtra("infoDomiciliazioneApprovata",  getIntent().getBooleanExtra("infoDomiciliazioneApprovata", false));
	    
	    BollettePagamentiDettaglioPiano.this.finish();
	    startActivity(intentHome);
	    overridePendingTransition(R.anim.fade, R.anim.hold);
	}
    }
}
