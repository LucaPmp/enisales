package com.eni.enigaseluce.httpcall;

import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.app.Dialog;

import com.eni.enigaseluce.autolettura.AutoLetturaInsert;
import com.eni.enigaseluce.autolettura.Autolettura;
import com.eni.enigaseluce.bollettePagamenti.BollettePagamenti;
import com.eni.enigaseluce.bollettePagamenti.BollettePagamentiDettaglioPiano;
import com.eni.enigaseluce.bollettePagamenti.BollettePagamentiProssimaBolletta;
import com.eni.enigaseluce.bollettePagamenti.BollettePagamentiRateizzate;
import com.eni.enigaseluce.bollettePagamenti.BollettePagamentiRicerca;
import com.eni.enigaseluce.bollettePagamenti.BollettePagamentiRicercaIntervallo;
import com.eni.enigaseluce.bollettePagamenti.BollettePagamentiRicercaPostIntervallo;
import com.eni.enigaseluce.consumigas.ConsumiGas;
import com.eni.enigaseluce.consumigas.ConsumiGasGraficoConsumi;
import com.eni.enigaseluce.consumiluce.ConsumiLuce;
import com.eni.enigaseluce.error.OopsPageLogin;
import com.eni.enigaseluce.error.OopsPageMain;
import com.eni.enigaseluce.home.Home;
import com.eni.enigaseluce.login.Login;
import com.eni.enigaseluce.statoavanzamentofornitura.StatoAvanzamentoFornitura;
import com.eni.enigaseluce.storicoletture.StoricoLettureDetail;
import com.eni.enigaseluce.webolletta.BollettaDigitaleAttivazioneNotifica;
import com.eni.enigaseluce.webolletta.WebollettaActivate;
import com.eni.enigaseluce.webolletta.WebollettaAttivazioneNotifica;
import com.eni.enigaseluce.webolletta.WebollettaAttivazionePostPreconsenso;
import com.eni.enigaseluce.webolletta.WebollettaDetail;
import com.eni.enigaseluce.webolletta.WebollettaDisattivazionePostPreconsenso;

import org.json.JSONObject;

public interface ServiceCall {

    public String executeHttpGet(String url) throws Exception;

    public String executeHttpsPostAuth(String urlString, String username, String password, final Login context) throws Exception;

    public String executeHttpsPostNoAuth(String urlString, String username, String password, Map<String,String> header, JSONObject body, final Login context) throws Exception;

    public String executeHttpsPostAuth(String urlString, String username, String password, final OopsPageLogin context) throws Exception;

    public void executeHttpsGet(String urlString, final HashMap<String, String> parametri, final Home context, final int posizioneIcona, final Activity oops) throws Exception;

    public void executeHttpsGetDummyRes(String urlString, final Login context, String token) throws Exception;

    public void executeHttpsGetMaintenance(String urlString, Activity context) throws Exception;

    public void executeHttpsGet(String urlString, final String parametri, final Activity context, final int posizioneIcona, final Activity oops) throws Exception;

    public void executeHttpsGetAvanzamentoForniture(String urlString, final HashMap<String, String> parametri, final StatoAvanzamentoFornitura context, final Activity oops) throws Exception;

    public void executeHttpsGetAutolettura(String urlString, final HashMap<String, String> parametri, final Autolettura context, final String pdf, final String pdr, final String sistemaBilling, final String societaFornitrice, final String numeroCifreMisuratore, final Activity oops) throws Exception;

    public void executeHttpsGetAutoletturaFromStoricoDetail(String urlString, final HashMap<String, String> parametri, final StoricoLettureDetail context, final String pdf, final String pdr, final String sistemaBilling, final String societaFornitrice, final String numeroCifreMisuratore, final Activity oops) throws Exception;

    public void executeHttpsGetAutoletturaInsert(String urlString, final HashMap<String, String> parametri, final AutoLetturaInsert context, final Activity oops) throws Exception;

    public void executeHttpsGetAutoletturaInsert(String urlString, final HashMap<String, String> parametri, final AutoLetturaInsert context, final String annullaOrConferma, final Activity oops) throws Exception;

    public void executeHttpsGetStoricoLetture(String urlString, HashMap<String, String> parametri, final AutoLetturaInsert context, final String pdf, final String pdr, final Activity oops) throws Exception;

    public void executeHttpsGetDomiciliazione(String urlString, String parametri, HashMap<String, String> parametriHash, final Activity context, final int code, final Activity oops) throws Exception;

    public void executeHttpsGetBollettePagamenti(String urlString, final HashMap<String, String> parametri, final BollettePagamenti context, final Activity oops, final boolean prossimaFatttura, final boolean bolletteRateizzabili) throws Exception;

    public void executeHttpsGetProssimaBolletta(String urlString, final HashMap<String, String> parametri, final AutoLetturaInsert context, final Activity oops, final boolean prossimaFatttura, final boolean bolletteRateizzabili) throws Exception;

    public void executeHttpsGetBollettePagamentiRicerca(String urlString, final HashMap<String, String> parametri, final BollettePagamentiRicerca context, final Activity oops, final String numeroFattura) throws Exception;

    public void executeHttpsGetBollettePagamentiRicercaPostIntervallo(String urlString, final HashMap<String, String> parametri, final BollettePagamentiRicercaPostIntervallo context, final Activity oops, final boolean isDownload, final String numeroFattura) throws Exception;

    public void executeHttpsGetBollettePagamentiRicercaIntervallo(String urlString, final HashMap<String, String> parametri, final BollettePagamentiRicercaIntervallo context, final Activity oops) throws Exception;

    public void executeHttpsGetProssimaBolletta(String urlString, final String parametri, final BollettePagamentiProssimaBolletta context, final Activity oops) throws Exception;

    public void executeHttpsGetBollettePagamentiCalcolaPropostaRate(String urlString, final HashMap<String, String> parametri, final HashMap<String, String> parametriSecondario, final BollettePagamentiRateizzate context, final Activity oops, final int tipoProposta) throws Exception;

    public void executeHttpsGetBollettePagamentiCalcolaPropostaRateDettaglio(String urlString, final HashMap<String, String> parametri, final BollettePagamentiDettaglioPiano context, final Activity oops, final int tipoProposta, final boolean reset) throws Exception;

    public void executeHttpsGetBollettePagamentiAttivaPropostaPiano(String urlString, final HashMap<String, String> parametri, final BollettePagamentiDettaglioPiano context, final Activity oops, final String accettaOModifica) throws Exception;

    public void executeHttpsGetWebollettaActivateDom(String urlString, final String parametri, final WebollettaActivate context, final Dialog dialog, final Activity oops) throws Exception;

    public void executeHttpsGetWebollettaActivatePreconsenso(String urlString, final String parametri, final WebollettaActivate context, final Activity oops) throws Exception;

    public void executeHttpsGetWebollettaAttivazioneNotifica(String urlString, final String parametri, final WebollettaAttivazioneNotifica context, final Activity oops, final boolean notifica, final boolean aggiorna) throws Exception;

    public void executeHttpsGetWebollettaAttivazioneNotifica(String urlString, final String parametri, final BollettaDigitaleAttivazioneNotifica context, final Activity oops, final boolean notifica, final boolean aggiorna) throws Exception;

    public void executeHttpsGetWebollettaActivatePostPreconsenso(String urlString, final HashMap<String, String> parametri, final WebollettaAttivazionePostPreconsenso context, final Activity oops) throws Exception;

    public void executeHttpsGetWebollettaDisattivazionePostPreconsenso(String urlString, final HashMap<String, String> parametri, final WebollettaDisattivazionePostPreconsenso context, final Activity oops) throws Exception;

    public void executeHttpsGetBollettaDigitaleDisattivazionePostPreconsenso(String urlString, final HashMap<String, String> parametri, final BollettaDigitaleAttivazioneNotifica context, final Activity oops) throws Exception;

    public void executeHttpsGetWebollettaDetail(String urlString, final HashMap<String, String> parametri, final WebollettaDetail context, final Activity oops) throws Exception;

    public void executeHttpsGetConsumiLuce(String urlString, final String parametri, final ConsumiLuce context, final Activity oops) throws Exception;

    public void executeHttpsGetConsumiGas(String urlString, final String parametri, final ConsumiGas context, final Activity oops) throws Exception;

    public void executeHttpsGetConsumiGasGrafico(String urlString, final String parametri, final ConsumiGasGraficoConsumi context, final Activity oops) throws Exception;

}
