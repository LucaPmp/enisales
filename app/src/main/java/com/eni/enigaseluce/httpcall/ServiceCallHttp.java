package com.eni.enigaseluce.httpcall;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URI;
import java.net.URLEncoder;
import java.security.KeyStore;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.cookie.Cookie;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Message;

import com.eni.enigaseluce.Main;
import com.eni.enigaseluce.autolettura.AutoLetturaInsert;
import com.eni.enigaseluce.autolettura.Autolettura;
import com.eni.enigaseluce.bollettePagamenti.BollettePagamenti;
import com.eni.enigaseluce.bollettePagamenti.BollettePagamentiDettaglioPiano;
import com.eni.enigaseluce.bollettePagamenti.BollettePagamentiProssimaBolletta;
import com.eni.enigaseluce.bollettePagamenti.BollettePagamentiRateizzate;
import com.eni.enigaseluce.bollettePagamenti.BollettePagamentiRicerca;
import com.eni.enigaseluce.bollettePagamenti.BollettePagamentiRicercaIntervallo;
import com.eni.enigaseluce.bollettePagamenti.BollettePagamentiRicercaPostIntervallo;
import com.eni.enigaseluce.consumigas.ConsumiGas;
import com.eni.enigaseluce.consumigas.ConsumiGasGraficoConsumi;
import com.eni.enigaseluce.consumiluce.ConsumiLuce;
import com.eni.enigaseluce.domiciliazione.DomiciliazioneInserimentoNonResidenziali;
import com.eni.enigaseluce.domiciliazione.DomiciliazioneInserimentoPersonaFisica;
import com.eni.enigaseluce.domiciliazione.DomiciliazioneInserimentoPersonaGiuridica;
import com.eni.enigaseluce.error.OopsPageLogin;
import com.eni.enigaseluce.error.OopsPageMain;
import com.eni.enigaseluce.home.Home;
import com.eni.enigaseluce.login.Login;
import com.eni.enigaseluce.statoavanzamentofornitura.StatoAvanzamentoFornitura;
import com.eni.enigaseluce.storicoletture.StoricoLettureDetail;
import com.eni.enigaseluce.utils.Log;
import com.eni.enigaseluce.webolletta.BollettaDigitaleAttivazioneNotifica;
import com.eni.enigaseluce.webolletta.WebollettaActivate;
import com.eni.enigaseluce.webolletta.WebollettaAttivazioneNotifica;
import com.eni.enigaseluce.webolletta.WebollettaAttivazionePostPreconsenso;
import com.eni.enigaseluce.webolletta.WebollettaDetail;
import com.eni.enigaseluce.webolletta.WebollettaDisattivazionePostPreconsenso;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.PersistentCookieStore;
import com.loopj.android.http.RequestParams;

/**
 * @author erinda.lleshi
 * 
 */
@SuppressLint("NewApi")
public class ServiceCallHttp implements ServiceCall {

	public static final String SERVICE_CALL_TAG = "SERVICE_CALL_TAG";
	private static final String TAG = "ServiceCallHttp";
	private HttpClient client;
	private HttpGet method;
	static boolean exception = false;
	public PersistentCookieStore myCookieStore;
	private boolean timeout = false;

	protected ServiceCallHttp(Context context) {

		myCookieStore = new PersistentCookieStore(context);
	}

	/**
	 * Metodo per chiamata http get
	 * 
	 * @param url
	 *            la url da chiamare
	 * @return restituisce la risposta della chiamate in formato String
	 * @throws Exception
	 */
	public String executeHttpGet(String url) throws Exception
	{
		BufferedReader in = null;
		String responseString = "";
		try
		{
			client = new DefaultHttpClient();
			method = new HttpGet(url);
			HttpGet request = new HttpGet();
			request.setURI(new URI(url));

			HttpResponse response = client.execute(method);
			in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuffer sb = new StringBuffer("");
			String line = "";
			String NL = System.getProperty("line.separator");
			while ((line = in.readLine()) != null)
			{
				sb.append(line + NL);
			}
			in.close();
			responseString = sb.toString();
		} catch (ProtocolException e)
		{
			throw new Exception(e.getMessage());
		} catch (MalformedURLException e1)
		{
			throw new Exception(e1.getMessage());
		} catch (IOException e2)
		{
			throw new Exception(e2.getMessage());
		} finally
		{
			if (in != null)
			{
				try
				{
					in.close();
				} catch (IOException e)
				{
					throw new Exception(e.getMessage());
				}
			}
		}
		return responseString;
	}


	@Override
	public String executeHttpsPostNoAuth(String urlString, String username, String password, Map<String,String> header, JSONObject body, final Login context) throws Exception {
		String responseString = "";
		final Timer timer = new Timer();
		timeout = false;
		trustAllHosts();
		final AsyncHttpClient myClient = new AsyncHttpClient();

		for (Map.Entry<String, String> entry : header.entrySet())
		{
			myClient.addHeader(entry.getKey(),entry.getValue());
		}



		myClient.setTimeout(20000);
//		myClient.setCookieStore(myCookieStore);
//		if (myCookieStore.getCookies().size() != 0)
//		{
//			myCookieStore.clear();
//		}
//		BasicClientCookie newCookie = new BasicClientCookie("AMTESTCOOKIE", "amtestcookie");
//		newCookie.setVersion(1);
//
//		if (Constants.TEST_ENV)
//		{
//			// test
//			newCookie.setDomain("st-password.eni.it");
//		} else
//		{
//			// prod
//			newCookie.setDomain("password.eni.it");
//		}
//		newCookie.setPath("/opensso/UI/");
//		myCookieStore.addCookie(newCookie);

		RequestParams params = new RequestParams();

//		JSONObject jsonParams = new JSONObject();
//		jsonParams.put("deviceid", "asdasdasdasdas");
//		jsonParams.put("idapp", "EniAppl004");
//		jsonParams.put("operation", "authenticate");
//		jsonParams.put("password", "customer");
//		jsonParams.put("secondarydeviceid", "optional");
//		jsonParams.put("signature", "4030D3096DC5303C5D4159447CCC4F85test911@test.itcustomerauthenticateoptionaloptional2016-05-11T13:43:37.801ZEniAppl004");
//		jsonParams.put("timestamp", "2016-05-11T13:43:37.801Z");
//		jsonParams.put("tokenid", "");
//		jsonParams.put("user", "test911@test.it");
		StringEntity entity = new StringEntity(body.toString());



//		client.post(context, restApiUrl, entity, "application/json",
//				responseHandler);

//		params.put("IDToken1", username);
//		params.put("IDToken2", password);

//		myClient.setCookieStore(myCookieStore);
		for (int c = 0; c < myCookieStore.getCookies().size(); c++)
		{
		}
		try
		{
			KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
			trustStore.load(null, null);
			MySSLSocketFactory sf = new MySSLSocketFactory(trustStore);
			sf.setHostnameVerifier(MySSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
			myClient.setSSLSocketFactory(sf);
		} catch (Exception e)
		{
		}

//		String urlIntera = urlString + URLEncoder.encode(Constants.URL_LOGIN);
		String urlIntera = urlString;

		Log.d(SERVICE_CALL_TAG, "Login URL - executeHttpsPostNoAuth(String, String, String, Login) - " + urlIntera);
		Log.d(SERVICE_CALL_TAG, "Login Input Params - executeHttpsPostNoAuth(String, String, String, Login) - " + "IDToken1[" + username + "] - IDToken2["
				+ password + "]");



		myClient.post(context, urlIntera, entity, "application/json", new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(String response) {

				if (!context.isFinishing() && !timeout && (context.hasWindowFocus())) {


					String token="";
					String errorCode="";

					Log.d(SERVICE_CALL_TAG, "Login Response - executeHttpsPostNoAuth(String, String, String, Login) - " + response);
					try {
						JSONObject responseJSON = new JSONObject(response);
						token  =(String)((JSONObject)responseJSON.get("response")).get("TOKEN");


						errorCode =(String)((JSONObject)responseJSON.get("response")).get("ERRCODE");



						BasicClientCookie newCookie = new BasicClientCookie("SMSESSION", "SMSESSION");
						newCookie.setVersion(0);
						newCookie.setDomain(".enigaseluce.com");
						newCookie.setPath("/");
						newCookie.setValue(token);
						myCookieStore.addCookie(newCookie);


					} catch (JSONException e) {
						e.printStackTrace();
					}
					List<Cookie> listaCookies = myCookieStore.getCookies();

					context.postAuthSiteminder(true,errorCode);
				}
				timeout = false;
			}

			@Override
			public void onFinish() {
				timer.cancel();
				super.onFinish();
			}

			@Override
			public void onStart() {
			}

			@Override
			protected void handleFailureMessage(Throwable arg0, String arg1) {
				// super.handleFailureMessage(arg0, arg1);
				if (!context.isFinishing() && !timeout && (context.hasWindowFocus())) {

					context.postAuthSiteminder(false,"");
				}
				timeout = false;
			}

			@Override
			protected void handleMessage(Message arg0) {
				super.handleMessage(arg0);
			}

			@Override
			protected void handleSuccessMessage(String arg0) {
				super.handleSuccessMessage(arg0);
			}

			@Override
			protected Message obtainMessage(int arg0, Object arg1) {
				int i = 0;
				return super.obtainMessage(arg0, arg1);
			}

			@Override
			public void onFailure(Throwable arg0, String arg1) {
				myCookieStore.clear();
				// super.onFailure(arg0, arg1);
			}

			@Override
			protected void sendFailureMessage(Throwable arg0, byte[] arg1) {
				super.sendFailureMessage(arg0, arg1);
			}

			@Override
			protected void sendFailureMessage(Throwable arg0, String arg1) {
				super.sendFailureMessage(arg0, arg1);
			}

			@Override
			protected void sendFinishMessage() {
				super.sendFinishMessage();
			}

			@Override
			protected void sendMessage(Message arg0) {
				int i = 0;
				super.sendMessage(arg0);
			}

			@Override
			protected void sendStartMessage() {
				super.sendStartMessage();
			}

			@Override
			protected void sendSuccessMessage(String arg0) {
				super.sendSuccessMessage(arg0);
			}
		});


//		myClient.post(urlIntera, params, new AsyncHttpResponseHandler() {
//			@Override
//			public void onSuccess(String response)
//			{
//
//				if (!context.isFinishing() && !timeout && (context.hasWindowFocus()))
//				{
//					List<Cookie> listaCookies = myCookieStore.getCookies();
//
//					Log.d(SERVICE_CALL_TAG, "Login Response - executeHttpsPostAuth(String, String, String, Login) - " + response);
//
//					context.postAuth(response, listaCookies);
//				}
//				timeout = false;
//			}
//
//			@Override
//			public void onFinish()
//			{
//				timer.cancel();
//				super.onFinish();
//			}
//
//			@Override
//			public void onStart()
//			{
//			}
//
//			@Override
//			protected void handleFailureMessage(Throwable arg0, String arg1)
//			{
//				// super.handleFailureMessage(arg0, arg1);
//				if (!context.isFinishing() && !timeout && (context.hasWindowFocus()))
//				{
//					context.postLoginException(true);
//				}
//				timeout = false;
//			}
//
//			@Override
//			protected void handleMessage(Message arg0)
//			{
//				super.handleMessage(arg0);
//			}
//
//			@Override
//			protected void handleSuccessMessage(String arg0)
//			{
//				super.handleSuccessMessage(arg0);
//			}
//
//			@Override
//			protected Message obtainMessage(int arg0, Object arg1)
//			{
//				int i=0;
//				return super.obtainMessage(arg0, arg1);
//			}
//
//			@Override
//			public void onFailure(Throwable arg0, String arg1)
//			{
//				myCookieStore.clear();
//				// super.onFailure(arg0, arg1);
//			}
//
//			@Override
//			protected void sendFailureMessage(Throwable arg0, byte[] arg1)
//			{
//				super.sendFailureMessage(arg0, arg1);
//			}
//
//			@Override
//			protected void sendFailureMessage(Throwable arg0, String arg1)
//			{
//				super.sendFailureMessage(arg0, arg1);
//			}
//
//			@Override
//			protected void sendFinishMessage()
//			{
//				super.sendFinishMessage();
//			}
//
//			@Override
//			protected void sendMessage(Message arg0)
//			{
//				int i = 0;
//				super.sendMessage(arg0);
//			}
//
//			@Override
//			protected void sendStartMessage()
//			{
//				super.sendStartMessage();
//			}
//
//			@Override
//			protected void sendSuccessMessage(String arg0)
//			{
//				super.sendSuccessMessage(arg0);
//			}
//		});

		timer.schedule(new TimerTask() {
			public void run()
			{
				myClient.cancelRequests(context, true);
				timeout = true;
				if (!context.isFinishing() && (context.hasWindowFocus()))
				{
					context.runOnUiThread(new Runnable() {
						public void run()
						{
							context.postLoginException(true);

						}
					});

				}
			}
		}, 60000);
		return responseString;
	}

	/**
	 * Metodo da chiamare per l'autenticazione
	 * 
	 * @param urlString
	 * @param username
	 * @param password
	 * @param context
	 * @return
	 * @throws Exception
	 */
	public String executeHttpsPostAuth(String urlString, String username, String password, final Login context) throws Exception
	{
		String responseString = "";
		final Timer timer = new Timer();
		timeout = false;
		trustAllHosts();
		final AsyncHttpClient myClient = new AsyncHttpClient();
		myClient.setTimeout(20000);

//		myClient.setCookieStore(myCookieStore);

		PersistentCookieStore localCookieStore = new PersistentCookieStore(context);
//		myClient.setCookieStore(myCookieStore);

		for (Cookie cookie : myCookieStore.getCookies()) {
			if("SMSESSION".equals(cookie.getName()))
			{
				localCookieStore.addCookie(cookie);
			}
		}
		myClient.setCookieStore(localCookieStore);


		HashMap<String,String> headers= new HashMap<String, String>();
		headers.put("content-type", "application/x-www-form-urlencoded");
		headers.put("accept", "application/json");
//		headers.put("signresponse", "false");

		for (Map.Entry<String, String> entry : headers.entrySet())
		{
			myClient.addHeader(entry.getKey(),entry.getValue());
		}

//		if (myCookieStore.getCookies().size() != 0)
//		{
//			myCookieStore.clear();
//		}
//		BasicClientCookie newCookie = new BasicClientCookie("AMTESTCOOKIE", "amtestcookie");
//		newCookie.setVersion(1);
//
//		if (Constants.TEST_ENV)
//		{
//			// test
//			newCookie.setDomain("st-password.eni.it");
//		} else
//		{
//			// prod
//			newCookie.setDomain("password.eni.it");
//		}
//		newCookie.setPath("/opensso/UI/");
//		myCookieStore.addCookie(newCookie);

		RequestParams params = new RequestParams();
//		params.put("IDToken1", username);
		params.put("SMUSERNAME", username);

//		params.put("IDToken2", password);
//		myClient.setCookieStore(myCookieStore);
//		for (int c = 0; c < myCookieStore.getCookies().size(); c++)
//		{
//
//		}

		try
		{
			KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
			trustStore.load(null, null);
			MySSLSocketFactory sf = new MySSLSocketFactory(trustStore);
			sf.setHostnameVerifier(MySSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
			myClient.setSSLSocketFactory(sf);
		} catch (Exception e) {
		}

//		String urlIntera = urlString + URLEncoder.encode(Constants.URL_LOGIN);
//		String urlIntera = Constants.URL_LOGIN;

//		Log.d(SERVICE_CALL_TAG, "Login URL - executeHttpsPostAuth(String, String, String, Login) - " + urlIntera);
		Log.d(SERVICE_CALL_TAG, "Login Input Params - executeHttpsPostAuth(String, String, String, Login) - " + "IDToken1[" + username + "] - IDToken2["
				+ password + "]");

		myClient.post(urlString, new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(String response)
			{

				if (!context.isFinishing() && !timeout && (context.hasWindowFocus()))
				{
					List<Cookie> listaCookies = myCookieStore.getCookies();

					Log.d(SERVICE_CALL_TAG, "Login Response - executeHttpsPostAuth(String, String, String, Login) - " + response);

					context.postAuth(response, listaCookies);
				}
				timeout = false;
			}

			@Override
			public void onFinish()
			{
				timer.cancel();
				super.onFinish();
			}

			@Override
			public void onStart()
			{
			}

			@Override
			protected void handleFailureMessage(Throwable arg0, String arg1)
			{
				// super.handleFailureMessage(arg0, arg1);
				if (!context.isFinishing() && !timeout && (context.hasWindowFocus()))
				{
					context.postLoginException(true);
				}
				timeout = false;
			}

			@Override
			protected void handleMessage(Message arg0)
			{
				super.handleMessage(arg0);
			}

			@Override
			protected void handleSuccessMessage(String arg0)
			{
				super.handleSuccessMessage(arg0);
			}

			@Override
			protected Message obtainMessage(int arg0, Object arg1)
			{
				return super.obtainMessage(arg0, arg1);
			}

			@Override
			public void onFailure(Throwable arg0, String arg1)
			{
				myCookieStore.clear();
				// super.onFailure(arg0, arg1);
			}

			@Override
			protected void sendFailureMessage(Throwable arg0, byte[] arg1)
			{
				super.sendFailureMessage(arg0, arg1);
			}

			@Override
			protected void sendFailureMessage(Throwable arg0, String arg1)
			{
				super.sendFailureMessage(arg0, arg1);
			}

			@Override
			protected void sendFinishMessage()
			{
				super.sendFinishMessage();
			}

			@Override
			protected void sendMessage(Message arg0)
			{
				super.sendMessage(arg0);
			}

			@Override
			protected void sendStartMessage()
			{
				super.sendStartMessage();
			}

			@Override
			protected void sendSuccessMessage(String arg0)
			{
				super.sendSuccessMessage(arg0);
			}
		});

		timer.schedule(new TimerTask() {
			public void run()
			{
				myClient.cancelRequests(context, true);
				timeout = true;
				if (!context.isFinishing() && (context.hasWindowFocus()))
				{
					context.runOnUiThread(new Runnable() {
						public void run()
						{
							context.postLoginException(true);

						}
					});

				}
			}
		}, 60000);
		return responseString;
	}



	public String executeHttpsPostAuth(String urlString, String username, String password, final OopsPageLogin context) throws Exception
	{
		String responseString = "";
		final Timer timer = new Timer();
		timeout = false;
		trustAllHosts();
		final AsyncHttpClient myClient = new AsyncHttpClient();
		myClient.setTimeout(20000);

		myClient.setCookieStore(myCookieStore);

//		if (myCookieStore.getCookies().size() != 0)
//		{
//			myCookieStore.clear();
//		}
//		BasicClientCookie newCookie = new BasicClientCookie("AMTESTCOOKIE", "amtestcookie");
//		newCookie.setVersion(1);
//
//		if (Constants.TEST_ENV)
//		{
//			// test
//			newCookie.setDomain("st-password.eni.it");
//		} else
//		{
//			// prod
//			newCookie.setDomain("password.eni.it");
//		}
//		newCookie.setPath("/opensso/UI/");
//		myCookieStore.addCookie(newCookie);

		RequestParams params = new RequestParams();
//		params.put("IDToken1", username);
		params.put("SMUSERNAME", username);

//		params.put("IDToken2", password);
		myClient.setCookieStore(myCookieStore);
		for (int c = 0; c < myCookieStore.getCookies().size(); c++)
		{

		}

		try
		{
			KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
			trustStore.load(null, null);
			MySSLSocketFactory sf = new MySSLSocketFactory(trustStore);
			sf.setHostnameVerifier(MySSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
			myClient.setSSLSocketFactory(sf);
		} catch (Exception e)
		{
		}

//		String urlIntera = urlString + URLEncoder.encode(Constants.URL_LOGIN);
//		String urlIntera = Constants.URL_LOGIN;

//		Log.d(SERVICE_CALL_TAG, "Login URL - executeHttpsPostAuth(String, String, String, Login) - " + urlIntera);
		Log.d(SERVICE_CALL_TAG, "Login Input Params - executeHttpsPostAuth(String, String, String, Login) - " + "IDToken1[" + username + "] - IDToken2["
				+ password + "]");

		myClient.post(urlString, params, new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(String response)
			{

				if (!context.isFinishing() && !timeout && (context.hasWindowFocus()))
				{
					List<Cookie> listaCookies = myCookieStore.getCookies();

					Log.d(SERVICE_CALL_TAG, "Login Response - executeHttpsPostAuth(String, String, String, Login) - " + response);

					context.postAuth(response, listaCookies);
				}
				timeout = false;
			}

			@Override
			public void onFinish()
			{
				timer.cancel();
				super.onFinish();
			}

			@Override
			public void onStart()
			{
			}

			@Override
			protected void handleFailureMessage(Throwable arg0, String arg1)
			{
				// super.handleFailureMessage(arg0, arg1);
				if (!context.isFinishing() && !timeout && (context.hasWindowFocus()))
				{
					context.postLoginException(true);
				}
				timeout = false;
			}

			@Override
			protected void handleMessage(Message arg0)
			{
				super.handleMessage(arg0);
			}

			@Override
			protected void handleSuccessMessage(String arg0)
			{
				super.handleSuccessMessage(arg0);
			}

			@Override
			protected Message obtainMessage(int arg0, Object arg1)
			{
				return super.obtainMessage(arg0, arg1);
			}

			@Override
			public void onFailure(Throwable arg0, String arg1)
			{
				myCookieStore.clear();
				// super.onFailure(arg0, arg1);
			}

			@Override
			protected void sendFailureMessage(Throwable arg0, byte[] arg1)
			{
				super.sendFailureMessage(arg0, arg1);
			}

			@Override
			protected void sendFailureMessage(Throwable arg0, String arg1)
			{
				super.sendFailureMessage(arg0, arg1);
			}

			@Override
			protected void sendFinishMessage()
			{
				super.sendFinishMessage();
			}

			@Override
			protected void sendMessage(Message arg0)
			{
				super.sendMessage(arg0);
			}

			@Override
			protected void sendStartMessage()
			{
				super.sendStartMessage();
			}

			@Override
			protected void sendSuccessMessage(String arg0)
			{
				super.sendSuccessMessage(arg0);
			}
		});

		timer.schedule(new TimerTask() {
			public void run()
			{
				myClient.cancelRequests(context, true);
				timeout = true;
				if (!context.isFinishing() && (context.hasWindowFocus()))
				{
					context.runOnUiThread(new Runnable() {
						public void run()
						{
							context.postLoginException(true);

						}
					});

				}
			}
		}, 60000);
		return responseString;
	}

	/**
	 * Metodo da chiamare per l'autenticazione
	 * 
	 * @param urlString
	 * @param username
	 * @param password
	 * @param context
	 * @return
	 * @throws Exception
	 */
//	public String executeHttpsPostAuth(String urlString, String username, String password, final OopsPageLogin context) throws Exception
//	{
//		String responseString = "";
//		final Timer timer = new Timer();
//		timeout = false;
//		trustAllHosts();
//		final AsyncHttpClient myClient = new AsyncHttpClient();
//		myClient.setTimeout(20000);
//		myClient.setCookieStore(myCookieStore);
//		if (myCookieStore.getCookies().size() != 0)
//		{
//			myCookieStore.clear();
//		}
//		BasicClientCookie newCookie = new BasicClientCookie("AMTESTCOOKIE", "amtestcookie");
//		newCookie.setVersion(1);
//
//		if (Constants.TEST_ENV)
//		{
//			// test
//			newCookie.setDomain("st-password.eni.it");
//		} else
//		{
//			// prod
//			newCookie.setDomain("password.eni.it");
//		}
//		newCookie.setPath("/opensso/UI/");
//		myCookieStore.addCookie(newCookie);
//
//		RequestParams params = new RequestParams();
//		params.put("IDToken1", username);
//		params.put("IDToken2", password);
//		myClient.setCookieStore(myCookieStore);
//		try
//		{
//			KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
//			trustStore.load(null, null);
//			MySSLSocketFactory sf = new MySSLSocketFactory(trustStore);
//			sf.setHostnameVerifier(MySSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
//			myClient.setSSLSocketFactory(sf);
//		} catch (Exception e)
//		{
//		}
//
//		String urlIntera = urlString + URLEncoder.encode(Constants.URL_LOGIN);
//
//		Log.d(SERVICE_CALL_TAG, "Login URL - executeHttpsPostAuth(String, String, String, OopsPageLogin) - " + urlIntera);
//		Log.d(SERVICE_CALL_TAG, "Login Input Params - executeHttpsPostAuth(String, String, String, OopsPageLogin) - " + "IDToken1[" + username
//				+ "] - IDToken2[" + password + "]");
//
//		myClient.post(urlIntera, params, new AsyncHttpResponseHandler() {
//			@Override
//			public void onSuccess(String response) {
//
//				if (!context.isFinishing() && !timeout && (context.hasWindowFocus())) {
//					List<Cookie> listaCookies = myCookieStore.getCookies();
//
//					Log.d(SERVICE_CALL_TAG, "Login Response - executeHttpsPostAuth(String, String, String, OopsPageLogin) - " + response);
//
//					context.postAuth(response, listaCookies);
//				}
//				timeout = false;
//			}
//
//			@Override
//			public void onFinish() {
//				timer.cancel();
//				super.onFinish();
//			}
//
//			@Override
//			public void onStart() {
//			}
//
//			@Override
//			protected void handleFailureMessage(Throwable arg0, String arg1) {
//				// super.handleFailureMessage(arg0, arg1);
//				if (!context.isFinishing() && !timeout && (context.hasWindowFocus())) {
//					context.postLoginException(true);
//				}
//				timeout = false;
//			}
//
//			@Override
//			protected void handleMessage(Message arg0) {
//				super.handleMessage(arg0);
//			}
//
//			@Override
//			protected void handleSuccessMessage(String arg0) {
//				super.handleSuccessMessage(arg0);
//			}
//
//			@Override
//			protected Message obtainMessage(int arg0, Object arg1) {
//				return super.obtainMessage(arg0, arg1);
//			}
//
//			@Override
//			public void onFailure(Throwable arg0, String arg1) {
//				myCookieStore.clear();
//				// super.onFailure(arg0, arg1);
//			}
//
//			@Override
//			protected void sendFailureMessage(Throwable arg0, byte[] arg1) {
//				super.sendFailureMessage(arg0, arg1);
//			}
//
//			@Override
//			protected void sendFailureMessage(Throwable arg0, String arg1) {
//				super.sendFailureMessage(arg0, arg1);
//			}
//
//			@Override
//			protected void sendFinishMessage() {
//				super.sendFinishMessage();
//			}
//
//			@Override
//			protected void sendMessage(Message arg0) {
//				super.sendMessage(arg0);
//			}
//
//			@Override
//			protected void sendStartMessage() {
//				super.sendStartMessage();
//			}
//
//			@Override
//			protected void sendSuccessMessage(String arg0) {
//				super.sendSuccessMessage(arg0);
//			}
//		});
//		timer.schedule(new TimerTask() {
//			@Override
//			public void run()
//			{
//				myClient.cancelRequests(context, true);
//				timeout = true;
//				if (!context.isFinishing() && (context.hasWindowFocus()))
//				{
//					context.runOnUiThread(new Runnable() {
//
//						public void run()
//						{
//							context.postLoginException(true);
//
//						}
//					});
//
//				}
//			}
//		}, 20000);
//		return responseString;
//	}

	/**
	 * Metodo da chiamare per le chiamate dalla schermata di settings (Home)
	 * 
	 * @param urlString
	 * @param parametri
	 * @param context
	 * @throws Exception
	 */
	public void executeHttpsGet(String urlString, final HashMap<String, String> parametri, final Home context, final int posizioneIcona, final Activity oops)
			throws Exception
			{
		exception = false;
		final Timer timer = new Timer();
		timeout = false;
		final AsyncHttpClient myClient = new AsyncHttpClient();
		myClient.setTimeout(20000);
		List<Cookie> listaCookie = myCookieStore.getCookies();

//		for (int i = 0; i < listaCookie.size(); i++)
//		{
//			Cookie cookie = listaCookie.get(i);
//
//			BasicClientCookie newCookie = new BasicClientCookie(cookie.getName(), cookie.getValue());
//			newCookie.setVersion(cookie.getVersion());
//			newCookie.setDomain(".eni.mobi");
//			newCookie.setPath(cookie.getPath());
//
//			myCookieStore.addCookie(newCookie);
//		}

		String paramString = hashMapParameterToString(parametri);

		paramString = adjustQueryString(paramString);

		myClient.setCookieStore(myCookieStore);
		try
		{
			KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
			trustStore.load(null, null);
			MySSLSocketFactory sf = new MySSLSocketFactory(trustStore);
			sf.setHostnameVerifier(MySSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
			myClient.setSSLSocketFactory(sf);
		} catch (Exception e)
		{
			throw new Exception(e.getMessage());
		}

		Log.d(SERVICE_CALL_TAG, "HttpGet URL - executeHttpsGet - " + urlString + paramString);
		Log.d(SERVICE_CALL_TAG, "HttpGet Input Params - executeHttpsGet - parametri[" + parametri + "]");

		myClient.setCookieStore(myCookieStore);

		myClient.get(urlString + paramString, new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(String response)
			{

				if (!context.isFinishing() && !timeout
						&& (context.hasWindowFocus() || Home.getDisableClicksDialog().isShowing() || (oops != null && oops.hasWindowFocus())))
				{
					List<Cookie> listaCookies = myCookieStore.getCookies();

					Log.d(SERVICE_CALL_TAG, "HttpGet Response - executeHttpsGet - " + response);

					context.postChiamata(response, listaCookies, posizioneIcona, parametri);
				}
				timeout = false;
			}

			@Override
			public void onFinish()
			{
				timer.cancel();
				if (oops != null)
				{
					oops.finish();
				}
				super.onFinish();
			}

			@Override
			public void onStart()
			{
			}

			@Override
			protected void handleFailureMessage(Throwable arg0, String arg1)
			{
				// super.handleFailureMessage(arg0, arg1);
				if (!context.isFinishing() && !timeout
						&& (context.hasWindowFocus() || Home.getDisableClicksDialog().isShowing() || (oops != null && oops.hasWindowFocus())))
				{
					context.postChiamataException(true, posizioneIcona, null, parametri);
				}
				timeout = false;
			}

			@Override
			protected void handleMessage(Message arg0)
			{
				super.handleMessage(arg0);
			}

			@Override
			protected void handleSuccessMessage(String arg0)
			{
				super.handleSuccessMessage(arg0);
			}

			@Override
			protected Message obtainMessage(int arg0, Object arg1)
			{
				return super.obtainMessage(arg0, arg1);
			}

			@Override
			public void onFailure(Throwable arg0, String arg1)
			{
				myCookieStore.clear();
				// super.onFailure(arg0, arg1);
			}

			@Override
			protected void sendFailureMessage(Throwable arg0, byte[] arg1)
			{
				super.sendFailureMessage(arg0, arg1);
			}

			@Override
			protected void sendFailureMessage(Throwable arg0, String arg1)
			{
				super.sendFailureMessage(arg0, arg1);
			}

			@Override
			protected void sendFinishMessage()
			{
				super.sendFinishMessage();
			}

			@Override
			protected void sendMessage(Message arg0)
			{
				super.sendMessage(arg0);
			}

			@Override
			protected void sendStartMessage()
			{
				super.sendStartMessage();
			}

			@Override
			protected void sendSuccessMessage(String arg0)
			{
				super.sendSuccessMessage(arg0);
			}
		});
		timer.schedule(new TimerTask() {
			@Override
			public void run()
			{
				myClient.cancelRequests(context, true);
				timeout = true;
				if (oops != null)
				{
					oops.finish();
				}
				if (!context.isFinishing()
						&& (context.hasWindowFocus() || Home.getDisableClicksDialog().isShowing() || (oops != null && oops.hasWindowFocus())))
				{
					context.runOnUiThread(new Runnable() {

						public void run()
						{
							context.postChiamataException(true, posizioneIcona, null, parametri);

						}
					});
				}
			}
		}, 20000);
			}

	@Override
	public void executeHttpsGetDummyRes(String urlString, final Login context, String token) throws Exception {

		exception = false;
		final Timer timer = new Timer();
		timeout = false;
		final AsyncHttpClient myClient = new AsyncHttpClient();
		myClient.setTimeout(20000);

		myClient.setCookieStore(myCookieStore);
		if (myCookieStore.getCookies().size() != 0)
		{
			myCookieStore.clear();
		}

		BasicClientCookie newCookie = new BasicClientCookie("SMSESSION", "SMSESSION");
		newCookie.setVersion(1);
		newCookie.setDomain(".enigaseluce.com");
		newCookie.setPath("/");
		newCookie.setValue(token);
		myCookieStore.addCookie(newCookie);
		myClient.setCookieStore(myCookieStore);


		try
		{
			KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
			trustStore.load(null, null);
			MySSLSocketFactory sf = new MySSLSocketFactory(trustStore);
			sf.setHostnameVerifier(MySSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
			myClient.setSSLSocketFactory(sf);
		} catch (Exception e)
		{
			throw new Exception(e.getMessage());
		}

		Log.d(SERVICE_CALL_TAG, "HttpGet URL - executeHttpsGetDummyRes - " + urlString );



		Log.d(TAG,myClient.toString());

		myClient.get(urlString, new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(String response)
			{



					Log.d(SERVICE_CALL_TAG, "HttpGet Response - executeHttpsGetDummyRes - " + response);
					context.postSendToken(response);


				timeout = false;
			}

			@Override
			public void onFinish()
			{
				timer.cancel();

				super.onFinish();
			}

			@Override
			public void onStart()
			{
			}

			@Override
			protected void handleFailureMessage(Throwable arg0, String arg1)
			{
				// super.handleFailureMessage(arg0, arg1);

				timeout = false;
			}

			@Override
			protected void handleMessage(Message arg0)
			{
				super.handleMessage(arg0);
			}

			@Override
			protected void handleSuccessMessage(String arg0)
			{
				super.handleSuccessMessage(arg0);
			}

			@Override
			protected Message obtainMessage(int arg0, Object arg1)
			{
				return super.obtainMessage(arg0, arg1);
			}

			@Override
			public void onFailure(Throwable arg0, String arg1)
			{
				myCookieStore.clear();
				// super.onFailure(arg0, arg1);
			}

			@Override
			protected void sendFailureMessage(Throwable arg0, byte[] arg1)
			{
				super.sendFailureMessage(arg0, arg1);
			}

			@Override
			protected void sendFailureMessage(Throwable arg0, String arg1)
			{
				super.sendFailureMessage(arg0, arg1);
			}

			@Override
			protected void sendFinishMessage()
			{
				super.sendFinishMessage();
			}

			@Override
			protected void sendMessage(Message arg0)
			{
				super.sendMessage(arg0);
			}

			@Override
			protected void sendStartMessage()
			{
				super.sendStartMessage();
			}

			@Override
			protected void sendSuccessMessage(String arg0)
			{
				super.sendSuccessMessage(arg0);
			}
		});
		timer.schedule(new TimerTask() {
			@Override
			public void run()
			{
				myClient.cancelRequests(context, true);
				timeout = true;

				if (!context.isFinishing()
						&& (context.hasWindowFocus() || Home.getDisableClicksDialog().isShowing() ))
				{
					context.runOnUiThread(new Runnable() {

						public void run()
						{


						}
					});
				}
			}
		}, 20000);
	}

	@Override
	public void executeHttpsGetMaintenance(final String urlString, final Activity context) throws Exception {
		exception = false;
		final Timer timer = new Timer();
		timeout = false;
		final AsyncHttpClient myClient = new AsyncHttpClient();
		myClient.setTimeout(20000);

		myClient.setCookieStore(myCookieStore);

		try
		{
			KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
			trustStore.load(null, null);
			MySSLSocketFactory sf = new MySSLSocketFactory(trustStore);
			sf.setHostnameVerifier(MySSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
			myClient.setSSLSocketFactory(sf);
		} catch (Exception e)
		{
			throw new Exception(e.getMessage());
		}

		Log.d(SERVICE_CALL_TAG, "HttpGet URL - executeHttpsGetMaintenance - " + urlString );



		Log.d(TAG,myClient.toString());

		myClient.get(urlString, new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(String response) {

				Log.d(SERVICE_CALL_TAG, "HttpGet Response - executeHttpsGetMaintenance - " + response);

				if (context instanceof OopsPageMain)
				{
					((OopsPageMain)context).onResponse(response);
				}
				else if (context instanceof Main)
				{
					((Main)context).onResponse(response);
				}



				timeout = false;
			}

			@Override
			public void onFinish()
			{
				timer.cancel();

				super.onFinish();
			}

			@Override
			public void onStart()
			{
			}

			@Override
			protected void handleFailureMessage(Throwable arg0, String arg1)
			{
				Log.d(SERVICE_CALL_TAG, "HttpGet Response - executeHttpsGetMaintenance - error calling method");

				timeout = false;
			}

			@Override
			protected void handleMessage(Message arg0)
			{
				super.handleMessage(arg0);
			}

			@Override
			protected void handleSuccessMessage(String arg0)
			{
				super.handleSuccessMessage(arg0);
			}

			@Override
			protected Message obtainMessage(int arg0, Object arg1)
			{
				return super.obtainMessage(arg0, arg1);
			}

			@Override
			public void onFailure(Throwable arg0, String arg1)
			{
				myCookieStore.clear();
				// super.onFailure(arg0, arg1);
			}

			@Override
			protected void sendFailureMessage(Throwable arg0, byte[] arg1)
			{
				super.sendFailureMessage(arg0, arg1);
			}

			@Override
			protected void sendFailureMessage(Throwable arg0, String arg1)
			{
				super.sendFailureMessage(arg0, arg1);
			}

			@Override
			protected void sendFinishMessage()
			{
				super.sendFinishMessage();
			}

			@Override
			protected void sendMessage(Message arg0)
			{
				super.sendMessage(arg0);
			}

			@Override
			protected void sendStartMessage()
			{
				super.sendStartMessage();
			}

			@Override
			protected void sendSuccessMessage(String arg0)
			{
				super.sendSuccessMessage(arg0);
			}
		});
		timer.schedule(new TimerTask() {
			@Override
			public void run()
			{
				myClient.cancelRequests(context, true);
				timeout = true;

				if (!context.isFinishing()
						&& (context.hasWindowFocus() || Home.getDisableClicksDialog().isShowing() ))
				{
					context.runOnUiThread(new Runnable() {

						public void run()
						{


						}
					});
				}
			}
		}, 20000);
	}

	/**
	 * Metodo da chiamare per le chiamate dalla schermata di settings (Home)
	 * 
	 * @param urlString
	 * @param parametri
	 *            in formato String gia' costruita nel activity context
	 * @param context
	 * @throws Exception
	 */
	public void executeHttpsGet(String urlString, final String parametri, final Activity context, final int posizioneIcona, final Activity oops) throws Exception
	{
		
		exception = false;
		final Timer timer = new Timer();
		timeout = false;
		final AsyncHttpClient myClient = new AsyncHttpClient();
		myClient.setTimeout(20000);
		List<Cookie> listaCookie = myCookieStore.getCookies();

//		for (int i = 0; i < listaCookie.size(); i++)
//		{
//			Cookie cookie = listaCookie.get(i);
//
//			BasicClientCookie newCookie = new BasicClientCookie(cookie.getName(), cookie.getValue());
//			newCookie.setVersion(cookie.getVersion());
//			newCookie.setDomain(".eni.mobi");
//			newCookie.setPath(cookie.getPath());
//
//			myCookieStore.addCookie(newCookie);
//		}
		myClient.setCookieStore(myCookieStore);

		String parametriAdjusted = adjustQueryString(parametri);

		try
		{
			KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
			trustStore.load(null, null);
			MySSLSocketFactory sf = new MySSLSocketFactory(trustStore);
			sf.setHostnameVerifier(MySSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
			myClient.setSSLSocketFactory(sf);
		} catch (Exception e)
		{
			throw new Exception(e.getMessage());
		}

		Log.d(SERVICE_CALL_TAG, "HomePosizioneiCona[" + posizioneIcona + "] URL - " + urlString);
		Log.d(SERVICE_CALL_TAG, "HomePosizioneiCona[" + posizioneIcona + "] - " + "parametri[" + parametriAdjusted + "]");

		myClient.get(urlString + parametriAdjusted, new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(String response)
			{
				if (!context.isFinishing() && !timeout && (context.hasWindowFocus() || Home.getDisableClicksDialog().isShowing() || (oops != null && oops.hasWindowFocus()))) {

					List<Cookie> listaCookies = myCookieStore.getCookies();

					HashMap<String, String> parameter = new HashMap<String, String>();
					parameter.put("PARAMETER", parametri);

					Log.d(SERVICE_CALL_TAG, "HomePosizioneiCona[" + posizioneIcona + "] Response - " + response);

					if(context instanceof Home) {

						((Home)context).postChiamata(response, listaCookies, posizioneIcona, parameter);

					}else if(context instanceof BollettePagamenti) {
						
						((BollettePagamenti)context).postChiamataDomiciliazione(response);

						
					}

				}
				timeout = false;
			}

			@Override
			public void onFinish()
			{
				timer.cancel();
				if (oops != null)
				{
					oops.finish();
				}
				super.onFinish();
			}

			@Override
			public void onStart()
			{
			}

			@Override
			protected void handleFailureMessage(Throwable arg0, String arg1)
			{
				// super.handleFailureMessage(arg0, arg1);
				if (!context.isFinishing() && !timeout && (context.hasWindowFocus() || Home.getDisableClicksDialog().isShowing() || (oops != null && oops.hasWindowFocus()))) {

					if(context instanceof Home) {

						((Home)context).postChiamataException(true, posizioneIcona, parametri, null);

					}else if(context instanceof BollettePagamenti) {
						
						((BollettePagamenti)context).postChiamataDomiciliazioneException();
						
					}

				}

				timeout = false;

			}

			@Override
			protected void handleMessage(Message arg0)
			{
				super.handleMessage(arg0);
			}

			@Override
			protected void handleSuccessMessage(String arg0)
			{
				super.handleSuccessMessage(arg0);
			}

			@Override
			protected Message obtainMessage(int arg0, Object arg1)
			{
				return super.obtainMessage(arg0, arg1);
			}

			@Override
			public void onFailure(Throwable arg0, String arg1)
			{
				myCookieStore.clear();
				// super.onFailure(arg0, arg1);
			}

			@Override
			protected void sendFailureMessage(Throwable arg0, byte[] arg1)
			{
				super.sendFailureMessage(arg0, arg1);
			}

			@Override
			protected void sendFailureMessage(Throwable arg0, String arg1)
			{
				super.sendFailureMessage(arg0, arg1);
			}

			@Override
			protected void sendFinishMessage()
			{
				super.sendFinishMessage();
			}

			@Override
			protected void sendMessage(Message arg0)
			{
				super.sendMessage(arg0);
			}

			@Override
			protected void sendStartMessage()
			{
				super.sendStartMessage();
			}

			@Override
			protected void sendSuccessMessage(String arg0)
			{
				super.sendSuccessMessage(arg0);
			}
		});
		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				
				myClient.cancelRequests(context, true);
				timeout = true;
				
				if (oops != null) {
					oops.finish();
				}
				
				if (!context.isFinishing() && (context.hasWindowFocus() || Home.getDisableClicksDialog().isShowing() || (oops != null && oops.hasWindowFocus()))) {

					context.runOnUiThread(new Runnable() {

						public void run() {

							if(context instanceof Home) {

								((Home)context).postChiamataException(true, posizioneIcona, parametri, null);

							}else if(context instanceof BollettePagamenti) {
								
								((BollettePagamenti)context).postChiamataDomiciliazioneException();
								
							}

						}
					});
				}
			}
		}, 20000);
	}

	/**
	 * Metodo da chiamare per le chiamate dalla schermata di stato avanzamento
	 * forniture
	 * 
	 * @param urlString
	 * @param parametri
	 * @param context
	 * @throws Exception
	 */
	public void executeHttpsGetAvanzamentoForniture(String urlString, final HashMap<String, String> parametri, final StatoAvanzamentoFornitura context,
			final Activity oops) throws Exception
			{
		exception = false;
		final Timer timer = new Timer();
		timeout = false;
		final AsyncHttpClient myClient = new AsyncHttpClient();
		myClient.setTimeout(20000);
		List<Cookie> listaCookie = myCookieStore.getCookies();

//		for (int i = 0; i < listaCookie.size(); i++)
//		{
//			Cookie cookie = listaCookie.get(i);
//
//			BasicClientCookie newCookie = new BasicClientCookie(cookie.getName(), cookie.getValue());
//			newCookie.setVersion(cookie.getVersion());
//			newCookie.setDomain(".eni.mobi");
//			newCookie.setPath(cookie.getPath());
//
//			myCookieStore.addCookie(newCookie);
//		}
		String paramString = hashMapParameterToString(parametri);
		paramString = adjustQueryString(paramString);

		myClient.setCookieStore(myCookieStore);
		try
		{
			KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
			trustStore.load(null, null);
			MySSLSocketFactory sf = new MySSLSocketFactory(trustStore);
			sf.setHostnameVerifier(MySSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
			myClient.setSSLSocketFactory(sf);
		} catch (Exception e)
		{
			throw new Exception(e.getMessage());
		}

		Log.d(SERVICE_CALL_TAG, "GetAvanzamentoForniture URL - executeHttpsGetAvanzamentoForniture - " + urlString + paramString);
		Log.d(SERVICE_CALL_TAG, "GetAvanzamentoForniture Input Params - executeHttpsGetAvanzamentoForniture - " + "parametri[" + parametri + "]");

		myClient.get(urlString + paramString, new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(String response)
			{
				if (!context.isFinishing() && !timeout && (context.hasWindowFocus() || (oops != null && oops.hasWindowFocus())))
				{
					List<Cookie> listaCookies = myCookieStore.getCookies();

					Log.d(SERVICE_CALL_TAG, "GetAvanzamentoForniture Response - executeHttpsGetAvanzamentoForniture - " + response);
					context.postChiamata(response, listaCookies, parametri);
				}
				timeout = false;
			}

			@Override
			public void onFinish()
			{
				timer.cancel();
				if (oops != null)
				{
					oops.finish();
				}
				super.onFinish();
			}

			@Override
			public void onStart()
			{
			}

			@Override
			protected void handleFailureMessage(Throwable arg0, String arg1)
			{
				// super.handleFailureMessage(arg0, arg1);
				if (!context.isFinishing() && !timeout && (context.hasWindowFocus() || (oops != null && oops.hasWindowFocus())))
				{
					context.postChiamataException(true, parametri);
				}
				timeout = false;
			}

			@Override
			protected void handleMessage(Message arg0)
			{
				super.handleMessage(arg0);
			}

			@Override
			protected void handleSuccessMessage(String arg0)
			{
				super.handleSuccessMessage(arg0);
			}

			@Override
			protected Message obtainMessage(int arg0, Object arg1)
			{
				return super.obtainMessage(arg0, arg1);
			}

			@Override
			public void onFailure(Throwable arg0, String arg1)
			{
				// myCookieStore.clear();
				// super.onFailure(arg0, arg1);
			}

			@Override
			protected void sendFailureMessage(Throwable arg0, byte[] arg1)
			{
				super.sendFailureMessage(arg0, arg1);
			}

			@Override
			protected void sendFailureMessage(Throwable arg0, String arg1)
			{
				super.sendFailureMessage(arg0, arg1);
			}

			@Override
			protected void sendFinishMessage()
			{
				super.sendFinishMessage();
			}

			@Override
			protected void sendMessage(Message arg0)
			{
				super.sendMessage(arg0);
			}

			@Override
			protected void sendStartMessage()
			{
				super.sendStartMessage();
			}

			@Override
			protected void sendSuccessMessage(String arg0)
			{
				super.sendSuccessMessage(arg0);
			}
		});
		timer.schedule(new TimerTask() {
			@Override
			public void run()
			{
				myClient.cancelRequests(context, true);
				timeout = true;
				if (oops != null)
				{
					oops.finish();
				}
				if (!context.isFinishing() && (context.hasWindowFocus() || (oops != null && oops.hasWindowFocus())))
				{
					context.runOnUiThread(new Runnable() {

						public void run()
						{
							context.postChiamataException(true, parametri);

						}
					});
				}
			}
		}, 20000);
			}

	public void executeHttpsGetAutolettura(String urlString, final HashMap<String, String> parametri, final Autolettura context, final String pdf,
			final String pdr, final String sistemaBilling, final String societaFornitrice, final String numeroCifreMisuratore, final Activity oops)
					throws Exception
					{
		exception = false;
		final Timer timer = new Timer();
		timeout = false;
		final AsyncHttpClient myClient = new AsyncHttpClient();
		myClient.setTimeout(20000);
		List<Cookie> listaCookie = myCookieStore.getCookies();
//		for (int i = 0; i < listaCookie.size(); i++)
//		{
//			Cookie cookie = listaCookie.get(i);
//
//			BasicClientCookie newCookie = new BasicClientCookie(cookie.getName(), cookie.getValue());
//			newCookie.setVersion(cookie.getVersion());
//			newCookie.setDomain(".eni.mobi");
//			newCookie.setPath(cookie.getPath());
//
//			myCookieStore.addCookie(newCookie);
//		}
		String paramString = hashMapParameterToString(parametri);
		paramString = adjustQueryString(paramString);

		myClient.setCookieStore(myCookieStore);
		try
		{
			KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
			trustStore.load(null, null);
			MySSLSocketFactory sf = new MySSLSocketFactory(trustStore);
			sf.setHostnameVerifier(MySSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
			myClient.setSSLSocketFactory(sf);
		} catch (Exception e)
		{
			throw new Exception(e.getMessage());
		}

		Log.d(SERVICE_CALL_TAG, "GetAutolettura URL - executeHttpsGetAutolettura - " + urlString + paramString);
		Log.d(SERVICE_CALL_TAG, "GetAutolettura Input Params - executeHttpsGetAutolettura - " + "parametri[" + parametri + "]");

		myClient.get(urlString + paramString, new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(String response)
			{
				if (!context.isFinishing() && !timeout && (context.hasWindowFocus() || (oops != null && oops.hasWindowFocus())))
				{
					List<Cookie> listaCookies = myCookieStore.getCookies();

					Log.d(SERVICE_CALL_TAG, "GetAutolettura Response - executeHttpsGetAutolettura - " + response);
					context.postChiamata(response, listaCookies, pdf, pdr, sistemaBilling, societaFornitrice, numeroCifreMisuratore,
							parametri.get("codiceCliente"));
				}
				timeout = false;
			}

			@Override
			public void onFinish()
			{
				timer.cancel();
				if (oops != null)
				{
					oops.finish();
				}
				super.onFinish();
			}

			@Override
			public void onStart()
			{
			}

			@Override
			protected void handleFailureMessage(Throwable arg0, String arg1)
			{
				// super.handleFailureMessage(arg0, arg1);
				if (!context.isFinishing() && !timeout && (context.hasWindowFocus() || (oops != null && oops.hasWindowFocus())))
				{
					context.postChiamataException(true, pdf, sistemaBilling, societaFornitrice, numeroCifreMisuratore, parametri.get("codiceCliente"));
				}
				timeout = false;
			}

			@Override
			protected void handleMessage(Message arg0)
			{
				super.handleMessage(arg0);
			}

			@Override
			protected void handleSuccessMessage(String arg0)
			{
				super.handleSuccessMessage(arg0);
			}

			@Override
			protected Message obtainMessage(int arg0, Object arg1)
			{
				return super.obtainMessage(arg0, arg1);
			}

			@Override
			public void onFailure(Throwable arg0, String arg1)
			{
				myCookieStore.clear();
				// super.onFailure(arg0, arg1);
			}

			@Override
			protected void sendFailureMessage(Throwable arg0, byte[] arg1)
			{
				super.sendFailureMessage(arg0, arg1);
			}

			@Override
			protected void sendFailureMessage(Throwable arg0, String arg1)
			{
				super.sendFailureMessage(arg0, arg1);
			}

			@Override
			protected void sendFinishMessage()
			{
				super.sendFinishMessage();
			}

			@Override
			protected void sendMessage(Message arg0)
			{
				super.sendMessage(arg0);
			}

			@Override
			protected void sendStartMessage()
			{
				super.sendStartMessage();
			}

			@Override
			protected void sendSuccessMessage(String arg0)
			{
				super.sendSuccessMessage(arg0);
			}
		});
		timer.schedule(new TimerTask() {
			@Override
			public void run()
			{
				myClient.cancelRequests(context, true);
				timeout = true;
				if (oops != null)
				{
					oops.finish();
				}
				if (!context.isFinishing() && (context.hasWindowFocus() || (oops != null && oops.hasWindowFocus())))
				{
					context.runOnUiThread(new Runnable() {
						public void run()
						{
							context.postChiamataException(true, pdf, sistemaBilling, societaFornitrice, numeroCifreMisuratore, parametri.get("codiceCliente"));

						}
					});
				}
			}
		}, 20000);
					}

	public void executeHttpsGetAutoletturaFromStoricoDetail(String urlString, final HashMap<String, String> parametri, final StoricoLettureDetail context,
			final String pdf, final String pdr, final String sistemaBilling, final String societaFornitrice, final String numeroCifreMisuratore,
			final Activity oops) throws Exception
			{
		exception = false;
		final Timer timer = new Timer();
		timeout = false;
		final AsyncHttpClient myClient = new AsyncHttpClient();
		myClient.setTimeout(20000);
		List<Cookie> listaCookie = myCookieStore.getCookies();
//		for (int i = 0; i < listaCookie.size(); i++)
//		{
//			Cookie cookie = listaCookie.get(i);
//
//			BasicClientCookie newCookie = new BasicClientCookie(cookie.getName(), cookie.getValue());
//			newCookie.setVersion(cookie.getVersion());
//			newCookie.setDomain(".eni.mobi");
//			newCookie.setPath(cookie.getPath());
//
//			myCookieStore.addCookie(newCookie);
//		}
		String paramString = hashMapParameterToString(parametri);
		paramString = adjustQueryString(paramString);

		myClient.setCookieStore(myCookieStore);
		try
		{
			KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
			trustStore.load(null, null);
			MySSLSocketFactory sf = new MySSLSocketFactory(trustStore);
			sf.setHostnameVerifier(MySSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
			myClient.setSSLSocketFactory(sf);
		} catch (Exception e)
		{
			throw new Exception(e.getMessage());
		}

		Log.d(SERVICE_CALL_TAG, "GetAutolettura URL - executeHttpsGetAutolettura - " + urlString + paramString);
		Log.d(SERVICE_CALL_TAG, "GetAutolettura Input Params - executeHttpsGetAutolettura - " + "parametri[" + parametri + "]");

		myClient.get(urlString + paramString, new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(String response)
			{
				if (!context.isFinishing() && !timeout && (context.hasWindowFocus() || (oops != null && oops.hasWindowFocus())))
				{
					List<Cookie> listaCookies = myCookieStore.getCookies();

					Log.d(SERVICE_CALL_TAG, "GetAutolettura Response - executeHttpsGetAutolettura - " + response);
					context.postChiamata(response, listaCookies, pdf, pdr, sistemaBilling, societaFornitrice, numeroCifreMisuratore,
							parametri.get("codiceCliente"));
				}
				timeout = false;
			}

			@Override
			public void onFinish()
			{
				timer.cancel();
				if (oops != null)
				{
					oops.finish();
				}
				super.onFinish();
			}

			@Override
			public void onStart()
			{
			}

			@Override
			protected void handleFailureMessage(Throwable arg0, String arg1)
			{
				// super.handleFailureMessage(arg0, arg1);
				if (!context.isFinishing() && !timeout && (context.hasWindowFocus() || (oops != null && oops.hasWindowFocus())))
				{
					context.postChiamataException(true, pdf, sistemaBilling, societaFornitrice, numeroCifreMisuratore, parametri.get("codiceCliente"));
				}
				timeout = false;
			}

			@Override
			protected void handleMessage(Message arg0)
			{
				super.handleMessage(arg0);
			}

			@Override
			protected void handleSuccessMessage(String arg0)
			{
				super.handleSuccessMessage(arg0);
			}

			@Override
			protected Message obtainMessage(int arg0, Object arg1)
			{
				return super.obtainMessage(arg0, arg1);
			}

			@Override
			public void onFailure(Throwable arg0, String arg1)
			{
				myCookieStore.clear();
				// super.onFailure(arg0, arg1);
			}

			@Override
			protected void sendFailureMessage(Throwable arg0, byte[] arg1)
			{
				super.sendFailureMessage(arg0, arg1);
			}

			@Override
			protected void sendFailureMessage(Throwable arg0, String arg1)
			{
				super.sendFailureMessage(arg0, arg1);
			}

			@Override
			protected void sendFinishMessage()
			{
				super.sendFinishMessage();
			}

			@Override
			protected void sendMessage(Message arg0)
			{
				super.sendMessage(arg0);
			}

			@Override
			protected void sendStartMessage()
			{
				super.sendStartMessage();
			}

			@Override
			protected void sendSuccessMessage(String arg0)
			{
				super.sendSuccessMessage(arg0);
			}
		});
		timer.schedule(new TimerTask() {
			@Override
			public void run()
			{
				myClient.cancelRequests(context, true);
				timeout = true;
				if (oops != null)
				{
					oops.finish();
				}
				if (!context.isFinishing() && (context.hasWindowFocus() || (oops != null && oops.hasWindowFocus())))
				{
					context.runOnUiThread(new Runnable() {
						public void run()
						{
							context.postChiamataException(true, pdf, sistemaBilling, societaFornitrice, numeroCifreMisuratore, parametri.get("codiceCliente"));

						}
					});
				}
			}
		}, 20000);
			}

	public void executeHttpsGetAutoletturaInsert(String urlString, final HashMap<String, String> parametri, final AutoLetturaInsert context, final Activity oops)
			throws Exception
			{
		exception = false;
		final Timer timer = new Timer();
		timeout = false;
		final AsyncHttpClient myClient = new AsyncHttpClient();
		myClient.setTimeout(20000);
		List<Cookie> listaCookie = myCookieStore.getCookies();
//		for (int i = 0; i < listaCookie.size(); i++)
//		{
//			Cookie cookie = listaCookie.get(i);
//
//			BasicClientCookie newCookie = new BasicClientCookie(cookie.getName(), cookie.getValue());
//			newCookie.setVersion(cookie.getVersion());
//			newCookie.setDomain(".eni.mobi");
//			newCookie.setPath(cookie.getPath());
//
//			myCookieStore.addCookie(newCookie);
//		}
		String paramString = hashMapParameterToString(parametri);
		paramString = adjustQueryString(paramString);

		myClient.setCookieStore(myCookieStore);
		try
		{
			KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
			trustStore.load(null, null);
			MySSLSocketFactory sf = new MySSLSocketFactory(trustStore);
			sf.setHostnameVerifier(MySSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
			myClient.setSSLSocketFactory(sf);
		} catch (Exception e)
		{
			throw new Exception(e.getMessage());
		}

		Log.d(SERVICE_CALL_TAG, "GetAutoletturaInsert URL - executeHttpsGetAutoletturaInsert - " + urlString + paramString);
		Log.d(SERVICE_CALL_TAG, "GetAutoletturaInsert Input Params - executeHttpsGetAutoletturaInsert - " + "parametri[" + parametri + "]");

		myClient.get(urlString + paramString, new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(String response)
			{
				if (!context.isFinishing() && !timeout && (context.hasWindowFocus() || (oops != null && oops.hasWindowFocus())))
				{
					List<Cookie> listaCookies = myCookieStore.getCookies();

					Log.d(SERVICE_CALL_TAG, "GetAutoletturaInsert Response - executeHttpsGetAutoletturaInsert - " + response);
					context.postChiamata(response, listaCookies, parametri, oops);
				}
				timeout = false;
			}

			@Override
			public void onFinish()
			{
				timer.cancel();
				if (oops != null)
				{
					oops.finish();
				}
				super.onFinish();
			}

			@Override
			public void onStart()
			{
			}

			@Override
			protected void handleFailureMessage(Throwable arg0, String arg1)
			{
				// super.handleFailureMessage(arg0, arg1);
				if (!context.isFinishing() && !timeout && (context.hasWindowFocus() || (oops != null && oops.hasWindowFocus())))
				{
					context.postChiamataException(true, null);
				}
				timeout = false;
			}

			@Override
			protected void handleMessage(Message arg0)
			{
				super.handleMessage(arg0);
			}

			@Override
			protected void handleSuccessMessage(String arg0)
			{
				super.handleSuccessMessage(arg0);
			}

			@Override
			protected Message obtainMessage(int arg0, Object arg1)
			{
				return super.obtainMessage(arg0, arg1);
			}

			@Override
			public void onFailure(Throwable arg0, String arg1)
			{
				myCookieStore.clear();
				// super.onFailure(arg0, arg1);
			}

			@Override
			protected void sendFailureMessage(Throwable arg0, byte[] arg1)
			{
				super.sendFailureMessage(arg0, arg1);
			}

			@Override
			protected void sendFailureMessage(Throwable arg0, String arg1)
			{
				super.sendFailureMessage(arg0, arg1);
			}

			@Override
			protected void sendFinishMessage()
			{
				super.sendFinishMessage();
			}

			@Override
			protected void sendMessage(Message arg0)
			{
				super.sendMessage(arg0);
			}

			@Override
			protected void sendStartMessage()
			{
				super.sendStartMessage();
			}

			@Override
			protected void sendSuccessMessage(String arg0)
			{
				super.sendSuccessMessage(arg0);
			}
		});
		timer.schedule(new TimerTask() {
			@Override
			public void run()
			{
				myClient.cancelRequests(context, true);
				timeout = true;
				if (oops != null)
				{
					oops.finish();
				}
				if (!context.isFinishing() && (context.hasWindowFocus() || (oops != null && oops.hasWindowFocus())))
				{
					context.runOnUiThread(new Runnable() {

						public void run()
						{
							context.postChiamataException(true, null);

						}
					});
				}
			}
		}, 20000);
			}

	public void executeHttpsGetAutoletturaInsert(String urlString, final HashMap<String, String> parametri, final AutoLetturaInsert context,
			final String annullaOrConferma, final Activity oops) throws Exception
			{
		exception = false;
		final Timer timer = new Timer();
		timeout = false;
		final AsyncHttpClient myClient = new AsyncHttpClient();
		myClient.setTimeout(20000);
		List<Cookie> listaCookie = myCookieStore.getCookies();

//		for (int i = 0; i < listaCookie.size(); i++)
//		{
//			Cookie cookie = listaCookie.get(i);
//
//			BasicClientCookie newCookie = new BasicClientCookie(cookie.getName(), cookie.getValue());
//			newCookie.setVersion(cookie.getVersion());
//			newCookie.setDomain(".eni.mobi");
//			newCookie.setPath(cookie.getPath());
//
//			myCookieStore.addCookie(newCookie);
//		}
		String paramString = hashMapParameterToString(parametri);
		paramString = adjustQueryString(paramString);

		myClient.setCookieStore(myCookieStore);
		try
		{
			KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
			trustStore.load(null, null);
			MySSLSocketFactory sf = new MySSLSocketFactory(trustStore);
			sf.setHostnameVerifier(MySSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
			myClient.setSSLSocketFactory(sf);
		} catch (Exception e)
		{
			throw new Exception(e.getMessage());
		}

		Log.d(SERVICE_CALL_TAG, "GetAutoletturaInsert URL - executeHttpsGetAutoletturaInsert - " + urlString + paramString);
		Log.d(SERVICE_CALL_TAG, "GetAutoletturaInsert Input Params - executeHttpsGetAutoletturaInsert - " + "parametri[" + parametri + "]");

		myClient.get(urlString + paramString, new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(String response)
			{
				if (!context.isFinishing() && !timeout && (context.hasWindowFocus() || (oops != null && oops.hasWindowFocus())))
				{
					List<Cookie> listaCookies = myCookieStore.getCookies();

					Log.d(SERVICE_CALL_TAG, "GetAutoletturaInsert Response - executeHttpsGetAutoletturaInsert - " + response);
					context.postChiamata(response, listaCookies, parametri, annullaOrConferma);
				}
				timeout = false;
			}

			@Override
			public void onFinish()
			{
				timer.cancel();
				if (oops != null)
				{
					oops.finish();
				}
				super.onFinish();
			}

			@Override
			public void onStart()
			{
			}

			@Override
			protected void handleFailureMessage(Throwable arg0, String arg1)
			{
				// super.handleFailureMessage(arg0, arg1);
				if (!context.isFinishing() && !timeout && (context.hasWindowFocus() || (oops != null && oops.hasWindowFocus())))
				{
					context.postChiamataException(true, annullaOrConferma);
				}
				timeout = false;
			}

			@Override
			protected void handleMessage(Message arg0)
			{
				super.handleMessage(arg0);
			}

			@Override
			protected void handleSuccessMessage(String arg0)
			{
				super.handleSuccessMessage(arg0);
			}

			@Override
			protected Message obtainMessage(int arg0, Object arg1)
			{
				return super.obtainMessage(arg0, arg1);
			}

			@Override
			public void onFailure(Throwable arg0, String arg1)
			{
				myCookieStore.clear();
				// super.onFailure(arg0, arg1);
			}

			@Override
			protected void sendFailureMessage(Throwable arg0, byte[] arg1)
			{
				super.sendFailureMessage(arg0, arg1);
			}

			@Override
			protected void sendFailureMessage(Throwable arg0, String arg1)
			{
				super.sendFailureMessage(arg0, arg1);
			}

			@Override
			protected void sendFinishMessage()
			{
				super.sendFinishMessage();
			}

			@Override
			protected void sendMessage(Message arg0)
			{
				super.sendMessage(arg0);
			}

			@Override
			protected void sendStartMessage()
			{
				super.sendStartMessage();
			}

			@Override
			protected void sendSuccessMessage(String arg0)
			{
				super.sendSuccessMessage(arg0);
			}
		});
		timer.schedule(new TimerTask() {
			@Override
			public void run()
			{
				myClient.cancelRequests(context, true);
				timeout = true;
				if (oops != null)
				{
					oops.finish();
				}
				if (!context.isFinishing() && (context.hasWindowFocus() || (oops != null && oops.hasWindowFocus())))
				{
					context.runOnUiThread(new Runnable() {
						public void run()
						{
							context.postChiamataException(true, annullaOrConferma);

						}
					});
				}
			}
		}, 20000);
			}

	public void executeHttpsGetStoricoLetture(String urlString, final HashMap<String, String> parametri, final AutoLetturaInsert context, final String pdf,
			final String pdr, final Activity oops) throws Exception
			{
		exception = false;
		final Timer timer = new Timer();
		timeout = false;
		final AsyncHttpClient myClient = new AsyncHttpClient();
		myClient.setTimeout(20000);
		List<Cookie> listaCookie = myCookieStore.getCookies();

//		for (int i = 0; i < listaCookie.size(); i++)
//		{
//			Cookie cookie = listaCookie.get(i);
//
//			BasicClientCookie newCookie = new BasicClientCookie(cookie.getName(), cookie.getValue());
//			newCookie.setVersion(cookie.getVersion());
//			newCookie.setDomain(".eni.mobi");
//			newCookie.setPath(cookie.getPath());
//
//			myCookieStore.addCookie(newCookie);
//		}
		String paramString = hashMapParameterToString(parametri);

		paramString = adjustQueryString(paramString);

		myClient.setCookieStore(myCookieStore);
		try
		{
			KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
			trustStore.load(null, null);
			MySSLSocketFactory sf = new MySSLSocketFactory(trustStore);
			sf.setHostnameVerifier(MySSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
			myClient.setSSLSocketFactory(sf);
		} catch (Exception e)
		{
			throw new Exception(e.getMessage());
		}

		Log.d(SERVICE_CALL_TAG, "GetStoricoLetture URL - executeHttpsGetStoricoLetture - " + urlString + paramString);
		Log.d(SERVICE_CALL_TAG, "GetStoricoLetture Input Params - executeHttpsGetStoricoLetture - " + "parametri[" + parametri + "]");

		myClient.get(urlString + paramString, new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(String response)
			{
				if (!context.isFinishing() && !timeout && (context.hasWindowFocus() || (oops != null && oops.hasWindowFocus())))
				{
					List<Cookie> listaCookies = myCookieStore.getCookies();

					Log.d(SERVICE_CALL_TAG, "GetStoricoLetture Response - executeHttpsGetStoricoLetture - " + response);
					context.postChiamataStoricoLetture(response, listaCookies, pdf, pdr, parametri);
				}
				timeout = false;
			}

			@Override
			public void onFinish()
			{

				timer.cancel();
				if (oops != null)
				{
					oops.finish();
				}
				super.onFinish();
			}

			@Override
			public void onStart()
			{
			}

			@Override
			protected void handleFailureMessage(Throwable arg0, String arg1)
			{

				// super.handleFailureMessage(arg0, arg1);
				if (!context.isFinishing() && !timeout && (context.hasWindowFocus() || (oops != null && oops.hasWindowFocus())))
				{
					context.postChiamataStoricoLettureException(true);
				}
				timeout = false;
			}

			@Override
			protected void handleMessage(Message arg0)
			{
				super.handleMessage(arg0);
			}

			@Override
			protected void handleSuccessMessage(String arg0)
			{
				super.handleSuccessMessage(arg0);
			}

			@Override
			protected Message obtainMessage(int arg0, Object arg1)
			{
				return super.obtainMessage(arg0, arg1);
			}

			@Override
			public void onFailure(Throwable arg0, String arg1)
			{
				myCookieStore.clear();
				// super.onFailure(arg0, arg1);
			}

			@Override
			protected void sendFailureMessage(Throwable arg0, byte[] arg1)
			{
				super.sendFailureMessage(arg0, arg1);
			}

			@Override
			protected void sendFailureMessage(Throwable arg0, String arg1)
			{
				super.sendFailureMessage(arg0, arg1);
			}

			@Override
			protected void sendFinishMessage()
			{
				super.sendFinishMessage();
			}

			@Override
			protected void sendMessage(Message arg0)
			{
				super.sendMessage(arg0);
			}

			@Override
			protected void sendStartMessage()
			{
				super.sendStartMessage();
			}

			@Override
			protected void sendSuccessMessage(String arg0)
			{
				super.sendSuccessMessage(arg0);
			}
		});

		timer.schedule(new TimerTask() {
			@Override
			public void run()
			{
				myClient.cancelRequests(context, true);
				timeout = true;
				if (oops != null)
				{
					oops.finish();
				}
				if (!context.isFinishing() && (context.hasWindowFocus() || (oops != null && oops.hasWindowFocus())))
				{
					context.runOnUiThread(new Runnable() {
						public void run()
						{
							context.postChiamataStoricoLettureException(true);

						}
					});
				}
			}
		}, 20000);
			}

	/**
	 * Metodo da chiamare per le chiamate dalla schermata di stato avanzamento
	 * forniture
	 * 
	 * @param urlString
	 * @param parametri
	 * @param context
	 * @throws Exception
	 */
	public void executeHttpsGetDomiciliazione(String urlString, String parametri, HashMap<String, String> parametriHash, final Activity context,
			final int code, final Activity oops) throws Exception
			{
		exception = false;
		final Timer timer = new Timer();
		timeout = false;
		final AsyncHttpClient myClient = new AsyncHttpClient();

		parametri = adjustQueryString(parametri);
		myClient.setTimeout(20000);
		List<Cookie> listaCookie = myCookieStore.getCookies();

//		for (int i = 0; i < listaCookie.size(); i++)
//		{
//			Cookie cookie = listaCookie.get(i);
//
//			BasicClientCookie newCookie = new BasicClientCookie(cookie.getName(), cookie.getValue());
//			newCookie.setVersion(cookie.getVersion());
//			newCookie.setDomain(".eni.mobi");
//			newCookie.setPath(cookie.getPath());
//
//			myCookieStore.addCookie(newCookie);
//		}
		myClient.setCookieStore(myCookieStore);
		try
		{
			KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
			trustStore.load(null, null);
			MySSLSocketFactory sf = new MySSLSocketFactory(trustStore);
			sf.setHostnameVerifier(MySSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
			myClient.setSSLSocketFactory(sf);
		} catch (Exception e)
		{
			throw new Exception(e.getMessage());
		}
		String richiesta = "";
		switch (code) {
		case 1:
//			richiesta = adjustQueryString(richiesta);
			richiesta = urlString + adjustQueryString(hashMapParameterToString(parametriHash));
			richiesta = richiesta.replaceAll(" ", "%20");
			break;
		case 2:
			richiesta = urlString + adjustQueryString(parametri);
			break;
		}

		Log.d(SERVICE_CALL_TAG, "GetDomiciliazione URL - executeHttpsGetDomiciliazione - " + richiesta);
		Log.d(SERVICE_CALL_TAG, "GetDomiciliazione Input Params - executeHttpsGetDomiciliazione - " + "parametri[" + parametri + "]");

		myClient.get(richiesta, new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(String response)
			{
				if (!context.isFinishing() && !timeout && (context.hasWindowFocus() || (oops != null && oops.hasWindowFocus())))
				{
					List<Cookie> listaCookies = myCookieStore.getCookies();

					Log.d(SERVICE_CALL_TAG, "GetDomiciliazione Response - executeHttpsGetDomiciliazione - " + response);

					if (context instanceof DomiciliazioneInserimentoPersonaFisica)
						((DomiciliazioneInserimentoPersonaFisica) context).postChiamata(response, listaCookies, code);
					else if (context instanceof DomiciliazioneInserimentoPersonaGiuridica)
					{
						((DomiciliazioneInserimentoPersonaGiuridica) context).postChiamata(response, listaCookies, code);
					} else
						((DomiciliazioneInserimentoNonResidenziali) context).postChiamata(response, listaCookies, code);
				}
				timeout = false;
			}

			@Override
			public void onFinish()
			{
				timer.cancel();
				if (oops != null)
				{
					oops.finish();
				}
				super.onFinish();
			}

			@Override
			public void onStart()
			{
			}

			@Override
			protected void handleFailureMessage(Throwable arg0, String arg1)
			{
				// super.handleFailureMessage(arg0, arg1);
				if (!context.isFinishing() && !timeout && (context.hasWindowFocus() || (oops != null && oops.hasWindowFocus())))
				{
					if (context instanceof DomiciliazioneInserimentoPersonaFisica)
						((DomiciliazioneInserimentoPersonaFisica) context).postChiamataException(true);
					else if (context instanceof DomiciliazioneInserimentoPersonaGiuridica)
					{
						((DomiciliazioneInserimentoPersonaGiuridica) context).postChiamataException(true);
					} else
						((DomiciliazioneInserimentoNonResidenziali) context).postChiamataException(true);
				}
				timeout = false;
			}

			@Override
			protected void handleMessage(Message arg0)
			{
				super.handleMessage(arg0);
			}

			@Override
			protected void handleSuccessMessage(String arg0)
			{
				super.handleSuccessMessage(arg0);
			}

			@Override
			protected Message obtainMessage(int arg0, Object arg1)
			{
				return super.obtainMessage(arg0, arg1);
			}

			@Override
			public void onFailure(Throwable arg0, String arg1)
			{
				myCookieStore.clear();
				// super.onFailure(arg0, arg1);
			}

			@Override
			protected void sendFailureMessage(Throwable arg0, byte[] arg1)
			{
				super.sendFailureMessage(arg0, arg1);
			}

			@Override
			protected void sendFailureMessage(Throwable arg0, String arg1)
			{
				super.sendFailureMessage(arg0, arg1);
			}

			@Override
			protected void sendFinishMessage()
			{
				super.sendFinishMessage();
			}

			@Override
			protected void sendMessage(Message arg0)
			{
				super.sendMessage(arg0);
			}

			@Override
			protected void sendStartMessage()
			{
				super.sendStartMessage();
			}

			@Override
			protected void sendSuccessMessage(String arg0)
			{
				super.sendSuccessMessage(arg0);
			}
		});
		timer.schedule(new TimerTask() {
			@Override
			public void run()
			{
				myClient.cancelRequests(context, true);
				timeout = true;
				if (oops != null)
				{
					oops.finish();
				}
				if (!context.isFinishing() && (context.hasWindowFocus() || (oops != null && oops.hasWindowFocus())))
				{
					context.runOnUiThread(new Runnable() {
						public void run()
						{
							if (context instanceof DomiciliazioneInserimentoPersonaFisica)
								((DomiciliazioneInserimentoPersonaFisica) context).postChiamataException(true);
							else if (context instanceof DomiciliazioneInserimentoPersonaGiuridica)
							{
								((DomiciliazioneInserimentoPersonaGiuridica) context).postChiamataException(true);
							} else
								((DomiciliazioneInserimentoNonResidenziali) context).postChiamataException(true);
						}
					});
				}
			}
		}, 20000);
			}

	/**
	 * Metodo da chiamare per le chiamate dalla schermata di bollette pagamenti
	 * 
	 * @param urlString
	 * @param parametri
	 * @param context
	 * @throws Exception
	 */
	public void executeHttpsGetBollettePagamenti(String urlString, final HashMap<String, String> parametri, final BollettePagamenti context,
			final Activity oops, final boolean prossimaFatttura, final boolean bolletteRateizzabili) throws Exception
			{
		exception = false;
		final Timer timer = new Timer();
		timeout = false;
		final AsyncHttpClient myClient = new AsyncHttpClient();
		myClient.setTimeout(20000);
		List<Cookie> listaCookie = myCookieStore.getCookies();

//		for (int i = 0; i < listaCookie.size(); i++)
//		{
//			Cookie cookie = listaCookie.get(i);
//
//			BasicClientCookie newCookie = new BasicClientCookie(cookie.getName(), cookie.getValue());
//			newCookie.setVersion(cookie.getVersion());
//			newCookie.setDomain(".eni.mobi");
//			newCookie.setPath(cookie.getPath());
//
//			myCookieStore.addCookie(newCookie);
//		}
		String paramString = hashMapParameterToString(parametri);
		paramString = adjustQueryString(paramString);

		myClient.setCookieStore(myCookieStore);
		try
		{
			KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
			trustStore.load(null, null);
			MySSLSocketFactory sf = new MySSLSocketFactory(trustStore);
			sf.setHostnameVerifier(MySSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
			myClient.setSSLSocketFactory(sf);
		} catch (Exception e)
		{
			throw new Exception(e.getMessage());
		}

		Log.d(SERVICE_CALL_TAG, "GetBollettePagamenti URL - executeHttpsGetBollettePagamenti - " + urlString + paramString);
		Log.d(SERVICE_CALL_TAG, "GetBollettePagamenti Input Params - executeHttpsGetBollettePagamenti - " + "parametri[" + parametri + "]");

		myClient.get(urlString + paramString, new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(String response)
			{
				if (!context.isFinishing() && !timeout && (context.hasWindowFocus() || (oops != null && oops.hasWindowFocus())))
				{
					List<Cookie> listaCookies = myCookieStore.getCookies();

					Log.d(SERVICE_CALL_TAG, "GetBollettePagamenti Response - executeHttpsGetBollettePagamenti - " + response);
					context.postChiamata(response, listaCookies, parametri, prossimaFatttura, bolletteRateizzabili);
				}
				timeout = false;
			}

			@Override
			public void onFinish()
			{
				timer.cancel();
				if (oops != null)
				{
					oops.finish();
				}
				super.onFinish();
			}

			@Override
			public void onStart()
			{
			}

			@Override
			protected void handleFailureMessage(Throwable arg0, String arg1)
			{
				// super.handleFailureMessage(arg0, arg1);
				if (!context.isFinishing() && !timeout && (context.hasWindowFocus() || (oops != null && oops.hasWindowFocus())))
				{
					context.postChiamataException(true, parametri, prossimaFatttura, bolletteRateizzabili);
				}
				timeout = false;
			}

			@Override
			protected void handleMessage(Message arg0)
			{
				super.handleMessage(arg0);
			}

			@Override
			protected void handleSuccessMessage(String arg0)
			{
				super.handleSuccessMessage(arg0);
			}

			@Override
			protected Message obtainMessage(int arg0, Object arg1)
			{
				return super.obtainMessage(arg0, arg1);
			}

			@Override
			public void onFailure(Throwable arg0, String arg1)
			{
				// myCookieStore.clear();
				// super.onFailure(arg0, arg1);
			}

			@Override
			protected void sendFailureMessage(Throwable arg0, byte[] arg1)
			{
				super.sendFailureMessage(arg0, arg1);
			}

			@Override
			protected void sendFailureMessage(Throwable arg0, String arg1)
			{
				super.sendFailureMessage(arg0, arg1);
			}

			@Override
			protected void sendFinishMessage()
			{
				super.sendFinishMessage();
			}

			@Override
			protected void sendMessage(Message arg0)
			{
				super.sendMessage(arg0);
			}

			@Override
			protected void sendStartMessage()
			{
				super.sendStartMessage();
			}

			@Override
			protected void sendSuccessMessage(String arg0)
			{
				super.sendSuccessMessage(arg0);
			}
		});
		timer.schedule(new TimerTask() {
			@Override
			public void run()
			{
				myClient.cancelRequests(context, true);
				timeout = true;
				if (oops != null)
				{
					oops.finish();
				}
				if (!context.isFinishing() && (context.hasWindowFocus() || (oops != null && oops.hasWindowFocus())))
				{
					context.runOnUiThread(new Runnable() {

						public void run()
						{
							context.postChiamataException(true, parametri, prossimaFatttura, bolletteRateizzabili);

						}
					});
				}
			}
		}, 20000);
			}

	public void executeHttpsGetProssimaBolletta(String urlString, final HashMap<String, String> parametri, final AutoLetturaInsert context,
			final Activity oops, final boolean prossimaFatttura, final boolean bolletteRateizzabili) throws Exception
			{
		exception = false;
		final Timer timer = new Timer();
		timeout = false;
		final AsyncHttpClient myClient = new AsyncHttpClient();
		myClient.setTimeout(20000);
		List<Cookie> listaCookie = myCookieStore.getCookies();

//		for (int i = 0; i < listaCookie.size(); i++)
//		{
//			Cookie cookie = listaCookie.get(i);
//
//			BasicClientCookie newCookie = new BasicClientCookie(cookie.getName(), cookie.getValue());
//			newCookie.setVersion(cookie.getVersion());
//			newCookie.setDomain(".eni.mobi");
//			newCookie.setPath(cookie.getPath());
//
//			myCookieStore.addCookie(newCookie);
//		}
		String paramString = hashMapParameterToString(parametri);
		paramString = adjustQueryString(paramString);

		myClient.setCookieStore(myCookieStore);
		try
		{
			KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
			trustStore.load(null, null);
			MySSLSocketFactory sf = new MySSLSocketFactory(trustStore);
			sf.setHostnameVerifier(MySSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
			myClient.setSSLSocketFactory(sf);
		} catch (Exception e)
		{
			throw new Exception(e.getMessage());
		}

		Log.d(SERVICE_CALL_TAG, "GetProssimaBolletta URL - executeHttpsGetProssimaBolletta - " + urlString + paramString);
		Log.d(SERVICE_CALL_TAG, "GetProssimaBolletta Input Params - executeHttpsGetProssimaBolletta - " + "parametri[" + parametri + "]");

		myClient.get(urlString + paramString, new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(String response)
			{
				System.out.println("!context.isFinishing(): " + (!context.isFinishing()));
				System.out.println("!timeout: " + (!timeout));
				System.out.println("context.hasWindowFocus(): " + (context.hasWindowFocus()));
				System.out.println("oops != null && oops.hasWindowFocus(): " + (oops != null && oops.hasWindowFocus()));

				if (!context.isFinishing() && !timeout && (context.hasWindowFocus() || (oops != null && oops.hasWindowFocus())))
				{
					List<Cookie> listaCookies = myCookieStore.getCookies();

					Log.d(SERVICE_CALL_TAG, "GetProssimaBolletta Response - executeHttpsGetProssimaBolletta - " + response);
					context.postChiamataProssimaBolletta(response, listaCookies, parametri, prossimaFatttura, bolletteRateizzabili);
				}
				timeout = false;
			}

			@Override
			public void onFinish()
			{
				timer.cancel();
				if (oops != null)
				{
					oops.finish();
				}
				super.onFinish();
			}

			@Override
			public void onStart()
			{
			}

			@Override
			protected void handleFailureMessage(Throwable arg0, String arg1)
			{
				// super.handleFailureMessage(arg0, arg1);
				if (!context.isFinishing() && !timeout && (context.hasWindowFocus() || (oops != null && oops.hasWindowFocus())))
				{
					context.postChiamataProssimaBollettaException(true, parametri, prossimaFatttura, bolletteRateizzabili);
				}
				timeout = false;
			}

			@Override
			protected void handleMessage(Message arg0)
			{
				super.handleMessage(arg0);
			}

			@Override
			protected void handleSuccessMessage(String arg0)
			{
				super.handleSuccessMessage(arg0);
			}

			@Override
			protected Message obtainMessage(int arg0, Object arg1)
			{
				return super.obtainMessage(arg0, arg1);
			}

			@Override
			public void onFailure(Throwable arg0, String arg1)
			{
				System.out.println("onFailure: " + arg1);
				arg0.printStackTrace();
				// myCookieStore.clear();
				// super.onFailure(arg0, arg1);
			}

			@Override
			protected void sendFailureMessage(Throwable arg0, byte[] arg1)
			{
				super.sendFailureMessage(arg0, arg1);
			}

			@Override
			protected void sendFailureMessage(Throwable arg0, String arg1)
			{
				super.sendFailureMessage(arg0, arg1);
			}

			@Override
			protected void sendFinishMessage()
			{
				super.sendFinishMessage();
			}

			@Override
			protected void sendMessage(Message arg0)
			{
				super.sendMessage(arg0);
			}

			@Override
			protected void sendStartMessage()
			{
				super.sendStartMessage();
			}

			@Override
			protected void sendSuccessMessage(String arg0)
			{
				super.sendSuccessMessage(arg0);
			}
		});
		timer.schedule(new TimerTask() {
			@Override
			public void run()
			{
				myClient.cancelRequests(context, true);
				timeout = true;
				if (oops != null)
				{
					oops.finish();
				}
				if (!context.isFinishing() && (context.hasWindowFocus() || (oops != null && oops.hasWindowFocus())))
				{
					context.runOnUiThread(new Runnable() {

						public void run()
						{
							context.postChiamataProssimaBollettaException(true, parametri, prossimaFatttura, bolletteRateizzabili);

						}
					});
				}
			}
		}, 20000);
			}

	public void executeHttpsGetBollettePagamentiRicerca(String urlString, final HashMap<String, String> parametri, final BollettePagamentiRicerca context,
			final Activity oops, final String numeroFattura) throws Exception
			{
		exception = false;
		final Timer timer = new Timer();
		timeout = false;
		final AsyncHttpClient myClient = new AsyncHttpClient();
		myClient.setTimeout(20000);
		List<Cookie> listaCookie = myCookieStore.getCookies();

//		for (int i = 0; i < listaCookie.size(); i++)
//		{
//			Cookie cookie = listaCookie.get(i);
//
//			BasicClientCookie newCookie = new BasicClientCookie(cookie.getName(), cookie.getValue());
//			newCookie.setVersion(cookie.getVersion());
//			newCookie.setDomain(".eni.mobi");
//			newCookie.setPath(cookie.getPath());
//
//			myCookieStore.addCookie(newCookie);
//		}
		String paramString = hashMapParameterToString(parametri);
		paramString=adjustQueryString(paramString);

		myClient.setCookieStore(myCookieStore);
		try
		{
			KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
			trustStore.load(null, null);
			MySSLSocketFactory sf = new MySSLSocketFactory(trustStore);
			sf.setHostnameVerifier(MySSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
			myClient.setSSLSocketFactory(sf);
		} catch (Exception e)
		{
			throw new Exception(e.getMessage());
		}

		Log.d(SERVICE_CALL_TAG, "GetBollettePagamentiRicerca URL - executeHttpsGetBollettePagamentiRicerca - " + urlString + paramString);
		Log.d(SERVICE_CALL_TAG, "GetBollettePagamentiRicerca Input Params - executeHttpsGetBollettePagamentiRicerca - " + "parametri[" + parametri + "]");

		myClient.get(urlString + paramString, new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(String response)
			{
				if (!context.isFinishing() && !timeout && (context.hasWindowFocus() || (oops != null && oops.hasWindowFocus())))
				{
					List<Cookie> listaCookies = myCookieStore.getCookies();

					Log.d(SERVICE_CALL_TAG, "GetBollettePagamentiRicerca Response - executeHttpsGetBollettePagamentiRicerca - " + response);
					context.postChiamata(response, listaCookies, parametri, numeroFattura);
				}
				timeout = false;
			}

			@Override
			public void onFinish()
			{
				timer.cancel();
				if (oops != null)
				{
					oops.finish();
				}
				super.onFinish();
			}

			@Override
			public void onStart()
			{
			}

			@Override
			protected void handleFailureMessage(Throwable arg0, String arg1)
			{
				// super.handleFailureMessage(arg0, arg1);
				if (!context.isFinishing() && !timeout && (context.hasWindowFocus() || (oops != null && oops.hasWindowFocus())))
				{
					context.postChiamataException(true, parametri, numeroFattura);
				}
				timeout = false;
			}

			@Override
			protected void handleMessage(Message arg0)
			{
				super.handleMessage(arg0);
			}

			@Override
			protected void handleSuccessMessage(String arg0)
			{
				super.handleSuccessMessage(arg0);
			}

			@Override
			protected Message obtainMessage(int arg0, Object arg1)
			{
				return super.obtainMessage(arg0, arg1);
			}

			@Override
			public void onFailure(Throwable arg0, String arg1)
			{
				// myCookieStore.clear();
				// super.onFailure(arg0, arg1);
			}

			@Override
			protected void sendFailureMessage(Throwable arg0, byte[] arg1)
			{
				super.sendFailureMessage(arg0, arg1);
			}

			@Override
			protected void sendFailureMessage(Throwable arg0, String arg1)
			{
				super.sendFailureMessage(arg0, arg1);
			}

			@Override
			protected void sendFinishMessage()
			{
				super.sendFinishMessage();
			}

			@Override
			protected void sendMessage(Message arg0)
			{
				super.sendMessage(arg0);
			}

			@Override
			protected void sendStartMessage()
			{
				super.sendStartMessage();
			}

			@Override
			protected void sendSuccessMessage(String arg0)
			{
				super.sendSuccessMessage(arg0);
			}
		});
		timer.schedule(new TimerTask() {
			@Override
			public void run()
			{
				myClient.cancelRequests(context, true);
				timeout = true;
				if (oops != null)
				{
					oops.finish();
				}
				if (!context.isFinishing() && (context.hasWindowFocus() || (oops != null && oops.hasWindowFocus())))
				{
					context.runOnUiThread(new Runnable() {

						public void run()
						{
							context.postChiamataException(true, parametri, numeroFattura);

						}
					});
				}
			}
		}, 30000);
			}

	public void executeHttpsGetBollettePagamentiRicercaPostIntervallo(String urlString, final HashMap<String, String> parametri,
			final BollettePagamentiRicercaPostIntervallo context, final Activity oops, final boolean isDownload, final String numeroFattura) throws Exception
			{
		exception = false;
		final Timer timer = new Timer();
		timeout = false;
		final AsyncHttpClient myClient = new AsyncHttpClient();
		myClient.setTimeout(20000);
		List<Cookie> listaCookie = myCookieStore.getCookies();

//		for (int i = 0; i < listaCookie.size(); i++)
//		{
//			Cookie cookie = listaCookie.get(i);
//
//			BasicClientCookie newCookie = new BasicClientCookie(cookie.getName(), cookie.getValue());
//			newCookie.setVersion(cookie.getVersion());
//			newCookie.setDomain(".eni.mobi");
//			newCookie.setPath(cookie.getPath());
//
//			myCookieStore.addCookie(newCookie);
//		}
		String paramString = hashMapParameterToString(parametri);
		paramString = adjustQueryString(paramString);

		myClient.setCookieStore(myCookieStore);
		try
		{
			KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
			trustStore.load(null, null);
			MySSLSocketFactory sf = new MySSLSocketFactory(trustStore);
			sf.setHostnameVerifier(MySSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
			myClient.setSSLSocketFactory(sf);
		} catch (Exception e)
		{
			throw new Exception(e.getMessage());
		}

		Log.d(SERVICE_CALL_TAG, "GetBollettePagamentiRicercaPostIntervallo URL - executeHttpsGetBollettePagamentiRicercaPostIntervallo - " + urlString
				+ paramString);
		Log.d(SERVICE_CALL_TAG, "GetBollettePagamentiRicercaPostIntervallo Input Params - executeHttpsGetBollettePagamentiRicercaPostIntervallo - "
				+ "parametri[" + parametri + "]");

		myClient.get(urlString + paramString, new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(String response)
			{
				if (!context.isFinishing() && !timeout && (context.hasWindowFocus() || (oops != null && oops.hasWindowFocus())))
				{
					List<Cookie> listaCookies = myCookieStore.getCookies();

					Log.d(SERVICE_CALL_TAG, "GetBollettePagamentiRicercaPostIntervallo Response - GetBollettePagamentiRicercaPostIntervallo - " + response);
					context.postChiamata(response, listaCookies, parametri, isDownload, numeroFattura);
				}
				timeout = false;
			}

			@Override
			public void onFinish()
			{
				timer.cancel();
				if (oops != null)
				{
					oops.finish();
				}
				super.onFinish();
			}

			@Override
			public void onStart()
			{
			}

			@Override
			protected void handleFailureMessage(Throwable arg0, String arg1)
			{
				// super.handleFailureMessage(arg0, arg1);
				if (!context.isFinishing() && !timeout && (context.hasWindowFocus() || (oops != null && oops.hasWindowFocus())))
				{
					context.postChiamataException(true, parametri, isDownload, numeroFattura);
				}
				timeout = false;
			}

			@Override
			protected void handleMessage(Message arg0)
			{
				super.handleMessage(arg0);
			}

			@Override
			protected void handleSuccessMessage(String arg0)
			{
				super.handleSuccessMessage(arg0);
			}

			@Override
			protected Message obtainMessage(int arg0, Object arg1)
			{
				return super.obtainMessage(arg0, arg1);
			}

			@Override
			public void onFailure(Throwable arg0, String arg1)
			{
				// myCookieStore.clear();
				// super.onFailure(arg0, arg1);
			}

			@Override
			protected void sendFailureMessage(Throwable arg0, byte[] arg1)
			{
				super.sendFailureMessage(arg0, arg1);
			}

			@Override
			protected void sendFailureMessage(Throwable arg0, String arg1)
			{
				super.sendFailureMessage(arg0, arg1);
			}

			@Override
			protected void sendFinishMessage()
			{
				super.sendFinishMessage();
			}

			@Override
			protected void sendMessage(Message arg0)
			{
				super.sendMessage(arg0);
			}

			@Override
			protected void sendStartMessage()
			{
				super.sendStartMessage();
			}

			@Override
			protected void sendSuccessMessage(String arg0)
			{
				super.sendSuccessMessage(arg0);
			}
		});
		timer.schedule(new TimerTask() {
			@Override
			public void run()
			{
				myClient.cancelRequests(context, true);
				timeout = true;
				if (oops != null)
				{
					oops.finish();
				}
				if (!context.isFinishing() && (context.hasWindowFocus() || (oops != null && oops.hasWindowFocus())))
				{
					context.runOnUiThread(new Runnable() {

						public void run()
						{
							context.postChiamataException(true, parametri, isDownload, numeroFattura);

						}
					});
				}
			}
		}, 30000);
			}

	public void executeHttpsGetBollettePagamentiRicercaIntervallo(String urlString, final HashMap<String, String> parametri,
			final BollettePagamentiRicercaIntervallo context, final Activity oops) throws Exception
			{
		exception = false;
		final Timer timer = new Timer();
		timeout = false;
		final AsyncHttpClient myClient = new AsyncHttpClient();
		myClient.setTimeout(20000);
		List<Cookie> listaCookie = myCookieStore.getCookies();

//		for (int i = 0; i < listaCookie.size(); i++)
//		{
//			Cookie cookie = listaCookie.get(i);
//
//			BasicClientCookie newCookie = new BasicClientCookie(cookie.getName(), cookie.getValue());
//			newCookie.setVersion(cookie.getVersion());
//			newCookie.setDomain(".eni.mobi");
//			newCookie.setPath(cookie.getPath());
//
//			myCookieStore.addCookie(newCookie);
//		}
		String paramString = hashMapParameterToString(parametri);
		paramString = adjustQueryString(paramString);

		myClient.setCookieStore(myCookieStore);
		try
		{
			KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
			trustStore.load(null, null);
			MySSLSocketFactory sf = new MySSLSocketFactory(trustStore);
			sf.setHostnameVerifier(MySSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
			myClient.setSSLSocketFactory(sf);
		} catch (Exception e)
		{
			throw new Exception(e.getMessage());
		}

		Log.d(SERVICE_CALL_TAG, "GetBollettePagamentiRicercaIntervallo URL - executeHttpsGetBollettePagamentiRicercaIntervallo - " + urlString + paramString);
		Log.d(SERVICE_CALL_TAG, "GetBollettePagamentiRicercaIntervallo Input Params - executeHttpsGetBollettePagamentiRicercaIntervallo - " + "parametri["
				+ parametri + "]");

		myClient.get(urlString + paramString, new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(String response)
			{
				if (!context.isFinishing() && !timeout && (context.hasWindowFocus() || (oops != null && oops.hasWindowFocus())))
				{
					List<Cookie> listaCookies = myCookieStore.getCookies();

					Log.d(SERVICE_CALL_TAG, "GetBollettePagamentiRicercaIntervallo Response - executeHttpsGetBollettePagamentiRicercaIntervallo - " + response);
					context.postChiamata(response, listaCookies, parametri);
				}
				timeout = false;
			}

			@Override
			public void onFinish()
			{
				timer.cancel();
				if (oops != null)
				{
					oops.finish();
				}
				super.onFinish();
			}

			@Override
			public void onStart()
			{
			}

			@Override
			protected void handleFailureMessage(Throwable arg0, String arg1)
			{
				// super.handleFailureMessage(arg0, arg1);
				if (!context.isFinishing() && !timeout && (context.hasWindowFocus() || (oops != null && oops.hasWindowFocus())))
				{
					context.postChiamataException(true, parametri);
				}
				timeout = false;
			}

			@Override
			protected void handleMessage(Message arg0)
			{
				super.handleMessage(arg0);
			}

			@Override
			protected void handleSuccessMessage(String arg0)
			{
				super.handleSuccessMessage(arg0);
			}

			@Override
			protected Message obtainMessage(int arg0, Object arg1)
			{
				return super.obtainMessage(arg0, arg1);
			}

			@Override
			public void onFailure(Throwable arg0, String arg1)
			{
				// myCookieStore.clear();
				// super.onFailure(arg0, arg1);
			}

			@Override
			protected void sendFailureMessage(Throwable arg0, byte[] arg1)
			{
				super.sendFailureMessage(arg0, arg1);
			}

			@Override
			protected void sendFailureMessage(Throwable arg0, String arg1)
			{
				super.sendFailureMessage(arg0, arg1);
			}

			@Override
			protected void sendFinishMessage()
			{
				super.sendFinishMessage();
			}

			@Override
			protected void sendMessage(Message arg0)
			{
				super.sendMessage(arg0);
			}

			@Override
			protected void sendStartMessage()
			{
				super.sendStartMessage();
			}

			@Override
			protected void sendSuccessMessage(String arg0)
			{
				super.sendSuccessMessage(arg0);
			}
		});
		timer.schedule(new TimerTask() {
			@Override
			public void run()
			{
				myClient.cancelRequests(context, true);
				timeout = true;
				if (oops != null)
				{
					oops.finish();
				}
				if (!context.isFinishing() && (context.hasWindowFocus() || (oops != null && oops.hasWindowFocus())))
				{
					context.runOnUiThread(new Runnable() {

						public void run()
						{
							context.postChiamataException(true, parametri);

						}
					});
				}
			}
		}, 20000);
			}

	public void executeHttpsGetProssimaBolletta(String urlString, final String parametri, final BollettePagamentiProssimaBolletta context, final Activity oops)
			throws Exception
			{
		exception = false;
		final Timer timer = new Timer();
		timeout = false;
		final AsyncHttpClient myClient = new AsyncHttpClient();
		myClient.setTimeout(20000);

		final String parametriAdjusted = adjustQueryString(parametri);

		List<Cookie> listaCookie = myCookieStore.getCookies();

//		for (int i = 0; i < listaCookie.size(); i++)
//		{
//			Cookie cookie = listaCookie.get(i);
//
//			BasicClientCookie newCookie = new BasicClientCookie(cookie.getName(), cookie.getValue());
//			newCookie.setVersion(cookie.getVersion());
//			newCookie.setDomain(".eni.mobi");
//			newCookie.setPath(cookie.getPath());
//
//			myCookieStore.addCookie(newCookie);
//		}
		myClient.setCookieStore(myCookieStore);
		try
		{
			KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
			trustStore.load(null, null);
			MySSLSocketFactory sf = new MySSLSocketFactory(trustStore);
			sf.setHostnameVerifier(MySSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
			myClient.setSSLSocketFactory(sf);
		} catch (Exception e)
		{
			throw new Exception(e.getMessage());
		}

		Log.d(SERVICE_CALL_TAG, "GetProssimaBolletta URL - executeHttpsGetProssimaBolletta - " + urlString + parametriAdjusted);
		Log.d(SERVICE_CALL_TAG, "GetProssimaBolletta Input Params - executeHttpsGetProssimaBolletta - " + "parametri[" + parametriAdjusted + "]");

		myClient.get(urlString + parametriAdjusted, new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(String response)
			{
				if ((context != null && !context.isFinishing()) && !timeout
						&& ((context != null && context.hasWindowFocus()) || (oops != null && oops.hasWindowFocus())))
				{
					List<Cookie> listaCookies = myCookieStore.getCookies();

					HashMap<String, String> parameter = new HashMap<String, String>();
					parameter.put("PARAMETER", parametriAdjusted);

					Log.d(SERVICE_CALL_TAG, "GetProssimaBolletta Response - executeHttpsGetProssimaBolletta - " + response);
					context.postChiamata(response, listaCookies, parameter);
				}
				timeout = false;
			}

			@Override
			public void onFinish()
			{
				timer.cancel();
				if (oops != null)
				{
					oops.finish();
				}
				super.onFinish();
			}

			@Override
			public void onStart()
			{
			}

			@Override
			protected void handleFailureMessage(Throwable arg0, String arg1)
			{
				// super.handleFailureMessage(arg0, arg1);
				if (!context.isFinishing() && !timeout && (context.hasWindowFocus() || (oops != null && oops.hasWindowFocus())))
				{
					context.postChiamataException(true, parametri, null);
				}
				timeout = false;
			}

			@Override
			protected void handleMessage(Message arg0)
			{
				super.handleMessage(arg0);
			}

			@Override
			protected void handleSuccessMessage(String arg0)
			{
				super.handleSuccessMessage(arg0);
			}

			@Override
			protected Message obtainMessage(int arg0, Object arg1)
			{
				return super.obtainMessage(arg0, arg1);
			}

			@Override
			public void onFailure(Throwable arg0, String arg1)
			{
				myCookieStore.clear();
				// super.onFailure(arg0, arg1);
			}

			@Override
			protected void sendFailureMessage(Throwable arg0, byte[] arg1)
			{
				super.sendFailureMessage(arg0, arg1);
			}

			@Override
			protected void sendFailureMessage(Throwable arg0, String arg1)
			{
				super.sendFailureMessage(arg0, arg1);
			}

			@Override
			protected void sendFinishMessage()
			{
				super.sendFinishMessage();
			}

			@Override
			protected void sendMessage(Message arg0)
			{
				super.sendMessage(arg0);
			}

			@Override
			protected void sendStartMessage()
			{
				super.sendStartMessage();
			}

			@Override
			protected void sendSuccessMessage(String arg0)
			{
				super.sendSuccessMessage(arg0);
			}
		});
		timer.schedule(new TimerTask() {
			@Override
			public void run()
			{
				myClient.cancelRequests(context, true);
				timeout = true;
				if (oops != null)
				{
					oops.finish();
				}
				if ((context != null && !context.isFinishing()) && ((context != null && context.hasWindowFocus()) || (oops != null && oops.hasWindowFocus())))
				{
					context.runOnUiThread(new Runnable() {

						public void run()
						{
							context.postChiamataException(true, parametri, null);

						}
					});
				}
			}
		}, 20000);
			}

	public void executeHttpsGetBollettePagamentiCalcolaPropostaRate(String urlString, final HashMap<String, String> parametri,
			final HashMap<String, String> parametriSecondario, final BollettePagamentiRateizzate context, final Activity oops, final int tipoProposta)
					throws Exception
					{
		exception = false;
		final Timer timer = new Timer();
		timeout = false;
		final AsyncHttpClient myClient = new AsyncHttpClient();
		myClient.setTimeout(20000);
		List<Cookie> listaCookie = myCookieStore.getCookies();

//		for (int i = 0; i < listaCookie.size(); i++)
//		{
//			Cookie cookie = listaCookie.get(i);
//
//			BasicClientCookie newCookie = new BasicClientCookie(cookie.getName(), cookie.getValue());
//			newCookie.setVersion(cookie.getVersion());
//			newCookie.setDomain(".eni.mobi");
//			newCookie.setPath(cookie.getPath());
//
//			myCookieStore.addCookie(newCookie);
//		}
		String paramString = hashMapParameterToString(parametri);
		paramString = adjustQueryString(paramString);

		myClient.setCookieStore(myCookieStore);
		try
		{
			KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
			trustStore.load(null, null);
			MySSLSocketFactory sf = new MySSLSocketFactory(trustStore);
			sf.setHostnameVerifier(MySSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
			myClient.setSSLSocketFactory(sf);
		} catch (Exception e)
		{
			throw new Exception(e.getMessage());
		}

		Log.d(SERVICE_CALL_TAG, "GetBollettePagamentiCalcolaPropostaRate URL - executeHttpsGetBollettePagamentiCalcolaPropostaRate - " + urlString + parametri);
		Log.d(SERVICE_CALL_TAG, "GetBollettePagamentiCalcolaPropostaRate Input Params - executeHttpsGetBollettePagamentiCalcolaPropostaRate - " + "parametri["
				+ parametri + "]");

		myClient.get(urlString + paramString, new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(String response)
			{
				if (!context.isFinishing() && !timeout && (context.hasWindowFocus() || (oops != null && oops.hasWindowFocus())))
				{
					List<Cookie> listaCookies = myCookieStore.getCookies();

					Log.d(SERVICE_CALL_TAG, "GetBollettePagamentiCalcolaPropostaRate Response - executeHttpsGetBollettePagamentiCalcolaPropostaRate - "
							+ response);
					context.postChiamata(response, listaCookies, parametri, parametriSecondario, tipoProposta);
				}
				timeout = false;
			}

			@Override
			public void onFinish()
			{
				timer.cancel();
				if (oops != null)
				{
					oops.finish();
				}
				super.onFinish();
			}

			@Override
			public void onStart()
			{
			}

			@Override
			protected void handleFailureMessage(Throwable arg0, String arg1)
			{
				// super.handleFailureMessage(arg0, arg1);
				if (!context.isFinishing() && !timeout && (context.hasWindowFocus() || (oops != null && oops.hasWindowFocus())))
				{
					context.postChiamataException(true, parametri, parametriSecondario, tipoProposta);
				}
				timeout = false;
			}

			@Override
			protected void handleMessage(Message arg0)
			{
				super.handleMessage(arg0);
			}

			@Override
			protected void handleSuccessMessage(String arg0)
			{
				super.handleSuccessMessage(arg0);
			}

			@Override
			protected Message obtainMessage(int arg0, Object arg1)
			{
				return super.obtainMessage(arg0, arg1);
			}

			@Override
			public void onFailure(Throwable arg0, String arg1)
			{
				// myCookieStore.clear();
				// super.onFailure(arg0, arg1);
			}

			@Override
			protected void sendFailureMessage(Throwable arg0, byte[] arg1)
			{
				super.sendFailureMessage(arg0, arg1);
			}

			@Override
			protected void sendFailureMessage(Throwable arg0, String arg1)
			{
				super.sendFailureMessage(arg0, arg1);
			}

			@Override
			protected void sendFinishMessage()
			{
				super.sendFinishMessage();
			}

			@Override
			protected void sendMessage(Message arg0)
			{
				super.sendMessage(arg0);
			}

			@Override
			protected void sendStartMessage()
			{
				super.sendStartMessage();
			}

			@Override
			protected void sendSuccessMessage(String arg0)
			{
				super.sendSuccessMessage(arg0);
			}
		});
		timer.schedule(new TimerTask() {
			@Override
			public void run()
			{
				myClient.cancelRequests(context, true);
				timeout = true;
				if (oops != null)
				{
					oops.finish();
				}
				if (!context.isFinishing() && (context.hasWindowFocus() || (oops != null && oops.hasWindowFocus())))
				{
					context.runOnUiThread(new Runnable() {

						public void run()
						{
							context.postChiamataException(true, parametri, parametriSecondario, tipoProposta);

						}
					});
				}
			}
		}, 20000);
					}

	public void executeHttpsGetBollettePagamentiCalcolaPropostaRateDettaglio(String urlString, final HashMap<String, String> parametri,
			final BollettePagamentiDettaglioPiano context, final Activity oops, final int tipoProposta, final boolean reset) throws Exception
			{
		exception = false;
		final Timer timer = new Timer();
		timeout = false;
		final AsyncHttpClient myClient = new AsyncHttpClient();
		myClient.setTimeout(20000);
		List<Cookie> listaCookie = myCookieStore.getCookies();

//		for (int i = 0; i < listaCookie.size(); i++)
//		{
//			Cookie cookie = listaCookie.get(i);
//
//			BasicClientCookie newCookie = new BasicClientCookie(cookie.getName(), cookie.getValue());
//			newCookie.setVersion(cookie.getVersion());
//			newCookie.setDomain(".eni.mobi");
//			newCookie.setPath(cookie.getPath());
//
//			myCookieStore.addCookie(newCookie);
//		}
		String paramString = hashMapParameterToString(parametri);
		paramString = adjustQueryString(paramString);

		myClient.setCookieStore(myCookieStore);
		try
		{
			KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
			trustStore.load(null, null);
			MySSLSocketFactory sf = new MySSLSocketFactory(trustStore);
			sf.setHostnameVerifier(MySSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
			myClient.setSSLSocketFactory(sf);
		} catch (Exception e)
		{
			throw new Exception(e.getMessage());
		}

		Log.d(SERVICE_CALL_TAG, "GetBollettePagamentiCalcolaPropostaRateDettaglio URL - executeHttpsGetBollettePagamentiCalcolaPropostaRateDettaglio - "
				+ urlString + paramString);
		Log.d(SERVICE_CALL_TAG,
				"GetBollettePagamentiCalcolaPropostaRateDettaglio Input Params - executeHttpsGetBollettePagamentiCalcolaPropostaRateDettaglio - "
						+ "parametri[" + parametri + "]");

		myClient.get(urlString + paramString, new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(String response)
			{
				if (!context.isFinishing() && !timeout && (context.hasWindowFocus() || (oops != null && oops.hasWindowFocus())))
				{
					List<Cookie> listaCookies = myCookieStore.getCookies();

					Log.d(SERVICE_CALL_TAG,
							"GetBollettePagamentiCalcolaPropostaRateDettaglio Response - executeHttpsGetBollettePagamentiCalcolaPropostaRateDettaglio - "
									+ response);
					context.postChiamata(response, listaCookies, parametri, tipoProposta, "MODIFICA", reset);
				}
				timeout = false;
			}

			@Override
			public void onFinish()
			{
				timer.cancel();
				if (oops != null)
				{
					oops.finish();
				}
				super.onFinish();
			}

			@Override
			public void onStart()
			{
			}

			@Override
			protected void handleFailureMessage(Throwable arg0, String arg1)
			{
				// super.handleFailureMessage(arg0, arg1);
				if (!context.isFinishing() && !timeout && (context.hasWindowFocus() || (oops != null && oops.hasWindowFocus())))
				{
					context.postChiamataException(true, parametri, tipoProposta, "MODIFICA", reset);
				}
				timeout = false;
			}

			@Override
			protected void handleMessage(Message arg0)
			{
				super.handleMessage(arg0);
			}

			@Override
			protected void handleSuccessMessage(String arg0)
			{
				super.handleSuccessMessage(arg0);
			}

			@Override
			protected Message obtainMessage(int arg0, Object arg1)
			{
				return super.obtainMessage(arg0, arg1);
			}

			@Override
			public void onFailure(Throwable arg0, String arg1)
			{
				// myCookieStore.clear();
				// super.onFailure(arg0, arg1);
			}

			@Override
			protected void sendFailureMessage(Throwable arg0, byte[] arg1)
			{
				super.sendFailureMessage(arg0, arg1);
			}

			@Override
			protected void sendFailureMessage(Throwable arg0, String arg1)
			{
				super.sendFailureMessage(arg0, arg1);
			}

			@Override
			protected void sendFinishMessage()
			{
				super.sendFinishMessage();
			}

			@Override
			protected void sendMessage(Message arg0)
			{
				super.sendMessage(arg0);
			}

			@Override
			protected void sendStartMessage()
			{
				super.sendStartMessage();
			}

			@Override
			protected void sendSuccessMessage(String arg0)
			{
				super.sendSuccessMessage(arg0);
			}
		});
		timer.schedule(new TimerTask() {
			@Override
			public void run()
			{
				myClient.cancelRequests(context, true);
				timeout = true;
				if (oops != null)
				{
					oops.finish();
				}
				if (!context.isFinishing() && (context.hasWindowFocus() || (oops != null && oops.hasWindowFocus())))
				{
					context.runOnUiThread(new Runnable() {

						public void run()
						{
							context.postChiamataException(true, parametri, tipoProposta, "MODIFICA", reset);
						}
					});
				}
			}
		}, 20000);
			}

	/*
	 * public void
	 * executeHttpsGetBollettePagamentiCalcolaPropostaRateScelta(String
	 * urlString, final HashMap<String,String> parametri, final
	 * BollettePagamentiSceltaPiano context, final Activity oops) throws
	 * Exception { exception = false; final Timer timer = new Timer();
	 * timeout=false; final AsyncHttpClient myClient = new AsyncHttpClient();
	 * myClient.setTimeout(20000); List<Cookie> listaCookie =
	 * myCookieStore.getCookies();
	 * 
	 * for (int i = 0; i < listaCookie.size(); i++) { Cookie cookie =
	 * listaCookie.get(i);
	 * 
	 * BasicClientCookie newCookie = new BasicClientCookie(cookie.getName(),
	 * cookie.getValue()); newCookie.setVersion(cookie.getVersion());
	 * newCookie.setDomain(".eni.mobi"); newCookie.setPath(cookie.getPath());
	 * 
	 * myCookieStore.addCookie(newCookie); } String paramString =
	 * hashMapParameterToString(parametri);
	 * myClient.setCookieStore(myCookieStore); try { KeyStore trustStore =
	 * KeyStore.getInstance(KeyStore.getDefaultType()); trustStore.load(null,
	 * null); MySSLSocketFactory sf = new MySSLSocketFactory(trustStore);
	 * sf.setHostnameVerifier(MySSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
	 * myClient.setSSLSocketFactory(sf); } catch (Exception e) { throw new
	 * Exception(e.getMessage()); }
	 * 
	 * myClient.get(urlString + paramString, new AsyncHttpResponseHandler() {
	 * 
	 * @Override public void onSuccess(String response) {
	 * if(!context.isFinishing() && !timeout && (context.hasWindowFocus() ||
	 * (oops != null && oops.hasWindowFocus()))) { List<Cookie> listaCookies =
	 * myCookieStore.getCookies(); context.postChiamata(response, listaCookies,
	 * parametri); } timeout=false; }
	 * 
	 * @Override public void onFinish() { timer.cancel(); if(oops != null){
	 * oops.finish(); } super.onFinish(); }
	 * 
	 * @Override public void onStart() { }
	 * 
	 * @Override protected void handleFailureMessage(Throwable arg0, String
	 * arg1) { // super.handleFailureMessage(arg0, arg1);
	 * if(!context.isFinishing() && !timeout && (context.hasWindowFocus() ||
	 * (oops != null && oops.hasWindowFocus()))) {
	 * context.postChiamataException(true, parametri); } timeout=false; }
	 * 
	 * @Override protected void handleMessage(Message arg0) {
	 * super.handleMessage(arg0); }
	 * 
	 * @Override protected void handleSuccessMessage(String arg0) { }
	 * 
	 * @Override protected Message obtainMessage(int arg0, Object arg1) { return
	 * super.obtainMessage(arg0, arg1); }
	 * 
	 * @Override public void onFailure(Throwable arg0, String arg1) { //
	 * myCookieStore.clear(); // super.onFailure(arg0, arg1); }
	 * 
	 * @Override protected void sendFailureMessage(Throwable arg0, byte[] arg1)
	 * { super.sendFailureMessage(arg0, arg1); }
	 * 
	 * @Override protected void sendFailureMessage(Throwable arg0, String arg1)
	 * { super.sendFailureMessage(arg0, arg1); }
	 * 
	 * @Override protected void sendFinishMessage() { super.sendFinishMessage();
	 * }
	 * 
	 * @Override protected void sendMessage(Message arg0) {
	 * super.sendMessage(arg0); }
	 * 
	 * @Override protected void sendStartMessage() { super.sendStartMessage(); }
	 * 
	 * @Override protected void sendSuccessMessage(String arg0) {
	 * super.sendSuccessMessage(arg0); } }); timer.schedule(new TimerTask(){
	 * 
	 * @Override public void run() { myClient.cancelRequests(context, true);
	 * timeout=true; if(oops != null){ oops.finish(); }
	 * if(!context.isFinishing() && (context.hasWindowFocus() || (oops != null
	 * && oops.hasWindowFocus()))) { context.runOnUiThread(new Runnable() {
	 * 
	 * public void run() { context.postChiamataException(true, parametri); } });
	 * } } }, 20000); }
	 */

	public void executeHttpsGetBollettePagamentiAttivaPropostaPiano(String urlString, final HashMap<String, String> parametri,
			final BollettePagamentiDettaglioPiano context, final Activity oops, final String accettaOModifica) throws Exception
			{
		exception = false;
		final Timer timer = new Timer();
		timeout = false;
		final AsyncHttpClient myClient = new AsyncHttpClient();
		myClient.setTimeout(20000);
		List<Cookie> listaCookie = myCookieStore.getCookies();

//		for (int i = 0; i < listaCookie.size(); i++)
//		{
//			Cookie cookie = listaCookie.get(i);
//
//			BasicClientCookie newCookie = new BasicClientCookie(cookie.getName(), cookie.getValue());
//			newCookie.setVersion(cookie.getVersion());
//			newCookie.setDomain(".eni.mobi");
//			newCookie.setPath(cookie.getPath());
//
//			myCookieStore.addCookie(newCookie);
//		}
		String paramString = hashMapParameterToString(parametri);
		paramString = adjustQueryString(paramString);

		myClient.setCookieStore(myCookieStore);
		try
		{
			KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
			trustStore.load(null, null);
			MySSLSocketFactory sf = new MySSLSocketFactory(trustStore);
			sf.setHostnameVerifier(MySSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
			myClient.setSSLSocketFactory(sf);
		} catch (Exception e)
		{
			throw new Exception(e.getMessage());
		}

		Log.d(SERVICE_CALL_TAG, "GetBollettePagamentiAttivaPropostaPiano URL - executeHttpsGetBollettePagamentiAttivaPropostaPiano - " + urlString
				+ paramString);
		Log.d(SERVICE_CALL_TAG, "GetBollettePagamentiAttivaPropostaPiano Input Params - executeHttpsGetBollettePagamentiAttivaPropostaPiano - " + "parametri["
				+ parametri + "]");

		myClient.get(urlString + paramString, new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(String response)
			{
				if (!context.isFinishing() && !timeout && (context.hasWindowFocus() || (oops != null && oops.hasWindowFocus())))
				{
					List<Cookie> listaCookies = myCookieStore.getCookies();

					Log.d(SERVICE_CALL_TAG, "GetBollettePagamentiAttivaPropostaPiano Response - executeHttpsGetBollettePagamentiAttivaPropostaPiano - "
							+ response);
					context.postChiamata(response, listaCookies, parametri, 0, accettaOModifica, false);
				}
				timeout = false;
			}

			@Override
			public void onFinish()
			{
				timer.cancel();
				if (oops != null)
				{
					oops.finish();
				}
				super.onFinish();
			}

			@Override
			public void onStart()
			{
			}

			@Override
			protected void handleFailureMessage(Throwable arg0, String arg1)
			{
				// super.handleFailureMessage(arg0, arg1);
				if (!context.isFinishing() && !timeout && (context.hasWindowFocus() || (oops != null && oops.hasWindowFocus())))
				{
					context.postChiamataException(true, parametri, 0, accettaOModifica, false);
				}
				timeout = false;
			}

			@Override
			protected void handleMessage(Message arg0)
			{
				super.handleMessage(arg0);
			}

			@Override
			protected void handleSuccessMessage(String arg0)
			{
				super.handleSuccessMessage(arg0);
			}

			@Override
			protected Message obtainMessage(int arg0, Object arg1)
			{
				return super.obtainMessage(arg0, arg1);
			}

			@Override
			public void onFailure(Throwable arg0, String arg1)
			{
				// myCookieStore.clear();
				// super.onFailure(arg0, arg1);
			}

			@Override
			protected void sendFailureMessage(Throwable arg0, byte[] arg1)
			{
				super.sendFailureMessage(arg0, arg1);
			}

			@Override
			protected void sendFailureMessage(Throwable arg0, String arg1)
			{
				super.sendFailureMessage(arg0, arg1);
			}

			@Override
			protected void sendFinishMessage()
			{
				super.sendFinishMessage();
			}

			@Override
			protected void sendMessage(Message arg0)
			{
				super.sendMessage(arg0);
			}

			@Override
			protected void sendStartMessage()
			{
				super.sendStartMessage();
			}

			@Override
			protected void sendSuccessMessage(String arg0)
			{
				super.sendSuccessMessage(arg0);
			}
		});
		timer.schedule(new TimerTask() {
			@Override
			public void run()
			{
				myClient.cancelRequests(context, true);
				timeout = true;
				if (oops != null)
				{
					oops.finish();
				}
				if (!context.isFinishing() && (context.hasWindowFocus() || (oops != null && oops.hasWindowFocus())))
				{
					context.runOnUiThread(new Runnable() {

						public void run()
						{
							context.postChiamataException(true, parametri, 0, accettaOModifica, false);

						}
					});
				}
			}
		}, 20000);
			}

	public void executeHttpsGetWebollettaActivateDom(String urlString, final String parametri, final WebollettaActivate context, final Dialog dialog,
			final Activity oops) throws Exception
			{
		exception = false;
		final Timer timer = new Timer();
		timeout = false;
		final AsyncHttpClient myClient = new AsyncHttpClient();

		String parametriAdjusted = adjustQueryString(parametri);

		myClient.setTimeout(20000);
		List<Cookie> listaCookie = myCookieStore.getCookies();

//		for (int i = 0; i < listaCookie.size(); i++)
//		{
//			Cookie cookie = listaCookie.get(i);
//
//			BasicClientCookie newCookie = new BasicClientCookie(cookie.getName(), cookie.getValue());
//			newCookie.setVersion(cookie.getVersion());
//			newCookie.setDomain(".eni.mobi");
//			newCookie.setPath(cookie.getPath());
//
//			myCookieStore.addCookie(newCookie);
//		}
		myClient.setCookieStore(myCookieStore);
		try
		{
			KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
			trustStore.load(null, null);
			MySSLSocketFactory sf = new MySSLSocketFactory(trustStore);
			sf.setHostnameVerifier(MySSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
			myClient.setSSLSocketFactory(sf);
		} catch (Exception e)
		{
			throw new Exception(e.getMessage());
		}

		Log.d(SERVICE_CALL_TAG, "GetWebollettaActivateDom URL - executeHttpsGetWebollettaActivateDom - " + urlString + parametriAdjusted);
		Log.d(SERVICE_CALL_TAG, "GetWebollettaActivateDom Input Params - executeHttpsGetWebollettaActivateDom - " + "parametri[" + parametriAdjusted + "]");

		myClient.get(urlString + parametriAdjusted, new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(String response)
			{
				if (!context.isFinishing() && !timeout)
				{
					List<Cookie> listaCookies = myCookieStore.getCookies();

					HashMap<String, String> parameter = new HashMap<String, String>();
					parameter.put("PARAMETER", parametri);

					Log.d(SERVICE_CALL_TAG, "GetWebollettaActivateDom Response - executeHttpsGetWebollettaActivateDom - " + response);
					context.postChiamata(response, listaCookies, parameter, dialog, false);
				}
				timeout = false;
			}

			@Override
			public void onFinish()
			{
				timer.cancel();
				if (oops != null)
				{
					oops.finish();
				}
				super.onFinish();
			}

			@Override
			public void onStart()
			{
			}

			@Override
			protected void handleFailureMessage(Throwable arg0, String arg1)
			{
				// super.handleFailureMessage(arg0, arg1);
				if (!context.isFinishing() && !timeout)
				{
					context.postChiamataException(true, parametri, false);
				}
				timeout = false;
			}

			@Override
			protected void handleMessage(Message arg0)
			{
				super.handleMessage(arg0);
			}

			@Override
			protected void handleSuccessMessage(String arg0)
			{
				super.handleSuccessMessage(arg0);
			}

			@Override
			protected Message obtainMessage(int arg0, Object arg1)
			{
				return super.obtainMessage(arg0, arg1);
			}

			@Override
			public void onFailure(Throwable arg0, String arg1)
			{
				myCookieStore.clear();
				// super.onFailure(arg0, arg1);
			}

			@Override
			protected void sendFailureMessage(Throwable arg0, byte[] arg1)
			{
				super.sendFailureMessage(arg0, arg1);
			}

			@Override
			protected void sendFailureMessage(Throwable arg0, String arg1)
			{
				super.sendFailureMessage(arg0, arg1);
			}

			@Override
			protected void sendFinishMessage()
			{
				super.sendFinishMessage();
			}

			@Override
			protected void sendMessage(Message arg0)
			{
				super.sendMessage(arg0);
			}

			@Override
			protected void sendStartMessage()
			{
				super.sendStartMessage();
			}

			@Override
			protected void sendSuccessMessage(String arg0)
			{
				super.sendSuccessMessage(arg0);
			}
		});
		timer.schedule(new TimerTask() {
			@Override
			public void run()
			{
				myClient.cancelRequests(context, true);
				timeout = true;
				if (oops != null)
				{
					oops.finish();
				}
				if (!context.isFinishing())
				{
					context.runOnUiThread(new Runnable() {

						public void run()
						{
							context.postChiamataException(true, parametri, false);

						}
					});
				}
			}
		}, 20000);
			}

	public void executeHttpsGetWebollettaActivatePreconsenso(String urlString, final String parametri, final WebollettaActivate context, final Activity oops)
			throws Exception
			{
		exception = false;
		final Timer timer = new Timer();
		timeout = false;
		final AsyncHttpClient myClient = new AsyncHttpClient();
		myClient.setTimeout(20000);
		String parametriAdjusted = adjustQueryString(parametri);

		List<Cookie> listaCookie = myCookieStore.getCookies();

//		for (int i = 0; i < listaCookie.size(); i++)
//		{
//			Cookie cookie = listaCookie.get(i);
//
//			BasicClientCookie newCookie = new BasicClientCookie(cookie.getName(), cookie.getValue());
//			newCookie.setVersion(cookie.getVersion());
//			newCookie.setDomain(".eni.mobi");
//			newCookie.setPath(cookie.getPath());
//
//			myCookieStore.addCookie(newCookie);
//		}
		myClient.setCookieStore(myCookieStore);
		try
		{
			KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
			trustStore.load(null, null);
			MySSLSocketFactory sf = new MySSLSocketFactory(trustStore);
			sf.setHostnameVerifier(MySSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
			myClient.setSSLSocketFactory(sf);
		} catch (Exception e)
		{
			throw new Exception(e.getMessage());
		}

		Log.d(SERVICE_CALL_TAG, "GetWebollettaActivatePreconsenso URL - executeHttpsGetWebollettaActivatePreconsenso - " + urlString + parametriAdjusted);
		Log.d(SERVICE_CALL_TAG, "GetWebollettaActivatePreconsenso Input Params - executeHttpsGetWebollettaActivatePreconsenso - " + "parametri[" + parametri
				+ "]");

		myClient.get(urlString + parametriAdjusted, new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(String response)
			{
				if (!context.isFinishing() && !timeout && (context.hasWindowFocus() || (oops != null && oops.hasWindowFocus())))
				{
					List<Cookie> listaCookies = myCookieStore.getCookies();

					HashMap<String, String> parameter = new HashMap<String, String>();
					parameter.put("PARAMETER", parametri);

					Log.d(SERVICE_CALL_TAG, "GetWebollettaActivatePreconsenso Response - executeHttpsGetWebollettaActivatePreconsenso - " + response);
					context.postChiamata(response, listaCookies, parameter, null, true);
				}
				timeout = false;
			}

			@Override
			public void onFinish()
			{
				timer.cancel();
				if (oops != null)
				{
					oops.finish();
				}
				super.onFinish();
			}

			@Override
			public void onStart()
			{
			}

			@Override
			protected void handleFailureMessage(Throwable arg0, String arg1)
			{
				// super.handleFailureMessage(arg0, arg1);
				if (!context.isFinishing() && !timeout && (context.hasWindowFocus() || (oops != null && oops.hasWindowFocus())))
				{
					context.postChiamataException(true, parametri, true);
				}
				timeout = false;
			}

			@Override
			protected void handleMessage(Message arg0)
			{
				super.handleMessage(arg0);
			}

			@Override
			protected void handleSuccessMessage(String arg0)
			{
				super.handleSuccessMessage(arg0);
			}

			@Override
			protected Message obtainMessage(int arg0, Object arg1)
			{
				return super.obtainMessage(arg0, arg1);
			}

			@Override
			public void onFailure(Throwable arg0, String arg1)
			{
				myCookieStore.clear();
				// super.onFailure(arg0, arg1);
			}

			@Override
			protected void sendFailureMessage(Throwable arg0, byte[] arg1)
			{
				super.sendFailureMessage(arg0, arg1);
			}

			@Override
			protected void sendFailureMessage(Throwable arg0, String arg1)
			{
				super.sendFailureMessage(arg0, arg1);
			}

			@Override
			protected void sendFinishMessage()
			{
				super.sendFinishMessage();
			}

			@Override
			protected void sendMessage(Message arg0)
			{
				super.sendMessage(arg0);
			}

			@Override
			protected void sendStartMessage()
			{
				super.sendStartMessage();
			}

			@Override
			protected void sendSuccessMessage(String arg0)
			{
				super.sendSuccessMessage(arg0);
			}
		});
		timer.schedule(new TimerTask() {
			@Override
			public void run()
			{
				myClient.cancelRequests(context, true);
				timeout = true;
				if (oops != null)
				{
					oops.finish();
				}
				if (!context.isFinishing() && (context.hasWindowFocus() || (oops != null && oops.hasWindowFocus())))
				{
					context.runOnUiThread(new Runnable() {

						public void run()
						{
							context.postChiamataException(true, parametri, true);

						}
					});
				}
			}
		}, 20000);
			}

	public void executeHttpsGetWebollettaAttivazioneNotifica(String urlString, final String parametri, final WebollettaAttivazioneNotifica context,
			final Activity oops, final boolean notifica, final boolean aggiorna) throws Exception
			{
		exception = false;
		final Timer timer = new Timer();
		timeout = false;
		final AsyncHttpClient myClient = new AsyncHttpClient();
		myClient.setTimeout(20000);

		String parametriAdjusted = adjustQueryString(parametri);

		List<Cookie> listaCookie = myCookieStore.getCookies();

//		for (int i = 0; i < listaCookie.size(); i++)
//		{
//			Cookie cookie = listaCookie.get(i);
//
//			BasicClientCookie newCookie = new BasicClientCookie(cookie.getName(), cookie.getValue());
//			newCookie.setVersion(cookie.getVersion());
//			newCookie.setDomain(".eni.mobi");
//			newCookie.setPath(cookie.getPath());
//
//			myCookieStore.addCookie(newCookie);
//		}
		myClient.setCookieStore(myCookieStore);
		try
		{
			KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
			trustStore.load(null, null);
			MySSLSocketFactory sf = new MySSLSocketFactory(trustStore);
			sf.setHostnameVerifier(MySSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
			myClient.setSSLSocketFactory(sf);
		} catch (Exception e)
		{
			throw new Exception(e.getMessage());
		}

		Log.d(SERVICE_CALL_TAG, "GetWebollettaAttivazioneNotifica URL - executeHttpsGetWebollettaAttivazioneNotifica - " + urlString + parametri);
		Log.d(SERVICE_CALL_TAG, "GetWebollettaAttivazioneNotifica Input Params - executeHttpsGetWebollettaAttivazioneNotifica - " + "parametri[" + parametri
				+ "]");

		myClient.get(urlString + parametriAdjusted, new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(String response)
			{
				if (!context.isFinishing() && !timeout && (context.hasWindowFocus() || (oops != null && oops.hasWindowFocus())))
				{
					List<Cookie> listaCookies = myCookieStore.getCookies();

					HashMap<String, String> parameter = new HashMap<String, String>();
					parameter.put("PARAMETER", parametri);

					Log.d(SERVICE_CALL_TAG, "GetWebollettaAttivazioneNotifica Response - executeHttpsGetWebollettaAttivazioneNotifica - " + response);
					context.postChiamata(response, listaCookies, parameter, notifica, aggiorna);
				}
				timeout = false;
			}

			@Override
			public void onFinish()
			{
				timer.cancel();
				if (oops != null)
				{
					oops.finish();
				}
				super.onFinish();
			}

			@Override
			public void onStart()
			{
			}

			@Override
			protected void handleFailureMessage(Throwable arg0, String arg1)
			{
				// super.handleFailureMessage(arg0, arg1);
				if (!context.isFinishing() && !timeout && (context.hasWindowFocus() || (oops != null && oops.hasWindowFocus())))
				{
					context.postChiamataException(true, parametri, notifica, aggiorna);
				}
				timeout = false;
			}

			@Override
			protected void handleMessage(Message arg0)
			{
				super.handleMessage(arg0);
			}

			@Override
			protected void handleSuccessMessage(String arg0)
			{
				super.handleSuccessMessage(arg0);
			}

			@Override
			protected Message obtainMessage(int arg0, Object arg1)
			{
				return super.obtainMessage(arg0, arg1);
			}

			@Override
			public void onFailure(Throwable arg0, String arg1)
			{
				myCookieStore.clear();
				// super.onFailure(arg0, arg1);
			}

			@Override
			protected void sendFailureMessage(Throwable arg0, byte[] arg1)
			{
				super.sendFailureMessage(arg0, arg1);
			}

			@Override
			protected void sendFailureMessage(Throwable arg0, String arg1)
			{
				super.sendFailureMessage(arg0, arg1);
			}

			@Override
			protected void sendFinishMessage()
			{
				super.sendFinishMessage();
			}

			@Override
			protected void sendMessage(Message arg0)
			{
				super.sendMessage(arg0);
			}

			@Override
			protected void sendStartMessage()
			{
				super.sendStartMessage();
			}

			@Override
			protected void sendSuccessMessage(String arg0)
			{
				super.sendSuccessMessage(arg0);
			}
		});
		timer.schedule(new TimerTask() {
			@Override
			public void run()
			{
				myClient.cancelRequests(context, true);
				timeout = true;
				if (oops != null)
				{
					oops.finish();
				}
				if (!context.isFinishing() && (context.hasWindowFocus() || (oops != null && oops.hasWindowFocus())))
				{
					context.runOnUiThread(new Runnable() {

						public void run()
						{
							context.postChiamataException(true, parametri, notifica, aggiorna);

						}
					});
				}
			}
		}, 20000);
			}

	public void executeHttpsGetWebollettaAttivazioneNotifica(String urlString, final String parametri, final BollettaDigitaleAttivazioneNotifica context,
															 final Activity oops, final boolean notifica, final boolean aggiorna) throws Exception
	{
		exception = false;
		final Timer timer = new Timer();
		timeout = false;
		final AsyncHttpClient myClient = new AsyncHttpClient();
		myClient.setTimeout(20000);
		String parametriAdjusted = adjustQueryString(parametri);
		List<Cookie> listaCookie = myCookieStore.getCookies();

//		for (int i = 0; i < listaCookie.size(); i++)
//		{
//			Cookie cookie = listaCookie.get(i);
//
//			BasicClientCookie newCookie = new BasicClientCookie(cookie.getName(), cookie.getValue());
//			newCookie.setVersion(cookie.getVersion());
//			newCookie.setDomain(".eni.mobi");
//			newCookie.setPath(cookie.getPath());
//
//			myCookieStore.addCookie(newCookie);
//		}
		myClient.setCookieStore(myCookieStore);
		try
		{
			KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
			trustStore.load(null, null);
			MySSLSocketFactory sf = new MySSLSocketFactory(trustStore);
			sf.setHostnameVerifier(MySSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
			myClient.setSSLSocketFactory(sf);
		} catch (Exception e)
		{
			throw new Exception(e.getMessage());
		}

		Log.d(SERVICE_CALL_TAG, "GetWebollettaAttivazioneNotifica URL - executeHttpsGetWebollettaAttivazioneNotifica - " + urlString + parametri);
		Log.d(SERVICE_CALL_TAG, "GetWebollettaAttivazioneNotifica Input Params - executeHttpsGetWebollettaAttivazioneNotifica - " + "parametri[" + parametri
				+ "]");

		myClient.get(urlString + parametriAdjusted, new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(String response)
			{
				if (!context.isFinishing() && !timeout && (context.hasWindowFocus() || (oops != null && oops.hasWindowFocus())))
				{
					List<Cookie> listaCookies = myCookieStore.getCookies();

					HashMap<String, String> parameter = new HashMap<String, String>();
					parameter.put("PARAMETER", parametri);

					Log.d(SERVICE_CALL_TAG, "GetWebollettaAttivazioneNotifica Response - executeHttpsGetWebollettaAttivazioneNotifica - " + response);
					context.postChiamata(response, listaCookies, parameter, notifica, aggiorna);
				}
				timeout = false;
			}

			@Override
			public void onFinish()
			{
				timer.cancel();
				if (oops != null)
				{
					oops.finish();
				}
				super.onFinish();
			}

			@Override
			public void onStart()
			{
			}

			@Override
			protected void handleFailureMessage(Throwable arg0, String arg1)
			{
				// super.handleFailureMessage(arg0, arg1);
				if (!context.isFinishing() && !timeout && (context.hasWindowFocus() || (oops != null && oops.hasWindowFocus())))
				{
					context.postChiamataException(true, parametri, notifica, aggiorna);
				}
				timeout = false;
			}

			@Override
			protected void handleMessage(Message arg0)
			{
				super.handleMessage(arg0);
			}

			@Override
			protected void handleSuccessMessage(String arg0)
			{
				super.handleSuccessMessage(arg0);
			}

			@Override
			protected Message obtainMessage(int arg0, Object arg1)
			{
				return super.obtainMessage(arg0, arg1);
			}

			@Override
			public void onFailure(Throwable arg0, String arg1)
			{
				myCookieStore.clear();
				// super.onFailure(arg0, arg1);
			}

			@Override
			protected void sendFailureMessage(Throwable arg0, byte[] arg1)
			{
				super.sendFailureMessage(arg0, arg1);
			}

			@Override
			protected void sendFailureMessage(Throwable arg0, String arg1)
			{
				super.sendFailureMessage(arg0, arg1);
			}

			@Override
			protected void sendFinishMessage()
			{
				super.sendFinishMessage();
			}

			@Override
			protected void sendMessage(Message arg0)
			{
				super.sendMessage(arg0);
			}

			@Override
			protected void sendStartMessage()
			{
				super.sendStartMessage();
			}

			@Override
			protected void sendSuccessMessage(String arg0)
			{
				super.sendSuccessMessage(arg0);
			}
		});
		timer.schedule(new TimerTask() {
			@Override
			public void run()
			{
				myClient.cancelRequests(context, true);
				timeout = true;
				if (oops != null)
				{
					oops.finish();
				}
				if (!context.isFinishing() && (context.hasWindowFocus() || (oops != null && oops.hasWindowFocus())))
				{
					context.runOnUiThread(new Runnable() {

						public void run()
						{
							context.postChiamataException(true, parametri, notifica, aggiorna);

						}
					});
				}
			}
		}, 20000);
	}

	public void executeHttpsGetWebollettaActivatePostPreconsenso(String urlString, final HashMap<String, String> parametri,
			final WebollettaAttivazionePostPreconsenso context, final Activity oops) throws Exception
			{
		exception = false;
		final Timer timer = new Timer();
		timeout = false;
		final AsyncHttpClient myClient = new AsyncHttpClient();
		myClient.setTimeout(20000);
		List<Cookie> listaCookie = myCookieStore.getCookies();

//		for (int i = 0; i < listaCookie.size(); i++)
//		{
//			Cookie cookie = listaCookie.get(i);
//
//			BasicClientCookie newCookie = new BasicClientCookie(cookie.getName(), cookie.getValue());
//			newCookie.setVersion(cookie.getVersion());
//			newCookie.setDomain(".eni.mobi");
//			newCookie.setPath(cookie.getPath());
//
//			myCookieStore.addCookie(newCookie);
//		}
		String paramString = hashMapParameterToString(parametri);
		paramString = adjustQueryString(paramString);

		myClient.setCookieStore(myCookieStore);
		try
		{
			KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
			trustStore.load(null, null);
			MySSLSocketFactory sf = new MySSLSocketFactory(trustStore);
			sf.setHostnameVerifier(MySSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
			myClient.setSSLSocketFactory(sf);
		} catch (Exception e)
		{
			throw new Exception(e.getMessage());
		}

		Log.d(SERVICE_CALL_TAG, "GetWebollettaActivatePostPreconsenso URL - executeHttpsGetWebollettaActivatePostPreconsenso - " + urlString + paramString);
		Log.d(SERVICE_CALL_TAG, "GetWebollettaActivatePostPreconsenso Input Params - executeHttpsGetWebollettaActivatePostPreconsenso - " + "parametri["
				+ parametri + "]");

		myClient.get(urlString + paramString, new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(String response)
			{
				if (!context.isFinishing() && !timeout && (context.hasWindowFocus() || (oops != null && oops.hasWindowFocus())))
				{
					List<Cookie> listaCookies = myCookieStore.getCookies();

					Log.d(SERVICE_CALL_TAG, "GetWebollettaActivatePostPreconsenso Response - executeHttpsGetWebollettaActivatePostPreconsenso - " + response);
					context.postChiamata(response, listaCookies, parametri);
				}
				timeout = false;
			}

			@Override
			public void onFinish()
			{
				timer.cancel();
				if (oops != null)
				{
					oops.finish();
				}
				super.onFinish();
			}

			@Override
			public void onStart()
			{
			}

			@Override
			protected void handleFailureMessage(Throwable arg0, String arg1)
			{
				// super.handleFailureMessage(arg0, arg1);
				if (!context.isFinishing() && !timeout && (context.hasWindowFocus() || (oops != null && oops.hasWindowFocus())))
				{
					context.postChiamataException(true, parametri);
				}
				timeout = false;
			}

			@Override
			protected void handleMessage(Message arg0)
			{
				super.handleMessage(arg0);
			}

			@Override
			protected void handleSuccessMessage(String arg0)
			{
				super.handleSuccessMessage(arg0);
			}

			@Override
			protected Message obtainMessage(int arg0, Object arg1)
			{
				return super.obtainMessage(arg0, arg1);
			}

			@Override
			public void onFailure(Throwable arg0, String arg1)
			{
				// myCookieStore.clear();
				// super.onFailure(arg0, arg1);
			}

			@Override
			protected void sendFailureMessage(Throwable arg0, byte[] arg1)
			{
				super.sendFailureMessage(arg0, arg1);
			}

			@Override
			protected void sendFailureMessage(Throwable arg0, String arg1)
			{
				super.sendFailureMessage(arg0, arg1);
			}

			@Override
			protected void sendFinishMessage()
			{
				super.sendFinishMessage();
			}

			@Override
			protected void sendMessage(Message arg0)
			{
				super.sendMessage(arg0);
			}

			@Override
			protected void sendStartMessage()
			{
				super.sendStartMessage();
			}

			@Override
			protected void sendSuccessMessage(String arg0)
			{
				super.sendSuccessMessage(arg0);
			}
		});
		timer.schedule(new TimerTask() {
			@Override
			public void run()
			{
				myClient.cancelRequests(context, true);
				timeout = true;
				if (oops != null)
				{
					oops.finish();
				}
				if (!context.isFinishing() && (context.hasWindowFocus() || (oops != null && oops.hasWindowFocus())))
				{
					context.runOnUiThread(new Runnable() {

						public void run()
						{
							context.postChiamataException(true, parametri);

						}
					});
				}
			}
		}, 20000);
			}

	public void executeHttpsGetWebollettaDisattivazionePostPreconsenso(String urlString, final HashMap<String, String> parametri,
			final WebollettaDisattivazionePostPreconsenso context, final Activity oops) throws Exception
			{
		exception = false;
		final Timer timer = new Timer();
		timeout = false;
		final AsyncHttpClient myClient = new AsyncHttpClient();
		myClient.setTimeout(20000);
		List<Cookie> listaCookie = myCookieStore.getCookies();

//		for (int i = 0; i < listaCookie.size(); i++)
//		{
//			Cookie cookie = listaCookie.get(i);
//
//			BasicClientCookie newCookie = new BasicClientCookie(cookie.getName(), cookie.getValue());
//			newCookie.setVersion(cookie.getVersion());
//			newCookie.setDomain(".eni.mobi");
//			newCookie.setPath(cookie.getPath());
//
//			myCookieStore.addCookie(newCookie);
//		}
		String paramString = hashMapParameterToString(parametri);
		paramString = adjustQueryString(paramString);

		myClient.setCookieStore(myCookieStore);
		try
		{
			KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
			trustStore.load(null, null);
			MySSLSocketFactory sf = new MySSLSocketFactory(trustStore);
			sf.setHostnameVerifier(MySSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
			myClient.setSSLSocketFactory(sf);
		} catch (Exception e)
		{
			throw new Exception(e.getMessage());
		}

		Log.d(SERVICE_CALL_TAG, "GetWebollettaDisattivazionePostPreconsenso URL - executeHttpsGetWebollettaDisattivazionePostPreconsenso - " + urlString
				+ paramString);
		Log.d(SERVICE_CALL_TAG, "GetWebollettaDisattivazionePostPreconsenso Input Params - executeHttpsGetWebollettaDisattivazionePostPreconsenso - "
				+ "parametri[" + parametri + "]");

		myClient.get(urlString + paramString, new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(String response)
			{
				if (!context.isFinishing() && !timeout && (context.hasWindowFocus() || (oops != null && oops.hasWindowFocus())))
				{
					List<Cookie> listaCookies = myCookieStore.getCookies();

					Log.d(SERVICE_CALL_TAG, "GetWebollettaDisattivazionePostPreconsenso Response - executeHttpsGetWebollettaDisattivazionePostPreconsenso - "
							+ response);
					context.postChiamata(response, listaCookies, parametri);
				}
				timeout = false;
			}

			@Override
			public void onFinish()
			{
				timer.cancel();
				if (oops != null)
				{
					oops.finish();
				}
				super.onFinish();
			}

			@Override
			public void onStart()
			{
			}

			@Override
			protected void handleFailureMessage(Throwable arg0, String arg1)
			{
				// super.handleFailureMessage(arg0, arg1);
				if (!context.isFinishing() && !timeout && (context.hasWindowFocus() || (oops != null && oops.hasWindowFocus())))
				{
					context.postChiamataException(true, parametri);
				}
				timeout = false;
			}

			@Override
			protected void handleMessage(Message arg0)
			{
				super.handleMessage(arg0);
			}

			@Override
			protected void handleSuccessMessage(String arg0)
			{
				super.handleSuccessMessage(arg0);
			}

			@Override
			protected Message obtainMessage(int arg0, Object arg1)
			{
				return super.obtainMessage(arg0, arg1);
			}

			@Override
			public void onFailure(Throwable arg0, String arg1)
			{
				// myCookieStore.clear();
				// super.onFailure(arg0, arg1);
			}

			@Override
			protected void sendFailureMessage(Throwable arg0, byte[] arg1)
			{
				super.sendFailureMessage(arg0, arg1);
			}

			@Override
			protected void sendFailureMessage(Throwable arg0, String arg1)
			{
				super.sendFailureMessage(arg0, arg1);
			}

			@Override
			protected void sendFinishMessage()
			{
				super.sendFinishMessage();
			}

			@Override
			protected void sendMessage(Message arg0)
			{
				super.sendMessage(arg0);
			}

			@Override
			protected void sendStartMessage()
			{
				super.sendStartMessage();
			}

			@Override
			protected void sendSuccessMessage(String arg0)
			{
				super.sendSuccessMessage(arg0);
			}
		});
		timer.schedule(new TimerTask() {
			@Override
			public void run()
			{
				myClient.cancelRequests(context, true);
				timeout = true;
				if (oops != null)
				{
					oops.finish();
				}
				if (!context.isFinishing() && (context.hasWindowFocus() || (oops != null && oops.hasWindowFocus())))
				{
					context.runOnUiThread(new Runnable() {

						public void run()
						{
							context.postChiamataException(true, parametri);

						}
					});
				}
			}
		}, 20000);
			}

	public void executeHttpsGetBollettaDigitaleDisattivazionePostPreconsenso(String urlString, final HashMap<String, String> parametri,
																	   final BollettaDigitaleAttivazioneNotifica context, final Activity oops) throws Exception
	{
		exception = false;
		final Timer timer = new Timer();
		timeout = false;
		final AsyncHttpClient myClient = new AsyncHttpClient();
		myClient.setTimeout(20000);
		List<Cookie> listaCookie = myCookieStore.getCookies();

//		for (int i = 0; i < listaCookie.size(); i++)
//		{
//			Cookie cookie = listaCookie.get(i);
//
//			BasicClientCookie newCookie = new BasicClientCookie(cookie.getName(), cookie.getValue());
//			newCookie.setVersion(cookie.getVersion());
//			newCookie.setDomain(".eni.mobi");
//			newCookie.setPath(cookie.getPath());
//
//			myCookieStore.addCookie(newCookie);
//		}
		String paramString = hashMapParameterToString(parametri);
		paramString = adjustQueryString(paramString);

		myClient.setCookieStore(myCookieStore);
		try
		{
			KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
			trustStore.load(null, null);
			MySSLSocketFactory sf = new MySSLSocketFactory(trustStore);
			sf.setHostnameVerifier(MySSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
			myClient.setSSLSocketFactory(sf);
		} catch (Exception e)
		{
			throw new Exception(e.getMessage());
		}

		Log.d(SERVICE_CALL_TAG, "GetWebollettaDisattivazionePostPreconsenso URL - executeHttpsGetWebollettaDisattivazionePostPreconsenso - " + urlString
				+ paramString);
		Log.d(SERVICE_CALL_TAG, "GetWebollettaDisattivazionePostPreconsenso Input Params - executeHttpsGetWebollettaDisattivazionePostPreconsenso - "
				+ "parametri[" + parametri + "]");

		myClient.get(urlString + paramString, new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(String response)
			{
				if (!context.isFinishing() && !timeout && (context.hasWindowFocus() || (oops != null && oops.hasWindowFocus())))
				{
					List<Cookie> listaCookies = myCookieStore.getCookies();

					Log.d(SERVICE_CALL_TAG, "GetWebollettaDisattivazionePostPreconsenso Response - executeHttpsGetWebollettaDisattivazionePostPreconsenso - "
							+ response);
					context.postChiamata(response, listaCookies, parametri);
				}
				timeout = false;
			}

			@Override
			public void onFinish()
			{
				timer.cancel();
				if (oops != null)
				{
					oops.finish();
				}
				super.onFinish();
			}

			@Override
			public void onStart()
			{
			}

			@Override
			protected void handleFailureMessage(Throwable arg0, String arg1)
			{
				// super.handleFailureMessage(arg0, arg1);
				if (!context.isFinishing() && !timeout && (context.hasWindowFocus() || (oops != null && oops.hasWindowFocus())))
				{
					context.postChiamataException(true, parametri);
				}
				timeout = false;
			}

			@Override
			protected void handleMessage(Message arg0)
			{
				super.handleMessage(arg0);
			}

			@Override
			protected void handleSuccessMessage(String arg0)
			{
				super.handleSuccessMessage(arg0);
			}

			@Override
			protected Message obtainMessage(int arg0, Object arg1)
			{
				return super.obtainMessage(arg0, arg1);
			}

			@Override
			public void onFailure(Throwable arg0, String arg1)
			{
				// myCookieStore.clear();
				// super.onFailure(arg0, arg1);
			}

			@Override
			protected void sendFailureMessage(Throwable arg0, byte[] arg1)
			{
				super.sendFailureMessage(arg0, arg1);
			}

			@Override
			protected void sendFailureMessage(Throwable arg0, String arg1)
			{
				super.sendFailureMessage(arg0, arg1);
			}

			@Override
			protected void sendFinishMessage()
			{
				super.sendFinishMessage();
			}

			@Override
			protected void sendMessage(Message arg0)
			{
				super.sendMessage(arg0);
			}

			@Override
			protected void sendStartMessage()
			{
				super.sendStartMessage();
			}

			@Override
			protected void sendSuccessMessage(String arg0)
			{
				super.sendSuccessMessage(arg0);
			}
		});
		timer.schedule(new TimerTask() {
			@Override
			public void run()
			{
				myClient.cancelRequests(context, true);
				timeout = true;
				if (oops != null)
				{
					oops.finish();
				}
				if (!context.isFinishing() && (context.hasWindowFocus() || (oops != null && oops.hasWindowFocus())))
				{
					context.runOnUiThread(new Runnable() {

						public void run()
						{
							context.postChiamataException(true, parametri);

						}
					});
				}
			}
		}, 20000);
	}

	public void executeHttpsGetWebollettaDetail(String urlString, final HashMap<String, String> parametri, final WebollettaDetail context, final Activity oops)
			throws Exception
			{
		exception = false;
		final Timer timer = new Timer();
		timeout = false;
		final AsyncHttpClient myClient = new AsyncHttpClient();
		myClient.setTimeout(20000);
		List<Cookie> listaCookie = myCookieStore.getCookies();

//		for (int i = 0; i < listaCookie.size(); i++)
//		{
//			Cookie cookie = listaCookie.get(i);
//
//			BasicClientCookie newCookie = new BasicClientCookie(cookie.getName(), cookie.getValue());
//			newCookie.setVersion(cookie.getVersion());
//			newCookie.setDomain(".eni.mobi");
//			newCookie.setPath(cookie.getPath());
//
//			myCookieStore.addCookie(newCookie);
//		}

		String paramString = hashMapParameterToString(parametri);
		paramString = adjustQueryString(paramString);
		myClient.setCookieStore(myCookieStore);
		try
		{
			KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
			trustStore.load(null, null);
			MySSLSocketFactory sf = new MySSLSocketFactory(trustStore);
			sf.setHostnameVerifier(MySSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
			myClient.setSSLSocketFactory(sf);
		} catch (Exception e)
		{
			throw new Exception(e.getMessage());
		}

		Log.d(SERVICE_CALL_TAG, "GetWebollettaDetail URL - executeHttpsGetWebollettaDetail - " + urlString + paramString);
		Log.d(SERVICE_CALL_TAG, "GetWebollettaDetail Input Params - executeHttpsGetWebollettaDetail - " + "parametri[" + parametri + "]");

		myClient.get(urlString + paramString, new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(String response)
			{

				if (!context.isFinishing() && !timeout && (context.hasWindowFocus() || (oops != null && oops.hasWindowFocus())))
				{
					List<Cookie> listaCookies = myCookieStore.getCookies();

					Log.d(SERVICE_CALL_TAG, "GetWebollettaDetail Response - executeHttpsGetWebollettaDetail - " + response);
					context.postChiamata(response, listaCookies, parametri);
				}
				timeout = false;
			}

			@Override
			public void onFinish()
			{
				timer.cancel();
				if (oops != null)
				{
					oops.finish();
				}
				super.onFinish();
			}

			@Override
			public void onStart()
			{
			}

			@Override
			protected void handleFailureMessage(Throwable arg0, String arg1)
			{
				// super.handleFailureMessage(arg0, arg1);
				if (!context.isFinishing() && !timeout && (context.hasWindowFocus() || (oops != null && oops.hasWindowFocus())))
				{
					context.postChiamataException(true, null, parametri);
				}
				timeout = false;
			}

			@Override
			protected void handleMessage(Message arg0)
			{
				super.handleMessage(arg0);
			}

			@Override
			protected void handleSuccessMessage(String arg0)
			{
				super.handleSuccessMessage(arg0);
			}

			@Override
			protected Message obtainMessage(int arg0, Object arg1)
			{
				return super.obtainMessage(arg0, arg1);
			}

			@Override
			public void onFailure(Throwable arg0, String arg1)
			{
				myCookieStore.clear();
				// super.onFailure(arg0, arg1);
			}

			@Override
			protected void sendFailureMessage(Throwable arg0, byte[] arg1)
			{
				super.sendFailureMessage(arg0, arg1);
			}

			@Override
			protected void sendFailureMessage(Throwable arg0, String arg1)
			{
				super.sendFailureMessage(arg0, arg1);
			}

			@Override
			protected void sendFinishMessage()
			{
				super.sendFinishMessage();
			}

			@Override
			protected void sendMessage(Message arg0)
			{
				super.sendMessage(arg0);
			}

			@Override
			protected void sendStartMessage()
			{
				super.sendStartMessage();
			}

			@Override
			protected void sendSuccessMessage(String arg0)
			{
				super.sendSuccessMessage(arg0);
			}
		});
		timer.schedule(new TimerTask() {
			@Override
			public void run()
			{
				myClient.cancelRequests(context, true);
				timeout = true;
				if (oops != null)
				{
					oops.finish();
				}
				if (!context.isFinishing() && (context.hasWindowFocus() || (oops != null && oops.hasWindowFocus())))
				{
					context.runOnUiThread(new Runnable() {

						public void run()
						{
							context.postChiamataException(true, null, parametri);

						}
					});
				}
			}
		}, 20000);
			}

	public void executeHttpsGetConsumiLuce(String urlString, final String parametri, final ConsumiLuce context, final Activity oops) throws Exception
	{
		exception = false;
		final Timer timer = new Timer();
		timeout = false;
		final AsyncHttpClient myClient = new AsyncHttpClient();
		myClient.setTimeout(20000);
		String parametriAdjusted = adjustQueryString(parametri);
		List<Cookie> listaCookie = myCookieStore.getCookies();

//		for (int i = 0; i < listaCookie.size(); i++)
//		{
//			Cookie cookie = listaCookie.get(i);
//
//			BasicClientCookie newCookie = new BasicClientCookie(cookie.getName(), cookie.getValue());
//			newCookie.setVersion(cookie.getVersion());
//			newCookie.setDomain(".eni.mobi");
//			newCookie.setPath(cookie.getPath());
//
//			myCookieStore.addCookie(newCookie);
//		}
		myClient.setCookieStore(myCookieStore);
		try
		{
			KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
			trustStore.load(null, null);
			MySSLSocketFactory sf = new MySSLSocketFactory(trustStore);
			sf.setHostnameVerifier(MySSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
			myClient.setSSLSocketFactory(sf);
		} catch (Exception e)
		{
			throw new Exception(e.getMessage());
		}

		Log.d(SERVICE_CALL_TAG, "GetConsumiLuce URL - executeHttpsGetConsumiLuce - " + urlString + parametri);
		Log.d(SERVICE_CALL_TAG, "GetConsumiLuce Input Params - executeHttpsGetConsumiLuce - " + "parametri[" + parametri + "]");

		myClient.get(urlString + parametriAdjusted, new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(String response)
			{
				if (!context.isFinishing() && !timeout && (context.hasWindowFocus() || (oops != null && oops.hasWindowFocus())))
				{
					List<Cookie> listaCookies = myCookieStore.getCookies();

					Log.d(SERVICE_CALL_TAG, "GetConsumiLuce Response - executeHttpsGetConsumiLuce - " + response);
					context.postChiamata(response, listaCookies, parametri);
				}
				timeout = false;
			}

			@Override
			public void onFinish()
			{
				timer.cancel();
				if (oops != null)
				{
					oops.finish();
				}
				super.onFinish();
			}

			@Override
			public void onStart()
			{
			}

			@Override
			protected void handleFailureMessage(Throwable arg0, String arg1)
			{
				// super.handleFailureMessage(arg0, arg1);
				if (!context.isFinishing() && !timeout && (context.hasWindowFocus() || (oops != null && oops.hasWindowFocus())))
				{
					context.postChiamataException(true, parametri);
				}
				timeout = false;
			}

			@Override
			protected void handleMessage(Message arg0)
			{
				super.handleMessage(arg0);
			}

			@Override
			protected void handleSuccessMessage(String arg0)
			{
				super.handleSuccessMessage(arg0);
			}

			@Override
			protected Message obtainMessage(int arg0, Object arg1)
			{
				return super.obtainMessage(arg0, arg1);
			}

			@Override
			public void onFailure(Throwable arg0, String arg1)
			{
				// myCookieStore.clear();
				// super.onFailure(arg0, arg1);
			}

			@Override
			protected void sendFailureMessage(Throwable arg0, byte[] arg1)
			{
				super.sendFailureMessage(arg0, arg1);
			}

			@Override
			protected void sendFailureMessage(Throwable arg0, String arg1)
			{
				super.sendFailureMessage(arg0, arg1);
			}

			@Override
			protected void sendFinishMessage()
			{
				super.sendFinishMessage();
			}

			@Override
			protected void sendMessage(Message arg0)
			{
				super.sendMessage(arg0);
			}

			@Override
			protected void sendStartMessage()
			{
				super.sendStartMessage();
			}

			@Override
			protected void sendSuccessMessage(String arg0)
			{
				super.sendSuccessMessage(arg0);
			}
		});
		timer.schedule(new TimerTask() {
			@Override
			public void run()
			{
				myClient.cancelRequests(context, true);
				timeout = true;
				if (oops != null)
				{
					oops.finish();
				}
				if (!context.isFinishing() && (context.hasWindowFocus() || (oops != null && oops.hasWindowFocus())))
				{
					context.runOnUiThread(new Runnable() {

						public void run()
						{
							context.postChiamataException(true, parametri);

						}
					});
				}
			}
		}, 20000);
	}

	public void executeHttpsGetConsumiGas(String urlString, final String parametri, final ConsumiGas context, final Activity oops) throws Exception
	{
		exception = false;
		final Timer timer = new Timer();
		timeout = false;
		final AsyncHttpClient myClient = new AsyncHttpClient();
		myClient.setTimeout(20000);
		String parametriAdjusted = adjustQueryString(parametri);
		List<Cookie> listaCookie = myCookieStore.getCookies();

//		for (int i = 0; i < listaCookie.size(); i++)
//		{
//			Cookie cookie = listaCookie.get(i);
//
//			BasicClientCookie newCookie = new BasicClientCookie(cookie.getName(), cookie.getValue());
//			newCookie.setVersion(cookie.getVersion());
//			newCookie.setDomain(".eni.mobi");
//			newCookie.setPath(cookie.getPath());
//
//			myCookieStore.addCookie(newCookie);
//		}
		myClient.setCookieStore(myCookieStore);
		try
		{
			KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
			trustStore.load(null, null);
			MySSLSocketFactory sf = new MySSLSocketFactory(trustStore);
			sf.setHostnameVerifier(MySSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
			myClient.setSSLSocketFactory(sf);
		} catch (Exception e)
		{
			throw new Exception(e.getMessage());
		}

		Log.d(SERVICE_CALL_TAG, "GetConsumiGas URL - executeHttpsGetConsumiGas - " + urlString + parametri);
		Log.d(SERVICE_CALL_TAG, "GetConsumiGas Input Params - executeHttpsGetConsumiGas - " + "parametri[" + parametri + "]");

		myClient.get(urlString + parametriAdjusted, new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(String response)
			{
				if (!context.isFinishing() && !timeout && (context.hasWindowFocus() || (oops != null && oops.hasWindowFocus())))
				{
					List<Cookie> listaCookies = myCookieStore.getCookies();

					Log.d(SERVICE_CALL_TAG, "GetConsumiGas Response - executeHttpsGetConsumiGas - " + response);
					context.postChiamata(response, listaCookies, parametri);
				}
				timeout = false;
			}

			@Override
			public void onFinish()
			{
				timer.cancel();
				if (oops != null)
				{
					oops.finish();
				}
				super.onFinish();
			}

			@Override
			public void onStart()
			{
			}

			@Override
			protected void handleFailureMessage(Throwable arg0, String arg1)
			{
				// super.handleFailureMessage(arg0, arg1);
				if (!context.isFinishing() && !timeout && (context.hasWindowFocus() || (oops != null && oops.hasWindowFocus())))
				{
					context.postChiamataException(true, parametri);
				}
				timeout = false;
			}

			@Override
			protected void handleMessage(Message arg0)
			{
				super.handleMessage(arg0);
			}

			@Override
			protected void handleSuccessMessage(String arg0)
			{
				super.handleSuccessMessage(arg0);
			}

			@Override
			protected Message obtainMessage(int arg0, Object arg1)
			{
				return super.obtainMessage(arg0, arg1);
			}

			@Override
			public void onFailure(Throwable arg0, String arg1)
			{
				// myCookieStore.clear();
				// super.onFailure(arg0, arg1);
			}

			@Override
			protected void sendFailureMessage(Throwable arg0, byte[] arg1)
			{
				super.sendFailureMessage(arg0, arg1);
			}

			@Override
			protected void sendFailureMessage(Throwable arg0, String arg1)
			{
				super.sendFailureMessage(arg0, arg1);
			}

			@Override
			protected void sendFinishMessage()
			{
				super.sendFinishMessage();
			}

			@Override
			protected void sendMessage(Message arg0)
			{
				super.sendMessage(arg0);
			}

			@Override
			protected void sendStartMessage()
			{
				super.sendStartMessage();
			}

			@Override
			protected void sendSuccessMessage(String arg0)
			{
				super.sendSuccessMessage(arg0);
			}
		});
		timer.schedule(new TimerTask() {
			@Override
			public void run()
			{
				myClient.cancelRequests(context, true);
				timeout = true;
				if (oops != null)
				{
					oops.finish();
				}
				if (!context.isFinishing() && (context.hasWindowFocus() || (oops != null && oops.hasWindowFocus())))
				{
					context.runOnUiThread(new Runnable() {

						public void run()
						{
							context.postChiamataException(true, parametri);

						}
					});
				}
			}
		}, 20000);
	}

	public void executeHttpsGetConsumiGasGrafico(String urlString, final String parametri, final ConsumiGasGraficoConsumi context, final Activity oops)
			throws Exception
			{
		exception = false;
		final Timer timer = new Timer();
		timeout = false;
		final AsyncHttpClient myClient = new AsyncHttpClient();
		myClient.setTimeout(20000);
		String parametriAdjusted = adjustQueryString(parametri);
		List<Cookie> listaCookie = myCookieStore.getCookies();

//		for (int i = 0; i < listaCookie.size(); i++)
//		{
//			Cookie cookie = listaCookie.get(i);
//
//			BasicClientCookie newCookie = new BasicClientCookie(cookie.getName(), cookie.getValue());
//			newCookie.setVersion(cookie.getVersion());
//			newCookie.setDomain(".eni.mobi");
//			newCookie.setPath(cookie.getPath());
//
//			myCookieStore.addCookie(newCookie);
//		}
		myClient.setCookieStore(myCookieStore);
		try
		{
			KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
			trustStore.load(null, null);
			MySSLSocketFactory sf = new MySSLSocketFactory(trustStore);
			sf.setHostnameVerifier(MySSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
			myClient.setSSLSocketFactory(sf);
		} catch (Exception e)
		{
			throw new Exception(e.getMessage());
		}

		Log.d(SERVICE_CALL_TAG, "GetConsumiGasGrafico URL - executeHttpsGetConsumiGasGrafico - " + urlString + parametri);
		Log.d(SERVICE_CALL_TAG, "GetConsumiGasGrafico Input Params - executeHttpsGetConsumiGasGrafico - " + "parametri[" + parametri + "]");

		myClient.get(urlString + parametriAdjusted, new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(String response)
			{
				if (!context.isFinishing() && !timeout && (context.hasWindowFocus() || (oops != null && oops.hasWindowFocus())))
				{
					List<Cookie> listaCookies = myCookieStore.getCookies();
					HashMap<String, String> parameter = new HashMap<String, String>();
					parameter.put("PARAMETER", parametri);

					Log.d(SERVICE_CALL_TAG, "GetConsumiGasGrafico Response - executeHttpsGetConsumiGasGrafico - " + response);
					context.postChiamata(response, listaCookies, parameter);
				}
				timeout = false;
			}

			@Override
			public void onFinish()
			{
				timer.cancel();
				if (oops != null)
				{
					oops.finish();
				}
				super.onFinish();
			}

			@Override
			public void onStart()
			{
			}

			@Override
			protected void handleFailureMessage(Throwable arg0, String arg1)
			{
				// super.handleFailureMessage(arg0, arg1);
				if (!context.isFinishing() && !timeout && (context.hasWindowFocus() || (oops != null && oops.hasWindowFocus())))
				{
					context.postChiamataException(true, parametri, null);
				}
				timeout = false;
			}

			@Override
			protected void handleMessage(Message arg0)
			{
				super.handleMessage(arg0);
			}

			@Override
			protected void handleSuccessMessage(String arg0)
			{
				super.handleSuccessMessage(arg0);
			}

			@Override
			protected Message obtainMessage(int arg0, Object arg1)
			{
				return super.obtainMessage(arg0, arg1);
			}

			@Override
			public void onFailure(Throwable arg0, String arg1)
			{
				myCookieStore.clear();
				// super.onFailure(arg0, arg1);
			}

			@Override
			protected void sendFailureMessage(Throwable arg0, byte[] arg1)
			{
				super.sendFailureMessage(arg0, arg1);
			}

			@Override
			protected void sendFailureMessage(Throwable arg0, String arg1)
			{
				super.sendFailureMessage(arg0, arg1);
			}

			@Override
			protected void sendFinishMessage()
			{
				super.sendFinishMessage();
			}

			@Override
			protected void sendMessage(Message arg0)
			{
				super.sendMessage(arg0);
			}

			@Override
			protected void sendStartMessage()
			{
				super.sendStartMessage();
			}

			@Override
			protected void sendSuccessMessage(String arg0)
			{
				super.sendSuccessMessage(arg0);
			}
		});
		timer.schedule(new TimerTask() {
			@Override
			public void run()
			{
				myClient.cancelRequests(context, true);
				timeout = true;
				if (oops != null)
				{
					oops.finish();
				}
				if (!context.isFinishing() && (context.hasWindowFocus() || (oops != null && oops.hasWindowFocus())))
				{
					context.runOnUiThread(new Runnable() {

						public void run()
						{
							context.postChiamataException(true, parametri, null);

						}
					});
				}
			}
		}, 20000);
			}

	/**
	 * Motodo per accettare tutti i certificati usato in richieste https
	 */
	private static void trustAllHosts()
	{
		// Create a trust manager that does not validate certificate chains
		TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
			public java.security.cert.X509Certificate[] getAcceptedIssuers()
			{
				return new java.security.cert.X509Certificate[] {};
			}

			public void checkClientTrusted(X509Certificate[] chain, String authType)
			{
			}

			public void checkServerTrusted(X509Certificate[] chain, String authType)
			{
			}

		} };
		// Install the all-trusting trust manager
		try
		{
			SSLContext sc = SSLContext.getInstance("TLS");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	final static HostnameVerifier DO_NOT_VERIFY = new HostnameVerifier() {
		public boolean verify(String hostname, SSLSession session)
		{
			return true;
		}
	};

	/**
	 * Converte i byte dell'InputStream in una singola String
	 * 
	 * @param is
	 *            InputStrem
	 * @return i byte dell'InputStream in formato String
	 * @throws IOException
	 */
	public static String convertStreamToString(InputStream is) throws IOException
	{
		if (is != null)
		{
			Writer writer = new StringWriter();
			char[] buffer = new char[1024];

			try
			{
				Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
				int n;
				while ((n = reader.read(buffer)) != -1)
				{
					writer.write(buffer, 0, n);
				}
			} finally
			{
				is.close();
			}

			return writer.toString();

		} else
		{
			return "";
		}

	}

	/**
	 * Crea la sequenza della url che serve per il passaggio dei parametri (in
	 * formato: &<nome_parametro>=<valore_parametro>&...)
	 * 
	 * @param hashMap
	 *            con i parametri da passare alla url (chiave: nome_parametro,
	 *            valore: valore del parametro)
	 * @return la sequenza che contiene i parametri (formato:
	 *         ?<nome_parametro>=<valore_parametro>) da concatenare alla url
	 */
	public String hashMapParameterToString(HashMap<String, String> hashMap)
	{
		String result = "";
		Set<Entry<String, String>> entrySet = hashMap.entrySet();

		for (Iterator<Entry<String, String>> it = entrySet.iterator(); it.hasNext();)
		{

			Entry<String, String> row = it.next();

			Log.d(SERVICE_CALL_TAG, "EntrySet = " + row.getKey() + " - " + row.getValue());

			result = result + "&" + row.getKey();
			String value = (String) row.getValue();
			result = result + "=" + value.replace(" ", "%20");
		}
		return result;
	}

	private String adjustQueryString(String input)
	{
		if(input==null)
			return "";

		if(input.startsWith("&"))
		{
			input = input.substring(1);
		}

		StringBuilder sb = new StringBuilder(input);
		sb.insert(0,"?");

//		input +="?";
		return String.valueOf(sb);
	}

	public void clearCookies()
	{
		myCookieStore.getCookies();
		myCookieStore.clear();
	}

}
