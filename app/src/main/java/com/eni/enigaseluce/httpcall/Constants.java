package com.eni.enigaseluce.httpcall;

import java.util.List;

import org.apache.http.cookie.Cookie;

public class Constants {

    // DEV
    public static final boolean STUB_ENABLED = false;

    public static final boolean TEST_ENV = false;
    public static String PROTOCOL = "https";
    // TEST
    public static final String TEST_DOMAIN = "st3-eni.eni.mobi";
    public static final String UAT_DOMAIN = "pp.enigaseluce.com";
    public static final String PROD_DOMAIN = "enigaseluce.com";

    public static final String SITEMINDER_DOMAIN_PROD  = "wsbasic.eni.it";
    public static final String SITEMINDER_DOMAIN_TEST  = "st-wsbasic.eni.it";

    public static String URL_AUTH = "https://st-password.eni.it/opensso/UI/Login?goto=";

    public static String URL_SITEMINDER_TEST = "https://st-wsbasic.eni.it/EniWSSO/ServiceLoginRest";
    public static String URL_SITEMINDER_PROD = "https://wsbasic.eni.it/EniWSSO/ServiceLoginRest";

    public static String DUMMY_RESOURCE = "https://pp.enigaseluce.com/it-IT/famiglia/private/siteminder-headers";
//    public static String DUMMY_RESOURCE = "http://127.0.0.1:56902";
    /******************************************* Indirizzi st3 ********************************************/
//    public static String URL_MAINTAINANCE_MODE = "http://prev-famiglia.eni.it:81/dommPortalWEB/framework/forward/maintenance.jsp?portal=resid";
    public static String URL_MAINTAINANCE_MODE = "https://pp.enigaseluce.com/enigaseluce-services-public/maintenance?portal=resid";


    public static String URL_LOGIN = PROTOCOL + "://" + TEST_DOMAIN + "/dommPortalWEB/apprest/private/forwardplist.jsp?action=loginWSRest";

    public static String URL_LOGIN_TEST = PROTOCOL + "://" + UAT_DOMAIN + "/enigaseluce-services/loginWSRest";
    public static String URL_LOGIN_PROD = PROTOCOL + "://" + PROD_DOMAIN + "/enigaseluce-services/loginWSRest";

//    https://pp.enigaseluce.com/enigaseluce-services/loginWSRest
//    public static String URL_VISUALIZZA_FORNITURE_DOMICILIAZIONE = PROTOCOL + "://" + TEST_DOMAIN + "/dommPortalWEB/apprest/private/forwardplist.jsp?action=visualizzaFornitureDomiciliazioneWSRestNEW";
    public static String URL_VISUALIZZA_FORNITURE_DOMICILIAZIONE = "https://pp.enigaseluce.com/enigaseluce-services/visualizzaFornitureDomiciliazioneWSRestNEW";


//    public static String URL_STATO_AVANZAMENTO_FORNITURE = PROTOCOL + "://" + TEST_DOMAIN + "/dommPortalWEB/apprest/private/forwardplist.jsp?action=statoAvanzamentoFornitureWSRest";
    public static String URL_STATO_AVANZAMENTO_FORNITURE = "https://pp.enigaseluce.com/enigaseluce-services/statoAvanzamentoFornitureWSRest";

//    public static String URL_EXECUTE_DOMICILIAZIONE_CONTO = PROTOCOL + "://" + TEST_DOMAIN + "/dommPortalWEB/apprest/private/forwardplist.jsp?action=executeDomiciliazioneContoWSRest";
    public static String URL_EXECUTE_DOMICILIAZIONE_CONTO = "https://pp.enigaseluce.com/enigaseluce-services/executeDomiciliazioneContoWSRest";

//    public static String URL_FORNITURE_GAS_ATTIVE = PROTOCOL + "://" + TEST_DOMAIN + "/dommPortalWEB/apprest/private/forwardplist.jsp?action=loadFornitureInfoGasWSRest";
    public static String URL_FORNITURE_GAS_ATTIVE = "https://pp.enigaseluce.com/enigaseluce-services/loadFornitureInfoGasWSRest";

//    public static String URL_STORICO_LETTURE_GAS = PROTOCOL + "://" + TEST_DOMAIN + "/dommPortalWEB/apprest/private/forwardplist.jsp?action=storicoLettureGas";
    public static String URL_STORICO_LETTURE_GAS = "https://pp.enigaseluce.com/enigaseluce-services/storicoLettureGas";

//    public static String URL_ULTIMA_LETTURA_GAS = PROTOCOL + "://" + TEST_DOMAIN + "/dommPortalWEB/apprest/private/forwardplist.jsp?action=ultimaLetturaGas";
    public static String URL_ULTIMA_LETTURA_GAS = "https://pp.enigaseluce.com/enigaseluce-services/ultimaLetturaGas";

//    public static String URL_INVIA_AUTOLETTURA_GAS = PROTOCOL + "://" + TEST_DOMAIN + "/dommPortalWEB/apprest/private/forwardplist.jsp?action=invioAutolettura";
    public static String URL_INVIA_AUTOLETTURA_GAS = "https://pp.enigaseluce.com/enigaseluce-services/invioAutolettura";

//    public static String URL_ANNULLA_AUTOLETTURA_GAS = PROTOCOL + "://" + TEST_DOMAIN + "/dommPortalWEB/apprest/private/forwardplist.jsp?action=annullaAutolettura";
    public static String URL_ANNULLA_AUTOLETTURA_GAS = "https://pp.enigaseluce.com/enigaseluce-services/annullaAutolettura";

//    public static String URL_CONFERMA_AUTOLETTURA_GAS = PROTOCOL + "://" + TEST_DOMAIN + "/dommPortalWEB/apprest/private/forwardplist.jsp?action=confermaAutolettura";
    public static String URL_CONFERMA_AUTOLETTURA_GAS = "https://pp.enigaseluce.com/enigaseluce-services/confermaAutolettura";
    /************************************************ Rilascio 2 *********************************************************************************************/
//    public static String URL_SHOW_LISTA_FATTURE = PROTOCOL + "://" + TEST_DOMAIN + "/dommPortalWEB/apprest/private/forwardplist.jsp?action=showListaFattureWSRest";
    public static String URL_SHOW_LISTA_FATTURE = "https://pp.enigaseluce.com/enigaseluce-services/showListaFattureWSRest";

//    public static String URL_INFO_PROSSIMA_FATTURA = PROTOCOL + "://" + TEST_DOMAIN + "/dommPortalWEB/apprest/private/forwardplist.jsp?action=infoProssimaFatturaWSRest";
    public static String URL_INFO_PROSSIMA_FATTURA = "https://pp.enigaseluce.com/enigaseluce-services/infoProssimaFatturaWSRest";

//    public static String URL_DOWNLOAD_PDF = PROTOCOL + "://" + TEST_DOMAIN + "/dommPortalWEB/apprest/private/forwardplist.jsp?action=downloadDocPdfWSRest";
    public static String URL_DOWNLOAD_PDF = "https://pp.enigaseluce.com/enigaseluce-services/downloadDocPdfWSRest";

//    public static String URL_INFO_FATTURA_ONLINE = PROTOCOL + "://" + TEST_DOMAIN + "/dommPortalWEB/apprest/private/forwardplist.jsp?action=infoFOLSuContiWSRest";
    public static String URL_INFO_FATTURA_ONLINE = "https://pp.enigaseluce.com/enigaseluce-services/infoFOLSuContiWSRest";

//    public static String URL_INFO_FATTURA_ONLINE = "http://pre-famiglia.eni.it/dommPortalWEB/apprest/private/forwardplist.jsp?action=infoFOLSuContiWSRest";

//    public static String URL_RICHIESTA_PRECONSENSO = PROTOCOL + "://" + TEST_DOMAIN + "/dommPortalWEB/apprest/private/forwardplist.jsp?action=richiestaPreConsensoFatturaOnlineWSRest";
    public static String URL_RICHIESTA_PRECONSENSO = "https://pp.enigaseluce.com/enigaseluce-services/richiestaPreConsensoFatturaOnlineWSRest";

//    public static String URL_RICHIESTA_CONFERMA_MAIL = PROTOCOL + "://" + TEST_DOMAIN + "/dommPortalWEB/apprest/private/forwardplist.jsp?action=inviaRichiestaConfermaMailWSRest";
    public static String URL_RICHIESTA_CONFERMA_MAIL = "https://pp.enigaseluce.com/enigaseluce-services/inviaRichiestaConfermaMailWSRest";
//    public static String URL_RICHIESTA_CONFERMA_MAIL = "http://pre-famiglia.eni.it/dommPortalWEB/apprest/private/forwardplist.jsp?action=inviaRichiestaConfermaMailWSRest";

//    public static String URL_GESTIONE_NOTIFICA_CELLULARE = PROTOCOL + "://" + TEST_DOMAIN + "/dommPortalWEB/apprest/private/forwardplist.jsp?action=executeModificaNotificaSMSWSRest";
    public static String URL_GESTIONE_NOTIFICA_CELLULARE = "https://pp.enigaseluce.com/enigaseluce-services/executeModificaNotificaSMSWSRest";
    /************************************************* Rilascio 3 *****************************************************************************************/
//    public static String URL_CONSUMI_LUCE = PROTOCOL + "://" + TEST_DOMAIN + "/dommPortalWEB/apprest/private/forwardplist.jsp?action=consumiLuceWSRest";
    public static String URL_CONSUMI_LUCE = "https://pp.enigaseluce.com/enigaseluce-services/consumiLuceWSRest";

//    public static String URL_CONSUMI_GAS = PROTOCOL + "://" + TEST_DOMAIN + "/dommPortalWEB/apprest/private/forwardplist.jsp?action=consumiGasWSRest";
    public static String URL_CONSUMI_GAS = "https://pp.enigaseluce.com/enigaseluce-services/consumiGasWSRest";


//    public static String URL_INFO_FATTURE_RATEIZZABILI = PROTOCOL + "://" + TEST_DOMAIN + "/dommPortalWEB/apprest/private/forwardplist.jsp?action=infoFattureRateizzabiliWSRest";
    //cambio puntamenti
    public static String URL_INFO_FATTURE_RATEIZZABILI = "https://pp.enigaseluce.com/enigaseluce-services/infoFattureRateizzabiliWSRest";

//    public static String URL_CALCOLA_PROPOSTA_RATE = PROTOCOL + "://" + TEST_DOMAIN + "/dommPortalWEB/apprest/private/forwardplist.jsp?action=calcolaPropostaWSRest";
    public static String URL_CALCOLA_PROPOSTA_RATE = "https://pp.enigaseluce.com/enigaseluce-services/calcolaPropostaWSRest";

//    public static String URL_ATTIVA_PROPOSTA_RATEIZZAZIONE = PROTOCOL + "://" + TEST_DOMAIN + "/dommPortalWEB/apprest/private/forwardplist.jsp?action=attivaPropostaRateizzazioneWSRest";
public static String URL_ATTIVA_PROPOSTA_RATEIZZAZIONE = "https://pp.enigaseluce.com/enigaseluce-services/attivaPropostaRateizzazioneWSRest";

    // PROD
    // /******************************************* Indirizzi PROD
    // ********************************************/
    // public static final boolean TEST_ENV = false;
    // public static String URL_AUTH =
    // "https://password.eni.it/opensso/UI/Login?goto=";
    // public static String URL_MAINTAINANCE_MODE =
    // "http://www.famiglia.eni.it/dommPortalWEB/framework/forward/maintenance.jsp?portal=resid";
    // //Maintenance TEST
    // public static String URL_LOGIN =
    // "https://www.eni.mobi/dommPortalWEB/apprest/private/forwardplist.jsp?action=loginWSRest";
    // public static String URL_STATO_AVANZAMENTO_FORNITURE =
    // "https://www.eni.mobi/dommPortalWEB/apprest/private/forwardplist.jsp?action=statoAvanzamentoFornitureWSRest";
    // public static String URL_VISUALIZZA_FORNITURE_DOMICILIAZIONE =
    // "https://www.eni.mobi/dommPortalWEB/apprest/private/forwardplist.jsp?action=visualizzaFornitureDomiciliazioneWSRest";
    // public static String URL_EXECUTE_DOMICILIAZIONE_CONTO =
    // "https://www.eni.mobi/dommPortalWEB/apprest/private/forwardplist.jsp?action=executeDomiciliazioneContoWSRest";
    // public static String URL_FORNITURE_GAS_ATTIVE =
    // "https://www.eni.mobi/dommPortalWEB/apprest/private/forwardplist.jsp?action=loadFornitureInfoGasWSRest";
    // public static String URL_STORICO_LETTURE_GAS =
    // "https://www.eni.mobi/dommPortalWEB/apprest/private/forwardplist.jsp?action=storicoLettureGas";
    // public static String URL_ULTIMA_LETTURA_GAS =
    // "https://www.eni.mobi/dommPortalWEB/apprest/private/forwardplist.jsp?action=ultimaLetturaGas";
    // public static String URL_INVIA_AUTOLETTURA_GAS =
    // "https://www.eni.mobi/dommPortalWEB/apprest/private/forwardplist.jsp?action=invioAutolettura";
    // public static String URL_ANNULLA_AUTOLETTURA_GAS =
    // "https://www.eni.mobi/dommPortalWEB/apprest/private/forwardplist.jsp?action=annullaAutolettura";
    // public static String URL_CONFERMA_AUTOLETTURA_GAS =
    // "https://www.eni.mobi/dommPortalWEB/apprest/private/forwardplist.jsp?action=confermaAutolettura";
    // /************************************************Rilascio
    // 2*********************************************************************************************/
    // public static String URL_SHOW_LISTA_FATTURE =
    // "https://www.eni.mobi/dommPortalWEB/apprest/private/forwardplist.jsp?action=showListaFattureWSRest";
    // public static String URL_INFO_PROSSIMA_FATTURA =
    // "https://www.eni.mobi/dommPortalWEB/apprest/private/forwardplist.jsp?action=infoProssimaFatturaWSRest";
    // public static String URL_DOWNLOAD_PDF =
    // "https://www.eni.mobi/dommPortalWEB/apprest/private/forwardplist.jsp?action=downloadDocPdfWSRest";
    // public static String URL_INFO_FATTURA_ONLINE =
    // "https://www.eni.mobi/dommPortalWEB/apprest/private/forwardplist.jsp?action=infoFOLSuContiWSRest";
    // public static String URL_RICHIESTA_PRECONSENSO =
    // "https://www.eni.mobi/dommPortalWEB/apprest/private/forwardplist.jsp?action=richiestaPreConsensoFatturaOnlineWSRest";
    // public static String URL_RICHIESTA_CONFERMA_MAIL =
    // "https://www.eni.mobi/dommPortalWEB/apprest/private/forwardplist.jsp?action=inviaRichiestaConfermaMailWSRest";
    // public static String URL_GESTIONE_NOTIFICA_CELLULARE =
    // "https://www.eni.mobi/dommPortalWEB/apprest/private/forwardplist.jsp?action=executeModificaNotificaSMSWSRest";
    // /*************************************************Rilascio 3
    // *****************************************************************************************/
    // public static String URL_CONSUMI_LUCE =
    // "https://www.eni.mobi/dommPortalWEB/apprest/private/forwardplist.jsp?action=consumiLuceWSRest";
    // public static String URL_CONSUMI_GAS =
    // "https://www.eni.mobi/dommPortalWEB/apprest/private/forwardplist.jsp?action=consumiGasWSRest";
    // public static String URL_INFO_FATTURE_RATEIZZABILI =
    // "https://www.eni.mobi/dommPortalWEB/apprest/private/forwardplist.jsp?action=infoFattureRateizzabiliWSRest";
    // public static String URL_CALCOLA_PROPOSTA_RATE =
    // "https://www.eni.mobi/dommPortalWEB/apprest/private/forwardplist.jsp?action=calcolaPropostaWSRest";
    // public static String URL_ATTIVA_PROPOSTA_RATEIZZAZIONE =
    // "https://www.eni.mobi/dommPortalWEB/apprest/private/forwardplist.jsp?action=attivaPropostaRateizzazioneWSRest";

    static {

	if (!TEST_ENV) {
	    /******************************************* Indirizzi PROD ********************************************/
	    URL_AUTH = "https://password.eni.it/opensso/UI/Login?goto=";

//        URL_MAINTAINANCE_MODE = "http://www.famiglia.eni.it/dommPortalWEB/framework/forward/maintenance.jsp?portal=resid"; // Maintenance
        URL_MAINTAINANCE_MODE = "https://enigaseluce.com/enigaseluce-services-public/maintenance?portal=resid";

															       // TEST
	    URL_LOGIN = "https://www.eni.mobi/dommPortalWEB/apprest/private/forwardplist.jsp?action=loginWSRest";
//	    URL_STATO_AVANZAMENTO_FORNITURE = "https://www.eni.mobi/dommPortalWEB/apprest/private/forwardplist.jsp?action=statoAvanzamentoFornitureWSRest";
        URL_STATO_AVANZAMENTO_FORNITURE = "https://enigaseluce.com/enigaseluce-services/statoAvanzamentoFornitureWSRest";


//        URL_VISUALIZZA_FORNITURE_DOMICILIAZIONE = "https://www.eni.mobi/dommPortalWEB/apprest/private/forwardplist.jsp?action=visualizzaFornitureDomiciliazioneWSRestNEW";
        URL_VISUALIZZA_FORNITURE_DOMICILIAZIONE = "https://enigaseluce.com/enigaseluce-services/visualizzaFornitureDomiciliazioneWSRestNEW";

//	    URL_EXECUTE_DOMICILIAZIONE_CONTO = "https://www.eni.mobi/dommPortalWEB/apprest/private/forwardplist.jsp?action=executeDomiciliazioneContoWSRest";
        URL_EXECUTE_DOMICILIAZIONE_CONTO = "https://enigaseluce.com/enigaseluce-services/executeDomiciliazioneContoWSRest";


//        URL_FORNITURE_GAS_ATTIVE = "https://www.eni.mobi/dommPortalWEB/apprest/private/forwardplist.jsp?action=loadFornitureInfoGasWSRest";
        URL_FORNITURE_GAS_ATTIVE = "https://enigaseluce.com/enigaseluce-services/loadFornitureInfoGasWSRest";

//	    URL_STORICO_LETTURE_GAS = "https://www.eni.mobi/dommPortalWEB/apprest/private/forwardplist.jsp?action=storicoLettureGas";
        URL_STORICO_LETTURE_GAS = "https://enigaseluce.com/enigaseluce-services/storicoLettureGas";


//	    URL_ULTIMA_LETTURA_GAS = "https://www.eni.mobi/dommPortalWEB/apprest/private/forwardplist.jsp?action=ultimaLetturaGas";
        URL_ULTIMA_LETTURA_GAS = "https://enigaseluce.com/enigaseluce-services/ultimaLetturaGas";

//	    URL_INVIA_AUTOLETTURA_GAS = "https://www.eni.mobi/dommPortalWEB/apprest/private/forwardplist.jsp?action=invioAutolettura";
        URL_INVIA_AUTOLETTURA_GAS = "https://enigaseluce.com/enigaseluce-services/invioAutolettura";

//        URL_ANNULLA_AUTOLETTURA_GAS = "https://www.eni.mobi/dommPortalWEB/apprest/private/forwardplist.jsp?action=annullaAutolettura";
        URL_ANNULLA_AUTOLETTURA_GAS = "https://enigaseluce.com/enigaseluce-services/annullaAutolettura";

//	    URL_CONFERMA_AUTOLETTURA_GAS = "https://www.eni.mobi/dommPortalWEB/apprest/private/forwardplist.jsp?action=confermaAutolettura";
        URL_CONFERMA_AUTOLETTURA_GAS = "https://enigaseluce.com/enigaseluce-services/confermaAutolettura";
	    /************************************************ Rilascio 2 *********************************************************************************************/
//	    URL_SHOW_LISTA_FATTURE = "https://www.eni.mobi/dommPortalWEB/apprest/private/forwardplist.jsp?action=showListaFattureWSRest";
        URL_SHOW_LISTA_FATTURE = "https://enigaseluce.com/enigaseluce-services/showListaFattureWSRest";

//	    URL_INFO_PROSSIMA_FATTURA = "https://www.eni.mobi/dommPortalWEB/apprest/private/forwardplist.jsp?action=infoProssimaFatturaWSRest";
        URL_INFO_PROSSIMA_FATTURA = "https://enigaseluce.com/enigaseluce-services/infoProssimaFatturaWSRest";

//        URL_DOWNLOAD_PDF = "https://www.eni.mobi/dommPortalWEB/apprest/private/forwardplist.jsp?action=downloadDocPdfWSRest";
        URL_DOWNLOAD_PDF = "https://enigaseluce.com/enigaseluce-services/downloadDocPdfWSRest";


//	    URL_INFO_FATTURA_ONLINE = "https://www.eni.mobi/dommPortalWEB/apprest/private/forwardplist.jsp?action=infoFOLSuContiWSRest";
        URL_INFO_FATTURA_ONLINE = "https://enigaseluce.com/enigaseluce-services/infoFOLSuContiWSRest";

//	    URL_RICHIESTA_PRECONSENSO = "https://www.eni.mobi/dommPortalWEB/apprest/private/forwardplist.jsp?action=richiestaPreConsensoFatturaOnlineWSRest";
        URL_RICHIESTA_PRECONSENSO = "https://enigaseluce.com/enigaseluce-services/richiestaPreConsensoFatturaOnlineWSRest";

//        URL_RICHIESTA_CONFERMA_MAIL = "https://www.eni.mobi/dommPortalWEB/apprest/private/forwardplist.jsp?action=inviaRichiestaConfermaMailWSRest";
        URL_RICHIESTA_CONFERMA_MAIL = "https://enigaseluce.com/enigaseluce-services/inviaRichiestaConfermaMailWSRest";


//        URL_GESTIONE_NOTIFICA_CELLULARE = "https://www.eni.mobi/dommPortalWEB/apprest/private/forwardplist.jsp?action=executeModificaNotificaSMSWSRest";
        URL_GESTIONE_NOTIFICA_CELLULARE = "https://enigaseluce.com/enigaseluce-services/executeModificaNotificaSMSWSRest";
	    /************************************************* Rilascio 3 *****************************************************************************************/
//	    URL_CONSUMI_LUCE = "https://www.eni.mobi/dommPortalWEB/apprest/private/forwardplist.jsp?action=consumiLuceWSRest";
        URL_CONSUMI_LUCE = "https://enigaseluce.com/enigaseluce-services/consumiLuceWSRest";

//        URL_CONSUMI_GAS = "https://www.eni.mobi/dommPortalWEB/apprest/private/forwardplist.jsp?action=consumiGasWSRest";
        URL_CONSUMI_GAS = "https://enigaseluce.com/enigaseluce-services/consumiGasWSRest";

//	    URL_INFO_FATTURE_RATEIZZABILI = "https://www.eni.mobi/dommPortalWEB/apprest/private/forwardplist.jsp?action=infoFattureRateizzabiliWSRest";
        URL_INFO_FATTURE_RATEIZZABILI = "https://enigaseluce.com/enigaseluce-services/infoFattureRateizzabiliWSRest";
//	    URL_CALCOLA_PROPOSTA_RATE = "https://www.eni.mobi/dommPortalWEB/apprest/private/forwardplist.jsp?action=calcolaPropostaWSRest";
        URL_CALCOLA_PROPOSTA_RATE = "https://enigaseluce.com/enigaseluce-services/calcolaPropostaWSRest";
//	    URL_ATTIVA_PROPOSTA_RATEIZZAZIONE = "https://www.eni.mobi/dommPortalWEB/apprest/private/forwardplist.jsp?action=attivaPropostaRateizzazioneWSRest";
        URL_ATTIVA_PROPOSTA_RATEIZZAZIONE = "https://enigaseluce.com/enigaseluce-services/attivaPropostaRateizzazioneWSRest";
	}

    }

    // Lista cookie
    public static List<Cookie> LISTA_COOKIE;
    // variabili per i servizi
    public static String SERVIZIO_LOGIN = "";
    public static String SERVIZIO_STATO_AVANZAMENTO_FORNITURA = "";
    public static String SERVIZIO_FORNITURE_GAS_ATTIVE = "";
    public static String SERVIZIO_VISUALIZZA_FORNITURE_DOMICILIAZIONE = "";
    public static String SERVIZIO_ULTIMA_LETTURA = "";
    public static String SERVIZIO_STORICO_LETTURE = "";

    public static String SERVIZIO_SHOW_LISTA_FATTURE = "";
    public static String SERVIZIO_INFO_PROSSIMA_FATTURA = "";
    public static String SERVIZIO_DOWNLOAD_PDF = "";
    public static String SERVIZIO_INFO_FATTURA_ONLINE = "";
    public static String SERVIZIO_RICHIESTA_PRECONSENSO_FATTURA_ONLINE = "";
    public static String SERVIZIO_RICHIESTA_CONFERMA_MAIL = "";
    public static String SERVIZIO_GESTIONE_NOTIFICA_CELLULARE = "";

    public static String SERVIZIO_CONSUMI_LUCE = "";
    public static String SERVIZIO_CONSUMI_GAS = "";
    public static String SERVIZIO_INFO_FATTURE_RATEIZZABILI = "";
    public static String SERVIZIO_CALCOLA_PROPOSTA_RATE = "";

    // Memorizzazione user e password
    public static final String PREFS_NAME = "EniPrefsFile";

    // Funzionalit�
    public static final int FUNZIONALITA_STATO_AVANZAMENTO_FORNITURA = 1;
    public static final int FUNZIONALITA_DOMICILIAZIONE = 2;
    public static final int FUNZIONALITA_AUTOLETTURA = 3;
    public static final int FUNZIONALITA_STORICO_LETTURE = 4;
    public static final int FUNZIONALITA_BOLLETTE_PAGAMENTI = 6;
    public static final int FUNZIONALITA_ENI_WEB_BOLLETTA = 7;
    public static final int FUNZIONALITA_CONSUMI_LUCE = 8;
    public static final int FUNZIONALITA_CONSUMI_GAS = 9;

    // Piani di rateizzazione
    public static final int PIANO_RATEIZZAZIONE_SINGOLO = 1;
    public static final int PIANO_RATEIZZAZIONE_DOPPIO = 2;

    public static final String PIANO_SECONDO_DELIBERA = "01";
    public static final String PIANO_RITARDO_FATTURAZIONE = "02";

    // HELP
    public static final String ORIGINE_DOMICILIAZIONE = "origine_domiciliazione";
    public static final String ORIGINE_DOMICILIAZIONE_ACTIVATE_RESIDENZIALE = "origine_domiciliazione_activate_residenziale";
    public static final String ORIGINE_DOMICILIAZIONE_ACTIVATE_NON_RESIDENZIALE = "origine_domiciliazione_activate_non_residenziale";
    public static final String ORIGINE_DOMICILIAZIONE_DETAIL = "origine_domiciliazione_detail";
    public static final String ORIGINE_DOMICILIAZIONE_INSERIMENTO_PF = "origine_domiciliazione_inserimento_pf";
    public static final String ORIGINE_DOMICILIAZIONE_INSERIMENTO_PG = "origine_domiciliazione_inserimento_pg";
    public static final String ORIGINE_STORICO_LETTURE = "origine_storico_letture";
    public static final String ORIGINE_STORICO_LETTURE_DETAIL = "origine_storico_letture_detail";
    public static final String ORIGINE_AUTOLETTURA = "origine_autolettura";
    public static final String ORIGINE_AUTOLETTURA_INSERT = "origine_autolettura_insert";
    public static final String ORIGINE_BOLLETTE_PAGAMENTI = "origine_bollette_pagamenti";
    public static final String ORIGINE_BOLLETTE_PAGAMENTI_PROSSIMA_BOLLETTA = "origine_bollette_pagamenti_prossima_bolletta";
    public static final String ORIGINE_BOLLETTE_PAGAMENTI_RICERCA = "origine_bollette_pagamenti_ricerca";
    public static final String ORIGINE_BOLLETTE_PAGAMENTI_RICERCA_POST_INTERVALLO = "origine_bollette_pagamenti_ricerca_post_intervallo";
    public static final String ORIGINE_BOLLETTE_PAGAMENTI_RICERCA_INTERVALLO = "origine_bollette_pagamenti_ricerca_intervallo";
    public static final String ORIGINE_BOLLETTE_RATEIZZABILI = "origine_bollette_rateizzabili";
    public static final String ORIGINE_BOLLETTE_RATEIZZABILI_SELEZIONE_PIANO = "origine_bollette_rateizzabili_selezione_piano";
    public static final String ORIGINE_BOLLETTE_RATEIZZABILI_DETTAGLIO_PIANO = "origine_bollette_rateizzabili_dettaglio_piano";
    public static final String ORIGINE_WEBOLLETTA_ATTIVA = "origine_webolletta_attiva";
    public static final String ORIGINE_WEBOLLETTA = "origine_webolletta";
    public static final String ORIGINE_WEBOLLETTA_DETAIL = "origine_webolletta_detail";
    public static final String ORIGINE_WEBOLLETTA_ACTIVATE = "origine_webolletta_activate";
    public static final String ORIGINE_WEBOLLETTA_ATTIVAZIONE_POST_PRECONSENSO = "origine_webolletta_attivazione_post_preconsenso";
    public static final String ORIGINE_WEBOLLETTA_DISATTIVAZIONE_POST_PRECONSENSO = "origine_webolletta_disattivazione_post_preconsenso";
    public static final String ORIGINE_CONSUMI_LUCE = "origine_consumi_luce";
    public static final String ORIGINE_CONSUMI_LUCE_VALORI_RIFERIMENTO = "origine_consumi_luce_valori_riferimento";
    public static final String ORIGINE_CONSUMI_LUCE_GRAFICO_CONSUMI = "origine_consumi_luce_grafico_consumi";
    public static final String ORIGINE_CONSUMI_GAS = "origine_consumi_gas";
    public static final String ORIGINE_CONSUMI_GAS_GRAFICO_CONSUMI = "origine_consumi_gas_grafico_consumi";

    public static final String RETAIL_RIFERIMENTO_MINI = "Mini";
    public static final String RETAIL_RIFERIMENTO_MIDI = "Midi";
    public static final String RETAIL_RIFERIMENTO_MAXI = "Maxi";
    public static final String PMI_RIFERIMENTO_BASELOAD = "Baseload";
    public static final String PMI_RIFERIMENTO_PEAK = "Peak";
    public static final String PMI_RIFERIMENTO_OFFPEAK = "OffPeak";
    public static final String PMI_RIFERIMENTO_CIVILE = "Civile";
    public static final String PMI_RIFERIMENTO_CONDOMINIO = "Condominio";
    public static final String PMI_RIFERIMENTO_PARTITAIVA = "PartitaIva";
    public static final int RETAIL_PERIODO_MESI_6 = 6;
    public static final int RETAIL_PERIODO_MESI_12 = 12;
    public static final int RETAIL_PERIODO_MESI_24 = 24;
    public static final String ORIGINE_DOMICILIAZIONE_INSERIMENTO_NON_RESIDENZIALI = "ORIGINE_DOMICILIAZIONE_INSERIMENTO_NON_RESIDENZIALI";

    // costanti parametri invio servizio domiciliazione
    public static final String MESSAGGIO = "MESSAGGIO";
    public static final String TIPOLOGIA_UTENZA = "TIPOLOGIA_UTENZA";
    public static final String CODICE_CLIENTE = "codiceCliente";
    public static final String CODICE_CONTO = "codiceConto";
    public static final String CODICE_FISCALE_CLIENTE_CC = "codiceFiscaleClienteCC";
    public static final String NOME_CLIENTE_CC = "nomeClienteCC";
    public static final String COGNOME_CLIENTE_CC = "cognomeClienteCC";
    public static final String IBAN_CLIENTE_CC = "ibanClienteCC";
    public static final String PARTITA_IVA_CLIENTE_CC = "partitaIvaClienteCC";
    public static final String CF_MODIFICATO = "CF_modificato";
    public static final String PIVA_MODIFICATA = "PIVA_modificata";
    public static final String PROVINCIA = "provincia";
    public static final String CAP = "cap";
    public static final String CITTA = "citta";
    public static final String ESTENSIONE_NUM_CIVICO = "estensioneNumCivico";
    public static final String NUM_CIVICO = "numCivico";
    public static final String STRADA = "strada";
    public static final String TIPO_INDIRIZZO = "tipoIndirizzo";
    public static final String CODICE_FISCALE_CLIENTE = "codiceFiscaleCliente";
    public static final String PIVA_CLIENTE = "partitaIvaCliente";
    public static final String COGNOME_CLIENTE = "cognomeCliente";
    public static final String NOME_CLIENTE = "nomeCliente";
    public static final String USER_ID = "userId";
    public static final String TIPOLOGIA = "tipologia";
    public static final String TIPO_INTESTATARIO = "tipoIntestatario";
    public static final String TELEFONO_PRIMARIO = "telefonoPrimario";
    public static final String NOME_SOTTOSCRITTORE_CC = "nomeSottoscrittoreCC";
    public static final String COGNOME_SOTTOSCRITTORE_CC = "cognomeSottoscrittoreCC";
    public static final String CODICE_FISCALE_SOTTOSCRITTORE_CC = "codiceFiscaleSottoscrittoreCC";

    // info Utenze
    public static final int TIPOLOGIA_PERSONA_FISICA = 0;
    public static final int TIPOLOGIA_PERSONA_GIURIDICA = 1;
    public static final int TIPOLOGIA_NON_RESIDENZIALE = 2;
    public static final String TIPOLOGIA_RETAIL = "RETAIL";
    public static final String PFISICA = "PFISICA";
    public static final String PGIURIDICA = "PGIURIDICA";

    public enum TIPOLOGIA_COMMODITY {
	GAS, DUAL, LUCE
    }

    // maintenance constants
    public static final int MAINTENANCE_ATTIVA = 1;
    public static final int MAINTENANCE_DISATTIVA = 0;
    // public static final boolean MAINTENANCE_ACTIVE=false;



    public static final String ERROR_TYPE = "error_type";
    public static final String PREVIOUS_PAGE = "previous_page";

    public static final int ERROR_CONNECTION_NOT_AVAILABLE = 0;
    public static final int ERROR_SERVICE_NOT_AVAILABLE = 1;

    public static final int PAGE_STATO_ATTIVAZIONE_FORNITURA = 0;


}

// Maintenance PROD
// public static String URL_MAINTAINANCE_MODE =
// "http://www.famiglia.eni.it/dommPortalWEB/framework/forward/maintenance.jsp?portal=resid";
/******************************************* Indirizzi st ********************************************/
// Maintenance TEST
// public static String URL_MAINTAINANCE_MODE =
// "http://pre-famiglia.eni.it/dommPortalWEB/framework/forward/maintenance.jsp?portal=resid";
// public static String URL_LOGIN =
// "https://st-eni.eni.mobi/dommPortalWEB/apprest/private/forwardplist.jsp?action=loginWSRest";
// public static String URL_STATO_AVANZAMENTO_FORNITURE =
// "https://st-eni.eni.mobi/dommPortalWEB/apprest/private/forwardplist.jsp?action=statoAvanzamentoFornitureWSRest";
// public static String URL_VISUALIZZA_FORNITURE_DOMICILIAZIONE =
// "https://st-eni.eni.mobi/dommPortalWEB/apprest/private/forwardplist.jsp?action=visualizzaFornitureDomiciliazioneWSRest";
// public static String URL_EXECUTE_DOMICILIAZIONE_CONTO =
// "https://st-eni.eni.mobi/dommPortalWEB/apprest/private/forwardplist.jsp?action=executeDomiciliazioneContoWSRest";
// public static String URL_FORNITURE_GAS_ATTIVE =
// "https://st-eni.eni.mobi/dommPortalWEB/apprest/private/forwardplist.jsp?action=loadFornitureInfoGasWSRest";
// public static String URL_STORICO_LETTURE_GAS =
// "https://st-eni.eni.mobi/dommPortalWEB/apprest/private/forwardplist.jsp?action=storicoLettureGas";
// public static String URL_ULTIMA_LETTURA_GAS =
// "https://st-eni.eni.mobi/dommPortalWEB/apprest/private/forwardplist.jsp?action=ultimaLetturaGas";
// public static String URL_INVIA_AUTOLETTURA_GAS =
// "https://st-eni.eni.mobi/dommPortalWEB/apprest/private/forwardplist.jsp?action=invioAutolettura";
// public static String URL_ANNULLA_AUTOLETTURA_GAS =
// "https://st-eni.eni.mobi/dommPortalWEB/apprest/private/forwardplist.jsp?action=annullaAutolettura";
// public static String URL_CONFERMA_AUTOLETTURA_GAS =
// "https://st-eni.eni.mobi/dommPortalWEB/apprest/private/forwardplist.jsp?action=confermaAutolettura";
/************************************************ Rilascio 2 *********************************************************************************************/
// public static String URL_SHOW_LISTA_FATTURE =
// "https://st-eni.eni.mobi/dommPortalWEB/apprest/private/forwardplist.jsp?action=showListaFattureWSRest";
// public static String URL_INFO_PROSSIMA_FATTURA =
// "https://st-eni.eni.mobi/dommPortalWEB/apprest/private/forwardplist.jsp?action=infoProssimaFatturaWSRest";
// public static String URL_DOWNLOAD_PDF =
// "https://st-eni.eni.mobi/dommPortalWEB/apprest/private/forwardplist.jsp?action=downloadDocPdfWSRest";
// public static String URL_INFO_FATTURA_ONLINE =
// "https://st-eni.eni.mobi/dommPortalWEB/apprest/private/forwardplist.jsp?action=infoFOLSuContiWSRest";
// public static String URL_RICHIESTA_PRECONSENSO =
// "https://st-eni.eni.mobi/dommPortalWEB/apprest/private/forwardplist.jsp?action=richiestaPreConsensoFatturaOnlineWSRest";
// public static String URL_GESTIONE_NOTIFICA_CELLULARE =
// "https://st-eni.eni.mobi/dommPortalWEB/apprest/private/forwardplist.jsp?action=executeModificaNotificaSMSWSRest";
