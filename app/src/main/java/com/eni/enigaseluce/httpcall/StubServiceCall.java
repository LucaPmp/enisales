package com.eni.enigaseluce.httpcall;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.cookie.Cookie;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;

import com.eni.enigaseluce.autolettura.AutoLetturaInsert;
import com.eni.enigaseluce.autolettura.Autolettura;
import com.eni.enigaseluce.bollettePagamenti.BollettePagamenti;
import com.eni.enigaseluce.bollettePagamenti.BollettePagamentiDettaglioPiano;
import com.eni.enigaseluce.bollettePagamenti.BollettePagamentiProssimaBolletta;
import com.eni.enigaseluce.bollettePagamenti.BollettePagamentiRateizzate;
import com.eni.enigaseluce.bollettePagamenti.BollettePagamentiRicerca;
import com.eni.enigaseluce.bollettePagamenti.BollettePagamentiRicercaIntervallo;
import com.eni.enigaseluce.bollettePagamenti.BollettePagamentiRicercaPostIntervallo;
import com.eni.enigaseluce.consumigas.ConsumiGas;
import com.eni.enigaseluce.consumigas.ConsumiGasGraficoConsumi;
import com.eni.enigaseluce.consumiluce.ConsumiLuce;
import com.eni.enigaseluce.domiciliazione.DomiciliazioneInserimentoNonResidenziali;
import com.eni.enigaseluce.domiciliazione.DomiciliazioneInserimentoPersonaFisica;
import com.eni.enigaseluce.domiciliazione.DomiciliazioneInserimentoPersonaGiuridica;
import com.eni.enigaseluce.error.OopsPageLogin;
import com.eni.enigaseluce.error.OopsPageMain;
import com.eni.enigaseluce.home.Home;
import com.eni.enigaseluce.login.Login;
import com.eni.enigaseluce.statoavanzamentofornitura.StatoAvanzamentoFornitura;
import com.eni.enigaseluce.storicoletture.StoricoLettureDetail;
import com.eni.enigaseluce.utils.Log;
import com.eni.enigaseluce.webolletta.BollettaDigitaleAttivazioneNotifica;
import com.eni.enigaseluce.webolletta.WebollettaActivate;
import com.eni.enigaseluce.webolletta.WebollettaAttivazioneNotifica;
import com.eni.enigaseluce.webolletta.WebollettaAttivazionePostPreconsenso;
import com.eni.enigaseluce.webolletta.WebollettaDetail;
import com.eni.enigaseluce.webolletta.WebollettaDisattivazionePostPreconsenso;

public class StubServiceCall implements ServiceCall {

    private static final String SERVICE_CALL_STUB_TAG = "STUB_SERVICE_CALL";

    StubServiceCall() {
    }

    /**
     * Stub method for Login Service
     */
    @Override
    public String executeHttpsPostAuth(String urlString, String username, String password, final Login context) throws Exception {
	Log.d(SERVICE_CALL_STUB_TAG, "Login URL - executeHttpsPostAuth: urlString[" + urlString + "] - username[" + username + "] - password[" + password + "]");

	ArrayList<Cookie> listaCookies = new ArrayList<Cookie>();
	String resultString = "[" + "{" + "\"descEsito\":\"Chiamata effettuata correttamente\"," + "\"esito\":200" + "}," + "{" + "\"Strada\":\"DI VIGNA RIGACCI\"," + "\"UltimoAccesso\":\"21/10/2012\"," + "\"CodiceCliente\":\"800113102\"," + "\"CodiceFiscaleCliente\":\"GSINGL33P13H312C\"," + "\"NumCivico\":\"16\"," + "\"NomeCliente\":\"ANGELO\"," + "\"PhoneNumber\":\"0666154465\"," + "\"UserId\":\"test_app@test18.it\"," + "\"CellulareSiebel\":\"3395293376\"," + "\"StatoCliente\":\"ATTIVO\"," + "\"CognomeCliente\":\"GIOIOSA\"," + "\"Provincia\":\"RM\"," + "\"TipoIndirizzo\":\"VIA\"," + "\"IndirizzoResidenzaCliente\":\"VIA DI VIGNA RIGACCI 16 ROMA 00164 RM \"," + "\"Cap\":\"00164\"," + "\"CF_corretto\":\"false\"," + "\"PIVA_corretta\":\"false\"," + "\"SegmentoCliente\":\"NO RESIDENTIAL\"," + "\"TipologiaCliente\":\"RETAIL\"," + "\"Citta\":\"ROMA\"" + "}," + "[" + "{" + "\"CodiceConto\":\"800113102001\"," + "\"SocietaFornitrice\":\"800\"," + "\"StatoConto\":\"UTILIZZABILE\"," + "\"SistemaBilling\":\"NETA\"," + "\"CodiceContratto\":\"1-1635729\"" + "}," + "{" + "\"CodiceProdotto\":\"2010.4 - PPC11\"," + "\"IdServizio\":\"1-15AIM5M\"," + "\"IndirizzoCompleto\":\"VIALE DI FOCENE 40 FIUMICINO 00054 RM \"," + "\"TipoGaranzia\":\"DEPOSITO CAUZIONALE\"," + "\"DataAttivazione\":\"04/01/2011\"," + "\"Regione\":\"LAZIO\"," + "\"Tipologia\":\"GAS\"," + "\"TipoProdotto\":\"2010.4 - PPC11\"," + "\"Pdf\":\"8000010351\"," + "\"FrequenzaFatturazione\":\"BIMESTRALE\"," + "\"TipoUtilizzoGas\":\"\"," + "\"ConsumoPrevisto\":\"600\"" + "}" + "]," + "[" + "{" + "\"CodiceConto\":\"500040648530\"," + "\"StatoConto\":\"UTILIZZABILE\"," + "\"CodiceContratto\":\"\"" + "}" + "]," + "[" + "{" + "\"CodiceConto\":\"505345177552\"," + "\"SocietaFornitrice\":\"800\"," + "\"StatoConto\":\"UTILIZZABILE\"," + "\"SistemaBilling\":\"NETA\"," + "\"CodiceContratto\":\"1-1855131720\"" + "}," + "{" + "\"CodiceProdotto\":\"2010.2 - TR9\"," + "\"IdServizio\":\"1-UOHWFI\"," + "\"IndirizzoCompleto\":\"VIA DI VIGNA RIGACCI 16 ROMA 00164 RM \"," + "\"TipoGaranzia\":\"DEPOSITO CAUZIONALE\"," + "\"DataAttivazione\":\"10/01/2010\"," + "\"Regione\":\"LAZIO\"," + "\"Tipologia\":\"GAS\"," + "\"TipoProdotto\":\"2010.2 - TR9\"," + "\"Pdf\":\"1106453044\"," + "\"FrequenzaFatturazione\":\"QUADRIMESTRALE\"," + "\"TipoUtilizzoGas\":\"COTTURA CIBI\"," + "\"ConsumoPrevisto\":\"300\"" + "}" + "]," + "[" + "{" + "\"CodiceConto\":\"110645304401\"," + "\"SocietaFornitrice\":\"100\"," + "\"StatoConto\":\"NON UTILIZZABILE\"," + "\"SistemaBilling\":\"NETA\"," + "\"CodiceContratto\":\"110645304401\"" + "}," + "{" + "\"CodiceProdotto\":\"Regolata_GMM\"," + "\"IdServizio\":\"MIG14_1+3B7K+76-2-3UXIR\"," + "\"IndirizzoCompleto\":\"VIA DI VIGNA RIGACCI 16 ROMA 00164 RM \"," + "\"TipoGaranzia\":\"-\"," + "\"DataAttivazione\":\"04/15/1971\"," + "\"Regione\":\"LAZIO\"," + "\"Tipologia\":\"GAS\"," + "\"TipoProdotto\":\"Regolata_GMM\"," + "\"Pdf\":\"1106453044\"," + "\"FrequenzaFatturazione\":\"QUADRIMESTRALE\"," + "\"TipoUtilizzoGas\":\"COTTURA CIBI\"," + "\"ConsumoPrevisto\":\"53\"" + "}" + "]," + "[" + "{" + "\"CodiceConto\":\"130006498666\"," + "\"StatoConto\":\"UTILIZZABILE\"," + "\"CodiceContratto\":\"\"" + "}" + "]," + "[" + "{" + "\"CodiceConto\":\"111163439801\"," + "\"SocietaFornitrice\":\"100\"," + "\"StatoConto\":\"NON UTILIZZABILE\"," + "\"SistemaBilling\":\"NETA\"," + "\"CodiceContratto\":\"\"" + "}," + "{" + "\"CodiceProdotto\":\"Regolata_GMM\"," + "\"IdServizio\":\"MIG14_1+4B54+21-2-3UXIR\"," + "\"IndirizzoCompleto\":\"VIA CAME 8 FIUMICINO 00054 RM \"," + "\"TipoGaranzia\":\"DEPOSITO CAUZIONALE\"," + "\"DataAttivazione\":\"05/26/1995\"," + "\"Regione\":\"LAZIO\"," + "\"Tipologia\":\"GAS\"," + "\"TipoProdotto\":\"Regolata_GMM\"," + "\"Pdf\":\"1111634398\"," + "\"FrequenzaFatturazione\":\"QUADRIMESTRALE\"," + "\"TipoUtilizzoGas\":\"COTTURA CIBI\"," + "\"ConsumoPrevisto\":\"37\"" + "}" + "]" + "]";
	//String resultString = "[{\"descEsito\":\"Chiamata effettuata correttamente\",\"esito\":200},{\"Strada\":\"ENRICO FERMI\",\"UltimoAccesso\":\"9/5/2014\",\"CodiceCliente\":\"912001087473\",\"CF_corretto\":true,\"CodiceFiscaleCliente\":\"CHPSFN72T21A345O\",\"NumCivico\":\"7\",\"NomeCliente\":\"STEFANO\",\"PhoneNumber\":\"3471708066\",\"UserId\":\"stefchiap@yahoo.it\",\"CellulareSiebel\":\"0116810413\",\"StatoCliente\":\"ATTIVO\",\"CognomeCliente\":\"CHIAPPINI\",\"Provincia\":\"TO\",\"TipoIndirizzo\":\"VIA\",\"IndirizzoResidenzaCliente\":\"VIA ENRICO FERMI 7 MONCALIERI 10024 TO \",\"Cap\":\"10024\",\"PIVA_corretta\":true,\"SegmentoCliente\":\"RESIDENTIAL\",\"TipologiaCliente\":\"RETAIL\",\"Citta\":\"MONCALIERI\"},[{\"CodiceConto\":\"505358937496\",\"SocietaFornitrice\":\"800\",\"StatoConto\":\"UTILIZZABILE\",\"SistemaBilling\":\"NETA\",\"CodiceContratto\":\"1-3213452479\"},{\"CodiceProdotto\":\"2011.3 - TR14\",\"IdServizio\":\"1-1H573AN\",\"IndirizzoCompleto\":\"VIA ENRICO FERMI 7 MONCALIERI 10024 TO \",\"TipoGaranzia\":\"-\",\"DataAttivazione\":\"08/01/2011\",\"Regione\":\"PIEMONTE\",\"Tipologia\":\"GAS\",\"TipoProdotto\":\"2011.3 - TR14\",\"Pdf\":\"1207048371\",\"FrequenzaFatturazione\":\"BIMESTRALE\",\"TipoUtilizzoGas\":\"ACQUA CALDA + COTTURA CIBI + RISCALDAMENTO\",\"ConsumoPrevisto\":\"1208\"}],[{\"CodiceConto\":\"505359694930\",\"SocietaFornitrice\":\"800\",\"StatoConto\":\"UTILIZZABILE\",\"SistemaBilling\":\"NETA\",\"CodiceContratto\":\"1-3327177512\"},{\"CodiceProdotto\":\"2011.3 - LFRE03\",\"IdServizio\":\"1-1II902O\",\"IndirizzoCompleto\":\"VIA ENRICO FERMI 7 MONCALIERI 10024 TO \",\"TipoGaranzia\":\"-\",\"DataAttivazione\":\"11/01/2011\",\"Regione\":\"PIEMONTE\",\"Tipologia\":\"POWER\",\"TipoProdotto\":\"2011.3 - LFRE03\",\"Pdf\":\"8002363551\",\"FrequenzaFatturazione\":\"BIMESTRALE\",\"TipoUtilizzoGas\":\"\",\"ConsumoPrevisto\":\"1000\"}],[{\"CodiceConto\":\"120851196201\",\"SocietaFornitrice\":\"100\",\"StatoConto\":\"NON UTILIZZABILE\",\"SistemaBilling\":\"NETA\",\"CodiceContratto\":\"120851196201\"},{\"CodiceProdotto\":\"Regolata_GMM\",\"IdServizio\":\"MIG_2+5J3S+378-2-3UXIR\",\"IndirizzoCompleto\":\"VIA ARDUINO 49 TORINO 10134 TO \",\"TipoGaranzia\":\"-\",\"DataAttivazione\":\"05/26/2004\",\"Regione\":\"PIEMONTE\",\"Tipologia\":\"GAS\",\"TipoProdotto\":\"Regolata_GMM\",\"Pdf\":\"1208511962\",\"FrequenzaFatturazione\":\"BIMESTRALE\",\"TipoUtilizzoGas\":\"ACQUA CALDA + COTTURA CIBI + RISCALDAMENTO\",\"ConsumoPrevisto\":\"1039\"}]]";
	boolean success = true;
	if (success) {
	    Log.d(SERVICE_CALL_STUB_TAG, "Login SUCCESS Response - executeHttpsPostAuth: " + resultString);
	    context.postAuth(resultString, listaCookies);
	}
	else {
	    Log.d(SERVICE_CALL_STUB_TAG, "Login ERROR Response - executeHttpsPostAuth: " + resultString);
	    context.postLoginException(true);
	}

	return resultString;
    }

	@Override
	public String executeHttpsPostNoAuth(String urlString, String username, String password, Map<String, String> header, JSONObject body, Login context) throws Exception {
		return null;
	}



	/**
     * Metodo da chiamare per le chiamate dalla schermata di settings (Home)
     */
    @Override
    public void executeHttpsGet(String urlString, final String parametri, final Activity context, final int posizioneIcona, final Activity oops) throws Exception {
	Log.d(SERVICE_CALL_STUB_TAG, "HomeService URL - executeHttpsGet: urlString[" + urlString + "] - parametri[" + parametri + "] - posizioneIcona[" + posizioneIcona + "] - oops[" + oops + "]");

	ArrayList<Cookie> listaCookies = new ArrayList<Cookie>();
	HashMap<String, String> parameterHash = new HashMap<String, String>();
	parameterHash.put("PARAMETER", parametri);

	String resultString = "";
	JSONArray arrayResult = new JSONArray();

	switch (posizioneIcona) {
	case Constants.FUNZIONALITA_STATO_AVANZAMENTO_FORNITURA:
	    resultString = "";
	    break;
	case Constants.FUNZIONALITA_DOMICILIAZIONE:
	    resultString = "[" + "{" + "\"descEsito\":\"Chiamata effettuata correttamente\"," + "\"esito\":200" + "}," + "{" + "\"ContoCorrente\":\"000002549X35\"," + "\"NomeIntestatario\":\"ANGELO\"," + "\"NomeBanca\":\"BANCA POPOLARE DI SONDRIO\"," + "\"CodiceContoCliente\":\"800113102001\"," + "\"TipoDomiciliazione\":\"DOMICILIAZIONE\"," + "\"CognomeIntestatario\":\"GIOIOSA\"," + "\"StatoDomiciliazione\":\"RESPINTA\"" + "}" + "]";
	    break;
	case Constants.FUNZIONALITA_AUTOLETTURA:
	    resultString = "[{" + "\"descEsito\":\"Chiamata effettuata correttamente\"," + "\"esito\":200}," + "{\"Pdr\":\"00880000835030\"," + "\"NumCifreMisuratore\":\"5\"," + "\"TipoContatore\":\"026\"," + "\"CodiceContatore\":\"0032481424\"," + "\"Pdf\":\"8000010351\"," + "\"CorrettoreOmologato\":\"N\"" + "}]";
	    break;
	case Constants.FUNZIONALITA_STORICO_LETTURE:
	    resultString = "[{" + "\"descEsito\":\"Chiamata effettuata correttamente\"," + "\"esito\":200}," + "{\"Pdr\":\"00880000835030\"," + "\"NumCifreMisuratore\":\"5\"," + "\"TipoContatore\":\"026\"," + "\"CodiceContatore\":\"0032781777\"," + "\"Pdf\":\"8000010351\"," + "\"CorrettoreOmologato\":\"N\"" + "}]";
	    break;
	case Constants.FUNZIONALITA_BOLLETTE_PAGAMENTI:

	    resultString = arrayResult.toString();
	    break;
	case Constants.FUNZIONALITA_ENI_WEB_BOLLETTA:
	    resultString = arrayResult.toString();
	    break;
	case Constants.FUNZIONALITA_CONSUMI_LUCE:
	    resultString = arrayResult.toString();
	    break;
	case Constants.FUNZIONALITA_CONSUMI_GAS:
	    resultString = arrayResult.toString();
	    break;
	}

	boolean success = true;
	if (success) {
	    Log.d(SERVICE_CALL_STUB_TAG, "HomeService SUCCESS Response - executeHttpsGet: " + resultString);

	    if(context instanceof Home) {

	    	((Home)context).postChiamata(resultString, listaCookies, posizioneIcona, parameterHash);
	    	
	    }
	}
	else {
	    Log.d(SERVICE_CALL_STUB_TAG, "HomeService ERROR Response - executeHttpsGet: " + resultString);
	    
	    if(context instanceof Home) {

	    	((Home)context).postChiamataException(true, posizioneIcona, parametri, null);
	    
	    }
	}

    }

    /**
     * Stub method for Attiva Domiciliazione Service
     */
    public void executeHttpsGetDomiciliazione(String urlString, String parametri, HashMap<String, String> parametriHash, final Activity context, final int code, final Activity oops) throws Exception {
	Log.d(SERVICE_CALL_STUB_TAG, "AttivaDomiciliazione URL - executeHttpsGetDomiciliazione: urlString[" + urlString + "] - parametri[" + parametri + "] - parametriHash[" + parametriHash + "] - code[" + code + "] - oops[" + oops + "]");

	ArrayList<Cookie> listaCookies = new ArrayList<Cookie>();
	String resultString = "";

	boolean success = true;
	if (success) {

	    switch (code) {
	    case 1:

		resultString = "[" + "{" + "\"descEsito\":\"Richiesta effettuata correttamente\"," + "\"esito\":200" + "}" + "]";

		Log.d(SERVICE_CALL_STUB_TAG, "AttivaDomiciliazione SUCCESS Response - executeHttpsGetDomiciliazione: " + resultString);

		if (context instanceof DomiciliazioneInserimentoPersonaFisica) ((DomiciliazioneInserimentoPersonaFisica) context).postChiamata(resultString, listaCookies, code);
		else if (context instanceof DomiciliazioneInserimentoPersonaGiuridica) {
		    ((DomiciliazioneInserimentoPersonaGiuridica) context).postChiamata(resultString, listaCookies, code);
		}
		else
		    ((DomiciliazioneInserimentoNonResidenziali) context).postChiamata(resultString, listaCookies, code);

		break;

	    case 2:

		resultString = "[" + "{" + "\"descEsito\":\"Chiamata effettuata correttamente\"," + "\"esito\":200" + "}," + "{" + "\"ContoCorrente\":\"000002549X35\"," + "\"NomeIntestatario\":\"ANGELO\"," + "\"NomeBanca\":\"BANCA POPOLARE DI SONDRIO\"," + "\"CodiceContoCliente\":\"800113102001\"," + "\"TipoDomiciliazione\":\"DOMICILIAZIONE\"," + "\"CognomeIntestatario\":\"GIOIOSA\"," + "\"StatoDomiciliazione\":\"RESPINTA\"" + "}" + "]";

		Log.d(SERVICE_CALL_STUB_TAG, "AttivaDomiciliazione SUCCESS Response - executeHttpsGetDomiciliazione: " + resultString);

		if (context instanceof DomiciliazioneInserimentoPersonaFisica) ((DomiciliazioneInserimentoPersonaFisica) context).postChiamata(resultString, listaCookies, code);
		else if (context instanceof DomiciliazioneInserimentoPersonaGiuridica) {
		    ((DomiciliazioneInserimentoPersonaGiuridica) context).postChiamata(resultString, listaCookies, code);
		}
		else
		    ((DomiciliazioneInserimentoNonResidenziali) context).postChiamata(resultString, listaCookies, code);

		break;
	    }

	}
	else {

	    Log.d(SERVICE_CALL_STUB_TAG, "AttivaDomiciliazione ERROR Response - executeHttpsGetDomiciliazione: " + resultString);

	    if (context instanceof DomiciliazioneInserimentoPersonaFisica) ((DomiciliazioneInserimentoPersonaFisica) context).postChiamataException(true);
	    else if (context instanceof DomiciliazioneInserimentoPersonaGiuridica) {
		((DomiciliazioneInserimentoPersonaGiuridica) context).postChiamataException(true);
	    }
	    else
		((DomiciliazioneInserimentoNonResidenziali) context).postChiamataException(true);
	}
    }

    @Override
    public String executeHttpGet(String url) throws Exception {
	throw new UnsupportedOperationException("executeHttpGet - Operation not supported");
    }

    @Override
    public String executeHttpsPostAuth(String urlString, String username, String password, OopsPageLogin context) throws Exception {

	throw new UnsupportedOperationException("executeHttpsPostAuth - Operation not supported");
    }

    @Override
    public void executeHttpsGet(String urlString, HashMap<String, String> parametri, Home context, int posizioneIcona, Activity oops) throws Exception {
	throw new UnsupportedOperationException("executeHttpsGet - Operation not supported");
    }

	@Override
	public void executeHttpsGetDummyRes(String urlString, Login context, String token) throws Exception {

	}

	@Override
	public void executeHttpsGetMaintenance(String urlString, Activity context) throws Exception {

	}

	@Override
    public void executeHttpsGetAvanzamentoForniture(String urlString, HashMap<String, String> parametri, StatoAvanzamentoFornitura context, Activity oops) throws Exception {
	throw new UnsupportedOperationException("executeHttpsGetAvanzamentoForniture - Operation not supported");
    }

    @Override
    public void executeHttpsGetAutolettura(String urlString, HashMap<String, String> parametri, Autolettura context, String pdf, String pdr, String sistemaBilling, String societaFornitrice, String numeroCifreMisuratore, Activity oops) throws Exception {

	Log.d(SERVICE_CALL_STUB_TAG, "GetAutolettura URL - executeHttpsGetAutolettura: urlString[" + urlString + "] - parametri[" + parametri + "]");

	ArrayList<Cookie> listaCookies = new ArrayList<Cookie>();

	String resultString = "[{" + "\"descEsito\":\"Chiamata effettuata correttamente\"," + "\"esito\":200}," + "{\"DataLettura\":\"20/05/2013\"," + "\"StatoLettura\":\"VALIDA\"," + "\"ValoreLettura\":\"1599\"" + "}]";

	boolean success = true;

	if (success) {

	    Log.d(SERVICE_CALL_STUB_TAG, "GetAutolettura SUCCESS Response - executeHttpsGetAutolettura: " + resultString);

	    context.postChiamata(resultString, listaCookies, pdf, pdr, sistemaBilling, societaFornitrice, numeroCifreMisuratore, parametri.get("codiceCliente"));
	}
	else {

	    Log.d(SERVICE_CALL_STUB_TAG, "GetAutolettura ERROR Response - executeHttpsGetAutolettura: " + resultString);

	    context.postChiamataException(true, pdf, sistemaBilling, societaFornitrice, numeroCifreMisuratore, null);
	}
    }

    @Override
    public void executeHttpsGetAutoletturaFromStoricoDetail(String urlString, HashMap<String, String> parametri, StoricoLettureDetail context, String pdf, String pdr, String sistemaBilling, String societaFornitrice, String numeroCifreMisuratore, Activity oops) throws Exception {

	Log.d(SERVICE_CALL_STUB_TAG, "GetAutolettura URL - executeHttpsGetAutoletturaFromStoricoDetail: urlString[" + urlString + "] - parametri[" + parametri + "]");

	ArrayList<Cookie> listaCookies = new ArrayList<Cookie>();

	String resultString = "[{" + "\"descEsito\":\"Chiamata effettuata correttamente\"," + "\"esito\":200}," + "{\"DataLettura\":\"20/05/2013\"," + "\"StatoLettura\":\"VALIDA\"," + "\"ValoreLettura\":\"1599\"" + "}]";

	boolean success = true;

	if (success) {

	    Log.d(SERVICE_CALL_STUB_TAG, "GetAutolettura SUCCESS Response - executeHttpsGetAutoletturaFromStoricoDetail: " + resultString);

	    context.postChiamata(resultString, listaCookies, pdf, pdr, sistemaBilling, societaFornitrice, numeroCifreMisuratore, parametri.get("codiceCliente"));
	}
	else {

	    Log.d(SERVICE_CALL_STUB_TAG, "GetAutolettura ERROR Response - executeHttpsGetAutoletturaFromStoricoDetail: " + resultString);

	    context.postChiamataException(true, pdf, sistemaBilling, societaFornitrice, numeroCifreMisuratore, null);
	}
    }

    @Override
    public void executeHttpsGetAutoletturaInsert(String urlString, HashMap<String, String> parametri, AutoLetturaInsert context, Activity oops) throws Exception {
	throw new UnsupportedOperationException("executeHttpsGetAutoletturaInsert - Operation not supported");

    }

    @Override
    public void executeHttpsGetAutoletturaInsert(String urlString, HashMap<String, String> parametri, AutoLetturaInsert context, String annullaOrConferma, Activity oops) throws Exception {
	throw new UnsupportedOperationException("executeHttpsGetAutoletturaInsert - Operation not supported");

    }

    @Override
    public void executeHttpsGetStoricoLetture(String urlString, HashMap<String, String> parametri, AutoLetturaInsert context, String pdf, String pdr, Activity oops) throws Exception {

	Log.d(SERVICE_CALL_STUB_TAG, "GetStoricoLetture URL - executeHttpsGetStoricoLetture: urlString[" + urlString + "] - parametri[" + parametri + "]");

	ArrayList<Cookie> listaCookies = new ArrayList<Cookie>();

	String resultString = "" + "[{" + "\"descEsito\":\"Chiamata effettuata correttamente\"," + "\"esito\":200}," + "[{" + "\"DataLettura\":\"20/05/2013\"," + "\"StatoLettura\":\"Valida\"," + "\"ValoreLettura\":\"1599\"}," + "{\"DataLettura\":\"05/11/2012\"," + "\"StatoLettura\":\"Valida\"," + "\"ValoreLettura\":\"856\"}," + "{\"DataLettura\":\"15/11/2011\"," + "\"StatoLettura\":\"Valida\"," + "\"ValoreLettura\":\"88\"" + "}]" + "]";

	boolean success = true;

	if (success) {

	    Log.d(SERVICE_CALL_STUB_TAG, "GetStoricoLetture SUCCESS Response - executeHttpsGetStoricoLetture: " + resultString);

	    context.postChiamataStoricoLetture(resultString, listaCookies, pdf, pdr, parametri);
	}
	else {

	    Log.d(SERVICE_CALL_STUB_TAG, "GetStoricoLetture ERROR Response - executeHttpsGetStoricoLetture: " + resultString);

	    context.postChiamataStoricoLettureException(true);
	}
    }

    @Override
    public void executeHttpsGetBollettePagamenti(String urlString, HashMap<String, String> parametri, BollettePagamenti context, Activity oops, boolean prossimaFattura, boolean bolletteRateizzabili) throws Exception {

	Log.d(SERVICE_CALL_STUB_TAG, "GetBollettePagamenti URL - executeHttpsGetBollettePagamenti: urlString[" + urlString + "] - parametri[" + parametri + "]");

	ArrayList<Cookie> listaCookies = new ArrayList<Cookie>();

	String resultString = "[{\"descEsito\":\"Chiamata effettuata correttamente\",\"esito\":200},{\"esitoInfo\":true,\"periodoRiferimentoDal\":\"2013-09-25T00:00:00\",\"periodoRiferimentoA\":\"2013-11-24T00:00:00\",\"dataEmissioneProssimaFattura\":\"2013-11-24T00:00:00\"}]";

	boolean success = true;

	if (success) {

	    Log.d(SERVICE_CALL_STUB_TAG, "GetBollettePagamenti SUCCESS Response - executeHttpsGetBollettePagamenti: " + resultString);

	    if (context instanceof BollettePagamenti) ((BollettePagamenti) context).postChiamata(resultString, listaCookies, parametri, prossimaFattura, bolletteRateizzabili);
	}
	else {

	    Log.d(SERVICE_CALL_STUB_TAG, "GetBollettePagamenti ERROR Response - executeHttpsGetBollettePagamenti: " + resultString);

	    if (context instanceof BollettePagamenti) ((BollettePagamenti) context).postChiamataException(true, parametri, prossimaFattura, bolletteRateizzabili);
	}
    }

    @Override
    public void executeHttpsGetBollettePagamentiRicerca(String urlString, HashMap<String, String> parametri, BollettePagamentiRicerca context, Activity oops, String numeroFattura) throws Exception {
	throw new UnsupportedOperationException("executeHttpsGetBollettePagamentiRicerca - Operation not supported");
    }

    @Override
    public void executeHttpsGetBollettePagamentiRicercaPostIntervallo(String urlString, HashMap<String, String> parametri, BollettePagamentiRicercaPostIntervallo context, Activity oops, boolean isDownload, String numeroFattura) throws Exception {
	throw new UnsupportedOperationException("executeHttpsGetBollettePagamentiRicercaPostIntervallo - Operation not supported");
    }

    @Override
    public void executeHttpsGetBollettePagamentiRicercaIntervallo(String urlString, HashMap<String, String> parametri, BollettePagamentiRicercaIntervallo context, Activity oops) throws Exception {
	throw new UnsupportedOperationException("executeHttpsGetBollettePagamentiRicercaIntervallo - Operation not supported");
    }

    @Override
    public void executeHttpsGetProssimaBolletta(String urlString, String parametri, BollettePagamentiProssimaBolletta context, Activity oops) throws Exception {

	throw new UnsupportedOperationException("executeHttpsGetProssimaBolletta - Operation not supported");
    }

    @Override
    public void executeHttpsGetBollettePagamentiCalcolaPropostaRate(String urlString, HashMap<String, String> parametri, HashMap<String, String> parametriSecondario, BollettePagamentiRateizzate context, Activity oops, int tipoProposta) throws Exception {
	throw new UnsupportedOperationException("executeHttpsGetBollettePagamentiCalcolaPropostaRate - Operation not supported");
    }

    @Override
    public void executeHttpsGetBollettePagamentiCalcolaPropostaRateDettaglio(String urlString, HashMap<String, String> parametri, BollettePagamentiDettaglioPiano context, Activity oops, int tipoProposta, boolean reset) throws Exception {
	throw new UnsupportedOperationException("executeHttpsGetBollettePagamentiCalcolaPropostaRateDettaglio - Operation not supported");
    }

    @Override
    public void executeHttpsGetBollettePagamentiAttivaPropostaPiano(String urlString, HashMap<String, String> parametri, BollettePagamentiDettaglioPiano context, Activity oops, String accettaOModifica) throws Exception {
	throw new UnsupportedOperationException("executeHttpsGetBollettePagamentiAttivaPropostaPiano - Operation not supported");
    }

    @Override
    public void executeHttpsGetWebollettaActivateDom(String urlString, String parametri, WebollettaActivate context, Dialog dialog, Activity oops) throws Exception {
	throw new UnsupportedOperationException("executeHttpsGetWebollettaActivateDom - Operation not supported");
    }

    @Override
    public void executeHttpsGetWebollettaActivatePreconsenso(String urlString, String parametri, WebollettaActivate context, Activity oops) throws Exception {
	throw new UnsupportedOperationException("executeHttpsGetWebollettaActivatePreconsenso - Operation not supported");
    }

    @Override
    public void executeHttpsGetWebollettaAttivazioneNotifica(String urlString, String parametri, WebollettaAttivazioneNotifica context, Activity oops, boolean notifica, boolean aggiorna) throws Exception {
	throw new UnsupportedOperationException("executeHttpsGetWebollettaAttivazioneNotifica - Operation not supported");
    }

	@Override
	public void executeHttpsGetWebollettaAttivazioneNotifica(String urlString, String parametri, BollettaDigitaleAttivazioneNotifica context, Activity oops, boolean notifica, boolean aggiorna) throws Exception {
		throw new UnsupportedOperationException("executeHttpsGetWebollettaAttivazioneNotifica - Operation not supported");
	}

	@Override
    public void executeHttpsGetWebollettaActivatePostPreconsenso(String urlString, HashMap<String, String> parametri, WebollettaAttivazionePostPreconsenso context, Activity oops) throws Exception {
	throw new UnsupportedOperationException("executeHttpsGetWebollettaActivatePostPreconsenso - Operation not supported");
    }

    @Override
    public void executeHttpsGetWebollettaDisattivazionePostPreconsenso(String urlString, HashMap<String, String> parametri, WebollettaDisattivazionePostPreconsenso context, Activity oops) throws Exception {
	throw new UnsupportedOperationException("executeHttpsGetWebollettaDisattivazionePostPreconsenso - Operation not supported");
    }

	@Override
	public void executeHttpsGetBollettaDigitaleDisattivazionePostPreconsenso(String urlString, HashMap<String, String> parametri, BollettaDigitaleAttivazioneNotifica context, Activity oops) throws Exception {
		throw new UnsupportedOperationException("executeHttpsGetBollettaDigitaleDisattivazionePostPreconsenso - Operation not supported");
	}

	@Override
    public void executeHttpsGetWebollettaDetail(String urlString, HashMap<String, String> parametri, WebollettaDetail context, Activity oops) throws Exception {
	throw new UnsupportedOperationException("executeHttpsGetWebollettaDetail - Operation not supported");
    }

    @Override
    public void executeHttpsGetConsumiLuce(String urlString, String parametri, ConsumiLuce context, Activity oops) throws Exception {
	throw new UnsupportedOperationException("executeHttpsGetWebollettaDetail - Operation not supported");
    }

    @Override
    public void executeHttpsGetConsumiGas(String urlString, String parametri, ConsumiGas context, Activity oops) throws Exception {
	throw new UnsupportedOperationException("executeHttpsGetConsumiLuce - Operation not supported");
    }

    @Override
    public void executeHttpsGetConsumiGasGrafico(String urlString, String parametri, ConsumiGasGraficoConsumi context, Activity oops) throws Exception {
	throw new UnsupportedOperationException("executeHttpsGetConsumiGasGrafico - Operation not supported");
    }

    @Override
    public void executeHttpsGetProssimaBolletta(String urlString, HashMap<String, String> parametri, AutoLetturaInsert context, Activity oops, boolean prossimaFatttura, boolean bolletteRateizzabili) throws Exception {

	Log.d(SERVICE_CALL_STUB_TAG, "GetProssimaBolletta URL - executeHttpsGetProssimaBolletta: urlString[" + urlString + "] - parametri[" + parametri + "]");

	ArrayList<Cookie> listaCookies = new ArrayList<Cookie>();

	String resultString = "[{\"descEsito\":\"Chiamata effettuata correttamente\",\"esito\":200},{\"esitoInfo\":true,\"periodoRiferimentoDal\":\"2013-09-25T00:00:00\",\"periodoRiferimentoA\":\"2013-11-24T00:00:00\",\"dataEmissioneProssimaFattura\":\"2013-11-24T00:00:00\"}]";

	boolean success = true;

	if (success) {

	    Log.d(SERVICE_CALL_STUB_TAG, "GetBollettePagamenti SUCCESS Response - executeHttpsGetBollettePagamenti: " + resultString);

	    context.postChiamataProssimaBolletta(resultString, listaCookies, parametri, prossimaFatttura, bolletteRateizzabili);
	}
	else {

	    Log.d(SERVICE_CALL_STUB_TAG, "GetBollettePagamenti ERROR Response - executeHttpsGetBollettePagamenti: " + resultString);

	    context.postChiamataProssimaBollettaException(true, parametri, prossimaFatttura, bolletteRateizzabili);
	}
    }
}
