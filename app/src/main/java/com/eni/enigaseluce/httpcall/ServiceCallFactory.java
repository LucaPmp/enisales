package com.eni.enigaseluce.httpcall;

import com.eni.enigaseluce.utils.Log;

import android.content.Context;

public class ServiceCallFactory {

    private static final String SERVICE_CALL_FACTORY_TAG = "SERVICE_CALL_FACTORY_TAG";

    private ServiceCallFactory() {
    }

    public static ServiceCall createServiceCall(Context context) {
	if (Constants.STUB_ENABLED) {
	    Log.d(SERVICE_CALL_FACTORY_TAG, "new StubServiceCall()");
	    return new StubServiceCall();
	}
	else {
	    Log.d(SERVICE_CALL_FACTORY_TAG, "new ServiceCall()");
	    return new ServiceCallHttp(context);
	}
    }
}
