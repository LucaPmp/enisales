package com.eni.enigaseluce.httpcall;

import java.util.HashMap;

import com.eni.enigaseluce.model.InfoUtenzaEni;

public class ServiceCallAssembler {

    public static HashMap<String, String> fillParametersExecuteDomiciliazione(InfoUtenzaEni utenzaEni, boolean checkIntestatari, int tipologiaUtenza) {

	HashMap<String, String> parametriDomForn = new HashMap<String, String>();

	parametriDomForn.put(Constants.CODICE_CLIENTE, utenzaEni.getCodiceCliente());
	parametriDomForn.put(Constants.CODICE_CONTO, utenzaEni.getCodiceConto());
	parametriDomForn.put(Constants.USER_ID, utenzaEni.getUserId());
	parametriDomForn.put(Constants.TIPOLOGIA, utenzaEni.getTipologia());
	parametriDomForn.put(Constants.TELEFONO_PRIMARIO, utenzaEni.getTelefonoPrimario());
	parametriDomForn.put(Constants.IBAN_CLIENTE_CC, utenzaEni.getIbanClienteCC());

	// check tipologia
	if (utenzaEni.isRetail()) {
	    parametriDomForn.put(Constants.NOME_CLIENTE, utenzaEni.getNomeCliente());
	    parametriDomForn.put(Constants.COGNOME_CLIENTE, utenzaEni.getCognomeCliente());
	    parametriDomForn.put(Constants.CODICE_FISCALE_CLIENTE, utenzaEni.getCodiceFiscaleCliente());
	    parametriDomForn.put(Constants.TIPO_INTESTATARIO, utenzaEni.getTipoIntestatario());

	}
	else if (utenzaEni.isPmi()) {
	    parametriDomForn.put(Constants.COGNOME_CLIENTE, utenzaEni.getRagioneSocialeClienteCC());
	    parametriDomForn.put(Constants.PIVA_CLIENTE, utenzaEni.getPartitaIvaCliente());

	}

	switch (tipologiaUtenza) {
	case Constants.TIPOLOGIA_PERSONA_FISICA: // tipologia=RETAIL.
						 // segmento=RESIDENZIALE

	    parametriDomForn.put(Constants.NOME_CLIENTE_CC, utenzaEni.getNomeClienteCC());
	    parametriDomForn.put(Constants.COGNOME_CLIENTE_CC, utenzaEni.getCognomeClienteCC());
	    parametriDomForn.put(Constants.CODICE_FISCALE_CLIENTE_CC, utenzaEni.getCodiceFiscaleClienteCC());

	    if (checkIntestatari) {
		utenzaEni.setCf_Modificato("true");
	    }
	    else {
		utenzaEni.setCf_Modificato("false");
	    }
	    parametriDomForn.put(Constants.CF_MODIFICATO, utenzaEni.getCf_Modificato());

	    break;

	case Constants.TIPOLOGIA_PERSONA_GIURIDICA: // tipologia=PMI.
						    // segmento=RESIDENZIALE

	    parametriDomForn.put(Constants.COGNOME_CLIENTE_CC, utenzaEni.getRagioneSocialeClienteCC());
	    parametriDomForn.put(Constants.PARTITA_IVA_CLIENTE_CC, utenzaEni.getPartitaIvaClienteCC());
	    parametriDomForn.put(Constants.NOME_SOTTOSCRITTORE_CC, utenzaEni.getNomeSottoscrittoreCC());
	    parametriDomForn.put(Constants.COGNOME_SOTTOSCRITTORE_CC, utenzaEni.getCognomeSottoscrittoreCC());
	    parametriDomForn.put(Constants.CODICE_FISCALE_SOTTOSCRITTORE_CC, utenzaEni.getCodiceFiscaleSottoscrittoreCC());
	    break;

	case Constants.TIPOLOGIA_NON_RESIDENZIALE: // tipologia=PMI.
						   // segmento=NONRESIDENZIALE

	    parametriDomForn.put(Constants.COGNOME_CLIENTE_CC, utenzaEni.getRagioneSocialeClienteCC());
	    parametriDomForn.put(Constants.PARTITA_IVA_CLIENTE_CC, utenzaEni.getPartitaIvaClienteCC());
	    parametriDomForn.put(Constants.NOME_SOTTOSCRITTORE_CC, utenzaEni.getNomeSottoscrittoreCC());
	    parametriDomForn.put(Constants.COGNOME_SOTTOSCRITTORE_CC, utenzaEni.getCognomeSottoscrittoreCC());
	    parametriDomForn.put(Constants.CODICE_FISCALE_SOTTOSCRITTORE_CC, utenzaEni.getCodiceFiscaleSottoscrittoreCC());

	    if (checkIntestatari) {
		utenzaEni.setPiva_Modificata("true");
	    }
	    else {
		utenzaEni.setPiva_Modificata("false");
	    }
	    parametriDomForn.put(Constants.PIVA_MODIFICATA, utenzaEni.getPiva_Modificata());

	    break;
	}

	if (!utenzaEni.getTipoIndirizzo().equals("")) {
	    parametriDomForn.put(Constants.TIPO_INDIRIZZO, utenzaEni.getTipoIndirizzo());
	}
	if (!utenzaEni.getStrada().equals("")) {
	    parametriDomForn.put(Constants.STRADA, utenzaEni.getStrada());
	}
	if (!utenzaEni.getNumCivico().equals("")) {
	    parametriDomForn.put(Constants.NUM_CIVICO, utenzaEni.getNumCivico());
	}
	if (!utenzaEni.getEstensioneNumCivico().equals("")) {
	    parametriDomForn.put(Constants.ESTENSIONE_NUM_CIVICO, utenzaEni.getEstensioneNumCivico());
	}
	if (!utenzaEni.getCitta().equals("")) {
	    parametriDomForn.put(Constants.CITTA, utenzaEni.getCitta());
	}
	if (!utenzaEni.getCap().equals("")) {
	    parametriDomForn.put(Constants.CAP, utenzaEni.getCap());
	}
	if (!utenzaEni.getProvincia().equals("")) {
	    parametriDomForn.put(Constants.PROVINCIA, utenzaEni.getProvincia());
	}

	return parametriDomForn;
    }

}
