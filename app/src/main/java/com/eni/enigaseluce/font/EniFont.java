package com.eni.enigaseluce.font;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class EniFont extends TextView {

    public EniFont(Context context, AttributeSet attrs, int defStyle) {
	super(context, attrs, defStyle);
    }

    public EniFont(Context context, AttributeSet attrs) {
	super(context, attrs);
    }

    public EniFont(Context context) {
	super(context);
    }

    public void setTypeface(Typeface tf, int style) {
	if (style == Typeface.BOLD) {
	    super.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/EniExpBol.otf"));
	}
	else {
	    super.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/EniExpReg.otf"));
	}
    }
}