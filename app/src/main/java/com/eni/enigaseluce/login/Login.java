package com.eni.enigaseluce.login;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;
import java.util.regex.Pattern;

import org.apache.http.cookie.Cookie;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Base64;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.eni.enigaseluce.EniApplication;
import com.eni.enigaseluce.R;
import com.eni.enigaseluce.error.OopsPage;
import com.eni.enigaseluce.error.OopsPageLogin;
import com.eni.enigaseluce.error.Utils;
import com.eni.enigaseluce.home.Home;
import com.eni.enigaseluce.httpcall.Constants;
import com.eni.enigaseluce.httpcall.ServiceCall;
import com.eni.enigaseluce.httpcall.ServiceCallFactory;
import com.eni.enigaseluce.login.help.ClienteEni;
import com.eni.enigaseluce.login.help.EniFamiglia;
import com.eni.enigaseluce.login.help.LoginHelp;
import com.eni.enigaseluce.login.help.Offerte;
import com.eni.enigaseluce.login.help.Registrati;
import com.eni.enigaseluce.login.help.TrovaEni;
import com.eni.enigaseluce.showcase.Showcase;
import com.eni.enigaseluce.utils.Log;
import com.eni.enigaseluce.utils.Utilities;

@SuppressLint("NewApi")
public class Login extends Activity {

    private static final String TAG = "Login";
//    private static final String ID_APP = "EniAppl004";
    private static final String ID_APP = "eniselfcare.app";

    private Context context;
    final Activity activity = this;
    boolean accedi_help = false;
    private static String usernameInserted = "";
    private static String passwordInserted = "";
    AnimationDrawable animation;
    ImageView imageAnim;
    CheckBox checkBox;
    SharedPreferences settings;
    SharedPreferences.Editor editor;
    String user;
    String password;
    Typeface tf;
    Typeface tfb;
    private Showcase myShowcase;
    private TextView forgotUsername, forgotPassword, registrationHint;
    private Button offerteEni;

    private String DEVICE_ID;
    private final String SALT="4030D3096DC5303C5D4159447CCC4F85";
    private EditText usernameInput;
    private EditText passwordInput;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_pre);

        context = this;
        tf = Typeface.createFromAsset(this.getAssets(), "fonts/EniExpReg.otf");
        tfb = Typeface.createFromAsset(this.getAssets(), "fonts/EniExpBol.otf");

        forgotUsername = (TextView) findViewById(R.id.forgot_username);
        forgotPassword = (TextView) findViewById(R.id.forgot_password);
        registrationHint = (TextView) findViewById(R.id.registration_hint);
        offerteEni = (Button) findViewById(R.id.offerte_eni);

        DEVICE_ID = Settings.Secure.getString(this.getBaseContext().getContentResolver(), Settings.Secure.ANDROID_ID);
//        DEVICE_ID="optional";

        offerteEni.setTypeface(tfb);
        offerteEni.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                animation = Utilities.animation(Login.this);
                imageAnim = (ImageView) findViewById(R.id.waiting_anim);
                imageAnim.setVisibility(View.VISIBLE);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    imageAnim.setBackgroundDrawable(animation);
                } else {
                    imageAnim.setBackground(animation);
                }
                animation.start();

                if (!isNetworkAvailable(Login.this)) {
                    Intent intentOops = new Intent(getApplicationContext(), OopsPage.class);
                    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.connessione_assente));
                    // LoginHelp.this.finish();
                    if (animation != null) {
                        animation.stop();
                        imageAnim.setVisibility(View.INVISIBLE);
                    }
                    startActivityForResult(intentOops, OopsPage.ESCI);
                    overridePendingTransition(R.anim.fade, R.anim.hold);
                    if (animation != null) {
                        animation.stop();
                        imageAnim.setVisibility(View.INVISIBLE);
                    }

                } else {
                    Intent intentOfferte = new Intent(getApplicationContext(), Offerte.class);
                    intentOfferte.putExtra("Help_Accedi", getIntent().getBooleanExtra("Help_Accedi", false));
                    intentOfferte.putExtra("USERNAME", getIntent().getStringExtra("USERNAME"));
                    intentOfferte.putExtra("PASSWORD", getIntent().getStringExtra("PASSWORD"));
                    Login.this.finish();
                    startActivity(intentOfferte);
                    overridePendingTransition(R.anim.fade, R.anim.hold);
                }
            }
        });

        forgotUsername.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                animation = Utilities.animation(Login.this);
                imageAnim = (ImageView) findViewById(R.id.waiting_anim);
                imageAnim.setVisibility(View.VISIBLE);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    imageAnim.setBackgroundDrawable(animation);
                } else {
                    imageAnim.setBackground(animation);
                }
                animation.start();

                if (!isNetworkAvailable(Login.this)) {
                    Intent intentOops = new Intent(getApplicationContext(), OopsPage.class);
                    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.connessione_assente));
                    // LoginHelp.this.finish();
                    if (animation != null) {
                        animation.stop();
                        imageAnim.setVisibility(View.INVISIBLE);
                    }
                    startActivityForResult(intentOops, OopsPage.ESCI);
                    overridePendingTransition(R.anim.fade, R.anim.hold);
                } else {
                    ((EniApplication) getApplication()).sendAnalyticsEvent("[Android] - hai dimenticato la username");
                    Intent intentEniFamiglia = new Intent(getApplicationContext(), EniFamiglia.class);
                    intentEniFamiglia.putExtra("Help_Accedi", true);
                    intentEniFamiglia.putExtra("USERNAME", getIntent().getStringExtra("USERNAME"));
                    intentEniFamiglia.putExtra("PASSWORD", getIntent().getStringExtra("PASSWORD"));
                    Login.this.finish();
                    startActivity(intentEniFamiglia);
                    overridePendingTransition(R.anim.fade, R.anim.hold);
                }
            }
        });

        forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                animation = Utilities.animation(Login.this);
                imageAnim = (ImageView) findViewById(R.id.waiting_anim);
                imageAnim.setVisibility(View.VISIBLE);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    imageAnim.setBackgroundDrawable(animation);
                } else {
                    imageAnim.setBackground(animation);
                }
                animation.start();

                if (!isNetworkAvailable(Login.this)) {
                    Intent intentOops = new Intent(getApplicationContext(), OopsPage.class);
                    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.connessione_assente));
                    // LoginHelp.this.finish();
                    if (animation != null) {
                        animation.stop();
                        imageAnim.setVisibility(View.INVISIBLE);
                    }
                    startActivityForResult(intentOops, OopsPage.ESCI);
                    overridePendingTransition(R.anim.fade, R.anim.hold);
                } else {
                    ((EniApplication) getApplication()).sendAnalyticsEvent("[Android] - hai dimenticato la password");
                    Intent intentEniFamiglia = new Intent(getApplicationContext(), EniFamiglia.class);
                    intentEniFamiglia.putExtra("Help_Accedi", true);
                    intentEniFamiglia.putExtra("USERNAME", getIntent().getStringExtra("USERNAME"));
                    intentEniFamiglia.putExtra("PASSWORD", getIntent().getStringExtra("PASSWORD"));
                    Login.this.finish();
                    startActivity(intentEniFamiglia);
                    overridePendingTransition(R.anim.fade, R.anim.hold);
                }
            }
        });

        registrationHint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                animation = Utilities.animation(Login.this);
                imageAnim = (ImageView) findViewById(R.id.waiting_anim);
                imageAnim.setVisibility(View.VISIBLE);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    imageAnim.setBackgroundDrawable(animation);
                } else {
                    imageAnim.setBackground(animation);
                }
                animation.start();

                if (!isNetworkAvailable(Login.this)) {
                    Intent intentOops = new Intent(getApplicationContext(), OopsPage.class);
                    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.connessione_assente));
                    // LoginHelp.this.finish();
                    startActivityForResult(intentOops, OopsPage.ESCI);
                    overridePendingTransition(R.anim.fade, R.anim.hold);
                    if (animation != null) {
                        animation.stop();
                        imageAnim.setVisibility(View.INVISIBLE);
                    }
                } else {
                    Intent intentTrovaEni = new Intent(getApplicationContext(), Registrati.class);
                    intentTrovaEni.putExtra("Help_Accedi", true);
                    intentTrovaEni.putExtra("USERNAME", getIntent().getStringExtra("USERNAME"));
                    intentTrovaEni.putExtra("PASSWORD", getIntent().getStringExtra("PASSWORD"));
                    Login.this.finish();
                    startActivity(intentTrovaEni);
                    overridePendingTransition(R.anim.fade, R.anim.hold);
                }
            }
        });

        Log.d("LOGIN", "Android System API Level: " + Build.VERSION.SDK_INT);


        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
            myShowcase = new Showcase(getApplicationContext(), this);
        }


        // Restore preferences
        settings = getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
        editor = settings.edit();
        user = settings.getString("User", "");
        password = settings.getString("Password", "");

        usernameInput = (EditText) findViewById(R.id.username);
        usernameInput.setTypeface(tf);
        passwordInput = (EditText) findViewById(R.id.password);
        passwordInput.setTypeface(tf);
        checkBox = (CheckBox) findViewById(R.id.checkbox);
        checkBox.setTypeface(tf);
        if (!user.equals("") || !password.equals("")) {
            checkBox.setChecked(true);
            usernameInput.setText((getIntent().getStringExtra("USERNAME") == null) ? user : getIntent().getStringExtra("USERNAME"));
            passwordInput.setText((getIntent().getStringExtra("PASSWORD") == null) ? password : getIntent().getStringExtra("PASSWORD"));
        } else if (getIntent().getStringExtra("USERNAME") != null || getIntent().getStringExtra("PASSWORD") != null) {
            usernameInput.setText(getIntent().getStringExtra("USERNAME"));
            passwordInput.setText(getIntent().getStringExtra("PASSWORD"));
            checkBox.setChecked(getIntent().getBooleanExtra("CHECKBOX", false));
        }
        if (getIntent().getBooleanExtra("Help_Accedi", false)) {
            RelativeLayout login_help_btn1 = (RelativeLayout) findViewById(R.id.login_help_btn);
//            login_help_btn1.setVisibility(View.VISIBLE);
            accedi_help = true;
        }

        // InputMethodManager imm = (InputMethodManager)
        // getSystemService(Context.INPUT_METHOD_SERVICE);
        // imm.showSoftInput(usernameInput, InputMethodManager.SHOW_IMPLICIT);
        // imm.showSoftInput(passwordInput, InputMethodManager.SHOW_IMPLICIT);
    }

    @Override
    protected void onStart() {
        super.onStart();
        final EditText usernameInput = (EditText) findViewById(R.id.username);
        // usernameInput.setText("prova_siebel_2@cambio.it");
        // usernameInput.setText("test45@test.it");
        final EditText passwordInput = (EditText) findViewById(R.id.password);
        // passwordInput.setText("customer");
        usernameInput.setFocusable(true);
        usernameInput.setFocusableInTouchMode(true); // user touches widget on
        // phone with touch screen
        usernameInput.setClickable(true); // user navigates with wheel and
        // selects widget

        passwordInput.setFocusable(true);
        passwordInput.setFocusableInTouchMode(true); // user touches widget on
        // phone with touch screen
        passwordInput.setClickable(true); // user navigates with wheel and
        // selects widget

        final Button accedi = (Button) findViewById(R.id.accedi);
        accedi.setTypeface(tfb);
        accedi.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
        accedi.getBackground().invalidateSelf();
        accedi.setClickable(true);
        checkBox = (CheckBox) findViewById(R.id.checkbox);
        checkBox.setClickable(true);

        final RelativeLayout clienteEni = (RelativeLayout) findViewById(R.id.login_cliente_eni);
        clienteEni.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
        clienteEni.getBackground().invalidateSelf();
        clienteEni.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                clienteEni.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
                clienteEni.getBackground().invalidateSelf();

                animation = Utilities.animation(activity);
                imageAnim = (ImageView) findViewById(R.id.waiting_anim);
                imageAnim.setVisibility(View.VISIBLE);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    imageAnim.setBackgroundDrawable(animation);
                } else {
                    imageAnim.setBackground(animation);
                }
                animation.start();

                Intent intentClienteEni = new Intent(getApplicationContext(), ClienteEni.class);
                intentClienteEni.putExtra("Help_Accedi", (getIntent().getBooleanExtra("Help_Accedi", false)) ? getIntent().getBooleanExtra("Help_Accedi", false) : accedi_help);
                usernameInserted = usernameInput.getText().toString();
                passwordInserted = passwordInput.getText().toString();
                intentClienteEni.putExtra("USERNAME", usernameInserted);
                intentClienteEni.putExtra("PASSWORD", passwordInserted);
                Login.this.finish();
                startActivity(intentClienteEni);
                overridePendingTransition(R.anim.fade, R.anim.hold);
            }
        });

        final RelativeLayout energyStore = (RelativeLayout) findViewById(R.id.energy_store);
        energyStore.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
        energyStore.getBackground().invalidateSelf();
        energyStore.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v1) {
                energyStore.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
                energyStore.getBackground().invalidateSelf();

                animation = Utilities.animation(Login.this);
                imageAnim = (ImageView) findViewById(R.id.waiting_anim);
                imageAnim.setVisibility(View.VISIBLE);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    imageAnim.setBackgroundDrawable(animation);
                } else {
                    imageAnim.setBackground(animation);
                }
                animation.start();

                if (!isNetworkAvailable(Login.this)) {
                    Intent intentOops = new Intent(getApplicationContext(), OopsPage.class);
                    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.connessione_assente));
                    // LoginHelp.this.finish();
                    startActivityForResult(intentOops, OopsPage.ESCI);
                    overridePendingTransition(R.anim.fade, R.anim.hold);
                    if (animation != null) {
                        animation.stop();
                        imageAnim.setVisibility(View.INVISIBLE);
                    }
                } else {
                    ((EniApplication) getApplication()).sendAnalyticsEvent("[Android] - trova l'energy store");
                    Intent intentTrovaEni = new Intent(getApplicationContext(), TrovaEni.class);
                    intentTrovaEni.putExtra("Help_Accedi", getIntent().getBooleanExtra("Help_Accedi", false));
                    intentTrovaEni.putExtra("USERNAME", getIntent().getStringExtra("USERNAME"));
                    intentTrovaEni.putExtra("PASSWORD", getIntent().getStringExtra("PASSWORD"));
                    Login.this.finish();
                    intentTrovaEni.putExtra("PAGINA_DI_PROVENIENZA", Login.class.getCanonicalName());
                    startActivity(intentTrovaEni);
                    overridePendingTransition(R.anim.fade, R.anim.hold);
                }
            }
        });

        final RelativeLayout help = (RelativeLayout) findViewById(R.id.login_help_btn);
        help.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
        help.getBackground().invalidateSelf();
        help.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v1) {
                help.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
                help.getBackground().invalidateSelf();

                animation = Utilities.animation(Login.this);
                imageAnim = (ImageView) findViewById(R.id.waiting_anim);
                imageAnim.setVisibility(View.VISIBLE);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    imageAnim.setBackgroundDrawable(animation);
                } else {
                    imageAnim.setBackground(animation);
                }
                animation.start();

                Intent intentHelp = new Intent(getApplicationContext(), LoginHelp.class);
                usernameInserted = usernameInput.getText().toString();
                passwordInserted = passwordInput.getText().toString();
                intentHelp.putExtra("USERNAME", usernameInserted);
                intentHelp.putExtra("PASSWORD", passwordInserted);
                Login.this.finish();
                startActivity(intentHelp);
                overridePendingTransition(R.anim.fade, R.anim.hold);
            }
        });

        accedi.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {



                accedi.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
                accedi.getBackground().invalidateSelf();
                boolean userInserito = !usernameInput.getText().toString().trim().equals("");
                boolean pwdInserito = !passwordInput.getText().toString().trim().equals("");

                if (userInserito&&pwdInserito&&isValidEmail(usernameInput.getText().toString().trim())) {
                    // controllo della connettivita
                    if (!isNetworkAvailable(Login.this)) {
                        usernameInserted = usernameInput.getText().toString();
                        passwordInserted = passwordInput.getText().toString();
                        Intent intentOops = new Intent(getApplicationContext(), OopsPageLogin.class);
                        intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.connessione_assente));
                        intentOops.putExtra("USERNAME", usernameInserted);
                        intentOops.putExtra("PASSWORD", passwordInserted);
                        intentOops.putExtra("CHECKBOX", checkBox.isChecked());
                        riabilitaCampi();
                        Login.this.finish();
                        startActivity(intentOops);
                        overridePendingTransition(R.anim.fade, R.anim.hold);
                    } else {

                        usernameInserted = usernameInput.getText().toString();
                        passwordInserted = passwordInput.getText().toString();

                        callSiteminder();

                    }

                } else {


//                    String email = usernameInput.getText().toString().trim();


                    if(!userInserito) {
                        Utils.popup(Login.this, getResources().getString(R.string.errore_user_assente), "Riprova");
                    }
                    else if(!isValidEmail(usernameInput.getText().toString().trim()))
                    {
                        Utils.popup(Login.this, getResources().getString(R.string.errore_user_non_valido), "Riprova");


                    }
                    else if(!pwdInserito) {
                        Utils.popup(Login.this, getResources().getString(R.string.errore_pwd_assente), "Riprova");
                    }




                    RelativeLayout login_help_btn1 = (RelativeLayout) findViewById(R.id.login_help_btn);
//                    login_help_btn1.setVisibility(View.VISIBLE);
                    accedi_help = true;
                    accedi.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
                    accedi.getBackground().invalidateSelf();
                }

            }
        });

    }

    private boolean isValidEmail(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }


    private void doLogin() {





        user = settings.getString("User", "");
        password = settings.getString("Password", "");

        usernameInput.setFocusable(false);
        usernameInput.setFocusableInTouchMode(false); // user
        // touches
        // widget
        // on
        // phone
        // with
        // touch
        // screen
        usernameInput.setClickable(false); // user navigates
        // with wheel and
        // selects widget

        passwordInput.setFocusable(false);
        passwordInput.setFocusableInTouchMode(false); // user
        // touches
        // widget
        // on
        // phone
        // with
        // touch
        // screen
        passwordInput.setClickable(false); // user navigates
        // with wheel and
        // selects widget

        final Button accedi = (Button) findViewById(R.id.accedi);
        accedi.setClickable(false);
        checkBox.setClickable(false);
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
            myShowcase.setSwitcherState(false);
        }

        final RelativeLayout clienteEni = (RelativeLayout) findViewById(R.id.login_cliente_eni);
        clienteEni.setClickable(false);
        final RelativeLayout help = (RelativeLayout) findViewById(R.id.login_help_btn);
        help.setClickable(false);

        animation = Utilities.animation(Login.this);
        imageAnim = (ImageView) findViewById(R.id.waiting_anim);
        imageAnim.setVisibility(View.VISIBLE);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
            imageAnim.setBackgroundDrawable(animation);
        } else {
            imageAnim.setBackground(animation);
        }
        animation.start();

        checkBox = (CheckBox) findViewById(R.id.checkbox);
        if (checkBox.isChecked()) {
            checkBox.setChecked(true);
        } else {
            checkBox.setChecked(false);
        }
        /********************* Chiamata Login ******************************/
        ServiceCall loginServiceCall = ServiceCallFactory.createServiceCall(context);
        // loginServiceCall.myCookieStore = new
        // PersistentCookieStore(context);
        String rispostaLogin = "";
        try {

            if(Constants.TEST_ENV) {
                rispostaLogin = loginServiceCall.executeHttpsPostAuth(Constants.URL_LOGIN_TEST, usernameInserted, passwordInserted, Login.this);
            }
            else {
                rispostaLogin = loginServiceCall.executeHttpsPostAuth(Constants.URL_LOGIN_PROD, usernameInserted, passwordInserted, Login.this);
            }

        } catch (Exception e) {
            Intent intentOops = new Intent(getApplicationContext(), OopsPageLogin.class);
            intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
            intentOops.putExtra("USERNAME", usernameInserted);
            intentOops.putExtra("PASSWORD", passwordInserted);
            intentOops.putExtra("CHECKBOX", checkBox.isChecked());
            riabilitaCampi();
            Login.this.finish();
            startActivity(intentOops);
            overridePendingTransition(R.anim.fade, R.anim.hold);
        }

    }

    public String nowUTC()
    {
//        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        final SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyyHHmmss");

        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        final String utcTime = sdf.format(new Date());

        return utcTime;
    }

    private void callSiteminder() {
        Log.d(TAG, "inizio chiamata Siteminder");
        ServiceCall siteminderServiceCall = ServiceCallFactory.createServiceCall(context);
        try {
            String nowUtc = this.nowUTC();
            JSONObject bodyContent = new JSONObject();
            bodyContent.put("deviceid", DEVICE_ID);
            bodyContent.put("idapp", ID_APP);
            bodyContent.put("operation", "authenticate");
            bodyContent.put("password", passwordInserted);
//            bodyContent.put("secondarydeviceid", "optional");

            MessageDigest digest = MessageDigest.getInstance("SHA-256");

            String signature = SALT+usernameInserted+passwordInserted+"authenticate"+DEVICE_ID+nowUtc+ID_APP;


            Log.d(TAG, "signature prima di hash: " + signature);

            String signaturePostHash = generateHash(signature);

            Log.d(TAG, "signature post hash: " + signaturePostHash);

            bodyContent.put("signature", signaturePostHash);
//            bodyContent.put("timestamp", "2016-05-11T13:43:37.801Z");
            bodyContent.put("timestamp", nowUtc);
            bodyContent.put("tokenid", "");
            bodyContent.put("user", usernameInserted);


            HashMap<String,String> headers= new HashMap<String, String>();
            headers.put("content-type", "application/json");
            headers.put("accept", "application/json");
            headers.put("signresponse", "false");

            Log.d(TAG, bodyContent.toString());


            if(Constants.TEST_ENV)
            {
                siteminderServiceCall.executeHttpsPostNoAuth(Constants.URL_SITEMINDER_TEST, usernameInserted, passwordInserted, headers, bodyContent, Login.this);
            }
            else
            {
                siteminderServiceCall.executeHttpsPostNoAuth(Constants.URL_SITEMINDER_PROD, usernameInserted, passwordInserted, headers, bodyContent, Login.this);
            }


        } catch (Exception e) {
            Log.d(TAG, "eccezione chiamata siteminder " + e.getMessage());
        }


        Log.d(TAG,"fine chiamata Siteminder");

    }

    private String generateHash(String input)
    {

        MessageDigest digest=null;
        String hash="";
        try {
            digest = MessageDigest.getInstance("SHA-256");
            digest.update(input.getBytes());

            hash = bytesToHexString(digest.digest());


        } catch (NoSuchAlgorithmException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        return hash;
    }

    private static String bytesToHexString(byte[] bytes) {

        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < bytes.length; i++) {
            String hex = Integer.toHexString(0xFF & bytes[i]);
            if (hex.length() == 1) {
                sb.append('0');
            }
            sb.append(hex);
        }
        return sb.toString();
    }

    public void postAuthSiteminder(boolean esito, String errcode) {


        if(esito)
        {

            switch (errcode)
            {
                case "00":
                    doLogin();
                    break;

                case "01":
                    showDialog("La tua password è scaduta. Per cambiarla, vai a enigaseluce.com.");
                    break;

                case "06":
                    showDialog("L'indirizzo email o la password che hai inserito non sono validi. Ti consigliamo di utilizzare la funzionalità di recupero email e password.");
                    break;

                case "07":
                    showDialog("L'indirizzo email che hai inserito non è valido.");
                    break;

                case "18":
                    showDialogContinue("La password che hai inserito è in scadenza, ti consigliamo di provvedere quanto prima alla modifica.");
                    break;

                case "19":
                    showDialog("La tua password è scaduta. Per cambiarla, vai a enigaseluce.com.");
                    break;

                case "20":
                    showDialog("La tua password è scaduta. Per cambiarla, vai a enigaseluce.com.");
                    break;

                case "24":
                    showDialog("Hai effettuato troppi tentativi di accesso. Ti consigliamo di utilizzare la funzionalità di recupero email e password.");
                    break;

                case "25":
                    showDialog("La tua registrazione è stata disattivata per inattività. Procedi ad una nuova registrazione su enigaseluce.com.");
                    break;

                case "97":
                    showDialog("Siamo spiacenti, il servizio al momento non è disponibile. Ti chiediamo di riprovare più tardi.");
                    break;

                case "98":
                    showDialog("L'indirizzo email o la password che hai inserito non sono validi. Ti consigliamo di utilizzare la funzionalità di recupero email e password.");
                    break;

            }
//        ServiceCall dummyResourceServiceCall = ServiceCallFactory.createServiceCall(context);
//        try {
//            dummyResourceServiceCall.executeHttpsGetDummyRes(Constants.DUMMY_RESOURCE,Login.this,token);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }


        }
        else
        {
            showDialog("Errore nella login");
        }

    }

    private void showDialog(String text)
    {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Attenzione!");
        // alert.setMessage("Message");

        alert.setMessage(text);
        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //Your action here
            }
        });

//        alert.setNegativeButton("Cancel",
//                new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int whichButton) {
//                    }
//                });

        alert.show();
    }

    private void showDialogContinue(String text)
    {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Attenzione!");
        // alert.setMessage("Message");

        alert.setMessage(text);
        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                doLogin();
            }
        });

//        alert.setNegativeButton("Cancel",
//                new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int whichButton) {
//                    }
//                });

        alert.show();
    }

    public void postSendToken(String response)
    {

    }

    public void postAuth(String rispostaLogin, List<Cookie> lista) {
        if (animation != null) {

            animation.stop();
            imageAnim.setVisibility(View.INVISIBLE);
        }
        JSONArray rispostaJSON = new JSONArray();
        JSONObject oggettoRisposta = new JSONObject();
        String esito = "";
        String messaggio = "";
        // se si ha pagina di authentication failed
        if (rispostaLogin.contains("Authentication Failed")) {

            RelativeLayout login_help_btn1 = (RelativeLayout) findViewById(R.id.login_help_btn);
//            login_help_btn1.setVisibility(View.VISIBLE);
            accedi_help = true;
//            Utils.popup(Login.this, getResources().getString(R.string.errore_credenziali), "Riprova");
            Utils.popup(Login.this, getResources().getString(R.string.errore_credenziali), "Riprova");

            riabilitaCampi();

            return;
        }
        try {
            rispostaJSON = new JSONArray(rispostaLogin);
            oggettoRisposta = (JSONObject) rispostaJSON.get(0);
            esito = oggettoRisposta.getString("esito");
            messaggio = oggettoRisposta.getString("descEsito");

            Log.d("LOGIN", "Eni_debug: " + esito + " - " + messaggio);
        } catch (JSONException e) {
            // gestire le risposte non JSON (non andate a buon fine)
            if(rispostaLogin.contains("hold your data"))
            {
                ServiceCall loginServiceCall = ServiceCallFactory.createServiceCall(context);
                try {
                    if (Constants.TEST_ENV) {
                        rispostaLogin = loginServiceCall.executeHttpsPostAuth(Constants.URL_LOGIN_TEST, usernameInserted, passwordInserted, Login.this);
                    } else {
                        rispostaLogin = loginServiceCall.executeHttpsPostAuth(Constants.URL_LOGIN_PROD, usernameInserted, passwordInserted, Login.this);
                    }
                }
                catch (Exception ex)
                {
                    Log.d(TAG,ex.getMessage());
                }
                return;
            }

            Log.d("LOGIN", "LOGIN failed!");

            Intent intentOops = new Intent(getApplicationContext(), OopsPageLogin.class);
            intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
            intentOops.putExtra("USERNAME", usernameInserted);
            intentOops.putExtra("PASSWORD", passwordInserted);
            intentOops.putExtra("CHECKBOX", checkBox.isChecked());
            riabilitaCampi();
            Login.this.finish();
            startActivity(intentOops);
            overridePendingTransition(R.anim.fade, R.anim.hold);
            return;
        }
        if (!esito.equals("")) {

            if (!esito.equals("200")) {
                // checkbox
                checkBox = (CheckBox) findViewById(R.id.checkbox);
                if (!checkBox.isChecked() && (!user.equals("") || !password.equals(""))) {
                    editor.remove("User");
                    editor.remove("Password");
                    editor.commit();
                }
                // codici diversi da 200 devono far portare alla oops page
                if (!messaggio.equals("")) {
                    Intent intentOops = new Intent(getApplicationContext(), OopsPageLogin.class);
                    intentOops.putExtra("USERNAME", usernameInserted);
                    intentOops.putExtra("PASSWORD", passwordInserted);
                    intentOops.putExtra("CHECKBOX", checkBox.isChecked());
                    riabilitaCampi();
                    Login.this.finish();
                    // errori ops page
                    if (esito.equals("309")) {
                        Log.d("LOGIN", "Esito: 309");
                        intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
                        startActivity(intentOops);
                        overridePendingTransition(R.anim.fade, R.anim.hold);

                    } else if (esito.equals("400")) {
                        Log.d("LOGIN", "Esito: 400");
                        intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
                        startActivity(intentOops);
                        overridePendingTransition(R.anim.fade, R.anim.hold);

                    } else if (esito.equals("420")) {
                        intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
                        startActivity(intentOops);
                        overridePendingTransition(R.anim.fade, R.anim.hold);

                    } else if (esito.equals("449")) {
                        intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
                        startActivity(intentOops);
                        overridePendingTransition(R.anim.fade, R.anim.hold);

                    } else if (esito.equals("451")) {
                        intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_popup_451));
                        startActivity(intentOops);
                        overridePendingTransition(R.anim.fade, R.anim.hold);

                    }
                    // errori popup
                    else if (esito.equals("307")) {
                        Utils.popup(Login.this, getResources().getString(R.string.errore_popup_307), "Riprova");

                    } else if (esito.equals("435")) {
                        Utils.popup(Login.this, getResources().getString(R.string.errore_popup_435), "Riprova");

                    } else if (esito.equals("437")) {
                        Utils.popup(Login.this, getResources().getString(R.string.errore_popup_437), "Riprova");

                    } else if (esito.equals("438")) {
                        Utils.popup(Login.this, getResources().getString(R.string.errore_popup_438), "Riprova");

                    } else if (esito.equals("443")) {
                        Utils.popup(Login.this, getResources().getString(R.string.errore_popup_443), "Riprova");

                    } else if (esito.equals("444")) {
                        Utils.popup(Login.this, getResources().getString(R.string.errore_popup_444), "Riprova");

                    } else if (esito.equals("445")) {
                        Utils.popup(Login.this, getResources().getString(R.string.errore_popup_445), "Riprova");

                    } else if (esito.equals("446")) {
                        Utils.popup(Login.this, getResources().getString(R.string.errore_popup_446), "Riprova");

                    } else if (esito.equals("447")) {
                        Utils.popup(Login.this, getResources().getString(R.string.errore_popup_447), "Riprova");
                    } else {
                        Log.d("LOGIN", "OOPS GENERICO");
                        intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
                        startActivity(intentOops);
                        overridePendingTransition(R.anim.fade, R.anim.hold);
                    }
                }
            } else {

                // memorizzo la risposta in memoria
                Constants.SERVIZIO_LOGIN = rispostaJSON.toString();
                Constants.LISTA_COOKIE = lista;
                user = settings.getString("User", "");
                password = settings.getString("Password", "");
                // checkbox
                checkBox = (CheckBox) findViewById(R.id.checkbox);
                if (checkBox.isChecked() && (!usernameInserted.equals("") && !passwordInserted.equals(""))) {
                    editor.putString("User", usernameInserted);
                    editor.putString("Password", passwordInserted);
                    editor.commit();
                } else {
                    if (!user.equals("") || !password.equals("")) {
                        editor.remove("User");
                        editor.remove("Password");
                        editor.commit();
                    }
                }
                // si abilitano i campi
                riabilitaCampi();
                ((EniApplication)getApplication()).sendAnalyticsEvent("[Android] - servizi online");
                Intent intentHome = new Intent(getApplicationContext(), Home.class);
                Login.this.finish();
                startActivity(intentHome);
                overridePendingTransition(R.anim.fade, R.anim.hold);
            }
        }
    }

    private boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager == null) return false;
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }

    public void postLoginException(boolean exception) {
        if (exception) {
            if (animation != null) {
                animation.stop();
                imageAnim.setVisibility(View.INVISIBLE);
            }
            Intent intentOops = new Intent(getApplicationContext(), OopsPageLogin.class);
            intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
            intentOops.putExtra("USERNAME", usernameInserted);
            intentOops.putExtra("PASSWORD", passwordInserted);
            intentOops.putExtra("CHECKBOX", checkBox.isChecked());
            riabilitaCampi();
            Login.this.finish();
            startActivity(intentOops);
            overridePendingTransition(R.anim.fade, R.anim.hold);
        }
    }

    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(Login.this);
        builder.setMessage("Sei sicuro di voler uscire dall'applicazione?").setCancelable(false).setTitle("Uscita").setPositiveButton("Si", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Login.this.finish();
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void riabilitaCampi() {
        // si abilitano i campi
        final EditText usernameInput = (EditText) findViewById(R.id.username);
        final EditText passwordInput = (EditText) findViewById(R.id.password);

        usernameInput.setFocusableInTouchMode(true);
        usernameInput.setClickable(true);

        passwordInput.setFocusable(true);
        passwordInput.setFocusableInTouchMode(true);
        passwordInput.setClickable(true);

        final Button accedi = (Button) findViewById(R.id.accedi);
        accedi.setClickable(true);
        accedi.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
        accedi.getBackground().invalidateSelf();

        checkBox = (CheckBox) findViewById(R.id.checkbox);
        checkBox.setClickable(true);
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
            myShowcase.setSwitcherState(true);
        }

        RelativeLayout clienteEni = (RelativeLayout) findViewById(R.id.login_cliente_eni);
        clienteEni.setClickable(true);
        RelativeLayout help = (RelativeLayout) findViewById(R.id.login_help_btn);
        help.setClickable(true);
    }

    public void onResume() {
        super.onResume();
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
            myShowcase.onResume();
        }
    }
}