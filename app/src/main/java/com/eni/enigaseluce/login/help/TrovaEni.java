package com.eni.enigaseluce.login.help;

import java.util.Timer;
import java.util.TimerTask;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.AnimationDrawable;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import com.eni.enigaseluce.EniApplication;
import com.eni.enigaseluce.R;
import com.eni.enigaseluce.consumigas.ConsumiGas;
import com.eni.enigaseluce.consumiluce.ConsumiLuce;
import com.eni.enigaseluce.error.OopsPage;
import com.eni.enigaseluce.home.Home;
import com.eni.enigaseluce.httpcall.Constants;
import com.eni.enigaseluce.login.Login;
import com.eni.enigaseluce.utils.Utilities;

@SuppressLint("NewApi")
public class TrovaEni extends Activity {
    private WebView mWebView;
    AnimationDrawable animation;
    ImageView imageAnim;
    TimerTask stopAnimationTask;
    final Handler handler = new Handler();
    final Activity activity = this;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.trova_eni);

        mWebView = (WebView) findViewById(R.id.trova_eni_webview);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
//                animation = Utilities.animation(TrovaEni.this);
//                imageAnim = (ImageView) findViewById(R.id.waiting_anim);
//                imageAnim.setVisibility(View.VISIBLE);
//                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
//                    imageAnim.setBackgroundDrawable(animation);
//                } else {
//                    imageAnim.setBackground(animation);
//                }
//                animation.start();
            }
        });
        mWebView.setWebViewClient(new WebViewClient() {
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                if (animation != null) {
                    animation.stop();
                    imageAnim.setVisibility(View.INVISIBLE);
                }
                Intent intentOops = new Intent(getApplicationContext(), OopsPage.class);
                intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
                // TrovaEni.this.finish();
                startActivityForResult(intentOops, OopsPage.ESCI);
                overridePendingTransition(R.anim.fade, R.anim.hold);
            }

            public void onPageFinished(WebView view, String url) {
                stopAnimationTask = new TimerTask() {
                    public void run() {
                        handler.post(new Runnable() {
                            public void run() {
                                stopAnimation();
                            }
                        });
                    }
                };

                new Timer().schedule(stopAnimationTask, 100);
            }

            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                handler.proceed();
            }
        });
        final ImageView indietro = (ImageView) findViewById(R.id.trova_eni_indietro);
        indietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
        indietro.getBackground().invalidateSelf();
        indietro.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v1) {
                indietro.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
                indietro.getBackground().invalidateSelf();
                animation = Utilities.animation(TrovaEni.this);
                imageAnim = (ImageView) findViewById(R.id.waiting_anim);
                imageAnim.setVisibility(View.VISIBLE);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    imageAnim.setBackgroundDrawable(animation);
                } else {
                    imageAnim.setBackground(animation);
                }
                animation.start();
                String paginaDiProvenienza = getIntent().getStringExtra("PAGINA_DI_PROVENIENZA");
                Intent backPageIntent;
                if (paginaDiProvenienza != null && paginaDiProvenienza.equals(Login.class.getCanonicalName())) {
                    backPageIntent = new Intent(getApplicationContext(), Login.class);
                    backPageIntent.putExtra("Help_Accedi", getIntent().getBooleanExtra("Help_Accedi", false));
                    backPageIntent.putExtra("USERNAME", getIntent().getStringExtra("USERNAME"));
                    backPageIntent.putExtra("PASSWORD", getIntent().getStringExtra("PASSWORD"));
                } else if (paginaDiProvenienza != null && paginaDiProvenienza.equals(ConsumiLuce.class.getCanonicalName())) {
                    backPageIntent = new Intent(getApplicationContext(), ConsumiLuce.class);
                    backPageIntent.putExtra("Classificazione", getIntent().getStringExtra("Classificazione"));
                    backPageIntent.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
                    backPageIntent.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
                    backPageIntent.putExtra("PERIODO", getIntent().getIntExtra("PERIODO", Constants.RETAIL_PERIODO_MESI_6));
                    backPageIntent.putExtra("RIFERIMENTO", getIntent().getStringExtra("RIFERIMENTO"));
                } else if (paginaDiProvenienza != null && paginaDiProvenienza.equals(ConsumiGas.class.getCanonicalName())) {
                    backPageIntent = new Intent(getApplicationContext(), ConsumiGas.class);
                    backPageIntent.putExtra("Classificazione", getIntent().getStringExtra("Classificazione"));
                    backPageIntent.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
                    backPageIntent.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
                    backPageIntent.putExtra("PERIODO", getIntent().getIntExtra("PERIODO", Constants.RETAIL_PERIODO_MESI_6));
                    backPageIntent.putExtra("RIFERIMENTO", getIntent().getStringExtra("RIFERIMENTO"));
                } else {
                    backPageIntent = new Intent(getApplicationContext(), Home.class);
                }
                mWebView.stopLoading();
                backPageIntent.putExtra("PAGINA", 2);
                TrovaEni.this.finish();
                startActivity(backPageIntent);
                overridePendingTransition(R.anim.fade, R.anim.hold);
            }
        });


        animation = Utilities.animation(TrovaEni.this);
        imageAnim = (ImageView) findViewById(R.id.waiting_anim);
        imageAnim.setVisibility(View.VISIBLE);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
            imageAnim.setBackgroundDrawable(animation);
        } else {
            imageAnim.setBackground(animation);
        }
        animation.start();

//        mWebView.loadUrl("http://www.eni.mobi/m/eni_attorno/mappa/it_IT?project=#cont_map");
        mWebView.loadUrl("https://www.enigaseluce.com/it-IT/famiglia/energy-store/trova-energy-store");

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == OopsPage.ESCI) {
            Intent intentLoginHelp = new Intent(getApplicationContext(), LoginHelp.class);
            TrovaEni.this.finish();
            startActivity(intentLoginHelp);
            overridePendingTransition(R.anim.fade, R.anim.hold);
        }
    }

    private void stopAnimation() {
        if (animation != null) {
            animation.stop();
            imageAnim.setVisibility(View.INVISIBLE);
        }
    }

    public void onBackPressed() {
        animation = Utilities.animation(TrovaEni.this);
        imageAnim = (ImageView) findViewById(R.id.waiting_anim);
        imageAnim.setVisibility(View.VISIBLE);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
            imageAnim.setBackgroundDrawable(animation);
        } else {
            imageAnim.setBackground(animation);
        }
        animation.start();
        String paginaDiProvenienza = getIntent().getStringExtra("PAGINA_DI_PROVENIENZA");
        Intent backPageIntent;
        if (paginaDiProvenienza != null && paginaDiProvenienza.equals(Login.class.getCanonicalName())) {
            backPageIntent = new Intent(getApplicationContext(), Login.class);
            backPageIntent.putExtra("Help_Accedi", getIntent().getBooleanExtra("Help_Accedi", false));
            backPageIntent.putExtra("USERNAME", getIntent().getStringExtra("USERNAME"));
            backPageIntent.putExtra("PASSWORD", getIntent().getStringExtra("PASSWORD"));
        } else if (paginaDiProvenienza != null && paginaDiProvenienza.equals(ConsumiLuce.class.getCanonicalName())) {
            backPageIntent = new Intent(getApplicationContext(), ConsumiLuce.class);
            backPageIntent.putExtra("Classificazione", getIntent().getStringExtra("Classificazione"));
            backPageIntent.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
            backPageIntent.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
            backPageIntent.putExtra("PERIODO", getIntent().getIntExtra("PERIODO", Constants.RETAIL_PERIODO_MESI_6));
            backPageIntent.putExtra("RIFERIMENTO", getIntent().getStringExtra("RIFERIMENTO"));
        } else if (paginaDiProvenienza != null && paginaDiProvenienza.equals(ConsumiGas.class.getCanonicalName())) {
            backPageIntent = new Intent(getApplicationContext(), ConsumiGas.class);
            backPageIntent.putExtra("Classificazione", getIntent().getStringExtra("Classificazione"));
            backPageIntent.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
            backPageIntent.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
            backPageIntent.putExtra("PERIODO", getIntent().getIntExtra("PERIODO", Constants.RETAIL_PERIODO_MESI_6));
            backPageIntent.putExtra("RIFERIMENTO", getIntent().getStringExtra("RIFERIMENTO"));
        } else {
            backPageIntent = new Intent(getApplicationContext(), Home.class);
        }
        mWebView.stopLoading();
        backPageIntent.putExtra("PAGINA", 2);
        TrovaEni.this.finish();
        startActivity(backPageIntent);
        overridePendingTransition(R.anim.fade, R.anim.hold);
    }
}
