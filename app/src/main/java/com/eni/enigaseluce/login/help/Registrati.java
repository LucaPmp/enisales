package com.eni.enigaseluce.login.help;

import java.util.Timer;
import java.util.TimerTask;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.AnimationDrawable;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import com.eni.enigaseluce.EniApplication;
import com.eni.enigaseluce.R;
import com.eni.enigaseluce.error.OopsPage;
import com.eni.enigaseluce.login.Login;
import com.eni.enigaseluce.utils.Utilities;

@SuppressLint("NewApi")
public class Registrati extends Activity {
    private WebView mWebView;
    AnimationDrawable animation;
    ImageView imageAnim;
    TimerTask stopAnimationTask;
    final Handler handler = new Handler();
    final Activity activity = this;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.offerte);

        ((EniApplication) getApplication()).sendAnalyticsEvent("[Android] - non sei ancora registrato");

        mWebView = (WebView) findViewById(R.id.offerte_webview);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
        /*
		 * animation = new AnimationDrawable();
		 * animation.addFrame(getResources
		 * ().getDrawable(R.drawable.anim_load2),300);
		 * animation.addFrame
		 * (getResources().getDrawable(R.drawable.anim_load3), 300);
		 * animation
		 * .addFrame(getResources().getDrawable(R.drawable.anim_load4
		 * ),300); animation.setOneShot(false); imageAnim = (ImageView)
		 * findViewById(R.id.waiting_trovaeni);
		 * imageAnim.setVisibility(View.VISIBLE);
		 * if(Build.VERSION.SDK_INT < 16){
		 * imageAnim.setBackgroundDrawable(animation); }else{
		 * imageAnim.setBackground(animation); } // start the animation!
		 * animation.start();
		 */
            }
        });
        mWebView.setWebViewClient(new WebViewClient() {
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
		/*
		 * if (animation != null) { animation.stop();
		 * imageAnim.setVisibility(View.INVISIBLE); } Intent intentOops
		 * = new Intent(getApplicationContext(), OopsPage.class);
		 * intentOops.putExtra("MESSAGGIO",
		 * getResources().getString(R.string.servizio_non_disponibile));
		 * // TrovaEni.this.finish(); startActivityForResult(intentOops,
		 * OopsPage.ESCI); overridePendingTransition(R.anim.fade,
		 * R.anim.hold);
		 */
            }

            public void onPageFinished(WebView view, String url) {
                stopAnimationTask = new TimerTask() {
                    public void run() {
                        handler.post(new Runnable() {
                            public void run() {
                                stopAnimation();
                            }
                        });
                    }
                };

                new Timer().schedule(stopAnimationTask, 100);
            }

            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                handler.proceed();
            }
        });
        final ImageView indietro = (ImageView) findViewById(R.id.offerte_indietro);
        indietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
        indietro.getBackground().invalidateSelf();
        indietro.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v1) {
                indietro.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
                indietro.getBackground().invalidateSelf();
                animation = Utilities.animation(Registrati.this);
                imageAnim = (ImageView) findViewById(R.id.waiting_anim);
                imageAnim.setVisibility(View.VISIBLE);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    imageAnim.setBackgroundDrawable(animation);
                } else {
                    imageAnim.setBackground(animation);
                }
                animation.start();
                Intent backPageIntent = new Intent(getApplicationContext(), Login.class);
                mWebView.stopLoading();
                backPageIntent.putExtra("Help_Accedi", true);
                backPageIntent.putExtra("USERNAME", getIntent().getStringExtra("USERNAME"));
                backPageIntent.putExtra("PASSWORD", getIntent().getStringExtra("PASSWORD"));
                Registrati.this.finish();
                startActivity(backPageIntent);
                overridePendingTransition(R.anim.fade, R.anim.hold);
            }
        });

        // TEST
        // mWebView.loadUrl("http://st3-eni.eni.mobi/m/eni/registrazione/");
        // PROD
//        mWebView.loadUrl("http://www.eni.mobi/m/eni/registrazione/");
        mWebView.loadUrl("https://www.enigaseluce.com/it-IT/famiglia/home?modal=modal-myeni-login");

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == OopsPage.ESCI) {
            Intent intentLoginHelp = new Intent(getApplicationContext(), Login.class);
            Registrati.this.finish();
            startActivity(intentLoginHelp);
            overridePendingTransition(R.anim.fade, R.anim.hold);
        }
    }

    private void stopAnimation() {
        if (animation != null) {
            animation.stop();
            imageAnim.setVisibility(View.INVISIBLE);
        }
    }

    public void onBackPressed() {
        animation = Utilities.animation(Registrati.this);
        imageAnim = (ImageView) findViewById(R.id.waiting_anim);
        imageAnim.setVisibility(View.VISIBLE);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
            imageAnim.setBackgroundDrawable(animation);
        } else {
            imageAnim.setBackground(animation);
        }
        animation.start();
        Intent backPageIntent = new Intent(getApplicationContext(), Login.class);
        mWebView.stopLoading();
        backPageIntent.putExtra("Help_Accedi", true);
        backPageIntent.putExtra("USERNAME", getIntent().getStringExtra("USERNAME"));
        backPageIntent.putExtra("PASSWORD", getIntent().getStringExtra("PASSWORD"));
        Registrati.this.finish();
        startActivity(backPageIntent);
        overridePendingTransition(R.anim.fade, R.anim.hold);
    }
}
