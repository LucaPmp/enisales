package com.eni.enigaseluce.login.help;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.AnimationDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.eni.enigaseluce.R;
import com.eni.enigaseluce.error.OopsPage;
import com.eni.enigaseluce.login.Login;
import com.eni.enigaseluce.utils.Utilities;

@SuppressLint("NewApi")
public class ClienteEni extends Activity {
    AnimationDrawable animation;
    ImageView imageAnim;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cliente_eni);

        final ImageView help = (ImageView) findViewById(R.id.cliente_eni_indietro);
        help.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
        help.getBackground().invalidateSelf();
        help.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v1) {
                help.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
                help.getBackground().invalidateSelf();

                animation = Utilities.animation(ClienteEni.this);
                imageAnim = (ImageView) findViewById(R.id.waiting_anim);
                imageAnim.setVisibility(View.VISIBLE);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    imageAnim.setBackgroundDrawable(animation);
                } else {
                    imageAnim.setBackground(animation);
                }
                animation.start();

                Intent intentHelp = new Intent(getApplicationContext(), Login.class);
                intentHelp.putExtra("Help_Accedi", getIntent().getBooleanExtra("Help_Accedi", false));
                intentHelp.putExtra("USERNAME", getIntent().getStringExtra("USERNAME"));
                intentHelp.putExtra("PASSWORD", getIntent().getStringExtra("PASSWORD"));
                ClienteEni.this.finish();
                startActivity(intentHelp);
                overridePendingTransition(R.anim.fade, R.anim.hold);
            }
        });

        final RelativeLayout offerte = (RelativeLayout) findViewById(R.id.cliente_eni_aderisci);
        offerte.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
        offerte.getBackground().invalidateSelf();
        offerte.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v1) {
                offerte.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
                offerte.getBackground().invalidateSelf();

                animation = Utilities.animation(ClienteEni.this);
                imageAnim = (ImageView) findViewById(R.id.waiting_anim);
                imageAnim.setVisibility(View.VISIBLE);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    imageAnim.setBackgroundDrawable(animation);
                } else {
                    imageAnim.setBackground(animation);
                }
                animation.start();

                if (!isNetworkAvailable(ClienteEni.this)) {
                    Intent intentOops = new Intent(getApplicationContext(), OopsPage.class);
                    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.connessione_assente));
                    // LoginHelp.this.finish();
                    if (animation != null) {
                        animation.stop();
                        imageAnim.setVisibility(View.INVISIBLE);
                    }
                    startActivityForResult(intentOops, OopsPage.ESCI);
                    overridePendingTransition(R.anim.fade, R.anim.hold);
                    if (animation != null) {
                        animation.stop();
                        imageAnim.setVisibility(View.INVISIBLE);
                    }

                } else {
                    Intent intentOfferte = new Intent(getApplicationContext(), Offerte.class);
                    intentOfferte.putExtra("Help_Accedi", getIntent().getBooleanExtra("Help_Accedi", false));
                    intentOfferte.putExtra("USERNAME", getIntent().getStringExtra("USERNAME"));
                    intentOfferte.putExtra("PASSWORD", getIntent().getStringExtra("PASSWORD"));
                    ClienteEni.this.finish();
                    startActivity(intentOfferte);
                    overridePendingTransition(R.anim.fade, R.anim.hold);
                }
            }
        });

        final RelativeLayout trovaEni = (RelativeLayout) findViewById(R.id.cliente_eni_trova_eni);
        trovaEni.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
        trovaEni.getBackground().invalidateSelf();
        trovaEni.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v1) {
                trovaEni.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
                trovaEni.getBackground().invalidateSelf();

                animation = Utilities.animation(ClienteEni.this);
                imageAnim = (ImageView) findViewById(R.id.waiting_anim);
                imageAnim.setVisibility(View.VISIBLE);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    imageAnim.setBackgroundDrawable(animation);
                } else {
                    imageAnim.setBackground(animation);
                }
                animation.start();

                if (!isNetworkAvailable(ClienteEni.this)) {
                    Intent intentOops = new Intent(getApplicationContext(), OopsPage.class);
                    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.connessione_assente));
                    // LoginHelp.this.finish();
                    startActivityForResult(intentOops, OopsPage.ESCI);
                    overridePendingTransition(R.anim.fade, R.anim.hold);
                    if (animation != null) {
                        animation.stop();
                        imageAnim.setVisibility(View.INVISIBLE);
                    }
                } else {
                    Intent intentTrovaEni = new Intent(getApplicationContext(), TrovaEni.class);
                    intentTrovaEni.putExtra("Help_Accedi", getIntent().getBooleanExtra("Help_Accedi", false));
                    intentTrovaEni.putExtra("USERNAME", getIntent().getStringExtra("USERNAME"));
                    intentTrovaEni.putExtra("PASSWORD", getIntent().getStringExtra("PASSWORD"));
                    ClienteEni.this.finish();
                    intentTrovaEni.putExtra("PAGINA_DI_PROVENIENZA", ClienteEni.class.getCanonicalName());
                    startActivity(intentTrovaEni);
                    overridePendingTransition(R.anim.fade, R.anim.hold);
                }

            }
        });
    }

    public void recuperoCredenziali(View v) {

        animation = Utilities.animation(ClienteEni.this);
        imageAnim = (ImageView) findViewById(R.id.waiting_anim);
        imageAnim.setVisibility(View.VISIBLE);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
            imageAnim.setBackgroundDrawable(animation);
        } else {
            imageAnim.setBackground(animation);
        }
        animation.start();

        if (!isNetworkAvailable(ClienteEni.this)) {
            Intent intentOops = new Intent(getApplicationContext(), OopsPage.class);
            intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.connessione_assente));
            // LoginHelp.this.finish();
            if (animation != null) {
                animation.stop();
                imageAnim.setVisibility(View.INVISIBLE);
            }
            startActivityForResult(intentOops, OopsPage.ESCI);
            overridePendingTransition(R.anim.fade, R.anim.hold);
        } else {
            Intent intentEniFamiglia = new Intent(getApplicationContext(), EniFamiglia.class);
            ClienteEni.this.finish();
            startActivity(intentEniFamiglia);
            overridePendingTransition(R.anim.fade, R.anim.hold);
        }
    }

    private boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager == null) return false;
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == OopsPage.ESCI) {
            ClienteEni.this.finish();
        }
    }

    public void onBackPressed() {

        animation = Utilities.animation(ClienteEni.this);
        imageAnim = (ImageView) findViewById(R.id.waiting_anim);
        imageAnim.setVisibility(View.VISIBLE);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
            imageAnim.setBackgroundDrawable(animation);
        } else {
            imageAnim.setBackground(animation);
        }
        animation.start();

        Intent intentHelp = new Intent(getApplicationContext(), Login.class);
        intentHelp.putExtra("Help_Accedi", getIntent().getBooleanExtra("Help_Accedi", false));
        intentHelp.putExtra("USERNAME", getIntent().getStringExtra("USERNAME"));
        intentHelp.putExtra("PASSWORD", getIntent().getStringExtra("PASSWORD"));
        ClienteEni.this.finish();
        startActivity(intentHelp);
        overridePendingTransition(R.anim.fade, R.anim.hold);
    }
}
