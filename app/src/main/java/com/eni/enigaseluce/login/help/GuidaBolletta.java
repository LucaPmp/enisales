package com.eni.enigaseluce.login.help;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Timer;
import java.util.TimerTask;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.AnimationDrawable;
import android.net.Uri;
import android.net.http.SslError;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.Toast;

import com.eni.enigaseluce.EniApplication;
import com.eni.enigaseluce.R;
import com.eni.enigaseluce.bollettePagamenti.BollettePagamentiRicerca;
import com.eni.enigaseluce.bollettePagamenti.BollettePagamentiRicercaPostIntervallo;
import com.eni.enigaseluce.error.OopsPage;
import com.eni.enigaseluce.font.EniFont;
import com.eni.enigaseluce.home.Home;
import com.eni.enigaseluce.httpcall.Constants;
import com.eni.enigaseluce.utils.Utilities;

@SuppressLint("NewApi")
public class GuidaBolletta extends Activity {
    private WebView mWebView;
    AnimationDrawable animation;
    ImageView imageAnim;
    TimerTask stopAnimationTask;
    final Handler handler = new Handler();
    final Activity activity = this;
    String paginaDiProvenienza;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.trova_eni);
        paginaDiProvenienza = getIntent().getStringExtra("ORIGINE");

        ((EniApplication) getApplication()).sendAnalyticsEvent("[Android] - guida alla lettura della bolletta");

        EniFont titolo_testo = (EniFont) findViewById(R.id.titolo_testo);
        titolo_testo.setText(getResources().getString(R.string.guida_bolletta_titolo_testo));

        mWebView = (WebView) findViewById(R.id.trova_eni_webview);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                animation = Utilities.animation(GuidaBolletta.this);
                imageAnim = (ImageView) findViewById(R.id.waiting_anim);
                imageAnim.setVisibility(View.VISIBLE);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    imageAnim.setBackgroundDrawable(animation);
                } else {
                    imageAnim.setBackground(animation);
                }
                animation.start();
            }
        });


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            WebView.setWebContentsDebuggingEnabled(true);
        }


        mWebView.setWebViewClient(new WebViewClient() {
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                if (animation != null) {
                    animation.stop();
                    imageAnim.setVisibility(View.INVISIBLE);
                }
                Intent intentOops = new Intent(getApplicationContext(), OopsPage.class);
                intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
                // TrovaEni.this.finish();
                startActivityForResult(intentOops, OopsPage.ESCI);
                overridePendingTransition(R.anim.fade, R.anim.hold);


            }


            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url.endsWith(".pdf")) {
                    new DownloadFile().execute(url, url.substring(url.lastIndexOf("/")+1));
                } else {
                    mWebView.loadUrl(url);
                }
                return true;

            }

            public void onPageFinished(WebView view, String url) {
                stopAnimationTask = new TimerTask() {
                    public void run() {
                        handler.post(new Runnable() {
                            public void run() {
                                stopAnimation();

                                //TODO: eliminare la seguente riga
                                // onReceivedError(mWebView, 0, null, null);
                            }
                        });
                    }
                };

                new Timer().schedule(stopAnimationTask, 100);
            }

            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                handler.proceed();
            }
        });
        final ImageView indietro = (ImageView) findViewById(R.id.trova_eni_indietro);
        indietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
        indietro.getBackground().invalidateSelf();
        indietro.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v1) {
                indietro.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
                indietro.getBackground().invalidateSelf();
                animation = Utilities.animation(GuidaBolletta.this);
                imageAnim = (ImageView) findViewById(R.id.waiting_anim);
                imageAnim.setVisibility(View.VISIBLE);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    imageAnim.setBackgroundDrawable(animation);
                } else {
                    imageAnim.setBackground(animation);
                }
                animation.start();
                Intent backPageIntent;
                if (paginaDiProvenienza != null && paginaDiProvenienza.equals(Constants.ORIGINE_BOLLETTE_PAGAMENTI_RICERCA)) {
                    backPageIntent = new Intent(getApplicationContext(), BollettePagamentiRicerca.class);
                    backPageIntent.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
                    backPageIntent.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
                } else if (paginaDiProvenienza != null && paginaDiProvenienza.equals(Constants.ORIGINE_BOLLETTE_PAGAMENTI_RICERCA_POST_INTERVALLO)) {
                    backPageIntent = new Intent(getApplicationContext(), BollettePagamentiRicercaPostIntervallo.class);
                    backPageIntent.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
                    backPageIntent.putExtra("DATA_INIZIO", getIntent().getStringExtra("DATA_INIZIO"));
                    backPageIntent.putExtra("DATA_FINE", getIntent().getStringExtra("DATA_FINE"));
                    backPageIntent.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
                } else {
                    backPageIntent = new Intent(getApplicationContext(), Home.class);
                }
                mWebView.stopLoading();
                backPageIntent.putExtra("PAGINA", 1);
                GuidaBolletta.this.finish();
                startActivity(backPageIntent);
                overridePendingTransition(R.anim.fade, R.anim.hold);
            }
        });
        // TEST
        // mWebView.loadUrl("http://st3-eni.eni.mobi/m/eni/guidabolletta/");
        // PROD
        //mWebView.loadUrl("http://www.eni.mobi/m/eni/guidabolletta");

        if (!Constants.TEST_ENV) {
            mWebView.loadUrl("https://enigaseluce.com/it-IT/famiglia/guida-alla-lettura");
        } else {
            mWebView.loadUrl("https://pp.enigaseluce.com/it-IT/famiglia/guida-alla-lettura");
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == OopsPage.ESCI) {
            onBackPressed();
        }
    }

    private void stopAnimation() {
        if (animation != null) {
            animation.stop();
            imageAnim.setVisibility(View.INVISIBLE);
        }
    }

    public void onBackPressed() {
        animation = Utilities.animation(GuidaBolletta.this);
        imageAnim = (ImageView) findViewById(R.id.waiting_anim);
        imageAnim.setVisibility(View.VISIBLE);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
            imageAnim.setBackgroundDrawable(animation);
        } else {
            imageAnim.setBackground(animation);
        }
        animation.start();
        Intent backPageIntent;
        if (paginaDiProvenienza != null && paginaDiProvenienza.equals(Constants.ORIGINE_BOLLETTE_PAGAMENTI_RICERCA)) {
            backPageIntent = new Intent(getApplicationContext(), BollettePagamentiRicerca.class);
            backPageIntent.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
            backPageIntent.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
        } else if (paginaDiProvenienza != null && paginaDiProvenienza.equals(Constants.ORIGINE_BOLLETTE_PAGAMENTI_RICERCA_POST_INTERVALLO)) {
            backPageIntent = new Intent(getApplicationContext(), BollettePagamentiRicercaPostIntervallo.class);
            backPageIntent.putExtra("PRIMOCONTO", getIntent().getStringExtra("PRIMOCONTO"));
            backPageIntent.putExtra("DATA_INIZIO", getIntent().getStringExtra("DATA_INIZIO"));
            backPageIntent.putExtra("DATA_FINE", getIntent().getStringExtra("DATA_FINE"));
            backPageIntent.putExtra("INDICE_SPINNER", getIntent().getIntExtra("INDICE_SPINNER", 0));
        } else {
            backPageIntent = new Intent(getApplicationContext(), Home.class);
        }
        mWebView.stopLoading();
        backPageIntent.putExtra("PAGINA", 2);
        GuidaBolletta.this.finish();
        startActivity(backPageIntent);
        overridePendingTransition(R.anim.fade, R.anim.hold);
    }

    private class DownloadFile extends AsyncTask<String, Void, Void> {

        String fileName;

        @Override
        protected Void doInBackground(String... strings) {
            String fileUrl = strings[0];   // -> http://maven.apache.org/maven-1.x/maven.pdf
            fileName = strings[1];  // -> maven.pdf
            File folder = new File(Environment.getExternalStorageDirectory() + File.separator + "ENI");
            if (!folder.exists()) {
                folder.mkdir();
            }
            String extStorageDirectory = Environment.getExternalStorageDirectory().toString() + "/ENI";
//            File folder = new File(extStorageDirectory, "testthreepdf");
//            folder.mkdir();

            File pdfFile = new File(extStorageDirectory, fileName);

            try{
                pdfFile.createNewFile();
            }catch (IOException e){
                e.printStackTrace();
            }
            FileDownloader.downloadFile(fileUrl, pdfFile);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            File pdfFile = new File(Environment.getExternalStorageDirectory() + "/ENI/" + fileName);  // -> filename = maven.pdf
            Uri path = Uri.fromFile(pdfFile);
            Intent pdfIntent = new Intent(Intent.ACTION_VIEW);
            pdfIntent.setDataAndType(path, "application/pdf");
            pdfIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            try{
                startActivity(pdfIntent);
            }catch(ActivityNotFoundException e){
                Toast.makeText(GuidaBolletta.this, "No Application available to view PDF", Toast.LENGTH_SHORT).show();
            }


//            File pdfFile = new File(Environment.getExternalStorageDirectory() + "/ENI/" + fileName);  // -> filename = maven.pdf
//            Uri path = Uri.fromFile(pdfFile);
//            Intent pdfIntent = new Intent(Intent.ACTION_VIEW);
//            pdfIntent.setDataAndType(path, "application/pdf");
//            pdfIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//
//            try{
//                startActivity(pdfIntent);
//            }catch(ActivityNotFoundException e){
//                Toast.makeText(GuidaBolletta.this, "No Application available to view PDF", Toast.LENGTH_SHORT).show();
//            }
        }
    }
}

class FileDownloader {
    private static final int  MEGABYTE = 1024 * 1024;

    public static void downloadFile(String fileUrl, File directory){
        try {

            URL url = new URL(fileUrl);
            HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();
            //urlConnection.setRequestMethod("GET");
            //urlConnection.setDoOutput(true);
            urlConnection.connect();

            InputStream inputStream = urlConnection.getInputStream();
            FileOutputStream fileOutputStream = new FileOutputStream(directory);
            int totalSize = urlConnection.getContentLength();

            byte[] buffer = new byte[MEGABYTE];
            int bufferLength = 0;
            while((bufferLength = inputStream.read(buffer))>0 ){
                fileOutputStream.write(buffer, 0, bufferLength);
            }
            fileOutputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
