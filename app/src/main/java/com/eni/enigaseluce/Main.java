package com.eni.enigaseluce;

import java.util.Timer;
import java.util.TimerTask;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;

import com.crittercism.app.Crittercism;
import com.eni.enigaseluce.error.OopsPageMaintenance;
import com.eni.enigaseluce.httpcall.Constants;
import com.eni.enigaseluce.login.Login;
import com.eni.enigaseluce.utils.Log;
import com.eni.enigaseluce.utils.Utilities;

@SuppressLint("NewApi")
public class Main extends Activity {
    AnimationDrawable animation;
    ImageView imageAnim;
    TimerTask maintanenceTask;
    TimerTask animationTask;
    final Handler handler = new Handler();
    private Class<?> toGo = Login.class;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
    }

    @Override
    protected void onStart() {
        super.onStart();
        // setContentView(R.layout.main);
        animationTask = new TimerTask() {
            public void run() {
                handler.post(new Runnable() {
                    public void run() {
                        animation();
                    }
                });
            }
        };

        new Timer().schedule(animationTask, 100);

        maintanenceTask = new TimerTask() {
            public void run() {
                handler.post(new Runnable() {
                    public void run() {

                        // chiamata maintenance if stub disabled
                        if (!Constants.STUB_ENABLED) {
                            // maintenance();

                            Utilities.maintenance(Main.this, Login.class, null);
                        } else {
                            // bypassando la maintenance
                            Intent intentLogin = new Intent(getApplicationContext(), Login.class);
                            finish();
                            startActivity(intentLogin);
                            overridePendingTransition(R.anim.fade, R.anim.hold);
                        }
                    }
                });
            }
        };
        new Timer().schedule(maintanenceTask, 2000);
    }

    // private boolean isNetworkAvailable(Context context) {
    // ConnectivityManager connectivityManager = (ConnectivityManager)
    // context.getSystemService(Context.CONNECTIVITY_SERVICE);
    // if (connectivityManager == null) return false;
    // NetworkInfo activeNetworkInfo =
    // connectivityManager.getActiveNetworkInfo();
    // return (activeNetworkInfo != null &&
    // activeNetworkInfo.isConnectedOrConnecting());
    // }

    // public void maintenance() {
    // if (!this.isFinishing()) {
    // if (!isNetworkAvailable(Main.this)) {
    // Intent intentOops = new Intent(getApplicationContext(),
    // OopsPageMain.class);
    // intentOops.putExtra("MESSAGGIO",
    // getResources().getString(R.string.connessione_assente));
    // Main.this.finish();
    // startActivity(intentOops);
    // overridePendingTransition(R.anim.fade, R.anim.hold);
    // }
    // else {
    // // ******maintainance mode****//
    // try {
    // new RequestTask().execute(Constants.URL_MAINTAINANCE_MODE);
    // }
    // catch (Exception e) {
    // e.printStackTrace();
    // Intent intentOops = new Intent(getApplicationContext(),
    // OopsPageMain.class);
    // intentOops.putExtra("MESSAGGIO",
    // getResources().getString(R.string.maintenance_message));
    // Main.this.finish();
    // startActivity(intentOops);
    // overridePendingTransition(R.anim.fade, R.anim.hold);
    // return;
    // }
    // }
    // }
    // }

    public void onResponse(String result)
    {
        Log.d("REQUESTTASK", "Response Maintencance:\n" + result);

        // Do anything with response..
        if (!result.equals("")) {
            if (result.indexOf("<flag>") != -1) {
                int indexStart = result.indexOf("<flag>");
                int indexEnd = result.indexOf("</flag>");
                String flag = result.substring(indexStart + 6, indexEnd);
                if (flag.equals("false")) {

                    if (this.toGo != null && handler == null) {

                        // il portale non è in maintenace, quindi deve essere
                        // avviato l'activity corrispondente a "toGo"

                        Log.d("MAINTENCANCE_MODE", "Il portale non è in maintenance");

                        Intent intent = new Intent(this.getApplicationContext(), this.toGo);
                        this.finish();
                        this.startActivity(intent);
                        this.overridePendingTransition(R.anim.fade, R.anim.hold);
                    }
                    else {

                        // il portale non è in maintenace, quindi deve essere
                        // avviato il servizio dell'activity corrispondente

                        Log.d("MAINTENCANCE_MODE", "Il portale non è in maintenance");

                        handler.sendEmptyMessage(Constants.MAINTENANCE_DISATTIVA);

                        return;
                    }
                }
                else {
                    int indexMsgStart = result.indexOf("<text>");
                    int indexMsgEnd = result.indexOf("</text>");
                    String msgCortesia = result.substring(indexMsgStart + 6, indexMsgEnd);

                    Log.d("MAINTENCANCE_MODE", "Il portale è in maintenance");

                    Intent intentOops = new Intent(this.getApplicationContext(), OopsPageMaintenance.class);
                    intentOops.putExtra("MESSAGGIO", msgCortesia);
                    this.finish();
                    this.startActivity(intentOops);
                    this.overridePendingTransition(R.anim.fade, R.anim.hold);
                }
            }
            else {

                Log.d("MAINTENCANCE_MODE", "Il portale è in maintenance");

                Intent intentOops = new Intent(this.getApplicationContext(), OopsPageMaintenance.class);
                intentOops.putExtra("MESSAGGIO", this.getResources().getString(R.string.maintenance_message));
                this.finish();
                this.startActivity(intentOops);
                this.overridePendingTransition(R.anim.fade, R.anim.hold);
            }
        }
        else {

            Log.d("MAINTENCANCE_MODE", "Il portale è in maintenance");

            Intent intentOops = new Intent(this.getApplicationContext(), OopsPageMaintenance.class);
            intentOops.putExtra("MESSAGGIO", this.getResources().getString(R.string.errore_generico_opspage));
            this.finish();
            this.startActivity(intentOops);
            this.overridePendingTransition(R.anim.fade, R.anim.hold);
        }
    }


    public void animation() {
        animation = Utilities.animation(Main.this);
        // animation.addFrame(getResources().getDrawable(R.drawable.anim_load2),
        // 300);
        // animation.addFrame(getResources().getDrawable(R.drawable.anim_load3),
        // 300);
        // animation.addFrame(getResources().getDrawable(R.drawable.anim_load4),
        // 300);
        // animation.setOneShot(false);
        imageAnim = (ImageView) findViewById(R.id.waiting_anim);
        imageAnim.setVisibility(View.VISIBLE);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
            imageAnim.setBackgroundDrawable(animation);
        } else {
            imageAnim.setBackground(animation);
        }

        // start the animation!
        animation.start();
    }

    // class RequestTask extends AsyncTask<String, String, String> {
    //
    // @Override
    // protected String doInBackground(String... uri) {
    // HttpClient httpclient = new DefaultHttpClient();
    // HttpResponse response;
    // String responseString = "";
    // try {
    // response = httpclient.execute(new HttpGet(uri[0]));
    // StatusLine statusLine = response.getStatusLine();
    // if (statusLine.getStatusCode() == HttpStatus.SC_OK) {
    // ByteArrayOutputStream out = new ByteArrayOutputStream();
    // response.getEntity().writeTo(out);
    // out.close();
    // responseString = out.toString();
    // }
    // else {
    // // Closes the connection.
    // response.getEntity().getContent().close();
    // throw new IOException(statusLine.getReasonPhrase());
    // }
    // }
    // catch (ClientProtocolException e) {
    // e.printStackTrace();
    // }
    // catch (IOException e) {
    // e.printStackTrace();
    // }
    // return responseString;
    // }
    //
    // @Override
    // protected void onPostExecute(String result) {
    // super.onPostExecute(result);
    // // Do anything with response..
    // if (!result.equals("")) {
    // if (result.indexOf("<flag>") != -1) {
    // int indexStart = result.indexOf("<flag>");
    // int indexEnd = result.indexOf("</flag>");
    // String flag = result.substring(indexStart + 6, indexEnd);
    // if (flag.equals("false")) {
    // // Passaggio alla Login
    // Intent intentLogin = new Intent(getApplicationContext(), Login.class);
    // Main.this.finish();
    // startActivity(intentLogin);
    // overridePendingTransition(R.anim.fade, R.anim.hold);
    // }
    // else {
    // int indexMsgStart = result.indexOf("<text>");
    // int indexMsgEnd = result.indexOf("</text>");
    // String msgCortesia = result.substring(indexMsgStart + 6, indexMsgEnd);
    //
    // Intent intentOops = new Intent(getApplicationContext(),
    // OopsPageMain.class);
    // intentOops.putExtra("MESSAGGIO", msgCortesia);
    // Main.this.finish();
    // startActivity(intentOops);
    // overridePendingTransition(R.anim.fade, R.anim.hold);
    // }
    // }
    // else {
    // Intent intentOops = new Intent(getApplicationContext(),
    // OopsPageMain.class);
    // intentOops.putExtra("MESSAGGIO",
    // getResources().getString(R.string.maintenance_message));
    // Main.this.finish();
    // startActivity(intentOops);
    // overridePendingTransition(R.anim.fade, R.anim.hold);
    // }
    // }
    // else {
    // Intent intentOops = new Intent(getApplicationContext(),
    // OopsPageMain.class);
    // intentOops.putExtra("MESSAGGIO",
    // getResources().getString(R.string.errore_generico_opspage));
    // Main.this.finish();
    // startActivity(intentOops);
    // overridePendingTransition(R.anim.fade, R.anim.hold);
    // }
    // }
    // }
}
