package com.eni.enigaseluce.loyalty_data_contract;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class LoyaltyDBHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "Eni_Loyalty";

    private static String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " + LoyaltyDBContract.Data.DB_TABLE + "(" + LoyaltyDBContract.Data.Fields._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + LoyaltyDBContract.Data.Fields.TYPE + " INTEGER NOT NULL, ";

    static {
	for (String data : LoyaltyDBContract.Data.Fields.DATA) {
	    CREATE_TABLE += data + " TEXT, ";
	}

	for (String blob : LoyaltyDBContract.Data.Fields.BLOB) {
	    CREATE_TABLE += blob + " BLOB, ";
	}

	CREATE_TABLE += LoyaltyDBContract.Data.Fields.INDEX + " TEXT)";
    }

    private final static String[] CREATE_INDEXES = { "CREATE INDEX " + LoyaltyDBContract.Data.Fields.INDEX + " ON " + LoyaltyDBContract.Data.DB_TABLE + "(" + LoyaltyDBContract.Data.Fields.INDEX + ")", "CREATE INDEX " + LoyaltyDBContract.Data.Fields.TYPE + " ON " + LoyaltyDBContract.Data.DB_TABLE + "(" + LoyaltyDBContract.Data.Fields.TYPE + ")" };

    private final static String DROP_TABLE = "DROP TABLE IF EXISTS " + LoyaltyDBContract.Data.DB_TABLE;

    private final static String[] DROP_INDEXES = { "DROP INDEX " + LoyaltyDBContract.Data.Fields.INDEX, "DROP INDEX " + LoyaltyDBContract.Data.Fields.TYPE };

    public LoyaltyDBHelper(Context context) {
	super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

	sqLiteDatabase.execSQL(CREATE_TABLE);

	for (String sql : CREATE_INDEXES) {
	    sqLiteDatabase.execSQL(sql);
	}
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {

	if (newVersion > oldVersion) {
	    sqLiteDatabase.execSQL(DROP_TABLE);

	    for (String sql : DROP_INDEXES) {
		sqLiteDatabase.execSQL(sql);
	    }
	}

	onCreate(sqLiteDatabase);
    }
}
