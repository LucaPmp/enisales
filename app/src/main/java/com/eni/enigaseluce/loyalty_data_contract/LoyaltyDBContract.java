package com.eni.enigaseluce.loyalty_data_contract;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;

public class LoyaltyDBContract {

    public final static String AUTHORITY = "com.eni.enigaseluce";
    final static Uri BASE_URI = Uri.parse("content://" + AUTHORITY);

    private LoyaltyDBContract() {

    }

    public static class Data {

	public static final String PATH = "loyalty";
	public static final String DB_TABLE = PATH;
	public final static Uri CONTENT_URI = BASE_URI.buildUpon().appendPath(PATH).build();
	final static String MIME_TYPE = "vnd.com.eni.enigaseluce." + PATH;
	public final static String COLLECTION_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "\\" + MIME_TYPE;
	public final static String SINGLE_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "\\" + MIME_TYPE;

	public static class Fields {

	    public final static String _ID = BaseColumns._ID;
	    public final static String TYPE = "type";
	    public final static String INDEX = "indexed";
	    public final static String[] DATA = { "data0", "data1", "data2", "data3", "data4", "data5", "data6", "data7", "data8", "data9" };
	    public final static String[] BLOB = { "blob0", "blob1", "blob2" };
	}
    }

    public static class CartaLoyalty extends Data {

	public static final String CARTA_ITEM_TYPE = Data.SINGLE_ITEM_TYPE + ".carta";

	public static class Fields extends Data.Fields {

	    public static final String NUMERO_CARTA = DATA[0];
	    public static final String NOME_CARTA = DATA[1];
	    public static final String SALDO_PUNTI = DATA[2];
	    public static final String SALDO_AGGIORNATO = DATA[3];
	    public static final String TIPO_CARTA = DATA[4];
	}
    }
}
