package com.eni.enigaseluce.webolletta;

import java.util.HashMap;
import java.util.List;

import org.apache.http.cookie.Cookie;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.AnimationDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.eni.enigaseluce.R;
import com.eni.enigaseluce.domiciliazione.Domiciliazione;
import com.eni.enigaseluce.error.OopsPageSessioneScaduta;
import com.eni.enigaseluce.error.OopsPageWebollettaActivatePreconsenso;
import com.eni.enigaseluce.error.OopsPageWebollettaDomiciliazione;
import com.eni.enigaseluce.error.Utils;
import com.eni.enigaseluce.httpcall.Constants;
import com.eni.enigaseluce.httpcall.ServiceCall;
import com.eni.enigaseluce.httpcall.ServiceCallFactory;
import com.eni.enigaseluce.utils.Log;
import com.eni.enigaseluce.utils.MyHandler;
import com.eni.enigaseluce.utils.Utilities;

@SuppressLint("NewApi")
public class WebollettaActivate extends Activity {
    private Context context;
    private static Context staticContext;
    AnimationDrawable animation;
    ImageView imageAnim;
    String codiceConto;
    String tipologia;
    String operazioneWeb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
	final String loginService = Constants.SERVIZIO_LOGIN;
	context = this;
	WebollettaActivate.staticContext = this;

	super.onCreate(savedInstanceState);
	setContentView(R.layout.attiva_webolletta);

	Intent intent = getIntent();
	codiceConto = intent.getStringExtra("CodiceConto");
	tipologia = intent.getStringExtra("TIPOLOGIA");
	operazioneWeb = intent.getStringExtra("OperazioneWeb");

	TextView text_activation_condition = (TextView) findViewById(R.id.text_activation_condiction_2);
	text_activation_condition.setTextColor(Color.BLACK);

	String oldText = getString(R.string.activation_condiction_webolletta);

	String newText = oldText + " <font color='#e60000'>" + getString(R.string.activation_condiction_webolletta2) + "</font>";

	text_activation_condition.setText(Html.fromHtml(newText));

	final ImageView webollettaIndietro = (ImageView) findViewById(R.id.webolletta_attiva_indietro);
	webollettaIndietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
	webollettaIndietro.getBackground().invalidateSelf();
	webollettaIndietro.setOnClickListener(new View.OnClickListener() {
	    public void onClick(View v1) {
		webollettaIndietro.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
		webollettaIndietro.getBackground().invalidateSelf();
		animation = Utilities.animation(WebollettaActivate.this);
		imageAnim = (ImageView) findViewById(R.id.waiting_anim);
		imageAnim.setVisibility(View.VISIBLE);
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
		    imageAnim.setBackgroundDrawable(animation);
		}
		else {
		    imageAnim.setBackground(animation);
		}
		animation.start();
		Intent intentIndietro = new Intent(getApplicationContext(), Webolletta.class);
		WebollettaActivate.this.finish();
		startActivity(intentIndietro);
		overridePendingTransition(R.anim.fade, R.anim.hold);
	    }
	});

	/*
	 * final ImageView help = (ImageView)
	 * findViewById(R.id.webolletta_attiva_help);
	 * help.getBackground().setColorFilter(0xFFFFFFFF,
	 * PorterDuff.Mode.MULTIPLY); help.getBackground().invalidateSelf();
	 * help.setOnClickListener(new View.OnClickListener() { public void
	 * onClick(View v1) { help.getBackground().setColorFilter(0xFF999999,
	 * PorterDuff.Mode.MULTIPLY); help.getBackground().invalidateSelf();
	 * animation = new AnimationDrawable();
	 * animation.addFrame(getResources()
	 * .getDrawable(R.drawable.anim_load2),600);
	 * animation.addFrame(getResources().getDrawable(R.drawable.anim_load3),
	 * 600);
	 * animation.addFrame(getResources().getDrawable(R.drawable.anim_load4
	 * ),600); animation.setOneShot(false); imageAnim = (ImageView)
	 * findViewById(R.id.waiting_attivawebolletta);
	 * imageAnim.setVisibility(View.VISIBLE); if (Build.VERSION.SDK_INT <
	 * 16) { imageAnim.setBackgroundDrawable(animation); } else {
	 * imageAnim.setBackground(animation); } // start the animation!
	 * animation.start(); Intent intentHelp = new
	 * Intent(getApplicationContext(), WebollettaHelp.class);
	 * intentHelp.putExtra("ORIGINE",
	 * Constants.ORIGINE_WEBOLLETTA_ACTIVATE);
	 * intentHelp.putExtra("CodiceConto", codiceConto);
	 * intentHelp.putExtra("TIPOLOGIA", tipologia);
	 * intentHelp.putExtra("OperazioneWeb", operazioneWeb);
	 * WebollettaActivate.this.finish(); startActivity(intentHelp);
	 * overridePendingTransition(R.anim.fade, R.anim.hold); } });
	 */

	final ImageView webollettaAttiva = (ImageView) findViewById(R.id.attiva_webolletta_button);
	webollettaAttiva.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
	webollettaAttiva.getBackground().invalidateSelf();
	webollettaAttiva.setOnClickListener(new View.OnClickListener() {
	    public void onClick(View v) {
		webollettaAttiva.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
		webollettaAttiva.getBackground().invalidateSelf();
		if (operazioneWeb.equals("ATTIVA")) {
		    // chiamata preconsenso e vado alla
		    // webolletta_attiva_post_btn
		    animation = Utilities.animation(WebollettaActivate.this);
		    imageAnim = (ImageView) findViewById(R.id.waiting_anim);
		    imageAnim.setVisibility(View.VISIBLE);
		    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
			imageAnim.setBackgroundDrawable(animation);
		    }
		    else {
			imageAnim.setBackground(animation);
		    }
		    animation.start();
		    String codiceCliente = getCodiceCliente(loginService);
		    final String parametri = "&codiceCliente=" + codiceCliente + "&codiceContoCliente=" + codiceConto + "&tipologiaRichiesta=ATTIVA";

		    if (!isNetworkAvailable(context)) {
			Intent intentOops = new Intent(getApplicationContext(), OopsPageWebollettaActivatePreconsenso.class);
			intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.connessione_assente));
			intentOops.putExtra("PARAMETER", parametri);
			startActivity(intentOops);
			overridePendingTransition(R.anim.fade, R.anim.hold);
		    }
		    else {

			MyHandler handler = new MyHandler() {

			    @Override
			    public void handleMessage(Message msg) {

				if (msg.what == Constants.MAINTENANCE_DISATTIVA) {

				    Log.d("WEBOLLETTA","URL_RICHIESTA_PRECONSENSO - MAINTENANCE_DISATTIVA");

				    /**************** Chiamata richiesta preconsenso ***********************/
				    final ServiceCall visFornDomicilServiceCall = ServiceCallFactory.createServiceCall(context);

				    try {
					visFornDomicilServiceCall.executeHttpsGetWebollettaActivatePreconsenso(Constants.URL_RICHIESTA_PRECONSENSO, parametri, WebollettaActivate.this, null);
				    }
				    catch (Exception e) {
					Intent intentOops = new Intent(getApplicationContext(), OopsPageWebollettaActivatePreconsenso.class);
					intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
					intentOops.putExtra("PARAMETER", parametri);
					startActivity(intentOops);
					overridePendingTransition(R.anim.fade, R.anim.hold);
				    }
				}
			    }
			};

			// maintencance mode
			if (!Constants.STUB_ENABLED) {
			    Utilities.maintenance(WebollettaActivate.this, null, handler);
			}

			// /****************
			// * Chiamata richiesta preconsenso
			// ***********************/
			// final ServiceCall visFornDomicilServiceCall =
			// ServiceCallFactory.createServiceCall(context);
			//
			// try {
			// visFornDomicilServiceCall.executeHttpsGetWebollettaActivatePreconsenso(Constants.URL_RICHIESTA_PRECONSENSO,
			// parametri, WebollettaActivate.this, null);
			// }
			// catch (Exception e) {
			// Intent intentOops = new
			// Intent(getApplicationContext(),
			// OopsPageWebollettaActivatePreconsenso.class);
			// intentOops.putExtra("MESSAGGIO",
			// getResources().getString(R.string.servizio_non_disponibile));
			// intentOops.putExtra("PARAMETER", parametri);
			// startActivity(intentOops);
			// overridePendingTransition(R.anim.fade, R.anim.hold);
			// }

			// try {
			// visFornDomicilServiceCall.executeHttpsGetWebollettaActivatePreconsenso(Constants.URL_RICHIESTA_PRECONSENSO,
			// parametri, WebollettaActivate.this, null);
			// }
			// catch (Exception e) {
			// Intent intentOops = new
			// Intent(getApplicationContext(),
			// OopsPageWebollettaActivatePreconsenso.class);
			// intentOops.putExtra("MESSAGGIO",
			// getResources().getString(R.string.servizio_non_disponibile));
			// intentOops.putExtra("PARAMETER", parametri);
			// startActivity(intentOops);
			// overridePendingTransition(R.anim.fade, R.anim.hold);
			// }

			/************************************************************************/
		    }

		}
		else if (operazioneWeb.equals("NON ATTIVABILE")) {
		    // popup
		    final Dialog dialogDom = new Dialog(WebollettaActivate.this, android.R.style.Theme_Translucent_NoTitleBar);
		    dialogDom.setContentView(R.layout.popup_attiva_domiciliazione);
		    dialogDom.setCancelable(true);

		    final LinearLayout chiudi = (LinearLayout) dialogDom.findViewById(R.id.chiudi);
		    chiudi.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
			    webollettaAttiva.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
			    webollettaAttiva.getBackground().invalidateSelf();
			    dialogDom.dismiss();
			}
		    });
		    final ImageView domiciliazione = (ImageView) dialogDom.findViewById(R.id.vai_a_domiciliazione);
		    domiciliazione.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
		    domiciliazione.getBackground().invalidateSelf();
		    domiciliazione.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
			    domiciliazione.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
			    domiciliazione.getBackground().invalidateSelf();
			    animation = Utilities.animation(WebollettaActivate.this);
			    imageAnim = (ImageView) findViewById(R.id.waiting_anim);
			    imageAnim.setVisibility(View.VISIBLE);
			    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
				imageAnim.setBackgroundDrawable(animation);
			    }
			    else {
				imageAnim.setBackground(animation);
			    }
			    animation.start();

			    JSONArray array;
			    String parametri = "";
			    try {
				array = new JSONArray(loginService);
				String arrayCodiciConti[] = elencoCodiciContiAttiviCompleti(array);
				for (int i = 0; i < arrayCodiciConti.length; i++) {
				    parametri = parametri + "&codiciContoCliente=" + arrayCodiciConti[i];
				}
			    }
			    catch (JSONException e1) {
				e1.printStackTrace();
			    }
			    if (!isNetworkAvailable(context)) {
				Intent intentOops = new Intent(getApplicationContext(), OopsPageWebollettaDomiciliazione.class);
				intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.connessione_assente));
				intentOops.putExtra("PARAMETER", parametri);
				// Home.this.finish();
				startActivity(intentOops);
				overridePendingTransition(R.anim.fade, R.anim.hold);
			    }
			    else {
				/********************* Chiamata visualizza Forniture Domiciliazione ******************************/
				ServiceCall visFornDomicilServiceCall = ServiceCallFactory.createServiceCall(context);
				// visFornDomicilServiceCall.myCookieStore = new
				// PersistentCookieStore(context);
				try {
				    visFornDomicilServiceCall.executeHttpsGetWebollettaActivateDom(Constants.URL_VISUALIZZA_FORNITURE_DOMICILIAZIONE, parametri, WebollettaActivate.this, dialogDom, null);
				}
				catch (Exception e) {
				    Intent intentOops = new Intent(getApplicationContext(), OopsPageWebollettaDomiciliazione.class);
				    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
				    intentOops.putExtra("PARAMETER", parametri);
				    startActivity(intentOops);
				    overridePendingTransition(R.anim.fade, R.anim.hold);
				}
				/**********************************************************************************/
			    }

			}
		    });
		    dialogDom.show();
		}
		else if (operazioneWeb.equals("NESSUNA OPERAZIONE")) {
		    AlertDialog.Builder builder = new AlertDialog.Builder(context);
		    builder.setTitle("Attenzione").setMessage(R.string.operazione_non_consentita).setCancelable(false).setPositiveButton("Chiudi", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
			    if (animation != null) {
				animation.stop();
				imageAnim.setVisibility(View.INVISIBLE);
			    }
			}
		    });
		    builder.create().show();
		}
		else {
		    AlertDialog.Builder builder = new AlertDialog.Builder(context);
		    builder.setTitle("Attenzione").setMessage(R.string.operazione_non_consentita_call).setCancelable(false).setPositiveButton("Chiudi", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
			    if (animation != null) {
				animation.stop();
				imageAnim.setVisibility(View.INVISIBLE);
			    }
			}
		    });
		    builder.create().show();
		}

	    }
	});

	TextView numeroCliente = (TextView) findViewById(R.id.text_numero_cliente_attiva_domiciliazione);
	numeroCliente.setText(codiceConto);

	ImageView iconaLuce = (ImageView) findViewById(R.id.webolletta_luce_icona);
	ImageView iconaGas = (ImageView) findViewById(R.id.webolletta_gas_icona);
	if (tipologia.equals("DUAL")) {
	    iconaLuce.setBackgroundResource(R.drawable.light_yes);
	    iconaGas.setBackgroundResource(R.drawable.gas_yes);
	}
	else if (tipologia.equals("LUCE")) {
	    iconaLuce.setBackgroundResource(R.drawable.light_yes);
	    iconaGas.setBackgroundResource(R.drawable.gas_no);
	}
	else {
	    iconaLuce.setBackgroundResource(R.drawable.light_no);
	    iconaGas.setBackgroundResource(R.drawable.gas_yes);
	}

	/*
	 * TextView condizioni_servizio =
	 * (TextView)findViewById(R.id.text_activation_condiction2);
	 * condizioni_servizio.setOnClickListener(new View.OnClickListener() {
	 * public void onClick(View v) { final Dialog dialog = new
	 * Dialog(WebollettaActivate.this,
	 * android.R.style.Theme_Translucent_NoTitleBar);
	 * dialog.setContentView(R.layout.popup_condizioni_servizio);
	 * dialog.setCancelable(true);
	 * 
	 * final LinearLayout chiudi = (LinearLayout)
	 * dialog.findViewById(R.id.chiudi); chiudi.setOnClickListener(new
	 * View.OnClickListener() { public void onClick(View v) {
	 * dialog.dismiss(); } }); } });
	 */
    }

    public void postChiamata(String rispostaChiamata, List<Cookie> lista, HashMap<String, String> parametri, Dialog dialog, boolean isPreconsenso) {
	if (animation != null) {
	    animation.stop();
	    imageAnim.setVisibility(View.INVISIBLE);
	}
	if (dialog != null) {
	    dialog.dismiss();
	}

	JSONArray rispostaJSON = new JSONArray();
	String esito = "";
	String descEsito = "";
	try {
	    rispostaJSON = new JSONArray(rispostaChiamata);
	    JSONObject oggettoRisposta = (JSONObject) rispostaJSON.get(0);
	    esito = oggettoRisposta.getString("esito");
	    descEsito = oggettoRisposta.getString("descEsito");
	}
	catch (JSONException e) {
	    e.printStackTrace();
	}
	if (esito.equals("200")) {
	    if (!isPreconsenso) {
		Constants.SERVIZIO_VISUALIZZA_FORNITURE_DOMICILIAZIONE = rispostaChiamata;
		Intent intentVisualizzaFornitureDomiciliazione = new Intent(getApplicationContext(), Domiciliazione.class);
		WebollettaActivate.this.finish();
		startActivity(intentVisualizzaFornitureDomiciliazione);
		overridePendingTransition(R.anim.fade, R.anim.hold);
	    }
	    else {
		Constants.SERVIZIO_RICHIESTA_PRECONSENSO_FATTURA_ONLINE = rispostaChiamata;
		Intent intentVisualizzaFornitureDomiciliazione = new Intent(getApplicationContext(), WebollettaAttivazionePostPreconsenso.class);
		intentVisualizzaFornitureDomiciliazione.putExtra("CodiceConto", codiceConto);
		intentVisualizzaFornitureDomiciliazione.putExtra("TIPOLOGIA", tipologia);
		intentVisualizzaFornitureDomiciliazione.putExtra("OperazioneWeb", operazioneWeb);

		WebollettaActivate.this.finish();
		startActivity(intentVisualizzaFornitureDomiciliazione);
		overridePendingTransition(R.anim.fade, R.anim.hold);
	    }

	}
	else {
	    Intent intentOops = new Intent();
	    if (!isPreconsenso) {
		intentOops = new Intent(getApplicationContext(), OopsPageWebollettaDomiciliazione.class);
		intentOops.putExtra("PARAMETER", parametri.get("PARAMETER"));
	    }
	    else {
		intentOops = new Intent(getApplicationContext(), OopsPageWebollettaActivatePreconsenso.class);
		intentOops.putExtra("PARAMETER", parametri.get("PARAMETER"));
	    }

	    // errori ops page
	    if (esito.equals("309")) {
		intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
		startActivity(intentOops);
		overridePendingTransition(R.anim.fade, R.anim.hold);

	    }
	    else if (esito.equals("400")) {
		intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
		startActivity(intentOops);
		overridePendingTransition(R.anim.fade, R.anim.hold);

	    }
	    else if (esito.equals("420")) {
		intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
		startActivity(intentOops);
		overridePendingTransition(R.anim.fade, R.anim.hold);

	    }
	    else if (esito.equals("449")) {
		intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
		startActivity(intentOops);
		overridePendingTransition(R.anim.fade, R.anim.hold);

	    }
	    else if (esito.equals("451")) {
		intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_popup_451));
		startActivity(intentOops);
		overridePendingTransition(R.anim.fade, R.anim.hold);

	    }
	    // errori popup
	    else if (esito.equals("307")) {
		Utils.popup(WebollettaActivate.this, getResources().getString(R.string.errore_popup_307), "Riprova");

	    }
	    else if (esito.equals("435")) {
		Utils.popup(WebollettaActivate.this, getResources().getString(R.string.errore_popup_435), "Riprova");

	    }
	    else if (esito.equals("437")) {
		Utils.popup(WebollettaActivate.this, getResources().getString(R.string.errore_popup_437), "Riprova");

	    }
	    else if (esito.equals("438")) {
		Utils.popup(WebollettaActivate.this, getResources().getString(R.string.errore_popup_438), "Riprova");

	    }
	    else if (esito.equals("443")) {
		Utils.popup(WebollettaActivate.this, getResources().getString(R.string.errore_popup_443), "Riprova");

	    }
	    else if (esito.equals("444")) {
		Utils.popup(WebollettaActivate.this, getResources().getString(R.string.errore_popup_444), "Riprova");

	    }
	    else if (esito.equals("445")) {
		Utils.popup(WebollettaActivate.this, getResources().getString(R.string.errore_popup_445), "Riprova");

	    }
	    else if (esito.equals("446")) {
		Utils.popup(WebollettaActivate.this, getResources().getString(R.string.errore_popup_446), "Riprova");

	    }
	    else if (esito.equals("447")) {
		Utils.popup(WebollettaActivate.this, getResources().getString(R.string.errore_popup_447), "Riprova");

	    }
	    else {
	    	if (descEsito.equals("")) {
				intentOops = new Intent(getApplicationContext(), OopsPageSessioneScaduta.class);
				intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.sessione_scaduta));
			} else{
				intentOops.putExtra("MESSAGGIO", descEsito);
			}
			
			startActivity(intentOops);
			overridePendingTransition(R.anim.fade, R.anim.hold);
	    }

	}

    }

    public void postChiamataException(boolean exception, String parametri, boolean isPreconsenso) {
	if (exception) {
	    if (animation != null) {
		animation.stop();
		imageAnim.setVisibility(View.INVISIBLE);
	    }
	    Intent intentOops = new Intent();
	    if (!isPreconsenso) {
		intentOops = new Intent(getApplicationContext(), OopsPageWebollettaDomiciliazione.class);
		intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
		intentOops.putExtra("PARAMETER", parametri);
		startActivity(intentOops);
		overridePendingTransition(R.anim.fade, R.anim.hold);
	    }
	    else {
		intentOops = new Intent(getApplicationContext(), OopsPageWebollettaActivatePreconsenso.class);
		intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
		intentOops.putExtra("PARAMETER", parametri);
		startActivity(intentOops);
		overridePendingTransition(R.anim.fade, R.anim.hold);
	    }

	}
    }

    public String getCodiceCliente(String rispostaLogin) {
	String result = "";
	try {
	    JSONArray loginJson = new JSONArray(rispostaLogin);
	    JSONObject datiCliente = (JSONObject) loginJson.get(1);
	    result = datiCliente.getString("CodiceCliente");
	}
	catch (JSONException e) {
	    e.printStackTrace();
	}
	return result;
    }

    private boolean isNetworkAvailable(Context context) {
	ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
	if (connectivityManager == null) return false;
	NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
	return activeNetworkInfo != null;
    }

    public static Context getContext() {
	return WebollettaActivate.staticContext;
    }

    public String[] elencoCodiciContiAttiviCompleti(JSONArray array) {
	int lunghezza = numeroCodiciContoDiversiAttiviCompleti(array);
	String result[] = new String[lunghezza];
	String codiceContoConfrontato = "";
	String codiceContoIesimo = "";
	String codiciConcatenati = "";
	int count = 0;
	for (int i = 2; i < array.length(); i++) {
	    JSONArray contoIesimo;
	    try {
		contoIesimo = array.getJSONArray(i);
		// codice conto
		JSONObject anagrContoIesimo = contoIesimo.getJSONObject(0);
		codiceContoIesimo = anagrContoIesimo.getString("CodiceConto");

		if (!codiceContoIesimo.equals(codiceContoConfrontato)) {
		    String stato = anagrContoIesimo.getString("StatoConto");
		    if (!stato.equals("UTILIZZABILE")) {
			continue;
		    }
		    else {
			// codice conto utilizzabile diverso, controllo non sia
			// vuoto
			if (contoIesimo.length() > 1) {
			    // che non sia gia' presente
			    boolean isPresente = codiciConcatenati.contains("-" + codiceContoIesimo + "-");
			    if (isPresente) {
				continue;
			    }
			    else {
				codiceContoConfrontato = codiceContoIesimo;
				result[count] = codiceContoConfrontato;
				codiciConcatenati = codiciConcatenati + "-" + codiceContoIesimo + "-";
				count++;
			    }
			}
			else {
			    continue;
			}
		    }
		}
	    }
	    catch (JSONException e) {
		e.printStackTrace();
	    }
	}
	return result;
    }

    public int numeroCodiciContoDiversiAttiviCompleti(JSONArray array) {
	int result = 0;
	String codiceContoConfrontato = "";
	String codiceContoIesimo = "";
	String codiciConcatenati = "";
	for (int i = 2; i < array.length(); i++) {
	    JSONArray contoIesimo;
	    try {
		contoIesimo = array.getJSONArray(i);
		// codice conto
		JSONObject anagrContoIesimo = contoIesimo.getJSONObject(0);
		codiceContoIesimo = anagrContoIesimo.getString("CodiceConto");

		if (!codiceContoIesimo.equals(codiceContoConfrontato)) {
		    String stato = anagrContoIesimo.getString("StatoConto");
		    if (!stato.equals("UTILIZZABILE")) {
			continue;
		    }
		    else {
			// codice conto utilizzabile diverso, controllo non sia
			// vuoto
			if (contoIesimo.length() > 1) {
			    if (!codiciConcatenati.contains("-" + codiceContoIesimo)) {
				// codice conto diverso
				codiceContoConfrontato = codiceContoIesimo;
				codiciConcatenati = codiciConcatenati + "-" + codiceContoIesimo;
				result++;
			    }
			}
			else {
			    continue;
			}
		    }
		}
	    }
	    catch (JSONException e) {
		e.printStackTrace();
	    }
	}
	return result;
    }

    public void condizioniServizio(View v) {
	final Dialog dialog = new Dialog(WebollettaActivate.this, android.R.style.Theme_Translucent_NoTitleBar);
	dialog.setContentView(R.layout.popup_condizioni_servizio);
	dialog.setCancelable(true);

	final LinearLayout chiudi = (LinearLayout) dialog.findViewById(R.id.chiudi);
	chiudi.setOnClickListener(new View.OnClickListener() {
	    public void onClick(View v) {
		dialog.dismiss();
	    }
	});
	dialog.show();
    }

    public void onBackPressed() {
	animation = Utilities.animation(WebollettaActivate.this);
	imageAnim = (ImageView) findViewById(R.id.waiting_anim);
	imageAnim.setVisibility(View.VISIBLE);
	if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
	    imageAnim.setBackgroundDrawable(animation);
	}
	else {
	    imageAnim.setBackground(animation);
	}
	animation.start();
	Intent intentIndietro = new Intent(getApplicationContext(), Webolletta.class);
	WebollettaActivate.this.finish();
	startActivity(intentIndietro);
	overridePendingTransition(R.anim.fade, R.anim.hold);
    }
}
