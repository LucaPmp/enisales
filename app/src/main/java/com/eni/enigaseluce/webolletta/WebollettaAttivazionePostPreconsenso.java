package com.eni.enigaseluce.webolletta;

import java.util.HashMap;
import java.util.List;

import org.apache.http.cookie.Cookie;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.AnimationDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.eni.enigaseluce.R;
import com.eni.enigaseluce.error.OopsPageSessioneScaduta;
import com.eni.enigaseluce.error.OopsPageWebollettaActivateConfermaMail;
import com.eni.enigaseluce.httpcall.Constants;
import com.eni.enigaseluce.httpcall.ServiceCall;
import com.eni.enigaseluce.httpcall.ServiceCallFactory;
import com.eni.enigaseluce.utils.Utilities;

@SuppressLint("NewApi")
public class WebollettaAttivazionePostPreconsenso extends Activity {
    private Context context;
    private static Context staticContext;
    AnimationDrawable animation;
    ImageView imageAnim;
    String codiceConto;
    String tipologia;
    String operazioneWeb;
    String nomeClienteString = "";
    String cognomeClienteString = "";
    String indirizzoMail = "";

    public String rispostaLogin = Constants.SERVIZIO_LOGIN;
    public String rispostaPreconsenso = Constants.SERVIZIO_RICHIESTA_PRECONSENSO_FATTURA_ONLINE;

    String codiceEsito;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.webolletta_attiva_post_btn);
        context = this;
        WebollettaAttivazionePostPreconsenso.staticContext = this;
        Intent intent = getIntent();
        codiceConto = intent.getStringExtra("CodiceConto");
        tipologia = intent.getStringExtra("TIPOLOGIA");
        operazioneWeb = intent.getStringExtra("OperazioneWeb");

        final ImageView indietro = (ImageView) findViewById(R.id.webolletta_indietro);
        indietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
        indietro.getBackground().invalidateSelf();
        indietro.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v1) {
                indietro.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
                indietro.getBackground().invalidateSelf();
                animation = Utilities.animation(WebollettaAttivazionePostPreconsenso.this);
                imageAnim = (ImageView) findViewById(R.id.waiting_anim);
                imageAnim.setVisibility(View.VISIBLE);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    imageAnim.setBackgroundDrawable(animation);
                } else {
                    imageAnim.setBackground(animation);
                }
                animation.start();
                Intent intentIndietro = new Intent(getApplicationContext(), WebollettaActivate.class);
                intentIndietro.putExtra("CodiceConto", codiceConto);
                intentIndietro.putExtra("TIPOLOGIA", tipologia);
                intentIndietro.putExtra("OperazioneWeb", operazioneWeb);
                WebollettaAttivazionePostPreconsenso.this.finish();
                startActivity(intentIndietro);
                overridePendingTransition(R.anim.fade, R.anim.hold);
            }
        });

	/*
     * final ImageView domiciliazioneHelp = (ImageView)
	 * findViewById(R.id.webolletta_help);
	 * domiciliazioneHelp.getBackground().setColorFilter(0xFFFFFFFF,
	 * PorterDuff.Mode.MULTIPLY);
	 * domiciliazioneHelp.getBackground().invalidateSelf();
	 * domiciliazioneHelp.setOnClickListener(new View.OnClickListener() {
	 * public void onClick(View v1) {
	 * domiciliazioneHelp.getBackground().setColorFilter(0xFF999999,
	 * PorterDuff.Mode.MULTIPLY);
	 * domiciliazioneHelp.getBackground().invalidateSelf(); animation = new
	 * AnimationDrawable();
	 * animation.addFrame(getResources().getDrawable(R.drawable
	 * .anim_load2),300);
	 * animation.addFrame(getResources().getDrawable(R.drawable.anim_load3),
	 * 300);
	 * animation.addFrame(getResources().getDrawable(R.drawable.anim_load4
	 * ),300); animation.setOneShot(false); imageAnim = (ImageView)
	 * findViewById(R.id.waiting_webolletta_attivazione_notifica);
	 * imageAnim.setVisibility(View.VISIBLE); if (Build.VERSION.SDK_INT <
	 * 16) { imageAnim.setBackgroundDrawable(animation); } else {
	 * imageAnim.setBackground(animation); } // start the animation!
	 * animation.start(); Intent intentHelp = new
	 * Intent(getApplicationContext(), WebollettaHelp.class);
	 * intentHelp.putExtra("ORIGINE",
	 * Constants.ORIGINE_WEBOLLETTA_ATTIVAZIONE_POST_PRECONSENSO);
	 * intentHelp.putExtra("CodiceConto", codiceConto);
	 * intentHelp.putExtra("TIPOLOGIA", tipologia);
	 * intentHelp.putExtra("OperazioneWeb", operazioneWeb);
	 * WebollettaAttivazionePostPreconsenso.this.finish();
	 * startActivity(intentHelp); overridePendingTransition(R.anim.fade,
	 * R.anim.hold); } });
	 */

        final ImageView annulla = (ImageView) findViewById(R.id.annulla);
        annulla.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
        annulla.getBackground().invalidateSelf();
        annulla.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                annulla.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
                annulla.getBackground().invalidateSelf();
                Intent intentWebolletta = new Intent(getApplicationContext(), Webolletta.class);
                WebollettaAttivazionePostPreconsenso.this.finish();
                startActivity(intentWebolletta);
                overridePendingTransition(R.anim.fade, R.anim.hold);
            }
        });

        final EditText cel = (EditText) findViewById(R.id.cellulare);
        final ImageView accetta = (ImageView) findViewById(R.id.accetta);
        accetta.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
        accetta.getBackground().invalidateSelf();
        accetta.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                accetta.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
                accetta.getBackground().invalidateSelf();

                CheckBox checkbox = (CheckBox) findViewById(R.id.checkbox);
                boolean flag = checkbox.isChecked();
                if (!flag) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setTitle("Attenzione!").setMessage(R.string.selezionare_checkbox).setCancelable(false).setPositiveButton("Chiudi", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            if (animation != null) {
                                animation.stop();
                                imageAnim.setVisibility(View.INVISIBLE);
                            }
                            accetta.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
                            accetta.getBackground().invalidateSelf();
                        }
                    });
                    builder.create().show();
                } else {

                    if (cel != null && cel.getText() != null && cel.getText().length() > 0) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setTitle("Attiva Notifica").setMessage(R.string.webolletta_attivazione_notifica_popup).setCancelable(true).setNegativeButton("Annulla", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                            }
                        }).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                // chiamata invia richiesta conferma mail
                                callServiceMail();

                            }
                        });
                        builder.create().show();
                    } else {

                        // chiamata invia richiesta conferma mail
                        callServiceMail();
                    }
                }
            }
        });
        try {
            final JSONArray loginJSON = new JSONArray(rispostaLogin);
            JSONObject esito = loginJSON.getJSONObject(0);
            codiceEsito = (String) esito.getString("esito");
            if (codiceEsito.equals("200")) {
                JSONObject datiAnagr = loginJSON.getJSONObject(1);
                // ultimo accesso
                String ultimoaccesso = datiAnagr.getString("UltimoAccesso");
                TextView ultimoAccesso = (TextView) findViewById(R.id.ultimo_accesso);
                // ultimoAccesso.setTextSize(11);
                ultimoAccesso.setText(ultimoaccesso);
                // nome cliente
                nomeClienteString = datiAnagr.getString("NomeCliente");
                cognomeClienteString = datiAnagr.getString("CognomeCliente");
                TextView nomeCliente = (TextView) findViewById(R.id.nome_cliente);
                String nomeInteroCliente = nomeClienteString + " " + cognomeClienteString;
                nomeCliente.setText(nomeInteroCliente);

                TextView contoCodiceText = (TextView) findViewById(R.id.webolletta_detail_numero_cliente_text);
                contoCodiceText.setText(codiceConto);

                indirizzoMail = cercaIndirizzoMail(new JSONArray(rispostaPreconsenso));
                TextView mail = (TextView) findViewById(R.id.attivazione_webolletta_mail);
                mail.setText(indirizzoMail);

                String cellulare = cercaCellulare(new JSONArray(rispostaPreconsenso));
                cel.setText(cellulare);

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void postChiamata(String rispostaChiamata, List<Cookie> lista, HashMap<String, String> parametri) {
        if (animation != null) {
            animation.stop();
            imageAnim.setVisibility(View.INVISIBLE);
        }
        JSONArray rispostaJSON = new JSONArray();
        String esito = "";
        String descEsito = "";
        try {
            rispostaJSON = new JSONArray(rispostaChiamata);
            JSONObject oggettoRisposta = (JSONObject) rispostaJSON.get(0);
            esito = oggettoRisposta.getString("esito");
            descEsito = oggettoRisposta.getString("descEsito");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (esito.equals("200")) {
            Constants.SERVIZIO_RICHIESTA_CONFERMA_MAIL = rispostaChiamata;

            Intent intentWebollettaDetail = new Intent(getApplicationContext(), WebollettaDetail.class);
            intentWebollettaDetail.putExtra("CodiceConto", parametri.get("codiceContoCliente"));
            intentWebollettaDetail.putExtra("StatoWeb", "SERVIZIO IN ATTIVAZIONE");
            intentWebollettaDetail.putExtra("Chiamata", true);
            WebollettaAttivazionePostPreconsenso.this.finish();
            startActivity(intentWebollettaDetail);
            overridePendingTransition(R.anim.fade, R.anim.hold);
        } else {
            Intent intentOops = new Intent();
            intentOops = new Intent(getApplicationContext(), OopsPageWebollettaActivateConfermaMail.class);
            intentOops.putExtra("CODICE_CLIENTE", parametri.get("codiceCliente"));
            intentOops.putExtra("NOME", parametri.get("nome"));
            intentOops.putExtra("COGNOME", parametri.get("cognome"));
            intentOops.putExtra("TIPOLOGIA_CLIENTE", parametri.get("tipologia"));
            intentOops.putExtra("CODICE_CONTO_CLIENTE", parametri.get("codiceContoCliente"));
            intentOops.putExtra("TIPOLOGIA_RICHIESTA", parametri.get("tipologiaRichiesta"));
            intentOops.putExtra("EMAIL", parametri.get("eMail"));
            intentOops.putExtra("CELLULARE", parametri.get("cellulare"));
            // errori popup
            if (esito.equals("314") || esito.equals("317")) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Attenzione!").setMessage(descEsito).setCancelable(false).setPositiveButton("Chiudi", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (animation != null) {
                            animation.stop();
                            imageAnim.setVisibility(View.INVISIBLE);
                        }
                    }
                });
                builder.create().show();
            } else {
                if (descEsito.equals("")) {
                    intentOops = new Intent(getApplicationContext(), OopsPageSessioneScaduta.class);
                    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.sessione_scaduta));
                } else {
                    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
                }

                startActivity(intentOops);
                overridePendingTransition(R.anim.fade, R.anim.hold);
            }

        }

    }

    public void postChiamataException(boolean exception, HashMap<String, String> parameterMap) {
        if (exception) {
            if (animation != null) {
                animation.stop();
                imageAnim.setVisibility(View.INVISIBLE);
            }
            Intent intentOops = new Intent(getApplicationContext(), OopsPageWebollettaActivateConfermaMail.class);
            intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
            intentOops.putExtra("CODICE_CLIENTE", parameterMap.get("codiceCliente"));
            intentOops.putExtra("NOME", parameterMap.get("nome"));
            intentOops.putExtra("COGNOME", parameterMap.get("cognome"));
            intentOops.putExtra("TIPOLOGIA_CLIENTE", parameterMap.get("tipologia"));
            intentOops.putExtra("CODICE_CONTO_CLIENTE", parameterMap.get("codiceContoCliente"));
            intentOops.putExtra("TIPOLOGIA_RICHIESTA", parameterMap.get("tipologiaRichiesta"));
            intentOops.putExtra("EMAIL", parameterMap.get("eMail"));
            intentOops.putExtra("CELLULARE", parameterMap.get("cellulare"));
            startActivity(intentOops);
            overridePendingTransition(R.anim.fade, R.anim.hold);
        }
    }

    private boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager == null) return false;
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }

    public String cercaIndirizzoMail(JSONArray array) {
        String result = "";
        try {
            JSONObject oggetto = array.getJSONObject(1);
            result = oggetto.getString("EMailRegistrazione");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public String cercaCellulare(JSONArray array) {
        String result = "";
        try {
            JSONObject oggetto = array.getJSONObject(1);
            result = oggetto.getString("Cellulare");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public void condizioniServizio(View v) {
        final Dialog dialog = new Dialog(WebollettaAttivazionePostPreconsenso.this, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.setContentView(R.layout.popup_condizioni_servizio);
        dialog.setCancelable(true);

        final LinearLayout chiudi = (LinearLayout) dialog.findViewById(R.id.chiudi);
        chiudi.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public String getCodiceCliente(String rispostaLogin) {
        String result = "";
        try {
            JSONArray loginJson = new JSONArray(rispostaLogin);
            JSONObject datiCliente = (JSONObject) loginJson.get(1);
            result = datiCliente.getString("CodiceCliente");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public String getTipologiaCliente(String rispostaLogin) {
        String result = "";
        try {
            JSONArray loginJson = new JSONArray(rispostaLogin);
            JSONObject datiCliente = (JSONObject) loginJson.get(1);
            result = datiCliente.getString("TipologiaCliente");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static Context getContext() {
        return WebollettaAttivazionePostPreconsenso.staticContext;
    }

    public void onBackPressed() {
        animation = Utilities.animation(WebollettaAttivazionePostPreconsenso.this);
        imageAnim = (ImageView) findViewById(R.id.waiting_anim);
        imageAnim.setVisibility(View.VISIBLE);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
            imageAnim.setBackgroundDrawable(animation);
        } else {
            imageAnim.setBackground(animation);
        }
        animation.start();
        Intent intentIndietro = new Intent(getApplicationContext(), WebollettaActivate.class);
        intentIndietro.putExtra("CodiceConto", codiceConto);
        intentIndietro.putExtra("TIPOLOGIA", tipologia);
        intentIndietro.putExtra("OperazioneWeb", operazioneWeb);
        WebollettaAttivazionePostPreconsenso.this.finish();
        startActivity(intentIndietro);
        overridePendingTransition(R.anim.fade, R.anim.hold);
    }

    public void callServiceMail() {
        // chiamata invia richiesta conferma mail
        /*********************** Chiamata invia richiesta canferma mail *********************/
        animation = Utilities.animation(WebollettaAttivazionePostPreconsenso.this);
        imageAnim = (ImageView) findViewById(R.id.waiting_anim);
        imageAnim.setVisibility(View.VISIBLE);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
            imageAnim.setBackgroundDrawable(animation);
        } else {
            imageAnim.setBackground(animation);
        }
        animation.start();
        String codiceCliente = getCodiceCliente(rispostaLogin);
        String tipologiaCliente = getTipologiaCliente(rispostaLogin);
        HashMap<String, String> parametri = new HashMap<String, String>();
        parametri.put("codiceCliente", codiceCliente);
        parametri.put("nome", nomeClienteString);
        parametri.put("cognome", cognomeClienteString);
        parametri.put("tipologia", tipologiaCliente);
        parametri.put("codiceContoCliente", codiceConto);
        parametri.put("tipologiaRichiesta", "ATTIVA");
        parametri.put("eMail", indirizzoMail);
        EditText cel = (EditText) findViewById(R.id.cellulare);
        String celText = cel.getText().toString();
        parametri.put("cellulare", celText);

        if (!isNetworkAvailable(context)) {
            Intent intentOops = new Intent(getApplicationContext(), OopsPageWebollettaActivateConfermaMail.class);
            intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.connessione_assente));
            intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
            intentOops.putExtra("NOME", nomeClienteString);
            intentOops.putExtra("COGNOME", cognomeClienteString);
            intentOops.putExtra("TIPOLOGIA_CLIENTE", tipologiaCliente);
            intentOops.putExtra("CODICE_CONTO_CLIENTE", codiceConto);
            intentOops.putExtra("TIPOLOGIA_RICHIESTA", "ATTIVA");
            intentOops.putExtra("EMAIL", indirizzoMail);
            intentOops.putExtra("CELLULARE", celText);
            startActivity(intentOops);
            overridePendingTransition(R.anim.fade, R.anim.hold);
        } else {
            /**************** Chiamata richiesta preconsenso ***********************/
            ServiceCall visFornDomicilServiceCall = ServiceCallFactory.createServiceCall(context);
            // visFornDomicilServiceCall.myCookieStore = new
            // PersistentCookieStore(context);
            try {
                visFornDomicilServiceCall.executeHttpsGetWebollettaActivatePostPreconsenso(Constants.URL_RICHIESTA_CONFERMA_MAIL, parametri, WebollettaAttivazionePostPreconsenso.this, null);
            } catch (Exception e) {
                Intent intentOops = new Intent(getApplicationContext(), OopsPageWebollettaActivateConfermaMail.class);
                intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
                intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
                intentOops.putExtra("NOME", nomeClienteString);
                intentOops.putExtra("COGNOME", cognomeClienteString);
                intentOops.putExtra("TIPOLOGIA_CLIENTE", tipologiaCliente);
                intentOops.putExtra("CODICE_CONTO_CLIENTE", codiceConto);
                intentOops.putExtra("TIPOLOGIA_RICHIESTA", "ATTIVA");
                intentOops.putExtra("EMAIL", indirizzoMail);
                intentOops.putExtra("CELLULARE", celText);
                startActivity(intentOops);
                overridePendingTransition(R.anim.fade, R.anim.hold);
            }
            /*************************************************************************************/
        }
    }
}
