package com.eni.enigaseluce.webolletta;

import java.util.HashMap;
import java.util.List;

import org.apache.http.cookie.Cookie;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.AnimationDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.eni.enigaseluce.R;
import com.eni.enigaseluce.error.OopsPageEniWebBollettaDetail;
import com.eni.enigaseluce.error.OopsPageSessioneScaduta;
import com.eni.enigaseluce.error.Utils;
import com.eni.enigaseluce.httpcall.Constants;
import com.eni.enigaseluce.httpcall.ServiceCall;
import com.eni.enigaseluce.httpcall.ServiceCallFactory;
import com.eni.enigaseluce.utils.Utilities;

@SuppressLint("NewApi")
public class WebollettaDetail extends Activity {
    String codiceConto = "";
    String indirizzoFornitura = "";
    String stato = "";
    boolean chiamata;
    public String rispostaLogin = Constants.SERVIZIO_LOGIN;
    public String rispostavisualizzaWebolletta = Constants.SERVIZIO_VISUALIZZA_FORNITURE_DOMICILIAZIONE;

    AnimationDrawable animation;
    ImageView imageAnim;
    Context context;
    static Context staticContext;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.webolletta_detail);
        context = this;
        WebollettaDetail.staticContext = this;
        Intent intent = getIntent();
        codiceConto = intent.getStringExtra("CodiceConto");
        stato = intent.getStringExtra("StatoWeb");
        chiamata = intent.getBooleanExtra("Chiamata", false);

	

	/*
     * final ImageView help = (ImageView)
	 * findViewById(R.id.webolletta_detail_help);
	 * help.getBackground().setColorFilter(0xFFFFFFFF,
	 * PorterDuff.Mode.MULTIPLY); help.getBackground().invalidateSelf();
	 * help.setOnClickListener(new View.OnClickListener() { public void
	 * onClick(View v1) { help.getBackground().setColorFilter(0xFF999999,
	 * PorterDuff.Mode.MULTIPLY); help.getBackground().invalidateSelf();
	 * animation = new AnimationDrawable();
	 * animation.addFrame(getResources()
	 * .getDrawable(R.drawable.anim_load2),600);
	 * animation.addFrame(getResources().getDrawable(R.drawable.anim_load3),
	 * 600);
	 * animation.addFrame(getResources().getDrawable(R.drawable.anim_load4
	 * ),600); animation.setOneShot(false); imageAnim = (ImageView)
	 * findViewById(R.id.waiting_webolletta_detail);
	 * imageAnim.setVisibility(View.VISIBLE); if (Build.VERSION.SDK_INT <
	 * 16) { imageAnim.setBackgroundDrawable(animation); } else {
	 * imageAnim.setBackground(animation); } // start the animation!
	 * animation.start();
	 * 
	 * Intent intentHome = new Intent(getApplicationContext(),
	 * WebollettaHelp.class); intentHome.putExtra("ORIGINE",
	 * Constants.ORIGINE_WEBOLLETTA_DETAIL);
	 * intentHome.putExtra("CodiceConto", codiceConto);
	 * intentHome.putExtra("StatoWeb", stato);
	 * intentHome.putExtra("Chiamata", false);
	 * webolletta_detail_indietro_disabled.getBackground().setAlpha(255);
	 * WebollettaDetail.this.finish(); startActivity(intentHome);
	 * overridePendingTransition(R.anim.fade, R.anim.hold); } });
	 */

        try {
            final JSONArray loginJSON = new JSONArray(rispostaLogin);
            JSONObject esito = loginJSON.getJSONObject(0);
            String codiceEsito = (String) esito.getString("esito");
            if (codiceEsito.equals("200")) {
                JSONObject datiAnagr = loginJSON.getJSONObject(1);
                // ultimo accesso
                String ultimoaccesso = datiAnagr.getString("UltimoAccesso");
                TextView ultimoAccesso = (TextView) findViewById(R.id.ultimo_accesso);
                // ultimoAccesso.setTextSize(11);
                ultimoAccesso.setText(ultimoaccesso);
                // nome cliente
                String nomeClienteString = datiAnagr.getString("NomeCliente");
                String cognomeClienteString = datiAnagr.getString("CognomeCliente");
                TextView nomeCliente = (TextView) findViewById(R.id.nome_cliente);
                String nomeInteroCliente = nomeClienteString + " " + cognomeClienteString;
                nomeCliente.setText(nomeInteroCliente);

                TextView numeroConto = (TextView) findViewById(R.id.webolletta_detail_numero_cliente_text);
                numeroConto.setText(codiceConto);

                TextView statoTotaleText = (TextView) findViewById(R.id.webolletta_detail_descrizione_text);

                //TextView statoText = (TextView) findViewById(R.id.webolletta_detail_descrizione_stato);
                TextView descrizioneStato = (TextView) findViewById(R.id.webolletta_detail_descrizione2_text);
                if (stato.equals("SERVIZIO IN ATTIVAZIONE")) {
                    statoTotaleText.setText(Html.fromHtml(getString(R.string.webolletta_detail_1) + " <b>" + getString(R.string.webolletta_detail_stato_attivazione)+"</b> "+getString(R.string.webolletta_detail_2)));
                    //statoText.setText(getResources().getString(R.string.webolletta_detail_stato_attivazione));
                    descrizioneStato.setText(getResources().getString(R.string.weblolletta_detail_text_2_attivazione));
                } else {
                    statoTotaleText.setText("Hai scelto di disattivare la bolletta digitale sul numero cliente");
                    //statoText.setText(getResources().getString(R.string.webolletta_detail_stato_disattivazione));
                    descrizioneStato.setText(getResources().getString(R.string.weblolletta_detail_text_2_disattivazione));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onStart() {
        // TODO Auto-generated method stub
        super.onStart();

        final ImageView webolletta_detail_indietro_disabled = (ImageView) findViewById(R.id.webolletta_detail_indietro_disabled);
        webolletta_detail_indietro_disabled.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
        webolletta_detail_indietro_disabled.getBackground().setAlpha(255);
        webolletta_detail_indietro_disabled.getBackground().setAlpha(80);
        webolletta_detail_indietro_disabled.getBackground().invalidateSelf();

        final ImageView indietro = (ImageView) findViewById(R.id.webolletta_detail_indietro);
        indietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
        indietro.getBackground().invalidateSelf();
        indietro.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v1) {
                indietro.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
                indietro.getBackground().invalidateSelf();
                animation = Utilities.animation(WebollettaDetail.this);
                imageAnim = (ImageView) findViewById(R.id.waiting_anim);
                imageAnim.setVisibility(View.VISIBLE);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    imageAnim.setBackgroundDrawable(animation);
                } else {
                    imageAnim.setBackground(animation);
                }
                animation.start();
                if (chiamata) {
                    String codiceCliente = getCodiceCliente(rispostaLogin);
                    if (!isNetworkAvailable(context)) {
                        Intent intentOops = new Intent(getApplicationContext(), OopsPageEniWebBollettaDetail.class);
                        intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.connessione_assente));
                        intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
                        startActivity(intentOops);
                        if (animation != null) {
                            animation.stop();
                            imageAnim.setVisibility(View.INVISIBLE);
                        }
                        overridePendingTransition(R.anim.fade, R.anim.hold);
                    } else {
                        /********************* Chiamata webolletta *****************/
                        ServiceCall eniWebBollettaServiceCall = ServiceCallFactory.createServiceCall(context);

                        HashMap<String, String> parametri = new HashMap<String, String>();
                        parametri.put("codiceCliente", codiceCliente);

                        try {
                            eniWebBollettaServiceCall.executeHttpsGetWebollettaDetail(Constants.URL_INFO_FATTURA_ONLINE, parametri, WebollettaDetail.this, null);
                        } catch (Exception e) {
                            Intent intentOops = new Intent(getApplicationContext(), OopsPageEniWebBollettaDetail.class);
                            intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
                            intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
                            startActivity(intentOops);
                            overridePendingTransition(R.anim.fade, R.anim.hold);
                        }

                        /*******************************************************************************/
                    }
                    /**************************************/
                } else {
                    Intent intentDom = new Intent(getApplicationContext(), Webolletta.class);
                    WebollettaDetail.this.finish();
                    startActivity(intentDom);
                    overridePendingTransition(R.anim.fade, R.anim.hold);
                }

            }
        });
    }


    public void postChiamata(String rispostaChiamata, List<Cookie> lista, HashMap<String, String> parametri) {
        if (animation != null) {
            animation.stop();
            imageAnim.setVisibility(View.INVISIBLE);
        }

        JSONArray rispostaJSON = new JSONArray();
        String esito = "";
        String descEsito = "";
        try {
            rispostaJSON = new JSONArray(rispostaChiamata);
            JSONObject oggettoRisposta = (JSONObject) rispostaJSON.get(0);
            esito = oggettoRisposta.getString("esito");
            descEsito = oggettoRisposta.getString("descEsito");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (esito.equals("200")) {
            Constants.SERVIZIO_INFO_FATTURA_ONLINE = rispostaChiamata;
            Intent intentEniWebBolletta = new Intent(getApplicationContext(), Webolletta.class);
            WebollettaDetail.this.finish();
            startActivity(intentEniWebBolletta);
            overridePendingTransition(R.anim.fade, R.anim.hold);
        } else {

            Intent intentOops = null;
            intentOops = new Intent(getApplicationContext(), OopsPageEniWebBollettaDetail.class);
            intentOops.putExtra("CODICE_CLIENTE", parametri.get("codiceCliente"));

            // errori ops page
            if (esito.equals("309")) {
                intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
                startActivity(intentOops);
                overridePendingTransition(R.anim.fade, R.anim.hold);

            } else if (esito.equals("400")) {
                intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
                startActivity(intentOops);
                overridePendingTransition(R.anim.fade, R.anim.hold);

            } else if (esito.equals("420")) {
                intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
                startActivity(intentOops);
                overridePendingTransition(R.anim.fade, R.anim.hold);

            } else if (esito.equals("449")) {
                intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
                startActivity(intentOops);
                overridePendingTransition(R.anim.fade, R.anim.hold);

            } else if (esito.equals("451")) {
                intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_popup_451));
                startActivity(intentOops);
                overridePendingTransition(R.anim.fade, R.anim.hold);

            }
            // errori popup
            else if (esito.equals("307")) {
                Utils.popup(WebollettaDetail.this, getResources().getString(R.string.errore_popup_307), "Riprova");

            } else if (esito.equals("435")) {
                Utils.popup(WebollettaDetail.this, getResources().getString(R.string.errore_popup_435), "Riprova");

            } else if (esito.equals("437")) {
                Utils.popup(WebollettaDetail.this, getResources().getString(R.string.errore_popup_437), "Riprova");

            } else if (esito.equals("438")) {
                Utils.popup(WebollettaDetail.this, getResources().getString(R.string.errore_popup_438), "Riprova");

            } else if (esito.equals("443")) {
                Utils.popup(WebollettaDetail.this, getResources().getString(R.string.errore_popup_443), "Riprova");

            } else if (esito.equals("444")) {
                Utils.popup(WebollettaDetail.this, getResources().getString(R.string.errore_popup_444), "Riprova");

            } else if (esito.equals("445")) {
                Utils.popup(WebollettaDetail.this, getResources().getString(R.string.errore_popup_445), "Riprova");

            } else if (esito.equals("446")) {
                Utils.popup(WebollettaDetail.this, getResources().getString(R.string.errore_popup_446), "Riprova");

            } else if (esito.equals("447")) {
                Utils.popup(WebollettaDetail.this, getResources().getString(R.string.errore_popup_447), "Riprova");

            } else {
                if (descEsito.equals("")) {
                    intentOops = new Intent(getApplicationContext(), OopsPageSessioneScaduta.class);
                    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.sessione_scaduta));
                } else {
                    intentOops.putExtra("MESSAGGIO", descEsito);
                }

                startActivity(intentOops);
                overridePendingTransition(R.anim.fade, R.anim.hold);
            }

        }

    }

    public void postChiamataException(boolean exception, String parametri, HashMap<String, String> parametriHash) {
        if (exception) {
            if (animation != null) {
                animation.stop();
                imageAnim.setVisibility(View.INVISIBLE);
            }
            Intent intentOops = null;
            intentOops = new Intent(getApplicationContext(), OopsPageEniWebBollettaDetail.class);
            intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
            intentOops.putExtra("CODICE_CLIENTE", parametriHash.get("codiceCliente"));

            // Home.this.finish();
            startActivity(intentOops);
            overridePendingTransition(R.anim.fade, R.anim.hold);
        }
    }

    public String getCodiceCliente(String rispostaLogin) {
        String result = "";
        try {
            JSONArray loginJson = new JSONArray(rispostaLogin);
            JSONObject datiCliente = (JSONObject) loginJson.get(1);
            result = datiCliente.getString("CodiceCliente");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    private boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager == null) return false;
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }

    public static Context getContext() {
        return WebollettaDetail.staticContext;
    }

    public void onBackPressed() {
        animation = Utilities.animation(WebollettaDetail.this);
        imageAnim = (ImageView) findViewById(R.id.waiting_anim);
        imageAnim.setVisibility(View.VISIBLE);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
            imageAnim.setBackgroundDrawable(animation);
        } else {
            imageAnim.setBackground(animation);
        }
        animation.start();

        Intent intentDom = new Intent(getApplicationContext(), Webolletta.class);
        WebollettaDetail.this.finish();
        startActivity(intentDom);
        overridePendingTransition(R.anim.fade, R.anim.hold);
    }
}
