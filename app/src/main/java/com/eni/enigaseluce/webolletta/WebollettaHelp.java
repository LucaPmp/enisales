package com.eni.enigaseluce.webolletta;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.AnimationDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.eni.enigaseluce.R;
import com.eni.enigaseluce.httpcall.Constants;
import com.eni.enigaseluce.utils.Utilities;

@SuppressLint("NewApi")
public class WebollettaHelp extends Activity {
    AnimationDrawable animation;
    ImageView imageAnim;
    String origine;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.webolletta_help);
	origine = getIntent().getStringExtra("ORIGINE");

	final TextView contenuto = (TextView) findViewById(R.id.contenuto_help);
	if (origine.equals(Constants.ORIGINE_WEBOLLETTA)) {
	    contenuto.setText(getResources().getString(R.string.webolletta_help_text_1));
	}
	else if (origine.equals(Constants.ORIGINE_WEBOLLETTA_DETAIL)) {
	    contenuto.setText(getResources().getString(R.string.webolletta_help_text_1));
	}
	else if (origine.equals(Constants.ORIGINE_WEBOLLETTA_ATTIVA)) {
	    contenuto.setText(getResources().getString(R.string.webolletta_help_text_1));
	}
	else if (origine.equals(Constants.ORIGINE_WEBOLLETTA_ACTIVATE)) {
	    contenuto.setText(getResources().getString(R.string.webolletta_help_text_2));
	}
	else if (origine.equals(Constants.ORIGINE_WEBOLLETTA_ATTIVAZIONE_POST_PRECONSENSO)) {
	    contenuto.setText(getResources().getString(R.string.webolletta_help_text_2));
	}
	else if (origine.equals(Constants.ORIGINE_WEBOLLETTA_DISATTIVAZIONE_POST_PRECONSENSO)) {
	    contenuto.setText(getResources().getString(R.string.webolletta_help_text_3));
	}

	final ImageView help = (ImageView) findViewById(R.id.webolletta_help_indietro);
	help.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
	help.getBackground().invalidateSelf();
	help.setOnClickListener(new View.OnClickListener() {
	    public void onClick(View v1) {
		help.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
		help.getBackground().invalidateSelf();
		animation = Utilities.animation(WebollettaHelp.this);
		imageAnim = (ImageView) findViewById(R.id.waiting_anim);
		imageAnim.setVisibility(View.VISIBLE);
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
		    imageAnim.setBackgroundDrawable(animation);
		}
		else {
		    imageAnim.setBackground(animation);
		}
		animation.start();
		Intent intentHelp = new Intent();
		if (origine.equals(Constants.ORIGINE_WEBOLLETTA)) {
		    intentHelp = new Intent(getApplicationContext(), Webolletta.class);
		}
		else if (origine.equals(Constants.ORIGINE_WEBOLLETTA_DETAIL)) {
		    intentHelp = new Intent(getApplicationContext(), WebollettaDetail.class);
		    intentHelp.putExtra("CodiceConto", getIntent().getStringExtra("CodiceConto"));
		    intentHelp.putExtra("StatoWeb", getIntent().getStringExtra("StatoWeb"));
		    intentHelp.putExtra("Chiamata", getIntent().getBooleanExtra("Chiamata", false));
		    help.getBackground().invalidateSelf();
		}
		else if (origine.equals(Constants.ORIGINE_WEBOLLETTA_ATTIVA)) {
		    intentHelp = new Intent(getApplicationContext(), WebollettaAttivazioneNotifica.class);
		    intentHelp.putExtra("CodiceConto", getIntent().getStringExtra("CodiceConto"));
		    intentHelp.putExtra("TIPOLOGIA", getIntent().getStringExtra("TIPOLOGIA"));
		    intentHelp.putExtra("CellulareNotificaSMS", getIntent().getStringExtra("CellulareNotificaSMS"));
		    intentHelp.putExtra("OperazioneWeb", getIntent().getStringExtra("OperazioneWeb"));
		}
		else if (origine.equals(Constants.ORIGINE_WEBOLLETTA_ACTIVATE)) {
		    intentHelp = new Intent(getApplicationContext(), WebollettaActivate.class);
		    intentHelp.putExtra("CodiceConto", getIntent().getStringExtra("CodiceConto"));
		    intentHelp.putExtra("TIPOLOGIA", getIntent().getStringExtra("TIPOLOGIA"));
		    intentHelp.putExtra("OperazioneWeb", getIntent().getStringExtra("OperazioneWeb"));
		    intentHelp.putExtra("CellulareNotificaSMS", getIntent().getStringExtra("CellulareNotificaSMS"));
		}
		else if (origine.equals(Constants.ORIGINE_WEBOLLETTA_ATTIVAZIONE_POST_PRECONSENSO)) {
		    intentHelp = new Intent(getApplicationContext(), WebollettaAttivazionePostPreconsenso.class);
		    intentHelp.putExtra("CodiceConto", getIntent().getStringExtra("CodiceConto"));
		    intentHelp.putExtra("TIPOLOGIA", getIntent().getStringExtra("TIPOLOGIA"));
		    intentHelp.putExtra("OperazioneWeb", getIntent().getStringExtra("OperazioneWeb"));
		    intentHelp.putExtra("CellulareNotificaSMS", getIntent().getStringExtra("CellulareNotificaSMS"));
		}
		else if (origine.equals(Constants.ORIGINE_WEBOLLETTA_DISATTIVAZIONE_POST_PRECONSENSO)) {
		    intentHelp = new Intent(getApplicationContext(), WebollettaDisattivazionePostPreconsenso.class);
		    intentHelp.putExtra("CodiceConto", getIntent().getStringExtra("CodiceConto"));
		    intentHelp.putExtra("TIPOLOGIA", getIntent().getStringExtra("TIPOLOGIA"));
		    intentHelp.putExtra("OperazioneWeb", getIntent().getStringExtra("OperazioneWeb"));
		    intentHelp.putExtra("CellulareNotificaSMS", getIntent().getStringExtra("CellulareNotificaSMS"));
		}
		WebollettaHelp.this.finish();
		startActivity(intentHelp);
		overridePendingTransition(R.anim.fade, R.anim.hold);
	    }
	});
    }

    public void onBackPressed() {
	animation = Utilities.animation(WebollettaHelp.this);
	imageAnim = (ImageView) findViewById(R.id.waiting_anim);
	imageAnim.setVisibility(View.VISIBLE);
	if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
	    imageAnim.setBackgroundDrawable(animation);
	}
	else {
	    imageAnim.setBackground(animation);
	}
	animation.start();
	Intent intentHelp = new Intent();
	if (origine.equals(Constants.ORIGINE_WEBOLLETTA)) {
	    intentHelp = new Intent(getApplicationContext(), Webolletta.class);
	}
	else if (origine.equals(Constants.ORIGINE_WEBOLLETTA_DETAIL)) {
	    intentHelp = new Intent(getApplicationContext(), WebollettaDetail.class);
	    intentHelp.putExtra("CodiceConto", getIntent().getStringExtra("CodiceConto"));
	    intentHelp.putExtra("StatoWeb", getIntent().getStringExtra("StatoWeb"));
	    intentHelp.putExtra("Chiamata", getIntent().getBooleanExtra("Chiamata", false));
	}
	else if (origine.equals(Constants.ORIGINE_WEBOLLETTA_ATTIVA)) {
	    intentHelp = new Intent(getApplicationContext(), WebollettaAttivazioneNotifica.class);
	    intentHelp.putExtra("CodiceConto", getIntent().getStringExtra("CodiceConto"));
	    intentHelp.putExtra("TIPOLOGIA", getIntent().getStringExtra("TIPOLOGIA"));
	    intentHelp.putExtra("CellulareNotificaSMS", getIntent().getStringExtra("CellulareNotificaSMS"));
	    intentHelp.putExtra("OperazioneWeb", getIntent().getStringExtra("OperazioneWeb"));
	}
	else if (origine.equals(Constants.ORIGINE_WEBOLLETTA_ACTIVATE)) {
	    intentHelp = new Intent(getApplicationContext(), WebollettaActivate.class);
	    intentHelp.putExtra("CodiceConto", getIntent().getStringExtra("CodiceConto"));
	    intentHelp.putExtra("TIPOLOGIA", getIntent().getStringExtra("TIPOLOGIA"));
	    intentHelp.putExtra("OperazioneWeb", getIntent().getStringExtra("OperazioneWeb"));
	}
	else if (origine.equals(Constants.ORIGINE_WEBOLLETTA_ATTIVAZIONE_POST_PRECONSENSO)) {
	    intentHelp = new Intent(getApplicationContext(), WebollettaAttivazionePostPreconsenso.class);
	    intentHelp.putExtra("CodiceConto", getIntent().getStringExtra("CodiceConto"));
	    intentHelp.putExtra("TIPOLOGIA", getIntent().getStringExtra("TIPOLOGIA"));
	    intentHelp.putExtra("OperazioneWeb", getIntent().getStringExtra("OperazioneWeb"));
	}
	else if (origine.equals(Constants.ORIGINE_WEBOLLETTA_DISATTIVAZIONE_POST_PRECONSENSO)) {
	    intentHelp = new Intent(getApplicationContext(), WebollettaDisattivazionePostPreconsenso.class);
	    intentHelp.putExtra("CodiceConto", getIntent().getStringExtra("CodiceConto"));
	    intentHelp.putExtra("TIPOLOGIA", getIntent().getStringExtra("TIPOLOGIA"));
	    intentHelp.putExtra("OperazioneWeb", getIntent().getStringExtra("OperazioneWeb"));
	}
	WebollettaHelp.this.finish();
	startActivity(intentHelp);
	overridePendingTransition(R.anim.fade, R.anim.hold);
    }
}
