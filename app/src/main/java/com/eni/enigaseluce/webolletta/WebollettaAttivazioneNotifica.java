package com.eni.enigaseluce.webolletta;

import java.util.HashMap;
import java.util.List;

import org.apache.http.cookie.Cookie;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.AnimationDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.eni.enigaseluce.R;
import com.eni.enigaseluce.error.OopsPageSessioneScaduta;
import com.eni.enigaseluce.error.OopsPageWebollettaAttivazioneNotifica;
import com.eni.enigaseluce.error.OopsPageWebollettaAttivazioneNotificaCellulare;
import com.eni.enigaseluce.error.Utils;
import com.eni.enigaseluce.httpcall.Constants;
import com.eni.enigaseluce.httpcall.ServiceCall;
import com.eni.enigaseluce.httpcall.ServiceCallFactory;
import com.eni.enigaseluce.utils.Log;
import com.eni.enigaseluce.utils.MyHandler;
import com.eni.enigaseluce.utils.Utilities;

@SuppressLint("NewApi")
public class WebollettaAttivazioneNotifica extends Activity {
    private Context context;
    private static Context staticContext;
    AnimationDrawable animation;
    ImageView imageAnim;
    String codiceConto;
    String tipologia;
    String notificaCellulare;
    String operazioneWeb;
    String loginService = Constants.SERVIZIO_LOGIN;
    String tipologiaNotificaCellulare = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.webolletta_attiva_notifica);
	context = this;
	WebollettaAttivazioneNotifica.staticContext = this;

	Intent intent = getIntent();
	codiceConto = intent.getStringExtra("CodiceConto");
	tipologia = intent.getStringExtra("TIPOLOGIA");
	notificaCellulare = intent.getStringExtra("CellulareNotificaSMS");
	operazioneWeb = intent.getStringExtra("OperazioneWeb");
    }

    @Override
    protected void onStart() {
	super.onStart();

	final ImageView indietro = (ImageView) findViewById(R.id.webolletta_indietro);
	indietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
	indietro.getBackground().invalidateSelf();
	indietro.setOnClickListener(new View.OnClickListener() {
	    public void onClick(View v1) {
		indietro.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
		indietro.getBackground().invalidateSelf();
		animation = Utilities.animation(WebollettaAttivazioneNotifica.this);
		imageAnim = (ImageView) findViewById(R.id.waiting_anim);
		imageAnim.setVisibility(View.VISIBLE);
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
		    imageAnim.setBackgroundDrawable(animation);
		}
		else {
		    imageAnim.setBackground(animation);
		}
		animation.start();
		Intent intentIndietro = new Intent(getApplicationContext(), Webolletta.class);
		WebollettaAttivazioneNotifica.this.finish();
		startActivity(intentIndietro);
		overridePendingTransition(R.anim.fade, R.anim.hold);
	    }
	});

	/*
	 * final ImageView domiciliazioneHelp = (ImageView)
	 * findViewById(R.id.webolletta_help);
	 * domiciliazioneHelp.getBackground().setColorFilter(0xFFFFFFFF,
	 * PorterDuff.Mode.MULTIPLY);
	 * domiciliazioneHelp.getBackground().invalidateSelf();
	 * domiciliazioneHelp.setOnClickListener(new View.OnClickListener() {
	 * public void onClick(View v1) {
	 * domiciliazioneHelp.getBackground().setColorFilter(0xFF999999,
	 * PorterDuff.Mode.MULTIPLY);
	 * domiciliazioneHelp.getBackground().invalidateSelf(); animation = new
	 * AnimationDrawable();
	 * animation.addFrame(getResources().getDrawable(R.drawable
	 * .anim_load2),300);
	 * animation.addFrame(getResources().getDrawable(R.drawable.anim_load3),
	 * 300);
	 * animation.addFrame(getResources().getDrawable(R.drawable.anim_load4
	 * ),300); animation.setOneShot(false); imageAnim = (ImageView)
	 * findViewById(R.id.waiting_webolletta_attivazione_notifica);
	 * imageAnim.setVisibility(View.VISIBLE); if (Build.VERSION.SDK_INT <
	 * 16) { imageAnim.setBackgroundDrawable(animation); } else {
	 * imageAnim.setBackground(animation); } // start the animation!
	 * animation.start(); Intent intentHelp = new
	 * Intent(getApplicationContext(), WebollettaHelp.class);
	 * intentHelp.putExtra("ORIGINE", Constants.ORIGINE_WEBOLLETTA_ATTIVA);
	 * intentHelp.putExtra("CodiceConto", codiceConto);
	 * intentHelp.putExtra("TIPOLOGIA", tipologia);
	 * intentHelp.putExtra("CellulareNotificaSMS", notificaCellulare);
	 * intentHelp.putExtra("OperazioneWeb", operazioneWeb);
	 * WebollettaAttivazioneNotifica.this.finish();
	 * startActivity(intentHelp); overridePendingTransition(R.anim.fade,
	 * R.anim.hold); } });
	 */
	popolaPagina();

    }

    private void popolaPagina() {
	final RelativeLayout notifica_attiva = (RelativeLayout) findViewById(R.id.notifica_attiva);
	final RelativeLayout notifica_non_attivo = (RelativeLayout) findViewById(R.id.notifica_non_attivo);
	final RelativeLayout notifia_attiva_modifica = (RelativeLayout) findViewById(R.id.notifica_attiva_modifica);

	final ImageView disattivaWebolletta = (ImageView) findViewById(R.id.disattiva_webolletta_button);
	disattivaWebolletta.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
	disattivaWebolletta.getBackground().invalidateSelf();
	disattivaWebolletta.setOnClickListener(new View.OnClickListener() {
	    public void onClick(View v1) {
		disattivaWebolletta.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
		disattivaWebolletta.getBackground().invalidateSelf();
		if (operazioneWeb.equals("DISATTIVA")) {
		    // chiamata preconsenso e vado alla
		    // webolletta_attiva_post_btn
		    animation = Utilities.animation(WebollettaAttivazioneNotifica.this);
		    imageAnim = (ImageView) findViewById(R.id.waiting_anim);
		    imageAnim.setVisibility(View.VISIBLE);
		    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
			imageAnim.setBackgroundDrawable(animation);
		    }
		    else {
			imageAnim.setBackground(animation);
		    }
		    animation.start();
		    String codiceCliente = getCodiceCliente(loginService);
		    final String parametri = "&codiceCliente=" + codiceCliente + "&codiceContoCliente=" + codiceConto + "&tipologiaRichiesta=DISATTIVA";

		    if (!isNetworkAvailable(context)) {
			Intent intentOops = new Intent(getApplicationContext(), OopsPageWebollettaAttivazioneNotifica.class);
			intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.connessione_assente));
			intentOops.putExtra("PARAMETER", parametri);
			startActivity(intentOops);
			overridePendingTransition(R.anim.fade, R.anim.hold);
		    }
		    else {

			MyHandler handler = new MyHandler() {

			    @Override
			    public void handleMessage(Message msg) {

				if (msg.what == Constants.MAINTENANCE_DISATTIVA) {

					Log.d("WEBOLLETTAATTIVANOTIFICA","URL_RICHIESTA_PRECONSENSO - MAINTENANCE_DISATTIVA");

				    /**************** Chiamata richiesta preconsenso ***********************/
				    final ServiceCall visFornDomicilServiceCall = ServiceCallFactory.createServiceCall(context);

				    try {
					visFornDomicilServiceCall.executeHttpsGetWebollettaAttivazioneNotifica(Constants.URL_RICHIESTA_PRECONSENSO, parametri, WebollettaAttivazioneNotifica.this, null, false, false);
				    }
				    catch (Exception e) {
					Intent intentOops = new Intent(getApplicationContext(), OopsPageWebollettaAttivazioneNotifica.class);
					intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
					intentOops.putExtra("PARAMETER", parametri);
					startActivity(intentOops);
					overridePendingTransition(R.anim.fade, R.anim.hold);
				    }
				}
			    }
			};

			// maintencance mode
			if (!Constants.STUB_ENABLED) {
			    Utilities.maintenance(WebollettaAttivazioneNotifica.this, null, handler);
			}

			// /****************
			// * Chiamata richiesta preconsenso
			// ***********************/
			// final ServiceCall visFornDomicilServiceCall =
			// ServiceCallFactory.createServiceCall(context);
			//
			// try {
			// visFornDomicilServiceCall.executeHttpsGetWebollettaAttivazioneNotifica(Constants.URL_RICHIESTA_PRECONSENSO,
			// parametri, WebollettaAttivazioneNotifica.this, null,
			// false, false);
			// }
			// catch (Exception e) {
			// Intent intentOops = new
			// Intent(getApplicationContext(),
			// OopsPageWebollettaAttivazioneNotifica.class);
			// intentOops.putExtra("MESSAGGIO",
			// getResources().getString(R.string.servizio_non_disponibile));
			// intentOops.putExtra("PARAMETER", parametri);
			// startActivity(intentOops);
			// overridePendingTransition(R.anim.fade, R.anim.hold);
			// }

			// try {
			// visFornDomicilServiceCall.executeHttpsGetWebollettaAttivazioneNotifica(Constants.URL_RICHIESTA_PRECONSENSO,
			// parametri, WebollettaAttivazioneNotifica.this, null,
			// false, false);
			// }
			// catch (Exception e) {
			// Intent intentOops = new
			// Intent(getApplicationContext(),
			// OopsPageWebollettaAttivazioneNotifica.class);
			// intentOops.putExtra("MESSAGGIO",
			// getResources().getString(R.string.servizio_non_disponibile));
			// intentOops.putExtra("PARAMETER", parametri);
			// startActivity(intentOops);
			// overridePendingTransition(R.anim.fade, R.anim.hold);
			// }

			/************************************************************************/
		    }
		}
		else if (operazioneWeb.equals("NESSUNA OPERAZIONE")) {
		    AlertDialog.Builder builder = new AlertDialog.Builder(context);
		    builder.setTitle("Attenzione").setMessage(R.string.operazione_non_consentita).setCancelable(false).setPositiveButton("Chiudi", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
			    if (animation != null) {
				animation.stop();
				imageAnim.setVisibility(View.INVISIBLE);
			    }
			}
		    });
		    builder.create().show();
		}
		else {
		    AlertDialog.Builder builder = new AlertDialog.Builder(context);
		    builder.setTitle("Attenzione").setMessage(R.string.operazione_non_consentita_call).setCancelable(false).setPositiveButton("Chiudi", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
			    if (animation != null) {
				animation.stop();
				imageAnim.setVisibility(View.INVISIBLE);
			    }
			}
		    });
		    builder.create().show();
		}

	    }
	});

	final ImageView modifica = (ImageView) findViewById(R.id.modifica);
	modifica.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
	modifica.getBackground().invalidateSelf();
	modifica.setOnClickListener(new View.OnClickListener() {
	    public void onClick(View v) {
		modifica.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
		modifica.getBackground().invalidateSelf();
		notifica_attiva.setVisibility(View.GONE);
		notifia_attiva_modifica.setVisibility(View.VISIBLE);
		notifica_non_attivo.setVisibility(View.GONE);

		final EditText cellulare_modifica = (EditText) findViewById(R.id.cellulare_modifica);
		cellulare_modifica.setText(notificaCellulare);
		final ImageView applica = (ImageView) findViewById(R.id.applica);

		final ImageView annulla = (ImageView) findViewById(R.id.annulla);
		annulla.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
		annulla.getBackground().invalidateSelf();
		annulla.setOnClickListener(new View.OnClickListener() {
		    public void onClick(View v) {
			annulla.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
			annulla.getBackground().invalidateSelf();
			notifica_attiva.setVisibility(View.VISIBLE);
			notifia_attiva_modifica.setVisibility(View.GONE);
			notifica_non_attivo.setVisibility(View.GONE);

			TextView numero_cel = (TextView) findViewById(R.id.numero_cel);
			numero_cel.setText(notificaCellulare);

			modifica.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
			modifica.getBackground().invalidateSelf();
		    }
		});

		TextWatcher textWatcherNome = new TextWatcher() {
		    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

		    }

		    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

		    }

		    public void afterTextChanged(Editable editable) {
			if (!cellulare_modifica.getText().toString().trim().equals("") && !cellulare_modifica.getText().toString().trim().equals(notificaCellulare)) {
			    applica.setBackgroundResource(R.drawable.btn_applica_modifica);
			    applica.setClickable(true);
			    applica.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
				    applica.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
				    applica.getBackground().invalidateSelf();
				    AlertDialog.Builder builder = new AlertDialog.Builder(context);
				    builder.setTitle("Notifica SMS").setMessage(R.string.notifica_sms_popup).setCancelable(true).setNegativeButton("Annulla", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {

					}
				    }).setPositiveButton("Conferma", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
					    // chiamata
					    String cellulareInserito = cellulare_modifica.getText().toString();
					    tipologiaNotificaCellulare = "MODIFICA";
					    chiamataNotificaCellulare("MODIFICA", cellulareInserito);
					}
				    });
				    builder.create().show();
				    applica.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
				    applica.getBackground().invalidateSelf();
				}
			    });
			}
			else {
			    applica.setBackgroundResource(R.drawable.btn_applica_modifica_disabled);
			    applica.setClickable(false);
			}
		    }
		};
		cellulare_modifica.addTextChangedListener(textWatcherNome);
	    }
	});

	TextView numeroCliente = (TextView) findViewById(R.id.text_numero_cliente_attiva_domiciliazione);
	numeroCliente.setText(codiceConto);

	ImageView iconaLuce = (ImageView) findViewById(R.id.domiciliazione_luce_icona);
	ImageView iconaGas = (ImageView) findViewById(R.id.domiciliazione_gas_icona);
	if (tipologia.equals("DUAL")) {
	    iconaLuce.setBackgroundResource(R.drawable.light_yes);
	    iconaGas.setBackgroundResource(R.drawable.gas_yes);
	}
	else if (tipologia.equals("LUCE")) {
	    iconaLuce.setBackgroundResource(R.drawable.light_yes);
	    iconaGas.setBackgroundResource(R.drawable.gas_no);
	}
	else {
	    iconaLuce.setBackgroundResource(R.drawable.light_no);
	    iconaGas.setBackgroundResource(R.drawable.gas_yes);
	}

	TextView stato = (TextView) findViewById(R.id.stato);

	if (notificaCellulare.equals("")) {
	    stato.setText("Non attivo");
	    stato.setTextColor(getResources().getColor(R.color.red1));
	    notifica_attiva.setVisibility(View.GONE);
	    notifia_attiva_modifica.setVisibility(View.GONE);
	    notifica_non_attivo.setVisibility(View.VISIBLE);
	    final ImageView attiva_notifica = (ImageView) findViewById(R.id.attiva_notifica);

	    final EditText cellulare = (EditText) findViewById(R.id.cellulare);
	    TextWatcher textWatcherNome = new TextWatcher() {
		public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

		}

		public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

		}

		public void afterTextChanged(Editable editable) {
		    if (!cellulare.getText().toString().trim().equals("")) {
			attiva_notifica.setBackgroundResource(R.drawable.attiva_notifica);
			attiva_notifica.setClickable(true);
			attiva_notifica.setOnClickListener(new View.OnClickListener() {
			    public void onClick(View v) {
				final String cellulareInserito = cellulare.getText().toString();
				AlertDialog.Builder builder = new AlertDialog.Builder(context);
				builder.setTitle("Attiva Notifica").setMessage(R.string.webolletta_attivazione_notifica_popup).setCancelable(true).setNegativeButton("Annulla", new DialogInterface.OnClickListener() {
				    public void onClick(DialogInterface dialog, int id) {
				    }
				}).setPositiveButton("Conferma", new DialogInterface.OnClickListener() {
				    public void onClick(DialogInterface dialog, int id) {
					tipologiaNotificaCellulare = "INSERISCI";
					chiamataNotificaCellulare("INSERISCI", cellulareInserito);
				    }
				});
				builder.create().show();
			    }
			});
		    }
		    else {
			attiva_notifica.setBackgroundResource(R.drawable.attiva_notifica_disabled);
			attiva_notifica.setClickable(false);
		    }
		}
	    };
	    cellulare.addTextChangedListener(textWatcherNome);
	}
	else {
	    stato.setText("Attivo");
	    stato.setTextColor(getResources().getColor(R.color.green1));
	    notifica_attiva.setVisibility(View.VISIBLE);
	    notifia_attiva_modifica.setVisibility(View.GONE);
	    notifica_non_attivo.setVisibility(View.GONE);
	    TextView numero_cel = (TextView) findViewById(R.id.numero_cel);
	    numero_cel.setText(notificaCellulare);
	    ImageView disattiva_notifica = (ImageView) findViewById(R.id.disattiva_notifica);
	    disattiva_notifica.setOnClickListener(new View.OnClickListener() {
		public void onClick(View v) {
		    AlertDialog.Builder builder = new AlertDialog.Builder(context);
		    builder.setTitle("Disattiva Notifica").setMessage(R.string.webolletta_disattivazione_notifica_popup).setCancelable(true).setNegativeButton("Annulla", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {

			}
		    }).setPositiveButton("Conferma", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
			    tipologiaNotificaCellulare = "CANCELLA";
			    chiamataNotificaCellulare("CANCELLA", notificaCellulare);
			}
		    });
		    builder.create().show();
		}
	    });
	}
    }

    public void postChiamata(String rispostaChiamata, List<Cookie> lista, HashMap<String, String> parametri, boolean notifica, boolean aggiorna) {
	if (animation != null) {
	    animation.stop();
	    imageAnim.setVisibility(View.INVISIBLE);
	}
	JSONArray rispostaJSON = new JSONArray();
	String esito = "";
	String descEsito = "";
	try {
	    rispostaJSON = new JSONArray(rispostaChiamata);
	    JSONObject oggettoRisposta = (JSONObject) rispostaJSON.get(0);
	    esito = oggettoRisposta.getString("esito");
	    descEsito = oggettoRisposta.getString("descEsito");
	}
	catch (JSONException e) {
	    e.printStackTrace();
	}
	if (esito.equals("200")) {
	    if (!notifica) {

		if (!aggiorna) {
		    Constants.SERVIZIO_RICHIESTA_PRECONSENSO_FATTURA_ONLINE = rispostaChiamata;
		    Intent intentVisualizzaFornitureDomiciliazione = new Intent(getApplicationContext(), WebollettaDisattivazionePostPreconsenso.class);
		    intentVisualizzaFornitureDomiciliazione.putExtra("CodiceConto", codiceConto);
		    intentVisualizzaFornitureDomiciliazione.putExtra("TIPOLOGIA", tipologia);
		    intentVisualizzaFornitureDomiciliazione.putExtra("OperazioneWeb", operazioneWeb);
		    intentVisualizzaFornitureDomiciliazione.putExtra("CellulareNotificaSMS", notificaCellulare);

		    WebollettaAttivazioneNotifica.this.finish();
		    startActivity(intentVisualizzaFornitureDomiciliazione);
		    overridePendingTransition(R.anim.fade, R.anim.hold);
		}
		else {
		    Constants.SERVIZIO_INFO_FATTURA_ONLINE = rispostaChiamata;
		    if (tipologiaNotificaCellulare.equals("MODIFICA")) {
			String cellulareNotificaSMS = "";
			try {
			    cellulareNotificaSMS = trovaCellulareNotificaSMS(new JSONArray(Constants.SERVIZIO_INFO_FATTURA_ONLINE));
			}
			catch (JSONException e) {
			    e.printStackTrace();
			}
			notificaCellulare = cellulareNotificaSMS;

			Intent intentVisualizzaFornitureDomiciliazione = getIntent();
			intentVisualizzaFornitureDomiciliazione.putExtra("CodiceConto", codiceConto);
			intentVisualizzaFornitureDomiciliazione.putExtra("TIPOLOGIA", tipologia);
			intentVisualizzaFornitureDomiciliazione.putExtra("OperazioneWeb", operazioneWeb);
			intentVisualizzaFornitureDomiciliazione.putExtra("CellulareNotificaSMS", notificaCellulare);

			WebollettaAttivazioneNotifica.this.finish();
			startActivity(intentVisualizzaFornitureDomiciliazione);
			overridePendingTransition(R.anim.fade, R.anim.hold);
		    }
		    else {
			Intent intentWebolletta = new Intent(getApplicationContext(), Webolletta.class);
			WebollettaAttivazioneNotifica.this.finish();
			startActivity(intentWebolletta);
			overridePendingTransition(R.anim.fade, R.anim.hold);
		    }

		}

	    }
	    else {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		if (tipologiaNotificaCellulare.equals("INSERISCI")) {
		    builder.setTitle("Attiva Notifica").setMessage("Richiesta di attivazione inviata con successo").setCancelable(false).setPositiveButton("Chiudi", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
			    if (animation != null) {
				animation.stop();
				imageAnim.setVisibility(View.INVISIBLE);
				String codiceCliente = getCodiceCliente(loginService);
				animation = Utilities.animation(WebollettaAttivazioneNotifica.this);
				imageAnim = (ImageView) findViewById(R.id.waiting_anim);
				imageAnim.setVisibility(View.VISIBLE);
				if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
				    imageAnim.setBackgroundDrawable(animation);
				}
				else {
				    imageAnim.setBackground(animation);
				}
				animation.start();
				if (!isNetworkAvailable(context)) {
				    Intent intentOops = new Intent(getApplicationContext(), OopsPageWebollettaAttivazioneNotifica.class);
				    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.connessione_assente));
				    intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
				    startActivity(intentOops);
				    if (animation != null) {
					animation.stop();
					imageAnim.setVisibility(View.INVISIBLE);
				    }
				    overridePendingTransition(R.anim.fade, R.anim.hold);
				}
				else {
				    /***************************** chiamata visualizza info fattura online *******************************/
				    ServiceCall eniWebBollettaServiceCall = ServiceCallFactory.createServiceCall(context);
				    // eniWebBollettaServiceCall.myCookieStore =
				    // new PersistentCookieStore(context);

				    String parametriWebolletta = "&codiceCliente=" + codiceCliente;

				    try {
					eniWebBollettaServiceCall.executeHttpsGetWebollettaAttivazioneNotifica(Constants.URL_INFO_FATTURA_ONLINE, parametriWebolletta, WebollettaAttivazioneNotifica.this, null, false, true);
				    }
				    catch (Exception e) {
					Intent intentOops = new Intent(getApplicationContext(), OopsPageWebollettaAttivazioneNotifica.class);
					intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
					intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
					startActivity(intentOops);
					overridePendingTransition(R.anim.fade, R.anim.hold);
				    }

				    /*******************************************************************************/
				}
			    }
			}
		    });
		    builder.create().show();
		}
		else if (tipologiaNotificaCellulare.equals("CANCELLA")) {
		    builder.setTitle("Disattiva Notifica").setMessage("Richiesta di disattivazione inviata con successo").setCancelable(false).setPositiveButton("Chiudi", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
			    if (animation != null) {
				animation.stop();
				imageAnim.setVisibility(View.INVISIBLE);
			    }
			    String codiceCliente = getCodiceCliente(loginService);
			    animation = Utilities.animation(WebollettaAttivazioneNotifica.this);
			    imageAnim = (ImageView) findViewById(R.id.waiting_anim);
			    imageAnim.setVisibility(View.VISIBLE);
			    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
				imageAnim.setBackgroundDrawable(animation);
			    }
			    else {
				imageAnim.setBackground(animation);
			    }
			    animation.start();
			    if (!isNetworkAvailable(context)) {
				Intent intentOops = new Intent(getApplicationContext(), OopsPageWebollettaAttivazioneNotifica.class);
				intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.connessione_assente));
				intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
				startActivity(intentOops);
				if (animation != null) {
				    animation.stop();
				    imageAnim.setVisibility(View.INVISIBLE);
				}
				overridePendingTransition(R.anim.fade, R.anim.hold);
			    }
			    else {
				/***************************** chiamata visualizza info fattura online *******************************/
				ServiceCall eniWebBollettaServiceCall = ServiceCallFactory.createServiceCall(context);
				// eniWebBollettaServiceCall.myCookieStore = new
				// PersistentCookieStore(context);

				String parametriWebolletta = "&codiceCliente=" + codiceCliente;

				try {
				    eniWebBollettaServiceCall.executeHttpsGetWebollettaAttivazioneNotifica(Constants.URL_INFO_FATTURA_ONLINE, parametriWebolletta, WebollettaAttivazioneNotifica.this, null, false, true);
				}
				catch (Exception e) {
				    Intent intentOops = new Intent(getApplicationContext(), OopsPageWebollettaAttivazioneNotifica.class);
				    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
				    intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
				    startActivity(intentOops);
				    overridePendingTransition(R.anim.fade, R.anim.hold);
				}

				/*******************************************************************************/
			    }
			}
		    });
		    builder.create().show();
		}
		else if (tipologiaNotificaCellulare.equals("MODIFICA")) {
		    // chiamata webolletta e ricaricare
		    // WebollettaAttivazioneNotifica con i dati aggiornati
		    String codiceCliente = getCodiceCliente(loginService);
		    animation = Utilities.animation(WebollettaAttivazioneNotifica.this);
		    imageAnim = (ImageView) findViewById(R.id.waiting_anim);
		    imageAnim.setVisibility(View.VISIBLE);
		    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
			imageAnim.setBackgroundDrawable(animation);
		    }
		    else {
			imageAnim.setBackground(animation);
		    }
		    animation.start();
		    if (!isNetworkAvailable(context)) {
			Intent intentOops = new Intent(getApplicationContext(), OopsPageWebollettaAttivazioneNotifica.class);
			intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.connessione_assente));
			intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
			startActivity(intentOops);
			if (animation != null) {
			    animation.stop();
			    imageAnim.setVisibility(View.INVISIBLE);
			}
			overridePendingTransition(R.anim.fade, R.anim.hold);
		    }
		    else {
			/***************************** chiamata visualizza info fattura online *******************************/
			ServiceCall eniWebBollettaServiceCall = ServiceCallFactory.createServiceCall(context);
			// eniWebBollettaServiceCall.myCookieStore = new
			// PersistentCookieStore(context);

			String parametriWebolletta = "&codiceCliente=" + codiceCliente;

			try {
			    eniWebBollettaServiceCall.executeHttpsGetWebollettaAttivazioneNotifica(Constants.URL_INFO_FATTURA_ONLINE, parametriWebolletta, WebollettaAttivazioneNotifica.this, null, false, true);
			}
			catch (Exception e) {
			    Intent intentOops = new Intent(getApplicationContext(), OopsPageWebollettaAttivazioneNotifica.class);
			    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
			    intentOops.putExtra("CODICE_CLIENTE", codiceCliente);
			    startActivity(intentOops);
			    overridePendingTransition(R.anim.fade, R.anim.hold);
			}

			/*******************************************************************************/
		    }
		}

	    }

	}
	else {
	    Intent intentOops = new Intent();
	    if (!notifica) {
		intentOops = new Intent(getApplicationContext(), OopsPageWebollettaAttivazioneNotifica.class);
		intentOops.putExtra("PARAMETER", parametri.get("PARAMETER"));

	    }
	    else {
		if (tipologiaNotificaCellulare.equals("CANCELLA")) {
		    intentOops = new Intent(getApplicationContext(), OopsPageWebollettaAttivazioneNotificaCellulare.class);
		    intentOops.putExtra("PARAMETER", parametri.get("PARAMETER"));
		}
		else {
		    if (esito.equals("314") || esito.equals("317")) {
			AlertDialog.Builder builder = new AlertDialog.Builder(context);
			builder.setTitle("Attenzione!").setMessage("Verificare il numero di cellulare inserito").setCancelable(false).setPositiveButton("Chiudi", new DialogInterface.OnClickListener() {
			    public void onClick(DialogInterface dialog, int id) {
				if (animation != null) {
				    animation.stop();
				    imageAnim.setVisibility(View.INVISIBLE);
				}
				final ImageView applica = (ImageView) findViewById(R.id.applica);
				applica.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
				applica.getBackground().invalidateSelf();
			    }
			});
			builder.create().show();
			return;
		    }
		    else {
			intentOops = new Intent(getApplicationContext(), OopsPageWebollettaAttivazioneNotificaCellulare.class);
			intentOops.putExtra("PARAMETER", parametri.get("PARAMETER"));
		    }
		}

	    }

	    // errori ops page
	    if (esito.equals("309")) {
		intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
		startActivity(intentOops);
		overridePendingTransition(R.anim.fade, R.anim.hold);

	    }
	    else if (esito.equals("400")) {
		intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
		startActivity(intentOops);
		overridePendingTransition(R.anim.fade, R.anim.hold);

	    }
	    else if (esito.equals("420")) {
		intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
		startActivity(intentOops);
		overridePendingTransition(R.anim.fade, R.anim.hold);

	    }
	    else if (esito.equals("449")) {
		intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_generico_opspage));
		startActivity(intentOops);
		overridePendingTransition(R.anim.fade, R.anim.hold);

	    }
	    else if (esito.equals("451")) {
		intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.errore_popup_451));
		startActivity(intentOops);
		overridePendingTransition(R.anim.fade, R.anim.hold);

	    }
	    // errori popup
	    else if (esito.equals("307")) {
		Utils.popup(WebollettaAttivazioneNotifica.this, getResources().getString(R.string.errore_popup_307), "Riprova");

	    }
	    else if (esito.equals("435")) {
		Utils.popup(WebollettaAttivazioneNotifica.this, getResources().getString(R.string.errore_popup_435), "Riprova");

	    }
	    else if (esito.equals("437")) {
		Utils.popup(WebollettaAttivazioneNotifica.this, getResources().getString(R.string.errore_popup_437), "Riprova");

	    }
	    else if (esito.equals("438")) {
		Utils.popup(WebollettaAttivazioneNotifica.this, getResources().getString(R.string.errore_popup_438), "Riprova");

	    }
	    else if (esito.equals("443")) {
		Utils.popup(WebollettaAttivazioneNotifica.this, getResources().getString(R.string.errore_popup_443), "Riprova");

	    }
	    else if (esito.equals("444")) {
		Utils.popup(WebollettaAttivazioneNotifica.this, getResources().getString(R.string.errore_popup_444), "Riprova");

	    }
	    else if (esito.equals("445")) {
		Utils.popup(WebollettaAttivazioneNotifica.this, getResources().getString(R.string.errore_popup_445), "Riprova");

	    }
	    else if (esito.equals("446")) {
		Utils.popup(WebollettaAttivazioneNotifica.this, getResources().getString(R.string.errore_popup_446), "Riprova");

	    }
	    else if (esito.equals("447")) {
		Utils.popup(WebollettaAttivazioneNotifica.this, getResources().getString(R.string.errore_popup_447), "Riprova");

	    }
	    else {
	    	if (descEsito.equals("")) {
				intentOops = new Intent(getApplicationContext(), OopsPageSessioneScaduta.class);
				intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.sessione_scaduta));
			} else{
				intentOops.putExtra("MESSAGGIO", descEsito);
			}
			
			startActivity(intentOops);
			overridePendingTransition(R.anim.fade, R.anim.hold);
	    }

	}

    }

    public void postChiamataException(boolean exception, String parametri, boolean notifica, boolean aggiorna) {
	if (exception) {
	    if (animation != null) {
		animation.stop();
		imageAnim.setVisibility(View.INVISIBLE);
	    }
	    Intent intentOops = new Intent();
	    if (!notifica) {
		intentOops = new Intent(getApplicationContext(), OopsPageWebollettaAttivazioneNotifica.class);
		intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
		intentOops.putExtra("PARAMETER", parametri);

	    }
	    else {
		intentOops = new Intent(getApplicationContext(), OopsPageWebollettaAttivazioneNotificaCellulare.class);
		intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
		intentOops.putExtra("PARAMETER", parametri);
	    }
	    startActivity(intentOops);
	    overridePendingTransition(R.anim.fade, R.anim.hold);

	}
    }

    public static Context getContext() {
	return WebollettaAttivazioneNotifica.staticContext;
    }

    public String getCodiceCliente(String rispostaLogin) {
	String result = "";
	try {
	    JSONArray loginJson = new JSONArray(rispostaLogin);
	    JSONObject datiCliente = (JSONObject) loginJson.get(1);
	    result = datiCliente.getString("CodiceCliente");
	}
	catch (JSONException e) {
	    e.printStackTrace();
	}
	return result;
    }

    private boolean isNetworkAvailable(Context context) {
	ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
	if (connectivityManager == null) return false;
	NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
	return activeNetworkInfo != null;
    }

    public void chiamataNotificaCellulare(String tipologia, String cellulare) {
	animation = Utilities.animation(WebollettaAttivazioneNotifica.this);
	imageAnim = (ImageView) findViewById(R.id.waiting_anim);
	imageAnim.setVisibility(View.VISIBLE);
	if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
	    imageAnim.setBackgroundDrawable(animation);
	}
	else {
	    imageAnim.setBackground(animation);
	}
	animation.start();

	String codiceCliente = getCodiceCliente(loginService);
	final String parametri = "&codiceCliente=" + codiceCliente + "&cellulareCliente=" + cellulare + "&tipologiaRichiesta=" + tipologia;

	if (!isNetworkAvailable(context)) {
	    Intent intentOops = new Intent(getApplicationContext(), OopsPageWebollettaAttivazioneNotificaCellulare.class);
	    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.connessione_assente));
	    intentOops.putExtra("PARAMETER", parametri);
	    startActivity(intentOops);
	    overridePendingTransition(R.anim.fade, R.anim.hold);
	}
	else {

	    MyHandler handler = new MyHandler() {

		@Override
		public void handleMessage(Message msg) {

		    if (msg.what == Constants.MAINTENANCE_DISATTIVA) {

		    	Log.d("WEBOLLETTAATTIVANOTIFICA","URL_GESTIONE_NOTIFICA_CELLULARE - MAINTENANCE_DISATTIVA");

			/****************
			 * Chiamata notifica cellulare
			 ***********************/
			final ServiceCall visFornDomicilServiceCall = ServiceCallFactory.createServiceCall(context);

			try {
			    visFornDomicilServiceCall.executeHttpsGetWebollettaAttivazioneNotifica(Constants.URL_GESTIONE_NOTIFICA_CELLULARE, parametri, WebollettaAttivazioneNotifica.this, null, true, false);
			}
			catch (Exception e) {
			    Intent intentOops = new Intent(getApplicationContext(), OopsPageWebollettaAttivazioneNotificaCellulare.class);
			    intentOops.putExtra("MESSAGGIO", getResources().getString(R.string.servizio_non_disponibile));
			    intentOops.putExtra("PARAMETER", parametri);
			    startActivity(intentOops);
			    overridePendingTransition(R.anim.fade, R.anim.hold);
			}
		    }
		}
	    };

	    // maintencance mode
	    if (!Constants.STUB_ENABLED) {
		Utilities.maintenance(WebollettaAttivazioneNotifica.this, null, handler);
	    }

	    // /****************
	    // * Chiamata notifica cellulare
	    // ***********************/
	    // final ServiceCall visFornDomicilServiceCall =
	    // ServiceCallFactory.createServiceCall(context);
	    //
	    // try {
	    // visFornDomicilServiceCall.executeHttpsGetWebollettaAttivazioneNotifica(Constants.URL_GESTIONE_NOTIFICA_CELLULARE,
	    // parametri, WebollettaAttivazioneNotifica.this, null, true,
	    // false);
	    // }
	    // catch (Exception e) {
	    // Intent intentOops = new Intent(getApplicationContext(),
	    // OopsPageWebollettaAttivazioneNotificaCellulare.class);
	    // intentOops.putExtra("MESSAGGIO",
	    // getResources().getString(R.string.servizio_non_disponibile));
	    // intentOops.putExtra("PARAMETER", parametri);
	    // startActivity(intentOops);
	    // overridePendingTransition(R.anim.fade, R.anim.hold);
	    // }

	    // try {
	    // visFornDomicilServiceCall.executeHttpsGetWebollettaAttivazioneNotifica(Constants.URL_GESTIONE_NOTIFICA_CELLULARE,
	    // parametri, WebollettaAttivazioneNotifica.this, null, true,
	    // false);
	    // }
	    // catch (Exception e) {
	    // Intent intentOops = new Intent(getApplicationContext(),
	    // OopsPageWebollettaAttivazioneNotificaCellulare.class);
	    // intentOops.putExtra("MESSAGGIO",
	    // getResources().getString(R.string.servizio_non_disponibile));
	    // intentOops.putExtra("PARAMETER", parametri);
	    // startActivity(intentOops);
	    // overridePendingTransition(R.anim.fade, R.anim.hold);
	    // }

	    /************************************************************************/
	}
    }

    public String trovaCellulareNotificaSMS(JSONArray array) {
	String result = "";
	try {
	    JSONObject cellulareOggetto = array.getJSONObject(array.length() - 1);
	    result = cellulareOggetto.getString("CellulareNotificaSMS");
	}
	catch (JSONException e) {
	    e.printStackTrace();
	    result = "";
	}
	return result;
    }

    public void onBackPressed() {
	animation = Utilities.animation(WebollettaAttivazioneNotifica.this);
	imageAnim = (ImageView) findViewById(R.id.waiting_anim);
	imageAnim.setVisibility(View.VISIBLE);
	if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
	    imageAnim.setBackgroundDrawable(animation);
	}
	else {
	    imageAnim.setBackground(animation);
	}
	animation.start();
	Intent intentIndietro = new Intent(getApplicationContext(), Webolletta.class);
	WebollettaAttivazioneNotifica.this.finish();
	startActivity(intentIndietro);
	overridePendingTransition(R.anim.fade, R.anim.hold);
    }

}
