package com.eni.enigaseluce.webolletta;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.AnimationDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.eni.enigaseluce.EniApplication;
import com.eni.enigaseluce.R;
import com.eni.enigaseluce.home.Home;
import com.eni.enigaseluce.httpcall.Constants;
import com.eni.enigaseluce.pager.HorizontalPager;
import com.eni.enigaseluce.utils.Utilities;

@SuppressLint("NewApi")
public class Webolletta extends Activity {
	AnimationDrawable animation;
	ImageView imageAnim;
	/** Called when the activity is first created. */

	public String rispostaLogin = Constants.SERVIZIO_LOGIN;
	public String rispostaWebolletta = Constants.SERVIZIO_INFO_FATTURA_ONLINE;
	String codiceEsito;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.webolletta);

		((EniApplication)getApplication()).sendAnalyticsEvent("[Android] - bolletta digitale");

		final ImageView webollettaIndietro = (ImageView) findViewById(R.id.webolletta_indietro);
		webollettaIndietro.getBackground().setAlpha(255);
		webollettaIndietro.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
		webollettaIndietro.getBackground().invalidateSelf();
		webollettaIndietro.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v1)
			{
				webollettaIndietro.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
				webollettaIndietro.getBackground().invalidateSelf();
				animation = Utilities.animation(Webolletta.this);
				imageAnim = (ImageView) findViewById(R.id.waiting_anim);
				imageAnim.setVisibility(View.VISIBLE);
				if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN)
				{
					imageAnim.setBackgroundDrawable(animation);
				} else
				{
					imageAnim.setBackground(animation);
				}
				animation.start();
				Intent intentHome = new Intent(getApplicationContext(), Home.class);
				Webolletta.this.finish();
				startActivity(intentHome);
				overridePendingTransition(R.anim.fade, R.anim.hold);
			}
		});

//		final ImageView webollettaHelp = (ImageView) findViewById(R.id.webolletta_help);
//		webollettaHelp.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
//		webollettaHelp.getBackground().invalidateSelf();
//		webollettaHelp.setOnClickListener(new View.OnClickListener() {
//			public void onClick(View v1)
//			{
//				webollettaHelp.getBackground().setColorFilter(0xFF999999, PorterDuff.Mode.MULTIPLY);
//				webollettaHelp.getBackground().invalidateSelf();
//				animation = Utilities.animation(Webolletta.this);
//				imageAnim = (ImageView) findViewById(R.id.waiting_anim);
//				imageAnim.setVisibility(View.VISIBLE);
//				if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN)
//				{
//					imageAnim.setBackgroundDrawable(animation);
//				} else
//				{
//					imageAnim.setBackground(animation);
//				}
//				animation.start();
//
//				Intent intentHome = new Intent(getApplicationContext(), WebollettaHelp.class);
//				intentHome.putExtra("ORIGINE", Constants.ORIGINE_WEBOLLETTA);
//				Webolletta.this.finish();
//				startActivity(intentHome);
//				overridePendingTransition(R.anim.fade, R.anim.hold);
//			}
//		});
		final HorizontalPager webolletta_pager = (HorizontalPager) findViewById(R.id.webolletta_pager);
		webolletta_pager.removeAllViews();
		// RelativeLayout domiciliazione_page =
		// (RelativeLayout)findViewById(R.id.domiciliazione_page);
		LayoutInflater inflater = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);

		try
		{
			final JSONArray loginJSON = new JSONArray(rispostaLogin);
			JSONObject esito = loginJSON.getJSONObject(0);
			codiceEsito = (String) esito.getString("esito");
			if (codiceEsito.equals("200"))
			{
				JSONObject datiAnagr = loginJSON.getJSONObject(1);
				// ultimo accesso
				String ultimoaccesso = datiAnagr.getString("UltimoAccesso");
				TextView ultimoAccesso = (TextView) findViewById(R.id.ultimo_accesso);
				// ultimoAccesso.setTextSize(11);
				ultimoAccesso.setText(ultimoaccesso);
				// nome cliente
				String nomeClienteString = datiAnagr.getString("NomeCliente");
				String cognomeClienteString = datiAnagr.getString("CognomeCliente");
				TextView nomeCliente = (TextView) findViewById(R.id.nome_cliente);
				String nomeInteroCliente = nomeClienteString + " " + cognomeClienteString;
				nomeCliente.setText(nomeInteroCliente);

				JSONArray arrayWebBolletta = new JSONArray(rispostaWebolletta);
				int conti = countWebBolletteNonNulle(arrayWebBolletta);
				String[] elencoContiWebBolletta = elencoCodiciConti(arrayWebBolletta, conti);

				int top;
				View webBolletta_pagina = (View) inflater.inflate(R.layout.domiciliazione_pagina, null);
				RelativeLayout webBolletta_page = new RelativeLayout(this);
				webBolletta_page.removeAllViews();
				for (int i = 0; i < conti; i++)
				{
					if ((i % 2) == 0)
					{
						top = 0;
						webBolletta_page = new RelativeLayout(this);
						RelativeLayout.LayoutParams param_page = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
								RelativeLayout.LayoutParams.WRAP_CONTENT);
						param_page.setMargins(0, top * ((int) getResources().getDimension(R.dimen.domiciliazione_entry_second_top)), 0, 0);
						webBolletta_page.setLayoutParams(param_page);
					} else
					{
						top = 1;
					}
					webBolletta_pagina = (View) inflater.inflate(R.layout.domiciliazione_pagina, null);
					View webBolletta_entry = inflater.inflate(R.layout.webolletta_entry, null);
					RelativeLayout.LayoutParams param = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
					param.setMargins(
							((int) getResources().getDimension(R.dimen.domiciliazione_entry_margin_rightLeft)),
							top * ((int) getResources().getDimension(R.dimen.domiciliazione_entry_second_top))
									+ ((int) getResources().getDimension(R.dimen.top_domiciliazione)),
							((int) getResources().getDimension(R.dimen.domiciliazione_entry_margin_rightLeft)), 0);
					webBolletta_entry.setLayoutParams(param);

					TextView numero_cliente_iesimo = (TextView) webBolletta_entry.findViewById(R.id.number_client_iesimo);
					final String codiceConto = elencoContiWebBolletta[i];
					numero_cliente_iesimo.setText(codiceConto);
					// nome cliente
					TextView client_iesimo = (TextView) webBolletta_entry.findViewById(R.id.name_client_iesimo);
					client_iesimo.setText(nomeInteroCliente);

					final String indirizzoFornituraIesimo = cercaIndirizzoFornitura(loginJSON, codiceConto);
					TextView indirizzo_fornitura = (TextView) webBolletta_entry.findViewById(R.id.indirizzo_fornitura_iesimo);
					indirizzo_fornitura.setText(indirizzoFornituraIesimo);

					final ImageView luce_icona = (ImageView) webBolletta_entry.findViewById(R.id.domiciliazione_iesima_luce_icona);
					final ImageView gas_icona = (ImageView) webBolletta_entry.findViewById(R.id.domiciliazione_iesima_gas_icona);
					boolean isDual = isDual(loginJSON, codiceConto);
					if (isDual)
					{
						luce_icona.setImageResource(R.drawable.light_yes);
						gas_icona.setImageResource(R.drawable.gas_yes);
					} else
					{
						String tipologia = cercaTipologiaFornitura(loginJSON, codiceConto);
						if (tipologia.equals("POWER"))
						{
							luce_icona.setImageResource(R.drawable.light_yes);
							gas_icona.setImageResource(R.drawable.gas_no);
						} else
						{
							luce_icona.setImageResource(R.drawable.light_no);
							gas_icona.setImageResource(R.drawable.gas_yes);
						}
					}

					final String cellulareNotificaSMS = trovaCellulareNotificaSMS(arrayWebBolletta);
					final String statoWebBollettaIesima = cercaStato(arrayWebBolletta, codiceConto);
					TextView stato_domiciliazione_iesima = (TextView) webBolletta_entry.findViewById(R.id.stato_domiciliazione_iesima_descrizione);
					ImageView stato_domiciliazione_dot = (ImageView) webBolletta_entry.findViewById(R.id.stato_domiciliazione_iesima);
					// verifica se la domiciliazione non � attiva in tal caso
					// va
					// nella pagina di attivazione altrimenti va nella pagina
					// dei dettagli:
					if (statoWebBollettaIesima.equals("SERVIZIO ATTIVO"))
					{
						// green
						final String operazioneWebBolletta = cercaOperazioneWeb(arrayWebBolletta, codiceConto);

						stato_domiciliazione_iesima.setVisibility(View.GONE);
//                stato_domiciliazione_iesima.setText(R.string.webolletta_attiva);
//                stato_domiciliazione_iesima.setTextColor(getResources().getColor(R.color.green1));
						stato_domiciliazione_dot.setImageResource(R.drawable.green_dot);

						final ImageView arrow = (ImageView) webBolletta_entry.findViewById(R.id.domiciliazione_iesima_arrow);
						arrow.setBackgroundResource(R.drawable.select_domiciliazione_arrow_disabled);

						ImageView stato_domiciliazione_iesima_descricrizione_btn = (ImageView) webBolletta_entry
								.findViewById(R.id.stato_domiciliazione_iesima_descricrizione_btn);
						stato_domiciliazione_iesima_descricrizione_btn.setVisibility(View.VISIBLE);
						stato_domiciliazione_iesima_descricrizione_btn.setBackground(getResources().getDrawable(R.drawable.bolletta_digitale_disattiva_entry));
						stato_domiciliazione_iesima_descricrizione_btn.setOnClickListener(new OnClickListener() {
							public void onClick(View v)
							{
								// arrow.getBackground().setColorFilter(0xFF999999,
								// PorterDuff.Mode.MULTIPLY);
								// arrow.getBackground().invalidateSelf();
								animation = Utilities.animation(Webolletta.this);
								imageAnim = (ImageView) findViewById(R.id.waiting_anim);
								imageAnim.setVisibility(View.VISIBLE);
								if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN)
								{
									imageAnim.setBackgroundDrawable(animation);
								} else
								{
									imageAnim.setBackground(animation);
								}
								animation.start();
								Intent intentHome = new Intent(getApplicationContext(), WebollettaAttivazioneNotifica.class);
								intentHome.putExtra("CodiceConto", codiceConto);
								intentHome.putExtra("CellulareNotificaSMS", cellulareNotificaSMS);
								boolean isDual = isDual(loginJSON, codiceConto);
								if (isDual)
								{
									intentHome.putExtra("TIPOLOGIA", "DUAL");
								} else
								{
									String tipologia = cercaTipologiaFornitura(loginJSON, codiceConto);
									if (tipologia.equals("POWER"))
									{
										intentHome.putExtra("TIPOLOGIA", "LUCE");
									} else
									{
										intentHome.putExtra("TIPOLOGIA", "GAS");
									}
								}
								intentHome.putExtra("OperazioneWeb", operazioneWebBolletta);
								Webolletta.this.finish();
								startActivity(intentHome);
								overridePendingTransition(R.anim.fade, R.anim.hold);

							}
						});

					} else if (statoWebBollettaIesima.equals("DIGITALE")) {
						final String operazioneWebBolletta = cercaOperazioneWeb(arrayWebBolletta, codiceConto);

						stato_domiciliazione_iesima.setVisibility(View.GONE);
//						stato_domiciliazione_iesima.setText(R.string.bolletta_digitale_attiva);
//						stato_domiciliazione_iesima.setTextColor(getResources().getColor(R.color.green1));
						stato_domiciliazione_dot.setImageResource(R.drawable.green_dot);

//						final ImageView arrow = (ImageView) webBolletta_entry.findViewById(R.id.domiciliazione_iesima_arrow);
//						arrow.setBackgroundResource(R.drawable.select_domiciliazione_arrow);
//						arrow.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
//						arrow.getBackground().invalidateSelf();
//						arrow.setOnClickListener(new OnClickListener() {
//							public void onClick(View v)
//							{
//								// arrow.getBackground().setColorFilter(0xFF999999,
//								// PorterDuff.Mode.MULTIPLY);
//								// arrow.getBackground().invalidateSelf();
//								animation = Utilities.animation(Webolletta.this);
//								imageAnim = (ImageView) findViewById(R.id.waiting_anim);
//								imageAnim.setVisibility(View.VISIBLE);
//								if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN)
//								{
//									imageAnim.setBackgroundDrawable(animation);
//								} else
//								{
//									imageAnim.setBackground(animation);
//								}
//								animation.start();
//								Intent intentHome = new Intent(getApplicationContext(), BollettaDigitaleAttivazioneNotifica.class);
//								intentHome.putExtra("CodiceConto", codiceConto);
//								intentHome.putExtra("CellulareNotificaSMS", cellulareNotificaSMS);
//								boolean isDual = isDual(loginJSON, codiceConto);
//								if (isDual)
//								{
//									intentHome.putExtra("TIPOLOGIA", "DUAL");
//								} else
//								{
//									String tipologia = cercaTipologiaFornitura(loginJSON, codiceConto);
//									if (tipologia.equals("POWER"))
//									{
//										intentHome.putExtra("TIPOLOGIA", "LUCE");
//									} else
//									{
//										intentHome.putExtra("TIPOLOGIA", "GAS");
//									}
//								}
//								intentHome.putExtra("OperazioneWeb", operazioneWebBolletta);
//								Webolletta.this.finish();
//								startActivity(intentHome);
//								overridePendingTransition(R.anim.fade, R.anim.hold);
//
//							}
//						});


						final ImageView arrow = (ImageView) webBolletta_entry.findViewById(R.id.domiciliazione_iesima_arrow);
						arrow.setBackgroundResource(R.drawable.select_domiciliazione_arrow_disabled);

						ImageView stato_domiciliazione_iesima_descricrizione_btn = (ImageView) webBolletta_entry
								.findViewById(R.id.stato_domiciliazione_iesima_descricrizione_btn);
						stato_domiciliazione_iesima_descricrizione_btn.setVisibility(View.VISIBLE);
						stato_domiciliazione_iesima_descricrizione_btn.setBackground(getResources().getDrawable(R.drawable.bolletta_digitale_disattiva_entry));
						stato_domiciliazione_iesima_descricrizione_btn.setOnClickListener(new OnClickListener() {
							public void onClick(View v)
							{
								animation = Utilities.animation(Webolletta.this);
								imageAnim = (ImageView) findViewById(R.id.waiting_anim);
								imageAnim.setVisibility(View.VISIBLE);
								if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN)
								{
									imageAnim.setBackgroundDrawable(animation);
								} else
								{
									imageAnim.setBackground(animation);
								}
								animation.start();
								Intent intentHome = new Intent(getApplicationContext(), BollettaDigitaleAttivazioneNotifica.class);
								intentHome.putExtra("CodiceConto", codiceConto);
								intentHome.putExtra("CellulareNotificaSMS", cellulareNotificaSMS);
								boolean isDual = isDual(loginJSON, codiceConto);
								if (isDual)
								{
									intentHome.putExtra("TIPOLOGIA", "DUAL");
								} else
								{
									String tipologia = cercaTipologiaFornitura(loginJSON, codiceConto);
									if (tipologia.equals("POWER"))
									{
										intentHome.putExtra("TIPOLOGIA", "LUCE");
									} else
									{
										intentHome.putExtra("TIPOLOGIA", "GAS");
									}
								}
								intentHome.putExtra("OperazioneWeb", operazioneWebBolletta);
								Webolletta.this.finish();
								startActivity(intentHome);
								overridePendingTransition(R.anim.fade, R.anim.hold);

							}
						});


					} else if (statoWebBollettaIesima.equals("SERVIZIO NON ATTIVO"))
					{
						// rosso
						stato_domiciliazione_iesima.setVisibility(View.GONE);
						// stato_domiciliazione_iesima.setText(R.string.domiciliazione_non_attiva);
						// stato_domiciliazione_iesima.setTextColor(getResources().getColor(R.color.red1));
						stato_domiciliazione_dot.setImageResource(R.drawable.red_dot);

						final String operazioneWebBolletta = cercaOperazioneWeb(arrayWebBolletta, codiceConto);
						final ImageView arrow = (ImageView) webBolletta_entry.findViewById(R.id.domiciliazione_iesima_arrow);
						arrow.setBackgroundResource(R.drawable.select_domiciliazione_arrow_disabled);

						ImageView stato_domiciliazione_iesima_descricrizione_btn = (ImageView) webBolletta_entry
								.findViewById(R.id.stato_domiciliazione_iesima_descricrizione_btn);
						stato_domiciliazione_iesima_descricrizione_btn.setVisibility(View.VISIBLE);
						stato_domiciliazione_iesima_descricrizione_btn.setOnClickListener(new OnClickListener() {
							public void onClick(View v)
							{
								animation = Utilities.animation(Webolletta.this);
								imageAnim = (ImageView) findViewById(R.id.waiting_anim);
								imageAnim.setVisibility(View.VISIBLE);
								if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN)
								{
									imageAnim.setBackgroundDrawable(animation);
								} else
								{
									imageAnim.setBackground(animation);
								}
								animation.start();
								Intent intentHome = new Intent(getApplicationContext(), WebollettaActivate.class);
								intentHome.putExtra("CodiceConto", codiceConto);
								boolean isDual = isDual(loginJSON, codiceConto);
								if (isDual)
								{
									intentHome.putExtra("TIPOLOGIA", "DUAL");
								} else
								{
									String tipologia = cercaTipologiaFornitura(loginJSON, codiceConto);
									if (tipologia.equals("POWER"))
									{
										intentHome.putExtra("TIPOLOGIA", "LUCE");
									} else
									{
										intentHome.putExtra("TIPOLOGIA", "GAS");
									}
								}
								intentHome.putExtra("OperazioneWeb", operazioneWebBolletta);
								Webolletta.this.finish();
								startActivity(intentHome);
								overridePendingTransition(R.anim.fade, R.anim.hold);

							}
						});
					} else if (statoWebBollettaIesima.equals("SERVIZIO IN ATTIVAZIONE") || statoWebBollettaIesima.equals("SERVIZIO IN DISATTIVAZIONE"))
					{
						// gialla
						if (statoWebBollettaIesima.equals("SERVIZIO IN ATTIVAZIONE"))
						{
							stato_domiciliazione_iesima.setText(R.string.webolletta_in_attivazione);
						} else if (statoWebBollettaIesima.equals("SERVIZIO IN DISATTIVAZIONE"))
						{
							stato_domiciliazione_iesima.setText(R.string.webolletta_in_disattivazione);
						}
						stato_domiciliazione_iesima.setTextColor(getResources().getColor(R.color.yellow1));
						stato_domiciliazione_dot.setImageResource(R.drawable.yellow_dot);
						final ImageView arrow = (ImageView) webBolletta_entry.findViewById(R.id.domiciliazione_iesima_arrow);
						arrow.setBackgroundResource(R.drawable.select_domiciliazione_arrow);
						arrow.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
						arrow.getBackground().invalidateSelf();
						arrow.setOnClickListener(new OnClickListener() {
							public void onClick(View v)
							{
								// arrow.getBackground().setColorFilter(0xFF999999,
								// PorterDuff.Mode.MULTIPLY);
								// arrow.getBackground().invalidateSelf();
								animation = Utilities.animation(Webolletta.this);
								imageAnim = (ImageView) findViewById(R.id.waiting_anim);
								imageAnim.setVisibility(View.VISIBLE);
								if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN)
								{
									imageAnim.setBackgroundDrawable(animation);
								} else
								{
									imageAnim.setBackground(animation);
								}
								animation.start();
								Intent intentHome = new Intent(getApplicationContext(), WebollettaDetail.class);
								intentHome.putExtra("CodiceConto", codiceConto);
								intentHome.putExtra("StatoWeb", statoWebBollettaIesima);
								intentHome.putExtra("IndirizzoFornitura", indirizzoFornituraIesimo);
								Webolletta.this.finish();
								startActivity(intentHome);
								overridePendingTransition(R.anim.fade, R.anim.hold);

							}
						});
					} else if (statoWebBollettaIesima.equals(""))
					{
						// StatoWeb � vuoto
						continue;
					}
					webBolletta_page.addView(webBolletta_entry);

					if ((i % 2) != 0 && (i != 0))
					{
						LinearLayout pagina_domiciliazione = (LinearLayout) webBolletta_pagina.findViewById(R.id.domiciliazione_pagina);
						pagina_domiciliazione.removeAllViews();
						pagina_domiciliazione.addView(webBolletta_page);
						webolletta_pager.addView(webBolletta_pagina);
					}
				}

				if ((conti % 2) != 0)
				{
					LinearLayout pagina_domiciliazione = (LinearLayout) webBolletta_pagina.findViewById(R.id.domiciliazione_pagina);
					pagina_domiciliazione.removeAllViews();
					pagina_domiciliazione.addView(webBolletta_page);
					webolletta_pager.addView(webBolletta_pagina);
				}
			}
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
		final LinearLayout scroller = (LinearLayout) findViewById(R.id.webolletta_scroller);
		int numeroPagine = webolletta_pager.getChildCount();
		for (int i = 0; i < numeroPagine; i++)
		{
			ImageView iv = new ImageView(this);
			iv.setId(i);
			LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
			param.setMargins(10, 0, 0, 0);
			iv.setImageResource(R.drawable.dot_not_selected);
			iv.setLayoutParams(param);
			scroller.addView(iv);

			int idFocusedPage = webolletta_pager.getCurrentPage();
			ImageView pageFocused = (ImageView) findViewById(idFocusedPage);
			pageFocused.setImageResource(R.drawable.dot_selected);

		}

		webolletta_pager.addOnScrollListener(new HorizontalPager.OnScrollListener() {
			int numeroPagine = webolletta_pager.getChildCount();

			public void onScroll(int scrollX)
			{
				for (int i = 0; i < numeroPagine; i++)
				{
					ImageView iv = (ImageView) findViewById(i);
					iv.setImageResource(R.drawable.dot_not_selected);
				}
				int idFocusedPage = webolletta_pager.getCurrentPage();
				ImageView pageFocused = (ImageView) findViewById(idFocusedPage);
				pageFocused.setImageResource(R.drawable.dot_selected);
			}

			public void onViewScrollFinished(int currentPage)
			{
				for (int i = 0; i < numeroPagine; i++)
				{
					ImageView iv = (ImageView) findViewById(i);
					iv.setImageResource(R.drawable.dot_not_selected);
				}
				int idFocusedPage = webolletta_pager.getCurrentPage();
				ImageView pageFocused = (ImageView) findViewById(idFocusedPage);
				pageFocused.setImageResource(R.drawable.dot_selected);
			}
		});

	}

	private String[] elencoCodiciConti(JSONArray array, int lunghezza)
	{
		String result[] = new String[lunghezza];

		for (int i = 1; i <= lunghezza; i++)
		{
			try
			{
				JSONObject webBollettaEntry = (JSONObject) array.getJSONObject(i);
				String codiceConto = "";
				codiceConto = webBollettaEntry.getString("ContoCliente");
				if (!codiceConto.equals(""))
				{
					result[i - 1] = codiceConto;
				} else
				{
					continue;
				}
			} catch (JSONException e)
			{
				e.printStackTrace();
				continue;
			}
		}

		return result;

	}

	private String cercaIndirizzoFornitura(JSONArray array, String codiceConto)
	{
		String result = "";
		String codiceContoIesimo = "";
		JSONObject anagrContoIesimo = new JSONObject();

		try
		{
			for (int i = 2; i < array.length(); i++)
			{
				JSONArray contoIesimo;

				contoIesimo = array.getJSONArray(i);
				// codice conto
				anagrContoIesimo = contoIesimo.getJSONObject(0);
				codiceContoIesimo = anagrContoIesimo.getString("CodiceConto");

				if (!codiceContoIesimo.equals(codiceConto))
				{
					// codice conto diverso
					continue;
				} else
				{
					JSONObject anagrExtraContoIesimo = contoIesimo.getJSONObject(1);
					result = anagrExtraContoIesimo.getString("IndirizzoCompleto");
				}

			}
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
		return result;
	}

	private boolean isDual(JSONArray arrayFornitureLogin, String codiceConto)
	{
		boolean result = false;
		int count = 0;
		JSONObject anagrContoIesimo = new JSONObject();
		try
		{
			for (int i = 2; i < arrayFornitureLogin.length(); i++)
			{
				JSONArray contoIesimo;
				contoIesimo = arrayFornitureLogin.getJSONArray(i);
				anagrContoIesimo = contoIesimo.getJSONObject(0);

				String codiceContoIesimo = anagrContoIesimo.getString("CodiceConto");
				if (!codiceContoIesimo.equals(codiceConto))
				{
					continue;
				} else
				{
					// ho trovato il conto codiceConto
					count++;
				}
			}
			if (count == 2)
			{
				result = true;
			} else
			{
				result = false;
			}

		} catch (JSONException e)
		{
			e.printStackTrace();
		}
		return result;

	}

	private String cercaTipologiaFornitura(JSONArray array, String codiceConto)
	{
		String result = "";
		JSONObject anagrContoIesimo = new JSONObject();
		JSONObject anagrExtraContoIesimo = new JSONObject();
		try
		{
			for (int i = 2; i < array.length(); i++)
			{
				JSONArray contoIesimo;
				contoIesimo = array.getJSONArray(i);
				anagrContoIesimo = contoIesimo.getJSONObject(0);

				String codiceContoIesimo = anagrContoIesimo.getString("CodiceConto");
				if (!codiceContoIesimo.equals(codiceConto))
				{
					continue;
				} else
				{
					// ho trovato il conto codiceConto
					if (contoIesimo.length() > 1)
					{
						anagrExtraContoIesimo = contoIesimo.getJSONObject(1);
						result = anagrExtraContoIesimo.getString("Tipologia");
					}
				}
			}
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
		return result;

	}

	public String cercaStato(JSONArray arrayWebBolletta, String codiceConto)
	{
		String result = "";
		for (int i = 1; i < arrayWebBolletta.length(); i++)
		{
			try
			{
				JSONObject webBollettaEntry = arrayWebBolletta.getJSONObject(i);
				String statoWeb = "";
				if (webBollettaEntry.length() > 1)
				{
					statoWeb = webBollettaEntry.getString("StatoWeb");
				}
				if (!statoWeb.equals(""))
				{
					String codiceContoIesimo = webBollettaEntry.getString("ContoCliente");
					if (!codiceContoIesimo.equals(codiceConto))
					{
						continue;
					} else
					{
						result = statoWeb;
					}
				} else
				{
					continue;
				}
			} catch (JSONException e)
			{
				e.printStackTrace();
				continue;
			}
		}

		return result;
	}

	public String cercaOperazioneWeb(JSONArray arrayWebBolletta, String codiceConto)
	{
		String result = "";
		for (int i = 1; i < arrayWebBolletta.length(); i++)
		{
			try
			{
				JSONObject webBollettaEntry = arrayWebBolletta.getJSONObject(i);
				if (webBollettaEntry.length() > 1)
				{
					String codiceContoIesimo = webBollettaEntry.getString("ContoCliente");
					if (codiceContoIesimo.equals(codiceConto))
					{
						String operazioneWeb = webBollettaEntry.getString("OperazioneWeb");
						result = operazioneWeb;
					} else
					{
						continue;
					}
				} else
				{
					continue;
				}
			} catch (JSONException e)
			{
				e.printStackTrace();
				continue;
			}
		}

		return result;
	}

	public int countWebBolletteNonNulle(JSONArray array)
	{
		int result = 0;

		for (int i = 1; i < array.length(); i++)
		{
			try
			{
				JSONObject oggettoWebBolletta = array.getJSONObject(i);
				String statoWeb = "";
				if (oggettoWebBolletta.length() > 1)
				{
					statoWeb = oggettoWebBolletta.getString("ContoCliente");
				}
				if (!statoWeb.equals(""))
				{
					result++;
				} else
				{
					continue;
				}
			} catch (JSONException e)
			{
				e.printStackTrace();
				continue;
			}
		}

		return result;
	}

	public String trovaCellulareNotificaSMS(JSONArray array)
	{
		String result = "";
		try
		{
			JSONObject cellulareOggetto = array.getJSONObject(array.length() - 1);
			if (cellulareOggetto.length() == 1)
			{
				result = cellulareOggetto.getString("CellulareNotificaSMS");
			}
		} catch (JSONException e)
		{
			e.printStackTrace();
			result = "";
		}
		return result;
	}

	public void onBackPressed()
	{
		animation = Utilities.animation(Webolletta.this);
		imageAnim = (ImageView) findViewById(R.id.waiting_anim);
		imageAnim.setVisibility(View.VISIBLE);
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN)
		{
			imageAnim.setBackgroundDrawable(animation);
		} else
		{
			imageAnim.setBackground(animation);
		}
		animation.start();
		Intent intentHome = new Intent(getApplicationContext(), Home.class);
		Webolletta.this.finish();
		startActivity(intentHome);
		overridePendingTransition(R.anim.fade, R.anim.hold);
	}

}
